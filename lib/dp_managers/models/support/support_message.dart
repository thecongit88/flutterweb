
import '../../../models/files/images.dart';

class SupportMessage {
  int? id;
  String? content;
  String? messageDate;
  int? hbccId;
  String? hbccName;
  int? status;
  String? createdAt;
  String? updatedAt;
  int? conversationId;
  String? conversationName;
  String? link;
  int? memberId;
  String? memberName;
  List<Images>? images;

  SupportMessage(
      {this.id,
        this.content,
        this.messageDate,
        this.hbccId,
        this.hbccName,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.conversationId,
        this.conversationName,
        this.memberId,
        this.memberName,
        this.link,
        this.images});

  SupportMessage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    content = json['content'];
    messageDate = json['message_date'];
    hbccId = json["hbcc"] != null ? json['hbcc']["id"] : 0;
    hbccName =  json["hbcc"] != null ? json['hbcc']["full_name"] : "";
    memberId =  json["member"] != null ? json['member']["id"] : 0;
    memberName = json["member"] != null ? json['member']["full_name"] : "";
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    link = json['link'];
    if (json['images'] != null) {
      images = <Images>[];
      json['images'].forEach((v) {
        images!.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['content'] = this.content;
    data['message_date'] = this.messageDate;
    data['hbcc'] = this.hbccId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['conversation'] = this.conversationId;
    data['member'] = this.memberId;
    data['link'] = this.link;
    return data;
  }
}

class SupportMessageResponse {
  List<SupportMessage> items = [];
  int pages = 0;

  SupportMessageResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new SupportMessage.fromJson(item);
        items.add(it);
      });
    }
  }
}
