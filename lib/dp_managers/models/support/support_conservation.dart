import '../../../models/files/images.dart';

class SupportConversation {
  int? id;
  String? title;
  String? supportDate;
  int? memberId;
  String? memberName;
  int? ncsId;
  String? ncsName;
  int? status;
  int? hbccId;
  String? hbccName;
  String? createdAt;
  String? updatedAt;
  String? summary;
  int? provinceId;
  String? provinceName;
  int? districtId;
  String? districtName;
  int? wardId;
  String? wardName;
  String? code;
  List<Images>? images;

  SupportConversation(
      {this.id,
        this.title,
        this.supportDate,
        this.memberId,
        this.memberName,
        this.ncsId,
        this.ncsName,
        this.status,
        this.hbccId,
        this.hbccName,
        this.createdAt,
        this.updatedAt,
        this.summary,
        this.provinceId,
        this.provinceName,
        this.districtId,
        this.districtName,
        this.wardId,
        this.wardName,
        this.code,
        this.images});

  SupportConversation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    supportDate = json['support_date'];
    ncsId = json['ncsId'];
    ncsName = json['ncsName'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    summary = json['summary'];
    hbccId = json["hbcc"] != null ? json['hbcc']["id"] : 0;
    hbccName =  json["hbcc"] != null ? json['hbcc']["full_name"] : "";
    memberId =  json["member"] != null ? json['member']["id"] : 0;
    memberName = json["member"] != null ? json['member']["full_name"] : "";
    provinceId = json['provinceId'];
    provinceName = json['provinceName'];
    districtId = json['districtId'];
    districtName = json['districtName'];
    wardId = json['wardId'];
    wardName = json['wardName'];
    code = json['code'];
    if (json['images'] != null) {
      images = <Images>[];
      json['images'].forEach((v) {
        images!.add(new Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['support_date'] = this.supportDate;
    data['member'] = this.memberId;
    data['ncs'] = this.ncsId;
    data['status'] = this.status;
    data['hbcc'] = this.hbccId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['summary'] = this.summary;
    data['province'] = this.provinceId;
    data['district'] = this.districtId;
    data['ward'] = this.wardId;
    data['code'] = this.code;
    return data;
  }
}


class SupportConversationResponse {
  List<SupportConversation> items = [];
  int pages = 0;

  SupportConversationResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new SupportConversation.fromJson(item);
        items.add(it);
      });
    }
  }
}
