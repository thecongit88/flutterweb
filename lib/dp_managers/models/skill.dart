class Skill {
  Skill({
    this.id = 0,
    this.title = '',
    this.skillGroup = 0,
    this.isPublished = true,
    this.isPublic = true,
  });

  int id;
  String title;
  int skillGroup;
  bool isPublished;
  bool isPublic;

  static List<Skill> skillList = <Skill>[
    Skill(
        id: 1,
        title: 'Hướng dẫn TKT tự đi vệ sinh bằng bô',
        skillGroup: 1,
        isPublished: true,
        isPublic: true,
    ),
    Skill(
      id: 2,
      title: 'Chăm sóc TKT nặng đi vệ sinh',
      skillGroup: 1,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 3,
      title: 'Hướng dẫn NKT nam tự đi tiểu khi ngồi xe lăn hoặc ngồi ghế',
      skillGroup: 1,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 4,
      title: 'Chăm sóc cho NKT đi vệ sinh tại giường',
      skillGroup: 1,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 5,
      title: 'Kỹ năng hướng dẫn TKT tự tắm',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 6,
      title: 'Hướng dẫn tắm cho TKT ở tư thế ngồi',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 7,
      title: 'Hướng dẫn NKT tự tắm',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 8,
      title: 'Tắm cho NKT không có khả năng tự thực hiện',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 9,
      title: 'Hướng dẫn TKT tự mặc quần áo',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 10,
      title: 'Thay quần áo cho TKT',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 11,
      title: 'Hướng dẫn NKT tự mặc quần áo',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 12,
      title: 'Thay quần áo cho NKT',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 13,
      title: 'Thay ga giường khi có NKT đang nằm',
      skillGroup: 2,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 14,
      title: 'Hướng dẫn TKT tự đánh răng',
      skillGroup: 3,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 15,
      title: 'Kỹ năng đánh răng cho TKT, NKT',
      skillGroup: 3,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 16,
      title: 'Hướng dẫn NKT tự đánh răng',
      skillGroup: 3,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 17,
      title: 'Đánh răng cho NKT tại giường',
      skillGroup: 3,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 18,
      title: 'Hướng dẫn NKT tăng cường giao tiếp',
      skillGroup: 4,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 19,
      title: 'Hướng dẫn TKT tự ăn uống ',
      skillGroup: 5,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 20,
      title: 'Cho TKT ăn',
      skillGroup: 5,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 21,
      title: 'Hỗ trợ NKT có khả năng ngồi ăn',
      skillGroup: 5,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 22,
      title: 'Cho NKT nằm tại chỗ ăn bằng đường miệng',
      skillGroup: 5,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 23,
      title: 'Cho NKT ăn qua thông dạ dày',
      skillGroup: 5,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 24,
      title: 'Thay đổi tư thế từ nằm sang ngồi cho NKT',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 25,
      title: 'Thay đổi tư thế từ ngồi sang đứng cho NKT',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 26,
      title: 'Tập đi với nạng cho NKT',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 27,
      title: 'Kỹ thuật lên xuống cầu thang dùng nạng cho NKT',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 28,
      title: 'Hướng dẫn NKT liệt nửa người ra vào xe lăn',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 29,
      title: 'Hỗ trợ NKT sử dụng xe lăn qua các địa hình',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 30,
      title: 'Điều chỉnh, sửa chữa dụng cụ trong nhà giúp NKT di chuyển, vận động',
      skillGroup: 6,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 31,
      title: 'Nguyên tắc và kỹ năng phòng chống bạo lực dựa trên giới đối với NKT',
      skillGroup: 7,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 32,
      title: 'Phát hiện và phòng ngừa vết loét do tì đè',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 33,
      title: 'Chăm sóc vết loét đối với người nằm liệt lâu ngày',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 34,
      title: 'Đặt tư thế nằm đúng cho NKT: tư thế nằm ngửa',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 35,
      title: 'Đặt tư thế nằm đúng cho NKT: tư thế nằm nghiêng bên liệt',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 36,
      title: 'Đặt tư thế nằm đúng cho NKT: tư thế nằm nghiêng bên lành',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 37,
      title: 'Lăn trở cho NKT nằm lâu: lăn trở sang bên liệt',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 38,
      title: 'Lăn trở cho NKT nằm lâu: lăn trở sang bên lành',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 39,
      title: 'Phát hiện và xử lý các trường hợp khẩn cấp: sốt-bỏng-hóc ở TKT',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
    Skill(
      id: 40,
      title: 'Phát hiện các biểu hiện của đột quỵ',
      skillGroup: 8,
      isPublished: true,
      isPublic: true,
    ),
  ];
}

class UserSkill{
    UserSkill({
      this.id = 0,
      this.user = 0,
      this.skillId = 0,
      this.title = "",
      this.isHavePlan = true,
      this.isSeleted = false
    });

    int id;
    String title;
    int user;
    int skillId;
    bool isHavePlan;
    bool isSeleted;

    static List<UserSkill> userSkillList = <UserSkill>[
      UserSkill(
        title : "Hướng dẫn TKT tự đi vệ sinh bằng bô",
        skillId : 1,
        user : 1,
        isHavePlan : true,
          isSeleted : false
      ),
      UserSkill(
          title : "Chăm sóc cho NKT đi vệ sinh tại giường",
          skillId : 4,
          user : 1,
          isHavePlan : false,
          isSeleted : false
      ),
      UserSkill(
          title : "Kỹ năng hướng dẫn TKT tự tắm",
          skillId : 5,
          user : 1,
          isHavePlan : true,
          isSeleted : false
      ),
      UserSkill(
          title : "Thay quần áo cho NKT",
          skillId : 12,
          user : 1,
          isHavePlan : false,
          isSeleted : false
      ),
    ];

}