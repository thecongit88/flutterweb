import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/form_types.dart';

class FormItemLogBookResponse {
  List<FormItemLogBook> items = <FormItemLogBook>[];

  FormItemLogBookResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new FormItemLogBook.fromJson(item);
        items.add(it);
      });
    }
  }
}

class FormItemLogBook {
  int? id;
  String? name;
  String? formDate;
  int? formId;
  FormType? formType;
  int? formTypeId;
  bool? formKyNang;
  int? hbccId;
  HBCC? hbcc;
  int? memberId;
  Member? member;
  bool? is_ncs;

  FormItemLogBook({
    this.id = 0,
    this.name = '',
    this.formDate = '',
    this.formId = 0,
    this.formTypeId = 0,
    this.formType,
    this.hbccId = 0,
    this.hbcc,
    this.memberId = 0,
    this.member,
    this.is_ncs = false,
    this.formKyNang = false
  });

  FormItemLogBook.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = parsedJson["name"].toString().trim();
    formDate = parsedJson["form_date"];
    formId = parsedJson["form_id"];
    formTypeId = parsedJson["form_type"] != null ? parsedJson["form_type"]["id"] : 0;
    formKyNang = parsedJson["form_type"] != null && parsedJson["form_type"]["form_kynang"] != null? parsedJson["form_type"]["form_kynang"] : false;
    formType = parsedJson["form_type"] != null ? FormType.fromJson(parsedJson["form_type"]): new FormType();
    hbccId = parsedJson["hbcc"] != null ? parsedJson["hbcc"]["id"] : 0;
    memberId = parsedJson["member"] != null ? parsedJson["member"]["id"] : 0;
    member = parsedJson["member"] != null? Member.fromMap(parsedJson["member"]): new Member();
    is_ncs = parsedJson["is_ncs"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['form_date'] = this.formDate;
    data['form_id'] = this.formId;
    data['form_type_id'] = this.formTypeId;
    data['form_type'] = this.formType;
    data['hbcc_id'] = this.hbccId;
    data['hbcc'] = this.hbcc;
    data['member_id'] = this.memberId;
    data['member'] = this.memberId;
    data['is_ncs'] = this.is_ncs;
    return data;
  }
}