class AddCarePlanItem {
  int id = 0;
  String? title;
  String? itemDate;
  String? itemTime;
  String? plan;
  int? status;
  String? expectedResult;
  String? careProcessItem;

  AddCarePlanItem(
      {
        this.id = 0,
        this.title,
        this.itemDate,
        this.itemTime,
        this.plan,
        this.status,
        this.expectedResult,
        this.careProcessItem});

  AddCarePlanItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    itemDate = json['item_date'];
    itemTime = json['item_time'];
    plan = json['plan'];
    status = json['status'];
    expectedResult = json['expected_result'];
    careProcessItem = json['care_process_item'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['item_date'] = this.itemDate;
    data['item_time'] = this.itemTime;
    data['plan'] = this.plan;
    data['status'] = this.status;
    data['expected_result'] = this.expectedResult;
    data['care_process_item'] = this.careProcessItem;
    return data;
  }
}
