import '../../../common/utils.dart';

class HbccThemKeHoachModel {
  String? title;
  int? month;
  int? year;
  String? hbcc;
  String? member;
  int? status;
  String? province;
  String? district;
  String? ward;
  String? code;
  String? ncs;

  HbccThemKeHoachModel(
      {this.title,
        this.month,
        this.year,
        this.hbcc,
        this.member,
        this.status,
        this.province,
        this.district,
        this.ward,
        this.code,
        this.ncs});

  HbccThemKeHoachModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    month = json['month'];
    year = json['year'];
    hbcc = json['hbcc'];
    member = json['member'];
    status = json['status'];
    province = !isNullOrEmpty(json['province']) ? json['province'] : "0";
    district = !isNullOrEmpty(json['district']) ? json['district']: "0";
    ward = !isNullOrEmpty(json['ward']) ? json['ward']: "0";
    code = json['code'];
    ncs = json['ncs'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['month'] = this.month;
    data['year'] = this.year;
    data['hbcc'] = this.hbcc;
    data['member'] = this.member;
    data['status'] = this.status;
    data['province'] = !isNullOrEmpty(province) ? province : "0"; //this.province;
    data['district'] = !isNullOrEmpty(district) ? district : "0"; //this.district;
    data['ward'] = !isNullOrEmpty(ward) ? ward : "0"; //this.ward;
    data['code'] = this.code;
    data['ncs'] = this.ncs;
    return data;
  }
}
