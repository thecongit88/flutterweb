class HbccItemKeHoach {
  int? id;
  String? title;
  int? month;
  int? year;
  Hbcc? hbcc;
  MemberItemKeHoach? member;
  int? status;
  Province? province;
  District? district;
  Ward? ward;
  String? createdAt;
  String? updatedAt;
  String? code;
  Ncs? ncs;

  HbccItemKeHoach(
      {this.id,
        this.title,
        this.month,
        this.year,
        this.hbcc,
        this.member,
        this.status,
        this.province,
        this.district,
        this.ward,
        this.createdAt,
        this.updatedAt,
        this.code,
        this.ncs});

  HbccItemKeHoach.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    month = json['month'];
    year = json['year'];
    hbcc = json['hbcc'] != null ? new Hbcc.fromJson(json['hbcc']) : null;
    member =
    json['member'] != null ? new MemberItemKeHoach.fromJson(json['member']) : null;
    status = json['status'];
    province = json['province'] != null
        ? new Province.fromJson(json['province'])
        : null;
    district = json['district'] != null
        ? new District.fromJson(json['district'])
        : null;
    ward = json['ward'] != null ? new Ward.fromJson(json['ward']) : null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    code = json['code'];
    ncs = json['ncs'] != null ? new Ncs.fromJson(json['ncs']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['month'] = this.month;
    data['year'] = this.year;
    if (this.hbcc != null) {
      data['hbcc'] = this.hbcc!.toJson();
    }
    if (this.member != null) {
      data['member'] = this.member!.toJson();
    }
    data['status'] = this.status;
    if (this.province != null) {
      data['province'] = this.province!.toJson();
    }
    if (this.district != null) {
      data['district'] = this.district!.toJson();
    }
    if (this.ward != null) {
      data['ward'] = this.ward!.toJson();
    }
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['code'] = this.code;
    if (this.ncs != null) {
      data['ncs'] = this.ncs!.toJson();
    }
    return data;
  }
}

class Hbcc {
  int? id;
  String? firstName;
  String? createdAt;
  String? updatedAt;
  String? lastName;
  int? user;
  String? phone;
  String? address;
  int? district;
  int? province;
  int? ward;
  String? fullName;
  Null? createdBy;
  Null? updatedBy;
  Null? approved;
  int? gender;
  Null? avatar;

  Hbcc(
      {this.id,
        this.firstName,
        this.createdAt,
        this.updatedAt,
        this.lastName,
        this.user,
        this.phone,
        this.address,
        this.district,
        this.province,
        this.ward,
        this.fullName,
        this.createdBy,
        this.updatedBy,
        this.approved,
        this.gender,
        this.avatar});

  Hbcc.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    lastName = json['last_name'];
    user = json['user'];
    phone = json['phone'];
    address = json['address'];
    district = json['district'];
    province = json['province'];
    ward = json['ward'];
    fullName = json['full_name'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    approved = json['approved'];
    gender = json['gender'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['last_name'] = this.lastName;
    data['user'] = this.user;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['district'] = this.district;
    data['province'] = this.province;
    data['ward'] = this.ward;
    data['full_name'] = this.fullName;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['approved'] = this.approved;
    data['gender'] = this.gender;
    data['avatar'] = this.avatar;
    return data;
  }
}

class MemberItemKeHoach {
  int? id;
  String? memberCode;
  String? firstName;
  String? lastName;
  String? projectJoinDate;
  int? gender;
  String? fullName;

  /*int? disabilityType;
  int? nation;
  String? createdAt;
  String? updatedAt;
  String? birthDate;
  String? disabilityDate;
  int? hbcc;
  int? user;
  String? phone;
  Null? address;
  bool? isAgentOrange;
  int? birthYear;
  int? personalAssistant;
  int? province;
  int? district;
  int? ward;
  Null? disabilityStart;
  int? disabilityYear;
  Null? nationOther;
  Null? approved;
  Null? isSuccess;
  Null? disabilityOther;
  Null? createdBy;
  Null? updatedBy;
  Null? avatar;*/

  MemberItemKeHoach(
      {this.id,
        this.memberCode,
        this.firstName,
        this.lastName,
        this.projectJoinDate,
        this.gender,
        this.fullName,

        /*this.disabilityType,
        this.nation,
        this.createdAt,
        this.updatedAt,
        this.birthDate,
        this.disabilityDate,
        this.hbcc,
        this.user,
        this.phone,
        this.address,
        this.isAgentOrange,
        this.birthYear,
        this.personalAssistant,
        this.province,
        this.district,
        this.ward,

        this.disabilityStart,
        this.disabilityYear,
        this.nationOther,
        this.approved,
        this.isSuccess,
        this.disabilityOther,
        this.createdBy,
        this.updatedBy,
        this.avatar*/
      });

  MemberItemKeHoach.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberCode = json['member_code'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    projectJoinDate = json['project_join_date'];
    gender = json['gender'];
    fullName = json['full_name'];

    /*disabilityType = json['disability_type'];
    nation = json['nation'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    birthDate = json['birth_date'];
    disabilityDate = json['disability_date'];
    hbcc = json['hbcc'];
    user = json['user'];
    phone = json['phone'];
    address = json['address'];
    isAgentOrange = json['is_agent_orange'];
    birthYear = json['birth_year'];
    personalAssistant = json['personal_assistant'];
    province = json['province'];
    district = json['district'];
    ward = json['ward'];
    disabilityStart = json['disability_start'];
    disabilityYear = json['disability_year'];
    nationOther = json['nation_other'];
    approved = json['approved'];
    isSuccess = json['is_success'];
    disabilityOther = json['disability_other'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    avatar = json['avatar'];*/
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['member_code'] = this.memberCode;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['project_join_date'] = this.projectJoinDate;
    data['gender'] = this.gender;
    data['full_name'] = this.fullName;

    /*data['disability_type'] = this.disabilityType;
    data['nation'] = this.nation;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['birth_date'] = this.birthDate;
    data['disability_date'] = this.disabilityDate;
    data['hbcc'] = this.hbcc;
    data['user'] = this.user;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['is_agent_orange'] = this.isAgentOrange;
    data['birth_year'] = this.birthYear;
    data['personal_assistant'] = this.personalAssistant;
    data['province'] = this.province;
    data['district'] = this.district;
    data['ward'] = this.ward;
    data['disability_start'] = this.disabilityStart;
    data['disability_year'] = this.disabilityYear;
    data['nation_other'] = this.nationOther;
    data['approved'] = this.approved;
    data['is_success'] = this.isSuccess;
    data['disability_other'] = this.disabilityOther;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['avatar'] = this.avatar;*/
    return data;
  }
}

class Province {
  int? id;
  String? code;
  String? name;
  int? ordering;
  String? createdAt;
  String? updatedAt;
  Null? createdBy;
  Null? updatedBy;

  Province(
      {this.id,
        this.code,
        this.name,
        this.ordering,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy});

  Province.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    ordering = json['ordering'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['ordering'] = this.ordering;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class District {
  int? id;
  String? code;
  String? name;
  int? ordering;
  int? province;
  String? createdAt;
  String? updatedAt;
  Null? createdBy;
  Null? updatedBy;

  District(
      {this.id,
        this.code,
        this.name,
        this.ordering,
        this.province,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy});

  District.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    ordering = json['ordering'];
    province = json['province'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['ordering'] = this.ordering;
    data['province'] = this.province;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Ward {
  int? id;
  String? code;
  String? name;
  int? ordering;
  int? district;
  String? createdAt;
  String? updatedAt;
  Null? createdBy;
  Null? updatedBy;

  Ward(
      {this.id,
        this.code,
        this.name,
        this.ordering,
        this.district,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy});

  Ward.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    ordering = json['ordering'];
    district = json['district'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['ordering'] = this.ordering;
    data['district'] = this.district;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}

class Ncs {
  int? id;
  String? fistName;
  String? createdAt;
  String? updatedAt;
  String? lastName;
  String? birthDate;
  bool? isHavePhone;
  bool? isSmartphone;
  int? gender;
  int? nation;
  Null? disabilityTypes;
  bool? isHaveInternet;
  int? eduLevel;
  int? maritalStatus;
  int? jobType;
  int? relationshipType;
  Null? member;
  int? birthYear;
  Null? nationOther;
  String? maritalStatusOther;
  Null? jobTypeOther;
  String? relationshipTypeOther;
  bool? vaitroNcscChichamsocNkt;
  bool? vaitroNcscLaodongTaothunhap;
  bool? vaitroNcscNguoicsCacthanhvien;
  Null? createdBy;
  Null? updatedBy;
  int? province;
  int? district;
  int? ward;
  String? address;

  Ncs(
      {this.id,
        this.fistName,
        this.createdAt,
        this.updatedAt,
        this.lastName,
        this.birthDate,
        this.isHavePhone,
        this.isSmartphone,
        this.gender,
        this.nation,
        this.disabilityTypes,
        this.isHaveInternet,
        this.eduLevel,
        this.maritalStatus,
        this.jobType,
        this.relationshipType,
        this.birthYear,
        this.member,
        this.nationOther,
        this.maritalStatusOther,
        this.jobTypeOther,
        this.relationshipTypeOther,
        this.vaitroNcscChichamsocNkt,
        this.vaitroNcscLaodongTaothunhap,
        this.vaitroNcscNguoicsCacthanhvien,
        this.createdBy,
        this.updatedBy,
        this.province,
        this.district,
        this.ward,
        this.address});

  Ncs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fistName = json['fist_name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    lastName = json['last_name'];
    birthDate = json['birth_date'];
    isHavePhone = json['is_have_phone'];
    isSmartphone = json['is_smartphone'];
    gender = json['gender'];
    nation = json['nation'];
    disabilityTypes = json['disability_types'];
    isHaveInternet = json['is_have_internet'];
    eduLevel = json['edu_level'];
    maritalStatus = json['marital_status'];
    jobType = json['job_type'];
    relationshipType = json['relationship_type'];
    birthYear = json['birth_year'];
    member = json['member'];
    birthYear = json['BirthYear'];
    nationOther = json['nation_other'];
    maritalStatusOther = json['marital_status_other'];
    jobTypeOther = json['job_type_other'];
    relationshipTypeOther = json['relationship_type_other'];
    vaitroNcscChichamsocNkt = json['vaitro_ncsc_chichamsoc_nkt'];
    vaitroNcscLaodongTaothunhap = json['vaitro_ncsc_laodong_taothunhap'];
    vaitroNcscNguoicsCacthanhvien = json['vaitro_ncsc_nguoics_cacthanhvien'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    province = json['province'];
    district = json['district'];
    ward = json['ward'];
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fist_name'] = this.fistName;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['last_name'] = this.lastName;
    data['birth_date'] = this.birthDate;
    data['is_have_phone'] = this.isHavePhone;
    data['is_smartphone'] = this.isSmartphone;
    data['gender'] = this.gender;
    data['nation'] = this.nation;
    data['disability_types'] = this.disabilityTypes;
    data['is_have_internet'] = this.isHaveInternet;
    data['edu_level'] = this.eduLevel;
    data['marital_status'] = this.maritalStatus;
    data['job_type'] = this.jobType;
    data['relationship_type'] = this.relationshipType;
    data['birth_year'] = this.birthYear;
    data['member'] = this.member;
    data['BirthYear'] = this.birthYear;
    data['nation_other'] = this.nationOther;
    data['marital_status_other'] = this.maritalStatusOther;
    data['job_type_other'] = this.jobTypeOther;
    data['relationship_type_other'] = this.relationshipTypeOther;
    data['vaitro_ncsc_chichamsoc_nkt'] = this.vaitroNcscChichamsocNkt;
    data['vaitro_ncsc_laodong_taothunhap'] = this.vaitroNcscLaodongTaothunhap;
    data['vaitro_ncsc_nguoics_cacthanhvien'] =
        this.vaitroNcscNguoicsCacthanhvien;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['province'] = this.province;
    data['district'] = this.district;
    data['ward'] = this.ward;
    data['address'] = this.address;
    return data;
  }
}

class HbccItemKeHoachResponse {
  List<HbccItemKeHoach> items = [];
  int pages = 0;

  HbccItemKeHoachResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new HbccItemKeHoach.fromJson(item);
        items.add(it);
      });
    }
  }
}
