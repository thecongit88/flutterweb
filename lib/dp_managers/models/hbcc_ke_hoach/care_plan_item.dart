class CarePlanItem {
  int? id;
  String? title;
  String? itemDate;
  String? itemTime;
  Plan? plan;
  int? status;
  String? expectedResult;
  String? createdAt;
  String? updatedAt;
  CareProcessItem? careProcessItem;

  CarePlanItem(
      {this.id,
        this.title,
        this.itemDate,
        this.itemTime,
        this.plan,
        this.status,
        this.expectedResult,
        this.createdAt,
        this.updatedAt,
        this.careProcessItem});

  CarePlanItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    itemDate = json['item_date'];
    itemTime = json['item_time'];
    plan = json['plan'] != null ? new Plan.fromJson(json['plan']) : null;
    status = json['status'];
    expectedResult = json['expected_result'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    careProcessItem = json['care_process_item'] != null
        ? new CareProcessItem.fromJson(json['care_process_item'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['item_date'] = this.itemDate;
    data['item_time'] = this.itemTime;
    if (this.plan != null) {
      data['plan'] = this.plan!.toJson();
    }
    data['status'] = this.status;
    data['expected_result'] = this.expectedResult;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.careProcessItem != null) {
      data['care_process_item'] = this.careProcessItem!.toJson();
    }
    return data;
  }
}

class Plan {
  int? id;
  String? title;
  int? month;
  int? year;
  int? hbcc;
  int? member;
  int? status;
  int? province;
  int? district;
  int? ward;
  String? createdAt;
  String? updatedAt;
  String? code;
  int? ncs;

  Plan(
      {this.id,
        this.title,
        this.month,
        this.year,
        this.hbcc,
        this.member,
        this.status,
        this.province,
        this.district,
        this.ward,
        this.createdAt,
        this.updatedAt,
        this.code,
        this.ncs});

  Plan.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    month = json['month'];
    year = json['year'];
    hbcc = json['hbcc'];
    member = json['member'];
    status = json['status'];
    province = json['province'];
    district = json['district'];
    ward = json['ward'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    code = json['code'];
    ncs = json['ncs'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['month'] = this.month;
    data['year'] = this.year;
    data['hbcc'] = this.hbcc;
    data['member'] = this.member;
    data['status'] = this.status;
    data['province'] = this.province;
    data['district'] = this.district;
    data['ward'] = this.ward;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['code'] = this.code;
    data['ncs'] = this.ncs;
    return data;
  }
}

class CareProcessItem {
  int? id;
  int? carePlanItem;
  bool? proccesed;
  Null? resultValue;
  Null? resultText;
  int? carePlan;
  String? createdAt;
  String? updatedAt;
  String? processDate;
  String? processTime;

  CareProcessItem(
      {this.id,
        this.carePlanItem,
        this.proccesed,
        this.resultValue,
        this.resultText,
        this.carePlan,
        this.createdAt,
        this.updatedAt,
        this.processDate,
        this.processTime});

  CareProcessItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    carePlanItem = json['care_plan_item'];
    proccesed = json['proccesed'];
    resultValue = json['result_value'];
    resultText = json['result_text'];
    carePlan = json['care_plan'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    processDate = json['process_date'];
    processTime = json['process_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['care_plan_item'] = this.carePlanItem;
    data['proccesed'] = this.proccesed;
    data['result_value'] = this.resultValue;
    data['result_text'] = this.resultText;
    data['care_plan'] = this.carePlan;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['process_date'] = this.processDate;
    data['process_time'] = this.processTime;
    return data;
  }
}

class CarePlanItemResponse {
  List<CarePlanItem> items = [];
  int pages = 0;

  CarePlanItemResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new CarePlanItem.fromJson(item);
        items.add(it);
      });
    }
  }
}
