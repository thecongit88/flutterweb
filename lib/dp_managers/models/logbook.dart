class LogBook {
  LogBook({
    this.title = '',
    this.member = '',
    this.imagePath = '',
    this.createdDate = '',
    this.rating = 0.0,
    this.logBookType = 0,
  });

  String title;
  String member;
  String createdDate;
  double rating;
  String imagePath;
  int logBookType;

  static List<LogBook> logBookLastestList = <LogBook>[
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát tháng 02/2020',
      member: 'Nguyễn Vân Anh',
      createdDate: '28/02/2020',
      logBookType: 1,
      rating: 4.3,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 27/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '27/02/2020',
      logBookType: 0,
      rating: 4.6,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 26/02/2020',
      member: 'Hoàng Ân',
      createdDate: '26/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát ngày 25/02/2020',
      member: 'Nguyễn Long Vinh',
      createdDate: '25/02/2020',
      rating: 4.3,
      logBookType: 0,
    ),

  ];

  static List<LogBook> logbookList = <LogBook>[
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát tháng 02/2020',
      member: 'Nguyễn Long Vinh',
      createdDate: '28/02/2020',
      rating: 4.3,
      logBookType: 1,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 27/02/2020',
      member: 'Hoàng Ân',
      createdDate: '27/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 26/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '26/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát ngày 25/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '25/02/2020',
      rating: 4.3,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      member: 'Lê Văn Nam',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
  ];
}
