import 'package:flutter/material.dart';

class LogbookType {
  LogbookType({
    this.icon,
    this.title = "",
    this.id = 0,
  });

  IconData? icon;
  bool? isSelected;
  String? title = "";
  int? id;

  static final logBookDANHGIACHUNG = new LogbookType(icon: Icons.perm_identity,title: "Đánh giá chung NCS",id:1);
  static final logBookDANHGIAKYNANG = new LogbookType(icon: Icons.hotel,title: "Đánh giá Kỹ năng NCS chăm sóc NKT",id:2);
  static final logBookDANHGIAKHANANG = new LogbookType(icon: Icons.accessible_forward,title: "Đánh giá Khả năng thực hiện hoạt động",id:3);
  static final logBookDANHGIATIENBO = new LogbookType(icon: Icons.accessibility_new,title: "Đánh giá Tiến bộ lâm sàng",id:4);
  static final logBookDANHGIAMOITRUONG = new LogbookType(icon: Icons.store_mall_directory,title: "Đánh giá Môi trường",id:5);

}
