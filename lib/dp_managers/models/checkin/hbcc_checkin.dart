import '../../../models/files/images.dart';

class HbccCheckin {
  int? id;
  String? summary;
  String? result;
  String? hbccName;
  int? hbccId;
  String? memberName;
  int? memberId;
  String? provinceName;
  int? provinceId;
  String? districtName;
  int? districtId;
  String? wardName;
  int? wardId;
  String? createdAt;
  String? updatedAt;
  String? checkinTime;
  String? checkoutTime;
  String? checkinLatlong;
  String? checkoutLatlong;
  String? address;
  List<Images>? images;
  List<Images>? checkinImages;

  HbccCheckin(
      {this.id,
        this.summary,
        this.result,
        this.hbccName,
        this.hbccId,
        this.memberName,
        this.memberId,
        this.provinceName,
        this.provinceId,
        this.districtName,
        this.districtId,
        this.wardName,
        this.wardId,
        this.createdAt,
        this.updatedAt,
        this.checkinTime,
        this.checkoutTime,
        this.checkinLatlong,
        this.checkoutLatlong,
        this.images,
        this.checkinImages});

  HbccCheckin.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    summary = json['summary'];
    result = json['result'];
    hbccName = json['hbcc'] != null? json['hbcc']['full_name'] : "";
    hbccId =  json['hbcc'] != null? json['hbcc']['id'] : 0;
    memberName = json['member'] != null? json['member']['full_name'] : "";
    address = json['member'] != null? json['member']['address'] : "";
    memberId = json['member'] != null? json['member']['id'] : 0;
    provinceName = json['province'] != null? json['province']['name'] : "";
    provinceId = json['province'] != null? json['province']['id'] : 0;
    districtName = json['district'] != null? json['district']['name'] : "";
    districtId = json['district'] != null? json['district']['id'] : 0;
    wardName = json['ward'] != null? json['ward']['name'] : "";
    wardId = json['ward'] != null? json['ward']['id'] : 0;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    checkinTime = json['checkin_time'];
    checkoutTime = json['checkout_time'];
    checkinLatlong = json['checkin_latlong'];
    checkoutLatlong = json['checkout_latlong'];
    if (json['images'] != null) {
      images = <Images>[];
      json['images'].forEach((v) {
        images!.add(Images.fromJson(v));
      });
    }
    if (json['checkin_images'] != null) {
      checkinImages = <Images>[];
      json['checkin_images'].forEach((v) {
        checkinImages!.add(Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['summary'] = summary;
    data['result'] = result;
    data['hbcc'] = hbccId;
    data['member'] = memberId;
    data['province'] = provinceId;
    data['district'] = districtId;
    data['ward'] = wardId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['checkin_time'] = checkinTime;
    data['checkout_time'] = checkoutTime;
    data['checkin_latlong'] = checkinLatlong;
    data['checkout_latlong'] = checkoutLatlong;
    if (images != null) {
      data['images'] = images!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class HbccCheckinResponse {
  List<HbccCheckin> items = [];
  int pages = 0;

  HbccCheckinResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = HbccCheckin.fromJson(item);
        items.add(it);
      });
    }
  }
}
