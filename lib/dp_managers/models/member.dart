import 'dart:convert';

import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/images_util.dart';

class MemberResponse {
  List<Member> items = <Member>[];
  MemberResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Member.fromJson(item);
        items.add(it);
      });
    }
  }
}

class MemResponse {
  Member? mem;
  MemResponse.fromJson(Map<String, dynamic> json) {
    mem = Member.fromJson(json);
  }
}

class MemberPageResponse {
  List<Member> items = <Member>[];
  int pages = 0;

  MemberPageResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Member.fromJson(item);
        items.add(it);
      });
    }
  }
}

class Member {
  int? id;
  String? name;
  String? firstName;
  String? lastName;
  String? fullName;
  String? code;
  String? startJoin;
  String? gender;
  String? gender_other;
  int? genderId;
  String? nation;
  // ignore: non_constant_identifier_names
  String? nation_other;
  int? nationId;
  int? birth_year;
  String? phone;
  String? createdDate;
  String? address;
  String? avatar;
  String? ncs;
  String? disabilityType;
  String? disability_other;
  int? disabilityTypeId;
  int? disabilityStartId;
  String? disabilityStart;
  String? projectJoinDate;
  String? birth_date;
  int? disability_year;
  String? disability_date;
  String? disabilityDate;
  String? isAgentOrangeCertificates;
  int? idNguoiChamSoc, personal_assistant;
  bool? is_agent_orange;

  int? provinceId;
  String? provinceName;
  int? districtId;
  String? districtName;
  int? wardId;
  int? userId;
  int? hbccId;
  String? wardName;
  bool? approved;
  bool? isSuccess;

  String? large, thumbnail, small, medium;

  Member({
    this.id = 0,
    this.name = '',
    this.firstName = '',
    this.lastName = '',
    this.fullName="",
    this.code = '',
    this.startJoin = '',
    this.gender = '',
    this.gender_other = '',
    this.genderId = 0,
    this.userId = 0,
    this.nation = '',
    this.nation_other = '',
    this.birth_year = 0,
    this.phone = '',
    this.createdDate = null,
    this.address = '',
    this.avatar = '',
    this.ncs = '',
    this.disabilityType = '',
    this.disability_other = '',
    this.projectJoinDate = null,
    this.birth_date = '',
    this.disability_date = null,
    this.disabilityDate = null,
    this.idNguoiChamSoc = 0,
    this.personal_assistant = 0,
    this.is_agent_orange,
    this.nationId = 0,
    this. disabilityTypeId = 0,
    this.provinceId = 0,
    this.districtId = 0,
    this.wardId = 0,
    this.provinceName = '',
    this.districtName = '',
    this.wardName = '',
    this.approved,
    this.isSuccess,
    this.hbccId = 0
  });

  Member.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    userId = parsedJson["user"] != null ? User.fromJson(parsedJson["user"]).id : 0;
    name = parsedJson["first_name"] != null?
    "${parsedJson["first_name"].toString().trim()} ${parsedJson["last_name"].toString().trim()}" : "";
    firstName = parsedJson["first_name"] != null ? parsedJson["first_name"].toString().trim() : "";
    lastName = parsedJson["last_name"] != null ? parsedJson["last_name"].toString().trim() : "";
    fullName = parsedJson["full_name"] != null ? parsedJson["full_name"].toString().trim() :
         "${firstName} ${lastName}";
    code = parsedJson["member_code"] != null ? parsedJson["member_code"].toString().toUpperCase() : "";
    startJoin = parsedJson["startJoin"] ?? "";
    gender = parsedJson["gender"] != null ? Gender.fromJson(parsedJson["gender"]).getName() : "";
    genderId = parsedJson["gender"] != null ? Gender.fromJson(parsedJson["gender"]).getId() : 0;
    nation = parsedJson["nation"] != null ? Nation.fromJson(parsedJson["nation"]).geTitle() : "";
    nationId = parsedJson["nation"] != null ? Nation.fromJson(parsedJson["nation"]).getId() : 0;
    nation_other = parsedJson["nation_other"] ?? "";
    birth_year = parsedJson["birth_year"] ?? 0;
    phone = parsedJson["phone"] ?? "";
    createdDate = parsedJson["createdDate"] ?? "";
    address = parsedJson["address"] ?? "";
    avatar = parsedJson['avatar'] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']) : "";
    large = parsedJson['avatar'] != null && parsedJson['avatar']["formats"] != null
        && parsedJson['avatar']["formats"]["large"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']["formats"]["large"]) : "";
    small = parsedJson['avatar'] != null && parsedJson['avatar']["formats"] != null
        && parsedJson['avatar']["formats"]["small"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']["formats"]["small"]) : "";
    medium = parsedJson['avatar'] != null && parsedJson['avatar']["formats"] != null
        && parsedJson['avatar']["formats"]["medium"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']["formats"]["medium"]) : "";
    thumbnail = parsedJson['avatar'] != null && parsedJson['avatar']["formats"] != null
        && parsedJson['avatar']["formats"]["thumbnail"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']["formats"]["thumbnail"]) : "";
    ncs = parsedJson["personal_assistant"]!= null && parsedJson["personal_assistant"]["fist_name"] != null?
        parsedJson["personal_assistant"]["fist_name"].trim() +
            (parsedJson["personal_assistant"]["last_name"] != null?
            " " + parsedJson["personal_assistant"]["last_name"].trim():"")
    :"";
    disabilityType = parsedJson["disability_type"] != null ?
    DisabilityType.fromJson(parsedJson["disability_type"]).geTitle() : "";
    disabilityTypeId = parsedJson["disability_type"] != null ?
    DisabilityType.fromJson(parsedJson["disability_type"]).getId() : 0;

    var _disabilityStart = parsedJson["disability_start"];
    disabilityStart =  parsedJson["disability_start"] == null || isNumeric(_disabilityStart.toString()) == true ? ""
        : DisabilityStart.fromJson(_disabilityStart).geTitle();
    disabilityStartId = parsedJson["disability_start"] == null || isNumeric(_disabilityStart.toString()) == true ? _disabilityStart
        : DisabilityStart.fromJson(parsedJson["disability_start"]).getId();

    projectJoinDate = parsedJson["project_join_date"] ?? "";
    birth_date = parsedJson["birth_date"] ?? "";
    disability_date = parsedJson["disability_date"] ?? "";
    disability_year = parsedJson["disability_year"] ?? 0;
    disabilityDate = parsedJson["disability_date"] ?? "";
    idNguoiChamSoc = parsedJson["personal_assistant"] != null ? parsedJson["personal_assistant"]["id"] : 0;
    personal_assistant = parsedJson["personal_assistant"] != null ? parsedJson["personal_assistant"]["id"] : 0;
    is_agent_orange = parsedJson["is_agent_orange"] ?? false;
    disability_other = parsedJson["disability_other"] ?? '';
    hbccId = parsedJson["hbcc"] != null ? parsedJson["hbcc"]["id"] : 0;

    var _province = parsedJson["province"];
    provinceName = _province == null || isNumeric(_province.toString()) == true ? "" : Province.fromJson(_province).getName();
    provinceId = _province == null || isNumeric(_province.toString()) == true ? _province : Province.fromJson(_province).getId();

    var _district = parsedJson["district"];
    districtName = _district == null || isNumeric(_district.toString()) == true ? "" : District.fromJson(_district).getName();
    districtId = _district == null || isNumeric(_district.toString()) == true ? _district : District.fromJson(_district).getId();

    var _ward = parsedJson["ward"];
    wardName = _ward == null || isNumeric(_ward.toString()) == true ? "" : Ward.fromJson(_ward).getName();
    wardId = _ward == null || isNumeric(_ward.toString()) == true ? _ward : Ward.fromJson(_ward).getId();

    approved = parsedJson["approved"] ?? false;
    isSuccess = parsedJson["is_success"] ?? false;
    gender_other = parsedJson["gender_other"] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    //data['id'] = this.id;
    //data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['full_name'] = this.fullName;
    if(this.code != null || this.code != "")
      data['member_code'] = this.code;
    //data['startJoin'] = this.startJoin;
    data['gender'] = this.genderId;
    data['nation'] = this.nationId;
    data['nation_other'] = this.nation_other;
    data['province'] = this.provinceId;
    data['district'] = this.districtId;
    data['ward'] = this.wardId;
    data['user'] = this.userId;
    data['birth_year'] = this.birth_year;
    data['phone'] = this.phone;
    if(this.createdDate != null || this.createdDate != "")
      data['createdDate'] = this.createdDate;
    data['address'] = this.address;
    if(this.avatar != null || this.avatar != "")
      data['avatar'] = this.avatar;
    //data['ncs'] = this.ncs;
    data['disabilityType'] = this.disabilityTypeId;
    data['project_join_date'] = this.projectJoinDate;
    data['disability_date'] = this.disability_date;
    data['disability_year'] = this.disability_year;
    data['disability_start'] = this.disabilityStartId;

    data['birth_date'] = this.birth_date;
    //if(this.idNguoiChamSoc != null || this.idNguoiChamSoc != "")
      //data['personal_assistant'] = this.idNguoiChamSoc;
    data['is_agent_orange'] = this.is_agent_orange;
    data['disability_type'] = this.disabilityTypeId;
    data['approved'] = this.approved;
    data['is_success'] = this.isSuccess;

    data['personal_assistant'] = this.personal_assistant;
    data['gender_other'] = this.gender_other;
    data['disability_other'] = this.disability_other;
    return data;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  Member.fromMap(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = "${parsedJson["first_name"].toString().trim()} ${parsedJson["last_name"].toString().trim()}";
    code = parsedJson["member_code"].toString().toUpperCase();
    startJoin = parsedJson["startJoin"];
    gender = parsedJson["gender"] != null? parsedJson["gender"].toString():"";
    nation = parsedJson["nation"] != null? parsedJson["nation"].toString():"";
    birth_year = parsedJson["birth_year"] != null ? parsedJson["birth_year"] : 0;
    phone = parsedJson["phone"];
    createdDate = parsedJson["createdDate"];
    address = parsedJson["address"];
    avatar = parsedJson["avatar"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']): "";
    is_agent_orange = parsedJson["is_agent_orange"];
  }

  static Member member = new Member()
  ..id = 1
  ..name ='Nguyễn Văn Anh'
  ..phone = '0912123123'
  ..ncs = 'Hoàng Văn Bắc'
  ..address ="Xóm 8, Hưng Đông, Hưng Xuân, VN"
  ..gender = "Nam"
  ..birth_year = 0
  ..code = "XB01911991"
  ..projectJoinDate = "11/11/1991"
  ..disabilityDate = "1991"
  ..disabilityType = "Vận động"
  ..isAgentOrangeCertificates = "Không"
  ..nation = "Kinh"
  ..startJoin = "01/01/2019";

  static List<Member> memberList = <Member>[
    Member(
      id: 1,
      avatar: 'assets/design_course/interFace1.png',
      name: 'Nguyễn Văn Anh',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 2,
      avatar: 'assets/design_course/interFace2.png',
      name: 'Lê Thị Tám',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 3,
      avatar: 'assets/design_course/interFace1.png',
      name: 'Cao Văn Hoàng',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 4,
      avatar: 'assets/design_course/interFace2.png',
      name: 'Phan Văn Vĩnh',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 5,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Lê Thế Nam',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 6,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Vi Văn Hảo',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 7,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Nguyễn Hương Trà',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 8,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Nguyễn Cao Ánh',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 9,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Phan Như Hảo',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      id: 10,
      avatar: 'assets/design_course/interFace4.png',
      name: 'Trần Vân Anh',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
  ];

  static List<Member> popularMemberList = <Member>[
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Hoạt động của nhóm chuyên gia và các khoá đào tạo từ xa.',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace4.png',
      name: 'Một số phản hồi từ các thành viên tham gia dự án.',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
    Member(
      avatar: 'assets/design_course/interFace3.png',
      name: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      phone: '0912123123',
      ncs: 'Hoàng Văn Bắc',
    ),
  ];
}