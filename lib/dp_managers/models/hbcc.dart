import 'dart:convert';

import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/images_util.dart';

class HBCCResponse {
  List<HBCC> items = <HBCC>[];

  HBCCResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new HBCC.fromJson(item);
        items.add(it);
      });
    }
  }
}

class HBCCSingleResponse {
  HBCC? hbcc;
  HBCCSingleResponse.fromJson(Map<String, dynamic> json) {
    hbcc = HBCC.fromJson(json);
  }
}


class HBCC {
  int? id;
  String? firstName;
  String? lastName;
  String? fullName;
  String? phone;
  String? address;
  String? avatar;
  int? provinceId;
  String? provinceName;
  int? districtId;
  String? districtName;
  int? wardId;
  String? wardName;
  bool? approved;
  int? userId;

  HBCC({
    this.id = 0,
    this.firstName = '',
    this.lastName = '',
    this.fullName="",
    this.phone = '',
    this.address = '',
    this.avatar = '',
    this.wardId = 0,
    this.districtId = 0,
    this.provinceId = 0,
    this.approved,
    this.userId
  });

  HBCC.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    firstName = parsedJson["first_name"].toString().trim();
    lastName = parsedJson["last_name"].toString().trim();
    fullName = parsedJson["full_name"].toString().trim();
    phone = parsedJson["phone"];
    address = parsedJson["address"];
    avatar = parsedJson["avatar"] != null ? ImageUtil.getFullImagePath(parsedJson['avatar']): "";
    var _province = parsedJson["province"];
    provinceName = isNumeric(_province.toString()) == true ? "" : Province.fromJson(_province).getName();
    provinceId = isNumeric(_province.toString()) == true ? _province : Province.fromJson(_province).getId();

    var _district = parsedJson["district"];
    districtName = isNumeric(_district.toString()) == true ? "" : District.fromJson(_district).getName();
    districtId = isNumeric(_district.toString()) == true ? _district : District.fromJson(_district).getId();

    var _ward = parsedJson["ward"];
    wardName = isNumeric(_ward.toString()) == true ? "" : Ward.fromJson(_ward).getName();
    wardId = isNumeric(_ward.toString()) == true ? _ward : Ward.fromJson(_ward).getId();

    userId = parsedJson["user"] != null ? User.fromJson(parsedJson["user"]).id : 0;
    approved = parsedJson["approved"];
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['full_name'] = this.fullName;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['avatar'] = this.avatar;
    data['province'] = this.provinceId;
    data['district'] = this.districtId;
    data['ward'] = this.wardId;
    data['user'] = this.userId;
    data['approved'] = this.approved;
    return data;
  }
}