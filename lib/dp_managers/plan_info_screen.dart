import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';

class PlanInfoScreen extends StatefulWidget {
  @override
  _PlanInfoScreenState createState() => _PlanInfoScreenState();
}

class _PlanInfoScreenState extends State<PlanInfoScreen>
    with TickerProviderStateMixin {
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _status = false;
    String _value = "1";
    String _ncscValue ="1";
    final FocusNode myFocusNode = FocusNode();
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;
    return Container(
      color: ManagerAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius:
            BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: ManagerAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Kế hoạch chăm sóc"),
        ),
        backgroundColor: Colors.transparent,
        body:  SingleChildScrollView(
          child: Container(
                color: HexColor("#F3F4F9"),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 18.0, left: 18, right: 16),
                      child: Text(
                        'Hướng dẫn trẻ khuyết tật tự đi vệ sinh bằng bô.',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          letterSpacing: 0.27,
                          height: 1.25,
                          color: ManagerAppTheme.darkerText,
                        ),
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          color: ManagerAppTheme.nearlyWhite,
                          borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(8.0),
                          topRight: Radius.circular(8.0)),

                        ),
                      margin: const EdgeInsets.only(top: 10),
                      child: Column(
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Hoạt động chăm sóc:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                        decoration: const InputDecoration(
                                          hintText: "Nhập mô tả",
                                        )
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Tần suất dự kiến thực hiện:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: DropdownButton<String>(
                                      value: _value,
                                      items: [
                                        DropdownMenuItem<String>(
                                          child: Text('Hàng ngày'),
                                          value: '1',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('1-2 lần/tuần'),
                                          value: '2',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('3-4 lần/tuần'),
                                          value: '3',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('5-6 lần/tuần'),
                                          value: '4',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Khác'),
                                          value: '5',
                                        ),
                                      ],
                                      isExpanded: false,
                                      onChanged: (String? value) {
                                        setState(() {
                                          _value = value ?? "";
                                        });
                                      },
                                      hint: Text('Chọn tần suất'),
                                      underline: Container(
                                        decoration: const BoxDecoration(
                                            border: Border(bottom: BorderSide(color: Colors.grey))
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Người CSC phụ trách chăm sóc NKT:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: DropdownButton<String>(
                                      value: _ncscValue,
                                      items: [
                                        DropdownMenuItem<String>(
                                          child: Text('Chồng'),
                                          value: '1',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Vợ'),
                                          value: '2',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Ông'),
                                          value: '3',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Bà'),
                                          value: '4',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Con trai'),
                                          value: '5',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Con gái'),
                                          value: '6',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Con dâu'),
                                          value: '7',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Con rể'),
                                          value: '8',
                                        ),
                                        DropdownMenuItem<String>(
                                          child: Text('Khác (ghi rõ)'),
                                          value: '9',
                                        ),
                                      ],
                                      isExpanded: false,
                                      onChanged: (String? value) {
                                        setState(() {
                                          _ncscValue = value ?? "";
                                        });
                                      },
                                      hint: Text('Chọn NCSC'),
                                      underline: Container(
                                        decoration: const BoxDecoration(
                                            border: Border(bottom: BorderSide(color: Colors.grey))
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Thời gian dự kiến thực hiện:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: opacity1,
                            child: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Wrap(
                                children: <Widget>[
                                  getTimeBoxUI('T2'),
                                  getTimeBoxUI('T3'),
                                  getTimeBoxUI('T4'),
                                  getTimeBoxUI('T5'),
                                  getTimeBoxUI('T6'),
                                  getTimeBoxUI('T7'),
                                  getTimeBoxUI('CN'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Đầu ra mong muốn:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                        maxLines: 3,
                                        decoration: const InputDecoration(
                                          hintText: "Nhập đầu ra mong muốn",
                                        )
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Lưu ý khác (nếu có):',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                        maxLines: 3,
                                        decoration: const InputDecoration(
                                          hintText: "Nhập Lưu ý khác (nếu có)",
                                        )
                                    ),
                                  ),
                                ],
                              )),
                          AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: opacity3,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 16, bottom: 25, top:16, right: 16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      height: 48,
                                      decoration: BoxDecoration(
                                        color: ManagerAppTheme.nearlyBlue,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(16.0),
                                        ),
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: ManagerAppTheme
                                                  .nearlyBlue
                                                  .withOpacity(0.5),
                                              offset: const Offset(1.1, 1.1),
                                              blurRadius: 10.0),
                                        ],
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Lưu kế hoạch',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            letterSpacing: 0.0,
                                            color: ManagerAppTheme
                                                .nearlyWhite,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ),
          ],
                ),
              ),
        ),
      ),
    );
  }


  Widget getTimeBoxUI(String text1) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: ManagerAppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: ManagerAppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: ManagerAppTheme.nearlyBlue,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}