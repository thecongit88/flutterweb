import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/logbookType.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_month_screen.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_create_view.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_create_kncs_view.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_knth_screen.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_mt_screen.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_ncs_screen.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_tbls_screen.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/states/logbook_form_state.dart';
import 'package:provider/provider.dart';

class ManagerLogBookFormCreateScreen extends StatefulWidget {
  final FormType? formType;
  final Member? member;

  const ManagerLogBookFormCreateScreen({Key? key, this.formType, this.member}) : super(key: key);
  @override
  ManagerLogBookFormScreenState createState() => ManagerLogBookFormScreenState();
}

class ManagerLogBookFormScreenState extends State<ManagerLogBookFormCreateScreen>
    with TickerProviderStateMixin {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
      color: ManagerAppTheme.nearlyWhite,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          elevation: 0,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text(widget.formType!.formName ??""),
        ),
        backgroundColor: Colors.white,
        body:
    CustomScrollView(
    slivers: <Widget>[
    SliverList(
    delegate: SliverChildListDelegate([
    Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child:
        SingleChildScrollView(
          child:
              widget.formType!.formKynang! ?
              LogBookFormCreateKNCSView(member: widget.member,formType: widget.formType!)    :
              LogBookFormCreateView(member: widget.member,formType: widget.formType!),
        ),)]))])
      ),
    );
  }


}

