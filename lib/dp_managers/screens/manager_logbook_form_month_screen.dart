import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/main.dart';

class LogBookFormMonthScreen extends StatefulWidget {
  String? formFormatJson;
  LogBookFormMonthScreen({this.formFormatJson});
  @override
  _LogBookFormMonthScreenState createState() => _LogBookFormMonthScreenState();
}

class _LogBookFormMonthScreenState extends State<LogBookFormMonthScreen>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: const Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    String _date = "Chọn ngày";
    String _picked = "Không cải thiện - giữ nguyên như cũ";
    String _pickedVetloet = "Giữ nguyên như cũ";


    return Container(
      color: ManagerAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: ManagerAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: const Text("Đánh giá theo dõi cuối tháng"),
        ),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            color: HexColor("#F3F4F9"),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    decoration: const BoxDecoration(
                      color: ManagerAppTheme.nearlyWhite,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8.0),
                          topRight: Radius.circular(8.0)),
                    ),
                    margin: const EdgeInsets.only(top: 10),
                    child: Column(
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    const Text(
                                      'Thời gian thực hiện:',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 25.0, right: 25.0, top: 25.0),
                          child:
                          ElevatedButton(
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  theme: const DatePickerTheme(
                                    containerHeight: 210.0,
                                  ),
                                  showTitleActions: true,
                                  minTime: DateTime(2000, 1, 1),
                                  maxTime: DateTime(2022, 12, 31),
                                  onConfirm: (date) {
                                print('confirm $date');
                                _date =
                                    '${date.year} - ${date.month} - ${date.day}';
                                setState(() {
                                  _date = '${date.year} - ${date.month} - ${date.day}';
                                });
                              },
                                  currentTime: DateTime.now(), locale: LocaleType.vi);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 50.0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            const Icon(
                                              Icons.date_range,
                                              size: 18.0,
                                            ),
                                            Text(
                                              " $_date",
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.normal,
                                                  fontSize: 15.0),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  const Icon(Icons.arrow_drop_down)
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),

                        //content here


                        //end content

                        AnimatedOpacity(
                          duration: const Duration(milliseconds: 500),
                          opacity: opacity3,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 16, bottom: 25, top: 16, right: 16),
                            child:
                            InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      height: 48,
                                      decoration: BoxDecoration(
                                        color: ManagerAppTheme.nearlyBlue,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(16.0),
                                        ),
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: ManagerAppTheme
                                                  .nearlyBlue
                                                  .withOpacity(0.5),
                                              offset: const Offset(1.1, 1.1),
                                              blurRadius: 10.0),
                                        ],
                                      ),
                                      child: const Center(
                                        child: Text(
                                          'Hoàn thành',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 18,
                                            letterSpacing: 0.0,
                                            color:
                                            ManagerAppTheme.nearlyWhite,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              onTap: (){
                                finishForm();
                              },
                            )

                          ),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void finishFormBak(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  void finishForm(){
    var rng = Random();
    int randomNumber = rng.nextInt(2);
    if(randomNumber==0){
      return planSuccess();
    }else{
      return planChange();
    }
  }

  planSuccess(){
    return ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(backgroundColor: Colors.green,content: const Text('Bạn đã thực hiện thành công đánh giá!'),duration: const Duration(seconds: 2),action: SnackBarAction(
          label: 'Đóng', onPressed:ScaffoldMessenger.of(context).hideCurrentSnackBar,
        ),)
    );

  }

  planChange(){
    return ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(backgroundColor: Colors.redAccent,content: const Text('Đánh giá lỗi! Vui lòng thực hiện lại.'),duration: const Duration(seconds: 2),action: SnackBarAction(
          label: 'Đóng', onPressed:ScaffoldMessenger.of(context).hideCurrentSnackBar,
        ),)
    );

  }
}
