import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/dp_managers/models/support/support_message.dart';
import 'package:hmhapp/states/support/support_message_state.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common/app_colors.dart';
import '../../../services/api_response.dart';
import '../../../widgets/bottom_sheet_widget.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/error_message.dart';
import '../../../widgets/image_display_list_widget.dart';
import '../../../widgets/loading_message.dart';
import 'widget/hbcc_support_response_widget.dart';

class HbccSupportViewScreen extends StatefulWidget {
  final SupportConversation conversation;
  final bool? isShowLink;

  const HbccSupportViewScreen({Key? key,required this.conversation,this.isShowLink}) : super(key: key);

  @override
  _HbccSupportViewScreenState createState() => _HbccSupportViewScreenState();
}

class _HbccSupportViewScreenState extends State<HbccSupportViewScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  late final SupportConversation conversation;
  final _labelStyle = const TextStyle(fontSize: 15.0);

  var isShowLink = true;

  SupportMessageState supportMessageState  = SupportMessageState();

  @override
  void initState() {
    super.initState();
    conversation = widget.conversation;
    supportMessageState.listMessage(conversationId: conversation.id);
    isShowLink = widget.isShowLink == null || widget.isShowLink == true;

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => supportMessageState),
          ],
          child: Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.white,
              appBar: AppBar(
                elevation: 0,
                title: const Text("Chi tiết hỗ trợ"),
                backgroundColor: theme.primaryColor,
              ),
              body: SingleChildScrollView(
                    child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                              Container(
                              width: double.infinity,
                              color: Colors.white,
                              padding: const EdgeInsets.all(15.0),
                              child:
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      wdgRowItem(title: "Tiêu đề",
                                          value: "${conversation.title}"),
                                      wdgRowItem(title: "Người gửi",
                                          value: "${conversation.memberName}"),
                                      wdgRowItem(title: "Ngày gửi",
                                          value: "${DateFormat("dd/MM/yyyy HH:mm:ss")
                                              .format(DateTime.parse(conversation.createdAt.toString()))} "),
                                      wdgRowItemStatus(title: "Trạng thái",
                                          status: conversation.status ?? 0),
                                      wdgItem(title: "Nội dung",
                                          value: "${conversation.summary}"),
                                    ],
                                  )),
                            Container(color: AppColors.grey60, height: 8,),
                            Container(
                                width: double.infinity,
                                color: Colors.white,
                                padding: const EdgeInsets.all(15.0),
                              child:
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        "Lịch sử trao đổi",
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: theme.primaryColor
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ],
                                  ),
                                  AppConstant.spaceVerticalSmallMedium,
                                  Consumer<SupportMessageState>(
                                      builder: (context, state, child) {
                                        return wdgList(state.listMessageResponse)!;
                                      }
                                  ),
                                ],
                              )
                            )
                          ],
                        )
                    ),
            bottomNavigationBar:
            conversation.status == AppConstant.SUPPORT_HOANTHANH ?
            const SizedBox.shrink():
            Container(
              color: Colors.white,
              padding: const EdgeInsets.all(16),
              child:
              Row(
                children: [
                  Expanded(
                    child: ButtonWidget(
                      ic: Icons.chat,
                      bgColor: theme.primaryColor,
                      title: "Phản hồi",
                      onPress: () async {
                        await openResponse();
                      },
                    ),
                  )
                ],
              ),
            ),
          )
      );
  }


  Widget? wdgList(ApiResponse<List<SupportMessage>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgListRender(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget? wdgListRender(List<SupportMessage> list) {
    return list.isEmpty ?
    const SizedBox.shrink()
        :
     Column(
      children: [
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            separatorBuilder: (
                BuildContext context,
                int index) {
              return SizedBox(height: 16.sp,);
            },
            itemCount: list.length,
            itemBuilder: (BuildContext context,int index) {
              SupportMessage item = list[index];
              return
                Column(
                  children: [
                Container(
                    padding: EdgeInsets.all(1.sp),
                    decoration: BoxDecoration(
                      color: item.hbccName != "" ? AppColors.grey60 : AppColors.blue50,
                      borderRadius: const BorderRadius.all(Radius.circular(8))
                    ),
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(padding: const EdgeInsets.all(15),
                        child:
                        Text("${item.content}",style: TextStyle(fontSize: 13.sp,height: 1.5))),
                        Padding(padding: const EdgeInsets.only(left: 15,right: 15,bottom: 15),
                        child:
                        ImageDisplayListWidget(files: item.images ?? [])),
                        wdgLink(link: item.link),
                        Container(
                            padding: const EdgeInsets.all(16),
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(8),
                                bottomRight: Radius.circular(8))
                            ),
                            child:
                            Row(
                              children: [
                                Container(
                                  width: 24.sp,
                                  height: 24.sp,
                                  margin: EdgeInsets.only(right: 6.sp),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: item.hbccName != "" ? Colors.grey : ManagerAppTheme.nearlyBlue
                                  ),
                                  child:
                                  Text(item.hbccName != "" ? "M":"Y", textAlign: TextAlign.center,
                                      style: const TextStyle(color: Colors.white)),
                                ),
                                Text("${item.hbccName != ""? item.hbccName : (item.memberName != ""? item.memberName :"")}",
                                  style: const TextStyle(fontWeight: FontWeight.bold),),
                                const Spacer(),
                                Text("${DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(item.createdAt.toString()))} ")
                              ],
                            ))
                      ],
                    )),
                  ]);
            })
      ],
    );

  }

  wdgLink({String? link}) {
    final ThemeData theme = Theme.of(context);

    return link == null || link.isEmpty ?
    const SizedBox.shrink() :
    Container(
      margin: const EdgeInsets.only(bottom: 10,left: 15,right: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("Link:",
              style: TextStyle(fontSize: 13.sp,color: theme.primaryColor)),
          AppConstant.spaceVerticalSmallExtra,
          InkWell(
            child:
            Row(
              children: [
                Expanded(child:
                Text(link ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.normal,color: Colors.blue, fontSize: 13.sp),
                  maxLines: 2,))
              ],
            ),
            onTap: (){
              launchUrl(Uri.parse(link ?? ""),mode: LaunchMode.platformDefault);
            },
          ),
        ],
      ),
    );
  }

  wdgItem({String? title, String? value}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 13.sp)),
          AppConstant.spaceVerticalSmall,
          Text(value ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.normal, height: 1.5, fontSize: 13.sp))
        ],
      ),
    );
  }

  wdgRowItem({String? title, String? value}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 80,
            child:
            Text(title ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 13.sp))
          ),
          Text(value ?? "",
              style: TextStyle(
                  fontWeight: FontWeight.normal, fontSize: 13.sp))
        ],
      ),
    );
  }

  wdgRowItemStatus({String? title,int? status}) {
    var statusName  = "Chờ xem";
    var color = Colors.yellow;
    switch(status){
      case 1:
        statusName = "Chờ xem";
        color = Colors.amber;
        break;
      case 2:
        statusName = "Đã gửi";
        color = Colors.blue;
        break;
      case 3:
        statusName = "Đã trả lời";
        color = Colors.orange;
        break;
      case 4:
        statusName = "Hoàn thành";
        color = Colors.green;
        break;
      case 5:
        statusName = "Đã hủy";
        color = Colors.red;
        break;
    }
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              width: 80,
              child:
              Text(title ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 13.sp))
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 12),
            color: color,
            child:
            Text(statusName ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 12.sp,color: Colors.white))
          )
        ],
      ),
    );
  }

  openResponse() async {
    final ThemeData theme = Theme.of(context);

    var result = await openBottomSheet(
        context,
        title: "Phản hồi hỗ trợ",
        child: HbccSupportResponseWidget(conversation: conversation,isShowLink: isShowLink),
        color: theme.primaryColor,
    );
    if (result != null) {

      // ignore: use_build_context_synchronously
      Flushbar(
        message: 'Thêm phản hồi thành công.',
        backgroundColor: Colors.green,
        icon: const Icon(Icons.check_circle,color: Colors.white,),
        duration: const Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);

      supportMessageState.listMessage(conversationId: conversation.id);
    }
  }

}
