
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/dp_managers/models/support/support_message.dart';
import 'package:hmhapp/states/support/support_message_state.dart';

import '../../../../common/app_constant.dart';
import '../../../../common/app_global.dart';
import '../../../../common/text_theme_app.dart';
import '../../../../common_screens/camera/camera_state.dart';
import '../../../../models/files/file_fields_upload.dart';
import '../../../../states/support/support_conversation_state.dart';
import '../../../../widgets/button_widget.dart';
import '../../../../widgets/camera/camera_capture_widget.dart';
import '../../../manager_app_theme.dart';

class HbccSupportResponseWidget extends StatefulWidget {
  final SupportConversation? conversation;
  //Chỉ cho phép HBCC nhập link, Member không cho phép thêm link
  final bool? isShowLink;
  HbccSupportResponseWidget({this.conversation,this.isShowLink});

  @override
  State<StatefulWidget> createState() {
    return HbccSupportResponseWidgetState();
  }
}

class HbccSupportResponseWidgetState extends State<HbccSupportResponseWidget>
    with AutomaticKeepAliveClientMixin<HbccSupportResponseWidget> {
  String dateHint1 = "Chọn ngày", hintTuGio = "Chọn giờ";
  final contentController = TextEditingController();
  final ngayController = TextEditingController();
  final gioController = TextEditingController();
  final linkController = TextEditingController();

  var supportMessageState = SupportMessageState();
  var supportConversationState = SupportConversationState();
  final cameraState = CameraState();
  var isShowLink = true;

  @override
  void initState() {
    super.initState();
    isShowLink = widget.isShowLink == null || widget.isShowLink == true;
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    super.build(context);
    return Column(
      children: [
        TextThemeApp.wdgTitle("Nội dung", isRequired: true),
        TextFormField(
          controller: contentController,
          maxLines: 5,
          decoration:  InputDecoration(
              hintText: "Nhập chi tiết nội dung phản hồi",
              hintStyle: TextStyle(fontSize: 12.sp),
          ),
        ),
        AppConstant.spaceVerticalMediumExtra,
        isShowLink?
        TextThemeApp.wdgTitle("Liên kết", isRequired: false):
        const SizedBox.shrink(),
        isShowLink?
        TextFormField(
          controller: linkController,
          decoration:  InputDecoration(
            hintText: "Nhập liên kết nếu có",
            hintStyle: TextStyle(fontSize: 12.sp),
          ),
        ):
        const SizedBox.shrink(),
        AppConstant.spaceVerticalMediumExtra,
        CameraCaptureWidget(controllerCamera: cameraState),
        AppConstant.spaceVerticalMediumExtra,
        Row(
          children: [
            Expanded(
              child: ButtonWidget(
                bgColor: theme.primaryColor,
                ic: Icons.check_circle,
                title: "Gửi phản hồi",
                onPress: () {
                  save(widget.conversation,context); //status 1 - xuat ban
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  void save(SupportConversation? conversation,context) async {
    if(contentController.text.trim() == "") {
      showErrorToast(context, "Bạn chưa nhập nội dung.");
      return;
    }

    final item = SupportMessage();
    item.content = contentController.text.trim();
    item.link = linkController.text.trim();
    item.status = AppConstant.SUPPORT_DATRALOI;
    item.hbccId = conversation?.hbccId ?? 0;
    item.conversationId = conversation?.id ?? 0;
    item.messageDate = DateTime.now().toString();
    item.images = [];
    var res = await supportMessageState.addMessage(item);
    if (res != null) {
      if(cameraState.files.isNotEmpty) {
        var fields = FileFieldUpload()
          ..ref = "support-messages"
          ..refId = res.id
          ..field = "images";

        var fileResult = await cameraState.uploadImage(fields);
        if (fileResult == null) {
          supportMessageState.deleteMessage(res.id);
          Flushbar(
            message: 'Xảy ra lỗi trong quá trình upload hình ảnh.',
            backgroundColor: Colors.red,
            icon: const Icon(Icons.check_circle, color: Colors.white,),
            duration: const Duration(seconds: 5),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        } else {
          conversation?.status = AppConstant.SUPPORT_DATRALOI;
          await supportConversationState.updateConversation(
              conversation!, conversation!.id!);
          Navigator.pop(context, true);
        }
      }else{
        conversation?.status = AppConstant.SUPPORT_DATRALOI;
        await supportConversationState.updateConversation(
            conversation!, conversation!.id!);
        Navigator.pop(context, true);
      }
    } else {
      Flushbar(
        message: 'Xảy ra lỗi trong quá trình gửi câu trả lời.',
        backgroundColor: Colors.red,
        icon: const Icon(Icons.check_circle,color: Colors.white,),
        duration: const Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    }
  }
}
