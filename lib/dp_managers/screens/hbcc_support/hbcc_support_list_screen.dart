
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/states/support/support_conversation_state.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../common/app_constant.dart';
import '../../../services/api_response.dart';
import '../../../widgets/empty_data_widget.dart';
import '../../../widgets/error_message.dart';
import '../../../widgets/loading_message.dart';
import 'hbcc_support_view_screen.dart';

class HbccSupportListScreen extends StatefulWidget {
  final Member? member;
  final int? thang;
  final int? nam;

  const HbccSupportListScreen({Key? key, this.member, this.thang, this.nam})
      : super(key: key);

  @override
  HbccSupportListScreenState createState() => HbccSupportListScreenState();
}

class HbccSupportListScreenState extends State<HbccSupportListScreen>
    with TickerProviderStateMixin {

  var supportConversationState = SupportConversationState();

  TextStyle itemSubStyle = TextStyle(fontWeight: FontWeight.w400, fontSize: 13.sp);
  TextStyle itemStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 15.sp);

  @override
  void initState() {
    super.initState();
    supportConversationState.listConversation();
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
      color: Colors.white,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          elevation: 0,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: const Text("Yêu cầu hỗ trợ"),
        ),
        backgroundColor: Colors.white,
        body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => supportConversationState),
            ],
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Consumer<SupportConversationState>(
                    builder: (context, state, child) {
                      return wdgList(state.listConversationResponse)!;
                    },
                  ),
                ],
              ),
            )
        )
      ),
    );
  }

  List<DropdownMenuItem<int>> _dropDownItem() {
    List<int> ddl = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    return ddl
        .map((value) => DropdownMenuItem(
      value: value,
      child: Text("Tháng $value"),
    ))
        .toList();
  }


  Widget? wdgList(ApiResponse<List<SupportConversation>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgListRender(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget? wdgListRender(List<SupportConversation> list) {
    return list.isEmpty ?
    const EmptyDataWidget()
        :
    Container(
        padding: EdgeInsets.only(left: 16.sp, right: 16.sp, top: 0.sp, bottom: 0.sp),
        child:
    ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index){
          return Divider(height: 8.sp,);
        },
        physics: const NeverScrollableScrollPhysics(),
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          SupportConversation item = list[index];
          return
            Slidable(
                key: const ValueKey(0),
                endActionPane: ActionPane(
                  motion: const ScrollMotion(),
                  children: [

                  ],
                ),
                child:
                InkWell(
                  child:
                  Padding(
                      padding: EdgeInsets.only( bottom: 8.sp,top: 10.sp),
                      child:
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("${item.title}",
                                    style: itemStyle),
                                AppConstant.spaceVerticalSmallExtra,
                                Row(
                                  children: [
                                    const SizedBox(width: 90,
                                    child: Text("Ngày gửi:")),
                                    Text(item.createdAt != null ? DateFormat("dd/MM/yyyy HH:mm")
                                        .format(DateTime.parse(item.createdAt.toString())) : ""),
                                  ],
                                ),
                                AppConstant.spaceVerticalSmallExtra,
                                Row(
                                  children: [
                                    const SizedBox(width: 90,
                                        child: Text("Người gửi:")),
                                    Text(item.memberName != null ? "${item.memberName}" : ""),
                                  ],
                                ),
                                AppConstant.spaceVerticalSmallExtra,
                                wdgRowItemStatus(status: item.status),
                              ],
                            ),
                          ),
                        ],
                      )),
                  onTap: () async {
                    await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => HbccSupportViewScreen(
                        conversation: item,
                        isShowLink: true,
                      ),
                    ));
                  },
                )
            );
        }));
  }

  wdgRowItemStatus({int? status}) {
    var statusName = "Chờ xem";
    var color = Colors.yellow;
    switch (status) {
      case 1:
        statusName = "Chờ xem";
        color = Colors.amber;
        break;
      case 2:
        statusName = "Đã gửi";
        color = Colors.blue;
        break;
      case 3:
        statusName = "Đã trả lời";
        color = Colors.orange;
        break;
      case 4:
        statusName = "Hoàn thành";
        color = Colors.green;
        break;
      case 5:
        statusName = "Đã hủy";
        color = Colors.red;
        break;
    }
    return
      Row(
          children: [
            const SizedBox(width: 90,
            child:
            Text("Trạng thái:")),
            Icon(Icons.circle, size: 15, color: color,),
            AppConstant.spaceHorizontalSmall,
            Text(statusName ?? "",
                style: TextStyle(fontSize: 13.sp,))
          ]);
  }


}

