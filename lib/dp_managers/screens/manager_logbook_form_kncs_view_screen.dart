import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/widgets/form_skill_kncs_item.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/form_kncs_item.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/forms/form_type_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogBookFormKNCSViewScreen extends StatefulWidget {
  final FormType formType;
  final Member member;
  final int formSurveyId; // Id of record bang FormIte
  final int formId; //Id of form chi tiet
  final int memberId;

  const LogBookFormKNCSViewScreen({Key? key,required this.formType,required this.member,
    required this.formSurveyId,required this.formId,required this.memberId})
      : super(key: key);
  @override
  _LogBookFormKNCSViewScreenState createState() => _LogBookFormKNCSViewScreenState();
}

class _LogBookFormKNCSViewScreenState extends State<LogBookFormKNCSViewScreen>
    with TickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();

  late FormTypeState _formTypeState;
  late FormSurveyState _formSurveyState;
  late MemberServices _memberService;
  late PlansState _plansState;
  late FormItemLogBook _itemLogBook;

  late int _memberId;

  late String scores;

  dynamic response;
  late int hbccId;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _formDate;
  var formatStr = "yyyy-MM-dd";
  int _formUser = 0;
  String _dateLabel = "Chọn ngày";
  List<Color> colors = [
    Colors.green,
    Colors.blue,
    Colors.deepPurple,
    Colors.amber,
    Colors.red
  ];
  List<IconData> icons = [
    Icons.check_circle,
    Icons.info,
    Icons.adjust,
    Icons.remove_circle,
    Icons.cancel
  ];

  late List<DropdownMenuItem<int>> _listMembers;
  List<PlanCheck> _listPlan = <PlanCheck>[];
  late List<FormKNCSItem> _listFormKNCSItems;

  DropdownMenuItem<int> ddlMenuItemDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn NKT được đánh giá"),
  );

  late List<Widget> _listFormSkills;
  late List<PlanCheck> _listCheckedSkills;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _listMembers = <DropdownMenuItem<int>>[];
    _listMembers.add(ddlMenuItemDefault);

    _formDate = DateTime.now();
    _formUser = widget.member != null ? widget.member.id ?? 0 : 0;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
    _formTypeState = FormTypeState();
    _formSurveyState = new FormSurveyState();
    _plansState = new PlansState();

    if (_formUser > 0) {
      _plansState.getListPlanChecks(mId: _formUser);
    }

    _memberService = new MemberServices();
    super.initState();
    _listFormSkills = <Widget>[];
    _listCheckedSkills = <PlanCheck>[];
    scores = "0";
    _listFormKNCSItems = <FormKNCSItem>[];
    _itemLogBook = new FormItemLogBook();

    getSignedHbccId();
  }

  @override
  Widget build(BuildContext ctx) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => _formTypeState),
          ChangeNotifierProvider(create: (_) => _plansState),
          ChangeNotifierProvider(create: (_) => _formSurveyState)
        ],
        child: Padding(
          padding: const EdgeInsets.only(left: 9, right: 9, top: 15),
          child: new Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 0.0),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Đánh giá tại nhà:',
                    textAlign: TextAlign.left,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  ),
                  wdgNKT()
                ],
              ),
            ),
            wdgThoiGian(),
            Padding(
                padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                child: Text(
                  'TRONG THÁNG NÀY, Các hoạt động mà anh/chị áp dụng chăm sóc cho NKT là?:',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                )),
            Consumer<PlansState>(builder: (context, state, child) {
              switch (state.planChecksResponseList.status) {
                case Status.COMPLETED:
                  return wdgPlan(state.planChecksList);
                case Status.INIT:
                  return
                    Padding(
                      padding: EdgeInsets.only(top:10,left: 10,right: 10),
                      child:
                      Text("Vui lòng chọn Người khuyết tật tại mục Đánh giá tại nhà để tải kế hoạch chăm sóc.",
                      style: TextStyle(color:Colors.deepOrange),),
                    );
                  break;
                default:
                  return Container(
                    color: Colors.white30,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                  break;
              }
            }),
            Padding(
                padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _listFormSkills,
                )),
            _listFormSkills.length>0?
            GestureDetector(
              onTap: () {
                Map<String, dynamic> map = new Map<String, dynamic>();

                map["people_with_disability"] = _formUser;
                map["form_type"] = widget.formType;
                map["member"] = _formUser;
                map["form_date"] =
                    new DateFormat(formatStr).format(_formDate).toString();
                //map["form_kncs_items"] =  _listFormKNCSItem;

                _itemLogBook.name = widget.formType.formName;
                _itemLogBook.formDate =
                    new DateFormat(formatStr).format(_formDate).toString();
                _itemLogBook.memberId = _formUser;
                _itemLogBook.formTypeId = widget.formType.id;
                _itemLogBook.formType = widget.formType;
                _itemLogBook.member = new Member(id: _formUser);
                _itemLogBook.hbcc = new HBCC(id: hbccId);
                _formSurveyState.createFormKNCS(map, _itemLogBook,
                    widget.formType.formObject ?? "", _listFormKNCSItems);
              },
              child: Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Center(
                  child: Padding(
                      padding: const EdgeInsets.only(
                          left: 0, bottom: 25, top: 16, right: 0),
                      child: Expanded(
                        child: Container(
                          height: 48,
                          decoration: BoxDecoration(
                            color: ManagerAppTheme.nearlyBlue,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(16.0),
                            ),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ManagerAppTheme.nearlyBlue
                                      .withOpacity(0.5),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 10.0),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'Hoàn thành',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                letterSpacing: 0.0,
                                color: ManagerAppTheme.nearlyWhite,
                              ),
                            ),
                          ),
                        ),
                      )),
                ),
              ),
            )
            :
            Container(
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Center(
                child:
                Padding(
                    padding: const EdgeInsets.only(
                        left: 0, bottom: 25, top: 16, right: 0),
                    child: Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color: ManagerAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),
                    )),
              ),
            ),
            Consumer<FormSurveyState>(builder: (context, state, child) {
              switch (state.formKNCSResponse.status) {
                case Status.LOADING:
                  return Container(
                    color: Colors.white30,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                  break;
                case Status.COMPLETED:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    return planSuccess();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planSuccess();
                        }).then((shouldUpdate) {
                      Navigator.of(context).pop();
                    });
                  });
                  return SizedBox();
                  break;
                case Status.ERROR:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    return planChange();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planChange();
                        }).then((shouldUpdate) {
                      Navigator.of(context).pop();
                    });
                  });
                  return SizedBox(height: 0,);
                  break;
                case Status.INIT:
                  return SizedBox(height: 0,);
                  break;
                default:
                  return SizedBox(height: 0,);
                  break;
              }
            }),
          ]),
        ));
  }

  Widget wdgPlan(List<PlanCheck> plansList) {
    if(plansList.length>0) {
      return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          var plan = plansList[index];
          return CheckboxListTile(
            title: Text(
              plan.title ?? "",
              style: TextStyle(fontSize: 16),
            ),
            value: plan.isCheck,
            onChanged: (bool? value) {
              setState(() {
                plan.isCheck = (value == true);
              });
             var kncsItem = new FormKNCSItem()
                ..plan= new Plan(id: plan.id)
                ..scores = 0
                ..result ="Không thực hiện"
                ..formFormat = plan.formFormat;

              if (plan.isCheck == true) {
                _listCheckedSkills.add(plan);
                _listFormKNCSItems.add(kncsItem);

                var form = FormSkillKNCSItem(
                    key: ValueKey(plan.id.toString()),
                    plan: plan,
                    onChanged: (FormKNCSItem item) {
                      //if (_listFormKNCSItems.contains(item)) {
                      //  _listFormKNCSItems.remove(item);
                      //}
                      //_listFormKNCSItems.add(item);
                    });

                _listFormSkills.add(form);

              } else {
                var idx = _listCheckedSkills.indexOf(plan);
                _listCheckedSkills.remove(plan);

                var index = _listFormKNCSItems.indexWhere((element) => element.plan!.id == plan.id);
                _listFormKNCSItems.removeAt(index);

                _listFormSkills.removeWhere((element) => element.key == ValueKey(plan.id.toString()));
              }
            },
          );
        },
        itemCount: plansList.length ?? 0,
      );
    }else{
      return
        Padding(
          padding: EdgeInsets.only(top: 10, left: 10, right: 10),
          child:
          Text(
            "Kế hoạch chăm sóc Người khuyết tật này chưa được thiết lập. Vui lòng liên hệ với Người chăm sóc để thiết lập kế hoạch chăm sóc trước khi thực hiên.",
            style: TextStyle(color: Colors.deepOrange),),
        );
    }
  }

  Widget wdgNKT() {
    return FutureBuilder<List<Member>>(
      future: _memberService.listMembers(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return new Container();
        } else if (snapshot.hasData) {
          _listMembers.clear();
          _listMembers.add(ddlMenuItemDefault);
          snapshot.data!.forEach((_member) {
            _listMembers.add(new DropdownMenuItem<int>(
              value: _member.id,
              child: new Text(_member!.name ?? ""),
            ));
          });
          return DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            items: _listMembers,
            value: _formUser,
            onChanged: (int? value) {
              _listFormSkills.clear();
              setState(() {
                _formUser = value!;
              });
              _plansState.getListPlanChecks(mId: _formUser);
            },
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }

  Widget wdgThoiGian() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Thời gian thực hiện:',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                print('confirm $date');
                _formDate = date;
                setState(() {
                  _dateLabel = '${date.day}/${date.month}/${date.year}';
                });
              }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              '$_dateLabel',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  planSuccess() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã Cập nhật thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.check_circle,color: Colors.white),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Cập nhật đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.report,color: Colors.white,),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = (await preferences.getInt('hbccId'))!;
  }
}
