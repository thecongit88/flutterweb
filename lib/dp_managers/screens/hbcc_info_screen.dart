import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'dart:io';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/hbcc_state.dart';
import 'package:hmhapp/utils/camera_screen.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as pPath;

class HBCCInfoScreen extends StatefulWidget {
  @override
  _HBCCInfoScreenState createState() => _HBCCInfoScreenState();
}

class _HBCCInfoScreenState extends State<HBCCInfoScreen>
    with TickerProviderStateMixin {
  int _currentSelection = 0;
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  late File _imageFile;
  String _avatar = "";
  String imageAvatarUrl = "";

  HbccState _hbccState = new HbccState();

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    _avatar = "assets/design_course/userImage.png";
    setData();
    super.initState();
    _hbccState.getHbcc();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }


  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return ChangeNotifierProvider(
      create: (_) => _hbccState,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: theme.primaryColor,
            title: Text("Cá nhân"),
          ),
          backgroundColor: Colors.transparent,
          body: new Container(
            color: Colors.white,
            child: new ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    new Container(
                      height: 120.0,
                      color: Colors.white,
                      child: new Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 15.0),
                            child: Consumer<HbccState>(
                                builder: (context, state, child) {
                                  //_imageFile = File(state.getHbccAvatarUrl());
                                  imageAvatarUrl = state.getHbccAvatarUrl().toString().trim();
                                  return new Stack(fit: StackFit.loose, children: <Widget>[
                                    new Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Center(
                                          child:
                                          state.getIsCapture() == false || imageAvatarUrl.isEmpty
                                              ?
                                          Container(
                                            width: 80.0,
                                            height: 80.0,
                                            child:
                                            CircleAvatar(
                                                radius: 20,
                                                backgroundImage:
                                                AssetImage(_avatar)
                                            ),
                                          )
                                              : /* is local path */
                                          Container(
                                            width: 80.0,
                                            height: 80.0,
                                            child: CircleAvatar(
                                                radius: 20,
                                                backgroundImage: NetworkImage(imageAvatarUrl!)
                                            ),
                                          )
                                        )
                                      ],
                                    ),
                                    Padding(
                                        padding:
                                        EdgeInsets.only(top: 40.0, right: 50.0),
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            new CircleAvatar(
                                                backgroundColor: Colors.red,
                                                radius: 20.0,
                                                child: IconButton(
                                                  icon: Icon(Icons.camera_alt, size: 20.0, color: AppTheme.white),
                                                  tooltip: 'Chụp ảnh',
                                                  onPressed: () async {
                                                    var imagePathCapture = await Navigator.push(context, MaterialPageRoute(
                                                        builder: (context) => CameraScreen()),
                                                    );
                                                    if(imagePathCapture == null)
                                                      print("Bạn chưa chụp hình!");
                                                    else {
                                                      if(imagePathCapture != "") {
                                                        var path = join((await getTemporaryDirectory()).path,imagePathCapture);
                                                        _hbccState.setHbccAvatarUrl(path,imagePathCapture);
                                                        _hbccState.setIsCapture(true);
                                                      }
                                                    }
                                                    //setState(() {
                                                    //  imageAvatarPath = imagePathCapture;
                                                    //});
                                                    //print("My path: ");
                                                    //print(image_path_capture);
                                                  },
                                                )
                                            )
                                          ],
                                        )),
                                  ]);
                                }
                            )
                          )
                        ],
                      ),
                    ),
                    new Container(
                        child: wdgUserInfo()
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  Widget wdgUserInfo() {
    return Consumer<HbccState>(
        builder: (context, state, child) {
          return wdgUserInfoRender(state.hbccResponse);
        }
    );
  }

  Widget wdgUserInfoRender(ApiResponse<HBCC> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgUserInfoForm(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgUserInfoForm(HBCC hbcc) {
    return
      hbcc == null ?
      Center(
        child: Text("Thông tin HBCC chưa được cấu hình!"),
      )
          :
      Container(
      color: Color(0xffFFFFFF),
      child: Padding(
        padding: EdgeInsets.only(bottom: 25.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text(
                    'Thông tin HBCC',
                    style:
                    TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  new Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: Divider(
                        color: Colors.lightBlue,
                        height: 1,
                      )),
                ],
              ),
            ),

            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Họ và tên:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.fullName ?? "")
                    )
                  ],
                )),

            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Số điện thoại:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.phone ?? "")
                    )
                  ],
                )),

            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Địa chỉ:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.address ?? "")
                    )
                  ],
                )),

            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Xã/Phường:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.wardName ?? "")
                    )
                  ],
                )),

            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Quận/Huyện:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.districtName ?? "")
                    )
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Tỉnh/Tp:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(hbcc.provinceName ?? "")
                    )
                  ],
                )),


          ],
        ),
      ),
    );
  }
}
