
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/widgets/empty_data_widget.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../states/hbcc_state.dart';
import '../../../states/hbcc_member_ke_hoach_state.dart';
import '../../../widgets/error_message.dart';
import '../../../widgets/loading_message.dart';
import 'hbcc_member_add_ke_hoach_screen.dart';
import 'hbcc_member_update_ke_hoach.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HbccMemberListKeHoachScreen extends StatefulWidget {
  final Member? member;
  final int? thang;
  final int? nam;

  const HbccMemberListKeHoachScreen({Key? key, this.member, this.thang, this.nam}) : super(key: key);
  @override
  HbccMemberListKeHoachScreenState createState() => HbccMemberListKeHoachScreenState();
}

class HbccMemberListKeHoachScreenState extends State<HbccMemberListKeHoachScreen>
    with TickerProviderStateMixin {

  var hbccState = HbccState();
  var keHoachState = HbccMemberKeHoachState();
  int dayOfMonth = 0;

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal);

  @override
  void initState() {
    super.initState();
    keHoachState.listKeHoachHbccMember(memberId: widget.member?.id ?? 0);
    dayOfMonth = keHoachState.daysInMonth(widget.thang ?? DateTime.now().month, widget.nam ?? DateTime.now().year);
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
      color: ManagerAppTheme.nearlyWhite,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          elevation: 0,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: const Text("Danh sách kế hoạch"),
        ),
        backgroundColor: Colors.white,
        body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => hbccState),
              ChangeNotifierProvider(create: (_) => keHoachState)
            ],
            child: Consumer<HbccMemberKeHoachState>(
              builder: (context, state, child) {
                return SmartRefresher(
                  controller: keHoachState.refreshController,
                  enablePullDown: true,
                  enablePullUp: true,
                  onRefresh: () => keHoachState.listKeHoachHbccMember(memberId: widget.member?.id ?? 0),
                  onLoading: () => keHoachState.listKeHoachHbccMember(memberId: widget.member?.id ?? 0, isLoadMore: true),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 15, bottom: 15),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Text("#", style: headerStyle, textAlign: TextAlign.center,),
                              ),
                              Expanded(
                                flex: 7,
                                child: Text("Tiêu đề", style: headerStyle, textAlign: TextAlign.center,),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text("Tháng/Năm", style: headerStyle, textAlign: TextAlign.center),
                              ),
                            ],
                          ),
                        ),
                        wdgListKeHoach(state.listHbccMemberKeHoachResponse)!
                      ],
                    ),
                  ),
                );
              },
            ),

        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          onPressed: () async {
            var result = await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => HbccMemberAddKeHoachScreen(
                member: widget.member,
              ),
            ));
            if(result != null &&  result == 1) {
              await keHoachState.listKeHoachHbccMember(memberId: widget.member?.id ?? 0);
            }
          },
          child: const Icon(Icons.add, color: Colors.white),
        ),
      ),
    );
  }

  Widget? wdgListKeHoach(ApiResponse<List<HbccItemKeHoach>> data) {
    switch(data.status){
      case Status.LOADING:
        return const SizedBox(height: 0);
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgListKeHoachRender(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget? wdgListKeHoachRender(List<HbccItemKeHoach> list) {
    return /*list.isEmpty ?
        const EmptyDataWidget()
        :*/
    Expanded(
      child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          //separatorBuilder: (BuildContext context, int index) => Divider(height: 18.sp,),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            HbccItemKeHoach item = list[index];
            return InkWell(
              child: Container(
                color: index % 2 == 0 ? AppColors.grey60 : Colors.transparent,
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text("${index+1}", style: itemStyle, textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      flex: 7,
                      child: Text("${item.title}", style: itemStyle, textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text("${item.month}/${item.year}", style: itemStyle, textAlign: TextAlign.center),
                    ),
                  ],
                ),
              ),
              onTap: () async {
                var result = await Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => HbccMemberUpdateKeHoach(
                    itemKeHoach: item,
                  ),
                ));
                if(result != null &&  result == 1) {
                  await keHoachState.listKeHoachHbccMember(memberId: widget.member?.id ?? 0);
                }
              },
            );
          })
    );
  }

}

