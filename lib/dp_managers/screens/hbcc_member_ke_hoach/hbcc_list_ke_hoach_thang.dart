
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../states/hbcc_state.dart';
import '../../../states/hbcc_member_ke_hoach_state.dart';

class HbccListKeHoachThang extends StatefulWidget {
  final Member? member;
  final int? thang;
  final int? nam;

  const HbccListKeHoachThang({Key? key, this.member, this.thang, this.nam}) : super(key: key);
  @override
  HbccListKeHoachThangState createState() => HbccListKeHoachThangState();
}

class HbccListKeHoachThangState extends State<HbccListKeHoachThang>
    with TickerProviderStateMixin {

  var hbccState = HbccState();
  var themKeHoachState = HbccMemberKeHoachState();
  int dayOfMonth = 0;

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal);

  @override
  void initState() {
    super.initState();
    dayOfMonth = themKeHoachState.daysInMonth(widget.thang ?? DateTime.now().month, widget.nam ?? DateTime.now().year);
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
      color: ManagerAppTheme.nearlyWhite,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Kế hoạch tháng ${widget.thang}/${widget.nam}"),
        ),
        backgroundColor: Colors.white,
        body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => hbccState),
              ChangeNotifierProvider(create: (_) => themKeHoachState)
            ],
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    color: AppColors.grey300,
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text("Ngày", style: headerStyle, textAlign: TextAlign.center,),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text("Giờ", style: headerStyle, textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text("Hoạt động", style: headerStyle, textAlign: TextAlign.center),
                        ),
                        Expanded(
                          flex: 4,
                          child: Text("Kết quả", style: headerStyle, textAlign: TextAlign.center),
                        )
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(15),
                      child: ListView.separated(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          separatorBuilder: (BuildContext context, int index) => Divider(height: 18.sp,),
                          itemCount: dayOfMonth,
                          itemBuilder: (BuildContext context, int index) {
                            return Row(
                              children: [
                                Expanded(
                                  child: Text("${index+1}"),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Text("01:00"),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Text("Hoat dong ${index+1}"),
                                ),
                                Expanded(
                                  flex: 4,
                                  child: Text("Ket qua hoat dong ${index+1}"),
                                )
                              ],
                            );
                          })
                  )
                ],
              ),
            )
        ),
      ),
    );
  }

  List<DropdownMenuItem<int>> _dropDownItem() {
    List<int> ddl = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    return ddl
        .map((value) => DropdownMenuItem(
      value: value,
      child: Text("Tháng $value"),
    ))
        .toList();
  }

}

