import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/common/app_global.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc_ke_hoach/add_care_plan_item.dart';
import 'package:intl/intl.dart';

import '../../../app_theme.dart';
import '../../../common/app_colors.dart';
import '../../../common/app_constant.dart';
import '../../../common/text_theme_app.dart';
import '../../../states/hbcc_member_ke_hoach_state.dart';
import '../../../utils/utils.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/text_form_field_widget.dart';

class HbccMemberAddCarePlanItemScreen extends StatefulWidget {
  final AddCarePlanItem? carePlanItem;
  HbccMemberAddCarePlanItemScreen({this.carePlanItem});

  @override
  State<StatefulWidget> createState() {
    return HbccMemberAddCarePlanItemScreenState();
  }
}

class HbccMemberAddCarePlanItemScreenState extends State<HbccMemberAddCarePlanItemScreen>
    with AutomaticKeepAliveClientMixin<HbccMemberAddCarePlanItemScreen> {
  String dateHint1 = "Chọn ngày", hintTuGio = "Chọn giờ";
  final tieuDeController = TextEditingController();
  final ngayController = TextEditingController();
  final gioController = TextEditingController();
  final ketQuaHoatDongController = TextEditingController();
  var keHoachState = HbccMemberKeHoachState();

  @override
  void initState() {
    super.initState();
    tieuDeController.text = widget.carePlanItem?.title ?? "";
    if(!isNullOrEmpty(widget.carePlanItem?.itemDate?.toString())) {
      ngayController.text = DateTime.parse(widget.carePlanItem?.itemDate?.toString() ?? "").toString();
      dateHint1 = DateFormat("dd-MM-yyyy").format(DateTime.parse(ngayController.text)).toString();
    }

    if(!isNullOrEmpty(widget.carePlanItem?.itemTime)) {
      hintTuGio = getHourMinite(widget.carePlanItem?.itemTime ?? "");
    }

    ketQuaHoatDongController.text = widget.carePlanItem?.expectedResult ?? "";
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: [
        TextFormFieldWidget(
          label: "Nội dung",
          textEditingController: tieuDeController,
          isValidator: true,
          onChange: (keyword) {
          },
        ),
        AppConstant.spaceVerticalMediumExtra,
        Row(
          children: [
            Expanded(
                child: Column(
                  children: [
                    TextThemeApp.wdgTitle("Ngày", isRequired: true),
                    const SizedBox(height: 8,),
                    Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppTheme.borderTextField
                            ),
                            borderRadius: const BorderRadius.all(
                                Radius.circular(4.0)
                            )
                        ),
                        child: InkWell(
                          onTap: () async {
                            var selectedDate1 = await showDatePicker(
                                context: context,
                                locale: const Locale("vi", "VN"),
                                initialDate: DateTime.now(), //!MyFunctions.isNullOrEmpty(_tuNgayController.text) ? DateTime.parse(_tuNgayController.text) : DateTime.now(),
                                firstDate: DateTime(DateTime.now().year),
                                lastDate: DateTime(DateTime.now().year + 2),
                                helpText: "Chọn từ ngày", // Can be used as title
                                cancelText: 'Bỏ qua',
                                confirmText: 'Đồng ý',
                                builder: (context, child) {
                                  return Theme(
                                    data: Theme.of(context).copyWith(
                                      colorScheme: const ColorScheme.light(
                                        primary: ManagerAppTheme.nearlyBlue,
                                        onPrimary: Colors.white,
                                        onSurface: Colors.black,
                                      ),
                                      textButtonTheme: TextButtonThemeData(
                                        style: TextButton.styleFrom(
                                          foregroundColor: Colors.white,
                                          backgroundColor: ManagerAppTheme.nearlyBlue,
                                          textStyle: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    child: child!,
                                  );
                                }
                            );
                            if (selectedDate1 != null) {
                              setState(() {
                                ngayController.text = selectedDate1.toString();
                                dateHint1 = DateFormat("dd-MM-yyyy").format(selectedDate1).toString();
                              });
                            }
                          },
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                      dateHint1,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14,
                                      )
                                  )
                              ),
                              Expanded(
                                  flex: 0,
                                  child: Icon(
                                    Icons.date_range,
                                    size: 20.sp,
                                    color: Colors.grey,
                                  )
                              ),
                            ],
                          ),
                        )
                    )
                  ],
                )
            ),
            AppConstant.spaceHorizontalMedium,
            Expanded(
                child: Column(
                  children: [
                    TextThemeApp.wdgTitle("Giờ", isRequired: false),
                    const SizedBox(height: 8,),
                    Container(
                        width: double.infinity,
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppTheme.borderTextField
                            ),
                            borderRadius: const BorderRadius.all(
                                Radius.circular(4.0)
                            )
                        ),
                        child: InkWell(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: Text(hintTuGio, textAlign: TextAlign.left, style: const TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14,
                                )),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(Icons.access_time_outlined, color: Colors.grey, size: 20.sp),
                                ),
                              ),
                            ],
                          ),
                          onTap: () async {
                            final from = await showTimePicker(
                              helpText: "Chọn giờ",
                              context: context,
                              initialTime: hintTuGio != "" && hintTuGio != ":" && hintTuGio != "Chọn giờ"
                                  ?
                              TimeOfDay(hour: int.parse(hintTuGio.split(":")[0]), minute: int.parse(hintTuGio.split(":")[1]))
                                  :
                              TimeOfDay.now(),
                              builder: (context, child) {
                                return Theme(
                                  data: Theme.of(context).copyWith(
                                    colorScheme: const ColorScheme.light(
                                      primary: ManagerAppTheme.nearlyBlue,
                                      onPrimary: Colors.white,
                                      onSurface: Colors.black,
                                    ),
                                    textButtonTheme: TextButtonThemeData(
                                      style: TextButton.styleFrom(
                                        foregroundColor: Colors.white,
                                        backgroundColor: ManagerAppTheme.nearlyBlue,
                                        textStyle: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                  child: child ?? Container(),
                                );
                              },
                            );

                            if(from != null) {
                              setState(() {
                                String hour = from.hour < 10 ? "0${from.hour}" : "${from.hour}";
                                String minute = from.minute < 10 ? "0${from.minute}" : "${from.minute}";
                                hintTuGio = "$hour:$minute";
                                gioController.text = "$hintTuGio:00";
                              });
                            }
                          },
                        )
                    )
                    /*Container(
                      margin: EdgeInsets.only(left: 5),
                      child: TextFormFieldWidget(
                        label: "Giờ (hh:mm)",
                        textEditingController: gioController,
                        isValidator: false,
                        onChange: (keyword) {
                        },
                      ),
                    ),*/
                  ],
                )
            )
          ],
        ),
        AppConstant.spaceVerticalMediumExtra,
        TextFormFieldWidget(
          label: "Kết quả mong muốn",
          textEditingController: ketQuaHoatDongController,
          isValidator: false,
          onChange: (keyword) {
          },
        ),
        AppConstant.spaceVerticalMediumExtra,
        Row(
          children: [
            /*Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 5),
                  child: ButtonWidget(
                    bgColor: AppColors.orange,
                    ic: Icons.save_alt,
                    title: "Lưu nháp",
                    onPress: () async {
                      if (formKeyCarePlanItem.currentState == null) return;
                      if (formKeyCarePlanItem.currentState!.validate()) {
                        formKeyCarePlanItem.currentState!.save();
                        luuDuLieu(0); //status 0 - luu nhap
                      }
                    },
                  ),
                ),
              ),*/
            Expanded(
              child: ButtonWidget(
                bgColor: ManagerAppTheme.nearlyBlue,
                ic: Icons.check_circle,
                title: "Lưu chi tiết",
                onPress: () {
                  luuDuLieu(context, 1); //status 1 - xuat ban
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  void luuDuLieu(BuildContext context, int type) {
    if(tieuDeController.text.trim() == "") {
      showErrorToast(context, "Bạn chưa nhập nội dung.");
      return;
    } else if(ngayController.text.trim() == "") {
      showErrorToast(context, "Bạn chưa nhập ngày kế hoạch.");
      return;
    }

    final item = AddCarePlanItem();
    item.title = tieuDeController.text.trim();
    item.itemDate = ngayController.text.trim();
    item.itemTime = gioController.text.trim();
    item.expectedResult = ketQuaHoatDongController.text.trim();
    item.status = type;
    Navigator.of(context).pop(json.encode(item));
  }
}
