import 'package:hmhapp/common_screens/about_us_screen.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/custom_drawer/drawer_user_controller.dart';
import 'package:hmhapp/custom_drawer/home_drawer.dart';
import 'package:hmhapp/dp_managers/screens/manager_tab_screen.dart';
import 'package:hmhapp/common_screens/feedback_screen.dart';
import 'package:hmhapp/common_screens/help_screen.dart';
import 'package:hmhapp/common_screens/invite_friend_screen.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/widgets/manager_home_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManagerMainScreen extends StatefulWidget {
  const ManagerMainScreen({Key? key}) : super(key: key);

  @override
  ManagerMainScreenState createState() => ManagerMainScreenState();

}

class ManagerMainScreenState extends State<ManagerMainScreen> {
  late Widget screenView;
  late DrawerIndex drawerIndex;
  late AnimationController sliderAnimationController;

  Future<String> getLoginUser() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('accessToken') ?? "";
  }

  @override
  void initState() {
    super.initState();
    drawerIndex = DrawerIndex.HOME;
    screenView = const ManagerTabScreen();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Container(
      color: theme.primaryColor,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: Colors.white,
          body:
          DrawerUserController(
            screenIndex: drawerIndex,
            drawerWidth: MediaQuery.of(context).size.width * 0.55,
            animationController: (AnimationController animationController) {
              sliderAnimationController = animationController;
            },
            onDrawerCall: (DrawerIndex drawerIndex) {
              changeIndex(drawerIndex);
            },
            screenView: screenView,
          ),
        ),
      ),
    );
  }

  void changeIndex(DrawerIndex index) {
    if (drawerIndex != index) {
      drawerIndex = index;
      if (drawerIndex == DrawerIndex.HOME) {
        setState(() {
          screenView = const ManagerTabScreen();
        });
      } else if (drawerIndex == DrawerIndex.Help) {
        setState(() {
          screenView = HelpScreen();
        });
      } else if (drawerIndex == DrawerIndex.FeedBack) {
        setState(() {
          screenView = FeedbackScreen();
        });
      } else if (drawerIndex == DrawerIndex.Invite) {
        setState(() {
          screenView = InviteFriend();
        });
      } else if(drawerIndex == DrawerIndex.About){
        setState(() {
          screenView = AboutUsScreen();
        });
      }
    }
  }
}
