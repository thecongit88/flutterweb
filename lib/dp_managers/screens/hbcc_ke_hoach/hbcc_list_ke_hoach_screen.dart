
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_member_detail_screen.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../common/app_colors.dart';
import '../../../states/hbcc_ke_hoach_state.dart';
import '../../../states/hbcc_state.dart';
import '../../../widgets/empty_data_widget.dart';
import 'filter_hbcc_list_ke_hoach_screen.dart';
import 'hbcc_view_ke_hoach.dart';

class HbccListKeHoachScreen extends StatefulWidget {
  const HbccListKeHoachScreen({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _HbccListKeHoachScreenState createState() => _HbccListKeHoachScreenState();
}

class _HbccListKeHoachScreenState extends State<HbccListKeHoachScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;


  var hbccState = HbccState();
  var keHoachState = HbccKeHoachState();
  int dayOfMonth = 0;

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal);

  List<DropdownMenuItem<String>> _dsNganHang = [];
  final MemberState memberState = MemberState();
  DateTime? monthYearFilter;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
    keHoachState.listKeHoachHbcc();
    memberState.getListMembers();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            backgroundColor: ManagerAppTheme.nearlyBlue,
            elevation: 0,
            title: const Text("Ds kế hoạch HBCC"),
            actions: [
              InkWell(
                child: const SizedBox(
                  width: 50,
                  child: Icon(Icons.filter_list),
                ),
                onTap: () async {
                  var result = await Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => FilterHbccListKeHoachScreen(
                      monthFiltered: keHoachState.thangSelected,
                      yearFiltered: keHoachState.namSelected,
                      memberIdFiltered: keHoachState.memberIdSelected,
                    ),
                  ));
                  if(result != null) {
                    keHoachState.setThang(result["thang"]);
                    keHoachState.setNam(result["nam"]);
                    keHoachState.setMemberSelected(result["memberId"]);
                    await keHoachState.listKeHoachHbcc();
                  }
                },
              )
            ],
          ),
          backgroundColor: Colors.transparent,
          body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => hbccState),
              ChangeNotifierProvider(create: (_) => keHoachState),
            ],
            child: Consumer<HbccKeHoachState>(
              builder: (context, state, child) {
                return SmartRefresher(
                  controller: keHoachState.refreshController,
                  enablePullDown: true,
                  enablePullUp: true,
                  onRefresh: () => keHoachState.listKeHoachHbcc(),
                  onLoading: () => keHoachState.listKeHoachHbcc(isLoadMore: true),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(top: 15, bottom: 15),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Text("#", style: headerStyle, textAlign: TextAlign.center,),
                              ),
                              Expanded(
                                flex: 7,
                                child: Text("Tiêu đề", style: headerStyle, textAlign: TextAlign.center,),
                              ),
                              Expanded(
                                flex: 2,
                                child: Text("Tháng/Năm", style: headerStyle, textAlign: TextAlign.center),
                              ),
                            ],
                          ),
                        ),
                        wdgListKeHoach(state.listHbccKeHoachResponse)!
                      ],
                    ),
                  ),
                );
              },
            ),
        ),
      );
  }

  Widget wdgMainListNKT(ApiResponse<List<Member>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormNKT(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget wdgFormNKT(List<Member> listMembers) {
    final int count = listMembers.length;
    if (count == 0) {
      return const Center(
        child: Text("Chưa có dữ liệu!"),
      );
    }
    return ListView.separated(
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) =>
      const Divider(),
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        final Animation<double> animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: animationController,
                curve: Interval((1 / count) * index, 1.0,
                    curve: Curves.fastOutSlowIn)));
        animationController.forward();
        Member member =    listMembers[index];
        return ListTile(
          leading:
          Container(
              width: 60.0,
              height : 60.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      //image: AssetImage(member.avatar)
                      image:  member.avatar!.isEmpty ?
                      Image.asset("assets/images/avatar01.png", fit: BoxFit.fitWidth).image :
                      Image.network(member.avatar!, fit: BoxFit.fitWidth).image
                  )
              )),
          title: Padding(
              padding: const EdgeInsets.only(top: 7),
              child: Text(
                member.fullName ?? "",
                style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
          subtitle:
          Padding(padding:const EdgeInsets.only(top:5),
              child:
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text("NCS:",style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    letterSpacing: 0.27,
                    color: ManagerAppTheme.dark_grey,
                  ),),
                  Text(member.ncs ?? "",style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    letterSpacing: 0.27,
                    color: ManagerAppTheme.dark_grey,
                  ),)
                ],
              )),
          trailing: const Icon(Icons.arrow_forward_ios,size: 18,),
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => ManagerMemberDetailScreen(
                  member: member,
                ),
              ),
            );
          },
        );
      },
    );
  }

  Widget? wdgListKeHoach(ApiResponse<List<HbccItemKeHoach>> data) {
    switch(data.status){
      case Status.LOADING:
        return const SizedBox(height: 0);
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgListKeHoachRender(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget? wdgListKeHoachRender(List<HbccItemKeHoach> list) {
    return /*list.isEmpty ?
    const EmptyDataWidget()
        :*/
    Expanded(
      child: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          //separatorBuilder: (BuildContext context, int index) => Divider(height: 18.sp,),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            HbccItemKeHoach item = list[index];
            return InkWell(
              child: Container(
                color: index % 2 == 0 ? AppColors.grey60 : Colors.transparent,
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text("${index+1}", style: itemStyle, textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      flex: 7,
                      child: Text("${item.title}", style: itemStyle, textAlign: TextAlign.center,),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text("${item.month}/${item.year}", style: itemStyle, textAlign: TextAlign.center),
                    ),
                  ],
                ),
              ),
              onTap: () async {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => HbccViewKeHoach(
                    itemKeHoach: item,
                  ),
                ));
              },
            );
          }),
    );
  }

  Widget? wdgBanks(ApiResponse<List<Member>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message!,
        );
        break;
      case Status.COMPLETED:
        List<Member> list = data.data!;

        return SizedBox(
          height: 48,
          child: DropdownSearch<Member>(
            mode: Mode.MENU,
            maxHeight: 300,
            isFilteredOnline: true,
            //showClearButton: true,
            //showSelectedItems: true,
            showSearchBox: true,
            items: list,
            onFind: (String? filter) => getData(filter, list),
            dropdownSearchDecoration: const InputDecoration(
              //labelText: "Chọn ...",
              contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
              border: OutlineInputBorder(),
            ),
            onChanged: (Member? data) {
              //nganHangState.setNganHangSample(data!.bankCode);
            },
            //showSearchBox: true,
            dropdownBuilder: _customDropDownExample,
            popupItemBuilder: _customPopupItemBuilderExample2,
          ),
        );
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          //MyFunctions.showErrorFlushkBar(context, "Xảy ra lỗi trong quá trình tải dữ liệu.");
        });
        return const SizedBox(height: 0,);
        break;
      case Status.INIT:
        return const SizedBox(height: 0,);
        break;
    }
  }

  Future<List<Member>> getData(String? filter, List<Member> list) async {
    print("filter");
    print(filter);
    List<Member> tmp = list.where((element) => element.fullName!.toUpperCase().contains(filter!.toUpperCase()) ||
        element.phone!.toUpperCase().contains(filter.toUpperCase())).toList();
    return tmp;
  }

  Widget _customDropDownExample(BuildContext context, Member? item) {
    if (item == null) {
      return Container();
    }

    return Container(
      child: (item.code == "")
          ? const ListTile(
        contentPadding: EdgeInsets.all(0),
        title: Text("Không tìm thấy."),
      )
          : ListTile(
        contentPadding: const EdgeInsets.all(0),
        title: Text("${item.fullName} - ${item.phone}"),
      ),
    );
  }

  Widget _customPopupItemBuilderExample2(
      BuildContext context, Member? item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child: ListTile(
        selected: isSelected,
        title: Text("${item!.fullName}"),
      ),
    );
  }

}