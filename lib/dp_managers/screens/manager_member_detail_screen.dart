import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:format/format.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_choose_logbook_widget.dart';
import 'package:hmhapp/utils/calls_and_messages_service.dart';
import 'package:hmhapp/utils/service_locator.dart';
import 'package:intl/intl.dart';
import '../../common/app_colors.dart';
import '../../widgets/button_widget.dart';
import 'hbcc_member_ke_hoach/hbcc_member_add_ke_hoach_screen.dart';
import 'hbcc_member_ke_hoach/hbcc_member_list_ke_hoach_screen.dart';

class ManagerMemberDetailScreen extends StatefulWidget {

  final Member member;

  const ManagerMemberDetailScreen({Key? key, required this.member}) : super(key: key);

  @override
  MemberDetailScreenState createState() => MemberDetailScreenState();
}

class MemberDetailScreenState extends State<ManagerMemberDetailScreen>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();


  final double infoHeight = 460.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        5.0; /* 24.0 */
    return Container(
      color: ManagerAppTheme.nearlyWhite,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: widget.member.avatar!.isEmpty ? 1.0 : 1.1,
                  child:
                  widget.member.avatar!.isEmpty ?
                  Image.asset('assets/design_course/webInterFace.png', fit: BoxFit.cover)
                  :
                  Image.network(widget.member.medium!, fit: BoxFit.cover),
                ),
              ],
            ),
            Positioned(
              top: (MediaQuery.of(context).size.width / 1.8) - 24.0,
              bottom: 0,
              left: 0,
              right: 0,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                    color: ManagerAppTheme.nearlyWhite,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(32.0),
                        topRight: Radius.circular(32.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: ManagerAppTheme.grey.withOpacity(0.2),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child:
                  Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8,bottom: 60),
                    child: SingleChildScrollView(
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 32.0, left: 18, right: 16),
                                    child: Text(
                                      "${widget.member!.fullName ?? ""}",
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 22,
                                        letterSpacing: 0.27,
                                        color: ManagerAppTheme.darkerText,
                                      ),
                                    ),
                                  ),
                                  (widget.member.phone != null && widget.member.phone != "") ?
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16, right: 16, bottom: 8, top: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        InkWell(
                                          onTap: (){
                                            _service.call(widget.member.phone ?? "");
                                          },
                                          child:
                                          Text(
                                            widget.member.phone ?? "",
                                            textAlign: TextAlign.left,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 18.0,
                                              letterSpacing: 0.27,
                                              color: ManagerAppTheme.nearlyBlue,
                                            ),
                                          ),

                                        )
                                      ],
                                    ),
                                  ) : const SizedBox(height: 15.0),
                                ],
                              ),
                              const Spacer(),
                              (widget.member.phone != null && widget.member.phone != "") ?
                              Row(
                                children: <Widget>[
                                  Card(
                                    color: ManagerAppTheme.nearlyBlue,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(50.0)),
                                    elevation: 10.0,
                                    child: SizedBox(
                                      width: 60,
                                      height: 60,
                                      child: Center(
                                          child:
                                          InkWell(
                                            onTap: (){
                                              _service.call(widget.member.phone ?? "");
                                            },
                                            child:
                                            const Icon(
                                              Icons.phone,
                                              color: ManagerAppTheme.nearlyWhite,
                                              size: 30,
                                            ),
                                          )
                                      ),
                                    ),
                                  ),
                                ],
                              ): const SizedBox(height: 15.0)
                            ],
                          ),
                          Container(
                              constraints: BoxConstraints(
                                  minHeight: infoHeight,
                                  maxHeight: tempHeight > infoHeight
                                      ? tempHeight
                                      : infoHeight),
                            child: AnimatedOpacity(
                              duration: const Duration(milliseconds: 500),
                              opacity: opacity2,
                              child: AnimatedOpacity(
                                duration: const Duration(milliseconds: 500),
                                opacity: opacity1,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 18),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      /*const SizedBox(height: 10,),
                                      InkWell(
                                        child: Text("Thêm kế hoạch"),
                                        onTap: () {
                                          //print('KH{:07d}'.format(12));

                                          Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => HbccMemberAddKeHoachScreen(
                                              member: widget.member,
                                            ),
                                          ));
                                        },
                                      ),
                                      const SizedBox(height: 10,),
                                      InkWell(
                                        child: Text("Ds kế hoạch"),
                                        onTap: () {
                                          //print('KH{:07d}'.format(12));
                                          Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => HbccMemberListKeHoachScreen(
                                              member: widget.member,
                                            ),
                                          ));
                                        },
                                      ),*/
                                      const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Người chăm sóc: "),
                                          const Spacer(),
                                          Text(widget.member.ncs ?? "",style: const TextStyle(fontWeight: FontWeight.w600),),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Mã: "),
                                          const Spacer(),
                                          Text(widget.member.code ?? ""),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          const Text("Địa chỉ: "),
                                          Expanded(child:
                                          Padding(padding: const EdgeInsets.only(left: 10),
                                          child:
                                          Text(widget.member.address ?? "-",textAlign: TextAlign.right,))),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          const Text("Phường xã: "),
                                          const Spacer(),
                                          Text(widget.member.wardName ?? "-",textAlign: TextAlign.right,),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          const Text("Quận huyện: "),
                                          const Spacer(),
                                          Text(widget.member.districtName ?? "-",textAlign: TextAlign.right,),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          const Text("Tỉnh/TP: "),
                                          const Spacer(),
                                          Text(widget.member.provinceName ?? "-",textAlign: TextAlign.right,),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Giới tính: "),
                                          const Spacer(),
                                          Text(widget.member.gender ?? ""),
                                        ],
                                      ),const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Dân tộc: "),
                                          const Spacer(),
                                          Text(widget.member.nation ?? ""),
                                        ],
                                      ),
                                      const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Năm sinh"),
                                          const Spacer(),
                                          Text("${widget.member.birth_year ?? ""}"),
                                        ],
                                      ),
                                      const SizedBox(height: 10,),

                                      Row(
                                        children: [
                                          const Text("Loại khuyết tật"),
                                          const Spacer(),
                                          Text(widget.member.disabilityType ?? "-"),
                                        ],
                                      ),
                                      const SizedBox(height: 10,),

                                      Row(
                                        children: [
                                          const Text("Thời điểm băt đầu"),
                                          const Spacer(),
                                          Text(widget.member.disabilityDate != ""?
                                              DateFormat("dd-MM-yyyy").format(DateTime.parse(widget.member.disabilityDate.toString()))
                                                  : "---"),
                                        ],
                                      ),
                                      const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Tuổi"),
                                          const Spacer(),
                                          Text("${
                                              widget.member.birth_year != null && widget.member.birth_year! > 0?
                                              (DateTime.now().year - widget.member.birth_year! ?? ""):"---"
                                          }")
                                        ],
                                      ),
                                      const SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          const Text("Tham gia dự án từ:"),
                                          const Spacer(),
                                          Text(widget.member.projectJoinDate != ""?
                                              DateFormat("dd-MM-yyyy").format(DateTime.parse(widget.member.projectJoinDate.toString()))
                                                  : "---")
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).padding.bottom,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AnimatedOpacity(
                duration: const Duration(milliseconds: 500),
                opacity: opacity3,
                child:
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.all(16),
                    child: Column(
                    children: [
                        ButtonWidget(
                            bgColor: AppColors.orange,
                            title: "Kế hoạch chăm sóc",
                            onPress: () async {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => HbccMemberListKeHoachScreen(
                                  member: widget.member,
                                ),
                              ));
                            },
                          ),
                        const SizedBox(height: 16,),
                        ButtonWidget(
                          bgColor: ManagerAppTheme.nearlyBlue,
                          title: "Thực hiện đánh giá",
                          onPress: () async {
                            _settingModalBottomSheet(context,widget.member);
                          },
                        ),
                     /* Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:
                              InkWell(
                                child:
                                Container(
                                  height: 48,
                                  decoration: BoxDecoration(
                                    color: ManagerAppTheme.nearlyBlue,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(16.0),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: ManagerAppTheme
                                              .nearlyBlue
                                              .withOpacity(0.5),
                                          offset: const Offset(1.1, 1.1),
                                          blurRadius: 10.0),
                                    ],
                                  ),
                                  child: const Center(
                                    child: Text(
                                      'Thực hiện Đánh giá',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18,
                                        letterSpacing: 0.0,
                                        color: ManagerAppTheme
                                            .nearlyWhite,
                                      ),
                                    ),
                                  ),
                                ),
                                onTap: (){
                                  _settingModalBottomSheet(context,widget.member);
                                },
                              )
                          )
                        ],
                      ),*/
                    ],
                  ))
              ),
            ),
        Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: SizedBox(
                width: AppBar().preferredSize.height,
                height: AppBar().preferredSize.height,
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius:
                        BorderRadius.circular(AppBar().preferredSize.height),
                    child: const Icon(
                      Icons.arrow_back_ios,
                      color: ManagerAppTheme.nearlyBlue,
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _settingModalBottomSheet(context,member){
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc){
          return ManagerChooseLogbookWidget(member: member,);
        }
    );

    /*Get.bottomSheet(
        //ManagerChooseLogbookWidget(member: member,)
    );*/
  }
}
