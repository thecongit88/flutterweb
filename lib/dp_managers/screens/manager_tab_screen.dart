import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/bottom_navigation_view/bottom_bar_view.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/models/tabIcon.dart';
import 'package:hmhapp/dp_managers/widgets/manager_home_view.dart';
import 'package:hmhapp/dp_managers/widgets/manager_logbooks_view.dart';
import 'package:hmhapp/dp_managers/widgets/manager_member_view.dart';
import 'package:hmhapp/dp_managers/widgets/manager_report_view.dart';
import 'package:hmhapp/states/bottom_tab_state.dart';
import 'package:provider/provider.dart';

import 'hbcc_ke_hoach/hbcc_list_ke_hoach_screen.dart';
import 'hbcc_member_ke_hoach/hbcc_list_ke_hoach_thang.dart';

class ManagerTabScreen extends StatefulWidget {
  final int screenIndex;

  const ManagerTabScreen({Key? key, this.screenIndex=0}) : super(key: key);
  @override
  _ManagerTabScreenState createState() => _ManagerTabScreenState();
}

class _ManagerTabScreenState extends State<ManagerTabScreen> with TickerProviderStateMixin  {
  CategoryType categoryType = CategoryType.ui;
  late AnimationController animationController;

  List<TabIcon> tabIconsList = TabIcon.tabIconsManagerList;
  late List<Widget> _screens;

  Widget tabBody = Container(
    color: ManagerAppTheme.grey,
  );

  @override
  void initState() {
    for (var tab in tabIconsList) {
      tab.isSelected = false;
    }
    tabIconsList[widget.screenIndex].isSelected = true;

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    super.initState();

    _screens = <Widget>[];
    _screens.add(ManagerHomeView());
    _screens.add(const ManagerMemberView());
    _screens.add(const ManagerLogBooksView());
    _screens.add(const HbccListKeHoachScreen());
    _screens.add(const ManagerReportView());
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      ChangeNotifierProvider(
        create: (_) => BottomTabState(),
    child:
      Container(
      color: ManagerAppTheme.nearlyWhite,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body:
        Consumer<BottomTabState>(
            builder: (context, state, child) {
              return Stack(
                children: <Widget>[
                  _screens[state.screenIndex],
                  bottomBar(state),
                ],
              );
            }),
      ),
    ));
  }



  Widget bottomBar(BottomTabState state) {
    return Column(
      children: <Widget>[
        const Expanded(
          child: SizedBox(),
        ),
        BottomBarView(
          selectIndex: state.screenIndex,
          tabIconsList: tabIconsList,
          addClick: () {},
          changeIndex: (int index) {
            if (!mounted) {
              return;
            }
            state.setScreenIndex(index);
          },
        ),
      ],
    );
  }
}

