import 'dart:io';
import 'package:another_flushbar/flushbar.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../common/text_theme_app.dart';
import '../../../common_screens/camera/camera_state.dart';
import '../../../models/files/file_fields_upload.dart';
import '../../../services/api_response.dart';
import '../../../states/checkin/hbcc_checkin_state.dart';
import '../../../states/member_state.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/camera/camera_capture_single_widget.dart';
import '../../models/member.dart';

class HbccThemCheckinScreen extends StatefulWidget {

  const HbccThemCheckinScreen({Key? key}) : super(key: key);

  @override
  _HbccThemCheckinScreenState createState() => _HbccThemCheckinScreenState();
}

class _HbccThemCheckinScreenState extends State<HbccThemCheckinScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  HbccCheckinState checkinState  = HbccCheckinState();
  final cameraState = CameraState();
  final MemberState memberState = MemberState();
  Member? member;

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal, fontSize: 13);

  void checkPermission() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled) {
      // Use location.
    }
  }

  @override
  void initState() {
    super.initState();
    checkPermission();
    memberState.getListMembers();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => checkinState),
            ChangeNotifierProvider(create: (_) => memberState),
          ],
          child: Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                elevation: 0,
                title: const Text("Checkin"),
                backgroundColor: ManagerAppTheme.nearlyBlue,
              ),
              body:
              SingleChildScrollView(
                child: Container(
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            ManagerAppTheme.nearlyBlue,
                            Colors.white,
                          ],
                          stops: [
                            0.1,
                            0.65,
                          ],
                        )
                    ),
                    padding: const EdgeInsets.all(15.0),
                    child:
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppConstant.spaceVerticalSmallExtraExtraExtra,
                          const Center(
                            child: Icon(Icons.safety_check,size: 120,color: Colors.white),
                          ),
                          AppConstant.spaceVerticalSmallExtraExtraExtra,
                          Container(
                              padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 20)  ,
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15),
                                ),
                              ),
                              child:Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TextThemeApp.wdgTitle("Chọn NKT", isRequired: true),
                                  AppConstant.spaceVerticalSmallExtra,
                                  Consumer<MemberState>(
                                      builder: (context, state, child) {
                                        return wdgMembersList(state.memberResponseList, context)!;
                                      }
                                  ),
                                  AppConstant.spaceVerticalMediumExtra,
                                  CameraCaptureSingleWidget(controllerCamera: cameraState)
                                ],
                              )
                          )
                        ]
                    )
                ),
              ),
            bottomNavigationBar: Container(
              color: Colors.white,
              padding: const EdgeInsets.all(16),
              child:
              Row(
                children: [
                  Expanded(
                    child: ButtonWidget(
                      ic: Icons.maps_ugc_rounded,
                      bgColor: ManagerAppTheme.nearlyBlue,
                      title: "Checkin",
                      onPress: () async {
                          await save(context);
                      },
                    ),
                  )
                ],
              ),
            ),
          )
      );
  }

  Widget? wdgMembersList(ApiResponse<List<Member>> data, BuildContext context) {
    final ThemeData theme = Theme.of(context);

    if (data.status == Status.COMPLETED) {
      List<Member> list = data.data!;
      return SizedBox(
        height: 48,
        child: DropdownSearch<Member>(
          mode: Mode.BOTTOM_SHEET,
          isFilteredOnline: true,
          //showClearButton: true,
          //showSelectedItems: true,
          showSearchBox: true,
          items: list,
          onFind: (String? filter) => getData(filter, list),
          dropdownSearchDecoration: const InputDecoration(
            hintText: "Chọn người khuyết tật..",
          contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
            border: OutlineInputBorder(),
          ),
          onChanged: (Member? data) {
            member = data;
          },
          dropdownBuilder: _customDropDownExample,
          popupItemBuilder: _customPopupItemBuilderExample2,
          popupTitle:
          Padding(
            padding: const EdgeInsets.all(16),
            child:
            Text("Chọn Người khuyết tật",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,
            color: theme.primaryColor),),
          ),
        ),
      );
    }else{
        return const SizedBox(height: 0,);
    }
  }


  Widget _customDropDownExample(BuildContext context, Member? item) {
    if (item == null) {
      return Container();
    }
    String name = item.fullName ?? "";
    if(item.phone?.trim() != "") {
      name = "$name - ${item.phone}";
    }
    return Container(
      child: (item.id == 0)
          ? ListTile(
        contentPadding: const EdgeInsets.all(0),
        title: Text("Chọn người khuyết tật", style: itemStyle,),
      )
          : ListTile(
        contentPadding: const EdgeInsets.all(0),
        title: Text("${item.fullName}", style: itemStyle),
      ),
    );
  }

  Widget _customPopupItemBuilderExample2(
      BuildContext context, Member? item, bool isSelected) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16,vertical: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child:
      Row(
        children: [
          Expanded(
            flex: 3,
            child: Text("${item?.fullName}", style: itemStyle.copyWith(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
          ),
          const Spacer(),
          Expanded(
            flex: 2,
            child: Text("${item?.phone}", style: itemStyle, textAlign: TextAlign.right,),
          )
        ],
      ),
    );
  }


  Future<List<Member>> getData(String? filter, List<Member> list) async {
    List<Member> tmp = list.where((element) => element.fullName!.toUpperCase().contains(filter!.toUpperCase())).toList();
    return tmp;
  }

  Future<String> getCurrentLocation() async {
    String gpsLatLongCheckin;
    try{
      Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      gpsLatLongCheckin = "${position.latitude},${position.longitude}";
    } catch(e) {
      gpsLatLongCheckin = "";
    }
    return gpsLatLongCheckin;
  }

  save(context) async {
    var checkInLatLong = await getCurrentLocation();
    var diaDiem = "Thăm hộ NKT ${member != null ? member?.fullName : ""}";
    var checkIn = HbccCheckin()
      ..summary = diaDiem
      ..checkinTime = DateTime.now().toString()
      ..checkinLatlong = checkInLatLong
      ..memberId = (member != null ? member?.id : 0)
      ..provinceId = (member != null ? member?.provinceId : 0)
      ..districtId = (member != null ? member?.districtId : 0)
      ..wardId = (member != null ? member?.wardId : 0);

    var res = await checkinState.addCheckin(checkIn);
    if (res != null) {
      if(cameraState.files.isNotEmpty) {
        var fields = FileFieldUpload()
          ..ref = "checkin"
          ..refId = res.id
          ..field = "checkin_images";

        var fileResult = await cameraState.uploadImage(fields);
        if (fileResult == null) {
          checkinState.deleteCheckin(res.id);
          Flushbar(
            message: 'Xảy ra lỗi trong quá trình upload hình ảnh.',
            backgroundColor: Colors.red,
            icon: const Icon(Icons.check_circle, color: Colors.white,),
            duration: const Duration(seconds: 5),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        } else {
          Navigator.pop(context, res);
        }
      }else{
        Navigator.pop(context, res);
      }
    } else {
      Flushbar(
        message: 'Xảy ra lỗi trong quá trình checkin.',
        backgroundColor: Colors.red,
        icon: const Icon(Icons.check_circle,color: Colors.white,),
        duration: const Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    }
  }

}
