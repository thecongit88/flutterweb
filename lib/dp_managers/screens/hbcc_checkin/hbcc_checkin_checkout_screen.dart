import 'dart:io';
import 'package:another_flushbar/flushbar.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../common/text_theme_app.dart';
import '../../../common_screens/camera/camera_state.dart';
import '../../../models/files/file_fields_upload.dart';
import '../../../services/api_response.dart';
import '../../../states/checkin/hbcc_checkin_state.dart';
import '../../../states/member_state.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/camera/camera_capture_single_widget.dart';
import '../../../widgets/camera/camera_capture_widget.dart';
import '../../../widgets/image_display_list_widget.dart';
import '../../models/member.dart';

class HbccCheckinCheckoutScreen extends StatefulWidget {
  final HbccCheckin checkin;
  const HbccCheckinCheckoutScreen({Key? key,required this.checkin}) : super(key: key);

  @override
  _HbccCheckinCheckoutScreenState createState() => _HbccCheckinCheckoutScreenState();
}

class _HbccCheckinCheckoutScreenState extends State<HbccCheckinCheckoutScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  late HbccCheckin checkin;
  final contentController = TextEditingController();
  final resultController = TextEditingController();

  HbccCheckinState checkinState  = HbccCheckinState();
  final cameraState = CameraState();

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal, fontSize: 13);


  @override
  void initState() {
    super.initState();
    checkin = widget.checkin;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return
      Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          title: const Text("Checkout"),
          backgroundColor: ManagerAppTheme.nearlyBlue,
        ),
        body:
        SingleChildScrollView(
          child:
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
              width: double.infinity,
              color: Colors.white,
              padding: const EdgeInsets.all(15.0),
              child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                     Text("Thông tin checkin",style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: theme.primaryColor
                    ),),
                    AppConstant.spaceVerticalSmallExtraExtraExtra,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        wdgRowItem(title: "Người KT",
                            value: "${checkin.memberName}"),
                        wdgRowItem(title: "Địa chỉ",
                            value: "${checkin.address}, ${checkin.wardName}, ${checkin.districtName}, ${checkin.provinceName}"),
                      ],
                    ),
                    wdgRowItem(title: "Thời gian",
                        value: DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(checkin.checkinTime.toString()))),
                  ],
                )),
                Container(color: AppColors.grey60, height: 8,),
                Container(
                    width: double.infinity,
                    color: Colors.white,
                    padding: const EdgeInsets.all(15.0),
                    child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Thông tin checkout",style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: theme.primaryColor
                        ),),
                        AppConstant.spaceVerticalSmallExtraExtraExtra,
                        CameraCaptureWidget(controllerCamera: cameraState),
                        AppConstant.spaceVerticalMediumExtra,
                        TextThemeApp.wdgTitle("Kết quả", isRequired: false),
                        TextFormField(
                          controller: resultController,
                          maxLines: 4,
                          decoration:  InputDecoration(
                            hintText: "Nhập chi tiết kết quả",
                            hintStyle: TextStyle(fontSize: 12.sp),
                          ),
                        ),

                      ],
                    )),
              ]
          ),
        ),
        bottomNavigationBar: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(16),
          child:
          Row(
            children: [
              Expanded(
                child: ButtonWidget(
                  ic: Icons.maps_ugc_rounded,
                  bgColor: ManagerAppTheme.nearlyBlue,
                  title: "Checkout",
                  onPress: () async {
                    await save(context);
                  },
                ),
              )
            ],
          ),
        ),
      );
  }


  wdgRowItem({String? title, String? value}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              width: 80,
              child:
              Text(title ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 13.sp))
          ),
          Expanded(child:
          Text(value ?? "",
              style: TextStyle(
                  fontWeight: FontWeight.normal, fontSize: 13.sp)))
        ],
      ),
    );
  }


  Future<String> getCurrentLocation() async {
    String gpsLatLongCheckin;
    try{
      Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      gpsLatLongCheckin = "${position.latitude},${position.longitude}";
    } catch(e) {
      gpsLatLongCheckin = "";
    }
    return gpsLatLongCheckin;
  }


  save(context) async {
    var checkOutLatLong = await getCurrentLocation();
    var checkOut = checkin
      ..checkoutTime = DateTime.now().toString()
      ..checkoutLatlong = checkOutLatLong
      ..result = resultController.text;

    var res = await checkinState.updateCheckin(checkOut,checkOut.id!);
    if (res != null) {
      if(cameraState.files.isNotEmpty) {
        var fields = FileFieldUpload()
          ..ref = "checkin"
          ..refId = res.id
          ..field = "images";

        var fileResult = await cameraState.uploadImage(fields);
        if (fileResult == null) {
          Flushbar(
            message: 'Xảy ra lỗi trong quá trình upload hình ảnh.',
            backgroundColor: Colors.red,
            icon: const Icon(Icons.check_circle, color: Colors.white,),
            duration: const Duration(seconds: 5),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        } else {
          Navigator.pop(context, true);
        }
      }else{
        Navigator.pop(context, true);
      }
    } else {
      Flushbar(
        message: 'Xảy ra lỗi trong quá trình checkout.',
        backgroundColor: Colors.red,
        icon: const Icon(Icons.check_circle,color: Colors.white,),
        duration: const Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    }
  }


}
