
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../common/app_constant.dart';
import '../../../services/api_response.dart';
import '../../../states/checkin/hbcc_checkin_state.dart';
import '../../../widgets/bottom_sheet_widget.dart';
import '../../../widgets/empty_data_widget.dart';
import '../../../widgets/error_message.dart';
import '../../../widgets/loading_message.dart';
import '../../../widgets/search/filter_item_ui_widget.dart';
import '../../../widgets/search/filter_month_member_info_widget.dart';
import '../../../widgets/search/filter_month_member_widget.dart';
import '../../models/checkin/hbcc_checkin.dart';
import 'hbcc_checkin_checkout_screen.dart';
import 'hbcc_checkin_them_screen.dart';
import 'hbcc_checkin_view_screen.dart';

class HbccListCheckinScreen extends StatefulWidget {
  final Member? member;
  final int? thang;
  final int? nam;

  const HbccListCheckinScreen({Key? key, this.member, this.thang, this.nam}) : super(key: key);
  @override
  HbccListCheckinScreenState createState() => HbccListCheckinScreenState();
}

class HbccListCheckinScreenState extends State<HbccListCheckinScreen>
    with TickerProviderStateMixin {

  var checkinState = HbccCheckinState();

  TextStyle headerStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 13.sp);
  TextStyle itemStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 13.sp);

  @override
  void initState() {
    super.initState();
    checkinState.listCheckin();
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
      color: Colors.white,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          elevation: 0,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: const Text("Lịch sử checkin"),
          actions: [
            InkWell(
              child: const SizedBox(
                width: 50,
                child: Icon(Icons.filter_list),
              ),
              onTap: () async {
                openFilter();
              },
            )
          ],
        ),
        backgroundColor: Colors.white,
        body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => checkinState),
            ],
            child:
            Consumer<HbccCheckinState>(
                builder: (context, state, child) {
                  return SmartRefresher(
                      controller: checkinState.refreshController,
                      enablePullDown: true,
                      enablePullUp: true,
                      onRefresh: () => checkinState.listCheckin(),
                      onLoading: () => checkinState.listCheckin(isLoadMore: true),
                      child:
                  SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child:
                      Column(
                        children: [
                          FilterMonthMemberInfoWidget(filter: state.filterModel,onTap: (){
                            state.resetFilter();
                          }),
                          wdgList(state.listCheckinResponse)!
                        ],
                      )));
                },
              ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          onPressed: () async {
            var result = await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const HbccThemCheckinScreen()
            ));

            if(result != null) {
               await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => HbccCheckinCheckoutScreen(
                  checkin: result,
                ),
              ));

              await checkinState.listCheckin();
            }
          },
          child: const Icon(Icons.add, color: Colors.white),
        ),
      ),
    );
  }

  Widget? wdgList(ApiResponse<List<HbccCheckin>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgListRender(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget? wdgListRender(List<HbccCheckin> list) {
    return list.isEmpty ?
    const EmptyDataWidget()
        :

    ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index){
          return Divider(height: 1.sp,);
        },
        physics: const NeverScrollableScrollPhysics(),
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          HbccCheckin item = list[index];
          var actions = <Widget>[];
          if(item.checkinTime != null && item.checkoutTime == null
              && DateFormat("dd/MM/yyyy").format(DateTime.parse(item.checkinTime.toString()))
                  == DateFormat("dd/MM/yyyy").format(DateTime.now())){
            actions.add(
                SlidableAction(
                  flex: 2,
                  onPressed: (ctx) async {
                    await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => HbccCheckinCheckoutScreen(
                        checkin: item,
                      ),
                    ));
                  },
                  backgroundColor: Colors.orange,
                  foregroundColor: Colors.white,
                  icon: Icons.outbond,
                  label: 'Checkout',
                ));
          }
          return
            Slidable(
                key: const ValueKey(0),
                endActionPane: ActionPane(
                  motion: const ScrollMotion(),
                  children: actions,
                ),
                child:
                InkWell(
                  child:
                  Padding(
                      padding: EdgeInsets.only( bottom: 16.sp,top: 16.sp,left: 16,right: 16),
                      child:
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 24.sp,
                            height: 24.sp,
                            margin: EdgeInsets.only(right: 16.sp),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius
                                    .circular(100),
                                color: Colors.grey
                            ),
                            child:
                            Text("${index + 1}",
                                textAlign: TextAlign.center,
                                style: itemStyle.copyWith(color: Colors.white)),
                          ),
                          Expanded(
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("${item.summary}",
                                    style: itemStyle),
                                AppConstant.spaceVerticalSmallExtra,
                                Row(
                                  children: [
                                    const Icon(Icons.circle,size: 13,color: ManagerAppTheme.nearlyBlue,),
                                    AppConstant.spaceHorizontalSmall,
                                    Text(item.checkinTime != null ? DateFormat("dd/MM/yyyy HH:mm")
                                        .format(DateTime.parse(item.checkinTime.toString())) : "",
                                        style: itemStyle.copyWith(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey)),
                                    AppConstant.spaceHorizontalSmallExtra,
                                    item.checkoutTime != null ?
                                    const Icon(Icons.circle,size: 13,color: Colors.orange,):
                                    const SizedBox.shrink(),
                                    AppConstant.spaceHorizontalSmall,
                                    Text(item.checkoutTime != null ? DateFormat("dd/MM/yyyy HH:mm")
                                        .format(DateTime.parse(item.checkoutTime.toString())) : "",
                                        style: itemStyle.copyWith(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey)),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      )),
                  onTap: () async {
                    await Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => HbccCheckinViewScreen(
                        checkin: item,
                      ),
                    ));
                  },
                )
            );
        });
  }

  /// Mở bottom Modal search
  void openFilter() async {
    final ThemeData theme = Theme.of(context);

    FilterMonthMemberWidgetModel result = await openBottomSheet(
      context,
      title: "Tìm kiếm",
      child: FilterMonthMemberWidget(modelFiltered: checkinState.filterModel),
      color: theme.primaryColor,
    );
    if (result != null) {
      checkinState.setFilter(result);
    }
  }

}

