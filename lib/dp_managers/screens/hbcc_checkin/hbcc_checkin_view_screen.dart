import 'dart:io';
import 'package:another_flushbar/flushbar.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../common/text_theme_app.dart';
import '../../../common_screens/camera/camera_state.dart';
import '../../../models/files/file_fields_upload.dart';
import '../../../services/api_response.dart';
import '../../../states/checkin/hbcc_checkin_state.dart';
import '../../../states/member_state.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/camera/camera_capture_single_widget.dart';
import '../../../widgets/image_display_list_widget.dart';
import '../../models/member.dart';

class HbccCheckinViewScreen extends StatefulWidget {
  final HbccCheckin checkin;
  const HbccCheckinViewScreen({Key? key,required this.checkin}) : super(key: key);

  @override
  _HbccCheckinViewScreenState createState() => _HbccCheckinViewScreenState();
}

class _HbccCheckinViewScreenState extends State<HbccCheckinViewScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  late HbccCheckin checkin;


  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal, fontSize: 13);


  @override
  void initState() {
    super.initState();
    checkin = widget.checkin;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return
      Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: const Text("Chi tiết Checkin"),
          backgroundColor: ManagerAppTheme.nearlyBlue,
        ),
        body:
        SingleChildScrollView(
          child:
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: double.infinity,
                    color: Colors.white,
                    padding: const EdgeInsets.all(15.0),
                    child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        wdgRowItem(title: "Địa điểm",
                            value: "${checkin.summary}"),
                        wdgRowItem(title: "Thời gian",
                            value: "${DateFormat("dd/MM/yyyy HH:mm:ss")
                                .format(DateTime.parse(checkin.createdAt.toString()))} "),
                        wdgRowItem(title: "HBCC",
                            value: "${checkin.hbccName}"),
                        wdgRowItem(title: "Người KT",
                            value: "${checkin.memberName}"),
                        wdgRowItem(title: "Địa chỉ",
                            value: "${checkin.address}, ${checkin.wardName}, ${checkin.districtName}, ${checkin.provinceName}"),
                      ],
                    )),
                    Container(color: AppColors.grey60, height: 8,),
                    Container(
              width: double.infinity,
              color: Colors.white,
              padding: const EdgeInsets.all(15.0),
              child:
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Thông tin checkin",style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: theme.primaryColor
                    ),),
                    AppConstant.spaceVerticalSmallExtraExtraExtra,
                    wdgRowItem(title: "Thời gian",
                        value: DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(checkin.checkinTime.toString()))),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                            width: 80,
                            child:
                            Text("Hình ảnh",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 13.sp))
                        ),
                        Expanded(child:
                        ImageDisplayListWidget(files: checkin.checkinImages ?? [],isHideTitle: true))
                      ],
                    )
                  ],
                )),
                Container(color: AppColors.grey60, height: 8,),
                Container(
                    width: double.infinity,
                    color: Colors.white,
                    padding: const EdgeInsets.all(15.0),
                    child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Thông tin checkout",style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: theme.primaryColor
                        ),),
                        AppConstant.spaceVerticalSmallExtraExtraExtra,
                        wdgRowItem(title: "Thời gian",
                            value:
                            checkin.checkoutTime != null ?
                            DateFormat("dd/MM/yyyy HH:mm:ss").format(DateTime.parse(checkin.checkoutTime.toString())): ""
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                                width: 80,
                                child:
                                Text("Hình ảnh",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold, fontSize: 13.sp))
                            ),
                            Expanded(child:
                            ImageDisplayListWidget(files: checkin.images ?? [],isHideTitle: true))
                          ],
                        ),
                        AppConstant.spaceVerticalSmallExtraExtraExtra,
                        wdgRowItem(title: "Kết quả",
                            value: checkin.result ?? ""),

                      ],
                    ))
              ]
          ),
        ),
      );
  }


  wdgRowItem({String? title, String? value}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              width: 80,
              child:
              Text(title ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 13.sp))
          ),
          Expanded(child:
          Text(value ?? "",
              style: TextStyle(
                  fontWeight: FontWeight.normal, fontSize: 13.sp)))
        ],
      ),
    );
  }

  wdgRowItemStatus({String? title,int? status}) {
    var statusName  = "Chờ xem";
    var color = Colors.yellow;
    switch(status){
      case 1:
        statusName = "Chờ xem";
        color = Colors.amber;
        break;
      case 2:
        statusName = "Đã gửi";
        color = Colors.blue;
        break;
      case 3:
        statusName = "Đã trả lời";
        color = Colors.orange;
        break;
      case 4:
        statusName = "Hoàn thành";
        color = Colors.green;
        break;
      case 5:
        statusName = "Đã hủy";
        color = Colors.red;
        break;
    }
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
              width: 80,
              child:
              Text(title ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 13.sp))
          ),
          Container(
              padding: const EdgeInsets.symmetric(vertical: 4,horizontal: 12),
              color: color,
              child:
              Text(statusName ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 12.sp,color: Colors.white))
          )
        ],
      ),
    );
  }


}
