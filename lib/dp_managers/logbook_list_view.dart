import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_month_screen.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/logbook.dart';
import 'package:hmhapp/dp_managers/models/post.dart';
import 'package:hmhapp/dp_managers/plan_info_screen.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_create_screen.dart';
import 'package:hmhapp/main.dart';
import 'package:hmhapp/widgets/calendar_strip.dart';
import 'package:hmhapp/widgets/footer.dart';
import 'package:hmhapp/widgets/i18n_calendar_strip.dart';

class LogBookListView extends StatefulWidget {
  const LogBookListView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _LogBookViewState createState() => _LogBookViewState();
}

class _LogBookViewState extends State<LogBookListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    DateTime startDate = DateTime.now().subtract(const Duration(days: 2));
    DateTime endDate = DateTime.now().add(const Duration(days: 2));
    DateTime selectedDate = DateTime.now().subtract(const Duration(days: 2));
    List<DateTime> markedDates = [
      DateTime.now().subtract(const Duration(days: 1)),
      DateTime.now().subtract(const Duration(days: 2)),
      DateTime.now().add(const Duration(days: 4))
    ];

    onSelect(data) {
      print("Selected Date -> $data");
    }

    _monthNameWidget(monthName) {
      return Container(
        padding: const EdgeInsets.only(top: 8, bottom: 4),
        child: Text(monthName,
            style: const TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w600,
                color: Colors.black87)),
      );
    }

    getMarkedIndicatorWidget() {
      return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          margin: const EdgeInsets.only(left: 1, right: 1),
          width: 7,
          height: 7,
          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.red),
        ),
        Container(
          width: 7,
          height: 7,
          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
        )
      ]);
    }

    dateTileBuilder(
        date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {
      bool isSelectedDate = date.compareTo(selectedDate) == 0;
      Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.black87;
      TextStyle normalStyle = TextStyle(
          fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
      TextStyle selectedStyle = const TextStyle(
          fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black87);
      TextStyle dayNameStyle = TextStyle(fontSize: 14.5, color: fontColor);
      List<Widget> children = [
        Text(dayName, style: dayNameStyle),
        Text(date.day.toString(),
            style: !isSelectedDate ? normalStyle : selectedStyle),
      ];

      if (isDateMarked == true) {
        children.add(getMarkedIndicatorWidget());
      }

      return AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        alignment: Alignment.center,
        padding: const EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
        decoration: BoxDecoration(
          color: !isSelectedDate ? Colors.transparent : Colors.white70,
          borderRadius: const BorderRadius.all(Radius.circular(60)),
        ),
        child: Column(
          children: children,
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          title: const Text("Danh sách khảo sát"),
        ),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(bottom: 120),
                  child: Column(
                    children: <Widget>[
                      CalendarStrip(
                        startDate: startDate,
                        endDate: endDate,
                        onDateSelected: onSelect,
                        dateTileBuilder: dateTileBuilder,
                        iconColor: Colors.black87,
                        monthNameWidget: _monthNameWidget,
                        markedDates: markedDates,
                        containerDecoration: const BoxDecoration(color: Colors.black12),
                        locale: LocaleType.vi, selectedDate: selectedDate,
                      ),
                      FutureBuilder<bool>(
                        future: getData(),
                        builder:
                            (BuildContext context, AsyncSnapshot<bool> snapshot) {
                          if (!snapshot.hasData) {
                            return const SizedBox();
                          } else {
                            return ListView.separated(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              separatorBuilder: (BuildContext context, int index) =>
                                  const Divider(),
                              padding: const EdgeInsets.only(
                                  top: 10, bottom: 0, right: 16, left: 16),
                              itemCount: LogBook.logbookList.length,
                              itemBuilder: (BuildContext context, int index) {
                                final int count = LogBook.logbookList.length > 10
                                    ? 10
                                    : LogBook.logbookList.length;
                                final Animation<double> animation =
                                Tween<double>(begin: 0.0, end: 1.0).animate(
                                    CurvedAnimation(
                                        parent: animationController,
                                        curve: Interval((1 / count) * index, 1.0,
                                            curve: Curves.fastOutSlowIn)));
                                animationController.forward();

                                return _buildRow(LogBook.logbookList[index]);
                              },
                            );
                          }
                        },
                      ),
                    ],
                  ),
        )));
  }

  Widget _buildRow(LogBook logBook) {
    return ListTile(
      leading:
      logBook.logBookType==0? const Icon(Icons.access_alarms):const Icon(Icons.date_range,color: ManagerAppTheme.nearlyBlue,),
      title:
      logBook.logBookType==0? Text(logBook.title):Text(logBook.title,style: const TextStyle(color: ManagerAppTheme.nearlyBlue)),
      trailing:
      logBook.logBookType==0? const Icon(Icons.chevron_right):const Icon(Icons.chevron_right,color: ManagerAppTheme.nearlyBlue,),
    onTap: (){
        var detailScreen = logBook.logBookType==0?
        const ManagerLogBookFormCreateScreen(): LogBookFormMonthScreen();

      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => detailScreen,
          ));
    },
    );
  }
}
