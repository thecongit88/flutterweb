import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/post.dart';
import 'package:hmhapp/main.dart';

class PopularVideoListView extends StatefulWidget {
  const PopularVideoListView({Key? key,required this.callBack}) : super(key: key);

  final Function callBack;
  @override
  _PopularVideoListViewState createState() => _PopularVideoListViewState();
}

class _PopularVideoListViewState extends State<PopularVideoListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return GridView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: List<Widget>.generate(
                Post.popularVideoList.length,
                (int index) {
                  final int count = Post.popularVideoList.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return VideoView(
                    callback: () {
                      widget.callBack();
                    },
                    post:  Post.popularVideoList[index],
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 14.0,
                crossAxisSpacing: 15.0,
                childAspectRatio: 0.7,
              ),
            );
          }
        },
      ),
    );
  }
}

class VideoView extends StatelessWidget {
  const VideoView(
      {Key? key,
      required this.post,
        required this.animationController,
        required this.animation,
        required this.callback})
      : super(key: key);

  final VoidCallback callback;
  final Post post;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                callback();
              },
              child:  Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: ClipRRect(
                        borderRadius:
                        const BorderRadius.all(Radius.circular(16.0)),
                        child: AspectRatio(
                            aspectRatio: 1.28,
                            child: Image.asset(post.imagePath)),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 16),
                              child: Text(
                                post.title,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  letterSpacing: 0.27,
                                  color: ManagerAppTheme
                                      .darkerText,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8,),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment
                                    .spaceBetween,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '${post.createdDate}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w200,
                                      fontSize: 12,
                                      letterSpacing: 0.27,
                                      color: ManagerAppTheme
                                          .grey,
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          '${post.rating}',
                                          textAlign:
                                          TextAlign.left,
                                          style: TextStyle(
                                            fontWeight:
                                            FontWeight.w200,
                                            fontSize: 14,
                                            letterSpacing: 0.27,
                                            color:
                                            ManagerAppTheme
                                                .grey,
                                          ),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color:
                                          ManagerAppTheme
                                              .nearlyBlue,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
