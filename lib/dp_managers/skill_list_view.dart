import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/logbook.dart';
import 'package:hmhapp/dp_managers/models/post.dart';
import 'package:hmhapp/dp_managers/models/skill.dart';
import 'package:hmhapp/dp_managers/models/skill_groups.dart';
import 'package:hmhapp/dp_managers/plan_info_screen.dart';
import 'package:hmhapp/main.dart';
import 'package:hmhapp/widgets/calendar_strip.dart';
import 'package:hmhapp/widgets/footer.dart';
import 'package:hmhapp/widgets/i18n_calendar_strip.dart';

class SkillListView extends StatefulWidget {
  const SkillListView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _SkillListViewState createState() => _SkillListViewState();
}

class _SkillListViewState extends State<SkillListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          title: Text("Danh sách khảo sát"),
          leading: InkWell(
          borderRadius:
          BorderRadius.circular(AppBar().preferredSize.height),
          child: Icon(
            Icons.close,
            color: ManagerAppTheme.nearlyWhite,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),

        ),
        body: FutureBuilder<bool>(
                    future: getData(),
                    builder:
                        (BuildContext context, AsyncSnapshot<bool> snapshot) {
                      if (!snapshot.hasData) {
                        return const SizedBox();
                      } else {

                        return
                          Padding(
                            padding: EdgeInsets.only(left: 10,right: 10),
                            child:
                          ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return ExpansionTile(
                              title: Text(SkillGroup.groupList[index].title,
                                style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                              key: PageStorageKey(SkillGroup.groupList[index]),
                              children: SkillGroup.groupList[index].skills!.map((skill){
                                return
                                  CheckboxListTile(
                                    title: Text(skill.title, style: TextStyle(fontSize: 16),),
                                    value: skill.isPublished,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        skill.isPublished = (value == true);
                                      });
                                    },
                                  );
                              }).toList(),
                            );
                          },
                          itemCount: SkillGroup.groupList.length,
                        ),
                          );
                      }
                    },
                  ),
        );
  }

}
