import 'dart:io';

import 'package:date_strip_report/date_strip_report.dart';
import 'package:date_strip_report/i18n_calendar_strip.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/report_thuchien_kehoach.dart';
import 'package:hmhapp/models/report_tinhtrangnkt_hbcc.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/states/report_thuchien_kehoach_state.dart';
import 'package:hmhapp/states/report_tinhtrangnkt_hbcc_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class ManagerReportMainView extends StatefulWidget {
  final String reportType;

  const ManagerReportMainView({Key? key, required this.reportType}) : super(key: key);
  @override
  ManagerReportViewState createState() => ManagerReportViewState();
}

class ManagerReportViewState extends State<ManagerReportMainView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  late String _year;
  late int _yearCurrent;
  late String _nam;
  late Map<String, num> _soLuong;
  late int _currentSelection;
  late int _currentKehoach;
  late DateType _dateType;
  late DateTime _selectedDate, currentDateTime = DateTime.now();
  late int _selectedQuarter;
  late int _selectedHaft;
  late List<charts.Series<TanSuat, String>> _pieChartData;

  ReportThuchienKehoachState reportThuchienKehoachState = ReportThuchienKehoachState();
  ReportTanSuatNCSState reportTanSuatNCSState = ReportTanSuatNCSState();
  ReportTinhtrangnktHbccState reportTinhTrangNKTHbccState = ReportTinhtrangnktHbccState();
  late int fromMonth, toMonth;
  String typeChart = "";
  String exportTileFile = "";
  String listDataChart = "";


  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _yearCurrent = currentDateTime.year;
    _year = _yearCurrent.toString();
    _currentSelection = 0;
    _currentKehoach = 0;
    _dateType = DateType.month;
    _selectedDate = currentDateTime;
    _selectedQuarter = (_selectedDate.month/3).round();
    _selectedHaft = (_selectedDate.month/6).ceil();
    //_pieChartData = _sampleDataPie2020();

    reportThuchienKehoachState.getListReportThuchienKehoach(month: currentDateTime.month, year: currentDateTime.year);
    reportTanSuatNCSState.getListReportTanSuatThucHienKeHoachHBCC(month: currentDateTime.month, year: currentDateTime.year);
    reportTinhTrangNKTHbccState.getListReportTinhtrangnktHbcc(month: currentDateTime.month, year: currentDateTime.year);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/ds.csv');
  }

  Future<File> writeCounter(String str) async {
    final file = await _localFile;
    // Write the file.
    return file.writeAsString('$str', flush: true);
  }

  Future<String> readCounter() async {
    try {
      final file = await _localFile;
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return "";
    }
  }


  @override
  Widget build(BuildContext context) {
    final deviceOrientation = MediaQuery.of(context).orientation;

    List<Widget> _childWidgets = <Widget>[];
    _childWidgets.add(wdgBCKehoachCircle());
    _childWidgets.add(wdgBCKeHoachColumn());
    _childWidgets.add(wdgBCTinhTrang());


    Widget chooseReport() {
      if(widget.reportType == Constants.MANAGER_REPORT_PLAN_PERCENT_PIE){
        _currentSelection = 0;
      }else if(widget.reportType == Constants.MANAGER_REPORT_PLAN_PERCENT_COLUMN) {
        _currentSelection = 1;
      }else if(widget.reportType == Constants.MANAGER_REPORT_COMPARE_STATE_LINE){
        _currentSelection = 2;
      }else{
        _currentSelection = 0;
      }

      return _childWidgets[_currentSelection];
    }

    if(_currentSelection == 0) {
      exportTileFile = "BC % thực hiện theo kế hoạch";
    } else if(_currentSelection == 1) {
      exportTileFile = "BC so sánh % thực hiện kế hoạch";
    } else {
      exportTileFile = "BC tiến bộ lâm sàng";
    }

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => reportThuchienKehoachState), /* Report_thuchien_kehoach table */
        ChangeNotifierProvider(create: (_) => reportTanSuatNCSState), /* Report_tansuat_ncs table */
        ChangeNotifierProvider(create: (_) => reportTinhTrangNKTHbccState), /* Report_tinhtrangnkt_hbcc table */
      ],
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ManagerAppTheme.nearlyBlue,
          title: Text(exportTileFile),
          actions: <Widget>[
            Padding(padding: const EdgeInsets.only(right: 10),
                child: InkWell(
                  child: const Icon(Icons.save_alt),
                  onTap: () async {
                    String csvContent = "$exportTileFile $typeChart\n";
                    csvContent += "$listDataChart\n";
                    await writeCounter(csvContent);
                    try {
                      final path = await _localPath;
                      //ShareExtend.share("$path/ds.csv", "file", subject: exportTileFile);
                    } catch (e) {
                    }
                  },
                )
            )
          ],
        ),
        body: FutureBuilder<bool>(
          future: getData(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (!snapshot.hasData) {
              return const SizedBox();
            } else {
              return SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10,0,10,100),
                        child:
                        Container(child: chooseReport()),
                      )
                    ],
                  ));
            }
          },
        ),
      )
    );

  }

  Widget getButtonUI(DateType dateType, bool isSelected) {
    String txt = '';
    if (DateType.month == dateType) {
      txt = 'Tháng';
    } else if (DateType.quarter == dateType) {
      txt = 'Quý';
    } else if (DateType.half == dateType) {
      txt = 'Nửa năm';
    }else if (DateType.year == dateType) {
      txt = 'Năm';
    }
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: isSelected
                ? ManagerAppTheme.nearlyBlue
                : ManagerAppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: ManagerAppTheme.nearlyBlue)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
              setState(() {
               _dateType = dateType;
               _selectedDate = DateTime.now();
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 10, left: 10, right: 10),
              child: Center(
                child: Text(
                  txt,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: isSelected
                        ? ManagerAppTheme.nearlyWhite
                        : ManagerAppTheme.nearlyBlue,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }


  Widget wdgBCTanSuat(){
    List<Widget> childKeHoachWidgets = <Widget>[];
    childKeHoachWidgets.add(wdgBCKehoachCircle());
    childKeHoachWidgets.add(wdgBCKeHoachColumn());

    final children =
    SizedBox(
      child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20,0,20,50),
          child:
          Container(child: childKeHoachWidgets[_currentKehoach]),
        )
      ],
    ),);
    return children;
  }

  onSelect(ResultDate data) {
    print("Selected Date -> $data");
    //print(_currentSelection);
    //print(data.fromDate);
    //print(data.toDate);
    typeChart = data.info!;
    fromMonth = data.fromDate!.month;
    toMonth = data.toDate!.month;

    if(_currentSelection == 0) {
      WidgetsBinding.instance.addPostFrameCallback((_){
        if(fromMonth == toMonth) {
          reportThuchienKehoachState.getListReportThuchienKehoach(month: fromMonth, year: data.fromDate!.year);
        } else {
          reportThuchienKehoachState.getlistReportThuchienKehoachTheoKhoangThang(fromMonth: fromMonth, toMonth: toMonth, year: currentDateTime.year);
        }
      });
    }

    if(_currentSelection == 1) {
      WidgetsBinding.instance.addPostFrameCallback((_){
        if(fromMonth == toMonth) {
          reportTanSuatNCSState.getListReportTanSuatThucHienKeHoachHBCC(month: fromMonth, year: data.fromDate!.year);
        } else {
          reportTanSuatNCSState.getListReportTanSuatThucHienKeHoachHBCCTheoKhoangThang(fromMonth: fromMonth, toMonth: toMonth, year: currentDateTime.year);
        }
      });
    }

    if(_currentSelection == 2) {
      WidgetsBinding.instance.addPostFrameCallback((_){
        if(fromMonth == toMonth) {
          reportTinhTrangNKTHbccState.getListReportTinhtrangnktHbcc(month: fromMonth, year: data.fromDate!.year);
        } else {
          reportTinhTrangNKTHbccState.getListReportTinhtrangnktHbccTheoKhoangThang(fromMonth: fromMonth, toMonth: toMonth, year: currentDateTime.year);
        }
      });
    }
  }

  /* dữ liệu sẽ được lấy từ bảng report-tansuat-ncs (Report_tansuat_ncs) */
  Widget wdgBCKeHoachColumn(){
    final children =
    SizedBox(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: DateStripReport(
              startDate: DateTime(currentDateTime.year,1,1),
              endDate: DateTime(currentDateTime.year+1,1,1),
              isShowMonth: true,
              isShowHalfYear: true,
              isShowQuarter: true,
              isShowYear: true,
              textColor: ManagerAppTheme.nearlyBlue,
              leftIcon: const Icon(Icons.arrow_back_ios),
              rightIcon: const Icon(Icons.arrow_forward_ios),
              selectedColor: ManagerAppTheme.nearlyBlue,
              unSelectedColor: Colors.white,
              locale: LocaleType.vi,
              onDateSelected: onSelect,
            ),
          ),

          Consumer<ReportTanSuatNCSState>(
              builder: (context, state, child) {
                return wdgBCKehoachColumnRender(state.reporttansuatResponseList);
              }
          ),

          const SizedBox(height: 10,),
          Text(
            "Báo cáo so sánh % thực hiện kế hoạch $_year",
            textAlign: TextAlign.center,
          ),
        ],
      ),);
    return children;
  }

  /* dữ liệu sẽ được lấy từ bảng report-thuchien-kehoach (Report_thuchien_kehoach) */
  Widget wdgBCKehoachCircle(){
    final children =
    SizedBox(
      child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: DateStripReport(
                startDate: DateTime(_yearCurrent,1,1),
                endDate: DateTime(_yearCurrent+1,1,1),
                isShowMonth: true,
                isShowHalfYear: true,
                isShowQuarter: true,
                isShowYear: true,
                textColor: ManagerAppTheme.nearlyBlue,
                leftIcon: const Icon(Icons.arrow_back_ios),
                rightIcon: const Icon(Icons.arrow_forward_ios),
                selectedColor: ManagerAppTheme.nearlyBlue,
                unSelectedColor: Colors.white,
                locale: LocaleType.vi,
                onDateSelected: onSelect,
              ),
            ),

            Consumer<ReportThuchienKehoachState>(
                builder: (context, state, child) {
                  return wdgBCKehoachCircleRender(state.reportThucHienKeHoachResponseList);
                }
            ),

            const SizedBox(height: 10,),
            Text(
              "Báo cáo % thực hiện kế hoạch $_year",
              textAlign: TextAlign.center,
            ),
          ]),);
    return children;
  }

  Widget wdgBCTinhTrang(){
    final children =
    SizedBox(
        child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10),
            child: DateStripReport(
              startDate: DateTime(_yearCurrent,1,1),
              endDate: DateTime(_yearCurrent+1,1,1),
              isShowMonth: true,
              isShowHalfYear: true,
              isShowQuarter: true,
              isShowYear: true,
              textColor: ManagerAppTheme.nearlyBlue,
              leftIcon: const Icon(Icons.arrow_back_ios),
              rightIcon: const Icon(Icons.arrow_forward_ios),
              selectedColor: ManagerAppTheme.nearlyBlue,
              unSelectedColor: Colors.white,
              locale: LocaleType.vi,
              onDateSelected: onSelect,
            ),
          ),

          Consumer<ReportTinhtrangnktHbccState>(
              builder: (context, state, child) {
                return wdgBCTinhtrangnktHbccRender(state.reportTinhTrangNktHbccResponseList);
              }
          ),

          const SizedBox(height: 10,),
          Text(
            "Báo cáo tình trạng bệnh nhân năm $_year",
            textAlign: TextAlign.center,
          ),
    ]),);
    return children;
  }

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    String nam = "";
    final soluong = <String, num>{};

    // We get the model that updated with a list of [SeriesDatum] which is
    // simply a pair of series & datum.
    //
    // Walk the selection updating the measures map, storing off the sales and
    // series name for each selection point.
    if (selectedDatum.isNotEmpty) {
       nam = selectedDatum.first.datum.year;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        soluong[datumPair.series.displayName!] = datumPair.datum.soLuong;
      });
    }

    // Request a build.
    setState(() {
      _nam = nam;
      _soLuong = soluong;
    });
  }

  List<charts.Series<TanSuat, String>> _sampleData2020(List<ReportTanSuatNCS> data) {
    listDataChart = "Tháng,Tổng thực hiện,Tổng kế hoạch\n";

    var vTH = <ReportTanSuatNCS>[];
    final List<TanSuat> dataDaThucHien = [];
    final List<TanSuat> dataKeHoach = [];
    int tongThucHien, tongKeHoach, n;
    for(int i=1; i<=12; i++){
      if(fromMonth != null && toMonth != null && (i < fromMonth || i > toMonth)) continue;
      vTH = data.where((item) => item.month == i).toList();
      n = vTH.length; tongThucHien = 0; tongKeHoach = 0;
      if(n > 0) {
        for(int k = 0; k < n; k++) {
          tongThucHien += vTH[k].tansuat_thuchien!;
          tongKeHoach += vTH[k].tansuat_kehoach!;
        }
        dataDaThucHien.add(new TanSuat('T$i', tongThucHien));
        dataKeHoach.add(new TanSuat('T$i', tongKeHoach));
        listDataChart += '$i,$tongThucHien,$tongKeHoach\n';
      } else {
        dataDaThucHien.add(new TanSuat('T$i', 0));
        dataKeHoach.add(new TanSuat('T$i', 0));
        listDataChart += '$i,0,0\n';
      }
    }

    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }

  List<charts.Series<TanSuat, String>> _sampleDataPie2020(List<ReportThuchienKehoach> data) {
    int tong_theokehoach = 0, tong_khongtheokehoach = 0;
    data.forEach((thKeHoach) {
      tong_theokehoach += thKeHoach.soluong_theokehoach!;
      tong_khongtheokehoach += thKeHoach.soluong_khongtheokehoach!;
    });

    int tongSoLuong = tong_theokehoach + tong_khongtheokehoach;


    listDataChart = "";
    listDataChart += 'Theo kế hoạch,$tong_theokehoach%\n';
    listDataChart += 'Không theo kế hoạch,$tong_khongtheokehoach%\n';

    final dataDaThucHien = [
      new TanSuat('Theo kế hoạch', tong_theokehoach),
      new TanSuat('Không theo kế hoạch', tong_khongtheokehoach),
    ];

    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
        labelAccessorFn: (TanSuat row, _) => '${((row.soLuong/tongSoLuong)*100).toStringAsFixed(2)}%',
      )
    ];
  }

  List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2020(data) {

    var vTH = <ReportTinhTrangNktHbcc>[];
    final List<TinhTrang> dataXauHon = [];
    final List<TinhTrang> dataGiuNguyen = [];
    final List<TinhTrang> dataCaiThienIt = [];
    final List<TinhTrang> dataCaiThienVua = [];
    final List<TinhTrang> dataCaiThienNhieu = [];
    listDataChart = "Tháng,SL Xấu Hơn, SL Giữ Nguyên, SL Cải Thiện Ít, SL Cải Thiện Vừa, SL Cải Thiện Nhiều\n";
    for(int i=1; i<=12; i++) {
      if(fromMonth != null && toMonth != null && (i < fromMonth || i > toMonth)) continue;
      vTH = data.where((item) => item.month == i).toList();
      if(vTH.length > 0) {
        dataXauHon.add(new TinhTrang(i, vTH[0].count_xauhon ?? 0));
        dataGiuNguyen.add(new TinhTrang(i, vTH[0].count_giunguyen ?? 0));
        dataCaiThienIt.add(new TinhTrang(i, vTH[0].count_caithienit ?? 0));
        dataCaiThienVua.add(new TinhTrang(i, vTH[0].count_caithienvua ?? 0));
        dataCaiThienNhieu.add(new TinhTrang(i, vTH[0].count_caithiennhieu ?? 0));
        listDataChart += '$i,${vTH[0].count_xauhon},${vTH[0].count_giunguyen},${vTH[0].count_caithienit},${vTH[0].count_caithienvua},${vTH[0].count_caithiennhieu}\n';
      } else {
        dataXauHon.add(new TinhTrang(i, 0));
        dataGiuNguyen.add(new TinhTrang(i, 0));
        dataCaiThienIt.add(new TinhTrang(i, 0));
        dataCaiThienVua.add(new TinhTrang(i, 0));
        dataCaiThienNhieu.add(new TinhTrang(i, 0));
        listDataChart += '$i,0,0,0,0,0\n';
      }
    }

    return [
      new charts.Series<TinhTrang, int>(
        id: 'TT xấu hơn',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.red.shadeDefault,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataXauHon,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'TT giữ nguyên',
        colorFn: (_, __) => charts.MaterialPalette.deepOrange.shadeDefault.lighter,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.deepOrange.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataGiuNguyen,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'TT cải thiện ít',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.blue.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataCaiThienIt,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'TT cải thiện vừa',
        colorFn: (_, __) => charts.MaterialPalette.cyan.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.cyan.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataCaiThienVua,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'TT cải thiện nhiều',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.green.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataCaiThienNhieu,
      )
    ];
  }

  Widget wdgBCKehoachCircleRender(ApiResponse<List<ReportThuchienKehoach>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgBCKehoachCircleContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgBCKehoachCircleContent(List<ReportThuchienKehoach> data) {
    return
    data.length > 0 ?
    SizedBox(
      height: 400.0,
      child: charts.PieChart(
          _sampleDataPie2020(data),
          animate: false,
          //behaviors: [new charts.DatumLegend()],
          behaviors: [
            new charts.DatumLegend(
              position: charts.BehaviorPosition.top,
              outsideJustification: charts.OutsideJustification.endDrawArea,
              horizontalFirst: false,
              desiredMaxRows: 1,
            )
          ],
          defaultRenderer: new charts.ArcRendererConfig(arcRendererDecorators: [
            new charts.ArcLabelDecorator(
                labelPosition: charts.ArcLabelPosition.auto)

          ])
      ),
    )
    :
    SizedBox(
      height: 100.0,
      child: Center(child: Text("Không có dữ liệu cho bản đồ")),
    );
  }

  Widget wdgBCKehoachColumnRender(ApiResponse<List<ReportTanSuatNCS>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgBCKehoachColumnContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        case Status.INIT:
      return SizedBox.shrink();

    }
  }

  Widget wdgBCKehoachColumnContent(List<ReportTanSuatNCS> data) {
    return SizedBox(
      height: 400.0,
      child:
      data.length > 0 ?
      charts.BarChart(
        _sampleData2020(data),
        animate: false,
        barGroupingType: charts.BarGroupingType.grouped,
        behaviors: [new charts.SeriesLegend(
          showMeasures: true,
          measureFormatter: (num? value) {
            return value == null ? '-' : '${value}';
          },
        )],
      )
          :
      SizedBox(
        height: 100.0,
        child: Center(child: Text("Không có dữ liệu cho bản đồ")),
      ),
    );
  }

  Widget wdgBCTinhtrangnktHbccRender(ApiResponse<List<ReportTinhTrangNktHbcc>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgBCTinhtrangnktHbccContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();

    }
  }

  Widget wdgBCTinhtrangnktHbccContent(List<ReportTinhTrangNktHbcc> data) {
    return SizedBox(
      height: 400.0,
      child:
      data.length > 0 ?
        charts.LineChart(
          _sampleDataTinhTrang2020(data),
          animate: false,
          defaultRenderer: new charts.LineRendererConfig(
            includePoints: true,
          ),
          behaviors: [
              new charts.SeriesLegend(
                showMeasures: true,
                legendDefaultMeasure: charts.LegendDefaultMeasure.none,
                measureFormatter: (num? value) {
                  return value == null ? '-' : '${value}';
                },
                outsideJustification: charts.OutsideJustification.middle,
                horizontalFirst: false,
                desiredMaxRows: 3,
                entryTextStyle: charts.TextStyleSpec(fontSize: 15),
              ),
          ],
      )
        :
      SizedBox(
        height: 100.0,
        child: Center(child: Text("Không có dữ liệu cho bản đồ")),
      ),
    );
  }

}


enum DateType {
  month,
  quarter,
  half,
  year,
}

class TanSuat {
  final String year;
  final int soLuong;

  TanSuat(this.year, this.soLuong);
}

class TinhTrang {
  final int year;
  final int soLuong;

  TinhTrang(this.year, this.soLuong);
}