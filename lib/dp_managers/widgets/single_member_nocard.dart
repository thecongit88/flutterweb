import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';

class SingleMemberNoCard extends StatelessWidget {
  final Member entry;
  final int maxLines;
  final bool? showCategoryName;
  final bool showAuthor;
  final Function onTap;

  SingleMemberNoCard(
      {Key? key,
      required this.entry,
      this.showCategoryName,
      this.maxLines = 3,
      this.showAuthor = false,
      required  this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {

    final subTitle = "NCS: ${StringUtils.capitalize(entry.ncs ?? "")}";

    return ListTile(
      leading:
      entry.avatar!.isNotEmpty?
      CoverImageDecoration(
        url: entry.avatar,
        width: 100,
        height: 80,
        borderRadius: 2.0,
      ):
          Image(
            width: 100,
            height: 80,
            image:
            Image.asset("assets/images/avatar01.png", fit: BoxFit.fitWidth).image
          ),

      title: Text(
        entry.name ?? "",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        maxLines: maxLines,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Padding(
          padding: EdgeInsets.only(top: 5),
          child: Row(children: [
            Text(
              subTitle,
              maxLines: 1,
              style: TextStyle(fontSize: 12),
            )
          ])),
      trailing: Icon(Icons.arrow_forward_ios,size: 18,),
      onTap:  onTap(),
    );
  }
}
