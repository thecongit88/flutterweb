import 'dart:math';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/common_screens/posts/post_screen.dart';
import 'package:hmhapp/common_screens/videos/video_detail_screen.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_month_screen.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_create_screen.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_detail_screen.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/manager_home_state.dart';
import 'package:hmhapp/states/bottom_tab_state.dart';
import 'package:hmhapp/widgets/footer.dart';
import 'package:hmhapp/widgets/posts/post_carousel_view.dart';
import 'package:provider/provider.dart';

import '../screens/hbcc_checkin/hbcc_checkin_list_screen.dart';
import '../screens/hbcc_support/hbcc_support_list_screen.dart';

class ManagerHomeView extends StatefulWidget {
  @override
  _ManagerHomeViewState createState() => _ManagerHomeViewState();
}

class _ManagerHomeViewState extends State<ManagerHomeView> {
  final GlobalKey<ScaffoldState> scaffoldkey = new GlobalKey<ScaffoldState>();

  String avataUrl = "";

  CategoryType categoryType = CategoryType.ui;
  late AnimationController animationController;
  late ManagerHomeState managerHomeState;
  late BottomTabState bottomTabState;

  @override
  void initState() {
    super.initState();
    managerHomeState = ManagerHomeState();
    bottomTabState = BottomTabState();
    managerHomeState.getListPost();
    managerHomeState.getListFormItemLogbookHBCC(); /* lấy danh sách 5 đánh giá gần đây cho HBCC */
    managerHomeState.getAvatar();
  }



  @override
  Widget build(BuildContext context) {
    return
    ChangeNotifierProvider(
      create: (_) => managerHomeState,
    child:
    Scaffold(
      appBar: AppBar(
        backgroundColor: ManagerAppTheme.nearlyBlue,
      elevation: 0,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: const <Widget>[
          Text(
            'Đánh giá chăm sóc',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              letterSpacing: 0.2,
              color: ManagerAppTheme.nearlyWhite,
            ),
          ),
          Text(
            'Ứng dụng HBCC',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 22,
              letterSpacing: 0.27,
              color: ManagerAppTheme.nearlyWhite,
            ),
          ),
        ],
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child:
          GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, "/hbcc_profile");
              },
              child:
              Container(
                padding: const EdgeInsets.only(top:8,bottom: 8),
                width: 40,
                child: Consumer<ManagerHomeState>(
                    builder: (context, state, child) {
                      return state!.avataUrl!.isEmpty ? CircleAvatar(
                          backgroundImage:
                      NetworkImage( state!.avataUrl!)
                      ): const CircleAvatar(
                      backgroundImage:
                      AssetImage('assets/images/userImage.png')
                      );
                    }),
              )
          ),
        )
      ],
    ),
      key: scaffoldkey,
      backgroundColor: Colors.transparent,
      body:
      Column(
        children: <Widget>[
         Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  //getNotification(),
                  wdgControlsHome(),
                  Container(
                    padding: const EdgeInsets.only(top: 20),
                    color: HexColor('#F8FAFB'),
                    child: getLogbookLastest(),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16,bottom: 0),
                    child:
                    Row(
                      children: <Widget>[
                        const Expanded(
                          child: Text(
                            'Tin tức',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 22,
                              color: Colors.black87,
                            ),
                          ),
                        ),
                        InkWell(
                          highlightColor: Colors.transparent,
                          borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) => PostScreen()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Row(
                              children: const <Widget>[
                                Text(
                                  "Tất cả",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16,
                                    letterSpacing: 0.5,
                                    color: Colors.black87,
                                  ),
                                ),
                                SizedBox(
                                  height: 38,
                                  width: 26,
                                  child: Icon(
                                    Icons.chevron_right,
                                    color: Colors.black87,
                                    size: 18,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Consumer<ManagerHomeState>(
                      builder: (context, state, child) {
                        return getLastestPost(state);
                      }),
                  Footer()
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }


  Widget getLastestPost(ManagerHomeState state) {
    switch (state.postResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
      case Status.COMPLETED:
        return PostCarouselView(
          posts: state.postResponse.data!,
          callBack: () {
            moveTo();
          },
        );
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.postResponse.message,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return SizedBox();
      case Status.INIT:
        return SizedBox();
      default:
        return SizedBox();
    }
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  Widget getLogbookLastest() {

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              const Expanded(
                child: Text(
                  'Đánh giá gần đây',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    color: ManagerAppTheme.darkerText,
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                onTap: () {
                  Provider.of<BottomTabState>(context, listen: false).setScreenIndex(2);
                  //Navigator.push(context,
                    //  MaterialPageRoute(
                      //    builder: (BuildContext context) => ManagerLogBooksView()));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Row(
                    children: const <Widget>[
                      Text(
                        "Tất cả",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                          letterSpacing: 0.5,
                          color: ManagerAppTheme.nearlyBlack,
                        ),
                      ),
                      SizedBox(
                        height: 38,
                        width: 26,
                        child: Icon(
                          Icons.chevron_right,
                          color: ManagerAppTheme.darkText,
                          size: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),

          Consumer<ManagerHomeState>(builder: (context, state, child) {
            return bindLastestListSurveyForm(state, context);
          }),

        ],
      ),
    );
  }

  Widget _buildRow(FormItemLogBook formItemLogBook) {
    /*
    String _title = "Chưa nhập ngày đánh giá";
    if(formItemLogBook.formDate != null) {
      DateTime formDate = DateTime.parse(formItemLogBook.formDate);
      _title = "Khảo sát T"+formDate.month.toString() + "/" + formDate.year.toString();
    }
    */

    String _title = formItemLogBook.name.toString().isNotEmpty ? formItemLogBook.name ?? "" : "-";

    return new ListTile(
      leading: Icon(Icons.date_range,color: ManagerAppTheme.nearlyBlue,),
      title: Text(_title, style: TextStyle(color: ManagerAppTheme.nearlyBlue)),
      subtitle:
      Padding(
        padding: EdgeInsets.only(top:6),
        child:
        Text('${formItemLogBook.member!.name ?? ""} / ${formItemLogBook.formDate}',
            style: TextStyle(fontSize: 13,color: Colors.black54)),
      ),
      trailing: Icon(Icons.chevron_right,color: ManagerAppTheme.nearlyBlue,),
      onTap: () {

        var detailScreen = LogBookFormDetailScreen(
          //formType: formItemLogBook.formType,
          //memberId: formItemLogBook.memberId,
          //formId: formItemLogBook.formId,
          //member: formItemLogBook.member,
          //formSurveyId: formItemLogBook.id,
            formItemLogBook: formItemLogBook
        );
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => detailScreen,
            ));
      }
    );
  }

  void moveTo() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => VideoDetail(
          //detail: YoutubeModel.youtubeData[0],
        ),
      ),

    );
  }

  Widget getButtonUI(CategoryType categoryTypeData, bool isSelected) {
    String txt = '';
    if (CategoryType.ui == categoryTypeData) {
      txt = 'Hoạt động';
    } else if (CategoryType.coding == categoryTypeData) {
      txt = 'Thông báo';
    } else if (CategoryType.basic == categoryTypeData) {
      txt = 'Hỏi đáp';
    }
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: isSelected
                ? ManagerAppTheme.nearlyBlue
                : ManagerAppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: ManagerAppTheme.nearlyBlue)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
              setState(() {
                categoryType = categoryTypeData;
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 12, bottom: 12, left: 18, right: 18),
              child: Center(
                child: Text(
                  txt,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: isSelected
                        ? ManagerAppTheme.nearlyWhite
                        : ManagerAppTheme.nearlyBlue,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getNotification(){
    var rng = new Random();
    int randomNumber = rng.nextInt(3);
    if(randomNumber==0){
      return notifyLogBookDay();
    }else if(randomNumber==1){
      return notifyLogBookMonth();
    }else {
      return SizedBox(height: 1,);
    }
  }

  Widget notifyLogBookMonth() {
    return
      GestureDetector(
        child: Container(
            decoration: BoxDecoration(
              color: ManagerAppTheme.nearlyYellow,
              borderRadius: const BorderRadius.all(
                  Radius.circular(26.0)),
            ),
            margin: EdgeInsets.fromLTRB(20,20,20,10),
            padding: EdgeInsets.fromLTRB(20,10,20,10),
            child:
            Row(
              mainAxisAlignment:MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.new_releases,color: Colors.white),
                Text("Thực hiện đánh giá cuối tháng 03",
                    style:TextStyle(color: Colors.white, fontSize: 16)),
                Icon(Icons.chevron_right,color: Colors.white,),
              ],
            )
        ),
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => LogBookFormMonthScreen(),
              ));
        },
      );
  }

  Widget notifyLogBookDay(){
   return GestureDetector(
      child: Container(
          decoration: BoxDecoration(
            color: Colors.orange,
            borderRadius: const BorderRadius.all(
                Radius.circular(26.0)),
          ),
          margin: EdgeInsets.fromLTRB(20,20,20,10),
          padding: EdgeInsets.fromLTRB(20,10,20,10),
          child:
          Row(
            mainAxisAlignment:MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(Icons.date_range,color: Colors.white),
              Text("Vui lòng thực hiện đánh giá theo ngày",
                  style:TextStyle(color: Colors.white, fontSize: 16)),
              Icon(Icons.chevron_right,color: Colors.white,),
            ],
          )
      ),
      onTap: (){
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => ManagerLogBookFormCreateScreen(),
            ));
      },
    );
  }

  Widget bindLastestListSurveyForm(ManagerHomeState formState, BuildContext context) {
    switch (formState.formItemLogBookResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child: const Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
      case Status.COMPLETED:
        return bindLastestListSurveyContent(formState.formItemLogBookResponse.data!);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: formState.formItemLogBookResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

  bindLastestListSurveyContent(List<FormItemLogBook> _logbookList) {
    int logbookListLen = _logbookList.length;
    if (logbookListLen == 0) {
      return Center(
        child:
        Padding(
          padding: const EdgeInsets.only(top:50, bottom: 50),
          child:
          Column(
            children: [
              const Icon(Icons.deblur,size: 60,color: ManagerAppTheme.nearlyBlue,),
              AppConstant.spaceVerticalMedium,
              const Text("Chưa có bất kỳ đánh giá nào.", style: TextStyle(color: ManagerAppTheme.nearlyBlue)),
            ]
          )
        ),
      );
    } else {
      return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.only(
            top: 10, bottom: 0, right: 0, left: 0),
        itemCount: logbookListLen > 5
            ? 5
            : logbookListLen,
        itemBuilder: (BuildContext context, int index) {
          final int count = logbookListLen > 5
              ? 5
              : logbookListLen;
          return _buildRow(_logbookList[index]);
        },
      );
    }
  }

  wdgControlsHome() {
    var list = [
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: const Icon(Icons.group, size: 35,
                    color: ManagerAppTheme.nearlyBlue)
              /*Image.asset("assets/icons/truyen-thong.png",
                      fit: BoxFit.cover),*/
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                child:
                Text("NKT",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(1);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.access_time, size: 30.sp,color: ManagerAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Đánh giá",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(2);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.notifications_active_outlined, size: 30.sp,color: ManagerAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Tin tức",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PostScreen()));
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.pie_chart, size: 30.sp,color: ManagerAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Báo cáo",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(4);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: const Icon(Icons.post_add, size: 35,
                    color: ManagerAppTheme.nearlyBlue)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                child:
                Text("Kế hoạch chăm sóc",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(3);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.maps_ugc_outlined, size: 30.sp,color: ManagerAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Checkin",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const HbccListCheckinScreen()));
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                padding: const EdgeInsets.all(8),
                alignment: Alignment.center,
                decoration: ManagerAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.support, size: 30.sp,color: ManagerAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Hỗ trợ",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: ManagerAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => HbccSupportListScreen()));
        },
      ),
    ];

    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 20, left: 16, right: 16),
      child: GridView.builder(
        padding: EdgeInsets.zero,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          crossAxisSpacing: 4.sp,
          mainAxisSpacing: 4.sp,
          mainAxisExtent: 110.sp,
        ),
        shrinkWrap: true,
        primary: false,
        itemCount: list?.length ?? 0,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return list![index];
        },
      ),
    );
  }

}

enum CategoryType {
  ui,
  coding,
  basic,
}
