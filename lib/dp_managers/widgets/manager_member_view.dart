import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_member_detail_screen.dart';
import 'package:hmhapp/dp_managers/widgets/manage_members_search_delegate.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';

class ManagerMemberView extends StatefulWidget {
  const ManagerMemberView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _ManagerMemberViewState createState() => _ManagerMemberViewState();
}

class _ManagerMemberViewState extends State<ManagerMemberView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  final MemberState _memberState = MemberState();

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
    _memberState.getListMembers();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        appBar: AppBar(
        backgroundColor: ManagerAppTheme.nearlyBlue,
        elevation: 0,
        title: const Text("Danh sách NKT"),
        actions: <Widget>[
          Padding(padding: const EdgeInsets.only(right: 10),
          child:
          InkWell(
            child: const Icon(Icons.search),
            onTap: (){
              showSearch(
                context: context,
                delegate: ManageMemberSearchDelegate(),
              );
            },
          ),)
        ],
    ),
    backgroundColor: Colors.transparent,
    body: ChangeNotifierProvider(
      create: (_) => _memberState,
      child: Consumer<MemberState>(
        builder: (context, state, child) {
          return
            Padding(
              padding: const EdgeInsets.only(bottom: 80),
              child:
              wdgMainListNKT(state.memberResponseList)
            );
        },
      ),
    )
  );
  }

  Widget wdgMainListNKT(ApiResponse<List<Member>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormNKT(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget wdgFormNKT(List<Member> listMembers) {
    final int count = listMembers.length;
    if (count == 0) {
      return const Center(
        child: Text("Chưa có dữ liệu người khuyết tật!"),
      );
    }
    return ListView.separated(
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) =>
          const Divider(),
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        final Animation<double> animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
                parent: animationController,
                curve: Interval((1 / count) * index, 1.0,
                    curve: Curves.fastOutSlowIn)));
        animationController.forward();
        Member member =    listMembers[index];
        return ListTile(
          leading:
          Container(
              width: 60.0,
              height : 60.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      //image: AssetImage(member.avatar)
                      image:  member.avatar!.isEmpty ?
                      Image.asset("assets/images/avatar01.png", fit: BoxFit.fitWidth).image :
                      Image.network(member.avatar!, fit: BoxFit.fitWidth).image
                  )
              )),
          title: Padding(
              padding: const EdgeInsets.only(top: 7),
              child: Text(
                member.fullName ?? "",
                style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
          subtitle:
          Padding(padding:const EdgeInsets.only(top:5),
              child:
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text("NCS:",style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    letterSpacing: 0.27,
                    color: ManagerAppTheme.dark_grey,
                  ),),
                  Text(member.ncs ?? "",style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    letterSpacing: 0.27,
                    color: ManagerAppTheme.dark_grey,
                  ),)
                ],
              )),
          trailing: const Icon(Icons.arrow_forward_ios,size: 18,),
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => ManagerMemberDetailScreen(
                  member: member,
                ),
              ),
            );
          },
        );
      },
    );
  }
}