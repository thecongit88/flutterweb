import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/form_kncs_item.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';


class FormSkillKNCSItem extends StatefulWidget {
  final PlanCheck plan;
  final double? scores;
  final String?  result;
  final Function onChanged;

  const FormSkillKNCSItem({Key? key,required this.plan, this.scores, this.result,required this.onChanged}) : super(key: key);
  @override
  _FormSkillKNCSItemState createState() => _FormSkillKNCSItemState();
}

class _FormSkillKNCSItemState extends State<FormSkillKNCSItem> {
  late PlanCheck plan;
  late double scores;
   late String  result;
   late FormKNCSItem item;
   dynamic response;

   @override
   void initState() {
     plan = widget.plan;
     scores = (widget.scores!= null && widget.scores! > 0 ? widget.scores!: 0);
     result = (widget.result!= null && widget.result!.isNotEmpty ? widget.result!: "Không thực hiện");
     item = FormKNCSItem();
     item.plan= new Plan(id: plan.id);
     item.scores = scores;
     item.result = result;
     item.formFormat = plan.formFormat;
     super.initState();
   }

   Widget getResult(double score){
     var title = "Không tốt";
     var color = ManagerAppTheme.nearRed;
    if(score <=4.9){
      title =  "Không tốt";
      color = ManagerAppTheme.nearRed;
    }else if(score>4.9 && score <= 7.9){
      title =  "Trung bình";
      color = ManagerAppTheme.nearLightBlue;
    }else if(score>7.9){
      title =  "Tốt";
      color = ManagerAppTheme.nearlyBlue;
     }
     result = title;
     return Text(title,style: TextStyle(color: color, fontSize: 18, fontWeight: FontWeight.w700));
   }

  void _handleChanged() {
    widget.onChanged(item);
  }

   @override
   Widget build(BuildContext ctx) {
    return
      Card(
        elevation: 4,
        child: ClipPath(
          child: Container(
              decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: ManagerAppTheme.nearlyBlue, width: 5))),
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    width: double.infinity,
                    color: ManagerAppTheme.nearlyBlue,
                    child: Text(plan.title ?? "",style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w700),),
                  ),
                  new JsonSchema(
                    formMap: plan.formFormat!,
                    onChanged: (dynamic response) {
                      this.response = response;
                      double _scores = 0;
                      var items = this.response["fields"];
                     for (var i = 0; i < items.length; i++) {
                       if(items[i]["score"]!=null) {
                         _scores += double.parse(items[i]["score"]);
                       }
                     }
                      setState(() {
                        scores = _scores;
                      });
                      //print(response);
                      item.result = result;
                      item.formFormat = this.response;
                      item.scores = scores;
                      _handleChanged();
                    },
                  ),
                  Container(
                      padding: EdgeInsets.all(10),
                      width: double.infinity,
                      color: Color(0xffededed),
                      child:
                      Column(
                        children: <Widget>[
                          Text("Đánh giá kết quả",style: TextStyle(color: Colors.black87, fontSize: 18,fontWeight: FontWeight.w700),),
                          SizedBox(height: 8,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Điểm số:",style: TextStyle(color: Colors.black87, fontSize: 18),),
                              SizedBox(width: 20,),
                              Text("${scores}/10",style: TextStyle(color: ManagerAppTheme.nearlyBlue, fontSize: 18, fontWeight: FontWeight.w700),),
                            ],
                          ),
                          SizedBox(height: 3,),
                          getResult(scores)
                        ],
                      )
                  ),
                ],
              )
          ),
          clipper: ShapeBorderClipper(shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
        ),
        margin: EdgeInsets.only(top:20),
      );
  }
}
