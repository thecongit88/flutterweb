import 'package:date_strip_report/date_strip_report.dart';
import 'package:date_strip_report/i18n_calendar_strip.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/utils/constants.dart';

class ManagerFormStepView extends StatefulWidget {
  final String reportType;

  const ManagerFormStepView({Key? key, required this.reportType}) : super(key: key);
  @override
  _ManagerFormStepViewState createState() => _ManagerFormStepViewState();
}

class _ManagerFormStepViewState extends State<ManagerFormStepView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  late String _year;
  late  String _nam;
  late Map<String, num> _soLuong;
  late  int _currentSelection;
  late int _currentKehoach;
  late DateType _dateType;
  late DateTime _selectedDate;
  late int _selectedQuarter;
  late int _selectedHaft;
  late List<charts.Series<TanSuat, String>> _pieChartData;

  Map<int, Widget> _children = {
    0: Text('Hình tròn'),
    1: Text('Cột')
  };



  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _year = DateTime.now().year.toString();
    _currentSelection = 0;
    _currentKehoach = 0;
    _dateType = DateType.month;
    _selectedDate = DateTime.now();
    _selectedQuarter = (_selectedDate.month/3).round();
    _selectedHaft = (_selectedDate.month/6).ceil();
    _pieChartData = _sampleDataPie2020();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceOrientation = MediaQuery.of(context).orientation;

    List<Widget> _childWidgets = <Widget>[];
    _childWidgets.add(wdgBCKehoachCircle());
    _childWidgets.add(wdgBCKeHoachColumn());
    _childWidgets.add(wdgBCTinhTrang());


    Widget chooseReport() {
        if(widget.reportType == Constants.MANAGER_REPORT_PLAN_PERCENT_PIE){
          _currentSelection = 0;
        }else if(widget.reportType == Constants.MANAGER_REPORT_PLAN_PERCENT_COLUMN) {
          _currentSelection = 1;
        }else if(widget.reportType == Constants.MANAGER_REPORT_COMPARE_STATE_LINE){
          _currentSelection = 2;
        }else{
          _currentSelection = 0;
      }
      return _childWidgets[_currentSelection];
    }

    return new Scaffold(
      appBar: AppBar(
        backgroundColor: ManagerAppTheme.nearlyBlue,
        title: Text("Báo cáo"),
          actions: <Widget>[
            Padding(padding: EdgeInsets.only(right: 10),
            child: InkWell(
                  child: Icon(Icons.save_alt),
                ))
          ],
      ),
      body: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return SingleChildScrollView(
                child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(20,0,20,100),
                          child:
                          new Container(child: chooseReport()),
                        )
                      ],
                    ));
          }
        },
      ),
    );

  }

  Widget getButtonUI(DateType dateType, bool isSelected) {
    String txt = '';
    if (DateType.month == dateType) {
      txt = 'Tháng';
    } else if (DateType.quarter == dateType) {
      txt = 'Quý';
    } else if (DateType.half == dateType) {
      txt = 'Nửa năm';
    }else if (DateType.year == dateType) {
      txt = 'Năm';
    }
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: isSelected
                ? ManagerAppTheme.nearlyBlue
                : ManagerAppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: ManagerAppTheme.nearlyBlue)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
              setState(() {
               _dateType = dateType;
               _selectedDate = DateTime.now();
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 10, left: 10, right: 10),
              child: Center(
                child: Text(
                  txt,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: isSelected
                        ? ManagerAppTheme.nearlyWhite
                        : ManagerAppTheme.nearlyBlue,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }


  Widget wdgBCTanSuat(){
    List<Widget> _childKeHoachWidgets = <Widget>[];
    _childKeHoachWidgets.add(wdgBCKehoachCircle());
    _childKeHoachWidgets.add(wdgBCKeHoachColumn());

    final children =
    SizedBox(
      child: Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(20,0,20,50),
          child:
          new Container(child: _childKeHoachWidgets[_currentKehoach]),
        )
      ],
    ),);
    return children;
  }

  onSelect(data) {
    print("Selected Date -> $data");
  }

  Widget wdgBCKeHoachColumn(){
    final children =
    SizedBox(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: DateStripReport(
              startDate: new DateTime(2020,1,1),
              endDate: new DateTime(2021,1,1),
              isShowMonth: true,
              isShowHalfYear: true,
              isShowQuarter: true,
              isShowYear: true,
              textColor: ManagerAppTheme.nearlyBlue,
              leftIcon: Icon(Icons.arrow_back_ios),
              rightIcon: Icon(Icons.arrow_forward_ios),
              selectedColor: ManagerAppTheme.nearlyBlue,
              unSelectedColor: Colors.white,
              locale: LocaleType.vi,
              onDateSelected: onSelect,
            ),
          ),
          new SizedBox(
            height: 400.0,
            child: charts.BarChart(
              _year=="2020"?
              _sampleData2020():_sampleData2019(),
              animate: true,
              barGroupingType: charts.BarGroupingType.stacked,
              behaviors: [
                new charts.PercentInjector(totalType: charts.PercentInjectorTotalType.domain)
              ,new charts.SeriesLegend(
                  showMeasures: true,
                  measureFormatter: (num? value) {
                    return value == null ? '-' : '${(value*100).toStringAsFixed(1)}';
                  },
                )
              ],
              primaryMeasureAxis: new charts.PercentAxisSpec(),
            ),
          ),
          SizedBox(height: 10,),
          Text(
            "Báo cáo so sánh % thực hiện kế hoạch $_year",
            textAlign: TextAlign.center,
          ),
        ],
      ),);
    return children;
  }

  Widget wdgBCKehoachCircle(){
    final children =
    SizedBox(
      child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10),
              child: DateStripReport(
                startDate: new DateTime(2020,1,1),
                endDate: new DateTime(2021,1,1),
                isShowMonth: true,
                isShowHalfYear: true,
                isShowQuarter: true,
                isShowYear: true,
                textColor: ManagerAppTheme.nearlyBlue,
                leftIcon: Icon(Icons.arrow_back_ios),
                rightIcon: Icon(Icons.arrow_forward_ios),
                selectedColor: ManagerAppTheme.nearlyBlue,
                unSelectedColor: Colors.white,
                locale: LocaleType.vi,
                onDateSelected: onSelect,
              ),
            ),
            new SizedBox(
              height: 400.0,
              child: charts.PieChart(
                _pieChartData,
                  animate: false,
                  behaviors: [new charts.DatumLegend()],
                  defaultRenderer: new charts.ArcRendererConfig(arcRendererDecorators: [
                new charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.auto)

              ])
              ),
            ),
            SizedBox(height: 10,),
            Text(
              "Báo cáo % thực hiện kế hoạch $_year",
              textAlign: TextAlign.center,
            ),
          ]),);
    return children;
  }

  Widget wdgBCTinhTrang(){
    final children =
    SizedBox(
        child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10),
            child: DateStripReport(
              startDate: new DateTime(2020,1,1),
              endDate: new DateTime(2021,1,1),
              isShowMonth: true,
              isShowHalfYear: true,
              isShowQuarter: true,
              isShowYear: true,
              textColor: ManagerAppTheme.nearlyBlue,
              leftIcon: Icon(Icons.arrow_back_ios),
              rightIcon: Icon(Icons.arrow_forward_ios),
              selectedColor: ManagerAppTheme.nearlyBlue,
              unSelectedColor: Colors.white,
              locale: LocaleType.vi,
              onDateSelected: onSelect,
            ),
          ),
      new SizedBox(
        height: 400.0,
        child: charts.LineChart(
          _year=="2020"?
          _sampleDataTinhTrang2020():_sampleDataTinhTrang2019(),
          animate: false,
            defaultRenderer: new charts.LineRendererConfig(
                includePoints: true,
            )

        ),
      ),
          SizedBox(height: 10,),
      Text(
        "Báo cáo tình trạng bệnh nhân năm $_year",
        textAlign: TextAlign.center,
      ),
    ]),);
    return children;
  }


  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    String nam = "";
    final soluong = <String, num>{};

    // We get the model that updated with a list of [SeriesDatum] which is
    // simply a pair of series & datum.
    //
    // Walk the selection updating the measures map, storing off the sales and
    // series name for each selection point.
    if (selectedDatum.isNotEmpty) {
       nam = selectedDatum.first.datum.year;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        soluong[datumPair.series.displayName!] = datumPair.datum.soLuong;
      });
    }

    // Request a build.
    setState(() {
      _nam = nam;
      _soLuong = soluong;
    });
  }

  static List<charts.Series<TanSuat, String>> _sampleData2020() {
    final dataDaThucHien = [
      new TanSuat('T1', 8),
      new TanSuat('T2', 12),
      new TanSuat('T3', 10),
      new TanSuat('T4', 9),
      new TanSuat('T5', 6),
      new TanSuat('T6', 5),
      new TanSuat('T7', 6),
      new TanSuat('T8', 5),
      new TanSuat('T9', 4),
      new TanSuat('T10', 7),
      new TanSuat('T11', 8),
      new TanSuat('T12', 9),
    ];
    final dataKeHoach = [
      new TanSuat('T1', 8),
      new TanSuat('T2', 15),
      new TanSuat('T3', 12),
      new TanSuat('T4', 8),
      new TanSuat('T5', 5),
      new TanSuat('T6', 5),
      new TanSuat('T7', 9),
      new TanSuat('T8', 6),
      new TanSuat('T9', 6),
      new TanSuat('T10', 9),
      new TanSuat('T11', 4),
      new TanSuat('T12', 9),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }

  static List<charts.Series<TanSuat, String>> _sampleDataPie2020() {
    final dataDaThucHien = [
      new TanSuat('Theo kế hoạch', 30),
      new TanSuat('Không theo kế hoạch', 70),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
        labelAccessorFn: (TanSuat row, _) => '${row.soLuong}%',
      )
    ];
  }
  static List<charts.Series<TanSuat, String>> _sampleDataPie2019() {
    final dataDaThucHien = [
      new TanSuat('Theo kế hoạch', 40),
      new TanSuat('Không theo kế hoạch', 60),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
        labelAccessorFn: (TanSuat row, _) => '${row.soLuong}%',
      )
    ];
  }
  static List<charts.Series<TanSuat, String>> _sampleData2019() {
    final dataDaThucHien = [
      new TanSuat('T1', 3),
      new TanSuat('T2', 4),
      new TanSuat('T3', 5),
      new TanSuat('T4', 4),
      new TanSuat('T5', 3),
      new TanSuat('T6', 5),
      new TanSuat('T7', 6),
      new TanSuat('T8', 8),
      new TanSuat('T9', 6),
      new TanSuat('T10', 5),
      new TanSuat('T11', 6),
      new TanSuat('T12', 10),
    ];
    final dataKeHoach = [
      new TanSuat('T1', 5),
      new TanSuat('T2', 5),
      new TanSuat('T3', 10),
      new TanSuat('T4', 6),
      new TanSuat('T5', 9),
      new TanSuat('T6', 4),
      new TanSuat('T7', 6),
      new TanSuat('T8', 9),
      new TanSuat('T9', 5),
      new TanSuat('T10', 7),
      new TanSuat('T11', 4),
      new TanSuat('T12', 9),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }
  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2020() {
    final dataXauDi = [
      new TinhTrang(1, 8),
      new TinhTrang(2, 12),
      new TinhTrang(3, 10),
      new TinhTrang(4, 9),
      new TinhTrang(5, 6),
      new TinhTrang(6, 5),
      new TinhTrang(7, 6),
      new TinhTrang(8, 5),
      new TinhTrang(9, 4),
      new TinhTrang(10, 7),
      new TinhTrang(11, 8),
      new TinhTrang(12, 9),
    ];
    final dataGiuNguyen = [
      new TinhTrang(1, 18),
      new TinhTrang(2, 22),
      new TinhTrang(3, 20),
      new TinhTrang(4, 19),
      new TinhTrang(5, 16),
      new TinhTrang(6, 15),
      new TinhTrang(7, 16),
      new TinhTrang(8, 15),
      new TinhTrang(9, 14),
      new TinhTrang(10, 17),
      new TinhTrang(11, 18),
      new TinhTrang(12, 19),
    ];

    final dataTienTrien = [
      new TinhTrang(1, 12),
      new TinhTrang(2, 16),
      new TinhTrang(3, 15),
      new TinhTrang(4, 14),
      new TinhTrang(5, 12),
      new TinhTrang(6, 14),
      new TinhTrang(7, 10),
      new TinhTrang(8, 9),
      new TinhTrang(9, 13),
      new TinhTrang(10, 15),
      new TinhTrang(11, 10),
      new TinhTrang(12, 12),
    ];
    return [
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng giữ nguyên',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.blue.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataGiuNguyen,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng xấu đi',
        colorFn: (_, __) => charts.MaterialPalette.deepOrange.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.deepOrange.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataXauDi,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng tiến triển tốt',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.green.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataTienTrien,
      ),
    ];
  }
  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2019() {
    final dataDaThucHien = [
      new TinhTrang(1, 3),
      new TinhTrang(2, 4),
      new TinhTrang(3, 5),
      new TinhTrang(4, 4),
      new TinhTrang(5, 3),
      new TinhTrang(6, 5),
      new TinhTrang(7, 6),
      new TinhTrang(8, 8),
      new TinhTrang(9, 6),
      new TinhTrang(10, 5),
      new TinhTrang(11, 6),
      new TinhTrang(12, 10),
    ];
    return [
      new charts.Series<TinhTrang, int>(
        id: 'Đã thực hiện',
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
    ];
  }
}

enum DateType {
  month,
  quarter,
  half,
  year,
}

class TanSuat {
  final String year;
  final int soLuong;

  TanSuat(this.year, this.soLuong);
}

class TinhTrang {
  final int year;
  final int soLuong;

  TinhTrang(this.year, this.soLuong);
}