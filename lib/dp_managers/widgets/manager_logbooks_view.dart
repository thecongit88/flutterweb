
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_detail_screen.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/form_item_logbook_state.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/forms/form_type_state.dart';
import 'package:hmhapp/widgets/calendar_strip.dart';
import 'package:hmhapp/widgets/i18n_calendar_strip.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ManagerLogBooksView extends StatefulWidget {
  const ManagerLogBooksView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _ManagerLogBooksViewState createState() => _ManagerLogBooksViewState();
}

class _ManagerLogBooksViewState extends State<ManagerLogBooksView>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  late FormTypeState _formTypeState;
  late FormSurveyState _formSurveyState;
  late FormItemLogbookState _formItemLogbookState;
  String firstFormObject = "";

  String formFormatJson = "";
  late DateTime _selectedDate;

  final RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  void _onRefresh() async{
    await Future.delayed(const Duration(milliseconds: 1000));
    _formItemLogbookState.getAllListFormItemLogbookHBCC();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(const Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()

    _refreshController.loadComplete();
  }

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);

    super.initState();

    _formTypeState = FormTypeState();
    _formTypeState.getList();
    _formSurveyState = FormSurveyState();
    _formSurveyState.getListFormsSurveyState("");

    _formItemLogbookState = FormItemLogbookState();
    _formItemLogbookState.getAllListFormItemLogbookHBCC();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    DateTime currentDate = DateTime.now();
    DateTime startDate = DateTime(currentDate.year, 1, 1);
    DateTime endDate = DateTime(currentDate.year, currentDate.month + 2, 1)
        .subtract(const Duration(days: 1));
    DateTime selectedDate = DateTime.now().subtract(const Duration(days: 2));
    List<DateTime> markedDates = [
    ];

    onSelect(data) {
      setState(() {
        _selectedDate = data;
      });

      _formItemLogbookState.setFormDate(_selectedDate.toString());
      _formItemLogbookState.getAllListFormItemLogbookHBCC();
    }

    _monthNameWidget(monthName) {
      return Container(
        child: Text(monthName,
            style: const TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w600,
                color: Colors.black87)),
        padding: const EdgeInsets.only(top: 8, bottom: 4),
      );
    }

    getMarkedIndicatorWidget() {
      return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          margin: const EdgeInsets.only(left: 1, right: 1),
          width: 7,
          height: 7,
          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.red),
        ),
        Container(
          width: 7,
          height: 7,
          decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
        )
      ]);
    }

    dateTileBuilder(date, selectedDate, rowIndex, dayName, isDateMarked,
        isDateOutOfRange) {
      bool isSelectedDate = date.compareTo(selectedDate) == 0;
      Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.black87;
      TextStyle normalStyle = TextStyle(
          fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
      TextStyle selectedStyle = const TextStyle(
          fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black87);
      TextStyle dayNameStyle = TextStyle(fontSize: 14.5, color: fontColor);
      List<Widget> children = [
        Text(dayName, style: dayNameStyle),
        Text(date.day.toString(),
            style: !isSelectedDate ? normalStyle : selectedStyle),
      ];

      if (isDateMarked == true) {
        children.add(getMarkedIndicatorWidget());
      }

      return AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        alignment: Alignment.center,
        padding: const EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
        decoration: BoxDecoration(
          color: !isSelectedDate ? Colors.transparent : Colors.white70,
          borderRadius: const BorderRadius.all(Radius.circular(60)),
        ),
        child: Column(
          children: children,
        ),
      );
    }

    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => _formTypeState),
          ChangeNotifierProvider(create: (_) => _formItemLogbookState),
        ],
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: ManagerAppTheme.nearlyBlue,
            title: const Text("Danh sách khảo sát"),
            elevation: 0,
            /*
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: InkWell(
                  child: Icon(Icons.more_vert),
                  onTap: (){

                  },
                ),
              )
            ],
            */
          ),
          backgroundColor: Colors.transparent,
          body:
          SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              header: const MaterialClassicHeader(),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus? mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = const Text("Cuộn lên để tải");
                  }
                  else if (mode == LoadStatus.loading) {
                    body = const CupertinoActivityIndicator();
                  }
                  else if (mode == LoadStatus.failed) {
                    body = const Text("Tải dữ liệu lỗi! Thử lại!");
                  }
                  else if (mode == LoadStatus.canLoading) {
                    body = const Text("Cuộn để tải thêm");
                  }
                  else {
                    body = const Text("Không có dữ liệu");
                  }
                  return SizedBox(
                    height: 55.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child:
              SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(bottom: 0),
                  child: Column(
                    children: <Widget>[
                      CalendarStrip(
                        selectedDate: DateTime.now(),
                        startDate: startDate,
                        endDate: endDate,
                        onDateSelected: onSelect,
                        dateTileBuilder: dateTileBuilder,
                        iconColor: Colors.black87,
                        monthNameWidget: _monthNameWidget,
                        markedDates: markedDates,
                        containerDecoration: const BoxDecoration(
                            color: Colors.black12),
                        locale: LocaleType.vi,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(
                              top: 0, bottom: 0, right: 16, left: 16),
                          child: Consumer<FormItemLogbookState>(
                              builder: (context, state, child) {
                                return bindListSurveyForm(state, context);
                              })
                      )
                    ],
                  ),
                ),
              )),
        )
    );
  }

  Widget _buildRow(FormItemLogBook formItemLogBook) {
    /*
    String _title = "Chưa nhập ngày đánh giá";
    if(formItemLogBook.formDate != null) {
      DateTime formDate = DateTime.parse(formItemLogBook.formDate);
      _title = "Khảo sát T"+formDate.month.toString() + "/" + formDate.year.toString();
    }
    */

    String title = formItemLogBook.name.toString().isNotEmpty ? formItemLogBook.name ?? "" : "-";

    return ListTile(
      leading: const Icon(Icons.date_range,color: ManagerAppTheme.nearlyBlue,),
      title: Text(title, style: const TextStyle(color: ManagerAppTheme.nearlyBlue)),
      subtitle:
      Padding(
        padding: const EdgeInsets.only(top:6),
        child:
        Text('${formItemLogBook.member!.name ?? ""} / ${formItemLogBook.formDate}',
            style: const TextStyle(fontSize: 13,color: Colors.black54)),
      ),
      trailing: const Icon(Icons.chevron_right,color: ManagerAppTheme.nearlyBlue,),
      onTap: (){
        //print("formSurvey ID");
        //print(formItemLogBook.formDate);

        var detailScreen = LogBookFormDetailScreen(
          //formType: formItemLogBook.formType,
          //memberId: formItemLogBook.memberId,
          //formId: formItemLogBook.formId,
          //member: formItemLogBook.member,
          //formSurveyId: formItemLogBook.id
          formItemLogBook: formItemLogBook
        );
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => detailScreen),
        );
      },
    );
  }

  Widget bindListSurveyForm(FormItemLogbookState state, BuildContext context) {
    switch (state.formItemLogBookResponse.status) {
      case Status.LOADING:
        return Container(
          color: Colors.white30,
          child: const Center(
            child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
          ),
        );
      case Status.COMPLETED:
        return bindListSurveyContent(state.formItemLogBookResponse.data!);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.formItemLogBookResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

  Widget bindListSurveyContent(List<FormItemLogBook> listFormItemLogBook) {
    int lenListSurvey = listFormItemLogBook.length;
    if(lenListSurvey == 0) {
      return const Padding(
        padding: EdgeInsets.all(30.0),
        child: Center(
            child: Text("Chưa có dữ liệu đánh giá!")
        )
      );
    }

    return
          ListView.separated(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            padding: const EdgeInsets.only(
                top: 0, bottom: 0),
            itemCount: lenListSurvey,
            itemBuilder: (BuildContext context, int index) {
              final int count = lenListSurvey > 10
                  ? 10
                  : lenListSurvey;
              final Animation<double> animation =
              Tween<double>(begin: 0.0, end: 1.0).animate(
                  CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn)));
              animationController.forward();

              return _buildRow(listFormItemLogBook[index]);
            },
          )
      ;
  }
}
