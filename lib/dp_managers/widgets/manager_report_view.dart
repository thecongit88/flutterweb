import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/widgets/manager_report_main_view.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:hmhapp/states/report_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:provider/provider.dart';

class ManagerReportView extends StatefulWidget {
  const ManagerReportView({Key? key}) : super(key: key);

  @override
  ManagerReportViewState createState() => ManagerReportViewState();
}

class ManagerReportViewState extends State<ManagerReportView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  late String _year;
  late String _nam;
  late Map<String, num> _soLuong;
  late int _currentSelection;
  late int _currentKehoach;
  final ReportState _reportState = ReportState();

  final Map<int, Widget> _children = {
    0: const Text('Hình tròn'),
    1: const Text('Cột')
  };

  List<DropdownMenuItem<String>> listYears = <DropdownMenuItem<String>>[];


  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _currentSelection = 0;
    _currentKehoach = 0;
    _reportState.getTotalMembers();
    _reportState.getMaleMembers();
    _reportState.getFeMaleMembers();
    _reportState.getDaCamMembers();
    for(var i= DateTime.now().year; i >= 2019;i--){
      var it = DropdownMenuItem<String>(
        value: '$i',
        child: Text('$i'),
      );
      listYears.add(it);
    }
    _year = DateTime.now().year.toString();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceOrientation = MediaQuery.of(context).orientation;

    List<Widget> childWidgets = <Widget>[];
    childWidgets.add(wdgBCTanSuat());
    childWidgets.add(wdgBCTinhTrang());


    return Scaffold(
      backgroundColor: Colors.black12,
      appBar: AppBar(
        backgroundColor: ManagerAppTheme.nearlyBlue,
        title: const Text("Báo cáo"),
        elevation: 0,
      ),
      body: ChangeNotifierProvider(
        create: (_) => _reportState,
        child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Card(
                  elevation:1,
                  margin: const EdgeInsets.fromLTRB(10,10,10,0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  child:
                  Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                        ),
                      ),
                      padding: const EdgeInsets.all(10),
                      child:
                      InkWell(
                        child:
                        Row(
                          children: const <Widget>[
                            Icon(Icons.pie_chart_outline,
                              color: ManagerAppTheme.nearlyBlue,
                              size: 30,),
                            SizedBox(width: 10,),
                            Text("BC % thực hiện theo kế hoạch", style: TextStyle(fontSize: 16)),
                            Spacer(),
                            Icon(Icons.arrow_forward_ios, size: 15,)
                          ],
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ManagerReportMainView(
                              reportType: Constants.MANAGER_REPORT_PLAN_PERCENT_PIE,
                            ),
                          ));
                        },
                      )
                  ),
                ),
                Card(
                  elevation:1,
                  margin: const EdgeInsets.fromLTRB(10,10,10,0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  child:
                  Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                        ),
                      ),
                      padding: const EdgeInsets.all(10),
                      child:
                      InkWell(
                        child:
                        Row(
                          children: const <Widget>[
                            Icon(Icons.insert_chart,
                              color: ManagerAppTheme.nearlyBlue,
                              size: 30,),
                            SizedBox(width: 10,),
                            Text("BC so sánh % thực hiện kế hoạch", style: TextStyle(fontSize: 16)),
                            Spacer(),
                            Icon(Icons.arrow_forward_ios, size: 15,)
                          ],
                        ),
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ManagerReportMainView(
                              reportType: Constants.MANAGER_REPORT_PLAN_PERCENT_COLUMN,
                            ),
                          ));
                        },
                      )
                  ),
                ),
                Card(
                  elevation:1,
                  margin: const EdgeInsets.fromLTRB(10,10,10,0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  child:
                  Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                        ),
                      ),
                      padding: const EdgeInsets.all(10),
                      child:
                      InkWell(
                        child:
                        Row(
                          children: const <Widget>[
                            Icon(Icons.show_chart,
                              color: ManagerAppTheme.nearlyBlue,
                              size: 30,),
                            SizedBox(width: 10,),
                            Text("BC tiến bộ lâm sàng", style: TextStyle(fontSize: 16),),
                            Spacer(),
                            Icon(Icons.arrow_forward_ios, size: 15,)
                          ],
                        ),
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ManagerReportMainView(
                              reportType: Constants.MANAGER_REPORT_COMPARE_STATE_LINE,
                            ),
                          ));
                        },
                      )
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.fromLTRB(15, 30, 10, 0),
                  child: Text("Thống kê", style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),),
                ),
                Container(
                  child: GridView.count(
                    crossAxisCount: 2,
                    childAspectRatio: 1.35,
                    padding: const EdgeInsets.all(4.0),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    shrinkWrap: true,
                    children: <Widget>[
                      Card(
                        elevation:1,
                        child:
                        Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                              ),
                            ),
                            padding: const EdgeInsets.all(10),
                            child:
                            SingleChildScrollView(
                              child:
                              Column(
                                children: <Widget>[
                                   Icon(Icons.accessible,
                                    color: ManagerAppTheme.nearlyBlue,
                                    size: 35,),
                                  const SizedBox(height: 5,),
                                  Consumer<ReportState>(
                                    builder: (context, state, child) {
                                      return wdgTotalMember(state.totalMembersResponse);
                                    },
                                  ),
                                  //Text("30", style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack),),
                                  const SizedBox(height: 5,),
                                  const Text("Số lượng NKT"),
                                ],
                              ),
                            )
                        ),
                        margin: const EdgeInsets.fromLTRB(10,10,10,0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      Card(
                        elevation:1,
                        child:
                        Container(
                            decoration: BoxDecoration(
                              border: Border(
                                left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                              ),
                            ),
                            padding: const EdgeInsets.all(10),
                            child:
                            SingleChildScrollView(
                              child:
                              Column(
                                children: <Widget>[
                                  const Icon(Icons.access_time,
                                    color: ManagerAppTheme.nearlyBlue,
                                    size: 35,),
                                  const SizedBox(height: 5,),
                                  //Text("19", style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack),),
                                  Consumer<ReportState>(
                                    builder: (context, state, child) {
                                      return wdgDacamMember(state.dacamMembersResponse);
                                    },
                                  ),
                                  const SizedBox(height: 5,),
                                  const Text("NKT da cam"),
                                ],
                              ),
                            )
                        ),
                        margin: const EdgeInsets.fromLTRB(10,10,10,0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      Card(
                        elevation:1,
                        child:
                        Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                              ),
                            ),
                            padding: EdgeInsets.all(10),
                            child:
                            SingleChildScrollView(
                              child:
                              Column(
                                children: <Widget>[
                                  Icon(Icons.people_outline,
                                    color: ManagerAppTheme.nearlyBlue,
                                    size: 35,),
                                  SizedBox(height: 5,),
                                  //Text("14", style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack),),
                                  Consumer<ReportState>(
                                    builder: (context, state, child) {
                                      return wdgMaleMember(state.maleMembersResponse);
                                    },
                                  ),
                                  SizedBox(height: 5,),
                                  Text("Nữ giới"),
                                ],
                              ),
                            )
                        ),
                        margin: EdgeInsets.fromLTRB(10,10,10,0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                      Card(
                        elevation:1,
                        child:
                        Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                left: BorderSide(width: 5.0, color: ManagerAppTheme.nearlyBlue),
                              ),
                            ),
                            padding: EdgeInsets.all(10),
                            child: SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  Icon(Icons.people,
                                    color: ManagerAppTheme.nearlyBlue,
                                    size: 35,),
                                  SizedBox(height: 5,),
                                  //Text("16", style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack),),
                                  Consumer<ReportState>(
                                    builder: (context, state, child) {
                                      return wdgFemaleMember(state.femaleMembersResponse);
                                    },
                                  ),
                                  SizedBox(height: 5,),
                                  Text("Nam giới"),
                                ],
                              ),
                            )
                        ),
                        margin: EdgeInsets.fromLTRB(10,10,10,0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            ))
      )
    );
  }

  Widget wdgBCTanSuat(){
    List<Widget> _childKeHoachWidgets = <Widget>[];
    _childKeHoachWidgets.add(wdgBCKehoachCircle());
    _childKeHoachWidgets.add(wdgBCKeHoachColumn());

    final children =
    SizedBox(
      child: Column(
      children: <Widget>[
        SizedBox(
          child:
          MaterialSegmentedControl(
            children: _children,
            verticalOffset: 10,
            borderColor: Colors.lightBlue,
            selectedColor: Colors.lightBlue,
            unselectedColor: Colors.white,
            selectionIndex: _currentKehoach,
            borderRadius: 20.0,
            onSegmentChosen: (int? index) {
              setState(() {
                _currentKehoach = index ?? 0;
              });
            },
          ),
          width: double.infinity,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(20,0,20,50),
          child:
          new Container(child: _childKeHoachWidgets[_currentKehoach]),
        )
      ],
    ),);
    return children;
  }

  Widget wdgBCKeHoachColumn(){
    final children =
    SizedBox(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text("Năm"),
              Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
                child:
                DropdownButton<String>(
                  value: _year,
                  items: listYears,
                  isExpanded: false,
                  onChanged: (String? value) {
                    setState(() {
                      _year = value ?? "";
                    });
                  },
                ),
              ),
            ],
          ),
          new SizedBox(
            height: 400.0,
            child: charts.BarChart(
              _year=="2020"?
              _sampleData2020():_sampleData2019(),
              animate: true,
              barGroupingType: charts.BarGroupingType.stacked,
              behaviors: [
                new charts.PercentInjector(totalType: charts.PercentInjectorTotalType.domain)
              ,new charts.SeriesLegend(
                  showMeasures: true,
                  measureFormatter: (num? value) {
                    return value == null ? '-' : '${(value*100).toStringAsFixed(1)}';
                  },
                )
              ],
              primaryMeasureAxis: new charts.PercentAxisSpec(),
            ),
          ),
          SizedBox(height: 10,),
          Text(
            "Báo cáo so sánh % thực hiện kế hoạch $_year",
            textAlign: TextAlign.center,
          ),
        ],
      ),);
    return children;
  }

  Widget wdgBCKehoachCircle(){
    final children =
    SizedBox(
      child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text("Chọn năm báo cáo"),
                Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
                  child: DropdownButton<String>(
                    value: _year,
                    items: listYears,
                    isExpanded: false,
                    onChanged: (String? value) {
                      setState(() {
                        _year = value ?? "";
                      });
                    },
                  ),
                ),
              ],
            ),
            new SizedBox(
              height: 400.0,
              child: charts.PieChart(
                _sampleDataPie2020(),
                  animate: false,
                  behaviors: [new charts.DatumLegend()],
                  defaultRenderer: new charts.ArcRendererConfig(arcRendererDecorators: [
                new charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.auto)

              ])
              ),
            ),
            SizedBox(height: 10,),
            Text(
              "Báo cáo % thực hiện kế hoạch $_year",
              textAlign: TextAlign.center,
            ),
          ]),);
    return children;
  }

  Widget wdgBCTinhTrang(){
    final children =
    SizedBox(
        child: Column(
        children: <Widget>[
      Row(
        children: <Widget>[
          Text("Chọn năm báo cáo"),
          Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
            child: DropdownButton<String>(
              value: _year,
              items: listYears,
              isExpanded: false,
              onChanged: (String? value) {
                setState(() {
                  _year = value ?? "";
                });
              },
            ),
          ),
        ],
      ),
      new SizedBox(
        height: 400.0,
        child: charts.LineChart(
          _year=="2020"?
          _sampleDataTinhTrang2020():_sampleDataTinhTrang2019(),
          animate: false,
            defaultRenderer: new charts.LineRendererConfig(
                includePoints: true,
            )

        ),
      ),
          SizedBox(height: 10,),
      Text(
        "Báo cáo tình trạng bệnh nhân năm $_year",
        textAlign: TextAlign.center,
      ),
    ]),);
    return children;
  }

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    String nam = "";
    final soluong = <String, num>{};

    // We get the model that updated with a list of [SeriesDatum] which is
    // simply a pair of series & datum.
    //
    // Walk the selection updating the measures map, storing off the sales and
    // series name for each selection point.
    if (selectedDatum.isNotEmpty) {
       nam = selectedDatum.first.datum.year;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        soluong[datumPair.series.displayName!] = datumPair.datum.soLuong;
      });
    }

    // Request a build.
    setState(() {
      _nam = nam;
      _soLuong = soluong;
    });
  }

  static List<charts.Series<TanSuat, String>> _sampleData2020() {
    final dataDaThucHien = [
      new TanSuat('T1', 8),
      new TanSuat('T2', 12),
      new TanSuat('T3', 10),
      new TanSuat('T4', 9),
      new TanSuat('T5', 6),
      new TanSuat('T6', 5),
      new TanSuat('T7', 6),
      new TanSuat('T8', 5),
      new TanSuat('T9', 4),
      new TanSuat('T10', 7),
      new TanSuat('T11', 8),
      new TanSuat('T12', 9),
    ];
    final dataKeHoach = [
      new TanSuat('T1', 8),
      new TanSuat('T2', 15),
      new TanSuat('T3', 12),
      new TanSuat('T4', 8),
      new TanSuat('T5', 5),
      new TanSuat('T6', 5),
      new TanSuat('T7', 9),
      new TanSuat('T8', 6),
      new TanSuat('T9', 6),
      new TanSuat('T10', 9),
      new TanSuat('T11', 4),
      new TanSuat('T12', 9),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }

  static List<charts.Series<TanSuat, String>> _sampleDataPie2020() {
    final dataDaThucHien = [
      new TanSuat('Theo kế hoạch', 30),
      new TanSuat('Không theo kế hoạch', 70),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
        labelAccessorFn: (TanSuat row, _) => '${row.soLuong}%',
      )
    ];
  }
  static List<charts.Series<TanSuat, String>> _sampleData2019() {
    final dataDaThucHien = [
      new TanSuat('T1', 3),
      new TanSuat('T2', 4),
      new TanSuat('T3', 5),
      new TanSuat('T4', 4),
      new TanSuat('T5', 3),
      new TanSuat('T6', 5),
      new TanSuat('T7', 6),
      new TanSuat('T8', 8),
      new TanSuat('T9', 6),
      new TanSuat('T10', 5),
      new TanSuat('T11', 6),
      new TanSuat('T12', 10),
    ];
    final dataKeHoach = [
      new TanSuat('T1', 5),
      new TanSuat('T2', 5),
      new TanSuat('T3', 10),
      new TanSuat('T4', 6),
      new TanSuat('T5', 9),
      new TanSuat('T6', 4),
      new TanSuat('T7', 6),
      new TanSuat('T8', 9),
      new TanSuat('T9', 5),
      new TanSuat('T10', 7),
      new TanSuat('T11', 4),
      new TanSuat('T12', 9),
    ];
    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }

  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2020() {
    final dataXauDi = [
      new TinhTrang(1, 8),
      new TinhTrang(2, 12),
      new TinhTrang(3, 10),
      new TinhTrang(4, 9),
      new TinhTrang(5, 6),
      new TinhTrang(6, 5),
      new TinhTrang(7, 6),
      new TinhTrang(8, 5),
      new TinhTrang(9, 4),
      new TinhTrang(10, 7),
      new TinhTrang(11, 8),
      new TinhTrang(12, 9),
    ];
    final dataGiuNguyen = [
      new TinhTrang(1, 18),
      new TinhTrang(2, 22),
      new TinhTrang(3, 20),
      new TinhTrang(4, 19),
      new TinhTrang(5, 16),
      new TinhTrang(6, 15),
      new TinhTrang(7, 16),
      new TinhTrang(8, 15),
      new TinhTrang(9, 14),
      new TinhTrang(10, 17),
      new TinhTrang(11, 18),
      new TinhTrang(12, 19),
    ];

    final dataTienTrien = [
      new TinhTrang(1, 12),
      new TinhTrang(2, 16),
      new TinhTrang(3, 15),
      new TinhTrang(4, 14),
      new TinhTrang(5, 12),
      new TinhTrang(6, 14),
      new TinhTrang(7, 10),
      new TinhTrang(8, 9),
      new TinhTrang(9, 13),
      new TinhTrang(10, 15),
      new TinhTrang(11, 10),
      new TinhTrang(12, 12),
    ];
    return [
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng giữ nguyên',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.blue.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataGiuNguyen,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng xấu đi',
        colorFn: (_, __) => charts.MaterialPalette.deepOrange.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.deepOrange.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataXauDi,
      ),
      new charts.Series<TinhTrang, int>(
        id: 'Tình trạng tiến triển tốt',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        areaColorFn: (_, __) =>
        charts.MaterialPalette.green.shadeDefault.lighter,
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataTienTrien,
      ),
    ];
  }
  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2019() {
    final dataDaThucHien = [
      new TinhTrang(1, 3),
      new TinhTrang(2, 4),
      new TinhTrang(3, 5),
      new TinhTrang(4, 4),
      new TinhTrang(5, 3),
      new TinhTrang(6, 5),
      new TinhTrang(7, 6),
      new TinhTrang(8, 8),
      new TinhTrang(9, 6),
      new TinhTrang(10, 5),
      new TinhTrang(11, 6),
      new TinhTrang(12, 10),
    ];
    return [
      new charts.Series<TinhTrang, int>(
        id: 'Đã thực hiện',
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
    ];
  }

  Widget wdgTotalMember(ApiResponse<int> totalMembersResponse) {
    switch(totalMembersResponse.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: totalMembersResponse.message,
        );
      case Status.COMPLETED:
        return Text(totalMembersResponse.data.toString(), style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack));
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: totalMembersResponse.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgMaleMember(ApiResponse<int> maleMembersResponse) {
    switch(maleMembersResponse.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: maleMembersResponse.message,
        );
      case Status.COMPLETED:
        return Text(maleMembersResponse.data.toString(), style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack));
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: maleMembersResponse.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgFemaleMember(ApiResponse<int> femaleMembersResponse) {
    switch(femaleMembersResponse.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: femaleMembersResponse.message,
        );
      case Status.COMPLETED:
        return Text(femaleMembersResponse.data.toString(), style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack));
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: femaleMembersResponse.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgDacamMember(ApiResponse<int> dacamMembersResponse) {
    switch(dacamMembersResponse.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: dacamMembersResponse.message,
        );
      case Status.COMPLETED:
        return Text(dacamMembersResponse.data.toString(), style: TextStyle(fontSize: 30,fontWeight: FontWeight.w500, color: ManagerAppTheme.nearlyBlack));
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: dacamMembersResponse.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

}

class TanSuat {
  final String year;
  final int soLuong;

  TanSuat(this.year, this.soLuong);
}

class TinhTrang {
  final int year;
  final int soLuong;

  TinhTrang(this.year, this.soLuong);
}