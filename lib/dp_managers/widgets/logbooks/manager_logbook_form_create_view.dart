import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/report_thuchien_kehoach.dart';
import 'package:hmhapp/models/report_tinhtrang_nkt.dart';
import 'package:hmhapp/models/report_tinhtrangnkt_hbcc.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/report_thuchien_kehoach_state.dart';
import 'package:hmhapp/states/report_tinhtrang_nkt_state.dart';
import 'package:hmhapp/states/report_tinhtrangnkt_hbcc_state.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:another_flushbar/flushbar.dart';

class LogBookFormCreateView extends StatefulWidget {
  final Member? member;
  final FormType formType;

  const LogBookFormCreateView({Key? key,required this.formType, this.member}) : super(key: key);
  @override
  _LogBookFormCreateScreenState createState() => _LogBookFormCreateScreenState();
}

class _LogBookFormCreateScreenState extends State<LogBookFormCreateView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();

  late FormItemLogBook _itemLogBook;
  late FormTypeState _formTypeState;
  late FormSurveyState _formSurveyState;

  dynamic response;
  late Map forma;

  late int hbccId;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  DateTime _formDate = DateTime.now();
  var formatStr = "dd-MM-yyyy";
  int _formUser = 0;
  late bool _formUserValidate;
  String _dateLabel = "Chọn ngày";

  /* Hide member dropdown */
  /*
  List<DropdownMenuItem<int>> _listMembers;
  DropdownMenuItem<int> ddlMenuItemDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn NKT được đánh giá"),
  );
  */

  MemberServices _memberService = new MemberServices();
  Map<String, dynamic> map = new Map<String, dynamic>();
  bool showHoanThanh = true, showError = true;
  ReportTinhTrangNKTState reportTinhTrangNKTState = new ReportTinhTrangNKTState();
  ReportTinhtrangnktHbccState reportTinhTrangNKTHbccState = new ReportTinhtrangnktHbccState();
  ReportThuchienKehoachState reportThuchienKehoachState = new ReportThuchienKehoachState();
  String errMsgDatao = "", errMsgThoiGian = "";

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    /* Hide member dropdown */
    /*
    _listMembers = new List<DropdownMenuItem<int>>();
    _listMembers.add(ddlMenuItemDefault);
    */

    _formUser = widget.member != null ? widget.member!.id ?? 0 : 0;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();

    /* Hide member dropdown */
    /*
    _memberService.listMembers().then((snapshot) {
      snapshot.forEach((_member) {
        _listMembers.add(new DropdownMenuItem<int>(
          value: _member.id,
          child: new Text(_member.name),
        ));
      });
    });
    */

    _formTypeState = new FormTypeState();
    _formSurveyState = new FormSurveyState();

    _itemLogBook = new FormItemLogBook(
      formDate: DateTime.now().toString(),
      formTypeId: widget.formType.id,
      name: widget.formType.formName,
    );

    super.initState();
    _formUserValidate = false;
    getSignedHbccId();

    showHoanThanh = true;
    showError = false;

    /* mỗi tháng chỉ được tạo đánh giá 1 lần */
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      int countSurveyExcuted = await _formSurveyState
          .countSurveyExcuted(formDate: _formDate,
          memberId: _formUser,
          formName: widget.formType.formObject);
      if (int.parse(countSurveyExcuted.toString()) > 0) {
        setState(() {
          showHoanThanh = false;
          showError = true;
        });
        //Functions.showSnackBarMsg(context, 'Xin lỗi, bạn đã tạo đánh giá tháng của NKT vừa chọn!');
        errMsgDatao = 'Báo lỗi, Bạn đã tạo đánh giá của tháng này.';
        return;
      } else {
        /* chỉ được sửa form từ 26 tháng hiện tại đến hết mùng 5 tháng kế tiếp */
        if(DateTime.now().day >= 26 || DateTime.now().day <= 5) {
          setState(() {
            showHoanThanh = true;
            showError = false;
          });
        } else {
          setState(() {
            showHoanThanh = false;
            showError = true;
          });
          //Functions.showSnackBarMsg(context, Functions.timeRangeAllowCreateSurvey());
          errMsgThoiGian = Functions.timeRangeAllowCreateSurvey();
        }
      }
    });
    /* end mỗi tháng chỉ được tạo đánh giá 1 lần */
  }


  @override
  Widget build(BuildContext ctx) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => _formTypeState),
          ChangeNotifierProvider(create: (_) => _formSurveyState),
        ],
        child: new Container(
          color: Colors.white,
          child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            Consumer<FormTypeState>(
                builder: (context, state, child) {
                  return Padding(
                      padding: const EdgeInsets.only(
                          left: 1, right: 1, top: 15),
                      child:
                      widget.formType.formFormat != null &&
                          widget.formType.formFormat!.isNotEmpty ?
                      wdgFormLogbook(widget.formType.formFormat!) :
                      Center(
                        child: Text('Mẫu đánh giá chưa được cấu hình.',
                          style: TextStyle(color: Colors.red),),
                      )
                  );
                }),
            Consumer<FormSurveyState>(
                builder: (context, state, child) {
                  switch (state.formResponse.status) {
                    case Status.LOADING:
                      return
                        Container(
                          color: Colors.white30,
                          child:
                          Center(
                            child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Colors.green)),
                          ),
                        );
                      break;
                    case Status.COMPLETED:
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        return planSuccess();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return planSuccess();
                            }).then((shouldUpdate) {
                          Navigator.of(context, rootNavigator: true).pop(true);
                        });
                      });
                      return SizedBox();
                      break;
                    case Status.ERROR:
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        return planChange();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return planChange();
                            });
                      });
                      return SizedBox();
                      break;
                    case Status.INIT:
                      return SizedBox();
                      break;
                    default:
                      return SizedBox();
                      break;
                  }
                }),
          ]),
        )
    );
  }

  Widget wdgFormLogbook(Map data) {
    print("da: "+ data.toString());
    var fields = data["fields"];
    String value;

    (fields as List<dynamic>).forEach((item) {
      value = item["value"].toString();
      if (item["type"].toString() == "RadioButton") {
        if (value == "true" || value == "false")
          value = value == "true" ? "1" : "0";
        else
          value = value.toString();
      }
      item["value"] = value;
    });
    data["fields"] = fields;
    data["form_date"] = _formDate.toString();

    return showError == true ?
          Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                errMsgDatao != "" ? Text(
                  errMsgDatao,
                  textAlign: TextAlign.left,
                  style:
                  TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.redAccent),
                ) : SizedBox(),
                SizedBox(height: 10.0,),
                Divider(color: Colors.grey,),
                SizedBox(height: 10.0,),
                errMsgThoiGian != "" ? Text(
                  errMsgThoiGian,
                  textAlign: TextAlign.left,
                  style:
                  TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ) : SizedBox(),
                SizedBox(height: 10.0,),
                Text(
                  'Họ tên NKT: ${widget.member!.name ?? ""}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          )
        :
        JsonSchema(
          formMap: data,
          onChanged: (dynamic response) {
            this.response = response;
            print(response);
          },
          actionSave: (data) async {
            int DiemKetLuan = 0, theoKeHoach = 0, koTheoKeHoach = 0, scoreVal = 0;
            String scoreMessage = "";

            if (_formUser > 0) {
              if (showHoanThanh == false) return;
              /* mỗi tháng chỉ được tạo đánh giá 1 lần */
              //print(data);
              var fields = data["fields"];
              //print(fields);
              String objText = '{';
              fields.forEach((field) {

                objText += '"${field["key"]}":${field["value"].toString()},';
                map["${field["key"]}"] = field["value"].toString();

                if (field["key"] == "KetLuanTinhTrang") {
                  /* Chú ý: với kết luận có thể là Độ 1, Độ 2, ... -> cần tách lấy phần số trong chuỗi để làm điểm kết luận */
                  if(field["value"] == null || field["value"] == "") DiemKetLuan = 0;
                  else DiemKetLuan = int.parse(field["value"].toString().replaceAll(RegExp('[^0-9]'), ''));
                  /* else DiemKetLuan = int.parse(field["value"].toString()); */
                }

                if (field["key"] == "chamsoc_duatren_kehoach") {
                  if(int.parse(field["value"].toString()) == 1) theoKeHoach = 1;
                  if(int.parse(field["value"].toString()) == 0) koTheoKeHoach = 1;
                }

                if (field["key"] == "ketluan_score") {
                  var resultItems = field["result_items"];
                  int n = resultItems.length;
                  if(resultItems != null && n > 0) {
                    for(int i=0; i<n; i++){
                      if(field["value"].toString().toLowerCase().trim() == resultItems[i]["label"].toString().toLowerCase().trim()) {
                        scoreVal = int.parse(resultItems[i]["value"].toString());
                        break;
                      }
                    }
                  }
                  scoreMessage = field["value"].toString();
                }
              });
              objText += '}';

              var obj = json.encode(objText);

              map["form_type"] = new FormType()
                ..id = widget.formType.id;
              map["member"] = _formUser;
              map["form_date"] = _formDate.toString();

              _itemLogBook.formDate = _formDate.toString();
              _itemLogBook.memberId = _formUser;
              _itemLogBook.formTypeId = widget.formType.id;
              _itemLogBook.formType = widget.formType;
              _itemLogBook.member = new Member(id: _formUser);
              _itemLogBook.hbcc = new HBCC(id: hbccId);
              _formSurveyState.create(
                  map, _itemLogBook, widget.formType.formObject!);

              /*
              Vấn đề còn tồn lại: Đã có đầu ra chưa có đầu vào:
              HBCC: Báo cáo % thực hiện theo kế hoạch - Dựa vào Đánh giá chung NCS, tiêu chí có thực  hiện đúng theo kế hoạch ko để tăng số lượng
              HBCC: BC so sánh % thực hiện kế hoạch - Đã có trong bảng Report_tansuat_ncs
              HBCC - Member: Báo cáo Tình Trạng trong phần App NCS/NKT/Member
              * */

              //01: cập nhật dữ liệu cho bảng Report_thuchien_kehoach, chỉ dùng trong đánh giá formType.id == 1: Đánh giá chung NCS
              if (widget.formType.id == 1) {
                await UpdateReportPhanTramThucHienKeHoach(koTheoKeHoach, theoKeHoach);
              }

              //02: fixcode: cập nhật dữ liệu cho bảng Report_tinhtrang_nkt (người chăm sóc)
              if (widget.formType.id == 3) {
                await UpdateReportTinhTrangNktNcs(scoreVal, scoreMessage);
              }

              //03: cập nhật dữ liệu cho bảng Report_tinhtrangnkt_hbcc, chỉ dùng trong đánh giá formType.id == 4: Đánh giá tiến bộ lâm sàng
              if (widget.formType.id == 4) {
                await UpdateReportTinhTrangNktHbcc(DiemKetLuan);
              }
            } else {
              setState(() {
                _formUserValidate = true;
              });
            }
          },
          selectUsers: wdgNKT(),
          chooseDate: wdgThoiGian(),
          buttonSave:
            showHoanThanh == true ?
            Container(
            child: Center(
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 0, bottom: 25, top: 16, right: 0),
                  child:
                  InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                              color: ManagerAppTheme.nearlyBlue,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: ManagerAppTheme
                                        .nearlyBlue
                                        .withOpacity(0.5),
                                    offset: const Offset(1.1, 1.1),
                                    blurRadius: 10.0),
                              ],
                            ),
                            child: Center(
                              child: Text(
                                'Hoàn thành',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  letterSpacing: 0.0,
                                  color:
                                  ManagerAppTheme.nearlyWhite,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
              ),
            ),
          )
              :
            Container(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Center(
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 0, bottom: 25, top: 16, right: 0),
                  child: Expanded(
                    child: Container(
                      height: 48,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16.0),
                        ),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey
                                  .withOpacity(0.5),
                              offset: const Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Hoàn thành',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            letterSpacing: 0.0,
                            color: ManagerAppTheme.nearlyWhite,
                          ),
                        ),
                      ),
                    ),
                  )),
            ),
          ),
    );
  }


  Widget wdgNKT() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 0.0),
        child:
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Đánh giá tại nhà: ${widget.member!.name ?? ""}',
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.bold),
            ),

            /* Hide member dropdown */
            /*
                  DropdownButtonFormField<int>(
                    isExpanded: true,
                    isDense: true,
                    items: _listMembers,
                    value: _formUser,
                    onChanged: (value) async {
                      setState(() {
                        _formUser = value;
                        if (value > 0) {
                          _formUserValidate = false;
                        } else {
                          _formUserValidate = true;
                        }
                      });

                      /* mỗi tháng chỉ được tạo đánh giá 1 lần */
                      int countSurveyExcuted = await _formSurveyState
                          .countSurveyExcuted(formDate: _formDate,
                          memberId: _formUser,
                          formName: widget.formType.formObject);
                      if (countSurveyExcuted > 0) {
                        setState(() {
                          showHoanThanh = false;
                        });

                        final snackBar = SnackBar(
                          content: Text(
                              'Xin lỗi, bạn đã tạo đánh giá tháng của NKT vừa chọn!'),
                          backgroundColor: Colors.red,
                          duration: Duration(seconds: 2),
                        );
                        Scaffold.of(context).showSnackBar(snackBar);
                        return;
                      } else {
                        setState(() {
                          showHoanThanh = true;
                        });
                      }
                    },
                  ),
                  */

            _formUserValidate == false
                ? SizedBox.shrink()
                : Padding(
              padding: EdgeInsets.only(top: 10),
              child: Text("Bạn cần chọn Người khuyết tật trước khi lưu.",
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        ),
      );

    return FutureBuilder<List<Member>>(
      future: _memberService.listMembers(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return new Container();
        } else if (snapshot.hasData) {
          /* Hide member dropdown */
          /*
          _listMembers.clear();
          _listMembers.add(ddlMenuItemDefault);
          snapshot.data.forEach((_member) {
            _listMembers.add(new DropdownMenuItem<int>(
              value: _member.id,
              child: new Text(_member.name),
            ));
          });
         */
          //return ...
        } else {
          return LoadingMessage(
            loadingMessage: "Đang tải dữ liệu",
          );
        }
      },
    );
  }

  Widget wdgThoiGian() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Thời gian thực hiện:',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm $date');
                      _formDate = date;
                      setState(() {
                        _dateLabel = new DateFormat(formatStr)
                            .format(_formDate)
                            .toString();
                        map['form_date'] = _formDate.toString();
                      });
                    },
                    currentTime: DateTime.now(),
                    locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            )
          ],
        ),);
  }

  planSuccess() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        icon: Icon(Icons.check_circle,color: Colors.white,),
        duration: Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.red,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.report,color: Colors.white,),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = (await preferences.getInt('hbccId'))!;
  }

  Future<void> UpdateReportTinhTrangNktHbcc(int diemKetLuan) async {
    await reportTinhTrangNKTHbccState.getListReportTinhtrangnktHbcc(
        month: _formDate.month, year: _formDate.year);
    List<
        ReportTinhTrangNktHbcc> repTinhTrangNktHbcc = reportTinhTrangNKTHbccState
        .repTinhTrangNktHbcc;
    var itemReportTinhTrangNktHbcc = new ReportTinhTrangNktHbcc();
    print(repTinhTrangNktHbcc.length);
    if (repTinhTrangNktHbcc.length > 0) {
      //cập nhật
      itemReportTinhTrangNktHbcc.id = repTinhTrangNktHbcc[0].id;
      itemReportTinhTrangNktHbcc.month = repTinhTrangNktHbcc[0].month;
      itemReportTinhTrangNktHbcc.year = repTinhTrangNktHbcc[0].year;
      switch (diemKetLuan) {
        case 1:
          {
            //giữ nguyên
            itemReportTinhTrangNktHbcc.count_xauhon =
                repTinhTrangNktHbcc[0].count_xauhon;
            itemReportTinhTrangNktHbcc.count_giunguyen =
                repTinhTrangNktHbcc[0].count_giunguyen! + 1;
            itemReportTinhTrangNktHbcc.count_caithienit =
                repTinhTrangNktHbcc[0].count_caithienit;
            itemReportTinhTrangNktHbcc.count_caithienvua =
                repTinhTrangNktHbcc[0].count_caithienvua;
            itemReportTinhTrangNktHbcc.count_caithiennhieu =
                repTinhTrangNktHbcc[0].count_caithiennhieu;
          }
          break;

        case 2:
          {
            //cải thiện ít
            itemReportTinhTrangNktHbcc.count_xauhon =
                repTinhTrangNktHbcc[0].count_xauhon;
            itemReportTinhTrangNktHbcc.count_giunguyen =
                repTinhTrangNktHbcc[0].count_giunguyen;
            itemReportTinhTrangNktHbcc.count_caithienit =
                repTinhTrangNktHbcc[0].count_caithienit! + 1;
            itemReportTinhTrangNktHbcc.count_caithienvua =
                repTinhTrangNktHbcc[0].count_caithienvua;
            itemReportTinhTrangNktHbcc.count_caithiennhieu =
                repTinhTrangNktHbcc[0].count_caithiennhieu;
          }
          break;

        case 3:
          {
            //cải thiện vừa
            itemReportTinhTrangNktHbcc.count_xauhon =
                repTinhTrangNktHbcc[0].count_xauhon;
            itemReportTinhTrangNktHbcc.count_giunguyen =
                repTinhTrangNktHbcc[0].count_giunguyen;
            itemReportTinhTrangNktHbcc.count_caithienit =
                repTinhTrangNktHbcc[0].count_caithienit;
            itemReportTinhTrangNktHbcc.count_caithienvua =
                repTinhTrangNktHbcc[0].count_caithienvua! + 1;
            itemReportTinhTrangNktHbcc.count_caithiennhieu =
                repTinhTrangNktHbcc[0].count_caithiennhieu;
          }
          break;

        case 4:
          {
            //cải thiện nhiều
            itemReportTinhTrangNktHbcc.count_xauhon =
                repTinhTrangNktHbcc[0].count_xauhon;
            itemReportTinhTrangNktHbcc.count_giunguyen =
                repTinhTrangNktHbcc[0].count_giunguyen;
            itemReportTinhTrangNktHbcc.count_caithienit =
                repTinhTrangNktHbcc[0].count_caithienit;
            itemReportTinhTrangNktHbcc.count_caithienvua =
                repTinhTrangNktHbcc[0].count_caithienvua;
            itemReportTinhTrangNktHbcc.count_caithiennhieu =
                repTinhTrangNktHbcc[0].count_caithiennhieu! + 1;
          }
          break;

        default:
          {
            itemReportTinhTrangNktHbcc.count_xauhon =
                repTinhTrangNktHbcc[0].count_xauhon;
            itemReportTinhTrangNktHbcc.count_giunguyen =
                repTinhTrangNktHbcc[0].count_giunguyen;
            itemReportTinhTrangNktHbcc.count_caithienit =
                repTinhTrangNktHbcc[0].count_caithienit;
            itemReportTinhTrangNktHbcc.count_caithienvua =
                repTinhTrangNktHbcc[0].count_caithienvua;
            itemReportTinhTrangNktHbcc.count_caithiennhieu =
                repTinhTrangNktHbcc[0].count_caithiennhieu;
          }
          break;
      }
      itemReportTinhTrangNktHbcc.hbccId = repTinhTrangNktHbcc[0].hbccId;
      await reportTinhTrangNKTHbccState.update(itemReportTinhTrangNktHbcc);
    } else {
      //thêm mới
      itemReportTinhTrangNktHbcc.month = _formDate.month;
      itemReportTinhTrangNktHbcc.year = _formDate.year;
      //itemReportTinhTrangNktHbcc.hbccId = this.hbccId;
      switch (diemKetLuan) {
        case 1:
          {
            //giữ nguyên
            itemReportTinhTrangNktHbcc.count_xauhon = 0;
            itemReportTinhTrangNktHbcc.count_giunguyen = 1;
            itemReportTinhTrangNktHbcc.count_caithienit = 0;
            itemReportTinhTrangNktHbcc.count_caithienvua = 0;
            itemReportTinhTrangNktHbcc.count_caithiennhieu = 0;
          }
          break;

        case 2:
          {
            //cải thiện ít
            itemReportTinhTrangNktHbcc.count_xauhon = 0;
            itemReportTinhTrangNktHbcc.count_giunguyen = 0;
            itemReportTinhTrangNktHbcc.count_caithienit = 1;
            itemReportTinhTrangNktHbcc.count_caithienvua = 0;
            itemReportTinhTrangNktHbcc.count_caithiennhieu = 0;
          }
          break;

        case 3:
          {
            //cải thiện vừa
            itemReportTinhTrangNktHbcc.count_xauhon = 0;
            itemReportTinhTrangNktHbcc.count_giunguyen = 0;
            itemReportTinhTrangNktHbcc.count_caithienit = 0;
            itemReportTinhTrangNktHbcc.count_caithienvua = 1;
            itemReportTinhTrangNktHbcc.count_caithiennhieu = 0;
          }
          break;

        case 4:
          {
            //cải thiện nhiều
            itemReportTinhTrangNktHbcc.count_xauhon = 0;
            itemReportTinhTrangNktHbcc.count_giunguyen = 0;
            itemReportTinhTrangNktHbcc.count_caithienit = 0;
            itemReportTinhTrangNktHbcc.count_caithienvua = 0;
            itemReportTinhTrangNktHbcc.count_caithiennhieu = 1;
          }
          break;

        default:
          {
            itemReportTinhTrangNktHbcc.count_xauhon = 0;
            itemReportTinhTrangNktHbcc.count_giunguyen = 0;
            itemReportTinhTrangNktHbcc.count_caithienit = 0;
            itemReportTinhTrangNktHbcc.count_caithienvua = 0;
            itemReportTinhTrangNktHbcc.count_caithiennhieu = 0;
          }
          break;
      }
      await reportTinhTrangNKTHbccState.add(itemReportTinhTrangNktHbcc);
    }
  }

  Future<void> UpdateReportTinhTrangNktNcs(int scoreVal, String scoreMessage) async {
    await reportTinhTrangNKTState.getListReportTinhTrangNKT(
        month: _formDate.month, year: _formDate.year, mId: _formUser);
    List<ReportTinhTrangNKT> repTanSuatNCS = reportTinhTrangNKTState
        .repTinhTrangNKT;
    var itemReportTinhTrangNKT = new ReportTinhTrangNKT();
    if (repTanSuatNCS.length > 0) {
      //cập nhật
      itemReportTinhTrangNKT.id = repTanSuatNCS[0].id;
      itemReportTinhTrangNKT.month = repTanSuatNCS[0].month;
      itemReportTinhTrangNKT.year = repTanSuatNCS[0].year;
      itemReportTinhTrangNKT.scores = repTanSuatNCS[0].scores! + scoreVal;
      itemReportTinhTrangNKT.info = scoreMessage;
      itemReportTinhTrangNKT.memberId = repTanSuatNCS[0].memberId;
      await reportTinhTrangNKTState.update(itemReportTinhTrangNKT);
    } else {
      //thêm mới
      itemReportTinhTrangNKT.month = _formDate.month;
      itemReportTinhTrangNKT.year = _formDate.year;
      itemReportTinhTrangNKT.scores = scoreVal;
      itemReportTinhTrangNKT.info = scoreMessage;
      itemReportTinhTrangNKT.memberId = _formUser;
      await reportTinhTrangNKTState.add(itemReportTinhTrangNKT);
    }
  }

  Future<void> UpdateReportPhanTramThucHienKeHoach(int soluong_khongtheokehoach, int soluong_theokehoach) async {
    await reportThuchienKehoachState.getListReportThuchienKehoach(month: _formDate.month, year: _formDate.year);
    List<ReportThuchienKehoach> repThucHienKeHoach = reportThuchienKehoachState
        .repThuchienKehoach;
    var itemThucHienKeHoach = new ReportThuchienKehoach();
    if (repThucHienKeHoach.length > 0) {
      //cập nhật
      itemThucHienKeHoach.id = repThucHienKeHoach[0].id;
      itemThucHienKeHoach.month = repThucHienKeHoach[0].month;
      itemThucHienKeHoach.year = repThucHienKeHoach[0].year;
      itemThucHienKeHoach.soluong_khongtheokehoach = repThucHienKeHoach[0].soluong_khongtheokehoach! + soluong_khongtheokehoach;
      itemThucHienKeHoach.soluong_theokehoach = repThucHienKeHoach[0].soluong_theokehoach! + soluong_theokehoach;
      print(itemThucHienKeHoach.toJson());
      await reportThuchienKehoachState.update(itemThucHienKeHoach);
    } else {
      //thêm mới
      itemThucHienKeHoach.month = _formDate.month;
      itemThucHienKeHoach.year = _formDate.year;
      itemThucHienKeHoach.soluong_khongtheokehoach = soluong_khongtheokehoach;
      itemThucHienKeHoach.soluong_theokehoach = soluong_theokehoach;
      await reportThuchienKehoachState.add(itemThucHienKeHoach);
    }
  }
}