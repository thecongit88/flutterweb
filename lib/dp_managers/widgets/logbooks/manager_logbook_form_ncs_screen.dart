import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:another_flushbar/flushbar.dart';

class LogBookFormNCSScreen extends StatefulWidget {
  final Member member;

  const LogBookFormNCSScreen({Key? key,required this.member}) : super(key: key);
  @override
  _LogBookFormNCSScreenState createState() => _LogBookFormNCSScreenState();
}

class _LogBookFormNCSScreenState extends State<LogBookFormNCSScreen>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _date;
  String _dateLabel = "Chọn ngày";
  String _picked = "Không cải thiện - giữ nguyên như cũ";
  String _pickedVetloet = "Giữ nguyên như cũ";
  String _vetloet = "Không";
  String _kehoachBanDau = "Có";
  String _ncsDuocTapHuan = "Có";
  String _ncsHuongDanLai = "Không";
  String _csDuaTrenKeHoach = "Có";
  double _phanTramKeHoach = 0.0;
  String _lydo = "";
  String _ykien = "";
  String _khuyenNghi = "";
  late DateTime _dateKeTiep;
  String _dateKeTiepLabel = "Chọn ngày";
  int _dateKeTiepDays = 0;
  List<DropdownMenuItem<String>> _listMembers = [new DropdownMenuItem<String>(
    value: "",
    child: new Text("Chọn NKT được đánh giá"),
  )];

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    _date = DateTime.now();

    _listMembers.addAll(Member.memberList.map((Member map) {
      return new DropdownMenuItem<String>(
        value: map.code,
        child: new Text(map.name ?? ""),
      );
    }).toList());

    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {

    return
      Container(
        color: HexColor("#F3F4F9"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  color: ManagerAppTheme.nearlyWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.only(top: 10),
                child: Column(
                  children: <Widget>[
                    wdgNKT(),
                    wdgThoiGian(),
                    wdgNguoiThucHien(),
                    wdgThucHienKeHoach(),
                    wdgYKien(),
                    wdgGiamSatTiepTheo(),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: opacity3,
                      child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16, bottom: 25, top: 16, right: 16),
                          child:
                          InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 48,
                                    decoration: BoxDecoration(
                                      color: ManagerAppTheme.nearlyBlue,
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(16.0),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: ManagerAppTheme
                                                .nearlyBlue
                                                .withOpacity(0.5),
                                            offset: const Offset(1.1, 1.1),
                                            blurRadius: 10.0),
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Hoàn thành',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18,
                                          letterSpacing: 0.0,
                                          color:
                                          ManagerAppTheme.nearlyWhite,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onTap: (){
                              finishForm();
                            },
                          )

                      ),
                    ),
                  ],
                )),
          ],
        ),
      );
  }


  Widget wdgNKT(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Đánh giá tại nhà:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0),
            child:
            DropdownButton<String>(
              isExpanded: true,
            items: _listMembers,
            value: widget.member!=null?widget.member.code:"",
            onChanged: (value) {
              setState(() {
              });
            },
    )
          ),
        ],
      );
  }

  Widget wdgThoiGian(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Thời gian thực hiện:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0),
            child:
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm $date');
                      _date = date;
                      setState(() {
                        _dateLabel = '${date.day}/${date.month}/${date.year}';
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
        ],
      );
  }

  Widget wdgNguoiThucHien(){
    return
    Wrap(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 25.0),
            child: Text(
              'Người thực hiện chăm sóc chính NKT là người tham gia tập huấn do tự án tổ chức không? ',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: EdgeInsets.only(
              left: 10, right: 25.0, top: 2.0),
          child:
          new RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            onSelected: (String selected) => setState((){
              _ncsDuocTapHuan = selected;
            }),
            picked: _ncsDuocTapHuan,
            labels: <String>["Có", "Không",],
            itemBuilder: (Radio radio,Text label, int index) {
              return Row(
                children: <Widget>[
                  radio,label,SizedBox(width: 10,)
                ],
              );
            },
          ),
        ),
        _ncsDuocTapHuan=="Không"?
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    left: 25.0, right: 25.0, top: 25.0),
                child: Text(
                  'Nếu NCS không tham gia tập huấn thì có được ai hướng dẫn lại hay không?',
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                )
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 10, right: 25.0, top: 2.0),
              child:
              new RadioButtonGroup(
                orientation: GroupedButtonsOrientation.HORIZONTAL,
                onSelected: (String selected) => setState((){
                  _ncsHuongDanLai = selected;
                }),
                picked: _ncsHuongDanLai,
                labels: <String>["Có", "Không",],
                itemBuilder: (Radio radio,Text label, int index) {
                  return Row(
                    children: <Widget>[
                      radio,label,SizedBox(width: 10,)
                    ],
                  );
                },
              ),
            )
          ],
        ):
        SizedBox(height: 0,)
      ],
    );
  }

  Widget wdgThucHienKeHoach(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thực hiện chăm sóc NKT dựa trên kế hoạch chăm sóc tại nhà hay không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _csDuaTrenKeHoach = selected;
              }),
              picked: _csDuaTrenKeHoach,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          _csDuaTrenKeHoach=="Có"?
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 25.0),
                  child: Text(
                    'Nếu có thực hiện, thì thưc hiện được khoảng bao nhiêu % kế hoạch đã đề ra? (Đơn vị là %)',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  )
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 25, right: 25.0, top: 2.0),
                child:  TextField(
                  decoration: new InputDecoration(labelText: "Ví dụ điền: 20, 25, 30"),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                ),
              )
            ],
          ):
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 25.0),
                  child: Text(
                    'Nếu không, Tại sao chưa thực hiện theo kế hoạch?',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  )
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: 25, right: 25.0, top: 2.0),
                child: TextField(
                    maxLines: 3,
                    decoration: const InputDecoration(
                      hintText: "Lý do của bạn là gì?",
                    )),
              )
            ],
          )
        ],
      );
  }

  Widget wdgYKien(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Ý kiến của NCS về kế hoạch chăm sóc NKT',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              )
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 25, right: 25.0, top: 2.0),
            child: TextField(
                maxLines: 3,
                decoration: const InputDecoration(
                  hintText: "Ý kiến..",
                )),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Khuyến nghị của người giám sát về kế hoạch chăm sóc NKT',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              )
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 25, right: 25.0, top: 2.0),
            child: TextField(
                maxLines: 3,
                decoration: const InputDecoration(
                  hintText: "Khuyến nghị của bạn..",
                )),
          ),
        ],
      );
  }

  Widget wdgGiamSatTiepTheo(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child:
              Text(
                'Thời gian dự kiến đến giám sát lần kế tiếp?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0),
            child:
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime.now(),
                    maxTime: DateTime.now().add(Duration(days: 31)),
                    onConfirm: (date) {
                      print('confirm $date');
                      _dateKeTiep = date;
                      var _dateHienTai = new DateTime(_date.year,_date.month,_date.day);
                      setState(() {
                        _dateKeTiepLabel = '${date.day}/${date.month}/${date.year}';
                        _dateKeTiepDays = _dateKeTiep.difference(_dateHienTai).inDays;
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateKeTiepLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: Text(
                'Khoảng cách thời gian giữa lần giám sát này đến lần giám sát kế tiếp?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: Text(
                '$_dateKeTiepDays ngày',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,color: ManagerAppTheme.nearlyBlue),
              )),
        ],
      );
  }


  void finishFormBak(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  void finishForm(){
    var rng = new Random();
    int randomNumber = rng.nextInt(2);
    if(randomNumber==0){
      return planSuccess();
    }else{
      return planChange();
    }
  }

  planSuccess(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 30),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;

  }
}
