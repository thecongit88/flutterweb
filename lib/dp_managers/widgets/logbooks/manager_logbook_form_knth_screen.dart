import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:another_flushbar/flushbar.dart';

class LogBookFormKNTHScreen extends StatefulWidget {
  final Member member;

  const LogBookFormKNTHScreen({Key? key,required this.member}) : super(key: key);
  @override
  _LogBookFormKNTHScreenState createState() => _LogBookFormKNTHScreenState();
}

class _LogBookFormKNTHScreenState extends State<LogBookFormKNTHScreen>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _date;
  String _dateLabel = "Chọn ngày";
  String _tuAnUong = "Tự thực hiện";
  String _tuDanhRang = "Tự thực hiện";
  String _tuTam = "Tự thực hiện";
  String _tuMacQuanAo = "Tự thực hiện";
  String _tuDaiTieuTien = "Tự thực hiện";
  String _tuLenGiuong = "Tự thực hiện";
  String _kiemSoatTieuTien = "Tự thực hiện";
  int _tuAnUongScore = 1;
  int _tuDanhRangScore = 1;
  int _tuTamScore = 1;
  int _tuMacQuanAoScore = 1;
  int _tuDaiTieuTienScore = 1;
  int _tuLenGiuongScore = 1;
  int _kiemSoatTieuTienScore = 1;
  int _tongDiem = 1;
  int _level = 1;
  String _ketLuan = "";
  List<Color> colors = [Colors.green,Colors.blue,Colors.deepPurple, Colors.amber, Colors.red];
  List<IconData> icons = [Icons.check_circle,Icons.info,Icons.adjust, Icons.remove_circle, Icons.cancel];
  List<DropdownMenuItem<String>> _listMembers = [new DropdownMenuItem<String>(
    value: "",
    child: new Text("Chọn NKT được đánh giá"),
  )];

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    _date = DateTime.now();

    _listMembers.addAll(Member.memberList.map((Member map) {
      return new DropdownMenuItem<String>(
        value: map.code,
        child: new Text(map.name ?? ""),
      );
    }).toList());

    calScore();

    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {

    return
      Container(
        color: HexColor("#F3F4F9"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  color: ManagerAppTheme.nearlyWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.only(top: 10),
                child: Column(
                  children: <Widget>[
                    wdgNKT(),
                    wdgThoiGian(),
                    wdgAnUong(),
                    wdgRuaMatDanhRang(),
                    wdgTamRua(),
                    wdgMacQuanAo(),
                    wdgDaiTieuTien(),
                    wdgLenXuongGiuong(),
                    wdgKiemSoatDaiTieuTien(),
                    wdgTinhDiem(),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: opacity3,
                      child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16, bottom: 25, top: 16, right: 16),
                          child:
                          InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 48,
                                    decoration: BoxDecoration(
                                      color: ManagerAppTheme.nearlyBlue,
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(16.0),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: ManagerAppTheme
                                                .nearlyBlue
                                                .withOpacity(0.5),
                                            offset: const Offset(1.1, 1.1),
                                            blurRadius: 10.0),
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Hoàn thành',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18,
                                          letterSpacing: 0.0,
                                          color:
                                          ManagerAppTheme.nearlyWhite,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onTap: (){
                              finishForm();
                            },
                          )

                      ),
                    ),
                  ],
                )),
          ],
        ),
      );
  }

  Widget wdgNKT(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Đánh giá tại nhà:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 10.0),
              child:
              DropdownButton<String>(
                isExpanded: true,
                items: _listMembers,
                value: widget.member!=null?widget.member.code:"",
                onChanged: (value) {
                  setState(() {
                  });
                },
              )
          ),
        ],
      );
  }

  Widget wdgThoiGian(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Thời gian thực hiện:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0),
            child:
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm $date');
                      _date = date;
                      setState(() {
                        _dateLabel = '${date.day}/${date.month}/${date.year}';
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
        ],
      );
  }

  Widget wdgAnUong(){
    return
    Wrap(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 25.0),
            child: Text(
              'Có thể tự ăn uống?',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: EdgeInsets.only(
              left: 10, right: 25.0, top: 2.0),
          child:
          new RadioButtonGroup(
            orientation: GroupedButtonsOrientation.VERTICAL,
            onChange: (String selected,int index) => setState((){
              _tuAnUongScore = index+1;
              calScore();
            }),
            onSelected: (String selected) => setState((){
              _tuAnUong = selected;
            }),
            picked: _tuAnUong,
            labels: <String>[
              "Tự thực hiện",
              "Cần hỗ trợ của dụng cụ trợ giúp",
              "Cần người khác hướng dẫn/giám sát",
              "Cần người khác trợ giúp để thực hiện",
              "Phụ thuộc hoàn toàn",
            ],
            itemBuilder: (Radio radio,Text label, int index) {
              return Row(
                children: <Widget>[
                  radio,label,SizedBox(width: 10,)
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  Widget wdgRuaMatDanhRang(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thể tự rửa mặt, đánh răng, chải đầu?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _tuDanhRangScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _tuDanhRang = selected;
              }),
              picked: _tuDanhRang,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgTamRua(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thể tự tắm?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _tuTamScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _tuTam = selected;
              }),
              picked: _tuTam,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgMacQuanAo(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thể tự mặc/cởi quần áo?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _tuMacQuanAoScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _tuMacQuanAo = selected;
              }),
              picked: _tuMacQuanAo,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgDaiTieuTien(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thể tự đi tiểu tiện/đại tiện?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _tuDaiTieuTienScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _tuDaiTieuTien = selected;
              }),
              picked: _tuDaiTieuTien,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgLenXuongGiuong(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Di chuyển vào, ra khỏi giường?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _tuLenGiuongScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _tuLenGiuong = selected;
              }),
              picked: _tuLenGiuong,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgKiemSoatDaiTieuTien(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Có thể kiếm soát quá trình đi tiểu tiện/đại tiện?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.VERTICAL,
              onChange: (String selected,int index) => setState((){
                _kiemSoatTieuTienScore = index+1;
                calScore();
              }),
              onSelected: (String selected) => setState((){
                _kiemSoatTieuTien = selected;
              }),
              picked: _kiemSoatTieuTien,
              labels: <String>[
                "Tự thực hiện",
                "Cần hỗ trợ của dụng cụ trợ giúp",
                "Cần người khác hướng dẫn/giám sát",
                "Cần người khác trợ giúp để thực hiện",
                "Phụ thuộc hoàn toàn",
              ],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgTinhDiem(){
    return
      Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0,bottom: 0),
      child:
      Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              top: BorderSide( //                    <--- top side
                color: colors[_level - 1],
                width: 2.0,
              )
          ),
        ),
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(icons[_level-1],color: colors[_level - 1],size: 30,),
            SizedBox(height: 5,),
            Text(
              _ketLuan,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  letterSpacing: 0.0,
                  color: colors[_level - 1]
              ),
            ),
          ],
        )

      ),
    );
  }

  void calScore(){
    _tongDiem = _tuAnUongScore +_tuDanhRangScore + _tuTamScore + _tuMacQuanAoScore + _tuDaiTieuTienScore + _tuLenGiuongScore + _kiemSoatTieuTienScore;
    setState((){
      _tongDiem = _tongDiem;
    });
    var ketLuan = "";
    if(_tongDiem ==7){
      ketLuan = "Tự thực hiện";
      _level  = 1;
    }else if(_tuAnUongScore== 5 || _tuDanhRangScore== 5 ||  _tuTamScore ==5 || _tuMacQuanAoScore == 5 ||
        _tuDaiTieuTienScore == 5 || _tuLenGiuongScore == 5 || _kiemSoatTieuTienScore == 5
    ){
      ketLuan = "Phụ thuộc hoàn toàn";
      _level  = 5;
    }else if(_tuAnUongScore== 4 || _tuDanhRangScore== 4 ||  _tuTamScore == 4 || _tuMacQuanAoScore == 4 ||
        _tuDaiTieuTienScore == 4 || _tuLenGiuongScore == 4 || _kiemSoatTieuTienScore == 4
    ){
      ketLuan = "Cần người khác trợ giúp để thực hiện";
      _level  = 4;
    }else if(
        _tuAnUongScore==3 || _tuDanhRangScore==3 ||  _tuTamScore ==3 || _tuMacQuanAoScore ==3 ||
            _tuDaiTieuTienScore ==3 || _tuLenGiuongScore ==3 || _kiemSoatTieuTienScore==3
    ){
      ketLuan = "Cần người khác hướng dẫn/giám sát";
      _level  = 3;
    }else if(_tongDiem == 8){
      ketLuan = "Cần hỗ trợ của dụng cụ trợ giúp";
      _level  = 2;
    }
    setState((){
      _ketLuan = ketLuan;
    });
  }

  void finishFormBak(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  void finishForm(){
    var rng = new Random();
    int randomNumber = rng.nextInt(2);
    if(randomNumber==0){
      return planSuccess();
    }else{
      return planChange();
    }
  }

  planSuccess(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 30),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;

  }
}
