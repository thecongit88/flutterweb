import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:another_flushbar/flushbar.dart';

class LogBookFormMTScreen extends StatefulWidget {
  final Member member;

  const LogBookFormMTScreen({Key? key,required this.member}) : super(key: key);
  @override
  _LogBookFormMTScreenState createState() => _LogBookFormMTScreenState();
}

class _LogBookFormMTScreenState extends State<LogBookFormMTScreen>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _date;
  String _dateLabel = "Chọn ngày";
  String _xungQuanhThoangKhi = "Không";
  String _benNgoaiMuiKhoChiu = "Không";
  String _trongNhaMuiKhoChiu = "Không";
  String _bacThangBacCua = "Không";
  String _bacThem = "Không";
  String _tayVinCua = "Không";
  String _tayVinTuongTrong = "Không";
  String _tayVinTuongNgoai = "Không";
  String _dungcuHoTroGiuong = "Không";
  String _dungcuHoTroTuGiuong = "Không";
  String _suDungNhaVeSinhChung = "Không";
  String _diChuyenRaNhaVeSinh = "Không";
  String _nhaVeSinhCoTayCam = "Không";
  List<String> _diVeSinhCachNao = ["Dùng bỉm"];
  bool _diVeSinhCachNaoKhac = false;
  String _keHoachDieuChinh = "Không";
  List<String> _dieuChinhGi = ["Lắp dụng cụ/dây hỗ trợ NKT dậy từ gường"];
  bool _dieuChinhGiKhac = false;
  int _xungQuanhThoangKhiScore = 1;
  int _tuDanhRangScore = 1;
  int __benNgoaiMuiKhoChiuScore = 1;
  int _tuMacQuanAoScore = 1;
  int _tuDaiTieuTienScore = 1;
  int _tuLenGiuongScore = 1;
  int _kiemSoatTieuTienScore = 1;
  int _tongDiem = 1;
  int _level = 1;
  String _ketLuan = "";
  List<Color> colors = [Colors.green,Colors.blue,Colors.deepPurple, Colors.amber, Colors.red];
  List<IconData> icons = [Icons.check_circle,Icons.info,Icons.adjust, Icons.remove_circle, Icons.cancel];
  List<DropdownMenuItem<String>> _listMembers = [new DropdownMenuItem<String>(
    value: "",
    child: new Text("Chọn NKT được đánh giá"),
  )];

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    _date = DateTime.now();
    calScore();
    _listMembers.addAll(Member.memberList.map((Member map) {
      return new DropdownMenuItem<String>(
        value: map.code,
        child: new Text(map.name ?? ""),
      );
    }).toList());

    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        color: HexColor("#F3F4F9"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  color: ManagerAppTheme.nearlyWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.only(top: 10),
                child: Column(
                  children: <Widget>[
                    wdgNKT(),
                    wdgThoiGian(),
                    wdgXungQuanh(),
                    wdgNhaCuaVatDung(),
                    wdgNhaVeSinh(),
                    wdgGiaDinh(),
                    wdgYKien(),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: opacity3,
                      child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16, bottom: 25, top: 16, right: 16),
                          child:
                          InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 48,
                                    decoration: BoxDecoration(
                                      color: ManagerAppTheme.nearlyBlue,
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(16.0),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: ManagerAppTheme
                                                .nearlyBlue
                                                .withOpacity(0.5),
                                            offset: const Offset(1.1, 1.1),
                                            blurRadius: 10.0),
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Hoàn thành',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18,
                                          letterSpacing: 0.0,
                                          color:
                                          ManagerAppTheme.nearlyWhite,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onTap: (){
                              finishForm();
                            },
                          )

                      ),
                    ),
                  ],
                )),
          ],
        ),
      );
  }

  Widget wdgNKT(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Đánh giá tại nhà:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 10.0),
              child:
              DropdownButton<String>(
                isExpanded: true,
                items: _listMembers,
                value: widget.member!=null?widget.member.code:"",
                onChanged: (value) {
                  setState(() {
                  });
                },
              )
          ),
        ],
      );
  }

  Widget wdgThoiGian(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Thời gian thực hiện:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 10.0),
            child:
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm $date');
                      _date = date;
                      setState(() {
                        _dateLabel = '${date.day}/${date.month}/${date.year}';
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
        ],
      );
  }

  Widget wdgXungQuanh(){
    return
    Wrap(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 25.0),
            child: Text(
              'I. Không khí môi trường xung quanh nhà NKT',
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,color: ManagerAppTheme.nearlyBlue),
            )),
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 25.0),
            child: Text(
              'Môi trường xung quanh nhà NKT có thông thoáng/thoáng khí không?',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: EdgeInsets.only(
              left: 10, right: 25.0, top: 2.0),
          child:
          new RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            onSelected: (String selected) => setState((){
              _xungQuanhThoangKhi = selected;
            }),
            picked: _xungQuanhThoangKhi,
            labels: <String>["Có", "Không",],
            itemBuilder: (Radio radio,Text label, int index) {
              return Row(
                children: <Widget>[
                  radio,label,SizedBox(width: 10,)
                ],
              );
            },
          ),
        ),
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 15.0),
            child: Text(
              'Có mùi gì khó chịu/không thoái mái khi đến gần nhà NKT không?',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: EdgeInsets.only(
              left: 10, right: 25.0, top: 2.0),
          child:
          new RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            onSelected: (String selected) => setState((){
              _benNgoaiMuiKhoChiu = selected;
            }),
            picked: _benNgoaiMuiKhoChiu,
            labels: <String>["Có", "Không",],
            itemBuilder: (Radio radio,Text label, int index) {
              return Row(
                children: <Widget>[
                  radio,label,SizedBox(width: 10,)
                ],
              );
            },
          ),
        ),
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 15.0),
            child: Text(
              'Khi vào trong nhà NKT, anh/chị có cảm thấy mùi gì khó chịu không?',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: EdgeInsets.only(
              left: 10, right: 25.0, top: 2.0),
          child:
          new RadioButtonGroup(
            orientation: GroupedButtonsOrientation.HORIZONTAL,
            onSelected: (String selected) => setState((){
              _trongNhaMuiKhoChiu = selected;
            }),
            picked: _trongNhaMuiKhoChiu,
            labels: <String>["Có", "Không",],
            itemBuilder: (Radio radio,Text label, int index) {
              return Row(
                children: <Widget>[
                  radio,label,SizedBox(width: 10,)
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  Widget wdgNhaCuaVatDung(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'II. Nhà cửa, vận dụng hỗ trợ NKT',
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,color: ManagerAppTheme.nearlyBlue),
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Trước khi bước vào nhà NKT, anh/chị có thấy BẬC THANG/BẬC CỬA bước lên nhà không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _bacThangBacCua = selected;
              }),
              picked: _bacThangBacCua,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Tại cửa chính ra vào nhà NKT, anh/chị có thấy BẬC THỀM bước vào nhà không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _bacThem = selected;
              }),
              picked: _bacThem,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Bên cạnh cửa chính ra vào nhà NKT, anh/chị có thấy THANH VỊN/TAY CẦM bên cạnh tường nhà NKT để hỗ trợ NKT di chuyển không? ',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _tayVinCua= selected;
              }),
              picked: _tayVinCua,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Xung quanh tường bên trong nhà NKT, anh/chị có thấy THANH VỊN/TAY CẦM bên cạnh tường nhà NKT để hỗ trợ NKT di chuyển không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _tayVinTuongTrong = selected;
              }),
              picked: _tayVinTuongTrong,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Xung quanh tường bên ngoài nhà NKT, anh/chị có thấy THANH VỊN/TAY CẦM bên cạnh tường nhà NKT để hỗ trợ NKT di chuyển không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _tayVinTuongNgoai = selected;
              }),
              picked: _tayVinTuongNgoai,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Giường của NKT có dây cầm/dụng cụ hỗ trợ NKT ngồi dậy không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _dungcuHoTroGiuong = selected;
              }),
              picked: _dungcuHoTroGiuong,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 15.0),
              child: Text(
                'Có dây cầm/dụng cụ hỗ trợ NKT di chuyển từ giường ra bàn uống nước hay đi ra vị trí khác không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _dungcuHoTroTuGiuong = selected;
              }),
              picked: _dungcuHoTroTuGiuong,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
        ],
      );
  }

  Widget wdgNhaVeSinh(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'III. Nhà vệ sinh',
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,color: ManagerAppTheme.nearlyBlue),
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'NKT có khả năng sử dụng nhà vệ sinh chung với gia đình không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _suDungNhaVeSinhChung = selected;
              }),
              picked: _suDungNhaVeSinhChung,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          _suDungNhaVeSinhChung=="Có"?
              Column(
                children: <Widget>[

                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 15.0),
                      child: Text(
                        'Nếu có, NKT có khả năng di chuyển ra nhà vệ sinh không?',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      )),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10, right: 25.0, top: 2.0),
                    child:
                    new RadioButtonGroup(
                      orientation: GroupedButtonsOrientation.HORIZONTAL,
                      onSelected: (String selected) => setState((){
                        _diChuyenRaNhaVeSinh = selected;
                      }),
                      picked: _diChuyenRaNhaVeSinh,
                      labels: <String>["Có", "Không",],
                      itemBuilder: (Radio radio,Text label, int index) {
                        return Row(
                          children: <Widget>[
                            radio,label,SizedBox(width: 10,)
                          ],
                        );
                      },
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 15.0),
                      child: Text(
                        'Nhà vệ sinh có TAY CẦM/KHUNG hỗ trợ NKT đi vệ sinh thuận tiện không?',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      )),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 10, right: 25.0, top: 2.0),
                    child:
                    new RadioButtonGroup(
                      orientation: GroupedButtonsOrientation.HORIZONTAL,
                      onSelected: (String selected) => setState((){
                        _nhaVeSinhCoTayCam = selected;
                      }),
                      picked: _nhaVeSinhCoTayCam,
                      labels: <String>["Có", "Không",],
                      itemBuilder: (Radio radio,Text label, int index) {
                        return Row(
                          children: <Widget>[
                            radio,label,SizedBox(width: 10,)
                          ],
                        );
                      },
                    ),
                  ),
                ],
              ):
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 25.0),
                  child: Text(
                    'Nếu không, NKT đi vệ sinh bằng cách nào?',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  )),
              Padding(
                  padding: EdgeInsets.only(
                      left: 0, right: 25.0, top: 2.0),
                  child:
                  CheckboxGroup(
                    labels: <String>[
                      "Dùng bỉm",
                      "Dùng bô",
                      "Thiết kế gường có vị trí đi vệ sinh",
                      "Khác (ghi rõ)"
                    ],
                    checked: _diVeSinhCachNao,
                    onChange: (bool isChecked, String label, int index) => print("isChecked: $isChecked   label: $label  index: $index"),
                    onSelected: (List<String> checked){
                      setState((){
                        _diVeSinhCachNao = checked;
                        _diVeSinhCachNaoKhac = checked.contains("Khác (ghi rõ)");
                      });
                    },
                  )
              ),
              _diVeSinhCachNaoKhac?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 25.0),
                      child: Text(
                        'Cụ thể đi vệ sinh bằng cách khác:',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25.0, top: 2.0),
                    child: TextField(
                        maxLines: 3,
                        decoration: const InputDecoration(
                          hintText: "Nội dung..",
                        )),
                  ),
                ],
              )
                  :SizedBox(height: 0,)
            ],
          ),
        ],
      );
  }

  Widget wdgGiaDinh(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'IV. Gia đình',
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,color: ManagerAppTheme.nearlyBlue),
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Gia đình có kế hoạch điều chỉnh nhà ở/thay đổi phù hợp với tình hình NKT không?',
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 10, right: 25.0, top: 2.0),
            child:
            new RadioButtonGroup(
              orientation: GroupedButtonsOrientation.HORIZONTAL,
              onSelected: (String selected) => setState((){
                _keHoachDieuChinh = selected;
              }),
              picked: _keHoachDieuChinh,
              labels: <String>["Có", "Không",],
              itemBuilder: (Radio radio,Text label, int index) {
                return Row(
                  children: <Widget>[
                    radio,label,SizedBox(width: 10,)
                  ],
                );
              },
            ),
          ),
          _keHoachDieuChinh == "Có"?
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      left: 25.0, right: 25.0, top: 25.0),
                  child: Text(
                    'Nếu có, gia đình sẽ điều chỉnh/thay đổi gì?',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  )),
              Padding(
                padding: EdgeInsets.only(
                    left: 10, right: 25.0, top: 2.0),
                child:
                CheckboxGroup(
                  itemBuilder: (Checkbox check, Text label, int idx){
                    return Row(
                      children: <Widget>[
                        check,
                        Flexible(
                          child: label,
                        )
                      ],
                    );
                  },
                  labels: <String>[
                    "Lắp dụng cụ/dây hỗ trợ NKT dậy từ gường",
                    "Lắp thêm khung quanh tường để hỗ trợ NKT đi lại",
                    "Lắp thêm khung trong nhà vệ sinh để hỗ trợ NKT đi vệ sinh thuận tiện hơn",
                    "Bỏ bậc cửa ở cửa chính",
                    "Bỏ bậc thềm khi bước lên cửa chín",
                    "Khác (ghi rõ)"
                  ],
                  checked: _dieuChinhGi,
                  onChange: (bool isChecked, String label, int index) => print("isChecked: $isChecked   label: $label  index: $index"),
                  onSelected: (List<String> checked){
                    setState((){
                      _dieuChinhGi = checked;
                      _dieuChinhGiKhac = checked.contains("Khác (ghi rõ)");
                    });
                  },
                ),
              ),
              _dieuChinhGiKhac?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 25.0),
                      child: Text(
                        'Điều chỉnh khác:',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25.0, top: 2.0),
                    child: TextField(
                        maxLines: 3,
                        decoration: const InputDecoration(
                          hintText: "Nội dung..",
                        )),
                  ),
                ],
              )
                  :SizedBox(height: 0,)
            ],
          ): SizedBox(height: 0,)
        ],
      );
  }

  Widget wdgYKien(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Nhận xét chung:',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              )
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 25, right: 25.0, top: 2.0),
            child: TextField(
                maxLines: 3,
                decoration: const InputDecoration(
                  hintText: "Nhận xét chung..",
                )),
          ),
          Padding(
              padding: EdgeInsets.only(
                  left: 25.0, right: 25.0, top: 25.0),
              child: Text(
                'Khuyến nghị:',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              )
          ),
          Padding(
            padding: EdgeInsets.only(
                left: 25, right: 25.0, top: 2.0),
            child: TextField(
                maxLines: 3,
                decoration: const InputDecoration(
                  hintText: "Khuyến nghị của bạn..",
                )),
          ),
        ],
      );
  }
  void calScore(){

  }

  void finishFormBak(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planSuccess();
          }
        });
  }

  void finishForm(){
    var rng = new Random();
    int randomNumber = rng.nextInt(2);
    if(randomNumber==0){
      return planSuccess();
    }else{
      return planChange();
    }
  }

  planSuccess(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }
}
