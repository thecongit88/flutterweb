import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/logbookType.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_logbook_form_create_screen.dart';
import 'package:hmhapp/dp_managers/widgets/logbooks/manager_logbook_form_create_view.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/form_item_logbook_state.dart';
import 'package:hmhapp/states/forms/form_type_state.dart';
import 'package:provider/provider.dart';

class ManagerChooseLogbookWidget extends StatefulWidget {
  final Member? member;
  const ManagerChooseLogbookWidget({Key? key, this.member}) : super(key: key);

  @override
  _ManagerChooseLogbookWidgetState createState() =>
      _ManagerChooseLogbookWidgetState();
}

class _ManagerChooseLogbookWidgetState extends State<ManagerChooseLogbookWidget>
    with TickerProviderStateMixin {
  //final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late FormTypeState _formTypeState;
  late FormItemLogbookState _formItemLogbookState;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: const Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    super.initState();

    _formItemLogbookState = FormItemLogbookState();
    _formTypeState = FormTypeState();
    _formTypeState.getList();
  }

  @override
  Widget build(BuildContext context) {
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;
    return ChangeNotifierProvider(
        create: (_) => _formTypeState,
        child: Consumer<FormTypeState>(builder: (context, state, child) {
          return bindForm(state, context);
        }));
  }

  Widget bindForm(FormTypeState state, BuildContext ctx) {
    switch (state.formTypeResponses.status) {
      case Status.LOADING:
        return Container(
          color: Colors.white30,
          child: const Center(
            child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
          ),
        );
        break;
      case Status.COMPLETED:
        return bindListFormType(state.formTypeResponses.data!);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.formTypeResponses.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

  Widget bindListFormType(List<FormType> list) {
    final ThemeData theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.only(top: 20, bottom: 10),
      child: Wrap(
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Center(
              child: Text(
                "Chọn đánh giá",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: ManagerAppTheme.nearlyBlue),
              ),
            ),
          ),
          const Divider(
            height: 1,
          ),
          const SizedBox(
            height: 10,
          ),
          ListView.separated(
            shrinkWrap: true,
            separatorBuilder: (BuildContext context, int index) => const Divider(),
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              final Animation<double> animation =
                  Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / list.length) * index, 1.0,
                          curve: Curves.fastOutSlowIn)));
              animationController.forward();
              FormType formType = list[index];
              return GestureDetector(
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 10, right: 10, top: 6, bottom: 6),
                    child: Row(
                      children: <Widget>[
                       /* new Icon(
                          IconData(formType.icon != null ? int.parse(formType.icon!) : 1),
                          color: ManagerAppTheme.nearlyBlue,
                        ),
                        SizedBox(
                          width: 10,
                        ),*/
                        Text(
                          formType.formName ?? "",
                          style: const TextStyle(),
                        ),
                      ],
                    ),
                  ),
                onTap: () async {
                  var a = await  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) =>
                            ManagerLogBookFormCreateScreen(
                              formType: formType,
                              member: widget.member,
                            ),
                      ));
                },
                      );
            },
          ),
        ],
      ),
    );
  }
}
