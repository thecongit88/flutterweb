import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/main.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:another_flushbar/flushbar.dart';

class LogBookFormTBLScreen extends StatefulWidget {
  final Member member;

  const LogBookFormTBLScreen({Key? key,required this.member}) : super(key: key);
  @override
  _LogBookFormTBLScreenState createState() => _LogBookFormTBLScreenState();
}

class _LogBookFormTBLScreenState extends State<LogBookFormTBLScreen>
    with TickerProviderStateMixin {

  final _formKey = GlobalKey<FormState>();

  dynamic response;
  late Map forma ;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _formDate;
  var formatStr =  "yyyy-MM-dd";
  int _formUser = 0;
  String _dateLabel = "Chọn ngày";
  String _tuAnUong = "Tự thực hiện";
  String _tuDanhRang = "Tự thực hiện";
  String _tuTam = "Tự thực hiện";
  String _tuMacQuanAo = "Tự thực hiện";
  String _tuDaiTieuTien = "Tự thực hiện";
  String _tuLenGiuong = "Tự thực hiện";
  String _kiemSoatTieuTien = "Tự thực hiện";
  int _tuAnUongScore = 1;
  int _tuDanhRangScore = 1;
  int _tuTamScore = 1;
  int _tuMacQuanAoScore = 1;
  int _tuDaiTieuTienScore = 1;
  int _tuLenGiuongScore = 1;
  int _kiemSoatTieuTienScore = 1;
  int _tongDiem = 1;
  int _level = 1;
  String _ketLuan = "";
  List<Color> colors = [Colors.green,Colors.blue,Colors.deepPurple, Colors.amber, Colors.red];
  List<IconData> icons = [Icons.check_circle,Icons.info,Icons.adjust, Icons.remove_circle, Icons.cancel];

  List<DropdownMenuItem<int>> _listMembers = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn NKT được đánh giá"),
  )];


  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _formDate = DateTime.now();
    _formUser = widget.member!=null?widget.member.id ?? 0 : 0;
    calScore();
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();

    _listMembers.addAll(Member.memberList.map((Member map) {
      return new DropdownMenuItem<int>(
        value: map.id,
        child: new Text(map.name ?? ""),
      );
    }).toList());
    super.initState();
  }


  @override
  Widget build(BuildContext ctx) {
    return ChangeNotifierProvider(
        create: (_) => FormTypeState(),
        child: Consumer<FormTypeState>(
            builder: (context, state, child) {
              return  new Container(
                color: ManagerAppTheme.nearlyWhite,
                child: new Column(children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.only(
                          left: 1, right: 1, top:15),
                      child:
                         wdgMainFormLogbook(state.formTypeResponse)
                  ),
                ]),
              );
            })
    );
  }

  Widget wdgMainFormLogbook(ApiResponse<FormType> data){
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormLogbook(data);
      case Status.ERROR:
        return ErrorMessage(
          errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgFormLogbook(ApiResponse<FormType> data){
    return new JsonSchema(
      formMap: data.data!.formFormat!,
      onChanged: (dynamic response) {
        this.response = response;
        //print(response);
      },
      actionSave: (data) {
        //print(data);
        var fields = data["fields"];
        Map<String, dynamic> map = new Map<String, dynamic>();
        String objText = '{';
        fields.forEach((field) {
          objText += '"${field["key"]}":${field["value"].toString()},';
          map["${field["key"]}"] = field["value"].toString();
        });
        objText += '}';
        map["people_with_disability"] = _formUser;
        map["form_date"] =
            new DateFormat(formatStr).format(_formDate).toString();
        var obj = json.encode(objText);
        //FormSurveyServices().create(map);
        finishForm();
      },
      selectUsers: wdgNKT(),
      chooseDate: wdgThoiGian(),
      buttonSave:
      new Container(
        child: Center(
          child: AnimatedOpacity(
            duration: const Duration(milliseconds: 500),
            opacity: opacity3,
            child: Padding(
                padding: const EdgeInsets.only(
                    left: 0, bottom: 25, top: 16, right: 0),
                child:
                InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 48,
                          decoration: BoxDecoration(
                            color: ManagerAppTheme.nearlyBlue,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(16.0),
                            ),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: ManagerAppTheme
                                      .nearlyBlue
                                      .withOpacity(0.5),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 10.0),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'Hoàn thành',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                letterSpacing: 0.0,
                                color:
                                ManagerAppTheme.nearlyWhite,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
            ),
          ),
        ),
      ),
    );
  }

  Widget wdgNKT(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 0.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Đánh giá tại nhà:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 5.0),
              child:
              DropdownButtonFormField<int>(
                isExpanded: true,
                isDense: true,
                items: _listMembers,
                value: _formUser,
                validator: (int? value) {
                  if (value == null || value<= 0) {
                    return 'Không để trống mục này !';
                  }
                },
                onChanged: (int? value) {
                  setState(() {
                    _formUser = value ?? 0;
                  });
                },
              )
          ),
        ],
      );
  }

  Widget wdgThoiGian(){
    return
      Wrap(
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15.0),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Text(
                        'Thời gian thực hiện:',
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
            padding: EdgeInsets.only(
                left: 15.0, right: 15.0, top: 15.0),
            child:
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm $date');
                      _formDate = date;
                      setState(() {
                        _dateLabel = '${date.day}/${date.month}/${date.year}';
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi);
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
          ),
        ],
      );
  }

  Widget wdgTinhDiem(){
    return
      Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0,bottom: 0),
      child:
      Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              top: BorderSide( //                    <--- top side
                color: colors[_level - 1],
                width: 2.0,
              )
          ),
        ),
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(icons[_level-1],color: colors[_level - 1],size: 30,),
            SizedBox(height: 5,),
            Text(
              _ketLuan,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  letterSpacing: 0.0,
                  color: colors[_level - 1]
              ),
            ),
          ],
        )

      ),
    );
  }

  void calScore(){
    _tongDiem = _tuAnUongScore +_tuDanhRangScore + _tuTamScore + _tuMacQuanAoScore + _tuDaiTieuTienScore + _tuLenGiuongScore + _kiemSoatTieuTienScore;
    setState((){
      _tongDiem = _tongDiem;
    });
    var ketLuan = "";
    if(_tongDiem ==7){
      ketLuan = "Tự thực hiện";
      _level  = 1;
    }else if(_tuAnUongScore== 5 || _tuDanhRangScore== 5 ||  _tuTamScore ==5 || _tuMacQuanAoScore == 5 ||
        _tuDaiTieuTienScore == 5 || _tuLenGiuongScore == 5 || _kiemSoatTieuTienScore == 5
    ){
      ketLuan = "Phụ thuộc hoàn toàn";
      _level  = 5;
    }else if(_tuAnUongScore== 4 || _tuDanhRangScore== 4 ||  _tuTamScore == 4 || _tuMacQuanAoScore == 4 ||
        _tuDaiTieuTienScore == 4 || _tuLenGiuongScore == 4 || _kiemSoatTieuTienScore == 4
    ){
      ketLuan = "Cần người khác trợ giúp để thực hiện";
      _level  = 4;
    }else if(
        _tuAnUongScore==3 || _tuDanhRangScore==3 ||  _tuTamScore ==3 || _tuMacQuanAoScore ==3 ||
            _tuDaiTieuTienScore ==3 || _tuLenGiuongScore ==3 || _kiemSoatTieuTienScore==3
    ){
      ketLuan = "Cần người khác hướng dẫn/giám sát";
      _level  = 3;
    }else if(_tongDiem == 8){
      ketLuan = "Cần hỗ trợ của dụng cụ trợ giúp";
      _level  = 2;
    }
    setState((){
      _ketLuan = ketLuan;
    });
  }

  void finishFormBak(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  void finishForm(){
    var rng = new Random();
    int randomNumber = rng.nextInt(2);
    if(randomNumber==0){
      return planSuccess();
    }else{
      return planChange();
    }
  }

  planSuccess(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;

  }

  planChange(){
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 30),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }
}

