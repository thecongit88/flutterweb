import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/widgets/form_skill_kncs_item.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/form_kncs_item.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogBookFormDetailKNCSView extends StatefulWidget {
  final FormType formType;
  final int formSurveyId; // Id of record bang FormItem
  final int formId; //Id of form chi tiet
  final int memberId;

  const LogBookFormDetailKNCSView({Key? key,
    required this.formType,required this.formSurveyId,
    required this.formId,required this.memberId}) : super(key: key);

  @override
  _LogBookFormKNCSViewScreenState createState() => _LogBookFormKNCSViewScreenState();
}

class _LogBookFormKNCSViewScreenState extends State<LogBookFormDetailKNCSView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  Map<String, dynamic> map = new Map<String, dynamic>();

  late FormSurveyState _formSurveyState;
  late PlansState _plansState;

  late String scores;

  dynamic response;
  late int hbccId;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _formDate;
  var formatStr = "dd-MM-yyyy";
  int _formUser = 0;
  String _dateLabel = "Chọn ngày";

  late List<DropdownMenuItem<int>> _listMembers;
  List<PlanCheck> _listPlan = <PlanCheck>[];
  late List<FormKNCSItem> _listFormKNCSItems;
  late List<FormKNCSItem> _listFormKNCSOldItems;

  DropdownMenuItem<int> ddlMenuItemDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn NKT được đánh giá"),
  );

  late List<Widget> _listFormSkills;
  late List<PlanCheck> _listCheckedSkills;
  FormSurveyServices _servicesSurvey = new FormSurveyServices();
  String formSurveyData = "", memberName = "Chưa gán NKT";
  bool showHoanThanh = true, showError = true;
  late Member _member;

  String errMsgDatao = "", errMsgThoiGian = "";

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _listMembers = <DropdownMenuItem<int>>[];
    _listMembers.add(ddlMenuItemDefault);

    _formDate = DateTime.now();
    _formUser = widget.memberId != null ? widget.memberId : 0;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
    _formSurveyState = new FormSurveyState();
    _plansState = new PlansState();

    if (_formUser > 0) {
      _plansState.getListPlanChecks(mId: _formUser);
    }

    _listFormSkills = <Widget>[];
    _listCheckedSkills = <PlanCheck>[];
    scores = "0";
    _listFormKNCSItems = <FormKNCSItem>[];
    _listFormKNCSOldItems = <FormKNCSItem>[];
    getSignedHbccId();

    super.initState();

    _servicesSurvey.getFormSurveyData(widget.formType.formObject, widget.formId).then((value) {
      //1 bảng dựa vào object trong bảng TormType (loại đánh giá)
      var _fromSurveyedJson = json.decode(value);

      if(_fromSurveyedJson['form_date'] !=null && _fromSurveyedJson['form_date'].toString().isNotEmpty) {
        _formDate = DateTime.parse(_fromSurveyedJson['form_date'].toString());
        _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
      }

      var list = FormKNCSItemResponse.fromJson(_fromSurveyedJson["form_kncs_items"]);
      _listFormKNCSOldItems.addAll(list.items);
      setState(() {
        formSurveyData = value; // lấy nội dung đã đánh giá từ
        _member = Member.fromMap(_fromSurveyedJson['member']);
        _listFormKNCSItems = list.items;
      });
    });

    /* chỉ được sửa form từ 26 tháng hiện tại đến hết mùng 5 tháng kế tiếp */
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      bool allowChangeSurveyForm = await _formSurveyState
          .isChangeSurveyFormForHBCC(formDate: _formDate,
          memberId: _formUser,
          formName: widget.formType.formObject);
      if (allowChangeSurveyForm == false) {
        setState(() {
          showHoanThanh = false;
          showError = true;
        });
        //Functions.showSnackBarMsg(context, Functions.timeRangeAllowCreateSurvey());
        errMsgThoiGian = Functions.timeRangeAllowCreateSurvey();
      } else {
        setState(() {
          showHoanThanh = true;
          showError = false;
        });
      }
    });
    /* end chỉ được sửa form từ 26 tháng hiện tại đến hết mùng 5 tháng kế tiếp */
  }

  @override
  Widget build(BuildContext ctx) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => _plansState),
          ChangeNotifierProvider(create: (_) => _formSurveyState)
        ],
        child: Padding(
          padding: const EdgeInsets.only(left: 9, right: 9, top: 15),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
              <Widget>[
            wdgNKT(),
            wdgThoiGian(),
            Padding(
                padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
                child: Text(
                  'TRONG THÁNG NÀY, Các hoạt động mà anh/chị áp dụng chăm sóc cho NKT là?:',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                )),
            Consumer<PlansState>(builder: (context, state, child) {
              switch (state.planChecksResponseList.status) {
                case Status.COMPLETED:
                  return wdgPlan(state.planChecksList);
                  break;
                case Status.INIT:
                  return
                    Padding(
                      padding: EdgeInsets.only(top:10,left: 10,right: 10),
                      child:
                      Text("Vui lòng chọn Người khuyết tật tại mục Đánh giá tại nhà để tải kế hoạch chăm sóc.",
                      style: TextStyle(color:Colors.deepOrange),),
                    );
                  break;
                default:
                  return Container(
                    color: Colors.white30,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                  break;
              }
            }),

            Consumer<PlansState>(builder: (context, state, child) {
              switch (state.planChecksResponseList.status) {
                case Status.COMPLETED:
                  if(_listFormSkills.length > 0 && showHoanThanh == true) {
                    return GestureDetector(
                      onTap: () {
                        map["people_with_disability"] = _formUser;
                        map["form_type"] = widget.formType;
                        map["member"] = _formUser;
                        map["form_date"] = _formDate.toString();

                        _formSurveyState.updateFormKNCS(map, widget.formId,
                            widget.formType.formObject!, _listFormKNCSItems,
                            _listFormKNCSOldItems);
                      },
                      child:
                      Container(
                        padding: EdgeInsets.only(left: 10.0, right: 10.0),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 0, bottom: 25, top: 16, right: 0),
                            child: Container(
                              height: 48,
                              decoration: BoxDecoration(
                                color: ManagerAppTheme.nearlyBlue,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(16.0),
                                ),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: ManagerAppTheme.nearlyBlue
                                          .withOpacity(0.5),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 10.0),
                                ],
                              ),
                              child: Center(
                                child: Text(
                                  'Hoàn thành',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    letterSpacing: 0.0,
                                    color: ManagerAppTheme.nearlyWhite,
                                  ),
                                ),
                              ),
                            ),),
                        ),
                      ),
                    );
                  } else {
                    /* làm mờ nút hoàn thành */
                    return Container(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 0, bottom: 25, top: 16, right: 0),
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.grey
                                        .withOpacity(0.5),
                                    offset: const Offset(1.1, 1.1),
                                    blurRadius: 10.0),
                              ],
                            ),
                            child: Center(
                              child: Text(
                                'Hoàn thành',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  letterSpacing: 0.0,
                                  color: ManagerAppTheme.nearlyWhite,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                  break;
                default:
                  return Container(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 0, bottom: 25, top: 16, right: 0),
                        child: Container(
                          height: 48,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(16.0),
                            ),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey
                                      .withOpacity(0.5),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 10.0),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'Hoàn thành',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                letterSpacing: 0.0,
                                color: ManagerAppTheme.nearlyWhite,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                  break;
              }
            }),
            Consumer<FormSurveyState>(builder: (context, state, child) {
              switch (state.formKNCSUpdateResponse.status) {
                case Status.LOADING:
                  return Container(
                    color: Colors.white30,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.green)),
                    ),
                  );
                  break;
                case Status.COMPLETED:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    return planSuccess();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planSuccess();
                        }).then((shouldUpdate) {
                      Navigator.of(context).pop();
                    });
                  });
                  return SizedBox();
                  break;
                case Status.ERROR:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    return planChange();
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planChange();
                        }).then((shouldUpdate) {
                      Navigator.of(context).pop();
                    });
                  });
                  return SizedBox();
                  break;
                case Status.INIT:
                  return SizedBox();
                  break;
                default:
                  return SizedBox();
                  break;
              }
            }),
          ]),
        ));
  }

  Widget wdgPlan(List<PlanCheck> plansList) {
    //sort plans
    plansList.sort((a, b) => a.title!.compareTo(b.title!));

    if(plansList.length>0) {
      if(_listFormKNCSItems.length>0){
        plansList.forEach((element) {
          var idx = _listFormKNCSItems.indexWhere((el) => el.plan!.id == element.id);
          var itemIdx =_listFormSkills.indexWhere((el) => el.key == ValueKey(element.id.toString()));
          if(idx >= 0 && itemIdx<0){
            element.isCheck = true;

            var itemCheck = _listFormKNCSItems[idx];
            if( itemCheck.formFormat != null) {
              element.formFormat = itemCheck.formFormat;
            }
            var form = FormSkillKNCSItem(
                key: ValueKey(_listFormKNCSItems[idx].plan!.id.toString()),
                plan: element,
                scores: itemCheck.scores,
                result: itemCheck.result,
                onChanged: (FormKNCSItem item) {
                  //if (_listFormKNCSItems.contains(item)) {
                  //  _listFormKNCSItems.remove(item);
                  //}
                  //_listFormKNCSItems.add(item);
                }
            );
            _listFormSkills.add(form);
          }
        });
      }
      return Column(
        children: <Widget>[
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              var plan = plansList[index];
              /*
              var idx = _listFormKNCSItems.indexWhere((el) => el.plan.id == plan.id);
              var itemIdx =_listFormSkills.indexWhere((el) => el.key == ValueKey(plan.id.toString()));
              if(idx >= 0 && itemIdx<0){
                plan.isCheck = true;

                var itemCheck = _listFormKNCSItems[idx];
                if( itemCheck.formFormat != null) {
                  plan.formFormat = itemCheck.formFormat;
                }
                var form = FormSkillKNCSItem(
                    key: ValueKey(_listFormKNCSItems[idx].plan.id.toString()),
                    plan: plan,
                    scores: itemCheck.scores,
                    result: itemCheck.result,
                    onChanged: (FormKNCSItem item) {
                      //if (_listFormKNCSItems.contains(item)) {
                      //  _listFormKNCSItems.remove(item);
                      //}
                      //_listFormKNCSItems.add(item);
                    });

                  _listFormSkills.add(form);
              }
              */

              return Column(
                children: <Widget>[
                  CheckboxListTile(
                    title: Text(
                      plan.title ?? "",
                      style: TextStyle(fontSize: 16),
                    ),
                    value: plan.isCheck,
                    onChanged: (bool? value) {
                      setState(() {
                        plan.isCheck = (value == true);
                      });
                      var kncsItem = new FormKNCSItem()
                        ..plan= new Plan(id: plan.id)
                        ..scores = 0
                        ..result ="Không thực hiện"
                        ..formFormat = plan.formFormat;

                      if (plan.isCheck == true) {
                        _listCheckedSkills.add(plan);
                        _listFormKNCSItems.add(kncsItem);

                        var form = FormSkillKNCSItem(
                            key: ValueKey(plan.id.toString()),
                            plan: plan,
                            onChanged: (FormKNCSItem item) {
                              //if (_listFormKNCSItems.contains(item)) {
                              //  _listFormKNCSItems.remove(item);
                              //}
                              //_listFormKNCSItems.add(item);
                              var index = _listFormKNCSItems.indexWhere((element) => element.plan!.id==plan.id);
                              _listFormKNCSItems.removeAt(index);
                              _listFormKNCSItems.add(item);

                            });

                        _listFormSkills.add(form);

                      } else {
                        var idx = _listCheckedSkills.indexOf(plan);
                        _listCheckedSkills.remove(plan);

                        var index = _listFormKNCSItems.indexWhere((element) => element.plan!.id==plan.id);
                        _listFormKNCSItems.removeAt(index);

                        _listFormSkills.removeWhere((element) => element.key == ValueKey(plan.id.toString()));
                      }
                    },
                  ),

                ],
              );
            },
            itemCount: plansList.length,
          ),

          Padding(
              padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: _listFormSkills,
              )),
        ],
      );
    }else{
      return
        Padding(
          padding: EdgeInsets.only(top: 10, left: 10, right: 10),
          child:
          Text(
            "Kế hoạch chăm sóc Người khuyết tật này chưa được thiết lập. Vui lòng liên hệ với Người chăm sóc để thiết lập kế hoạch chăm sóc trước khi thực hiên.",
            style: TextStyle(color: Colors.deepOrange),),
        );
    }
  }

  Widget wdgNKT() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            errMsgThoiGian != "" ? Text(
              errMsgThoiGian,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.redAccent),
            ) : SizedBox(),
            errMsgThoiGian != "" ? SizedBox(height: 15.0,) : SizedBox(),
            Text(
              'Đánh giá tại nhà: ' + (_member != null? _member.name.toString():""),
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            )
          ],
        )
      );
  }

  Widget wdgThoiGian() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Thời gian thực hiện:',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                print('confirm $date');
                _formDate = date;
                setState(() {
                  _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
                  map['form_date'] = _formDate.toString();
                });
              }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              '$_dateLabel',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  planSuccess() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã Cập nhật thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.check_circle,color: Colors.white),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  planChange() {
    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Cập nhật đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.report,color: Colors.white,),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = (await preferences.getInt('hbccId'))!;
  }
}
