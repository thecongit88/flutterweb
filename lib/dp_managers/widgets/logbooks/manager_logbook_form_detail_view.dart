import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class LogBookFormDetailView extends StatefulWidget {
  final Member member;
  final FormType formType;
  final int formSurveyId; // Id of record bang FormIte
  final int formId; //Id of form chi tiet
  final int memberId;

  const LogBookFormDetailView({Key? key,required this.formType,required this.member,
    required this.formSurveyId,required this.formId,required this.memberId}) : super(key: key);
  @override
  _LogBookFormViewScreenState createState() => _LogBookFormViewScreenState();
}

class _LogBookFormViewScreenState extends State<LogBookFormDetailView>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Map<String, dynamic> map = new Map<String, dynamic>();

  late FormSurveyState _formSurveyState;
  dynamic response;
  late Map forma ;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late Member _member;
  late DateTime _formDate;
  var formatStr =  "dd-MM-yyyy";
  int _formUser = 0;
  String _dateLabel = "Chọn ngày";

  String formSurveyData = "", memberName = "Chưa gán NKT";

  MemberServices _memberService = new MemberServices();
  FormTypeServices _formTypeServices = new FormTypeServices();
  FormSurveyServices _servicesSurvey = new FormSurveyServices();

  late DateTime formSurveyDate;
  late FormType _formTypeObj;
  bool showHoanThanh = true, showError = true;
  String errMsgDatao = "", errMsgThoiGian = "";

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _formSurveyState = new FormSurveyState();
    _formDate = DateTime.now();
    _formUser = widget.member!=null?widget.member.id ?? 0 : 0;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();

    formSurveyData = "";
    formSurveyDate = DateTime.now();

    /*
    _memberService.getMemberById(widget.memberId).then((Member _member) {
      setState(() {
        memberName = _member.name;
      });
    });

    _formTypeServices.getFormType(widget.formTypeId).then((FormType ft) {
      setState(() {
        formTypeObj = ft;
      });

      _servicesSurvey.getFormSurveyData(formTypeObj.formObject, widget.formId).then((value) {
        setState(() {
          formSurveyData = value; // lấy nội dung đã đánh giá từ 1 bảng dựa vào object trong bảng TormType (loại đánh giá)
        });
      });

      _servicesSurvey.getFormSurveyObject(formTypeObj.formObject, widget.formId).then((FormSurvey fs) {
        setState(() {
          formSurveyDate = DateTime.parse(fs.formDate.toString());
        });
      });
    });
  */
    super.initState();

    _formTypeObj = widget.formType;
    _servicesSurvey.getFormSurveyData(_formTypeObj.formObject, widget.formId).then((value) {
      setState(() {
        formSurveyData = value; // lấy nội dung đã đánh giá từ 1 bảng dựa vào object trong bảng TormType (loại đánh giá)
        var jsonObject = json.decode(value);
        _member = Member.fromMap(jsonObject['member']);
        _formUser = _member.id ?? 0;

        if(jsonObject['form_date'] !=null && jsonObject['form_date'].toString().isNotEmpty) {
          _formDate = DateTime.parse(jsonObject['form_date'].toString());
          _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
        }
      });
    });

    setState(() {
      showHoanThanh = true;
      showError = false;
    });

    /* chỉ được sửa form từ 26 tháng hiện tại đến hết mùng 5 tháng kế tiếp */
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      /*bool allowChangeSurveyForm = await _formSurveyState
          .isChangeSurveyFormForHBCC(formDate: _formDate,
          memberId: _formUser,
          formName: widget.formType.formObject);*/

      bool allowChangeSurveyForm = Functions.timeRangeAllowEditSurvey(formSurveyDate);
      if (allowChangeSurveyForm == false) {
        setState(() {
          showHoanThanh = false;
          showError = true;
        });

        //Functions.showSnackBarMsg(context, Functions.timeRangeAllowCreateSurvey());
        errMsgThoiGian = Functions.msgRangeAllowEditSurvey();
      } else {
        setState(() {
          showHoanThanh = true;
          showError = false;
        });
      }
    });
    /* end chỉ được sửa form từ 26 tháng hiện tại đến hết mùng 5 tháng kế tiếp */

  }

  @override
  Widget build(BuildContext ctx) {
    return ChangeNotifierProvider(
    create: (_) => _formSurveyState,
        child: new Container(
      color: Colors.white,
      child: new Column(children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(
                left: 1, right: 1, top:15),
            child:
            _formTypeObj.formFormat != null && _formTypeObj.formFormat!.isNotEmpty ?
            wdgFormLogbook(_formTypeObj.formFormat!) :
            Center(child: CircularProgressIndicator())
        ),
        Consumer<FormSurveyState>(
            builder: (context, state, child) {
              switch (state.formUpdateResponse.status) {
                case Status.LOADING:
                  return
                    Container(
                      color: Colors.white30,
                      child:
                      Center(
                        child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
                      ),
                    );
                  break;
                case Status.COMPLETED:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    return planSuccess(context);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planSuccess(context);
                        }).then((shouldUpdate) {
                      Navigator.of(context).pop();
                    });
                  });
                  return SizedBox();
                  break;
                case Status.ERROR:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return planChange();
                        });
                  });
                  return SizedBox();
                  break;
                case Status.INIT:
                  return SizedBox();
                  break;
                default:
                  return SizedBox();
                  break;
              }
            }),
      ])
        )
    );
  }

  Widget wdgFormLogbook(Map data){
    if(formSurveyData == "") return Center(
      child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
    );
    if(data == null) {
      return Center(
        child: Text('Mẫu đánh giá chưa được cấu hình.',style: TextStyle(color: Colors.red),),
      );
    }

    var _fromSurveyedJson = json.decode(formSurveyData);
    var fields = data["fields"];
    String value;

    (fields as List<dynamic>).forEach((item) {
      //print(item["value"].toString());
      value = _fromSurveyedJson[item["key"]].toString();
      if(item["type"].toString() == "RadioButton"){
        if(value == "true" || value == "false") value = value == "true" ? "1" : "0";
        else value = value.toString();
      }
      item["value"] = value;
    });
    data["fields"] = fields;
    data["form_date"] = _formDate.toString();

    return new JsonSchema(
      formMap: data,
      onChanged: (dynamic response) {
        this.response = response;
        //print(response);
      },
      actionSave: (data) {
          if (showHoanThanh == false) return;

          //print(data);
          var fields = data["fields"];
          String objText = '{';
          fields.forEach((field) {
            objText += '"${field["key"]}":${field["value"].toString()},';
            map["${field["key"]}"] = field["value"].toString();
          });
          objText += '}';

          map["form_type"] = _formTypeObj.id;
          map["member"] = _member.id;
          map["form_date"] = _formDate.toString();

          _formSurveyState.update(
              map,widget.formId,_formTypeObj.formObject!);
      },
      selectUsers: wdgNKT(),
      chooseDate: wdgThoiGian(),
      buttonSave:
      showHoanThanh == true ?
        Container(
        child: Center(
          child: Padding(
              padding: const EdgeInsets.only(
                  left: 0, bottom: 25, top: 16, right: 0),
              child:
              InkWell(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: ManagerAppTheme.nearlyBlue,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ManagerAppTheme
                                    .nearlyBlue
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color:
                              ManagerAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),

              )
          ),
        ),
      )
          :
      SizedBox()

    /* nút hoàn thành bị mờ và vô hiệu hóa  */
    /*
        Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: Center(
          child: Padding(
              padding: const EdgeInsets.only(
                  left: 0, bottom: 25, top: 16, right: 0),
              child:Container(
                  height: 48,
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey
                              .withOpacity(0.5),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Hoàn thành',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0.0,
                        color: ManagerAppTheme.nearlyWhite,
                      ),
                    ),
                  ),
                ),
              ),
        ),
      ),
    */
      //end container btnSave
    );
  }

  Widget wdgNKT() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 0.0),
        child:
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            errMsgThoiGian != "" ? Text(
              errMsgThoiGian,
              textAlign: TextAlign.left,
              style:
              TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.redAccent),
            ) : SizedBox(),
            errMsgThoiGian != "" ? SizedBox(height: 15.0,) : SizedBox(),
            Text(
              'Đánh giá tại nhà: ' + _member.name.toString(),
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            /*
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _listMembers,
              value: _formUser,
              validator: (int value) {
                if (value == null || value<= 0) {
                  return 'Không để trống mục này !';
                }else{
                  return '';
                }
              },
              onChanged: (value) {
                setState(() {
                  _formUser = value;
                });
              },
            )
            */

          ],
        ),);
  }

  Widget wdgThoiGian() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Thời gian thực hiện:',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                    print('confirm $date');
                    _formDate = date;
                    setState(() {
                      _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
                      map["form_date"] = _formDate.toString();
                    });
                  }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              '$_dateLabel',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

   planSuccess(context){

     Navigator.of(context).pop();
     WidgetsBinding.instance.addPostFrameCallback((_) {
       Flushbar(
         message: 'Bạn đã Cập nhật thành công đánh giá!',
         backgroundColor: Colors.green,
         duration: Duration(seconds: 5),
         icon: Icon(Icons.check_circle,color: Colors.white),
         flushbarPosition: FlushbarPosition.TOP,
       ).show(context);
     });
     return;
  }

   planChange(){

    Navigator.of(context).pop();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Cập nhật đánh giá lỗi! Vui lòng thực hiện lại.',
        backgroundColor: Colors.redAccent,
        duration: Duration(seconds: 5),
        icon: Icon(Icons.report,color: Colors.white,),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    return;
  }
}

