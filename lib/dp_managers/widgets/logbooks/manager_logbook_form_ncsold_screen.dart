import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grouped_buttons_ns/grouped_buttons_ns.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/main.dart';

class LogBookFormNCSScreen extends StatefulWidget {
  @override
  _LogBookFormNCSScreenState createState() => _LogBookFormNCSScreenState();
}

class _LogBookFormNCSScreenState extends State<LogBookFormNCSScreen>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  late DateTime _date;
  String _dateLabel = "Chọn ngày";
  String _picked = "Không cải thiện - giữ nguyên như cũ";
  String _pickedVetloet = "Giữ nguyên như cũ";
  String _vetloet = "Không";
  String _kehoachBanDau = "Có";

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    _date = DateTime.now();
    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {

    return
      Container(
        color: HexColor("#F3F4F9"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  color: ManagerAppTheme.nearlyWhite,
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0)),
                ),
                margin: const EdgeInsets.only(top: 10),
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(
                            left: 25.0, right: 25.0, top: 20.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Thời gian thực hiện:',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 25.0, right: 25.0, top: 10.0),
                      child:
                      ElevatedButton(
                        onPressed: () {
                          DatePicker.showDatePicker(context,
                              theme: DatePickerTheme(
                                containerHeight: 210.0,
                              ),
                              showTitleActions: true,
                              minTime: DateTime(2000, 1, 1),
                              maxTime: DateTime.now(),
                              onConfirm: (date) {
                                print('confirm $date');
                                _date = date;
                                setState(() {
                                  _dateLabel = '${date.day}/${date.month}/${date.year}';
                                });
                              },
                              currentTime: DateTime.now(), locale: LocaleType.vi);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.date_range,
                                          size: 18.0,
                                        ),
                                        SizedBox(width: 10,),
                                        Text(
                                          '$_dateLabel',
                                          style: TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 15.0),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Icon(Icons.arrow_drop_down)
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 25.0, right: 25.0, top: 25.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Tình trạng hoạt động hàng ngày của NKT thế nào?',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                new
                                Container(
                                  padding: EdgeInsets.only(top:5),
                                  constraints: BoxConstraints.expand(height: 25,
                                      width: MediaQuery.of(context).size.width-50),
                                  child: Text(
                                      '(Sinh hoạt hàng ngày là ăn uống, tắm rữa, vệ sinh…)',
                                      softWrap: true,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.normal,fontStyle: FontStyle.italic
                                      ),
                                      overflow: TextOverflow.ellipsis
                                  ),
                                )
                              ],
                            ),
                          ],
                        )),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 0, right: 25.0, top: 2.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Flexible(
                              child:
                              new RadioButtonGroup(
                                orientation: GroupedButtonsOrientation.VERTICAL,
                                onSelected: (String selected) => setState((){
                                  _picked = selected;
                                }),
                                picked: _picked,
                                labels: <String>[
                                  "Tiến triển xấu hơn",
                                  "Không cải thiện - giữ nguyên như cũ",
                                  "Cải thiện ít",
                                  "Cải thiện vừa",
                                  "Cải thiện nhiều",
                                ],
                              ),
                            ),
                          ],
                        )),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 25.0, right: 25.0, top: 25.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'NKT có vết loét không?',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        )),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 10, right: 25.0, top: 2.0),
                      child:
                      new RadioButtonGroup(
                        orientation: GroupedButtonsOrientation.HORIZONTAL,
                        onSelected: (String selected) => setState((){
                          _vetloet = selected;
                        }),
                        picked: _vetloet,
                        labels: <String>["Có", "Không",],
                        itemBuilder: (Radio radio,Text label, int index) {
                          return Row(
                            children: <Widget>[
                              radio,label,SizedBox(width: 10,)
                            ],
                          );
                        },
                      ),
                    ),
                    _vetloet=="Không"?
                        SizedBox(height: 0,)
                        :
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Tình trạng vết loét của NKT thế nào?',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 0, right: 25.0, top: 2.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Flexible(
                                  child:
                                  new RadioButtonGroup(
                                    orientation: GroupedButtonsOrientation.VERTICAL,
                                    onSelected: (String selected) => setState((){
                                      _pickedVetloet = selected;
                                    }),
                                    picked: _pickedVetloet,
                                    labels: <String>[
                                      "Tiến triển xấu hơn",
                                      "Giữ nguyên như cũ",
                                      "Cải thiện ít",
                                      "Cải thiện vừa",
                                      "Cải thiện nhiều",
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 25.0, right: 25.0, top: 25.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                new Text(
                                  'Bạn thực hiện đúng kế hoạch ban đầu không?',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        )),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 10.0, right: 25.0, top: 2.0),
                        child:
                        new RadioButtonGroup(
                          orientation: GroupedButtonsOrientation.HORIZONTAL,
                          onSelected: (String selected) => setState((){
                            _kehoachBanDau = selected;
                          }),
                          picked: _kehoachBanDau,
                          labels: <String>["Có", "Không",],
                          itemBuilder: (Radio radio,Text label, int index) {
                            return Row(
                              children: <Widget>[
                                radio,label,SizedBox(width: 10,)
                              ],
                            );
                          },
                        ),
                    ),
                    AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: opacity3,
                      child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16, bottom: 25, top: 16, right: 16),
                          child:
                          InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 48,
                                    decoration: BoxDecoration(
                                      color: ManagerAppTheme.nearlyBlue,
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(16.0),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        BoxShadow(
                                            color: ManagerAppTheme
                                                .nearlyBlue
                                                .withOpacity(0.5),
                                            offset: const Offset(1.1, 1.1),
                                            blurRadius: 10.0),
                                      ],
                                    ),
                                    child: Center(
                                      child: Text(
                                        'Hoàn thành',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18,
                                          letterSpacing: 0.0,
                                          color:
                                          ManagerAppTheme.nearlyWhite,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            onTap: (){
                              finishForm();
                            },
                          )

                      ),
                    ),
                  ],
                )),
          ],
        ),
      );
  }

  void finishForm(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  Widget planSuccess(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:

      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: ManagerAppTheme.nearlyBlue,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.card_giftcard,color: Colors.green, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Chúc mừng bạn, tháng này đã thực hiện đúng kế hoạch đề ra!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: ManagerAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 10),
                  child:
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.send,color: ManagerAppTheme.nearlyWhite),
                    label: Text("Đóng", style: TextStyle(color: ManagerAppTheme.nearlyWhite),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget planChange(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: ManagerAppTheme.nearlyYellow,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.info,color: ManagerAppTheme.nearlyYellow, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Tháng này bạn thực hiện khác kế hoạch ban đầu đề ra!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: ManagerAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child:
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: new TextField(
                        maxLines: 3,
                        decoration: const InputDecoration(
                          hintText: "Lý do thay đổi của bạn là gì?",
                        )),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:10, bottom: 10),
                        child:
                        ElevatedButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                            },
                          icon: Icon(Icons.send,color: ManagerAppTheme.nearlyWhite),
                          label: Text("Gửi", style: TextStyle(color: ManagerAppTheme.nearlyWhite),),
                        ),
                      )
                    ],
                  ),

                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
