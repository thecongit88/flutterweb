import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/course_info_screen.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';

class LastestVideoListView extends StatefulWidget {
  const LastestVideoListView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;

  @override
  _LastestVideoListViewState createState() => _LastestVideoListViewState();
}

class _LastestVideoListViewState extends State<LastestVideoListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0),
      child: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return ListView.separated(
              shrinkWrap: true,
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(),
              itemCount: Member.popularMemberList.length,
              itemBuilder: (BuildContext context, int index) {
                final int count = Member.popularMemberList.length;
                final Animation<double> animation =
                Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                        parent: animationController,
                        curve: Interval((1 / count) * index, 1.0,
                            curve: Curves.fastOutSlowIn)));
                animationController.forward();
                  Member video =    Member.popularMemberList[index];
                  return ListTile(
                    leading: Image.asset(video.avatar ?? ""),
                    title: Padding(
                        padding: EdgeInsets.only(top: 7),
                        child: Text(
                        video.name ?? "",
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        )),
                    subtitle:
                    Padding(padding:EdgeInsets.only(top:15),
                    child: Text(video.createdDate ?? "",style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      letterSpacing: 0.27,
                      color: ManagerAppTheme.grey,
                    ),)),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => CourseInfoScreen(),
                        ),
                      );
                    },
                  );
              },
            );
          }
        },
      ),
    );
  }
}

