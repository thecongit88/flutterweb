import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'dart:io';
import 'package:hmhapp/dp_managers/models/skill.dart';
import 'package:hmhapp/dp_managers/plan_info_screen.dart';
import 'package:hmhapp/dp_managers/widgets/manager_report_view.dart';
import 'package:hmhapp/dp_managers/skill_list_view.dart';
import 'package:hmhapp/main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as pPath;

class MemberInfoScreen extends StatefulWidget {
  @override
  _MemberInfoScreenState createState() => _MemberInfoScreenState();
}

class _MemberInfoScreenState extends State<MemberInfoScreen>
    with TickerProviderStateMixin {
  int _currentSelection = 0;
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  late File _imageFile;
  late String _avatar;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    _avatar = "assets/design_course/userImage.png";
    setData();
    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  Future<void> _pickImage(ImageSource source) async {
    final ImagePicker _picker = ImagePicker();
    final XFile? selected = await _picker.pickImage(source: source);
    final appDir = await pPath.getApplicationDocumentsDirectory();
    final fileName = path.basename(selected!.path);
    //final imageFile = await selected.copy('${appDir.path}/$fileName');
    final imageFile = await  File(selected!.path).copy('${appDir.path}/$fileName');

    setState(() {
      _imageFile = imageFile;
      _avatar = "assets/images/avatar01.png";
    });

  }

  Future<File> _getLocalFile(String filename) async {
    String dir = (await pPath.getApplicationDocumentsDirectory()).path;
    File f = new File('$dir/$filename');
    return f;
  }

  Map<int, Widget> _children = {
    0: Text('Người KT'),
    1: Text('Người CSC'),
    2: Text('Kỹ năng')
  };

  @override
  Widget build(BuildContext context) {
    Map<int, Widget> map = new Map();
    List<Widget> childWidgets = <Widget>[];
    int selectedIndex = 0;
    childWidgets.add(wdgNKT());
    childWidgets.add(wdgNCSC());
    childWidgets.add(wdgPlan());

    return new Scaffold(
        appBar: AppBar(
          backgroundColor: ManagerAppTheme.nearlyBlue,
          title: Text("Cá nhân"),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 15,top:10),
              child:
            InkWell(
              child: Icon(Icons.pie_chart),
              onTap: (){

              },
            ),
            )
          ],
        ),
        backgroundColor: Colors.transparent,
        body: new Container(
          color: Colors.white,
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  new Container(
                    height: 120.0,
                    color: Colors.white,
                    child: new Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 15.0),
                          child:
                              new Stack(fit: StackFit.loose, children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new
                                InkWell(
                                  child:
                                  _imageFile != null?
                                Container(
                                    width: 80.0,
                                    height: 80.0,
                                    child: Image.file(_imageFile)
                                ):
                                new Container(
                                  width: 80.0,
                                  height: 80.0,
                                  decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                  image: new AssetImage(_avatar), fit: BoxFit.cover)),
                                ),
                                  onTap: (){
                                    _pickImage(ImageSource.gallery);
                                  },
                                )
                              ],
                            ),
                            Padding(
                                padding:
                                    EdgeInsets.only(top: 40.0, right: 50.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new CircleAvatar(
                                      backgroundColor: Colors.red,
                                      radius: 20.0,
                                      child: new Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                )),
                          ]),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: MaterialSegmentedControl(
                      children: _children,
                      verticalOffset: 10,
                      borderColor: Colors.lightBlue,
                      selectedColor: Colors.lightBlue,
                      unselectedColor: Colors.white,
                      selectionIndex: _currentSelection,
                      borderRadius: 20.0,
                      onSegmentChosen: (int? index) {
                        setState(() {
                          _currentSelection = index ?? 0;
                        });
                      },
                    ),
                  ),
                  new Container(child: childWidgets[_currentSelection]),
                ],
              ),
            ],
          ),
        ));
  }

  Widget wdgNKT() {
    return new Container(
      color: Color(0xffFFFFFF),
      child: Padding(
        padding: EdgeInsets.only(bottom: 25.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text(
                    'Thông tin người khuyết tật',
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  new Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: Divider(
                        color: Colors.lightBlue,
                        height: 1,
                      )),
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Mã:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("0123123ABC"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Họ và tên:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("Hoàng Văn Nam"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Thời gian bắt đầu dự án:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("01/01/2020"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Giới tính:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("Nam"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Dân tộc:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("Kinh"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Ngày tháng năm sinh:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("20/01/1990"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Số tuổi:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("30"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Dạng khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("Khuyết tật vận động"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Thời điểm khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("Từ khi sinh"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Năm khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("1990"))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Text(
                          'NKT có giấy xác nhận chất độc da cam :',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Text(
                      "Có",
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget wdgNCSC() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Text(
                'Thông tin người chăm sóc chính',
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
              new Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: Divider(
                    color: Colors.lightBlue,
                    height: 1,
                  )),
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Họ và tên NCSC:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("Hoàng Văn Bắc"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Năm sinh NCSC:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("1970"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Giới tính NCSC:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("Nam"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Dân tộc NCSC:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("Kinh"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Tình trạng hôn nhân NCSC:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child: Text("- Độc thân chưa kết hôn"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Trình độ học vấn:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("Trung học phổ thông"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Nghề nghiệp chính:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text("Làm ruộng"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Mối quan hệ của NCSC với NKT:',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child: Text("- Anh/chị/em ruột"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Vai trò của NCSC trong gia đình?',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child: Text("- Chỉ chăm sóc NKT")),
                Padding(
                    padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child:
                        Text("- Là người lao động tạo thu nhập cho gia đình"))
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Nhà bạn có internet không?',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ],
            )),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Text(
                  "- Có",
                )
              ],
            )),
        Padding(
          padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Text(
                'Bạn sử dụng điện thoại di động?',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                  child: Text("- Có")),
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Nếu Có, Điện thoại của bạn thuộc loại nào?',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child: Text("- Điện thoại có thể tiếp cận internet"))
              ],
            )),
      ],
    );
  }

  Widget wdgPlan() {
    //List<String> list = new List<String>();
    //list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    //list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    List<UserSkill> list = UserSkill.userSkillList;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    'Kế hoạch chăm sóc',
                    style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    child: Icon(Icons.add_circle, color: ManagerAppTheme.nearlyBlue,),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => SkillListView(),
                          ));
                    },
                  )
                ],
              ),
              new Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: Divider(
                    color: Colors.lightBlue,
                    height: 1,
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 25, right: 25.0, top: 25.0),
          child: ListView.separated(
            shrinkWrap: true,
            separatorBuilder: (context, index) => Divider(
              color: ManagerAppTheme.nearlyWhite,
            ),
            itemCount: list.length,
            itemBuilder: (context, index) {
              UserSkill skill = list[index];
              return Container(
                decoration: BoxDecoration(
                  color: HexColor('#F8FAFB'),
                  borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                  // border: new Border.all(
                  //     color: ManagerAppTheme.notWhite),
                ),
                child: ListTile(
                  title: Text(skill.title, style: TextStyle(height: 1.5)),
                  trailing:
                  skill.isHavePlan?
                  Icon(
                    Icons.mode_edit,
                    color: ManagerAppTheme.nearlyBlue,
                  ):
                  Icon(
                    Icons.add,
                    color: ManagerAppTheme.nearlyBlue,
                  ),
                  onLongPress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => PlanInfoScreen(),
                        ));
                  },
                ),
              );
            }
          ),
        ),
      ],
    );
  }

  Widget getTimeBoxUI(String text1, String txt2) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: ManagerAppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: ManagerAppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: ManagerAppTheme.nearlyBlue,
                ),
              ),
              Text(
                txt2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w200,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: ManagerAppTheme.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
