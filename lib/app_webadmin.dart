import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/route_generator.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/app_state.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MyAppWebAdmin extends StatefulWidget {
  final Widget? defaultHome;
  const MyAppWebAdmin({Key? key, this.defaultHome}) : super(key: key);

  @override
  _MyAppWebAdminState createState() => _MyAppWebAdminState();
}

class _MyAppWebAdminState extends State<MyAppWebAdmin> {

  late AuthState authState;

  @override
  void initState() {
    super.initState();
    authState = AuthState();
    authState.getUser();
  }

  @override
  Widget build(BuildContext context) {

    return
      ChangeNotifierProvider(
          create: (_) => authState,
      child:
      MaterialApp(
        localizationsDelegates: [
          RefreshLocalizations.delegate,
        ],
      title: 'HMH - Chăm sóc sức khoẻ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
        textTheme: TextTheme(
          subtitle1: TextStyle(color: Colors.white,fontSize: 20),
        ),
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
        primaryColor: ManagerAppTheme.nearlyBlue,
        primarySwatch: Colors.blue,
        textTheme: AppTheme.textTheme,
      ),
      home:
      Consumer<AuthState>(builder: (context, state, child) {
      return buildScreen(state,context);
      }),
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    ));

  }

  Widget buildScreen(AuthState state,BuildContext ctx) {
    switch (state.authResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        if(state.authResponse.data != null && state.authResponse.data!.isNotEmpty){
          return widget.defaultHome!;
        } else {
          return SignInScreen();
        }
        break;
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.authResponse.message,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return SizedBox();
        break;
      case Status.INIT:
        return SizedBox();
        break;
      default:
        return SizedBox();
        break;
    }
  }

}


