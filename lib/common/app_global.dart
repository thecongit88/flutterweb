import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'app_colors.dart';

void showToast(String message) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: AppColors.grey,
      textColor: Colors.white,
      fontSize: 16.0);
}

void showSuccessToast(BuildContext context, String message) {
  Flushbar(
    message: message,
    backgroundColor: Colors.green,
    icon: const Icon(Icons.check_circle,color: Colors.white,),
    duration: const Duration(seconds: 5),
    flushbarPosition: FlushbarPosition.TOP,
  ).show(context);

  /*Get.snackbar("Thông báo", message,
      backgroundColor: Colors.green,
      duration: Duration(seconds: 3),
      colorText: Colors.white,
    icon: Icon(
      Icons.check_circle,
      color: Colors.white,
      size: 30.0,
    ),
  );*/
}

void showErrorToast(BuildContext context, String message) {
  Flushbar(
    message: message,
    backgroundColor: AppColors.red,
    icon: const Icon(Icons.highlight_off,color: Colors.white,),
    duration: const Duration(seconds: 5),
    flushbarPosition: FlushbarPosition.TOP,
  ).show(context);

  /*Get.snackbar("Báo lỗi", message,
      backgroundColor: AppColors.red,
      duration: Duration(seconds: 3),
      colorText: Colors.white,
    icon: Icon(
      Icons.highlight_off,
      color: Colors.white,
      size: 30.0,
    ),
  );*/
}

void showWarningToast(BuildContext context, String message) {
  Flushbar(
    message: message,
    backgroundColor: Colors.orange,
    icon: const Icon(Icons.info_outlined,color: Colors.white,),
    duration: const Duration(seconds: 5),
    flushbarPosition: FlushbarPosition.TOP,
  ).show(context);

  /*Get.snackbar("Cảnh báo", message,
      backgroundColor: Colors.orange,
      duration: Duration(seconds: 3),
      colorText: Colors.white,
    icon: Icon(
      Icons.info_outlined,
      color: Colors.white,
      size: 30.0,
    ),
  );*/
}

