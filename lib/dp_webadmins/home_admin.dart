import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/course_info_screen.dart';
import 'package:hmhapp/dp_members/lastest_video_list_view.dart';
import 'package:hmhapp/widgets/footer.dart';

class HomeAdminScreen extends StatefulWidget {
  @override
  _HomeAdminScreenState createState() => _HomeAdminScreenState();
}

class _HomeAdminScreenState extends State<HomeAdminScreen> {
  late AnimationController animationController;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Scaffold(
        appBar: AppBar(
        backgroundColor: Colors.blue,
        leading: InkWell(
          borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Video hướng dẫn"),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20),
              child:
              Icon(Icons.search),
            )
          ],
      ),
        backgroundColor: Colors.transparent,
        body:
        Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      getLastestVideos(),
                      Footer()
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getLastestVideos() {
    return LastestVideoListView(
      callBack: () {
        moveTo();
      },
    );
  }

  void moveTo() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => CourseInfoScreen(),
      ),
    );
  }

}
