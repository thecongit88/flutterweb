import 'dart:collection';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthState extends ChangeNotifier {

  AuthServices _services = AuthServices();
  bool isLoading = false;

  String errorMessage = "";
  bool errorOccured = false;
  ApiResponse<User> userResponse = ApiResponse.init("");
  ApiResponse<String> authResponse = ApiResponse.init("");
  User _user = User();
  String _token = "";


  User get user => _user;
  ApiResponse<User> userRegisterResponse = ApiResponse.init("Đang xử lý...");

  ApiResponse<User> changePassResponse = ApiResponse.init("Đang xử lý...");

  AuthState() {
    isLoading = true;
    _services = AuthServices();
    userResponse = ApiResponse.init("Đang xử lý...");
    userRegisterResponse = ApiResponse.init("Đang xử lý...");
    changePassResponse = ApiResponse.init("Đang xử lý...");
  }

  registerUser(UserRegister user) async {
    notifyListeners();
    userResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      _user = await _services.signUp(user);
      userResponse = ApiResponse.completed(_user);
    }
    catch (e) {
      userResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  signIn(UserLogin user) async {
    notifyListeners();
    userResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      print("User Data");
      print(json.encode(user));
      _user = await _services.signIn(user);
      if(_user.inValid == false) {
        userResponse = ApiResponse.error("Tài khoản này không tồn tại");
      } else if(_user.approved == false) {
        userResponse = ApiResponse.error("Tài khoản này đã tồn tại nhưng chưa được kích hoạt. Vui lòng liên hệ với quản trị để được kích hoạt.");
      } else {
        userResponse = ApiResponse.completed(_user);
      }
    }
    catch (e) {
      userResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getUser() async{
     authResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      _token = await _services.getCurentUser();
      authResponse = ApiResponse.completed(_token);
    }
    catch (e) {
      authResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();

  }

  setInit(){
    userResponse = ApiResponse.init("Đang xử lý...");
    notifyListeners();
  }

  setChangePasswordInit(){
    changePassResponse = ApiResponse.init("Đang xử lý...");
    notifyListeners();
  }

  changePassword(UserChangePassword user) async {
    changePassResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      _user = await _services.changePassword(user);
      changePassResponse = ApiResponse.completed(_user);
    }
    catch (e) {
      changePassResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


}
