import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/job_type.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/disability_start_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/job_type_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DisabilityStartState extends ChangeNotifier{
  late DisabilityStartServices _disabilityStartServices;
  late ApiResponse<List<DisabilityStart>> disabilityStartResponseList;
  late List<DisabilityStart> disabilityStarts;
  late int disabilityStartId;

  DisabilityStartState() {
    _disabilityStartServices = new DisabilityStartServices();
    disabilityStarts = <DisabilityStart>[];
  }

  listDisabilityStarts() async {
    disabilityStartResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      disabilityStarts = await _disabilityStartServices.listDisabilityStarts();
      disabilityStartResponseList = ApiResponse.completed(disabilityStarts);
    } catch(e) {
      disabilityStartResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setDisabilityStartId(int gId) {
    this.disabilityStartId = gId;
    notifyListeners();
  }

  getDisabilityStartId() {
    return this.disabilityStartId;
  }
}