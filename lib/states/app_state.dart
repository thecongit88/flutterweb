import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppState extends ChangeNotifier {
  bool? isDark = false;
  bool? isUnauthorized = true;
  late int _selectedScreenIndex = 0;
  Widget? _selectedScreen;
  late List _screens;

  AppState({required initialScreen, required screens}) {
    _selectedScreen = initialScreen;
    _screens = screens;
    _reteriveThemePreference();
  }

  Widget? get selectedScreen => _selectedScreen;

  List? get screens => _screens;


  _setTheme(ThemeData themeData) {
    notifyListeners();
    _saveThemePreference();
  }

  int? get selectedScreenIndex => _selectedScreenIndex;

  setScreenIndex(int pos) {
    _selectedScreenIndex = pos;
    _selectedScreen = _screens![pos];
    notifyListeners();
  }

  changeScreenTo(Widget widget) {
    _selectedScreenIndex = _screens!.indexOf(widget);
    _selectedScreen = _screens[_selectedScreenIndex];
    notifyListeners();
  }

  setDarkTheme() {
    isDark = true;
   // _setTheme(Constants.darkTheme);
  }

  setLightTheme() {
    isDark = false;
   // _setTheme(Constants.lightTheme);
  }

  _saveThemePreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(Constants.kLocalKey['isDarkTheme']!, isDark == true);
  }

  _reteriveThemePreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.containsKey(Constants.kLocalKey['isDarkTheme']!)) {
      isDark = preferences.getBool(Constants.kLocalKey['isDarkTheme']!);
      isDark  == true ? setDarkTheme() : setLightTheme();
    }
  }

  void navigateToHome() {
    setScreenIndex(0);
  }

  void navigateToCart() {
    setScreenIndex(2);
  }
}
