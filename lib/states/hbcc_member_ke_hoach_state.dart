import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:format/format.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/services/hbcc_member_care_plan_item_services.dart';
import 'package:hmhapp/services/hbcc_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../common/app_global.dart';
import '../dp_managers/models/hbcc_ke_hoach/add_care_plan_item.dart';
import '../dp_managers/models/hbcc_ke_hoach/care_plan_item.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';
import '../services/hbcc_member_ke_hoach_services.dart';

class HbccMemberKeHoachState extends ChangeNotifier{
  String tieuDeKeHoach = "";
  late ApiResponse<int> hbccThemKeHoachResponse;
  late ApiResponse<int> hbccUpdateKeHoachResponse;
  HbccMemberKeHoachServices hbccKeHoachServices = HbccMemberKeHoachServices();
  HbccMemberCarePlanItemServices carePlanItemServices = HbccMemberCarePlanItemServices();
  int result = 0, thangSelected = 0;

  List<AddCarePlanItem> listPlanItemModifed = [], listPlanItemOrigin = [];
  List<CarePlanItem> listPlanItem = [];

  late ApiResponse<List<HbccItemKeHoach>> listHbccMemberKeHoachResponse;
  late ApiResponse<List<HbccItemKeHoach>> listHbccKeHoachResponse;
  late ApiResponse<List<CarePlanItem>> listCarePlanItemResponse;
  int idKeHoach = 0;

  //phan trang
  List<HbccItemKeHoach> listKeHoach = [];
  int page = 1;
  RefreshController refreshController =
  RefreshController(initialRefresh: false);

  ///Thao tac vơi kế hoạch
  setThang(int _thangSelected) {
    this.thangSelected = _thangSelected;
    notifyListeners();
  }

  getThang() {
    return this.thangSelected;
  }

  setTieuDeKeHoach({int? nam, int? thang}) {
    nam ??= DateTime.now().year;
    thang ??= DateTime.now().month;
    tieuDeKeHoach = "Kế hoạch chăm sóc tháng $thang năm $nam";
    notifyListeners();
  }

  getTieuDeKeHoach() {
    return tieuDeKeHoach;
  }

  int daysInMonth(int month, int year){
    var firstDayThisMonth = DateTime(year, month, 1);
    var firstDayNextMonth = DateTime(firstDayThisMonth.year, firstDayThisMonth.month + 1, 1);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }

  hbccThemKeHoach(BuildContext context, HbccThemKeHoachModel keHoach) async {
    bool success = false;
    notifyListeners();
    hbccThemKeHoachResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccKeHoachServices.create(keHoach);
      idKeHoach = result["id"] ?? 0;
      if(idKeHoach > 0) {
        keHoach.code = 'KH{:06d}'.format(idKeHoach);
        final result_update = await hbccKeHoachServices.update(keHoach, idKeHoach);
        if((result_update["id"] ?? 0) > 0) {
          success = true;
        }
        if(success == true) {
          //them care plan item
          await Future.forEach(listPlanItemModifed, (AddCarePlanItem item) async {
            await createCarePlanItem(item, "$idKeHoach");
          });
          //quay ve man hinh truoc
          Navigator.pop(context, 1);
          showSuccessToast(context, "Thêm kế hoạch thành công!");
        }
      }
      hbccThemKeHoachResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccThemKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  hbccUpdateKeHoach(BuildContext context, HbccThemKeHoachModel keHoach, int id) async {
    notifyListeners();
    hbccUpdateKeHoachResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccKeHoachServices.update(keHoach, id);
      idKeHoach = result["id"] ?? 0;
      if(idKeHoach > 0) {
        //get ra những chi tiết kế hoạch chưa modify
        listPlanItem = await carePlanItemServices.listCarePlanItems(idKeHoach);
        listPlanItemOrigin = [];
        listPlanItem.forEach((parentItem) {
          var item = AddCarePlanItem();
          item.id = parentItem.id ?? 0;
          item.title = parentItem.title;
          item.itemDate = parentItem.itemDate;
          item.itemTime = parentItem.itemTime;
          item.itemTime = parentItem.itemTime;
          item.expectedResult = parentItem.expectedResult;
          item.status = parentItem.status;
          listPlanItemOrigin.add(item);
        });

        //xoa het nhung ke hoach cu
        await Future.forEach(listPlanItemOrigin, (AddCarePlanItem it) async {
          await carePlanItemServices.delete(it.id);
        });
        //them care plan item
        await Future.forEach(listPlanItemModifed, (AddCarePlanItem it) async {
          await createCarePlanItem(it, "$idKeHoach");
        });
        Navigator.pop(context, 1);
        showSuccessToast(context, "Cập nhật kế hoạch thành công!");
      }
      hbccUpdateKeHoachResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccUpdateKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  listKeHoachHbccMember({int? memberId, bool isLoadMore = false}) async {
    if (!isLoadMore) {
      page = 1;
      listKeHoach = [];
      refreshController.resetNoData();
      refreshController.refreshCompleted();
    } else {}

    try {
      final result = await hbccKeHoachServices.listKeHoachMember(memberId: memberId, pg: page);
      if (result.isNotEmpty == true) {
        listKeHoach.addAll(result);
        page++;
        refreshController.loadComplete();
      } else {
        refreshController.loadNoData();
      }
      listHbccMemberKeHoachResponse = ApiResponse.completed(listKeHoach);
    } catch(e) {
      listHbccMemberKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  hbccDeleteKeHoach(BuildContext context, int id) async {
    hbccThemKeHoachResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccKeHoachServices.delete(id);
      if((result["id"] ?? 0) > 0) {
        Navigator.pop(context);
        Navigator.pop(context);
        showSuccessToast(context, "Đã xoá kế hoạch thành công!");
      }
      hbccThemKeHoachResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccThemKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  ///Thao tac voi danh sach CarePlanItem khi thêm 1 kế hoạch
  refreshUI() {
    notifyListeners();
  }

  addListCarePlanItem(AddCarePlanItem item) {
    listPlanItemModifed.add(item);
    notifyListeners();
  }

  updateListCarePlanItem(int index, AddCarePlanItem item) {
    listPlanItemModifed[index] = item;
    notifyListeners();
  }

  removeItemInListCarePlanItem(int index) {
    return listPlanItemModifed.removeAt(index);
  }

  //Thao tac voi bang CarePlanItems khi cập nhật 1 kế hoạch
  getListCarePlanItem(int planId) async {
    listCarePlanItemResponse = ApiResponse.loading("Đang tải ...");
    notifyListeners();
    try {
      listPlanItem = await carePlanItemServices.listCarePlanItems(planId);

      //lay danh sach chi tiet ke hoach sau khi view/edit plan
      listPlanItemModifed = [];
      listPlanItem.forEach((parentItem) {
        var item = AddCarePlanItem();
        item.id = parentItem.id ?? 0;
        item.title = parentItem.title;
        item.itemDate = parentItem.itemDate;
        item.itemTime = parentItem.itemTime;
        item.itemTime = parentItem.itemTime;
        item.expectedResult = parentItem.expectedResult;
        item.status = parentItem.status;
        listPlanItemModifed.add(item);
      });

      //lay danh sach chi tiet ke hoach sau khi view/edit plan
      listCarePlanItemResponse = ApiResponse.completed(listPlanItem);
    } catch(e) {
      listCarePlanItemResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  disposeCarePlanItem() {
    listCarePlanItemResponse = ApiResponse.init("");
  }

  createCarePlanItem(AddCarePlanItem item, String planId) async {
    item.plan = planId;
    final result = await carePlanItemServices.create(item);
  }

  deleteCarePlanItem(BuildContext context, int id) async {
    hbccThemKeHoachResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await carePlanItemServices.delete(id);
      if((result["id"] ?? 0) > 0) {
        showSuccessToast(context, "Đã xoá chi tiết kế hoạch thành công!");
      }
      hbccThemKeHoachResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccThemKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  removeCarePlanItemFromDataBase(BuildContext context, int id) async {
    final result = await carePlanItemServices.delete(id);
    if((result["id"] ?? 0) > 0) {
      Navigator.pop(context);
      Navigator.pop(context);
      showSuccessToast(context, "Đã xoá chi tiết kế hoạch thành công!");
    }
  }

  setListCarePlanItemModified(List<AddCarePlanItem> _listPlanItem) {
    listPlanItemModifed = _listPlanItem;
  }
}