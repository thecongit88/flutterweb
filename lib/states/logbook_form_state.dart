import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/models/logbookType.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogBookFormState extends ChangeNotifier {
  late int _selectedScreenIndex = 0;
  late Widget _selectedScreen;
  late List _screens;

  late ApiResponse<int> formResponse;
  late FormSurveyServices _formSurveyServices;

  LogBookFormState({@required initialScreen, @required screens}) {
    _selectedScreen = initialScreen;
    _screens = screens;
    _formSurveyServices = FormSurveyServices();
  }

  Widget get selectedScreen => _selectedScreen;

  List get screens => _screens;

  int get selectedScreenIndex => _selectedScreenIndex;

  setScreenIndex(int pos) {
    _selectedScreenIndex = pos;
    _selectedScreen = _screens[pos];
    notifyListeners();
  }

  changeScreenTo(LogbookType logbookType) {
    _selectedScreenIndex = 0;

    if(logbookType == LogbookType.logBookDANHGIACHUNG){
      _selectedScreenIndex = 0;
    }else if(logbookType == LogbookType.logBookDANHGIAKYNANG){
      _selectedScreenIndex = 1;
    }else if(logbookType == LogbookType.logBookDANHGIAKHANANG){
      _selectedScreenIndex = 2;
    }else if(logbookType == LogbookType.logBookDANHGIATIENBO){
      _selectedScreenIndex = 3;
    }else if(logbookType == LogbookType.logBookDANHGIAMOITRUONG){
      _selectedScreenIndex = 4;
    }
    _selectedScreen = _screens[_selectedScreenIndex];
    notifyListeners();
  }

  void navigateToHome() {
    setScreenIndex(0);
  }


  create(Map<String, dynamic> form,String formType) async {
    notifyListeners();
    formResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var a = await _formSurveyServices.create(form,formType);
      formResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


}
