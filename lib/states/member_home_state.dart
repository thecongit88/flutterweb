import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/post_category_services.dart';
import 'package:hmhapp/services/post_services.dart';
import 'package:hmhapp/services/video_list_view_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberHomeState extends ChangeNotifier {

  late PostCategoryServices _services;
  late ApiResponse<List<PostCategory>> postCategoryResponse;
  late List<PostCategory> _categories;
  late int categoryId;

  String _avataUrl = "";
  String get avataUrl => _avataUrl;

  late PostServices _postServices;
  late ApiResponse<List<Post>> postResponse;
  late List<Post> _posts;

  late VideoListViewServices _videoServices;
  late ApiResponse<List<YoutubeModel>> youtubeVideoResponseList;
  late List<YoutubeModel> _listVideos;

  MemberHomeState() {
    _services = PostCategoryServices();
    _postServices = PostServices();
    _videoServices = VideoListViewServices();
  }

  getAvatar() async{
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      _avataUrl = sp.getString('avataUrl')!;
    });
    notifyListeners();
  }

  setAvatar(avataUrl) async{
    _avataUrl = avataUrl;
    SharedPreferences.getInstance().then((SharedPreferences sp) {
       sp.setString('avataUrl',avataUrl);
    });
    notifyListeners();
  }

  getListPostCategories() async{
    postCategoryResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _categories = await _services.listCategories();
      postCategoryResponse = ApiResponse.completed(_categories);
    }
   catch (e) {
     postCategoryResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  getListPost() async{
    postResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _posts = await _postServices.listPosts();
      postResponse = ApiResponse.completed(_posts);
    }
    catch (e) {
      postResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setCategoryId(int id) {
    categoryId = id;
    notifyListeners();
  }

  getListVideos() async {
    youtubeVideoResponseList = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _listVideos = await _videoServices.listVideos();
      youtubeVideoResponseList = ApiResponse.completed(_listVideos);
    }
    catch (e) {
      youtubeVideoResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

}
