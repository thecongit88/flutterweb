import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/services/post_category_services.dart';
import 'package:hmhapp/services/post_services.dart';

class FormItemLogbookState extends ChangeNotifier {

  late FormItemLogbookServices _servicesFormItemLogBook;
  late ApiResponse<List<FormItemLogBook>> formItemLogBookResponse;
  late ApiResponse<bool> formItemCheckResponse;
  late List<FormItemLogBook> formItems;

  String formDate = "";

  FormItemLogbookState() {
    _servicesFormItemLogBook = new FormItemLogbookServices();
    formItemCheckResponse = ApiResponse.loading("Đang tải dữ liệu...");
  }

  //lấy danh sách 5 đánh giá gần đây cho HBCC
  getAllListFormItemLogbookHBCC() async {
    formItemLogBookResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      formItems = await _servicesFormItemLogBook.getAllListFormItemsLogBookHBCC(this.getFormDate());
      formItemLogBookResponse = ApiResponse.completed(formItems);
    }
    catch (e) {
      formItemLogBookResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getAllListFormItemLogbookNCS() async {
    formItemLogBookResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      formItems = await _servicesFormItemLogBook.getAllListFormItemsLogBookNCS(this.getFormDate());
      formItemLogBookResponse = ApiResponse.completed(formItems);
    }
    catch (e) {
      formItemLogBookResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setFormDate(_formDate) {
    this.formDate = _formDate;
  }

  getFormDate() {
    return this.formDate;
  }

  getFormItemsLogBook({form_date: "", formId: 0, memId: 0, is_ncs: true}) async {
    formItemLogBookResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      formItems = await _servicesFormItemLogBook.getFormItemsLogBook(form_date, formId, memId, is_ncs);
      formItemLogBookResponse = ApiResponse.completed(formItems);
    }
    catch (e) {
      formItemLogBookResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  delete(int id) async {
    notifyListeners();
    try {
      await _servicesFormItemLogBook.delete(id);
    } catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }

  /*
  mỗi tháng chỉ dc tạo 1 báo cáo, 1 ngày chỉ dc 1 đánh giá, hàm này kiểm tra việc đó
  type: day or month
  * */
  Future<bool?> isValidForCreateSurvey(memberId, formDate, type) async {
    formItemCheckResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      bool isValid = await _servicesFormItemLogBook.isValidForCreateSurvey(memberId, formDate, type);
      formItemCheckResponse = ApiResponse.completed(isValid);
      notifyListeners();
      return isValid;
    } catch (e) {
      formItemCheckResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
  setIsValidStateInit(){
    formItemCheckResponse = ApiResponse.init("");
  }
}
