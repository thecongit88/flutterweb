import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/common_screens/videos/home_videos_screen.dart';
import 'package:hmhapp/dp_members/logbook_list_view.dart';
import 'package:hmhapp/dp_members/member_home_screen.dart';
import 'package:hmhapp/dp_members/member_info_screen.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomTabState extends ChangeNotifier {
  int _screenIndex = 0;

  int get screenIndex => _screenIndex;

  setScreenIndex(int pos) {
    _screenIndex = pos;
    notifyListeners();
  }

  void navigateToHome() {
    setScreenIndex(0);
  }

}
