import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/dp_members/models/skill_groups.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/plans_services.dart';
import 'package:hmhapp/services/skill_groups_services.dart';

class SkillGroupsState extends ChangeNotifier{
  late SkillGroupsServices skillGroupsServices;
  late ApiResponse<List<SkillGroup>> skillGroupResponseList;
  late List<SkillGroup> _skillGroups;
  late PlansServices? plansServices;
  late List<Plan> plansList;

  SkillGroupsState() {
    skillGroupsServices = new SkillGroupsServices();
    _skillGroups = <SkillGroup>[];
    plansServices = new PlansServices();
    plansList = <Plan>[];
  }

  getListSkillGroups() async {
    notifyListeners();
    skillGroupResponseList = ApiResponse.loading("Đang tải dữ liệu nhóm kế hoạch");
    notifyListeners();
    try {
      _skillGroups = await skillGroupsServices.listSkillGroups();
      skillGroupResponseList = ApiResponse.completed(_skillGroups);
    } catch(e) {
      skillGroupResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getCurrentPlanSkillList(int mId) async {
    plansList = await plansServices!.listPlans(mId);
    return this.plansList;
  }

}