import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:format/format.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/services/hbcc_member_care_plan_item_services.dart';
import 'package:hmhapp/services/hbcc_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../common/app_global.dart';
import '../dp_managers/models/hbcc_ke_hoach/add_care_plan_item.dart';
import '../dp_managers/models/hbcc_ke_hoach/care_plan_item.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';
import '../services/hbcc_member_ke_hoach_services.dart';
import '../services/ncs_ke_hoach_services.dart';

class NcsKeHoachState extends ChangeNotifier{
  NcsKeHoachServices ncsKeHoachServices = NcsKeHoachServices();
  late ApiResponse<List<HbccItemKeHoach>> listNcsKeHoachResponse;
  DateTime now = DateTime.now();
  int thangSelected = 0, namSelected = 0, memberIdSelected = 0;

  //phan trang
  List<HbccItemKeHoach> listKeHoach = [];
  int page = 1;
  RefreshController refreshController =
  RefreshController(initialRefresh: false);

  /*NcsKeHoachState() {
    thangSelected = now.month;
    namSelected = now.year;
  }*/
  ///Thao tac vơi kế hoạch
  setThang(int _thangSelected) {
    thangSelected = _thangSelected;
    notifyListeners();
  }

  getThang() {
    return thangSelected;
  }

  setNam(int value) {
    namSelected = value;
    notifyListeners();
  }

  getNam() {
    return namSelected;
  }

  listKeHoachNcs({bool isLoadMore = false}) async {
    listNcsKeHoachResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      if (!isLoadMore) {
        page = 1;
        listKeHoach = [];
        refreshController.resetNoData();
        refreshController.refreshCompleted();
      } else {}

      final result = await ncsKeHoachServices.listKeHoachNcs(month: thangSelected, year: namSelected, pg: page);

      if (result.isNotEmpty == true) {
        listKeHoach.addAll(result);
        page++;
        refreshController.loadComplete();
      } else {
        refreshController.loadNoData();
      }

      listNcsKeHoachResponse = ApiResponse.completed(listKeHoach);
    } catch(e) {
      listNcsKeHoachResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  ///Thao tac voi danh sach CarePlanItem khi thêm 1 kế hoạch
  refreshUI() {
    notifyListeners();
  }
}