import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/user_services.dart';

class FormTypeState extends ChangeNotifier {

  late FormTypeServices _services;
  late bool isLoading;
  late int _formTypeId;
  FormType? _formType, _formTypeNCS;
  late ApiResponse<FormType> formTypeResponse;
  late ApiResponse<FormType> formTypeNCSResponse;

  late String errorMessage;
  late bool errorOccured = false;

  FormType? get formType => _formType;


  FormTypeState() {
    isLoading = true;
    _services = new FormTypeServices();
    _formType = new FormType();
    _formTypeNCS = new FormType();
    //formTypeNCSResponse = new ApiResponse<FormType>.init("");
  }


   getListFormTypes() async{
      try {
        await _services.listFormTypes();
      }
      catch (e) {
        throw e;
      }
      notifyListeners();
  }

  getFormType(int id) async{
    this._formTypeId = id;
    formTypeResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      isLoading = false;
      _formType = await _services.getFormType(_formTypeId);
      formTypeResponse = ApiResponse.completed(_formType);
    }
    catch (e) {
      formTypeResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getFormTypeNCS({required bool isFormDay}) async{
    formTypeNCSResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _formTypeNCS = await _services.getFormTypeNCS(isFormDay);
      formTypeNCSResponse = ApiResponse.completed(_formTypeNCS);
    }
    catch (e) {
      formTypeNCSResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}
