import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/report_thuchien_kehoach.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/report_tan_suat_services.dart';
import 'package:hmhapp/services/report_thuchien_kehoach_services.dart';

class ReportThuchienKehoachState extends ChangeNotifier{
  late ApiResponse<List<ReportThuchienKehoach>> reportThucHienKeHoachResponseList;
  late ReportThuchienKehoachServices reportThuchienKehoachServices;
  late List<ReportThuchienKehoach> repThuchienKehoach;
  late ApiResponse<ReportThuchienKehoach> reportItemResponse;
  late ReportThuchienKehoach? reportItem;

  ReportThuchienKehoachState() {
    reportThuchienKehoachServices = new ReportThuchienKehoachServices();
    repThuchienKehoach =  <ReportThuchienKehoach>[];
    reportItem = new ReportThuchienKehoach();
  }

  getListReportThuchienKehoach({int month: 0, int year: 2020}) async {
    notifyListeners();
    reportThucHienKeHoachResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repThuchienKehoach = await reportThuchienKehoachServices.listReportThuchienKehoach(month, year);
      reportThucHienKeHoachResponseList = ApiResponse.completed(repThuchienKehoach);
    } catch(e) {
      reportThucHienKeHoachResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getlistReportThuchienKehoachTheoKhoangThang({required int fromMonth,required int toMonth, int year: 2020}) async {
    notifyListeners();
    reportThucHienKeHoachResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repThuchienKehoach = await reportThuchienKehoachServices.listReportThuchienKehoachTheoKhoangThang(fromMonth, toMonth, year);
      reportThucHienKeHoachResponseList = ApiResponse.completed(repThuchienKehoach);
    } catch(e) {
      reportThucHienKeHoachResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  add(ReportThuchienKehoach _reportItem) async {
    notifyListeners();
    reportItemResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      reportItem = await reportThuchienKehoachServices.add(_reportItem);
      reportItemResponse = ApiResponse.completed(reportItem);
    }
    catch (e) {
      reportItemResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  update(ReportThuchienKehoach _reportItem) async {
    notifyListeners();
    try {
      reportItem = await reportThuchienKehoachServices.update(_reportItem);
    }
    catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }
}