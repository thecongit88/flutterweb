
import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../services/checkin/hbcc_checkin_services.dart';
import '../../widgets/search/filter_month_member_widget.dart';

class HbccCheckinState extends ChangeNotifier{
  late ApiResponse<int> hbccThemCheckinResponse;
  late ApiResponse<int> hbccUpdateCheckinResponse;
  HbccCheckinServices hbccCheckinServices = HbccCheckinServices();
  int result = 0;
  FilterMonthMemberWidgetModel? filterModel = FilterMonthMemberWidgetModel();

  List<HbccCheckin> list = [];

  late ApiResponse<List<HbccCheckin>> listCheckinResponse;

  int page = 1;
  RefreshController refreshController =
  RefreshController(initialRefresh: false);

  HbccCheckinState(){
    listCheckinResponse = ApiResponse.init("");
  }

  setFilter(FilterMonthMemberWidgetModel? filter){
    filterModel = filter;
    listCheckin();
  }

  resetFilter(){
    filterModel = FilterMonthMemberWidgetModel();
    listCheckin();
  }

  listCheckin({bool isLoadMore = false}) async {
    var filter  = filterModel;

    int memberId = filter?.memberId ?? 0;
    int year = filter?.year ?? 0;
    int month = filter?.month ?? 0;

    if (!isLoadMore) {
      page = 1;
      list = [];
      refreshController.resetNoData();
      refreshController.refreshCompleted();
    }

    try {
      int hbccId = await getSignedHbccId();
      final result = await hbccCheckinServices.listByHbcc(hbccId: hbccId,memberId: memberId,
          month: month,year: year, pg: page);
      if (result.isNotEmpty == true) {
        list.addAll(result);
        page++;
        refreshController.loadComplete();
      } else {
        refreshController.loadNoData();
      }
      listCheckinResponse = ApiResponse.completed(list);
    } catch(e) {
      listCheckinResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  addCheckin(HbccCheckin item) async {
    try {
      item.hbccId = await getSignedHbccId();
      final result = await hbccCheckinServices.create(item);
      return result;
    }
    catch (e) {
      rethrow;
    }
  }

  updateCheckin(HbccCheckin item, int id) async {
    try {
      item.hbccId = await getSignedHbccId();
      final result = await hbccCheckinServices.update(item, id);
      return result;
    }
    catch (e) {
      rethrow;
    }
  }

  deleteCheckin(int id) async {
    notifyListeners();
    try {
      final result = await hbccCheckinServices.delete(id);
      if((result["id"] ?? 0) > 0) {
      }
      hbccThemCheckinResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccThemCheckinResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? hbccId = await preferences.getInt('hbccId');
    return hbccId ?? 0;
  }


}