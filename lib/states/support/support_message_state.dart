
import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dp_managers/models/support/support_message.dart';
import '../../services/checkin/hbcc_checkin_services.dart';
import '../../services/support/support_message_services.dart';

class SupportMessageState extends ChangeNotifier{
  String tieuDeMessage = "";
  late ApiResponse<int> hbccThemMessageResponse;
  late ApiResponse<int> hbccUpdateMessageResponse;
  late ApiResponse<int> imgUploadResponse;
  SupportMessageServices hbccMessageServices = SupportMessageServices();
  int result = 0, thangSelected = 0;

  List<SupportMessage> list = [];

  late ApiResponse<List<SupportMessage>> listMessageResponse;
  int idMessage = 0;

  SupportMessageState(){
    imgUploadResponse = ApiResponse.init("");
  }

  ///Thao tac vơi kế hoạch
  setThang(int _thangSelected) {
    this.thangSelected = _thangSelected;
    notifyListeners();
  }

  getThang() {
    return this.thangSelected;
  }

  setTieuDeMessage({int? nam, int? thang}) {
    nam ??= DateTime.now().year;
    thang ??= DateTime.now().month;
    tieuDeMessage = "Kế hoạch chăm sóc tháng $thang năm $nam";
    notifyListeners();
  }

  getTieuDeMessage() {
    return tieuDeMessage;
  }

  int daysInMonth(int month, int year){
    var firstDayThisMonth = DateTime(year, month, 1);
    var firstDayNextMonth = DateTime(firstDayThisMonth.year, firstDayThisMonth.month + 1, 1);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }

  addMessage(SupportMessage item) async {
    try {
      final result = await hbccMessageServices.create(item);
      return result;
    }
    catch (e) {
      throw e;
    }
  }

  updateMessage(SupportMessage item, int id) async {
    notifyListeners();
    hbccUpdateMessageResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccMessageServices.update(item, id);
      idMessage = result.id ?? 0;
      hbccUpdateMessageResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccUpdateMessageResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  listMessage({int? conversationId}) async {
    listMessageResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      final list = await hbccMessageServices.listByConversation(conversationId: conversationId);
      listMessageResponse = ApiResponse.completed(list);
    } catch(e) {
      listMessageResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

 deleteMessage(int id) async {
   listMessageResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccMessageServices.delete(id);
      if((result["id"] ?? 0) > 0) {
      }
      hbccThemMessageResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccThemMessageResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  ///Thao tac voi danh sach CarePlanItem khi thêm 1 kế hoạch
  refreshUI() {
    notifyListeners();
  }


  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? hbccId = await preferences.getInt('hbccId');
    return hbccId ?? 0;
  }

}