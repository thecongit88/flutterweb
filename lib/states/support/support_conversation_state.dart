
import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/checkin/hbcc_checkin.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dp_managers/models/support/support_conservation.dart';
import '../../services/checkin/hbcc_checkin_services.dart';
import '../../services/member_services.dart';
import '../../services/support/support_conversation_services.dart';

class SupportConversationState extends ChangeNotifier{
  String tieuDeConversation = "";
  late ApiResponse<int> hbccThemConversationResponse;
  late ApiResponse<int> hbccUpdateConversationResponse;
  SupportConversationServices hbccConversationServices = SupportConversationServices();
  MemberServices memberServices = MemberServices();

  int result = 0, thangSelected = 0;

  List<SupportConversation> list = [];

  late ApiResponse<List<SupportConversation>> listConversationResponse;
  int idConversation = 0;

  ///Thao tac vơi kế hoạch
  setThang(int _thangSelected) {
    thangSelected = _thangSelected;
    notifyListeners();
  }

  getThang() {
    return thangSelected;
  }

  setTieuDeConversation({int? nam, int? thang}) {
    nam ??= DateTime.now().year;
    thang ??= DateTime.now().month;
    tieuDeConversation = "Kế hoạch chăm sóc tháng $thang năm $nam";
    notifyListeners();
  }

  getTieuDeConversation() {
    return tieuDeConversation;
  }

  int daysInMonth(int month, int year){
    var firstDayThisMonth = DateTime(year, month, 1);
    var firstDayNextMonth = DateTime(firstDayThisMonth.year, firstDayThisMonth.month + 1, 1);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }

  addConversation(SupportConversation item) async {
    try {
      var member = await memberServices.getMember();
      if(member != null){
        item.hbccId = member.hbccId;
        item.memberId = member.id;
        item.provinceId = member.provinceId;
        item.districtId = member.districtId;
        item.wardId = member.wardId;

        final result = await hbccConversationServices.create(item);
        return result;
      }else{
        return null;
      }
    }
    catch (e) {
      rethrow;
    }
  }

  updateConversation(SupportConversation item, int id) async {
    notifyListeners();
    hbccUpdateConversationResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      final result = await hbccConversationServices.update(item, id);
      idConversation = result["id"] ?? 0;
      hbccUpdateConversationResponse = ApiResponse.completed(1);
    }
    catch (e) {
      hbccUpdateConversationResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  listConversation({int? memberId}) async {
    listConversationResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      int hbccId = await getSignedHbccId();
      final list = await hbccConversationServices.listByHbcc(hbccId: hbccId);
      listConversationResponse = ApiResponse.completed(list);
    } catch(e) {
      listConversationResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  listConversationByMember() async {
    listConversationResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      int memberId = await getSignedMemberId();
      final list = await hbccConversationServices.listByMember(memberId: memberId);
      listConversationResponse = ApiResponse.completed(list);
    } catch(e) {
      listConversationResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  deleteConversation(int id) async {
    try {
      var result = await hbccConversationServices.delete(id);
      if(result != null && result["id"] != null) {
        return 1;
      }else{
        return null;
      }
    }
    catch (e) {
      rethrow;
    }
  }

  ///Thao tac voi danh sach CarePlanItem khi thêm 1 kế hoạch
  refreshUI() {
    notifyListeners();
  }


  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? hbccId = await preferences.getInt('hbccId');
    return hbccId ?? 0;
  }


  getSignedMemberId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? memberId = await preferences.getInt('memberId');
    return memberId ?? 0;
  }

}