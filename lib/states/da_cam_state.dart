import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/da_cam_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DaCamState extends ChangeNotifier{
  late DaCamServices _daCamServices;
  late ApiResponse<List<DaCam>> daCamResponseList;
  List<DaCam> dacams = <DaCam>[];
  late int daCamId;
  late String daCamCode;

  DaCamState() {
    _daCamServices = new DaCamServices();
    dacams = <DaCam>[];
  }

  listDaCam() async {
    daCamResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      dacams = await _daCamServices.listDaCam();
      daCamResponseList = ApiResponse.completed(dacams);
    } catch(e) {
      daCamResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setDaCamCode(String code) {
    this.daCamCode = code;
  }

  getDaCamCode() {
    return this.daCamCode;
  }
}