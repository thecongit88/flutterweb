import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/video_categories.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/user_services.dart';
import 'package:hmhapp/services/video_list_view_services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoDetailState extends ChangeNotifier {

  late List<YoutubeModel> relatedVideo;
  late ApiResponse<List<YoutubeModel>> relatedVideoResponseList;
  late int currentVideoId;

  VideoDetailState() {
    relatedVideo = <YoutubeModel>[];
  }

  getRelatedVideos(listData, currentVideoId) async {
    //relatedVideo = new List<YoutubeModel>();
    relatedVideo.clear();
    relatedVideoResponseList = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      //các video trong danh mục hiện tại
      listData.forEach((video) {
        //compare with current video id
        if(video.id != currentVideoId) relatedVideo.add(video);
      });
      relatedVideoResponseList = ApiResponse.completed(relatedVideo);
    }
    catch (e) {
      relatedVideoResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}
