import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/services/post_category_services.dart';
import 'package:hmhapp/services/post_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManagerHomeState extends ChangeNotifier {

  late PostCategoryServices _services;
  late ApiResponse<List<PostCategory>> postCategoryResponse;
  late List<PostCategory> _categories;
  late int categoryId;


  late PostServices _postServices;
  late ApiResponse<List<Post>> postResponse;
  late List<Post> _posts;

  late FormItemLogbookServices _servicesFormItemLogBook;
  late  ApiResponse<List<FormItemLogBook>> formItemLogBookResponse;
  late  List<FormItemLogBook> _formItems;


  String _avataUrl = "";
  String get avataUrl => _avataUrl;

  ManagerHomeState() {
    _services = new PostCategoryServices();
    _postServices = new PostServices();
    _servicesFormItemLogBook = new FormItemLogbookServices();
  }


  getAvatar() async{
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      _avataUrl = sp.getString('avataUrl')!;
    });
    notifyListeners();
  }

  getListPostCategories() async{
    postCategoryResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _categories = await _services.listCategories();
      postCategoryResponse = ApiResponse.completed(_categories);
    }
   catch (e) {
     postCategoryResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  getListPost() async{
    postResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _posts = await _postServices.listPosts();
      postResponse = ApiResponse.completed(_posts);
    }
    catch (e) {
      postResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  setCategoryId(int id) {
    categoryId = id;
    notifyListeners();
  }

  /* lấy danh sách 5 đánh giá gần đây cho HBCC */
  getListFormItemLogbookHBCC() async{
    formItemLogBookResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _formItems = await _servicesFormItemLogBook.getListFormItemsLogBook(); //ko lọc theo date
      formItemLogBookResponse = ApiResponse.completed(_formItems);
    }
    catch (e) {
      formItemLogBookResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

}
