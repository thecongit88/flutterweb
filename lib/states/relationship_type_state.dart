import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/relationship_type.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/relationship_type_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RelationshipTypeState extends ChangeNotifier{
  late RelationshipTypeServices _relationshipTypeServices;
  late ApiResponse<List<RelationshipType>> relationshipTypeResponseList;
  late List<RelationshipType> relationshipTypes;
  late int relationshipTypeId;

  RelationshipTypeState() {
    _relationshipTypeServices = new RelationshipTypeServices();
    relationshipTypes = <RelationshipType>[];
  }

  listRelationshipType() async {
    relationshipTypeResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      relationshipTypes = await _relationshipTypeServices.listRelationshipType();
      relationshipTypeResponseList = ApiResponse.completed(relationshipTypes);
    } catch(e) {
      relationshipTypeResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setRelationshipTypeId(int gId) {
    this.relationshipTypeId = gId;
    notifyListeners();
  }

  getRelationshipTypeId() {
    return this.relationshipTypeId;
  }
}