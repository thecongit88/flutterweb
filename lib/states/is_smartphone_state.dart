import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/is_smartphone.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/da_cam_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/is_smartphone_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IsSmartphoneState extends ChangeNotifier{
  late IsSmartphoneServices _isSmartphoneServices;
  late ApiResponse<List<IsSmartphone>> isSmartphoneResponseList;
  late List<IsSmartphone> isSmartphones;
  late int isSmartphoneId;
  late bool isSmartphoneCode;

  IsSmartphoneState() {
    _isSmartphoneServices = new IsSmartphoneServices();
    isSmartphones = <IsSmartphone>[];
    isSmartphoneCode = false;
  }

  listIsSmartphones() async {
    isSmartphoneResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      isSmartphones = await _isSmartphoneServices.listSmartphones();
      isSmartphoneResponseList = ApiResponse.completed(isSmartphones);
    } catch(e) {
      isSmartphoneResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
/*
  setIsSmartphoneCode(bool code) {
    this.isSmartphoneCode = code;
    notifyListeners();
  }

  getIsSmartphoneCode() {
    return this.isSmartphoneCode;
  }
*/
}