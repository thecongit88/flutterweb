import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/utils/constants.dart';

class ReportState extends ChangeNotifier{
  late MemberServices _memberServices;
  late ApiResponse<int> totalMembersResponse;
  late ApiResponse<int> maleMembersResponse;
  late ApiResponse<int> femaleMembersResponse;
  late ApiResponse<int> dacamMembersResponse;
  late int _totalMembers, _maleMembers, _femaleMembers, _dacamMembers;

  String param = "", value = "";

  ReportState() {
    _memberServices = new MemberServices();
  }

  getTotalMembers() async {
    totalMembersResponse = ApiResponse.loading("");
    notifyListeners();
    try {
      _totalMembers = await _memberServices.countMembers("", 0);
      totalMembersResponse = ApiResponse.completed(_totalMembers);
    } catch(e) {
      totalMembersResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getMaleMembers() async {
    maleMembersResponse = ApiResponse.loading("");
    notifyListeners();
    try {
      _maleMembers = await _memberServices.countMembers("GioiTinh", Constants.GENDER_MALE);
      maleMembersResponse = ApiResponse.completed(_maleMembers);
    } catch(e) {
      maleMembersResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getFeMaleMembers() async {
    femaleMembersResponse = ApiResponse.loading("");
    notifyListeners();
    try {
      _femaleMembers = await _memberServices.countMembers("GioiTinh", Constants.GENDER_FEMALE);
      femaleMembersResponse = ApiResponse.completed(_femaleMembers);
    } catch(e) {
      femaleMembersResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getDaCamMembers() async {
    dacamMembersResponse = ApiResponse.loading("");
    notifyListeners();
    try {
      _dacamMembers = await _memberServices.countMembers("LoaiKhuyetTat", Constants.DACAM);
      dacamMembersResponse = ApiResponse.completed(_dacamMembers);
    } catch(e) {
      dacamMembersResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}