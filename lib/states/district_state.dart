import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DistrictState extends ChangeNotifier{
  late DistrictServices _districtServices;
  late ApiResponse<List<District>> districtResponseList;
  late List<District> districts;
  late int districtId;
  late String districtCode;
  late List<District> listHuyen;
  late bool changeProvince = false;

  DistrictState() {
    _districtServices = new DistrictServices();
    listHuyen = <District>[];
    districtResponseList = ApiResponse.init("");
  }

  listDistricts() async {
    districtResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      districts = <District>[];
      districts = await _districtServices.listDistricts();
      districtResponseList = ApiResponse.completed(districts);
    } catch(e) {
      districtResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  bindDistricts(List<District> districts) async {
    districtResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      districtResponseList = ApiResponse.completed(districts);
    } catch(e) {
      districtResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  listDistrictsByProvince1(int provinceId) async {
    districtResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      districts = <District>[];
      districts = await _districtServices.listDistrictsByProvince(provinceId);
      districtResponseList = ApiResponse.completed(districts);
    } catch(e) {
      districtResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setListHuyen(lstHuyen) {
    listHuyen = lstHuyen;
  }

  List<District> getListHuyen() {
    return listHuyen;
  }

  setDistrictId(int gId) {
    this.districtId = gId;
  }

  getDistrictId() {
    return this.districtId;
  }

  setChangeProvice(bool val) {
    this.changeProvince = val;
  }

  getChangeProvice() {
    return this.changeProvince;
  }
}