import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberState extends ChangeNotifier{
  late MemberServices _memberServices;
  late PersonalAssistantServices _personalAssistantServices;
  int? personalAssistantId, memberId, relationshipTypeId;
  bool? isSuccess, approved;
  String? _avatarUrl;
  Member? member;
  User? user;
  PersonalAssistant? _personalAssistant;
  late ApiResponse<Member> memberResponse;
  late ApiResponse<Member> memberCreateResponse;
  late ApiResponse<PersonalAssistant> personalAssistantResponse;
  late ApiResponse<List<Member>> memberResponseList;
  List<Member>? _members;
  int? checkUser = 0;

  List<Member>? membersSearch;
  late ApiResponse<List<Member>> memberSearchResponse;
  int? pageSearch;
  int? totalSearchPages;
  late MemberPageResponse _memberSearchResponse;

  bool? _isCapture = false;

  late ApiResponse<FileUpload> avatarResponse;
  FileUpload? _fileUpload;
  String? get avatarUrl => _avatarUrl;

  DateTime? projectJoinDate, birthDate, disabilityDate;

  MemberState() {
    _memberServices = MemberServices();
    _members = <Member>[];
    member = Member();
    _personalAssistantServices = PersonalAssistantServices();
    membersSearch = <Member>[];

    avatarResponse = ApiResponse.init("Đang xử lý...");
    memberCreateResponse = ApiResponse.init("Đang xử lý...");
    user = User();
    approved = false;
  }

  getAvatar() async{
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      _avatarUrl = sp.getString('avataUrl');
    });
    notifyListeners();
  }

  setAvatarUrl(_avatarUrl, fileName) async {
    avatarResponse = ApiResponse.loading("Đang xử lý...");
    try {
      _fileUpload = await _memberServices.uploadAvatar(_avatarUrl,fileName);
      this._avatarUrl = _fileUpload!.formats!.thumbnail!.url;
      avatarResponse = ApiResponse.completed(_fileUpload);
    }
    catch (e) {
      avatarResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getListMembers() async {
    memberResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      _members = await _memberServices.listMembers();
      memberResponseList = ApiResponse.completed(_members);
    } catch(e) {
      memberResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getMember() async {
    memberResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      member = await _memberServices.getMember();
      personalAssistantId = member!.idNguoiChamSoc;
      memberId = member!.id;
      _avatarUrl = member!.avatar;
      isSuccess = member!.isSuccess;
      approved = member!.approved;
      memberResponse = ApiResponse.completed(member);
    } catch(e) {
      memberResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getPersonalAssistant(id) async {
    personalAssistantResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      _personalAssistant = await _personalAssistantServices.getPersonalAst(id);
      relationshipTypeId = _personalAssistant!.relationship_type!.id;
      personalAssistantResponse = ApiResponse.completed(_personalAssistant);
    } catch(e) {
      personalAssistantResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  Future<List<Member>?> search(String? search,int? pg) async {
    pageSearch = pg;
    memberSearchResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _memberSearchResponse = await _memberServices.search(pg, search);
      if(_memberSearchResponse.items.isNotEmpty) {
        membersSearch!.addAll(_memberSearchResponse.items);
      }
      memberSearchResponse = ApiResponse.completed(membersSearch);
      totalSearchPages = _memberSearchResponse.pages;
    }
    catch (e) {
      memberSearchResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getIsCapture() {
    return _isCapture;
  }

  setIsCapture(bool cap) {
    _isCapture = cap;
  }

  updateMember(Member mem, int mId) async {
    notifyListeners();
    memberResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var m = await _memberServices.updateMember(mem, mId);
      memberResponse = ApiResponse.completed(m);
    }
    catch (e) {
      memberResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  createMember(Member mem) async {
    notifyListeners();
    memberCreateResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var m = await _memberServices.createMember(mem);
      memberCreateResponse = ApiResponse.completed(m);
    }
    catch (e) {
      memberCreateResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setProjectJoinDate(DateTime dateTime) {
    member!.projectJoinDate = dateTime.toString();
    notifyListeners();
  }

  DateTime? getProjectJoinDate() {
    if(member!.projectJoinDate != null && member!.projectJoinDate != "") {
      return DateTime.parse(member!.projectJoinDate!);
    } else {
      return null;
    }
  }

  setBirthDate(DateTime dateTime) {
    member!.birth_date = dateTime.toString();
    notifyListeners();
  }

  DateTime? getBirthDate() {
    if(member!.birth_date != null && member!.birth_date != "") {
      return DateTime.parse(member!.birth_date!);
    } else {
      return null;
    }
  }

  setDisabilityDate(DateTime? dateTime) {
    member!.disability_date = dateTime.toString();
    notifyListeners();
  }

  DateTime? getDisabilityDate() {
    if(member!.disability_date != null && member!.disability_date != "") {
      return DateTime.parse(member!.disability_date!);
    } else {
      return null;
    }
  }

  getUserItem({userName: ""}) async {
    try {
      checkUser = await _memberServices.getUserItem(userName: userName);
    }
    catch (e) {
    }
    return 0;
  }
}