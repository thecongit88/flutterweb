import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:hmhapp/services/user_services.dart';

class UserState extends ChangeNotifier {

  late UserServices _services;
  late bool isLoading;

  late String errorMessage;
  late bool errorOccured = false;

  UserState() {
    isLoading = true;
    _services = UserServices();
  }


   getCurrentUser() {
      try {
        return _services.currentUser();
      }
      catch (e) {
        throw e;
      }
    notifyListeners();
  }

}
