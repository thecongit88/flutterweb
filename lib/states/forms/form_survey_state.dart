import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/models/form_kncs_item.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/services/form_kncs_services.dart';
import 'package:hmhapp/services/form_survey_services.dart';

class FormSurveyState extends ChangeNotifier {

  late FormSurveyServices _services;
  late FormItemLogbookServices _itemLogbookServices;
  late FormKNCSServices _formKNCSServices;
  late ApiResponse<List<FormSurvey>> formSurveyResponse;
  List<FormSurvey> _formSurveys = <FormSurvey>[];
  String formDate = "";

  late  ApiResponse<int> formResponse;
  late  ApiResponse<int> formUpdateResponse;
  late  ApiResponse<int> formKNCSResponse;
  late ApiResponse<int> formKNCSUpdateResponse;
  late ApiResponse<int> formDeleteResponse;


  FormSurveyState() {
    _services = FormSurveyServices();
    _itemLogbookServices = FormItemLogbookServices();
    _formKNCSServices = FormKNCSServices();
    formResponse = ApiResponse.init("Đang xử lý...");
    formUpdateResponse = ApiResponse.init("Đang xử lý...");
    formKNCSUpdateResponse = ApiResponse.init("Đang xử lý...");
    formKNCSResponse = ApiResponse.init("Đang xử lý...");
  }

  getListFormsSurveyState(formName) async{
    if(formName.toString().isEmpty) {
      formSurveyResponse = ApiResponse.loading("Đang tải dữ liệu...");
      return;
    }

    formSurveyResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _formSurveys = await _services.getListFormsSurvey(formName, getFormDate());
      formSurveyResponse = ApiResponse.completed(_formSurveys);
    }
    catch (e) {
     formSurveyResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setFormDate(_formDate) {
    this.formDate = _formDate;
  }

  getFormDate() {
    return this.formDate;
  }

  create(Map<String, dynamic> form,FormItemLogBook formItem,String formType) async {
    notifyListeners();
    formResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var formObj = await _services.create(form,formType);
      if(formObj != null && formObj["id"]>0){
        formItem.formId = formObj["id"];
        _itemLogbookServices.create(formItem);
      }
      formResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  update(Map<String, dynamic> form,int formId,String formType) async {
    notifyListeners();
    formUpdateResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var formObj = await _services.update(form,formId,formType);
      formUpdateResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formUpdateResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  createFormKNCS(Map<String, dynamic> form,FormItemLogBook formItem,String formType, List<FormKNCSItem> items) async {
    notifyListeners();
    formKNCSResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var formObj = await _services.create(form,formType);
      if(formObj != null && formObj["id"]>0){
        var formId = formObj["id"];
        formItem.formId = formId;
        _itemLogbookServices.create(formItem);
        _formKNCSServices.createMulti(items,formObj,formType);

      }
      formKNCSResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formKNCSResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  updateFormKNCS(Map<String, dynamic> form,int formId,String formType, List<FormKNCSItem> items, List<FormKNCSItem> oldItems) async {
    notifyListeners();
    formKNCSUpdateResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var formObj = await _services.update(form,formId,formType);
      if(formObj != null && formObj["id"]>0){
        var formId = formObj["id"];
        _formKNCSServices.deleteMulti(oldItems);
        _formKNCSServices.createMulti(items,formObj,formType);
      }
      formKNCSUpdateResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formKNCSUpdateResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  delete(int formId, String formType) async {
    notifyListeners();
    try {
      await _services.delete(formId,formType);
    } catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }

  /* Đếm số lượng đánh giá đã thực hiện của 1 member từ đầu tháng đến cuối tháng */
  countSurveyExcuted({formName, memberId, formDate}) async {
    var startDate = DateTime(formDate.year, formDate.month, 1);
    var endDate = DateTime(formDate.year, formDate.month + 1, 1);
    notifyListeners();
    try {
      return await _services.countSurveyExcuted(formName, memberId, startDate, endDate);
    } catch (e) {
      throw(e.toString());
    }
  }

  /* Đếm số lượng đánh giá đã thực hiện của 1 member từ đầu tháng đến cuối tháng */
  isChangeSurveyFormForHBCC({formName, memberId, formDate}) async {
    var startDate = DateTime(DateTime.now().year, DateTime.now().month, 26);
    var endDate = DateTime(DateTime.now().year, DateTime.now().month + 1, 6);
    notifyListeners();
    try {
      var count = await _services.countSurveyExcuted(formName, memberId, startDate, endDate);
      if(count == 0) return false;
      return true;
    } catch (e) {
      throw(e.toString());
    }
  }
}
