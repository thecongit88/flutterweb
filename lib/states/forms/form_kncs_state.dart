import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/services/member_services.dart';

class FormKNCSState extends ChangeNotifier {

  late FormSurveyServices _services;
  late MemberServices _memberServices;
  late FormItemLogbookServices _itemLogbookServices;

  late ApiResponse<List<FormSurvey>> formSurveyResponse;
  List<FormSurvey> _formSurveys= <FormSurvey>[];
  List<Member> _listMembers = <Member>[];
  late ApiResponse<List<Member>> listMemberResponse;
  List<Member> get listMembers => _listMembers;


  List<PlanCheck> _listPlanCheck= <PlanCheck>[];
  List<PlanCheck> get listPlanCheck => _listPlanCheck;


  String formDate = "";

  late ApiResponse<int> formResponse;


  FormKNCSState() {
    _services = FormSurveyServices();
    _memberServices = MemberServices();
    _itemLogbookServices = FormItemLogbookServices();
    _listPlanCheck = <PlanCheck>[];
    formResponse = ApiResponse.init("Đang xử lý...");
  }

  getListFormsSurveyState(formName) async{
    if(formName.toString().isEmpty) {
      formSurveyResponse = ApiResponse.loading("Đang tải dữ liệu...");
      return;
    }

    formSurveyResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _formSurveys = await _services.getListFormsSurvey(formName, getFormDate());
      formSurveyResponse = ApiResponse.completed(_formSurveys);
    }
    catch (e) {
     formSurveyResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setFormDate(_formDate) {
    this.formDate = _formDate;
  }

  getFormDate() {
    return this.formDate;
  }

  create(Map<String, dynamic> form,FormItemLogBook formItem,String formType) async {
    notifyListeners();
    formResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var formObj = await _services.create(form,formType);
      if(formObj != null && formObj["id"]>0){
        formItem.formId = formObj["id"];
        _itemLogbookServices.create(formItem);
      }
      formResponse = ApiResponse.completed(1);
    }
    catch (e) {
      formResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setPlanCheckList(List<PlanCheck> list) {
    this._listPlanCheck.clear();
    this._listPlanCheck.addAll(listPlanCheck);
  }

}
