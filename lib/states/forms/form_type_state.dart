import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/post_category_services.dart';
import 'package:hmhapp/services/post_services.dart';

class FormTypeState extends ChangeNotifier {

  late FormTypeServices _services;
  late ApiResponse<List<FormType>> formTypeResponses;
  late ApiResponse<FormSurvey> formSurveyResponse;
  List<FormType> _formTypes = <FormType>[];
  String firstFormObject = "";
  FormType? formType;

  FormTypeState() {
    _services = FormTypeServices();
  }

  getList() async{
    formTypeResponses = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _formTypes = await _services.listFormTypes();
      this.firstFormObject = _formTypes != null ? _formTypes[0].formObject.toString().trim() : "";
      formTypeResponses = ApiResponse.completed(_formTypes);
    }
   catch (e) {
     formTypeResponses = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setFormType(FormType _formType) {
    this.formType = _formType;
  }

  getFormType() {
    return this.formType;
  }

  getFormObject() {
    return this.formType!.formObject ?? "";
  }
}
