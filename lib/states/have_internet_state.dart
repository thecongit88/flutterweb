import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/have_internet.dart';
import 'package:hmhapp/services/have_internet_services.dart';
import 'package:hmhapp/services/api_response.dart';


class HaveInternetState extends ChangeNotifier{
  late HaveInternetServices _haveInternetServices;
  late ApiResponse<List<HaveInternet>> haveInternetResponseList;
  late List<HaveInternet> haveInternet;
  late int haveInternetId;
  late bool haveInternetCode;

  HaveInternetState() {
    _haveInternetServices = new HaveInternetServices();
    haveInternet = <HaveInternet>[];
  }

  listHaveInternet() async {
    haveInternetResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      haveInternet = await _haveInternetServices.listInternet();
      haveInternetResponseList = ApiResponse.completed(haveInternet);
    } catch(e) {
      haveInternetResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setHaveInternetCode(bool code) {
    this.haveInternetCode = code;
    notifyListeners();
  }

  getHaveInternetCode() {
    return this.haveInternetCode;
  }
}