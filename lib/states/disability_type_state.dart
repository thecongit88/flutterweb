import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/disability_type_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DisabilityTypeState extends ChangeNotifier{
  late DisabilityTypeServices _disabilityTypeServices;
  late ApiResponse<List<DisabilityType>> disabilityTypeResponseList;
  late List<DisabilityType> dis;
  late int loaiKtId;

  DisabilityTypeState() {
    _disabilityTypeServices = new DisabilityTypeServices();
    dis = <DisabilityType>[];
  }

  listLoaiKT() async {
    disabilityTypeResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      dis = await _disabilityTypeServices.listLoaiKT();
      disabilityTypeResponseList = ApiResponse.completed(dis);
    } catch(e) {
      disabilityTypeResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setloaiKtId(int gId) {
    this.loaiKtId = gId;
    notifyListeners();
  }

  getloaiKtId() {
    return this.loaiKtId;
  }
}