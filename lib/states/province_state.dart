import 'package:flutter/foundation.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProvinceState extends ChangeNotifier{
  late ProvinceServices _provinceServices;
  late ApiResponse<List<Province>> provinceResponseList;
  late List<Province> provinces;
  late int provinceId;
  late String provinceCode;

  ProvinceState() {
    _provinceServices = new ProvinceServices();
    provinces = <Province>[];
    provinceResponseList = ApiResponse.init("");
  }

  listProvinces() async {
    provinceResponseList = ApiResponse.loading("");
    notifyListeners();
    try {
      provinces = await _provinceServices.listProvinces();
      provinceResponseList = ApiResponse.completed(provinces);
    } catch(e) {
      provinceResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  bindProvinces(List<Province> provinces) async {
    provinceResponseList = ApiResponse.loading("");
    notifyListeners();
    try {
      provinceResponseList = ApiResponse.completed(provinces);
    } catch(e) {
      provinceResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setProvinceId(int id) {
    this.provinceId = id;
  }

  getProvinceId() {
    return this.provinceId;
    notifyListeners();
  }
}