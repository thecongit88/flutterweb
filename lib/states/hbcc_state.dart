
import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/services/hbcc_services.dart';
import 'package:hmhapp/services/api_response.dart';

class HbccState extends ChangeNotifier{
  late HbccServices _hbccServices;
  HBCC? _hbcc;
  late ApiResponse<HBCC> hbccResponse;
  late ApiResponse<List<HBCC>> hbccResponseList;
  late String _avatarUrl = "";
  late bool _isCapture = false;
  late ApiResponse<HBCC> hbccCreateResponse;

  late ApiResponse<FileUpload> avatarResponse;
  late FileUpload _fileUpload;

  HbccState() {
    _hbccServices = HbccServices();
    _hbcc = HBCC();
    hbccCreateResponse = ApiResponse.init("Đang xử lý...");
  }

  getHbcc() async {
    hbccResponse = ApiResponse.loading("Đang tải dữ liệu HBCC");
    notifyListeners();
    try {
      _hbcc = await _hbccServices.getHBCC();
      if(_hbcc != null) {
        _avatarUrl = _hbcc!.avatar!;
      }
      hbccResponse = ApiResponse.completed(_hbcc);
    } catch(e) {
      hbccResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  HBCC? getHbccData() {
    return _hbcc;
  }

  getHbccAvatarUrl() {
    return _avatarUrl;
  }

  setHbccAvatarUrl(_avatarUrl, fileName) async {
    avatarResponse = ApiResponse.loading("Đang xử lý...");
    try {
      _fileUpload = await _hbccServices.uploadAvatar(_avatarUrl,fileName);
      this._avatarUrl = _fileUpload.formats!.thumbnail!.url!;
      avatarResponse = ApiResponse.completed(_fileUpload);
    }
    catch (e) {
      avatarResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getIsCapture() {
    return _isCapture;
  }

  setIsCapture(bool cap) {
    _isCapture = cap;
  }


  create(HBCC? hbcc) async {
    notifyListeners();
    hbccCreateResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      hbcc = await _hbccServices.createHBCC(hbcc!);
      hbccCreateResponse = ApiResponse.completed(hbcc);
    }
    catch (e) {
      hbccCreateResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}