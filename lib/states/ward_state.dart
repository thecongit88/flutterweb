import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/ward_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WardState extends ChangeNotifier{
  late WardServices _wardServices;
  late ApiResponse<List<Ward>> wardResponseList;
  late List<Ward> wards;
  late int wardId;
  late String wardCode;
  late bool changeDistrict  = false;

  WardState() {
    _wardServices = new WardServices();
    wards = <Ward>[];
    wardResponseList = ApiResponse.init("");
  }

  listWards() async {
    wardResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      wards = await _wardServices.listWards();
      wardResponseList = ApiResponse.completed(wards);
    } catch(e) {
      wardResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  bindWards(List<Ward> wds) async {
    wardResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      wards = wds;
      wardResponseList = ApiResponse.completed(wards);
    } catch(e) {
      wardResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  listWardsByDistrict(int districtId) async {
    wardResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      wards = await _wardServices.listWardsByDistrict(districtId);
      wardResponseList = ApiResponse.completed(wards);
    } catch(e) {
      wardResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setWardId(int gId) {
    this.wardId = gId;
  }

  getWardId() {
    return this.wardId;
  }


  setChangeDistrict(bool val) {
    this.changeDistrict = val;
  }

  getChangeDistrict() {
    return this.changeDistrict;
  }
}