import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NationState extends ChangeNotifier{
  late NationServices _nationServices;
  late ApiResponse<List<Nation>> nationResponseList;
  List<Nation>? nations;
  int? nationId;

  NationState() {
    _nationServices = new NationServices();
    nations = <Nation>[];
  }

  listNations() async {
    nationResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      nations = await _nationServices.listNations();
      nationResponseList = ApiResponse.completed(nations);
    } catch(e) {
      nationResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setNationId(int gId) {
    this.nationId = gId;
    notifyListeners();
  }

  getNationId() {
    return this.nationId;
  }
}