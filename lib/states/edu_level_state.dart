import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/edu_level.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/edu_level_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EduLevelState extends ChangeNotifier{
  late EduLevelServices _eduLevelServices;
  late ApiResponse<List<EduLevel>> eduLevelsResponseList;
  late List<EduLevel> eduLevels;
  late int eduLevelId;

  EduLevelState() {
    _eduLevelServices = new EduLevelServices();
    eduLevels = <EduLevel>[];
  }

  listEduLevels() async {
    eduLevelsResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      eduLevels = await _eduLevelServices.listEduLevels();
      eduLevelsResponseList = ApiResponse.completed(eduLevels);
    } catch(e) {
      eduLevelsResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setEduLevelId(int gId) {
    this.eduLevelId = gId;
  }

  getEduLevelId() {
    return this.eduLevelId;
  }
}