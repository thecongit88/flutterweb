import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/marital_status.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/marital_status_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MaritalStatusState extends ChangeNotifier{
  late MaritalStatusServices _maritalStatusServices;
  late ApiResponse<List<MaritalStatus>> maritalResponseList;
  late List<MaritalStatus> maritalStatus;
  late int maritalStatusId;

  MaritalStatusState() {
    _maritalStatusServices = new MaritalStatusServices();
    maritalStatus = <MaritalStatus>[];
  }

  listMaritalStatus() async {
    maritalResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      maritalStatus = await _maritalStatusServices.listMaritalStatus();
      maritalResponseList = ApiResponse.completed(maritalStatus);
    } catch(e) {
      maritalResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setMaritalStatusId(int gId) {
    this.maritalStatusId = gId;
    notifyListeners();
  }

  getMaritalStatusId() {
    return this.maritalStatusId;
  }
}