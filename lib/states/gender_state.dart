import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenderState extends ChangeNotifier{
  late GenderServices _genderServices;
  late ApiResponse<List<Gender>> genderResponseList;
  late List<Gender> genders;
  late int genderId;

  GenderState() {
    _genderServices = new GenderServices();
    genders = <Gender>[];
  }


  getListGenders() async {
    genderResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      genders = await _genderServices.listGenders();
      genderResponseList = ApiResponse.completed(genders);
    } catch(e) {
      genderResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setGenderId(int gId) {
    this.genderId = gId;
    notifyListeners();
  }

  getGenderId() {
    return this.genderId;
  }
}