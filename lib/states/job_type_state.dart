import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/job_type.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/job_type_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class JobTypeState extends ChangeNotifier{
  late JobTypeServices _jobTypeServices;
  late ApiResponse<List<JobType>> jobTypeResponseList;
  late List<JobType> jobTypes;
  late int jobTypeId;

  JobTypeState() {
    _jobTypeServices = new JobTypeServices();
    jobTypes = <JobType>[];
  }

  listJobTypes() async {
    jobTypeResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      jobTypes = await _jobTypeServices.listJobTypes();
      jobTypeResponseList = ApiResponse.completed(jobTypes);
    } catch(e) {
      jobTypeResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setJobTypeId(int gId) {
    this.jobTypeId = gId;
    notifyListeners();
  }

  getJobTypeId() {
    return this.jobTypeId;
  }
}