import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/widgets/manager_report_main_view.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/dp_members/models/skill.dart';
import 'package:hmhapp/dp_members/models/skill_groups.dart';
import 'package:hmhapp/models/relationship_type.dart';
import 'package:hmhapp/models/tan_suat.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/plans_services.dart';
import 'package:hmhapp/services/relationship_type_services.dart';
import 'package:hmhapp/services/skill_groups_services.dart';
import 'package:hmhapp/services/tan_suat_services.dart';
import 'package:hmhapp/widgets/i18n_calendar_strip.dart';

class PlansState extends ChangeNotifier{
  late ApiResponse<Plan> planResponse;
  Plan plan = new Plan();
  late PlansServices plansServices;
  late TanSuatServices tanSuatServices;
  late RelationshipTypeServices relationshipTypeServices;
  late ApiResponse<List<Plan>> plansResponseList;
  late ApiResponse<List<TanSuatCLass>> tansuatsResponseList;
  late ApiResponse<List<RelationshipType>> relationshipTypesResponseList;
  late List<Plan> plansList;
  late List<TanSuatCLass> tansuatsList;
  late List<RelationshipType> relationshipTypesList;
  late int memberId;
  dynamic isDeleted;


  late List<PlanCheck> _planChecksList;
   List<PlanCheck> get planChecksList => _planChecksList;
  late ApiResponse<List<PlanCheck>> planChecksResponseList;

  PlansState() {
    plansServices = new PlansServices();
    tanSuatServices = new TanSuatServices();
    relationshipTypeServices = new RelationshipTypeServices();
    this.memberId = 0;
    plan = new Plan();
    plansList = <Plan>[];

    tansuatsList = <TanSuatCLass>[];
    relationshipTypesList = <RelationshipType>[];

    _planChecksList = <PlanCheck>[];
    planChecksResponseList = ApiResponse.init("Đang tải dữ liệu nhóm kế hoạch");
  }

  setInit(){
    plansResponseList = ApiResponse.init("Đang xử lý...");
    notifyListeners();
  }

  getListPlans({int mId:0}) async {
    notifyListeners();
    plansResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      plansList = await plansServices.listPlans(mId);
      plansResponseList = ApiResponse.completed(plansList);
    } catch(e) {
      plansResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getPlanBySkill(int skillId, int mId) async {
    notifyListeners();
    planResponse = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      plan = await plansServices.getPlanBySkill(skillId, mId);
      planResponse = ApiResponse.completed(plan);
    } catch(e) {
      planResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  addPlan(Plan plan, {int mId:0}) async {
    notifyListeners();
    planResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var planObj = await plansServices.addPlan(plan, mId);
      planResponse = ApiResponse.completed(planObj);
    }
    catch (e) {
      planResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  updatePlan(Plan plan, int planId) async {
    notifyListeners();
    planResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var pl = await plansServices.updatePlan(plan, planId);
      planResponse = ApiResponse.completed(pl);
    }
    catch (e) {
      planResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  deletePlan(id) async {
    notifyListeners();
    try {
      isDeleted = await plansServices.deletePlan(id);
    } catch (e) {}
    notifyListeners();
  }

  setNewListPlans(List<Plan> _lstPlan) {
    plansResponseList = ApiResponse.completed(_lstPlan);
    notifyListeners();
  }


  getListTanSuat() async {
    notifyListeners();
    tansuatsResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      tansuatsList = await tanSuatServices.listTanSuats();
      tansuatsResponseList = ApiResponse.completed(tansuatsList);
    } catch (e) {
      tansuatsResponseList = ApiResponse.error(e.toString());
    }
  }

  getListPlanChecks({int mId:0}) async {
    if(mId>0){
      this.memberId = mId;
      notifyListeners();
      planChecksResponseList = ApiResponse.loading("Đang tải dữ liệu nhóm kế hoạch");
      notifyListeners();
      try {
        plansList = await plansServices.listPlans(this.memberId);
        _planChecksList = convertToPlanCheck(plansList);
        planChecksResponseList = ApiResponse.completed(_planChecksList);
      } catch(e) {
        planChecksResponseList = ApiResponse.error(e.toString());
      }
    }else{
      _planChecksList.clear();
      planChecksResponseList = ApiResponse.init("Đang tải dữ liệu nhóm kế hoạch");
    }
    notifyListeners();
  }

  getListRelationshipType() async {
    notifyListeners();
    relationshipTypesResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      relationshipTypesList =
      await relationshipTypeServices.listRelationshipType();
      relationshipTypesResponseList =
          ApiResponse.completed(relationshipTypesList);
    } catch (e) {
      relationshipTypesResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  convertToPlanCheck(List<Plan> plansList){
    var list = <PlanCheck>[];
    plansList.forEach((element) {
      var item = new PlanCheck()
        ..id=element.id
        ..title=element.title
        ..formFormat = element.formFormat
        ..formObject = element.formObject
        ..isCheck = false;
      list.add(item);
    });
    return list;
  }
}