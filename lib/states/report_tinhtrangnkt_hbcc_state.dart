import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/report_tinhtrangnkt_hbcc.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/report_tinhtrangnkt_hbcc_services.dart';

class ReportTinhtrangnktHbccState extends ChangeNotifier{
  late ApiResponse<List<ReportTinhTrangNktHbcc>> reportTinhTrangNktHbccResponseList;
  late ReportTinhtrangnktHbccServices reportTinhTrangNktHbccServices;
  late List<ReportTinhTrangNktHbcc> repTinhTrangNktHbcc;
  late ApiResponse<ReportTinhTrangNktHbcc> reportItemResponse;
  late ReportTinhTrangNktHbcc? reportItem;

  ReportTinhtrangnktHbccState() {
    reportTinhTrangNktHbccServices = new ReportTinhtrangnktHbccServices();
    repTinhTrangNktHbcc =  <ReportTinhTrangNktHbcc>[];
    reportItem = new ReportTinhTrangNktHbcc();
  }

  getListReportTinhtrangnktHbcc({int month: 0, int year: 2020}) async {
    notifyListeners();
    reportTinhTrangNktHbccResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTinhTrangNktHbcc = await reportTinhTrangNktHbccServices.listReportTinhtrangnktHbcc(month, year);
      reportTinhTrangNktHbccResponseList = ApiResponse.completed(repTinhTrangNktHbcc);
    } catch(e) {
      reportTinhTrangNktHbccResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getListReportTinhtrangnktHbccTheoKhoangThang({required int fromMonth,required int toMonth, int year: 2020}) async {
    notifyListeners();
    reportTinhTrangNktHbccResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTinhTrangNktHbcc = await reportTinhTrangNktHbccServices.listReportTinhtrangnktHbccTheoKhoangThang(fromMonth, toMonth, year);
      reportTinhTrangNktHbccResponseList = ApiResponse.completed(repTinhTrangNktHbcc);
    } catch(e) {
      reportTinhTrangNktHbccResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  add(ReportTinhTrangNktHbcc _reportItem) async {
    notifyListeners();
    reportItemResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      reportItem = await reportTinhTrangNktHbccServices.add(_reportItem);
      reportItemResponse = ApiResponse.completed(reportItem);
    }
    catch (e) {
      reportItemResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  update(ReportTinhTrangNktHbcc _reportItem) async {
    notifyListeners();
    try {
      reportItem = await reportTinhTrangNktHbccServices.update(_reportItem);
    }
    catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }
}