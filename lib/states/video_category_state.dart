import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/video_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/user_services.dart';
import 'package:hmhapp/services/video_category_services.dart';

class VideoCategoryState extends ChangeNotifier {

  late VideoCategoryServices _services;
  late ApiResponse<List<VideoCategory>> videoCategoryResponse;
  late List<VideoCategory> _categories;

  VideoCategoryState() {
    _services = VideoCategoryServices();
  }

  getList() async{
    videoCategoryResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _categories = await _services.listCategories();
      videoCategoryResponse = ApiResponse.completed(_categories);
    }
   catch (e) {
     videoCategoryResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

}
