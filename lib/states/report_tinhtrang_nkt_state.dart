import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/report_tinhtrang_nkt.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/report_tinhtrang_nkt_services.dart';

class ReportTinhTrangNKTState extends ChangeNotifier{
  late ApiResponse<List<ReportTinhTrangNKT>> reportTinhTrangNKTResponseList;
  late ReportTinhTrangNKTServices reportTinhTrangNKTServices;
  late List<ReportTinhTrangNKT> repTinhTrangNKT;
  late ApiResponse<ReportTinhTrangNKT> reportItemResponse;
  late ReportTinhTrangNKT? reportItem;

  ReportTinhTrangNKTState() {
    reportTinhTrangNKTServices = new ReportTinhTrangNKTServices();
    repTinhTrangNKT =  <ReportTinhTrangNKT>[];
    reportItem = new ReportTinhTrangNKT();
  }

  getListReportTinhTrangNKT({int month: 0, int year: 2020, int mId:0}) async {
    notifyListeners();
    reportTinhTrangNKTResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTinhTrangNKT = await reportTinhTrangNKTServices.listReportTinhTrangNKT(month, year, mId);
      reportTinhTrangNKTResponseList = ApiResponse.completed(repTinhTrangNKT);
    } catch(e) {
      reportTinhTrangNKTResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  add(ReportTinhTrangNKT _reportItem) async {
    notifyListeners();
    reportItemResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      reportItem = await reportTinhTrangNKTServices.add(_reportItem);
      reportItemResponse = ApiResponse.completed(reportItem);
    }
    catch (e) {
      reportItemResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  update(ReportTinhTrangNKT _reportItem) async {
    notifyListeners();
    try {
      reportItem = await reportTinhTrangNKTServices.update(_reportItem);
    }
    catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }
}