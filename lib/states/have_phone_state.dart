import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/have_phone.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/da_cam_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/have_phone_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HavePhoneState extends ChangeNotifier{
  late HavePhoneServices _havePhoneServices;
  late ApiResponse<List<HavePhone>> havePhoneResponseList;
  late List<HavePhone> havePhones;
  //int havePhoneId;
  late bool havePhoneCode;
  late bool isSmartphoneCode;

  HavePhoneState() {
    _havePhoneServices = new HavePhoneServices();
    havePhones = <HavePhone>[];
  }

  listHavePhones() async {
    havePhoneResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      havePhones = await _havePhoneServices.listPhones();
      havePhoneResponseList = ApiResponse.completed(havePhones);
    } catch(e) {
      havePhoneResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setHavePhoneCode(bool code) {
    this.havePhoneCode = code;
    notifyListeners();
  }

  getHavePhoneCode() {
    return this.havePhoneCode;
  }


  setIsSmartphoneCode(bool code) {
    this.isSmartphoneCode = code;
    notifyListeners();
  }

  getIsSmartphoneCode() {
    return this.isSmartphoneCode;
  }
}