import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';

class PersonalAssistantState extends ChangeNotifier{
  late PersonalAssistantServices _personalAssistantServices;
  late ApiResponse<List<PersonalAssistant>> personalAssistantResponseList;
  late ApiResponse<PersonalAssistant> personalAssistantResponse;
  List<PersonalAssistant>? personalAssistants;
  int? personalAssistantId;
  PersonalAssistant personalAssistant = new PersonalAssistant();

  PersonalAssistantState() {
    _personalAssistantServices = new PersonalAssistantServices();
    personalAssistants = <PersonalAssistant>[];
    personalAssistant = new PersonalAssistant();
  }

  listPersonalAssistants() async {
    personalAssistantResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      personalAssistants = await _personalAssistantServices.listPersonalAssistants();
      personalAssistantResponseList = ApiResponse.completed(personalAssistants);
    } catch(e) {
      personalAssistantResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setPersonalAssistantId(int gId) {
    this.personalAssistantId = gId;
  }

  getPersonalAssistantId() {
    return this.personalAssistantId;
  }

  setBirthDate(DateTime dateTime) {
    personalAssistant!.birth_date = dateTime.toString();
    notifyListeners();
  }

  DateTime? getBirthDate() {
    if(personalAssistant!.birth_date != null && personalAssistant!.birth_date != "")
      return DateTime.parse(personalAssistant!.birth_date!);
    else return null;
  }

  updatePersonalAssistant(PersonalAssistant per, int pId) async {
    notifyListeners();
    personalAssistantResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var p = await _personalAssistantServices.updatePersonalAssistant(per, pId);
      personalAssistantResponse = ApiResponse.completed(p);
    }
    catch (e) {
      personalAssistantResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setvaitro_ncsc_chichamsoc_nkt(val) {
    personalAssistant!.vaitro_ncsc_chichamsoc_nkt = val;
    notifyListeners();
  }

  getvaitro_ncsc_chichamsoc_nkt() {
    return this.personalAssistant!.vaitro_ncsc_chichamsoc_nkt;
  }

  setvaitro_ncsc_laodong_taothunhap(val) {
    personalAssistant.vaitro_ncsc_laodong_taothunhap = val;
    notifyListeners();
  }

  getvaitro_ncsc_laodong_taothunhap() {
    return this.personalAssistant.vaitro_ncsc_laodong_taothunhap;
  }

  setvaitro_ncsc_nguoics_cacthanhvien(val) {
    personalAssistant.vaitro_ncsc_nguoics_cacthanhvien = val;
    notifyListeners();
  }

  getvaitro_ncsc_nguoics_cacthanhvien() {
    return this.personalAssistant.vaitro_ncsc_nguoics_cacthanhvien;
  }

  setis_have_internet(val) {
    personalAssistant.is_have_internet = val;
    notifyListeners();
  }

  getis_have_internet() {
    return this.personalAssistant.is_have_internet;
  }

  setis_have_phone(val) {
    personalAssistant.is_have_phone = val;
    notifyListeners();
  }

  getis_have_phone() {
    return this.personalAssistant.is_have_phone;
  }

  setis_smartphone(val) {
    personalAssistant.is_smartphone = val;
    notifyListeners();
  }

  getis_smartphone() {
    return this.personalAssistant.is_smartphone;
  }

  createNCS(PersonalAssistant per) async {
    notifyListeners();
    personalAssistantResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      var p = await _personalAssistantServices.createNCS(per);
      personalAssistantResponse = ApiResponse.completed(p);
    }
    catch (e) {
      personalAssistantResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}