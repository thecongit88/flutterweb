import 'dart:collection';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/video_categories.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_type_services.dart';
import 'package:hmhapp/services/user_services.dart';
import 'package:hmhapp/services/video_list_view_services.dart';

class VideoListViewState extends ChangeNotifier {

  late VideoListViewServices _services;
  late ApiResponse<List<YoutubeModel>> youtubeVideoResponseList;
  late List<YoutubeModel> _listVideosByCategory;

  late List<YoutubeModel> videosSearch;
  late ApiResponse<List<YoutubeModel>> postSearchResponse;
  late int pageSearch;
  late int totalSearchPages;
  late VideosPageResponse _postSearchResponse;

  VideoListViewState() {
    _services = VideoListViewServices();
    _listVideosByCategory = <YoutubeModel>[];
    videosSearch = <YoutubeModel>[];
  }

  getList(category_video_id) async {
    youtubeVideoResponseList = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _listVideosByCategory = await _services.listVideosByCatgoryId(category_video_id);
      youtubeVideoResponseList = ApiResponse.completed(_listVideosByCategory);
    }
   catch (e) {
     youtubeVideoResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  Future search(String? search,int pg) async {
    pageSearch = pg;
    postSearchResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _postSearchResponse = await _services.search(pg, search);
      if(_postSearchResponse.items.isNotEmpty) {
        videosSearch.addAll(_postSearchResponse.items);
      }
      postSearchResponse = ApiResponse.completed(videosSearch);
      totalSearchPages = _postSearchResponse.pages;
    }
    catch (e) {
      postSearchResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }
}
