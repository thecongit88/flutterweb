import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/post_category_services.dart';
import 'package:hmhapp/services/post_services.dart';

class PostState extends ChangeNotifier {

  late PostServices _services;
  late  ApiResponse<List<Post>> postResponse;
  List<Post> _posts = <Post>[];


  late PostCategoryServices _categoryServices;
  late ApiResponse<List<PostCategory>> postCategoryResponse;
  List<PostCategory> _categories = <PostCategory>[];

  int categoryId = 0;


  int _count = 0;
  int page = 1;
  int totalPages = 0;
  late ApiResponse<List<Post>> postAllResponse;
  List<Post> postAll = <Post>[];
  late PostPageResponse _postPageResponse;


  late ApiResponse<List<Post>> postSearchResponse;
  List<Post> postSearch= <Post>[];
  int pageSearch  = 0;
  int totalSearchPages = 0;
  late PostPageResponse _postSearchResponse;

  PostState() {
    postAll = <Post>[];
    postSearch = <Post>[];
    _services = PostServices();
    _categoryServices = PostCategoryServices();
  }

  getList() async{
    postResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _posts = await _services.listPosts();
      postResponse = ApiResponse.completed(_posts);
    }
   catch (e) {
     postResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }


  getAll(pg,int categoryId) async {
    page = pg;
    postAllResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _postPageResponse = await _services.listAll(pg, categoryId);
      postAll.addAll(_postPageResponse.items);
      postAllResponse = ApiResponse.completed(postAll);
      totalPages = _postPageResponse.pages;
    }
    catch (e) {
      postAllResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getListCategories() async{
    postCategoryResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _categories = await _categoryServices.listCategories();
      postCategoryResponse = ApiResponse.completed(_categories);
    }
    catch (e) {
      postCategoryResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setCategoryId(int id) async{
    categoryId = id;
    postAll.clear();
    await getAll(1, categoryId);
    notifyListeners();
  }


  search(String search,int pg) async {
    pageSearch = pg;
    postSearchResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _postSearchResponse = await _services.search(pg, search);
      if(_postSearchResponse.items.isNotEmpty) {
        postSearch.addAll(_postSearchResponse.items);
      }
      postSearchResponse = ApiResponse.completed(postSearch);
      totalSearchPages = _postSearchResponse.pages;
    }
    catch (e) {
      postSearchResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

}
