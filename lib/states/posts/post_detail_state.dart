

import 'package:flutter/material.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/post_services.dart';

class PostDetailState extends ChangeNotifier {
  bool? isLoading,
      isRelatedLoading,
      doesContainReviews;
  Post? _post;
  List<Post> _relatedPosts = <Post>[];
  late PostServices _services;

  bool _isLike = false;
  int _postId = 0;
  List<int> _categoryIDs =  <int>[];


  late ApiResponse<Post> postResponse;
  late ApiResponse<List<Post>> relatedPostResponse;


  PostDetailState() {
    isLoading = true;
    isRelatedLoading = true;
    _isLike = false;
    _relatedPosts =  <Post>[];
    _services = PostServices();
  }

  Post? get post => _post;
  List<Post> get relatedPosts => _relatedPosts;
  bool get isLike => _isLike;


  getPost(int id) async{
    postResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _post = await _services.getPost(id);
      postResponse = ApiResponse.completed(_post);
    }
    catch (e) {
      postResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getRelatedPosts(int categoryId,int currentId) async {
    relatedPostResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _relatedPosts = await _services.relatedPosts(categoryId,currentId);
      relatedPostResponse = ApiResponse.completed(_relatedPosts);
    }
    catch (e) {
      relatedPostResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

}

