import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/post_category_services.dart';

class PostCategoryState extends ChangeNotifier {

  late PostCategoryServices _services;
  late ApiResponse<List<PostCategory>> postCategoryResponse;
  List<PostCategory> _categories = <PostCategory>[];

  int categoryId = 0;

  PostCategoryState() {
    _services = PostCategoryServices();
    categoryId = 1;
  }

  getList() async{
    postCategoryResponse = ApiResponse.loading("Đang tải dữ liệu...");
    notifyListeners();
    try {
      _categories = await _services.listCategories();
      postCategoryResponse = ApiResponse.completed(_categories);
    }
   catch (e) {
     postCategoryResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  setCategoryId(int id){
    categoryId = id;
    notifyListeners();
  }
}
