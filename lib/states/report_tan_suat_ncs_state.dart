import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/report_tan_suat_services.dart';

class ReportTanSuatNCSState extends ChangeNotifier{
  late ApiResponse<List<ReportTanSuatNCS>> reporttansuatResponseList;
  late ReportTanSuatNCSServices reportTanSuatNCSServices;
  late List<ReportTanSuatNCS> repTanSuatNCS;
  late ApiResponse<ReportTanSuatNCS> reportItemResponse;
  late ReportTanSuatNCS? reportItem;

  ReportTanSuatNCSState() {
    reportTanSuatNCSServices = new ReportTanSuatNCSServices();
    repTanSuatNCS =  <ReportTanSuatNCS>[];
    reportItem = new ReportTanSuatNCS();
  }

  getListReportTanSuatNCS({int month: 0, int year: 2020, int mId:0}) async {
    notifyListeners();
    reporttansuatResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTanSuatNCS = await reportTanSuatNCSServices.listReportTanSuatNCS(month, year, mId);
      reporttansuatResponseList = ApiResponse.completed(repTanSuatNCS);
    } catch(e) {
      reporttansuatResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getListReportTanSuatThucHienKeHoachHBCC({int month: 0, int year: 2020}) async {
    notifyListeners();
    reporttansuatResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTanSuatNCS = await reportTanSuatNCSServices.listReportTanSuatThucHienKeHoachHBCC(month, year);
      reporttansuatResponseList = ApiResponse.completed(repTanSuatNCS);
    } catch(e) {
      reporttansuatResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  getListReportTanSuatThucHienKeHoachHBCCTheoKhoangThang({required int fromMonth,required int toMonth, int year: 2020}) async {
    notifyListeners();
    reporttansuatResponseList = ApiResponse.loading("Đang tải dữ liệu ...");
    notifyListeners();
    try {
      repTanSuatNCS = await reportTanSuatNCSServices.listReportTanSuatThucHienKeHoachHBCCTheoKhoangThang(fromMonth, toMonth, year);
      reporttansuatResponseList = ApiResponse.completed(repTanSuatNCS);
    } catch(e) {
      reporttansuatResponseList = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  add(ReportTanSuatNCS _reportItem) async {
    notifyListeners();
    reportItemResponse = ApiResponse.loading("Đang xử lý...");
    notifyListeners();
    try {
      reportItem = await reportTanSuatNCSServices.add(_reportItem);
      reportItemResponse = ApiResponse.completed(reportItem);
    }
    catch (e) {
      reportItemResponse = ApiResponse.error(e.toString());
    }
    notifyListeners();
  }

  update(ReportTanSuatNCS _reportItem) async {
    notifyListeners();
    try {
      reportItem = await reportTanSuatNCSServices.update(_reportItem);
    }
    catch (e) {
      throw(e.toString());
    }
    notifyListeners();
  }
}