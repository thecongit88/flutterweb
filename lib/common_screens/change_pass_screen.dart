import 'package:another_flushbar/flushbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:provider/provider.dart';

class ChangePassScreen extends StatefulWidget {
  ChangePassScreen({required Key key,required this.userName}) : super(key: key);

  final String userName;

  @override
  ChangePassScreenState createState() => ChangePassScreenState();
}

class ChangePassScreenState extends State<ChangePassScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _passwordConfirmController = TextEditingController();
  final _logo = "assets/icons/light/logo_white.png";

  final _codeController = TextEditingController();
  AuthState _authState = new AuthState();
  UserChangePassword _userChangePassword = new UserChangePassword();
  late Flushbar flush;
  bool _autoValidateChangePass = false;
  bool _autoValidateCode = false;
  bool _autoValidatePhone = false;

  Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  ManagerMainScreen() : MemberMainScreen();
  FirebaseAuth _auth = FirebaseAuth.instance;
  late String userName;
  late Widget _myApp;

  @override
  void initState() {
    userName = widget.userName;
    super.initState();
  }


  String _validatorTitle(String? value) {
    if (value != null && (!(value.length > 5) || value.isEmpty)) {
      return "Mật khẩu yêu cầu nhiều hơn 5 ký tự.";
    } else if (_passwordController.text != _passwordConfirmController.text) {
      return "Mật khẩu và Xác nhận mật khẩu không trùng nhau.";
    }
    return "";
  }


  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    String? _validatorCode(String value) {
      if (!(value.length == 6) || value.isEmpty) {
        return "Mã OTP yêu cầu 6 chữ số.";
      }
      return null;
    }

    return ChangeNotifierProvider(
        create: (_) => AuthState(),
        child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body:
            SingleChildScrollView(
                child: Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery
                                  .of(context)
                                  .size
                                  .height / 2.5,
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey.shade200,
                                        offset: Offset(2, 4),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ],
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        theme.primaryColor,
                                        theme.primaryColor.withOpacity(.75)
                                      ])),
                              child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      _title(),
                                      Text(
                                        "Ứng dụng chăm sóc sức khoẻ tại nhà",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  )),
                            ),
                            Form(
                                key: _formKey,
                                autovalidateMode: _autoValidateChangePass?
                                AutovalidateMode.always: AutovalidateMode.disabled,
                                child: Padding(
                                  padding: MediaQuery
                                      .of(context)
                                      .viewInsets,
                                  child: Container(
                                      padding: EdgeInsets.only(
                                          left: 40,
                                          right: 40,
                                          top: 30,
                                          bottom: 30),
                                      color: Colors.white,
                                      child: Stack(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment
                                                .center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                "Mật khẩu mới",
                                                style: TextStyle(
                                                    color: theme.primaryColor,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 22),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "Nhập mật khẩu mới của bạn vào ô dưới.",
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: 16),
                                              ),
                                              SizedBox(
                                                height: 30,
                                              ),
                                              TextFormField(
                                                controller: _passwordController,
                                                validator: _validatorTitle,
                                                obscureText: true,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 10, 20, 10),
                                                  filled: true,
                                                  hintText: 'Mật khẩu của bạn',
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius
                                                        .all(
                                                        Radius.circular(10.0)),
                                                    borderSide: BorderSide(
                                                        color: theme
                                                            .primaryColor),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius
                                                        .all(
                                                        Radius.circular(10.0)),
                                                    borderSide:
                                                    BorderSide(
                                                        color: Colors.grey),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              TextFormField(
                                                controller: _passwordConfirmController,
                                                obscureText: true,
                                                validator: _validatorTitle,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 10, 20, 10),
                                                  filled: true,
                                                  hintText: 'Xác nhận mật khẩu của bạn',
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius
                                                        .all(
                                                        Radius.circular(10.0)),
                                                    borderSide: BorderSide(
                                                        color: theme
                                                            .primaryColor),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius
                                                        .all(
                                                        Radius.circular(10.0)),
                                                    borderSide:
                                                    BorderSide(
                                                        color: Colors.grey),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 30,
                                              ),
                                              Consumer<AuthState>(
                                                  builder: (context, state,
                                                      child) {
                                                    return InkWell(
                                                      child:
                                                      Container(
                                                          child: Text(
                                                            'Cập nhật mật khẩu',
                                                            style: TextStyle(
                                                                fontSize: 18,
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          width:
                                                          MediaQuery
                                                              .of(context)
                                                              .size
                                                              .width,
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                              vertical: 15),
                                                          alignment: Alignment
                                                              .center,
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius
                                                                  .all(
                                                                  Radius
                                                                      .circular(
                                                                      5)),
                                                              boxShadow: <
                                                                  BoxShadow>[
                                                                BoxShadow(
                                                                    color: Colors
                                                                        .grey
                                                                        .shade200,
                                                                    offset: Offset(
                                                                        2, 4),
                                                                    blurRadius: 5,
                                                                    spreadRadius: 2)
                                                              ],
                                                              gradient: LinearGradient(
                                                                  begin: Alignment
                                                                      .centerLeft,
                                                                  end: Alignment
                                                                      .centerRight,
                                                                  colors: [
                                                                    theme
                                                                        .primaryColor,
                                                                    theme
                                                                        .primaryColor
                                                                        .withOpacity(
                                                                        .75)
                                                                  ]))),
                                                      onTap: () async {
                                                        if (_formKey
                                                            .currentState!
                                                            .validate()) {
                                                          _formKey.currentState!
                                                              .save();
                                                          _userChangePassword
                                                              .password =
                                                              _passwordController
                                                                  .text.trim();
                                                          _userChangePassword
                                                              .passwordConfirmation =
                                                              _passwordController
                                                                  .text.trim();
                                                          _userChangePassword
                                                              .username =
                                                              userName;
                                                          try {
                                                            state
                                                                .changePassword(
                                                                _userChangePassword);
                                                          } catch (e) {
                                                            flush =
                                                            Flushbar<bool>(
                                                                isDismissible: true,
                                                                backgroundColor: Colors
                                                                    .red,
                                                                message:
                                                                "Tài khoản hoặc mật khẩu bị sai.",
                                                                duration: Duration(
                                                                    seconds: 20),
                                                                mainButton: TextButton(
                                                                  onPressed: () {
                                                                    flush
                                                                        .dismiss(
                                                                        true);
                                                                    state
                                                                        .setChangePasswordInit();
                                                                  },
                                                                  child: Text(
                                                                    "Đóng",
                                                                  ),
                                                                ))
                                                              ..show(context);
                                                          }
                                                        } else {
                                                          setState(() {
                                                            _autoValidateChangePass =
                                                            true;
                                                          });
                                                        }
                                                      },
                                                    );
                                                  }),
                                            ],
                                          ),
                                          Consumer<AuthState>(
                                              builder: (context, state, child) {
                                                switch (state.changePassResponse
                                                    .status) {
                                                  case Status.LOADING:
                                                    return Positioned(
                                                        child: Container(
                                                          color: Colors.white30,
                                                          child: Center(
                                                            child: CircularProgressIndicator(
                                                                valueColor:
                                                                new AlwaysStoppedAnimation<
                                                                    Color>(
                                                                    Colors
                                                                        .green)),
                                                          ),
                                                        ));
                                                    break;
                                                  case Status.COMPLETED:
                                                      WidgetsBinding.instance
                                                          .addPostFrameCallback((
                                                      _) {
                                                        Navigator.of(context).pop(true);
                                                      });
                                                      return SizedBox(height: 0, );
                                                    break;
                                                  case Status.ERROR:
                                                    WidgetsBinding.instance
                                                        .addPostFrameCallback((
                                                        _) {
                                                      flush = Flushbar(
                                                          isDismissible: true,
                                                          backgroundColor: Colors
                                                              .red,
                                                          message:
                                                          "Tài khoản hoặc mật khẩu bị sai.",
                                                          duration: Duration(
                                                              seconds: 2),
                                                          mainButton: TextButton(
                                                            onPressed: () {
                                                              state
                                                                  .setChangePasswordInit();
                                                              flush.dismiss(
                                                                  true); // result = true
                                                            },
                                                            child: Text(
                                                              "Đóng",
                                                            ),
                                                          ))
                                                        ..show(context);
                                                    });
                                                    return SizedBox(
                                                      height: 0,
                                                    );
                                                    break;
                                                  case Status.INIT:
                                                    return SizedBox(
                                                      height: 0,
                                                    );
                                                    break;
                                                  default:
                                                    return SizedBox(
                                                      height: 0,
                                                    );
                                                    break;
                                                }
                                              }),
                                        ],
                                      )),
                                ))
                          ],
                        ),
                      ),
                      Positioned(
                        child:
                        InkWell(
                          child:
                          Icon(Icons.clear, color: Colors.white,),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                        top: 35,
                        left: 20,
                      )
                    ],
                  ),
                ))
        )
    );
  }

  Widget _title() {
    return new Container(
      width: 80.0,
      height: 80.0,
      decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage(_logo), fit: BoxFit.fitWidth)),
    );
  }
}
