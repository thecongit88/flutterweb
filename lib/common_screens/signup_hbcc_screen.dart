import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:hmhapp/services/ward_services.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/states/disability_start_state.dart';
import 'package:hmhapp/states/disability_type_state.dart';
import 'package:hmhapp/states/district_state.dart';
import 'package:hmhapp/states/gender_state.dart';
import 'package:hmhapp/states/hbcc_state.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/nation_state.dart';
import 'package:hmhapp/states/personal_assistant_state.dart';
import 'package:hmhapp/states/province_state.dart';
import 'package:hmhapp/states/ward_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SignUpHbccScreen extends StatefulWidget {
  SignUpHbccScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  SignUpHbccScreenState createState() => SignUpHbccScreenState();
}

class SignUpHbccScreenState extends State<SignUpHbccScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _userName = TextEditingController();
  final _fullName = TextEditingController();
  final _password = TextEditingController();
  final _passwordConfirm = TextEditingController();
  final _logo = "assets/icons/light/logo_white.png";
  String _phoneCode = "+84";

  final hoCtrl = TextEditingController();
  final tenCtrl = TextEditingController();
  final phoneCtrl = TextEditingController();
  final namSinhCtrl = TextEditingController();
  final diaChiCtrl = TextEditingController();
  final namNKTBiKTCtrl = TextEditingController();
  final dantocKhacCtrl = TextEditingController();

  final hotenCtrl = TextEditingController();


  final _provinceService = ProvinceServices();
  final _districtServices = DistrictServices();
  final _wardServices = WardServices();
  final _authServices = AuthServices();
  final _memberServices = MemberServices();

  var provinceSelect = 0;
  var districtSelect = 0;
  var wardSelect = 0;

  var provinceCode = "";
  var orderProvince = 0;
  Province provinceObj = Province();

  MemberState memberState = MemberState();
  MemberHomeState memberHomeState = MemberHomeState();
  GenderState genderState = GenderState();
  NationState nationState = NationState();
  DisabilityStartState disabilityStartState = DisabilityStartState();
  DisabilityTypeState disabilityTypeState = DisabilityTypeState();
  ProvinceState provinceState = ProvinceState();
  DistrictState districtState = DistrictState();
  WardState wardState = WardState();
  PersonalAssistantState personalAssistantState = PersonalAssistantState();
  HbccState hbccState = HbccState();
  int? currentYear, currentMonth;

  DateTime? joinProjectDate;
  String joinProjectDateLabel = "";
  int birthYear = 0;
  int disableYear = 0;
  int genderId = 0, genderIdNcs = 0, disabilityTypeId = 0, nationId = 0, provinceId = 0, districtId = 0
  , wardId = 0, ncsId = 0,disabilityStartId=0;
  //String provinceCode= "", districtCode = "", wardCode = "";
  String? daCamCode;

  bool changeDropDown = false;

  DateTime? _projectJoinDate, _birthDate, _disabilityDate;
  var formatStr =  "dd-MM-yyyy";
  final String _dateLabel = "Chọn ngày sinh";


  late AuthState _authState;
  final Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  const ManagerMainScreen() : MemberMainScreen();
  late Widget _myApp;

  List<DropdownMenuItem<int>> _nations = [const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn dân tộc"),
  )];

  List<DropdownMenuItem<int>> _disableTypes = [const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn dạng khuyết tật"),
  )];

  List<DropdownMenuItem<int>> _genders = [const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn giới tính"),
  )];

  final List<DropdownMenuItem<int>> _gendersNcsTmp = [const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn giới tính"),
  )];

  final List<DropdownMenuItem<int>> _tinh = [];
  final _tinhDefault = const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn tỉnh"),
  );

  final _huyenDefault = const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn huyện"),
  );
  final List<DropdownMenuItem<int>> _huyen = [];
  final _xaDefault= const DropdownMenuItem<int>(
    value: 0,
    child: Text("Chọn xã"),
  );
  final List<DropdownMenuItem<int>> _xa = [];

  List<DropdownMenuItem<int>> _disabilityStarts = [];

  Member? member;
  List<Province> listTinh = <Province>[];
  List<District> listHuyenByTinh = <District>[];
  List<Ward> listXaByHuyen = <Ward>[];
  late GenderServices _genderServices;

  Future<User> registerUser() async {
    var phone = Functions.getStandardPhone(phone: _userName.text, maQuocGia: _phoneCode);

    var user = UserRegister()
      ..username = phone
      ..email = '${phone}@gmail.com'
      ..provider = 'local'
      ..password = _password.text.trim()
      ..resetPasswordToken=''
      ..confirmed = false
      ..blocked = false;
    return _authServices.signUp(user);
  }


  @override
  void initState() {
    super.initState();
    _authState = AuthState();
    _myApp = Constants.APP_MODULE == Constants.HBCC ?  MyAppManager(defaultHome: _defaultHome,) :
    MyAppMember(defaultHome: _defaultHome,);

    memberState.getMember();
    provinceState.listProvinces();
    districtState.listDistricts();
    wardState.listWards();
    _tinh.add(_tinhDefault);
    _huyen.add(_huyenDefault);
    _xa.add(_xaDefault);
    listTinh = <Province>[];
    getAllTinh();

  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    bool autoValidate = false;
    Widget _entryField(String title, TextEditingController controller,Function validator, TextInputType inputType,
        {bool isPassword = false}) {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 6),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
                controller: controller,
                validator: validator(),
                obscureText: isPassword,
                keyboardType: inputType,
                decoration: InputDecoration(
                  hintText: title,
                    border: InputBorder.none,
                    fillColor: const Color(0xfff3f3f4),
                    filled: true),
            )
          ],
        ),
      );
    }

    String? _validatorFullName(String? value) {
      if (value == null || value.isEmpty) {
        return "Họ tên yêu cầu phải nhập.";
      }
      return "";
    }

    String? _validatorTitle(String? value) {
      if (value == null || !(value.length > 5) || value.isEmpty) {
        return "Mật khẩu yêu cầu nhiều hơn 5 ký tự.";
      }else if(_password.text != _passwordConfirm.text){
        return "Mật khẩu và Xác nhận mật khẩu không trùng nhau.";
      }
      return "";
    }

    String? _validatorPhone(String? value) {
      if (value == null || !(value.length >= 9) || value.isEmpty) {
        return "Số điện thoại yêu cầu nhiều hơn 9 chữ số.";
      }
      return "";
    }

    String? _validatorGioiTinh(int? value) {
      if (value!= null && value <= 0) {
        return "Giới tính cần phải chọn.";
      }
      return "";
    }

    String? _validatorBirthDate(String? value) {
      if (value == null || value.isEmpty) {
        return "Ngày sinh cần được chọn.";
      }
      return "";
    }

    String? _validatorDangKT(int? value) {
      if (value == null || value<=0) {
        return "Dạng khuyết tật cần được chọn.";
      }
      return "";
    }

    String? _validatorThoiDiemKT(int? value) {
      if (value == null || value<=0) {
        return "Thời điểm khuyết tật cần được chọn.";
      }
      return "";
    }

    String? _validatorNamKT(String? value) {
      if (value == null || value.isEmpty) {
        return "Năm khuyết tật cần được nhập.";
      }
      return "";
    }

    String _validatorTinh(int value) {
      if (value<=0) {
        return "Tỉnh cần được chọn.";
      }
      return "";
    }

    String _validatorHuyen(int value) {
      if (value<=0) {
        return "Huyện cần được chọn.";
      }
      return "";
    }

    String? _validatorXa(int? value) {
      if (value == null || value<=0) {
        return "Xã cần được chọn.";
      }
      return "";
    }

    String? _validatorDiaChi(String? value) {
      if (value == null || value.isEmpty) {
        return "Địa chỉ cần được nhập.";
      }
      return "";
    }

    Widget _phoneField(String title, TextEditingController controller,Function validator) {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
                width: 130,
                child:
                ColoredBox(
                  color: const Color(0xfff3f3f4),
                  child:
                  Padding(
                    padding: const EdgeInsets.only(left: 10,right: 0),
                    child:
                    CountryCodePicker(
                      showDropDownButton: true,
                      onChanged: (CountryCode countryCode) {
                        _phoneCode = countryCode.toString();
                      },// Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: '+84',
                      favorite: ['+84','+1','+1','+44','+61'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      showOnlyCountryWhenClosed: false,
                      // optional. aligns the flag and the Text left
                      alignLeft: false,
                    ),
                  ),
                )
            ),
            const SizedBox(width: 10),
            SizedBox(
              width: MediaQuery.of(context).size.width - 180,
              child:
              TextFormField(
                controller: controller,
                validator: validator(),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: title,
                    border: InputBorder.none,
                    fillColor: const Color(0xfff3f3f4),
                    filled: true),
              ),
            )
          ],
        ),
      );
    }


    Widget _submitButton(state) {
      return
        GestureDetector(
          onTap: () async {
            if (_formKey.currentState!.validate()) {
              //kiểm tra thông tin username (sdt)
              //await memberState.getUserItem(userName: _userName.text.trim());
              var phone = Functions.getStandardPhone(phone: _userName.text, maQuocGia: _phoneCode);
              _memberServices.getUserItem(userName: phone).then((value){
                if(value > 0) {
                  Flushbar(
                    message: 'Số điện thoại này đã được đăng ký, vui lòng nhập số khác!',
                    backgroundColor: Colors.red,
                    duration: const Duration(seconds: 2),
                    flushbarPosition: FlushbarPosition.TOP,
                    icon: const Icon(Icons.remove_circle,color: Colors.white,),
                  ).show(context).then((shouldUpdate) {
                  });
                } else {
                  //thêm người chăm sóc
                  registerUser().then((user) {
                    var userId = user.id;
                    var hbccItem = HBCC();
                    hbccItem.phone = phone;
                    hbccItem.fullName = hotenCtrl.text.trim();
                    hbccItem.provinceId = provinceSelect;
                    hbccItem.districtId = districtSelect;
                    hbccItem.wardId = wardSelect;
                    hbccItem.address = diaChiCtrl.text.trim();
                    hbccItem.userId = userId;
                    hbccItem.approved = false;
                    var arr = getFirstLastName(hbccItem!.fullName ?? "");
                    hbccItem.firstName = arr.length > 0 ? arr[0] : "";
                    hbccItem.lastName = arr.length > 1 ? arr[1] : "";

                    //create hbcc
                    hbccState.create(hbccItem);
                    /*
                      Flushbar(
                        message: 'Bạn đã đăng ký HBCC thành công!',
                        backgroundColor: Colors.green,
                        duration: Duration(seconds: 2),
                        flushbarPosition: FlushbarPosition.TOP,
                        icon: Icon(Icons.check_circle,color: Colors.white,),
                      ).show(context).then((shouldUpdate) {
                      });
                    */
                    }).catchError((onError){
                    Flushbar(
                      message: 'Đã xảy ra lỗi trong quá trình đăng ký tài khoản',
                      duration: const Duration(seconds: 2),
                      flushbarPosition: FlushbarPosition.TOP,
                      backgroundColor: Colors.red,
                      icon: const Icon(Icons.check_circle,color: Colors.white,),
                    ).show(context).then((shouldUpdate) {
                    });
                  });

                }

              });
            }else{
              setState(() {
                autoValidate = true;
              });
              return;
            }
          },
          child:
          Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: const Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [theme.primaryColor.withOpacity(.75), theme.primaryColor])),
              child:const Text(
                'Đăng ký ngay',
                style: TextStyle(fontSize: 18, color: Colors.white),
              )
          ),
        );
    }


    Widget _loginAccountLabel() {
       return Container(
        margin: const EdgeInsets.symmetric(vertical: 20),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Bạn đã có tài khoản ?',
              style: TextStyle(fontSize: 15),
            ),
            const SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => SignInScreen()));
                },
              child: Text(
                'Đăng nhập',
                style: TextStyle(
                    color: theme.primaryColor,
                    fontSize: 15,),
              ),
            )
          ],
        ),
      );
    }

    Widget _title() {
      return Container(
        width: 80.0,
        height: 80.0,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(_logo), fit: BoxFit.fitWidth)),
      );
    }

    Widget _titleScreen() {
      return
        const Padding(padding: EdgeInsets.only(bottom: 8),
        child:
        Text('Đăng ký tài khoản',
                style: TextStyle(color: Colors.black87, fontSize: 20),
        ));
    }

    Widget wdgGioiTinhRender(ApiResponse<List<Gender>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
        case Status.COMPLETED:
          _genders.clear();
          _genders = [const DropdownMenuItem<int>(
            value: 0,
            child: Text("Chọn giới tính"),
          )];

          data.data!.forEach((Gender gd) {
            _genders.add(DropdownMenuItem<int>(
              value: gd.id,
              child: Text(gd.name ?? ""),
            ));
          });

          return DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            validator: _validatorGioiTinh,
            decoration: const InputDecoration(
                hintText: "Chọn giới tính",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            items: _genders,
            value: _genders.length > 1 ? genderId : 0,
            onChanged: (value) {
              genderState.setGenderId(value!);
            },
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return const SizedBox();
          break;
      }
    }

    Widget wdgDanTocRender(ApiResponse<List<Nation>> data, BuildContext context) {
      switch (data.status) {
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
        case Status.COMPLETED:
          _nations.clear();
          _nations = [
            const DropdownMenuItem<int>(
              value: 0,
              child: Text("Chọn dân tộc"),
            )
          ];
          data.data!.forEach((Nation dt) {
            _nations.add(DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title ?? ""),
            ));
          });

          var otherCheck = Nation.isOther(data.data!, nationState.getNationId());
          if(!otherCheck){
            dantocKhacCtrl.clear();
          }

          return Column(
            children: <Widget>[
              DropdownButtonFormField<int>(
                decoration: const InputDecoration(
                    hintText: "Chọn dân tộc",
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                isExpanded: true,
                isDense: true,
                items: _nations,
                value: _nations.length > 1 ? nationId : 0,
                onChanged: (int? value) {
                  nationState.setNationId(value ?? 0);
                },
              ),
              otherCheck
                  ? Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 25),
                    const Text(
                      'Dân tộc khác',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(
                      controller: dantocKhacCtrl,
                    ),
                  ],
                ),
              )
                  : const SizedBox(
                height: 0,
              ),
            ],
          );
        case Status.ERROR:
          return ErrorMessage(errorMessage: data.message);
        case Status.INIT:
          return const SizedBox.shrink();
      }
    }

    Widget wdgThoiDiemKTRender(ApiResponse<List<DisabilityStart>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
        case Status.COMPLETED:
          _disabilityStarts.clear();
          _disabilityStarts = [const DropdownMenuItem<int>(
            value: 0,
            child: Text("Chọn thời điểm khuyết tật"),
          )];
          data.data!.forEach((DisabilityStart dt) {
            _disabilityStarts.add(DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title),
            ));
          });


          var otherCheck = DisabilityStart.isOther(data.data!, disabilityStartState.getDisabilityStartId());
          if(!otherCheck){
            namNKTBiKTCtrl.clear();
          }

          return Column(
            children: <Widget>[
              DropdownButtonFormField<int>(
                validator: _validatorThoiDiemKT,
                decoration: const InputDecoration(
                    hintText: "Chọn thời điểm khuyết tật",
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                isExpanded: true,
                isDense: true,
                items: _disabilityStarts,
                value: _disabilityStarts.length > 1 ? disabilityStartId : 0,
                onChanged: (int? value) {
                  disabilityStartState.setDisabilityStartId(value ?? 0);
                },
              ),
              otherCheck
                  ? Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 15),
                    TextFormField(
                      validator: _validatorNamKT,
                      keyboardType: TextInputType.number,
                      controller: namNKTBiKTCtrl,
                      decoration: const InputDecoration(
                          hintText: "Cụ thể năm nào NKT bị khuyết tật?",
                          border: InputBorder.none,
                          fillColor: Color(0xfff3f3f4),
                          filled: true),
                    ),
                  ],
                ),
              )
                  : const SizedBox(
                height: 0,
              ),
            ],
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return const SizedBox();
          break;
      }
    }

    Widget wdgLoaiKtRender(ApiResponse<List<DisabilityType>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
        case Status.COMPLETED:
          _disableTypes.clear();
          _disableTypes = [const DropdownMenuItem<int>(
            value: 0,
            child: Text("Chọn dạng khuyết tật"),
          )];
          data.data!.forEach((DisabilityType dt) {
            _disableTypes.add(DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title),
            ));
          });
          return DropdownButtonFormField<int>(
            validator: _validatorDangKT,
            decoration: const InputDecoration(
                hintText: "Chọn dạng khuyết tật",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            isExpanded: true,
            isDense: true,
            items: _disableTypes,
            value: _disableTypes.length > 1 ? disabilityTypeId : 0,
            onChanged: (int? value) {
              disabilityTypeState.setloaiKtId(value ?? 0);
            },
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return const SizedBox();
          break;
      }
    }

    Widget HbccInfo(){
      return Column(
        children: <Widget>[
         Padding(
            padding: const EdgeInsets.only(bottom: 25.0,top: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'THÔNG TIN HBCC',
                  style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w500,
                      color: theme.primaryColor),
                ),
                const SizedBox(height: 10,),
                const Divider(height: 1,color: Colors.grey,),
                const SizedBox(height: 15,),
                TextFormField(
                  controller: hotenCtrl,
                  validator: _validatorFullName,
                  decoration: const InputDecoration(
                      hintText: "Họ và tên",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                ),
                const SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: const InputDecoration(
                      hintText: "Chọn Tỉnh",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _tinh,
                  value: provinceSelect,
                  onChanged: (int? value) {
                    setState(() {
                      provinceSelect = value ?? 0;
                    });
                    getAllHuyen(value!);
                  },
                ),
                const SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: const InputDecoration(
                      hintText: "Chọn Huyện",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _huyen,
                  value: districtSelect,
                  onChanged: (int? value) {
                    setState(() {
                      districtSelect = value ?? 0;
                    });
                    getAllXa(value ?? 0);
                  },
                ),
                const SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: const InputDecoration(
                      hintText: "Chọn Xã",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _xa,
                  value: wardSelect,
                  onChanged: (int? value) {
                    setState(() {
                      wardSelect = value ?? 0;
                    });
                  },
                ),
                const SizedBox(height: 15,),
                TextFormField(
                  validator: _validatorDiaChi,
                  controller: diaChiCtrl,
                  decoration: const InputDecoration(
                      hintText: "Địa chỉ cụ thể",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                ),
              ],
            ),
          )
        ],
      );
    }

    Widget _emailPasswordWidget() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 50,),
          Text(
          'THÔNG TIN TÀI KHOẢN',
          style: TextStyle(
              fontSize: 16.0, fontWeight: FontWeight.w500,
              color: theme.primaryColor),
        ),
          const SizedBox(height: 10,),
          const Divider(height: 1,color: Colors.grey,),
          const SizedBox(height: 15,),
          _phoneField("Số điện thoại", _userName, _validatorPhone),
          _entryField("Mật khẩu", _password,_validatorTitle,TextInputType.visiblePassword, isPassword: true),
          _entryField("Xác nhận mật khẩu", _passwordConfirm,_validatorTitle,TextInputType.visiblePassword, isPassword: true),
        ],
      );
    }

    return
      MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => disabilityStartState),
        ChangeNotifierProvider(create: (_) => hbccState),
        ChangeNotifierProvider(create: (_) => provinceState),
        ChangeNotifierProvider(create: (_) => districtState),
        ChangeNotifierProvider(create: (_) => wardState),
        ChangeNotifierProvider(create: (_) => _authState)],
    child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body:
    Form(
    key: _formKey,
    autovalidateMode: autoValidate ? AutovalidateMode.always : AutovalidateMode.disabled,
    child:
        SingleChildScrollView(
            child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height / 4.5,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            const BoxShadow(
                                color: Colors.white,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: [
                                theme.primaryColor.withOpacity(.75),
                                theme.primaryColor,
                              ])),
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _title(),
                          const Text(
                            "Ứng dụng chăm sóc sức khoẻ tại nhà",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child:
                      Column(
                        children: <Widget>[
                          _titleScreen(),
                          _emailPasswordWidget(),
                          HbccInfo(),
                          //SizedBox(height: 10,),
                          //ncsInfo(),
                          const SizedBox(height: 10,),
                          Consumer<HbccState>(
                              builder: (context, state, child) {
                                return _submitButton(state);
                              }),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: _loginAccountLabel(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Consumer<HbccState>(
                  builder: (context, state, child) {
                    switch (state.hbccCreateResponse.status) {
                      case Status.LOADING:
                        return
                          Positioned(
                              bottom: 300,
                              left: 0,
                              right: 0,
                              child: Align(
                                  alignment: Alignment.center,
                                  child:
                          Container(
                            color: Colors.white30,
                            child:
                            const Center(
                              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
                            ),
                          )));
                        break;
                      case Status.COMPLETED:

                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            Flushbar(
                              message: 'Bạn đã đăng ký tài khoản thành công!',
                              backgroundColor: Colors.green,
                              duration: const Duration(seconds: 2),
                              flushbarPosition: FlushbarPosition.TOP,
                              icon: const Icon(Icons.check_circle,color: Colors.white,),
                            ).show(context).then((shouldUpdate) {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                  (BuildContext context) => _myApp));
                            });

                          });

                        return const SizedBox();
                      case Status.ERROR:
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(backgroundColor: Colors.red,content: Text(state.hbccCreateResponse.message),
                                duration: const Duration(seconds: 20),action: SnackBarAction(
                                  label: 'Đóng',textColor: Colors.white, onPressed:ScaffoldMessenger.of(context).hideCurrentSnackBar,
                                ),)
                          );
                        });
                        return const SizedBox();
                      case Status.INIT:
                        return const SizedBox();
                      default:
                        return const SizedBox();
                    }
                  }),
            ],
          ),
        )))));
  }

  List<String> getFirstLastName(String fullName){
    var arr = <String>[];
    var idx = fullName.lastIndexOf(" ");
    if (idx != -1)
    {
      arr.add(fullName.substring(0, idx));
      arr.add(fullName.substring(idx + 1));
    }
    return arr;
  }

  String getMemberCode(List<Province> provinces, int provinceId){
    var memberCode = "";
    provinceObj = provinces.firstWhere((element) => element.id == provinceId);
    if(provinceObj != null){
      var ordering = provinceObj.ordering == null ||
          provinceObj.ordering.toString().isEmpty ? 0 : provinceObj.ordering;
      var prOrder = (ordering??0) + 1;
      var prCode = provinceObj.code;
      NumberFormat formatter = NumberFormat("0000000");
      memberCode = "${prCode}${formatter.format(prOrder)}";
    }
    return memberCode;
  }

  getAllTinh() {
    _tinh.clear();
    _tinh.add(_tinhDefault);
    _provinceService.listProvinces()
        .then((data) {

      listTinh.addAll(data);
      data.forEach((element) {
        var it = DropdownMenuItem<int>(
          value: element.id,
          child: Text(element.name ?? ""),
        );
        _tinh.add(it);
      });
    });
  }

  getAllHuyen(int provinceId) {
    districtSelect = 0;
    wardSelect = 0;
    _huyen.clear();
    _huyen.add(_huyenDefault);
    _xa.clear();
    _xa.add(_xaDefault);

    _districtServices
        .listDistrictsByProvince(provinceId)
        .then((data) {
      data.forEach((element) {
        var it = DropdownMenuItem<int>(
          value: element.id,
          child: Text(element.name),
        );
        _huyen.add(it);
      });
    });
  }

  getAllXa(int districtId) {
    wardSelect = 0;
    _xa.clear();
    _xa.add(_xaDefault);
    _wardServices
        .listWardsByDistrict(districtId)
        .then((data) {
      data.forEach((element) {
        var it = DropdownMenuItem<int>(
          value: element.id,
          child: Text(element.name ?? ""),
        );
        _xa.add(it);
      });
    });
  }

}
