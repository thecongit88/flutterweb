import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:hmhapp/services/ward_services.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/states/disability_start_state.dart';
import 'package:hmhapp/states/disability_type_state.dart';
import 'package:hmhapp/states/district_state.dart';
import 'package:hmhapp/states/gender_state.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/nation_state.dart';
import 'package:hmhapp/states/personal_assistant_state.dart';
import 'package:hmhapp/states/province_state.dart';
import 'package:hmhapp/states/ward_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  SignUpScreenState createState() => SignUpScreenState();
}

class SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _userName = TextEditingController();
  final _fullName = TextEditingController();
  final _password = TextEditingController();
  final _passwordConfirm = TextEditingController();
  String _phoneCode = "+84";
  final _logo = "assets/icons/light/logo_white.png";

  final hoCtrl = new TextEditingController();
  final tenCtrl = new TextEditingController();
  final phoneCtrl = new TextEditingController();
  final namSinhCtrl = new TextEditingController();
  final diaChiCtrl = new TextEditingController();
  final namNKTBiKTCtrl = new TextEditingController();
  final dantocKhacCtrl = new TextEditingController();

  final hoTenNcsCtrl = new TextEditingController();


  var _provinceService = new ProvinceServices();
  var _districtServices = new DistrictServices();
  var _wardServices = new WardServices();
  var _authServices = new AuthServices();
  var _memberServices = new MemberServices();

  var provinceSelect = 0;
  var districtSelect = 0;
  var wardSelect = 0;

  var provinceCode = "";
  var orderProvince = 0;
  Province provinceObj = new Province();

  MemberState memberState = new MemberState();
  MemberHomeState memberHomeState = new MemberHomeState();
  GenderState genderState = new GenderState();
  NationState nationState = new NationState();
  DisabilityStartState disabilityStartState = new DisabilityStartState();
  DisabilityTypeState disabilityTypeState = new DisabilityTypeState();
  ProvinceState provinceState = new ProvinceState();
  DistrictState districtState = new DistrictState();
  WardState wardState = new WardState();
  PersonalAssistantState personalAssistantState = new PersonalAssistantState();
  int? currentYear, currentMonth;

  DateTime? joinProjectDate;
  String joinProjectDateLabel = "";
  int birthYear = 0;
  int disableYear = 0;
  int genderId = 0, genderIdNcs = 0, disabilityTypeId = 0, nationId = 0, provinceId = 0, districtId = 0, wardId = 0, ncsId = 0,disabilityStartId=0;
  //String provinceCode= "", districtCode = "", wardCode = "";
  String? daCamCode;

  bool changeDropDown = false;

  DateTime? _projectJoinDate, _birthDate, _disabilityDate;
  var formatStr =  "dd-MM-yyyy";
  String _dateLabel = "Chọn ngày sinh";


  late AuthState _authState;
  Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  ManagerMainScreen() : MemberMainScreen();
  late Widget _myApp;

  List<DropdownMenuItem<int>> _nations = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn dân tộc"),
  )];

  List<DropdownMenuItem<int>> _disableTypes = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn dạng khuyết tật"),
  )];

  List<DropdownMenuItem<int>> _genders = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn giới tính"),
  )];

  List<DropdownMenuItem<int>> _gendersNcsTmp = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn giới tính"),
  )];

  List<DropdownMenuItem<int>> _tinh = [];
  var _tinhDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn tỉnh"),
  );

  var _huyenDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn huyện"),
  );
  List<DropdownMenuItem<int>> _huyen = [];
  var _xaDefault= new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn xã"),
  );
  List<DropdownMenuItem<int>> _xa = [];

  List<DropdownMenuItem<int>> _disabilityStarts = [];

  Member? member;
  List<Province> listTinh = <Province>[];
  List<District> listHuyenByTinh = <District>[];
  List<Ward> listXaByHuyen = <Ward>[];
  bool _changeTinh = false;

  late GenderServices _genderServices;
  late PersonalAssistantServices _personalAssistantServices;


  Future<User> registerUser() async {
    var _phone = Functions.getStandardPhone(phone: _userName.text, maQuocGia: _phoneCode);

    var user = new UserRegister()
      ..username = _phone
      ..email = '${_phone}@gmail.com'
      ..provider = 'local'
      ..password = _password.text.trim()
      ..resetPasswordToken=''
      ..confirmed = false
      ..blocked = false;
    return _authServices.signUp(user);
  }

  Future<PersonalAssistant?> registerNCS(PersonalAssistant personalAssistant) async {
    return _personalAssistantServices.createNCS(personalAssistant);
  }


  @override
  void initState() {
    super.initState();
    _authState = AuthState();
    _myApp = Constants.APP_MODULE == Constants.HBCC ?  MyAppManager(defaultHome: _defaultHome,) :
    MyAppMember(defaultHome: _defaultHome,);

    _personalAssistantServices = new PersonalAssistantServices();
    memberState.getMember();
    genderState.getListGenders();
    nationState.listNations();
    disabilityTypeState.listLoaiKT();
    provinceState.listProvinces();
    districtState.listDistricts();
    wardState.listWards();
    personalAssistantState.listPersonalAssistants();
    disabilityStartState.listDisabilityStarts();
    _tinh.add(_tinhDefault);
    _huyen.add(_huyenDefault);
    _xa.add(_xaDefault);
    listTinh = <Province>[];
    getAllTinh();

    _genderServices = new GenderServices();
    _genderServices.listGenders().then((value) {
      value.forEach((Gender gd) {
        _gendersNcsTmp.add(new DropdownMenuItem<int>(
          value: gd.id,
          child: Text(gd.name??""),
        ));
      });

    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    bool _autoValidate = false;
/*
    void registerUser(state) {
      var user = new UserRegister()
      ..username = _userName.text.trim()
      ..email = '${_userName.text.trim()}@gmail.com'
      ..provider = 'local'
      ..password = _password.text.trim()
      ..resetPasswordToken=''
      ..confirmed = false
      ..blocked = false;
      state.registerUser(user);
    }*/

    Widget _entryField(String title, TextEditingController controller,Function validator, TextInputType inputType,
        {bool isPassword = false}) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 6),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
                controller: controller,
                validator: validator(),
                obscureText: isPassword,
                keyboardType: inputType,
                decoration: InputDecoration(
                  hintText: title,
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
            )
          ],
        ),
      );
    }


    Widget _phoneField(String title, TextEditingController controller,Function validator) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
                width: 130,
                child:
                ColoredBox(
                  color: Color(0xfff3f3f4),
                  child:
                  Padding(
                    padding: EdgeInsets.only(left: 10,right: 0),
                    child:
                    CountryCodePicker(
                      showDropDownButton: true,
                      onChanged: (CountryCode countryCode) {
                        _phoneCode = countryCode.toString();
                      },// Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: '+84',
                      favorite: ['+84','+1','+1','+44','+61'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      showOnlyCountryWhenClosed: false,
                      // optional. aligns the flag and the Text left
                      alignLeft: false,
                    ),
                  ),
                )
            ),
            SizedBox(width: 10),
            SizedBox(
              width: MediaQuery.of(context).size.width - 180,
              child:
              TextFormField(
                controller: controller,
                validator: validator(),
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: title,
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
              ),
            )
          ],
        ),
      );
    }

    String? _validatorFullName(String? value) {
      if (value == null || value.isEmpty) {
        return "Họ tên yêu cầu phải nhập.";
      }
      return null;
    }

    String? _validatorTitle(String? value) {
      if (value == null || !(value.length > 5) || value.isEmpty) {
        return "Mật khẩu yêu cầu nhiều hơn 5 ký tự.";
      }else if(_password.text != _passwordConfirm.text){
        return "Mật khẩu và Xác nhận mật khẩu không trùng nhau.";
      }
      return null;
    }

    String? _validatorPhone(String? value) {
      if (value == null || !(value.length >= 9) || value.isEmpty) {
        return "Số điện thoại phải hơn 8 chữ số.";
      }
      return null;
    }

    String? _validatorGioiTinh(int? value) {
      if (value == null || value<= 0) {
        return "Giới tính cần phải chọn.";
      }
      return null;
    }

    String? _validatorBirthDate(String? value) {
      if (value == null || value.isEmpty) {
        return "Ngày sinh cần được chọn.";
      }
      return null;
    }

    String? _validatorDangKT(int? value) {
      if (value == null || value<=0) {
        return "Dạng khuyết tật cần được chọn.";
      }
      return null;
    }

    String? _validatorThoiDiemKT(int? value) {
      if (value == null || value<=0) {
        return "Thời điểm khuyết tật cần được chọn.";
      }
      return null;
    }

    String? _validatorNamKT(String? value) {
      if (value == null || value.isEmpty) {
        return "Năm khuyết tật cần được nhập.";
      }
      return null;
    }

    String? _validatorTinh(int? value) {
      if (value == null || value<=0) {
        return "Tỉnh cần được chọn.";
      }
      return null;
    }

    String? _validatorHuyen(int? value) {
      if (value == null || value<=0) {
        return "Huyện cần được chọn.";
      }
      return null;
    }

    String? _validatorXa(int? value) {
      if (value == null || value<=0) {
        return "Xã cần được chọn.";
      }
      return null;
    }

    String? _validatorDiaChi(String? value) {
      if (value == null || value.isEmpty) {
        return "Địa chỉ cần được nhập.";
      }
      return null;
    }



    Widget _submitButton(state) {
      return
        GestureDetector(
          onTap: () async {
            if (_formKey.currentState!.validate()) {
              //kiểm tra thông tin username (sdt)
              //await memberState.getUserItem(userName: _userName.text.trim());
               var _phone = Functions.getStandardPhone(phone: _userName.text, maQuocGia: _phoneCode);
              _memberServices.getUserItem(userName: _phone).then((value){
                if(value > 0) {
                  Flushbar(
                    message: 'Số điện thoại này đã được đăng ký, vui lòng nhập số khác!',
                    backgroundColor: Colors.red,
                    duration: Duration(seconds: 2),
                    flushbarPosition: FlushbarPosition.TOP,
                    icon: Icon(Icons.check_circle,color: Colors.white,),
                  ).show(context).then((shouldUpdate) {
                  });
                } else {
                  //thêm người chăm sóc
                  registerUser().then((user) {
                    PersonalAssistant per = new PersonalAssistant();
                    var arrHoTenNCS = getFirstLastName(hoTenNcsCtrl.text.trim());
                    per.firstName = arrHoTenNCS.length > 0 ? arrHoTenNCS[0] : "";
                    per.lastName = arrHoTenNCS.length > 1 ? arrHoTenNCS[1] : "";
                    per.genderId = genderIdNcs;
                    if (personalAssistantState.getBirthDate() != null)
                      per.birth_date = DateFormat("yyyy-MM-dd").format(
                          personalAssistantState.getBirthDate()!);
                    else
                      per.birth_date = null;

                    var userId = user.id;
                    //create ncs
                    _personalAssistantServices.createNCS(per).then((value) {
                      //thêm người khuyết tật
                      var mem = new Member();
                      mem.userId = userId;
                      mem.code = getMemberCode(listTinh, provinceSelect);
                      mem.fullName = hoCtrl.text.trim();
                      var arr = getFirstLastName(mem.fullName ?? "");
                      mem.firstName = arr.length > 0 ? arr[0] : "";
                      mem.lastName = arr.length > 1 ? arr[1] : "";
                      if (memberState.getProjectJoinDate() != null)
                        mem.projectJoinDate = DateFormat("yyyy-MM-dd").format(
                            memberState.getProjectJoinDate()!);
                      else
                        mem.projectJoinDate = null;
                      mem.phone = _phone;
                      mem.genderId = genderState.genderId;
                      mem.nationId = nationState.nationId;
                      mem.nation_other = dantocKhacCtrl.text;
                      mem.provinceId = provinceSelect;
                      mem.districtId = districtSelect;
                      mem.wardId = wardSelect;
                      mem.address = diaChiCtrl.text.trim();
                      mem.disabilityTypeId = disabilityTypeState.getloaiKtId();

                      mem.personal_assistant = value!.id;

                      if (memberState.getDisabilityDate() != null)
                        mem.disability_date = DateFormat("yyyy-MM-dd").format(
                            memberState.getDisabilityDate()!);
                      else
                        mem.disability_date = null;

                      if (memberState.getBirthDate() != null)
                        mem.birth_date = DateFormat("yyyy-MM-dd").format(
                            memberState.getBirthDate()!);
                      else
                        mem.birth_date = null;

                      if (memberState.getBirthDate() != null &&
                          namSinhCtrl.text.isNotEmpty)
                        mem.birth_year = int.parse(namSinhCtrl.text.trim());

                      mem.disabilityStartId =
                          disabilityStartState.getDisabilityStartId();
                      if (namNKTBiKTCtrl.text != null &&
                          namNKTBiKTCtrl.text.isNotEmpty)
                        mem.disability_year = int.parse(namNKTBiKTCtrl.text);

                      mem.avatar = "";
                      mem.isSuccess = false;
                      mem.approved = false;


                      memberState.createMember(mem);
                      //Cập nhật số thứ tự các memeber của Provicne;
                      provinceObj.ordering = (provinceObj.ordering ?? 0) + 1;
                      _provinceService.update(provinceObj);
                    });
                  }).catchError((onError){
                    Flushbar(
                      message: 'Đã xảy ra lỗi trong quá trình đăng ký tài khoản',
                      duration: Duration(seconds: 2),
                      flushbarPosition: FlushbarPosition.TOP,
                      backgroundColor: Colors.red,
                      icon: Icon(Icons.check_circle,color: Colors.white,),
                    ).show(context).then((shouldUpdate) {
                    });
                  });
                }

              });
            }else{
              setState(() {
                _autoValidate = true;
              });
              return;
            }
          },
          child:
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [theme.primaryColor.withOpacity(.75), theme.primaryColor])),
              child:Text(
                'Đăng ký ngay',
                style: TextStyle(fontSize: 18, color: Colors.white),
              )
          ),
        );
    }


    Widget _loginAccountLabel() {
       return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Bạn đã có tài khoản ?',
              style: TextStyle(fontSize: 15),
            ),
            SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => SignInScreen()));
                },
              child: Text(
                'Đăng nhập',
                style: TextStyle(
                    color: theme.primaryColor,
                    fontSize: 15,),
              ),
            )
          ],
        ),
      );
    }

    Widget _title() {
      return new Container(
        width: 80.0,
        height: 80.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage(_logo), fit: BoxFit.fitWidth)),
      );
    }

    Widget _titleScreen() {
      return
        Padding(padding: EdgeInsets.only(bottom: 8),
        child:
        Text('Đăng ký tài khoản',
                style: TextStyle(color: Colors.black87, fontSize: 20),
        ));
    }

    Widget wdgGioiTinhRender(ApiResponse<List<Gender>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
          break;
        case Status.COMPLETED:
          _genders.clear();
          _genders = [new DropdownMenuItem<int>(
            value: 0,
            child: new Text("Chọn giới tính"),
          )];

          data.data!.forEach((Gender gd) {
            _genders.add(new DropdownMenuItem<int>(
              value: gd.id,
              child: Text(gd.name ?? ""),
            ));
          });

          return DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            validator: _validatorGioiTinh,
            decoration: InputDecoration(
                hintText: "Chọn giới tính",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            items: _genders,
            value: _genders.length > 1 ? genderId : 0,
            onChanged: (int? value) {
              genderState.setGenderId(value ?? 0);
            },
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return SizedBox();
          break;
      }
    }

    Widget wdgDanTocRender(ApiResponse<List<Nation>> data, BuildContext context) {
      switch (data.status) {
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
          break;
        case Status.COMPLETED:
          _nations.clear();
          _nations = [
            new DropdownMenuItem<int>(
              value: 0,
              child: const Text("Chọn dân tộc"),
            )
          ];
          data.data!.forEach((Nation dt) {
            _nations.add(new DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title ?? ""),
            ));
          });

          var otherCheck = Nation.isOther(data.data!, nationState.getNationId());
          if(!otherCheck){
            dantocKhacCtrl.clear();
          }

          return Column(
            children: <Widget>[
              DropdownButtonFormField<int>(
                decoration: InputDecoration(
                    hintText: "Chọn dân tộc",
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                isExpanded: true,
                isDense: true,
                items: _nations,
                value: _nations.length > 1 ? nationId : 0,
                onChanged: (int? value) {
                  nationState.setNationId(value ?? 0);
                },
              ),
              otherCheck
                  ? Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 25),
                    const Text(
                      'Dân tộc khác',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(
                      controller: dantocKhacCtrl,
                    ),
                  ],
                ),
              )
                  : SizedBox(
                height: 0,
              ),
            ],
          );
          break;
        case Status.ERROR:
          return ErrorMessage(errorMessage: data.message);
          break;
        case Status.INIT:
          return SizedBox.shrink();
      }
    }

    Widget wdgThoiDiemKTRender(ApiResponse<List<DisabilityStart>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
          break;
        case Status.COMPLETED:
          _disabilityStarts.clear();
          _disabilityStarts = [new DropdownMenuItem<int>(
            value: 0,
            child: new Text("Chọn thời điểm khuyết tật"),
          )];
          data.data!.forEach((DisabilityStart dt) {
            _disabilityStarts.add(new DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title),
            ));
          });


          var otherCheck = DisabilityStart.isOther(data.data!, disabilityStartState.getDisabilityStartId());
          if(!otherCheck){
            namNKTBiKTCtrl.clear();
          }

          return Column(
            children: <Widget>[
              DropdownButtonFormField<int>(
                validator: _validatorThoiDiemKT,
                decoration: InputDecoration(
                    hintText: "Chọn thời điểm khuyết tật",
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                isExpanded: true,
                isDense: true,
                items: _disabilityStarts,
                value: _disabilityStarts.length > 1 ? disabilityStartId : 0,
                onChanged: (int? value) {
                  disabilityStartState.setDisabilityStartId(value ?? 0);
                },
              ),
              otherCheck
                  ? Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 15),
                    TextFormField(
                      validator: _validatorNamKT,
                      keyboardType: TextInputType.number,
                      controller: namNKTBiKTCtrl,
                      decoration: InputDecoration(
                          hintText: "Cụ thể năm nào NKT bị khuyết tật?",
                          border: InputBorder.none,
                          fillColor: Color(0xfff3f3f4),
                          filled: true),
                    ),
                  ],
                ),
              )
                  : SizedBox(
                height: 0,
              ),
            ],
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return SizedBox();
          break;
      }
    }

    Widget wdgLoaiKtRender(ApiResponse<List<DisabilityType>> data, BuildContext context) {
      switch(data.status){
        case Status.LOADING:
          return LoadingMessage(
            loadingMessage: data.message,
          );
          break;
        case Status.COMPLETED:
          _disableTypes.clear();
          _disableTypes = [new DropdownMenuItem<int>(
            value: 0,
            child: new Text("Chọn dạng khuyết tật"),
          )];
          data.data!.forEach((DisabilityType dt) {
            _disableTypes.add(new DropdownMenuItem<int>(
              value: dt.id,
              child: Text(dt.title),
            ));
          });
          return DropdownButtonFormField<int>(
            validator: _validatorDangKT,
            decoration: InputDecoration(
                hintText: "Chọn dạng khuyết tật",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            isExpanded: true,
            isDense: true,
            items: _disableTypes,
            value: _disableTypes.length > 1 ? disabilityTypeId : 0,
            onChanged: (int? value) {
              disabilityTypeState.setloaiKtId(value ?? 0);
            },
          );
          break;
        case Status.ERROR:
          return ErrorMessage(
              errorMessage: data.message
          );
          break;
        case Status.INIT:
          return SizedBox();
          break;
      }
    }

    Widget ncsInfo(){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'THÔNG TIN NGƯỜI CHĂM SÓC',
            style: TextStyle(
                fontSize: 16.0, fontWeight: FontWeight.w500,
                color: theme.primaryColor),
          ),
          SizedBox(height: 10,),
          Divider(height: 1,color: Colors.grey,),
          SizedBox(height: 15,),
          TextFormField(
            controller: hoTenNcsCtrl,
            validator: _validatorFullName,
            decoration: InputDecoration(
                hintText: "Họ và tên",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
          ),
          SizedBox(height: 15),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(1930, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                    print('confirm $date');
                    _birthDate = date;
                    personalAssistantState.setBirthDate(_birthDate!);
                  }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Consumer<PersonalAssistantState>(
                                builder: (context, state, child) {
                                  if (personalAssistantState.getBirthDate() != null)
                                    _dateLabel = DateFormat(formatStr)
                                        .format(
                                        personalAssistantState.getBirthDate()!)
                                        .toString();
                                  return Text(
                                    '${_dateLabel}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15.0),
                                  );
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ),
          SizedBox(height: 15),
          DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            validator: _validatorGioiTinh,
            decoration: InputDecoration(
                hintText: "Chọn giới tính",
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true),
            items: _gendersNcsTmp,
            value: _gendersNcsTmp.length > 1 ? genderIdNcs : 0,
            onChanged: (int? value) {
              setState(() {
                genderIdNcs = value ?? 0;
              });
            },
          ),
          /*
          Consumer<GenderState>(builder: (context, state, child) {
          return wdgGioiTinhRender(
            state.genderResponseList, context);
          }),
          */
          SizedBox(height: 15),
        ],
      );
    }

    Widget nktInfo(){
      return Column(
        children: <Widget>[
         Padding(
            padding: EdgeInsets.only(bottom: 25.0,top: 25),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'THÔNG TIN NGƯỜI KHUYẾT TẬT',
                  style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w500,
                      color: theme.primaryColor),
                ),
                SizedBox(height: 10,),
                Divider(height: 1,color: Colors.grey,),
                SizedBox(height: 15,),
                TextFormField(
                  controller: hoCtrl,
                  validator: _validatorFullName,
                  decoration: InputDecoration(
                      hintText: "Họ và tên",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                ),
                SizedBox(height: 15,),
                Consumer<GenderState>(
                    builder: (context, state, child) {
                      return wdgGioiTinhRender(state.genderResponseList, context);
                    }
                ),
                SizedBox(height: 15,),
                ElevatedButton(
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime(1900, 1, 1),
                        maxTime: DateTime.now(), onConfirm: (date) {
                          print('confirm $date');
                          _birthDate = date;
                          memberState.setBirthDate(_birthDate!);
                        },
                        currentTime: DateTime.now(), locale: LocaleType.vi
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18.0,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Consumer<MemberState>(
                                      builder: (context, state, child) {
                                        if(memberState.getBirthDate() != null)
                                          _dateLabel = DateFormat(formatStr).format(memberState.getBirthDate()!).toString();
                                        else _dateLabel = "Chọn ngày sinh";
                                        return Text(
                                          '${_dateLabel}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 15.0),
                                        );
                                      }
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 15,),
                Consumer<DisabilityTypeState>(
                    builder: (context, state, child) {
                      return wdgLoaiKtRender(state.disabilityTypeResponseList,context);
                    }
                ),
                SizedBox(height: 15,),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Consumer<DisabilityStartState>(
                              builder: (context, state, child) {
                                return wdgThoiDiemKTRender(state.disabilityStartResponseList,context);
                              }
                          ),
                          /*ElevatedButton(
                        onPressed: () {
                          DatePicker.showDatePicker(context,
                              theme: DatePickerTheme(
                                containerHeight: 210.0,
                              ),
                              showTitleActions: true,
                              minTime: DateTime(2000, 1, 1),
                              maxTime: DateTime.now(), onConfirm: (date) {
                                print('confirm $date');
                                _disabilityDate = date;
                                //setState(() {
                                //  _disabilityDateLabel = new DateFormat(formatStr).format(_disabilityDate).toString();
                                //});
                                memberState.setDisabilityDate(_disabilityDate);
                              },
                              currentTime: DateTime.now(), locale: LocaleType.vi
                          );
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 50.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.date_range,
                                          size: 18.0,
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Consumer<MemberState>(
                                            builder: (context, state, child) {
                                              if(memberState.getDisabilityDate() != null)
                                                _dateLabel = DateFormat(formatStr).format(memberState.getDisabilityDate()).toString();
                                              else _dateLabel = "Chọn ngày";
                                              return Text(
                                                '${_dateLabel}',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.normal,
                                                    fontSize: 15.0),
                                              );
                                            }
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Icon(Icons.arrow_drop_down)
                            ],
                          ),
                        ),
                        color: Colors.white,
                      ),*/
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                      hintText: "Chọn Tỉnh",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _tinh,
                  value: provinceSelect,
                  onChanged: (int? value) {
                    setState(() {
                      provinceSelect = value ?? 0;
                    });
                    getAllHuyen(value ?? 0);
                  },
                ),
                SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                      hintText: "Chọn Huyện",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _huyen,
                  value: districtSelect,
                  onChanged: (int? value) {
                    setState(() {
                      districtSelect = value ?? 0;
                    });
                    getAllXa(value ?? 0);
                  },
                ),
                SizedBox(height: 15,),
                DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                      hintText: "Chọn Xã",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  isExpanded: true,
                  isDense: true,
                  items: _xa,
                  value: wardSelect,
                  onChanged: (int? value) {
                    setState(() {
                      wardSelect = value ?? 0;
                    });
                  },
                ),
                SizedBox(height: 15,),
                TextFormField(
                  validator: _validatorDiaChi,
                  controller: diaChiCtrl,
                  decoration: InputDecoration(
                      hintText: "Địa chỉ cụ thể",
                      border: InputBorder.none,
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                ),
              ],
            ),
          )
        ],
      );
    }

    Widget _emailPasswordWidget() {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 50,),
          Text(
          'THÔNG TIN TÀI KHOẢN',
          style: TextStyle(
              fontSize: 16.0, fontWeight: FontWeight.w500,
              color: theme.primaryColor),
        ),
          SizedBox(height: 10,),
          Divider(height: 1,color: Colors.grey,),
          SizedBox(height: 15,),
          _phoneField("Số điện thoại", _userName, _validatorPhone),
//          _entryField("Số điện thoại", _userName, _validatorPhone,TextInputType.phone),
          _entryField("Mật khẩu", _password,_validatorTitle,TextInputType.visiblePassword, isPassword: true),
          _entryField("Xác nhận mật khẩu", _passwordConfirm,_validatorTitle,TextInputType.visiblePassword, isPassword: true),
        ],
      );
    }


    return
      MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => disabilityStartState),
        ChangeNotifierProvider(create: (_) => memberState),
        ChangeNotifierProvider(create: (_) => memberHomeState),
        ChangeNotifierProvider(create: (_) => genderState),
        ChangeNotifierProvider(create: (_) => nationState),
        ChangeNotifierProvider(create: (_) => disabilityTypeState),
        ChangeNotifierProvider(create: (_) => provinceState),
        ChangeNotifierProvider(create: (_) => districtState),
        ChangeNotifierProvider(create: (_) => wardState),
        ChangeNotifierProvider(create: (_) => personalAssistantState),
        ChangeNotifierProvider(create: (_) => _authState)],
    child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body:
    Form(
    key: _formKey,
    autovalidateMode: _autoValidate == true ? AutovalidateMode.always : AutovalidateMode.disabled,
    child:
        SingleChildScrollView(
            child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height / 4.5,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.white,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              colors: [
                                theme.primaryColor.withOpacity(.75),
                                theme.primaryColor,
                              ])),
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _title(),
                          Text(
                            "Ứng dụng chăm sóc sức khoẻ tại nhà",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      )),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child:
                      Column(
                        children: <Widget>[
                          _titleScreen(),
                          _emailPasswordWidget(),
                          nktInfo(),
                          SizedBox(height: 10,),
                          ncsInfo(),
                          SizedBox(height: 10,),
                          Consumer<MemberState>(
                              builder: (context, state, child) {
                                return _submitButton(state);
                              }),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: _loginAccountLabel(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Consumer<MemberState>(
                  builder: (context, state, child) {
                    switch (state.memberCreateResponse.status) {
                      case Status.LOADING:
                        return
                          Positioned(
                              bottom: 300,
                              left: 0,
                              right: 0,
                              child: Align(
                                  alignment: Alignment.center,
                                  child:
                          Container(
                            color: Colors.white30,
                            child:
                            Center(
                              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
                            ),
                          )));
                        break;
                      case Status.COMPLETED:

                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            Flushbar(
                              message: 'Bạn đã đăng ký thành công!',
                              backgroundColor: Colors.green,
                              duration: Duration(seconds: 2),
                              flushbarPosition: FlushbarPosition.TOP,
                              icon: Icon(Icons.check_circle,color: Colors.white,),
                            ).show(context).then((shouldUpdate) {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                  (BuildContext context) => _myApp));
                            });

                          });

                        return SizedBox();
                        break;
                      case Status.ERROR:
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(backgroundColor: Colors.red,content: Text(state.memberCreateResponse.message),
                                duration: Duration(seconds: 20),action: SnackBarAction(
                                  label: 'Đóng',textColor: Colors.white, onPressed:ScaffoldMessenger.of(context).hideCurrentSnackBar,
                                ),)
                          );
                        });
                        return SizedBox();
                        break;
                      case Status.INIT:
                        return SizedBox();
                        break;
                      default:
                        return SizedBox();
                        break;
                    }
                  }),
            ],
          ),
        )))));
  }

  List<String> getFirstLastName(String fullName){
    var arr = <String>[];
    var idx = fullName.lastIndexOf(" ");
    if (idx != -1)
    {
      arr.add(fullName.substring(0, idx));
      arr.add(fullName.substring(idx + 1));
    }
    return arr;
  }

  String getMemberCode(List<Province> provinces, int provinceId){
    var memberCode = "";
    provinceObj = provinces.firstWhere((element) => element.id == provinceId);
    if(provinceObj != null){
      int ordering = provinceObj.ordering == null || provinceObj.ordering!.toString().isEmpty
          ? 0 : provinceObj.ordering!;
      var prOrder = ordering + 1;
      var prCode = provinceObj.code;
      NumberFormat formatter = new NumberFormat("0000000");
      memberCode = "${prCode}${formatter.format(prOrder)}";
    }
    return memberCode;
  }

  getAllTinh() {
    _tinh.clear();
    _tinh.add(_tinhDefault);
    _provinceService.listProvinces()
        .then((data) {

      listTinh.addAll(data);
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.name ?? ""),
        );
        _tinh.add(it);
      });
    });
  }

  getAllHuyen(int provinceId) {
    districtSelect = 0;
    wardSelect = 0;
    _huyen.clear();
    _huyen.add(_huyenDefault);
    _xa.clear();
    _xa.add(_xaDefault);

    _districtServices
        .listDistrictsByProvince(provinceId)
        .then((data) {
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.name),
        );
        _huyen.add(it);
      });
    });
  }

  getAllXa(int districtId) {
    wardSelect = 0;
    _xa.clear();
    _xa.add(_xaDefault);
    _wardServices
        .listWardsByDistrict(districtId)
        .then((data) {
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.name ?? ""),
        );
        _xa.add(it);
      });
    });
  }

}
