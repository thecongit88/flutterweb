import 'package:another_flushbar/flushbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:provider/provider.dart';

class ConfirmCodeScreen extends StatefulWidget {
  ConfirmCodeScreen({required Key key,required this.verificationId}) : super(key: key);

  final String verificationId;

  @override
  ConfirmCodeScreenState createState() => ConfirmCodeScreenState();
}

class ConfirmCodeScreenState extends State<ConfirmCodeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _formValidateCodeKey = GlobalKey<FormState>();
  final _formValidatePhoneKey = GlobalKey<FormState>();
  final _logo = "assets/icons/light/logo_white.png";

  final _codeController = TextEditingController();
  late Flushbar flush;
  bool _autoValidateCode = false;
  bool _autoValidatePhone = false;

  Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  ManagerMainScreen() : MemberMainScreen();
  FirebaseAuth _auth = FirebaseAuth.instance;
  late String verificationId;

  @override
  void initState() {
    verificationId = widget.verificationId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    String _validatorCode(String? value) {
      if (value == null || (!(value.length == 6) || value.isEmpty)) {
        return "Mã OTP yêu cầu 6 chữ số.";
      }
      return "";
    }

    return ChangeNotifierProvider(
        create: (_) => AuthState(),
        child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
                child: Container(
                  child: Stack(
                    children: <Widget>[
                  Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height / 2.5,
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey.shade200,
                                        offset: Offset(2, 4),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ],
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        theme.primaryColor,
                                        theme.primaryColor.withOpacity(.75)
                                      ])),
                              child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      _title(),
                                      Text(
                                        "Ứng dụng chăm sóc sức khoẻ tại nhà",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  )),
                            ),
                            Form(
                                key: _formValidatePhoneKey,
                                autovalidateMode: _autoValidatePhone ? AutovalidateMode.always : AutovalidateMode.disabled,
                                child: Form(
                                    key: _formValidateCodeKey,
                                    autovalidateMode: _autoValidateCode ? AutovalidateMode.always : AutovalidateMode.disabled,
                                    child: Padding(
                                      padding: MediaQuery.of(context).viewInsets,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 40, right: 40, top: 30, bottom: 30),
                                        color: Colors.white,
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                "Xác nhận số điện thoại của bạn",
                                                style: TextStyle(
                                                    color: theme.primaryColor,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 22),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "Mã 6 ký tự sẽ gửi đến điện thoại của bạn.",
                                                style: TextStyle(
                                                    color: Colors.grey, fontSize: 16),
                                              ),
                                              SizedBox(
                                                height: 30,
                                              ),
                                              TextFormField(
                                                controller: _codeController,
                                                validator: _validatorCode,
                                                keyboardType: TextInputType.number,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                  EdgeInsets.fromLTRB(20, 10, 20, 10),
                                                  filled: true,
                                                  hintText: 'Mã số của bạn',
                                                  enabledBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(10.0)),
                                                    borderSide:
                                                    BorderSide(color: theme.primaryColor),
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(10.0)),
                                                    borderSide:
                                                    BorderSide(color: Colors.grey),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 30,
                                              ),
                                              GestureDetector(
                                                onTap: () async {
                                                  if (_formValidateCodeKey.currentState!
                                                      .validate()) {
                                                    _formValidateCodeKey.currentState!.save();
                                                    final code = _codeController.text.trim();
                                                    try {
                                                      AuthCredential credential =
                                                      PhoneAuthProvider.credential(
                                                          verificationId: verificationId,
                                                          smsCode: code);

                                                      var result = await _auth
                                                          .signInWithCredential(credential);

                                                      var user = result.user;

                                                      if (user != null) {
                                                        Navigator.pop(context,user);
                                                        //changePassword(context);
                                                      } else {
                                                        flush = Flushbar<bool>(
                                                            isDismissible: true,
                                                            backgroundColor: Colors.red,
                                                            message:
                                                            "Mã xác nhận sai. Vui lòng nhập lại.",
                                                            duration: Duration(seconds: 20),
                                                            mainButton: TextButton(
                                                              onPressed: () {
                                                                flush.dismiss(
                                                                    true); // result = true
                                                              },
                                                              child: Text(
                                                                "Đóng",
                                                              ),
                                                            ))
                                                          ..show(context);
                                                      }
                                                    } catch (e) {
                                                      flush = Flushbar<bool>(
                                                          isDismissible: true,
                                                          backgroundColor: Colors.red,
                                                          message:
                                                          "Mã xác nhận sai. Vui lòng nhập lại.",
                                                          duration: Duration(seconds: 20),
                                                          mainButton: TextButton(
                                                            onPressed: () {
                                                              flush.dismiss(
                                                                  true); // result = true
                                                            },
                                                            child: Text(
                                                              "Đóng",
                                                            ),
                                                          ))
                                                        ..show(context);
                                                    }
                                                  } else {
                                                    setState(() {
                                                      _autoValidateCode = true;
                                                    });
                                                  }
                                                },
                                                child: Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  padding: EdgeInsets.symmetric(vertical: 15),
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.all(
                                                          Radius.circular(5)),
                                                      boxShadow: <BoxShadow>[
                                                        BoxShadow(
                                                            color: Colors.grey.shade200,
                                                            offset: Offset(2, 4),
                                                            blurRadius: 5,
                                                            spreadRadius: 2)
                                                      ],
                                                      gradient: LinearGradient(
                                                          begin: Alignment.centerLeft,
                                                          end: Alignment.centerRight,
                                                          colors: [
                                                            theme.primaryColor,
                                                            theme.primaryColor
                                                                .withOpacity(.75)
                                                          ])),
                                                  child: Text(
                                                    'Xác nhận',
                                                    style: TextStyle(
                                                        fontSize: 18, color: Colors.white),
                                                  ),
                                                ),
                                              )
                                            ]),
                                      ),
                                    ))
                            )
                          ],
                        ),
                      )
                      ]),
                      Positioned(
                        child:
                        InkWell(
                          child:
                          Icon(Icons.clear,color: Colors.white,),
                          onTap: (){
                            Navigator.pop(context);
                          },
                        ),
                        top: 35,
                        left: 20,
                      ),
                    ],
                  ),
                ))
        )
    );
  }

  Widget _title() {
    return new Container(
      width: 80.0,
      height: 80.0,
      decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage(_logo), fit: BoxFit.fitWidth)),
    );
  }
}
