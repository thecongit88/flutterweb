import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';



class ImagePlayerScreen extends StatefulWidget {
  ImagePlayerScreen({Key? key, required this.imgLink}) : super(key: key);

  final String? imgLink;

  @override
  ImagePlayerScreenState createState() => ImagePlayerScreenState();
}

class ImagePlayerScreenState extends State<ImagePlayerScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(backgroundColor: Colors.black,),
          backgroundColor: Colors.black,
          body:
          Container(
            height: MediaQuery.of(context).size.height,
            alignment: Alignment.center,
            child:
            Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                child: Image.network(widget.imgLink ?? "",
                  errorBuilder: (context, exception, stackTrack) => Container(
                    height: 80.sp,
                    width: 80.sp,
                    child: const Icon(Icons.image,size: 120,color:  Colors.grey,),
                  ),
                  fit: BoxFit.fitWidth,
                )),
          ));
  }


}
