import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/posts/post_state.dart';
import 'package:hmhapp/widgets/posts/post_category_list.dart';
import 'package:hmhapp/widgets/posts/post_search_delegate.dart';
import 'package:hmhapp/widgets/posts/single_post_card.dart';
import 'package:hmhapp/widgets/posts/single_post_nocard.dart';
import 'package:provider/provider.dart';

class PostScreen extends StatefulWidget {
  @override
  _PostScreenState createState() => _PostScreenState();
}

var scrollCont =
    ScrollController(initialScrollOffset: 0.0, keepScrollOffset: true);

class _PostScreenState extends State<PostScreen> with TickerProviderStateMixin{
  late ScrollController _scrollController;

  late Animation<double> animation;
  late AnimationController controller;
  late PostState _postState;
  int _page = 1;
  int count = 0;

  @override
  void initState() {
    super.initState();
    _postState = PostState();
    _postState.setCategoryId(0);
    _postState.getListCategories();
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween<double>(begin: 0, end: 300).animate(controller);

    controller.forward();



    void _scrollListener() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent
      && _postState.page < _postState.totalPages
      ) {
        _postState.getAll(_postState.page + 1,_postState.categoryId);
      }
    }

    _scrollController = ScrollController()..addListener(_scrollListener);
    final ThemeData theme = Theme.of(context);

    return ChangeNotifierProvider(
        create: (_) => _postState,
        child:  Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                backgroundColor: theme.primaryColor,
                elevation: 0,
                leading:
              GestureDetector(
                child: const Icon(Icons.close),
                onTap: (){
                  Navigator.pop(context);
                },
              ),
                title: const Text(
                  "Tin tức",
                  style: TextStyle(color: Colors.white),
                ),
                actions: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: InkWell(
                      child: const Icon(
                        Icons.search,
                      ),
                      onTap: () {
                        showSearch(
                          context: context,
                          delegate: PostSearchDelegate(),
                        );
                      },
                    ),
                  )
                ],
              ),
              body:
              CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    SliverList(
                        delegate: SliverChildListDelegate([
                          Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child:
                              Consumer<PostState>(
                                  builder: (context, state, child) {

                                    return Column(
                                      children: <Widget>[
                                        PostCategoryList(state),
                                        bindList(state)
                                      ],
                                    );
                                  })
    )])
                        )
                  ])

        )
    );
  }

  Widget bindList(PostState state){

    switch (state.postAllResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            height: MediaQuery.of(context).size.height * 0.5,
            child:
            const Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.green)),
            ),
          );
      case Status.COMPLETED:
        return getList(state.postAll);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.postAllResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

  Widget getList(List<Post> posts) {
    return
      posts.isEmpty?
      Container(
        alignment: Alignment.center,
          height: MediaQuery.of(context).size.height * 0.5,
          child:
          const Text("Không có bài viết nào trong mục này.")
      ) :

      ListView.separated(
          separatorBuilder: (context, index) => const Divider(
            color: Colors.grey,
          ),
          shrinkWrap: true,
          physics:  const NeverScrollableScrollPhysics(),
          itemCount: posts.length,
          itemBuilder: (context, pos) {
            Post entry = posts[pos];
            return Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 0),
                child: pos == 0
                    ? SinglePostCard(
                  key: ValueKey(entry.id),
                  entry: entry,
                  showCategoryName: false,
                  showAuthor: true,
                  onTap: () {
                    return Navigator.of(context)
                        .pushNamed("/post",
                        arguments: entry);
                  },
                )
                    : SinglePostNoCard(
                  key: ValueKey(entry.id),
                  entry: entry,
                  showCategoryName: false,
                  showAuthor: true,
                  onTap: () {
                    return Navigator.of(context)
                        .pushNamed("/post",
                        arguments: entry);
                  },
                ));
          });;
  }

}

void scrollToTop() {
  scrollCont.animateTo(0.0,
      duration: const Duration(seconds: 1), curve: Curves.elasticInOut);
}
