import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/states/posts/post_detail_state.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';
import 'package:hmhapp/widgets/posts/post_related.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class PostDetailScreen extends StatefulWidget {
  static const String routeName = '/post';
  final Post entry;

  const PostDetailScreen({Key? key, required this.entry}) : super(key: key);

  @override
  PostDetailScreenState createState() => PostDetailScreenState();
}

class PostDetailScreenState extends State<PostDetailScreen> {
  late Post _entry;
  bool showAuthor = false;
  final PostDetailState _postDetailState = PostDetailState();


  @override
  void initState() {
    _entry = widget.entry;
    super.initState();
    _postDetailState.getPost(widget.entry.id!);
  }

  @override
  Widget build(BuildContext ctx) {

    return ChangeNotifierProvider(
        create: (_) => _postDetailState,
        child: Scaffold(
            backgroundColor: Colors.white,
            body:
            Consumer<PostDetailState>(builder: (context, state, child) {
              return getDetail(state,context);
            })
        ));
  }


  Widget getDetail(PostDetailState state,BuildContext ctx) {
    switch (state.postResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            const Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
      case Status.COMPLETED:
        return _getBody(ctx);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.postResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

  Widget _getBody(BuildContext ctx) {
    final ThemeData theme = Theme.of(context);

    var title = Text(_entry.title ?? "",
        style: const TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20.0,height: 1.5));

    return CustomScrollView(slivers: [
      SliverAppBar(
          floating: true,
          pinned: true,
          leading:
          GestureDetector(
            child: const Icon(Icons.close),
            onTap: (){
              Navigator.pop(context);
            },
          ),
          elevation: 0,
          backgroundColor: theme.primaryColor,
          title: Text(widget.entry.title ?? ""),
          actions: [
            Consumer<PostDetailState>(builder: (context, state, child) {
              return Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: IconButton(
                      onPressed: () {
                        /*final RenderObject box = context.findRenderObject()!;
                        return Share.share(
                            "${state.post.title ?? ""}. Xem thêm: ${state.post.Link ?? ""}",
                            subject: state.post!.description ?? "",
                            sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);*/
                      },
                      icon: const Icon(Icons.share)));
            })
          ]),
      SliverList(
          delegate: SliverChildListDelegate([
            Stack(children: [
              _cover(ctx),
              Container(
                height: 250,
                width: double.infinity,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topLeft,
                        colors: [Colors.black87, Colors.transparent])),
                alignment: Alignment.bottomLeft,
                child: Padding(padding: const EdgeInsets.all(10), child: title),
              ),
            ]),
            const SizedBox(height: 5),
            Markdown(
                shrinkWrap: true,
                selectable: true,
                physics:  const NeverScrollableScrollPhysics(),
                data: widget.entry.content ?? "",
                imageDirectory: EnpointUrl.ROOT_URL,
                onTapLink: (String text, String? href, String title) {

                  Fluttertoast.showToast(
                      msg: 'Vui lòng mở liên kết trên trang gốc của tin tức này.',
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 2,
                      backgroundColor: Colors.black,
                      textColor: Colors.white,
                      fontSize: 16.0);

                }
            ),
            /*Html(
                useRichText: true,
                data: widget.entry.content,
                padding: EdgeInsets.symmetric(horizontal: 10),
                defaultTextStyle: TextStyle(fontSize: 16,height: 1.5),
                linkStyle: const TextStyle(
                  color: Colors.green,
                ),

                onLinkTap: (url) {
                }),*/
            const Divider(),
            _publishDate(),
            const Divider(),
            Consumer<PostDetailState>(
              builder: (context, state, child) {
                return PostRelated(widget.entry.categoryId!,widget.entry.id!);
              },
            ),
          ]))
    ]);
  }

  Widget _cover(BuildContext ctx) {
    if (widget.entry.thumbnail!.isNotEmpty) {
      return CoverImageDecoration(
          url: widget.entry.thumbnail, height: 250.0, width: 250);
    } else {
      return Container(
        width: double.infinity,
        height: 250.0,
        color: Colors.green,
      );
    }
  }

  Widget _publishDate() {
    var dFormat = DateFormat('dd/MM/yyyy');
    String created = dFormat.format(DateTime.parse(widget.entry.created_at ?? "")).toString();

    return Padding(
        padding: const EdgeInsets.only(top: 5, left: 15),
        child: Text("Ngày đăng: $created",
            style: const TextStyle(color: Colors.black, fontSize: 15),
            textAlign: TextAlign.left));
  }
}
