import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/common_screens/videos/video_list_view.dart';
import 'package:hmhapp/common_screens/videos/video_search_delegate.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/video_list_view_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';


class CategoryVideosScreen extends StatefulWidget {
  int? videoCategoryId;
  CategoryVideosScreen({
     this.videoCategoryId
  });
  @override
  _CategoryVideosScreenState createState() => _CategoryVideosScreenState();
}

class _CategoryVideosScreenState extends State<CategoryVideosScreen> {
  VideoListViewState _videolistState = new VideoListViewState();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _videolistState.getList(widget.videoCategoryId);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MemberAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          title: Text("Video hướng dẫn"),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20),
              child:
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  showSearch(
                    context: context,
                    delegate: VideoSearchDelegate(),
                  );
                },
              )
            )
          ],
        ),
        backgroundColor: Colors.transparent,
        body: ChangeNotifierProvider(
          create: (_) => _videolistState,
          child: Consumer<VideoListViewState>(
            builder: (context, state, child) {
              return wdgMainListVideo(state.youtubeVideoResponseList)!;
            },
          ),
        )
      ),
    );
  }

  Widget? wdgMainListVideo(ApiResponse<List<YoutubeModel>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormVideList(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgFormVideList(List<YoutubeModel> data) {
    if (data.length == 0)
      return Center(
        child: Text("Chưa có dữ liệu videos!"),
      );

    return SingleChildScrollView(
      child: Container(
          child: data.length > 0 ?
          Column(
            children: <Widget>[
              VideoList(listData: data)
            ],
          ) :
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 50.0),
              Center(
                child: Text("Không có dữ liệu!", style: TextStyle(fontSize: 18.0, color: Colors.red)),
              )
            ],
          )
      )
    );
  }

}