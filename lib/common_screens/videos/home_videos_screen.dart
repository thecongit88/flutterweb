import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/videos/categories_videos_screen.dart';
import 'package:hmhapp/common_screens/videos/video_search_delegate.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/video_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/video_category_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:provider/provider.dart';


class HomeVideosScreen extends StatefulWidget {
  @override
  _HomeVideosScreenState createState() => _HomeVideosScreenState();
}

class _HomeVideosScreenState extends State<HomeVideosScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  VideoCategoryState videoCategoryState = VideoCategoryState();

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    videoCategoryState.getList();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => videoCategoryState,
        child: Scaffold(
          key: _scaffoldKey,
            appBar:
            AppBar(
              elevation: 0,
              backgroundColor: MemberAppTheme.nearlyBlue,
              title: const Text("Danh mục video hướng dẫn"),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child:
                  IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () {
                      showSearch(
                        context: context,
                        delegate: VideoSearchDelegate(),
                      );
                    },
                  )
                )
              ],
            ),
            backgroundColor: Colors.transparent,
            body: Container(
              color: MemberAppTheme.nearlyWhite,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Consumer<VideoCategoryState>(
                    builder: (context, state, child) {
                      switch (state.videoCategoryResponse.status) {
                        case Status.LOADING:
                          return
                            Container(
                              color: Colors.white30,
                              child:
                              const Center(
                                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(MemberAppTheme.nearlyBlue)),
                              ),
                            );
                          break;
                        case Status.COMPLETED:
                          return GridView(
                            shrinkWrap: true,
                            padding: const EdgeInsets.all(10),
                            physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 20.0,
                              crossAxisSpacing: 20.0,
                              childAspectRatio: 0.75,
                            ),
                            children: List<Widget>.generate(
                              state.videoCategoryResponse.data!.length,
                                  (int index) {
                                final int count = state.videoCategoryResponse.data!.length;
                                final Animation<double> animation =
                                Tween<double>(begin: 0.0, end: 1.0).animate(
                                  CurvedAnimation(
                                    parent: animationController,
                                    curve: Interval((1 / count) * index, 1.0,
                                        curve: Curves.fastOutSlowIn),
                                  ),
                                );
                                animationController.forward();
                                return VideoCategoryItem(
                                  post:  state.videoCategoryResponse.data![index],
                                  animation: animation,
                                  animationController: animationController,
                                );
                              },
                            ),
                          );
                        case Status.ERROR:
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            Flushbar(
                              message: state.videoCategoryResponse.message,
                              backgroundColor: Colors.green,
                              duration: const Duration(seconds: 2),
                              flushbarPosition: FlushbarPosition.TOP,
                            ).show(context);
                          });
                          return const SizedBox();
                        case Status.INIT:
                          return const SizedBox();
                        default:
                          return const SizedBox();
                      }

                    }),
              )
            )
          ),
        );
  }

}

class VideoCategoryItem extends StatelessWidget {
  const VideoCategoryItem(
      {Key? key,
        this.post,
        required this.animationController,
        this.animation,
        this.callback})
      : super(key: key);

  final VoidCallback? callback;
  final VideoCategory? post;
  final AnimationController animationController;
  final Animation<double>? animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation!,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation!.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => CategoryVideosScreen(videoCategoryId: post!.id),
                  ),
                );
              },
              child:  Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: HexColor('#F8FAFB'),
                  borderRadius: const BorderRadius.all(
                      Radius.circular(16.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                        child: AspectRatio(
                            aspectRatio: 1,
                            child:
                            post!.avatar!.isEmpty?
                            Image.asset( Constants.VIDEO_CATEGORY_IMAGE_DEFAULT,fit: BoxFit.fitWidth):
                            Image.network(post!.avatar!)),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top:15),
                            child: Text(
                              post!.name ?? "",
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 14.0,
                                letterSpacing: 0.27,
                                color: MemberAppTheme
                                    .darkerText,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
