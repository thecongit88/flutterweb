import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/common_screens/videos/video_list_view.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:flutter/rendering.dart';
import 'package:hmhapp/states/video_detail_state.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:provider/provider.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class VideoDetailSearch extends StatefulWidget {
  final YoutubeModel detail;

  VideoDetailSearch({Key? key, required this.detail}) : super(key: key);

  @override
  _VideoDetailSearchState createState() => _VideoDetailSearchState();
}

class _VideoDetailSearchState extends State<VideoDetailSearch> {
  late String youtubevideoId;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late YoutubePlayerController _controller;

  /*
  final List<String> _ids = [
    'nPt8bK2gbaU',
    'gQDByCdjUXw',
    'iLnmTe5Q2Qw',
    '_WoCV4c6XOE',
    'KmzdUe0RSJo',
    '6jZDSSZZxjQ',
    'p2lYr3vM_1w',
    '7QUtEmBT_-w',
    '34_PXCzGw1M',
  ];
   */
  late List<String> _ids;

  void initState() {
    super.initState();
    youtubevideoId = Functions.convertUrlToId(widget.detail.link!)!;
    _controller = YoutubePlayerController(
      initialVideoId: youtubevideoId,
      params: YoutubePlayerParams(
        startAt: const Duration(minutes: 0, seconds: 0),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: false,
      ),
    )..listen((value) {
      if (value.isReady && !value.hasPlayed) {
        _controller
          ..hidePauseOverlay()
          ..hideTopMenu();
      }
    });
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      Future.delayed(const Duration(seconds: 1), () {
        _controller.play();
      });
      Future.delayed(const Duration(seconds: 5), () {
        SystemChrome.setPreferredOrientations(DeviceOrientation.values);
      });
      log('Exited Fullscreen');
    };
  }

  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return YoutubePlayerControllerProvider(
      // Passing controller to widgets below.
      controller: _controller,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: MemberAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Video hướng dẫn"),
        ),
        body: LayoutBuilder(
          builder: (context, constraints) {
            /* comment here */
            /*
            if (constraints.maxWidth > 800) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Expanded(child: player),
                  const SizedBox(
                    width: 500,
                    child: SingleChildScrollView(
                      //child: Controls(),
                    ),
                  ),
                ],
              );
            }
            */
            return ListView(
              children: [
                player,
                _videoInfo(
                  widget.detail.title,
                  widget.detail.viewCount,
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget get _space => const SizedBox(height: 10);

  Widget _videoInfo(title, viewCount) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(title),
          subtitle: Text(viewCount),
          trailing: Icon(Icons.arrow_drop_down),
        ),
      ],
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }
}