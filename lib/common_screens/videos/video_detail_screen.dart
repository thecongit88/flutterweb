import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/common_screens/videos/video_list_view.dart';
import 'package:hmhapp/common_screens/videos/video_search_delegate.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:flutter/rendering.dart';
import 'package:hmhapp/states/video_detail_state.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:provider/provider.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class VideoDetail extends StatefulWidget {
  final YoutubeModel? detail;
  final List<YoutubeModel>? listVideosData;

  VideoDetail({Key? key, this.detail, this.listVideosData}) : super(key: key);

  @override
  _VideoDetailState createState() => _VideoDetailState();
}

class _VideoDetailState extends State<VideoDetail> {
  late String youtubevideoId;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  late List<YoutubeModel> _listVideoData;
  late YoutubePlayerController _controller;

  /*
  final List<String> _ids = [
    'nPt8bK2gbaU',
    'gQDByCdjUXw',
    'iLnmTe5Q2Qw',
    '_WoCV4c6XOE',
    'KmzdUe0RSJo',
    '6jZDSSZZxjQ',
    'p2lYr3vM_1w',
    '7QUtEmBT_-w',
    '34_PXCzGw1M',
  ];
   */
  late List<String> _ids;

  void initState() {
    super.initState();
    youtubevideoId = Functions.convertUrlToId(widget.detail!.link!)!;

    //get ids videos in a category
    _ids = <String>[];
    _listVideoData = widget.listVideosData ?? <YoutubeModel>[];

    if(_listVideoData != null && _listVideoData.length > 0) {
      for(int i=0; i<_listVideoData.length; i++){
        var id = Functions.convertUrlToId(_listVideoData[i].link!);
        if(id != null && id != "") {
          _ids.add(Functions.convertUrlToId(_listVideoData[i].link!)!);
        }
      }
    }

    _controller = YoutubePlayerController(
      initialVideoId: youtubevideoId,
      params: YoutubePlayerParams(
        startAt: const Duration(minutes: 0, seconds: 0),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: false,
      ),
    )..listen((value) {
      if (value.isReady && !value.hasPlayed) {
        _controller
          ..hidePauseOverlay()
          ..hideTopMenu();
      }
    });
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      Future.delayed(const Duration(seconds: 1), () {
        _controller.play();
      });
      Future.delayed(const Duration(seconds: 5), () {
        SystemChrome.setPreferredOrientations(DeviceOrientation.values);
      });
      log('Exited Fullscreen');
    };
  }

  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return YoutubePlayerControllerProvider(
      // Passing controller to widgets below.
      controller: _controller,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: MemberAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Video hướng dẫn"),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20),
                child: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    showSearch(
                      context: context,
                      delegate: VideoSearchDelegate(),
                    );
                  },
                )
            )
          ],
        ),
        body: LayoutBuilder(
          builder: (context, constraints) {
            /*
            if (constraints.maxWidth > 800) {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Expanded(child: player),
                  const SizedBox(
                    width: 500,
                    child: SingleChildScrollView(
                      //child: Controls(),
                    ),
                  ),
                ],
              );
            }
            */
            return ListView(
              children: [
                player,
                //const Controls(),
                _videoInfo(
                  _listVideoData[_ids.indexOf(_controller.metadata.videoId) == -1 ? 0 : _ids.indexOf(_controller.metadata.videoId)].title,
                  _listVideoData[_ids.indexOf(_controller.metadata.videoId) == -1 ? 0 : _ids.indexOf(_controller.metadata.videoId)].viewCount,
                ),
                _relatedVideos(widget.listVideosData, _listVideoData[_ids.indexOf(_controller.metadata.videoId) == -1 ? 0 : _ids.indexOf(_controller.metadata!.videoId!)].id!)
              ],
            );
          },
        ),
      ),
    );
  }

  Widget get _space => const SizedBox(height: 10);

  Widget _videoInfo(title, viewCount) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(title),
          subtitle: Text(viewCount),
          trailing: Icon(Icons.arrow_drop_down),
        ),
      ],
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  _relatedVideos(List<YoutubeModel>? listData, int currentVideoId) {
    List<YoutubeModel> relatedVideo = <YoutubeModel>[];
    //các video trong danh mục hiện tại
    listData = listData ?? <YoutubeModel>[];
    listData.forEach((video) {
      //compare with current video id
      if(video.id != currentVideoId) relatedVideo.add(video);
    });

    return ListView.separated(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            youtubevideoId = Functions.convertUrlToId(relatedVideo[index].link!)!;
            _controller.load(youtubevideoId);
            /*
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => VideoDetail(
                detail: listData[index],
                listVideosData: listData,
              ),
            ));
            */
          },
          child: _buildRelatedVideosForm(context, index, relatedVideo),
        );
      },
      separatorBuilder: (context, index) => Divider(
        height: 1.0,
        color: Colors.grey,
      ),
      itemCount: relatedVideo.length,
    );
  }

  _buildRelatedVideosForm(BuildContext context, int index, relatedVideo) {
    bool isMiniList = true;
    return Container(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: <Widget>[
          Container(
//          width: MediaQuery.of(context).size.width / 2,
            width: isMiniList
                ? MediaQuery.of(context).size.width / 2
                : 336.0 / 1.5,
            height: isMiniList ? 100.0 : 188.0 / 1.5,
            decoration: BoxDecoration(
              image: DecorationImage(
                //image: NetworkImage(listData[index].thumbnail),
                  image:  (relatedVideo[index].thumbnail.toString().isEmpty) ?
                  Image.asset("assets/images/youtube_default.jpg", fit: BoxFit.fitWidth).image :
                  Image.network(relatedVideo[index].thumbnail, fit: BoxFit.fitWidth).image,
                  fit: BoxFit.cover
              ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListTile(
                  contentPadding: const EdgeInsets.all(8.0),
                  dense: isMiniList ? true : false,
                  title: Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Text(relatedVideo[index].title),
                  ),
                  subtitle: !isMiniList
                      ? Text(
                      "${relatedVideo[index].viewCount} . ${relatedVideo[index].groupName}")
                      : Text(
                      "${relatedVideo[index].viewCount}"),
                  trailing: Container(
                      margin: const EdgeInsets.only(bottom: 30.0),
                      child: Icon(Icons.more_vert)),
                ),
                Container(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: !isMiniList
                      ? CircleAvatar(
                    backgroundImage:
                    NetworkImage(relatedVideo[index].channelAvatar),
                  )
                      : SizedBox(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}