import 'dart:convert';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/app_webadmin.dart';
import 'package:hmhapp/common_screens/forgotpass_screen.dart';
import 'package:hmhapp/common_screens/signup_hbcc_screen.dart';
import 'package:hmhapp/common_screens/signup_screen.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/models/media.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:provider/provider.dart';


class SignInScreen extends StatefulWidget {
  SignInScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  SignInState createState() => SignInState();
}

class SignInState extends State<SignInScreen> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();
  UserLogin user = UserLogin();
  final bool _autoValidate = false;

  final _userName = TextEditingController();
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _logo = "assets/icons/light/logo_white.png";
  String _userNameValue = "";
  String _passwordValue = "";
  String _phoneCode = "+84";

  final Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  const ManagerMainScreen() : MemberMainScreen();
  final Widget _defaultRegister = Constants.APP_MODULE == Constants.HBCC ?  SignUpHbccScreen() : SignUpScreen();
  late Widget _myApp;

  @override
  void initState() {
    super.initState();
    _myApp = Constants.APP_MODULE == Constants.HBCC ?  MyAppManager(defaultHome: _defaultHome,) :
    (Constants.APP_MODULE == Constants.NKT? MyAppMember(defaultHome: _defaultHome,): MyAppWebAdmin(defaultHome: _defaultHome,));
  }

  Widget _phoneField(String title, TextEditingController controller) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 130,
            child:
              ColoredBox(
                color: const Color(0xfff3f3f4),
                child:
                Padding(
                  padding: const EdgeInsets.only(left: 10,right: 0),
                  child:
                CountryCodePicker(
                  padding: EdgeInsets.zero,
                  showDropDownButton: true,
                  onChanged: (CountryCode countryCode) {
                    _phoneCode = countryCode.toString();
                  },// Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                  initialSelection: '+84',
                  favorite: const ['+84','+1','+1','+44','+61'],
                  // optional. Shows only country name and flag
                  showCountryOnly: false,
                  // optional. Shows only country name and flag when popup is closed.
                  showOnlyCountryWhenClosed: false,
                  // optional. aligns the flag and the Text left
                  alignLeft: false,
                ),
              ),
            )
          ),
          const SizedBox(width: 10),
          SizedBox(
            width: MediaQuery.of(context).size.width - 180,
            child:
            TextFormField(
              controller: controller,
              validator: _validatorTitle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: title,
                  border: InputBorder.none,
                  fillColor: const Color(0xfff3f3f4),
                  filled: true),
              onSaved: (String? val) {
                _userNameValue = val!.trim();
              },
              onChanged: (String val){

              },
            ),
          )
        ],
      ),
    );
  }


  Widget _titleField(String title, TextEditingController controller) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
              controller: controller,
              validator: _validatorTitle,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: title,
                  border: InputBorder.none,
                  fillColor: const Color(0xfff3f3f4),
                  filled: true),
            onSaved: (String? val) {
              _userNameValue = val!.trim();
          },
            onChanged: (String val){

            },
          )
        ],
      ),
    );
  }

  Widget _passwordField(String title, TextEditingController controller) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: controller,
            obscureText: true,
            validator: _validatorTitle,
            decoration: InputDecoration(
                hintText: title,
                border: InputBorder.none,
                fillColor: const Color(0xfff3f3f4),
                filled: true),
            onSaved: (String? val) {
              _passwordValue = val!.trim();
            },
          )
        ],
      ),
    );
  }

  Widget _titleScreen() {
    return const Padding(
        padding: EdgeInsets.only(bottom: 8),
        child: Text(
          'Đăng nhập',
          style: TextStyle(color: MemberAppTheme.nearlyBlack, fontSize: 20),
        ));
  }

  Widget _submitButton(AuthState state) {
    final ThemeData theme = Theme.of(context);
    return
      GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: const Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      theme.primaryColor.withOpacity(.75),
                      theme.primaryColor,])),
            child: const Text(
              'Đăng nhập',
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),),
          onTap: () {

        /*  if (_formKey.currentState!.validate()) {

         */
            _formKey.currentState!.save();
            print("abc");
            user.identifier = Functions.getStandardPhone(phone: _userNameValue, maQuocGia: _phoneCode);
            user.password = _passwordValue.trim();

            state.signIn(user);

          /*}else{
            setState(() {
              _autoValidate = true;
            });
            return;
          }*/
        }
      );
  }

  Widget _createAccountLabel() {
    final ThemeData theme = Theme.of(context);

  return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const Text(
            'Bạn chưa có tài khoản?',
            style: TextStyle(fontSize: 15),
          ),
          const SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => _defaultRegister));
            },
            child: Text(
              'Đăng ký',
              style: TextStyle(color: theme.primaryColor, fontSize: 15),
            ),
          )
        ],
      ),
    );
  }

  Widget _title() {
    return Container(
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(_logo), fit: BoxFit.fitWidth)),
    );
  }

  Widget _emailPasswordWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _phoneField("Số điện thoại", _email),
        _passwordField("Mật khẩu", _password),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return ChangeNotifierProvider(
        create: (_) => AuthState(),
        child:
        Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body:
            Form(
                key: _formKey,
                autovalidateMode: _autoValidate == true ? AutovalidateMode.always: AutovalidateMode.disabled,
                child:
                SingleChildScrollView(
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height / 2.5,
                              decoration: BoxDecoration(
                                  borderRadius:
                                  const BorderRadius.all(Radius.circular(5)),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey.shade200,
                                        offset: const Offset(2, 4),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ],
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        theme.primaryColor,
                                        theme.primaryColor.withOpacity(.75),
                                      ])),
                              child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      _title(),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      const Text(
                                        "Ứng dụng chăm sóc sức khoẻ tại nhà",
                                        style: TextStyle(color: Colors.white70),
                                      )
                                    ],
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                children: <Widget>[
                                  _titleScreen(),
                                  _emailPasswordWidget(),
                                  const SizedBox(
                                    height: 15,
                                  ),
                                Consumer<AuthState>(
                                builder: (context, state, child) {
                                  return _submitButton(state);
                                }),
                                  Container(
                                      padding: const EdgeInsets.only(top: 40),
                                      child: Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          InkWell(
                                            child: const Text('Quên mật khẩu?',
                                                style: TextStyle(fontSize: 15)),
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ForgotScreen()));
                                            },
                                          ),
                                          _createAccountLabel(),
                                        ],
                                      )),
                                  //const SizedBox(height: 50,),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      const Text("Sao chép token: "),
                                      InkWell(
                                        onTap: () async {
                                          FirebaseMessaging f = FirebaseMessaging.instance;
                                          String? token = await f.getToken();

                                          Clipboard.setData(ClipboardData(text: token));
                                          Fluttertoast.showToast(
                                              msg: "Đã sao chép",
                                              gravity: ToastGravity.CENTER
                                          );
                                        },
                                        child: const Icon(Icons.copy_rounded, size: 15, color: Colors.red,),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Consumer<AuthState>(
                            builder: (context, state, child) {
                              switch (state.userResponse.status) {
                                case Status.LOADING:
                                  return
                                    Container(
                                      color: Colors.white30,
                                      child:
                                      const Center(
                                        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
                                      ),
                                    );
                                case Status.COMPLETED:
                                  WidgetsBinding.instance.addPostFrameCallback((_) {
                                    Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                        (BuildContext context) => _myApp));
                                  });
                                  return const SizedBox();
                                case Status.ERROR:
                                  WidgetsBinding.instance.addPostFrameCallback((_) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                          backgroundColor: Colors.red,
                                          content: Text(_getMessage(state.userResponse.message)),
                                          duration: const Duration(seconds: 20),
                                          action: SnackBarAction(
                                            label: 'Đóng',textColor: Colors.white,
                                            onPressed: () {
                                              return ScaffoldMessenger.of(context)
                                                  .hideCurrentSnackBar();
                                            }
                                          ),
                                        )
                                    ).closed.then((SnackBarClosedReason reason) {
                                      state.setInit();
                                    });
                                  });
                                  return const SizedBox();
                                case Status.INIT:
                                  return const SizedBox();
                                default:
                                  return const SizedBox();
                              }
                            }),
                      ],
                    )))
        ));
  }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          const CircularProgressIndicator(),
          Container(margin: const EdgeInsets.only(left: 5), child: const Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  String _validatorTitle(String? value) {
    if (value == null || value.isEmpty) {
      return 'Tên đăng nhập không để trống.';
    }else if(value.length>50){
      return 'Chỉ nhập tối đa 50 ký tự';
    }
    return "";
  }

  String _getMessage(String? msg){
    var str = "";
    switch(msg){
      case "400":
        str = "Tài khoản đăng nhập hoặc mật khẩu sai.";
        break;
      default:
        str = msg??"";
    }
    return str;
  }

}

