import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  WelcomeState createState() => WelcomeState();
}

class WelcomeState extends State<WelcomeScreen> {


  @override
  Widget build(BuildContext context) {

    Widget _submitButton() {
      return InkWell(
        onTap: () {
          },
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 13),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: MemberAppTheme.nearlyWhite),
          child: Text(
            'Đăng nhập',
            style: TextStyle(fontSize: 18, color: MemberAppTheme.nearlyBlue),
          ),
        ),
      );
    }

    Widget _signUpButton() {
      return InkWell(
        onTap: () {
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 13),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: MemberAppTheme.nearlyBlue,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(color: MemberAppTheme.nearlyWhite, width: .5),
          ),
          child: Text(
            'Đăng ký ngay',
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      );
    }

    Widget _label() {
      return Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Text(
                'Sản phẩm được cung cấp bởi PHAD',
                style: TextStyle(color: Colors.white, fontSize: 17),
              ),
            ],
          ));
    }

    Widget _title() {
      return RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
            text: '',
            children: [
              TextSpan(
                text: 'CSSK',
                style: TextStyle(color: Colors.black, fontSize: 30),
              ),
              TextSpan(
                text: 'TAINHA',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
            ]),
      );
    }

    return Scaffold(
      body:SingleChildScrollView(
        child:Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [MemberAppTheme.nearlyBlue, Colors.lightBlue])),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            SizedBox(
              height: 100,
            ),
              _title(),
              SizedBox(
                height: 120,
              ),
              _submitButton(),
              SizedBox(
                height: 20,
              ),
              _signUpButton(),
              SizedBox(
                height: 20,
              ),
              _label()
            ],
          ),
        ),
      ),
    );
  }
}