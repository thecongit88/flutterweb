import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../common/app_colors.dart';
import '../../common/app_constant.dart';

class CameraScreen extends StatefulWidget {
  const CameraScreen({Key? key})
      : super(key: key);

  @override
  CameraScreenState createState() => CameraScreenState();
}

class CameraScreenState extends State<CameraScreen>
    with TickerProviderStateMixin {
  CameraController? controller;
  List cameras = [];
  int selectedCameraIdx = 0;
  String imagePath = "";
  PickedFile? imageFile;

  @override
  void initState(){
    _requestPermission();
    super.initState();

    var permission = Platform.isAndroid ? Permission.storage : Permission.photos;
    permission.status.then((status){
        if (status.isDenied) {
          permission.request().isGranted.then((value){
            if(value == true){

            }
          });
      }
    });
    availableCameras().then((availableCameras) {

      cameras = availableCameras;

      if (cameras.isNotEmpty) {
        setState(() {
          selectedCameraIdx = 0;
        });

        _initCameraController(cameras[selectedCameraIdx]).then((void v) {});
      }else{
        print("No camera available");
      }
    }).catchError((err) {
      print('Error: $err.code\nError Message: $err.message');
    });
  }

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller!.dispose();
    }

    controller = CameraController(cameraDescription, ResolutionPreset.high);

    // If the controller is updated then update the UI.
    controller!.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller!.value.hasError) {
        print('Camera error ${controller!.value.errorDescription}');
      }
    });

    try {
      await controller!.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses =
    Platform.isAndroid ?
    await [
      Permission.camera,
      Permission.storage,
      Permission.microphone,
    ].request():
    await [
      Permission.camera,
      Permission.photos,
      Permission.microphone
    ].request();

    final info = Platform.isAndroid? statuses[Permission.storage]:
    statuses[Permission.photos];
    print("store: ${info}");
    print("camera: ${statuses[Permission.camera]}");
    print("microphone: ${statuses[Permission.microphone]}");
    /*if (info == PermissionStatus.permanentlyDenied) {
      openAppSettings();
    }
    if (statuses[Permission.camera] == PermissionStatus.permanentlyDenied) {
      openAppSettings();
    }
    if (statuses[Permission.microphone] == PermissionStatus.permanentlyDenied) {
      openAppSettings();
    }*/
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          child: const Icon(Icons.arrow_back, color: Colors.black),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text(
          "Chụp ảnh", style: TextStyle(color: Colors.black),),
        actions: const [],
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 9,
              child: _cameraPreviewWidget(context),
            ),
            const SizedBox(height: 40.0),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  _cameraTogglesRowWidget(theme.primaryColor),
                  const Spacer(),
                  _captureControlRowWidget(context),
                  const Spacer(),
                  Padding(
                      padding: const EdgeInsets.only(right: 15),
                      child: InkWell(
                          child:
                              Row(
                                children: [
                                  Text("Chọn ảnh",style: TextStyle(color: AppColors.grey)),
                                  SizedBox(width: 5.sp,),
                                  Icon(Icons.image_outlined,size: 30.sp,color: theme.primaryColor,)
                                ],
                              ),
                          onTap: () async {
                            final pickedFile = await ImagePicker().getImage(
                              source: ImageSource.gallery ,
                            );

                            imageFile = pickedFile!;
                            String fileName = "";
                            if(imageFile!.path.trim() != "") {
                              var imageFileArr = imageFile!.path.trim().split("/");
                              fileName = imageFileArr.isNotEmpty ? imageFileArr[imageFileArr.length-1] : "";
                            }
                            print("Image gallery: $fileName");
                            var dataRet = {
                              "filePath": imageFile!.path,
                              "type": AppConstant.camera_gallery
                            };
                            Navigator.pop(context, dataRet);
                          }
                      )
                  )
                ],
              ),
            ),
            const SizedBox(height: 20.0),

          ],
        ),
      ),
    );
  }

  /// Display Camera preview.
  Widget _cameraPreviewWidget(BuildContext context) {

    if (controller == null || !controller!.value.isInitialized) {
      return const Text(
        'Loading',
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w900,
        ),
      );
    }

    var camera = controller!.value;
    // fetch screen size
    final size = MediaQuery.of(context).size;

    // calculate scale depending on screen and camera ratios
    // this is actually size.aspectRatio / (1 / camera.aspectRatio)
    // because camera preview size is received as landscape
    // but we're calculating for portrait orientation
    var scale = size.aspectRatio * camera.aspectRatio;

    // to prevent scaling down, invert the value
    if (scale < 1) scale = 1 / scale;

    return AspectRatio(
      aspectRatio: controller!.value.aspectRatio,
      child: CameraPreview(controller!),
    );
    return Transform.scale(
      scale: scale,
      child: Center(
        child: CameraPreview(controller!),
      ),
    );

  }

  /// Display the control bar with buttons to take pictures
  Widget _captureControlRowWidget(context) {

    final ThemeData theme = Theme.of(context);

    return Expanded(
      child: Align(
        alignment: Alignment.center,
        child:
        FloatingActionButton(
            backgroundColor:  theme.primaryColor,
            heroTag: "btn1",
            onPressed: () {
              _onCapturePressed(context);
            },
            child: const Icon(Icons.camera,color: Colors.white,)
        ),
      ),
    );
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget(Color primaryColor) {
    if (cameras.isEmpty) {
      return const Spacer();
    }

    CameraDescription selectedCamera = cameras[selectedCameraIdx];
    CameraLensDirection lensDirection = selectedCamera.lensDirection;

    return
      Padding(padding: EdgeInsets.only(left: 15.sp),
      child:
          TextButton.icon(
          onPressed: _onSwitchCamera,
          icon: Icon(_getCameraLensIcon(lensDirection),size: 30.sp, color: primaryColor),
          label: Text(selectedCameraIdx == 0 ? "Trước" :"Sau",style: TextStyle(color: AppColors.grey),)));
  }

  IconData _getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.cameraswitch_outlined;
      case CameraLensDirection.front:
        return Icons.cameraswitch_rounded;
      case CameraLensDirection.external:
        return Icons.cameraswitch;
      default:
        return Icons.device_unknown;
    }
  }

  void _onSwitchCamera() {
    selectedCameraIdx =
    selectedCameraIdx < cameras.length - 1 ? selectedCameraIdx + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIdx];
    _initCameraController(selectedCamera);
  }

  void _onCapturePressed(context) async {
    try {

      controller!.takePicture().then((XFile? file) {
        if (mounted) {
          var dataRet = {
            "filePath": file?.path,
            "type": AppConstant.camera_capture
          };
          Navigator.pop(context, dataRet);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error: ${e.code}\nError Message: ${e.description}';
    print(errorText);

    print('Error: ${e.code}\n${e.description}');
  }
}