import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/da_cam_services.dart';
import 'package:hmhapp/services/gender_services.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/services/personal_assistant_services.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/files/file_fields_upload.dart';
import '../../models/files/file_upload.dart';
import '../../services/file_services.dart';

class CameraState extends ChangeNotifier{

  String? urlImgAfterUploaded, urlProductImgAfterUploaded;

  //final IAppRepository repository = Get.find();
  final List<FileUploadSimple> files = <FileUploadSimple>[];
  late ApiResponse<List<FileUploadSimple>> listResponse;
  final FileServices fileServices = FileServices();

  CameraState() {
    listResponse = ApiResponse.init("");
  }


  uploadImage(FileFieldUpload fields) async {
    try {
      return  await fileServices.uploadImages(files,fields);
    }
    catch (e) {}
  }


  addFile(FileUploadSimple file){
    files.add(file);
    listResponse = ApiResponse.completed(files);
    notifyListeners();
  }

  removeFile(index){
    files.removeAt(index);
    listResponse = ApiResponse.completed(files);
    notifyListeners();
  }


  addSingleFile(FileUploadSimple file){
    files.clear;
    files.add(file);
    listResponse = ApiResponse.completed(files);
    notifyListeners();
  }


}