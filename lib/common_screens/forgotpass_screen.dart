import 'package:another_flushbar/flushbar.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/app_webadmin.dart';
import 'package:hmhapp/common_screens/change_pass_screen.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/common_screens/signup_hbcc_screen.dart';
import 'package:hmhapp/common_screens/signup_screen.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/common_screens/confirm_code_screen.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/functions.dart';
import 'package:provider/provider.dart';

class ForgotScreen extends StatefulWidget {
  ForgotScreen();

  @override
  ForgotScreenState createState() => ForgotScreenState();
}

class ForgotScreenState extends State<ForgotScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final _formValidateCodeKey = GlobalKey<FormState>();
  final _formValidatePhoneKey = GlobalKey<FormState>();
  final _userName = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordConfirmController = TextEditingController();
  final _logo = "assets/icons/light/logo_white.png";

  final _codeController = TextEditingController();
  AuthState _authState = new AuthState();
  UserChangePassword _userChangePassword = new UserChangePassword();
  late Flushbar flush;
  bool _autoValidateChangePass = false;
  bool _autoValidateCode = false;
  bool _autoValidatePhone = false;
  String _phoneCode = "+84";

  Widget _defaultHome = Constants.APP_MODULE == Constants.HBCC ?  ManagerMainScreen() : MemberMainScreen();
  late Widget _myApp;

  String labelText = "Gửi";

  @override
  void initState() {
    super.initState();
    _myApp = Constants.APP_MODULE == Constants.HBCC ?  MyAppManager(defaultHome: _defaultHome,) :
    MyAppMember(defaultHome: _defaultHome,);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    String _validatorTitle(String? value) {
      if (value == null || !(value.length > 5) || value.isEmpty) {
        return "Mật khẩu yêu cầu nhiều hơn 5 ký tự.";
      } else if (_passwordController.text != _passwordConfirmController.text) {
        return "Mật khẩu và Xác nhận mật khẩu không trùng nhau.";
      }
      return "";
    }

    String _validatorCode(String? value) {
      if (value == null || !(value.length == 6) || value.isEmpty) {
        return "Mã OTP yêu cầu 6 chữ số.";
      }
      return "";
    }

    String _validatorPhone(String? value) {
      if (value == null || !(value.length >= 9) || value.isEmpty) {
        return "Số điện thoại yêu cầu nhiều hơn 9 chữ số.";
      }
      return "";
    }

    void changePassword(contextMain) {
      showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          isDismissible: false,
          builder: (BuildContext bc) {
            return
              ChangeNotifierProvider(
                create: (_) => AuthState(),
                child: Form(
                    key: _formKey,
                    autovalidateMode: _autoValidateChangePass == true ? AutovalidateMode.always :
                    AutovalidateMode.disabled,
                    child:
                    Stack(
                        children: <Widget>[
                    Padding(
                      padding: MediaQuery.of(context).viewInsets,
                      child: Container(
                          padding: EdgeInsets.only(
                              left: 40, right: 40, top: 30, bottom: 30),
                          color: Colors.white,
                          child: Stack(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    "Mật khẩu mới",
                                    style: TextStyle(
                                        color: theme.primaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Nhập mật khẩu mới của bạn vào ô dưới.",
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  TextFormField(
                                    controller: _passwordController,
                                    validator: _validatorTitle,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(20, 10, 20, 10),
                                      filled: true,
                                      hintText: 'Mật khẩu của bạn',
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        borderSide: BorderSide(
                                            color: theme.primaryColor),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        borderSide:
                                            BorderSide(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(
                                    controller: _passwordConfirmController,
                                    obscureText: true,
                                    validator: _validatorTitle,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(20, 10, 20, 10),
                                      filled: true,
                                      hintText: 'Xác nhận mật khẩu của bạn',
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        borderSide: BorderSide(
                                            color: theme.primaryColor),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        borderSide:
                                            BorderSide(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Consumer<AuthState>(
                                      builder: (context, state, child) {
                                    return InkWell(
                                      child:
                                      Container(
                                          child: Text(
                                            'Cập nhật mật khẩu',
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.symmetric(
                                              vertical: 15),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5)),
                                              boxShadow: <BoxShadow>[
                                                BoxShadow(
                                                    color: Colors.grey.shade200,
                                                    offset: Offset(2, 4),
                                                    blurRadius: 5,
                                                    spreadRadius: 2)
                                              ],
                                              gradient: LinearGradient(
                                                  begin: Alignment.centerLeft,
                                                  end: Alignment.centerRight,
                                                  colors: [
                                                    theme.primaryColor,
                                                    theme.primaryColor
                                                        .withOpacity(.75)
                                                  ]))),
                                      onTap: () async {
                                        if (_formKey.currentState!.validate()) {

                                          var phoneNumber = Functions.getStandardPhone(phone: _userName.text, maQuocGia: _phoneCode);

                                          _formKey.currentState!.save();
                                          _userChangePassword.password =
                                              _passwordController.text.trim();
                                          _userChangePassword
                                                  .passwordConfirmation =
                                              _passwordController.text.trim();
                                          _userChangePassword.username = phoneNumber;
                                          try {
                                            state.changePassword(
                                                _userChangePassword);
                                          } catch (e) {
                                            flush = Flushbar<bool>(
                                                isDismissible: true,
                                                backgroundColor: Colors.red,
                                                message:
                                                    "Tài khoản hoặc mật khẩu bị sai.",
                                                duration: Duration(seconds: 20),
                                                mainButton: TextButton(
                                                  onPressed: () {
                                                    flush.dismiss(true);
                                                    state
                                                        .setChangePasswordInit();
                                                  },
                                                  child: Text(
                                                    "Đóng",
                                                  ),
                                                ))
                                              ..show(context);
                                          }
                                        } else {
                                          setState(() {
                                            _autoValidateChangePass = true;
                                          });
                                        }
                                      },
                                    );
                                  }),
                                ],
                              ),
                              Consumer<AuthState>(
                                  builder: (context, state, child) {
                                switch (state.changePassResponse.status) {
                                  case Status.LOADING:
                                    return Positioned(
                                        child: Container(
                                      color: Colors.white30,
                                      child: Center(
                                        child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                    Color>(Colors.green)),
                                      ),
                                    ));
                                    break;
                                  case Status.COMPLETED:
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) {

                                      Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                          (BuildContext context) => _myApp));
                                          Flushbar(
                                          backgroundColor: Colors.green,
                                          icon: Icon(Icons.check_circle, color: Colors.white,),
                                          message:
                                              "Mật khẩu cập nhật thành công.",
                                          duration: Duration(seconds: 2),)
                                        ..show(context);
                                    });
                                    return SizedBox(
                                      height: 0,
                                    );
                                    break;
                                  case Status.ERROR:
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) {
                                      flush = Flushbar(
                                          isDismissible: true,
                                          backgroundColor: Colors.red,
                                          message:
                                              "Tài khoản hoặc mật khẩu bị sai.",
                                          duration: Duration(seconds: 2),
                                          mainButton: TextButton(
                                            onPressed: () {
                                              state.setChangePasswordInit();
                                              flush.dismiss(
                                                  true); // result = true
                                            },
                                            child: Text(
                                              "Đóng",
                                            ),
                                          ))
                                        ..show(contextMain);
                                    });
                                    return SizedBox(
                                      height: 0,
                                    );
                                    break;
                                  case Status.INIT:
                                    return SizedBox(
                                      height: 0,
                                    );
                                    break;
                                  default:
                                    return SizedBox(
                                      height: 0,
                                    );
                                    break;
                                }
                              }),
                            ],
                          )),
                    ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child:
                            GestureDetector(
                              onTap: () async {
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.clear,color: theme.primaryColor,),
                            ),
                          )
                        ],
                    )));
          });
    }

    void codeAutoRetrievalTimeout(String verificationId){

    }

    Future<bool?> loginUser(String phone, BuildContext context) async {
      FirebaseAuth _auth = FirebaseAuth.instance;
      _auth.verifyPhoneNumber(
          phoneNumber: phone,
          timeout: Duration(seconds: 60),
          verificationCompleted: (AuthCredential credential) async {
            //Navigator.of(context).pop();
            var result = await _auth.signInWithCredential(credential);
            var user = result.user;
            if (user != null) {
              // Navigator.push(context, MaterialPageRoute(
              //   builder: (context) => Ma(user: user,)
              //));
            } else {
              flush = Flushbar<bool>(
                  isDismissible: true,
                  backgroundColor: Colors.red,
                  message: "Số điện thoại của bạn cung cấp không tồn tại.",
                  duration: Duration(seconds: 20),
                  mainButton: TextButton(
                    onPressed: () {
                      flush.dismiss(true); // result = true
                    },
                    child: Text(
                      "Đóng",
                    ),
                  ))
                ..show(context);
            }
          },
          verificationFailed: (exception) {
            /*
            Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => ConfirmCodeScreen(
                    verificationId: "verificationId",
                  ),
                )
            );
            return;
            */

            flush = Flushbar<bool>(
                isDismissible: true,
                backgroundColor: Colors.red,
                message:
                    "Xảy ra lỗi xác nhận số điện thoại của bạn. ${exception.message}",
                duration: Duration(seconds: 20),
                mainButton: TextButton(
                  onPressed: () {
                    flush.dismiss(true); // result = true
                  },
                  child: Text(
                    "Đóng",
                  ),
                ))
              ..show(context);
            print(exception);
          },
          codeSent: (String? verificationId, [int? forceResendingToken]) async {
            //redirect to other screen confirm phone code
/*
            var res = await  Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ConfirmCodeScreen(
                  verificationId: verificationId,
                ),
              )
            );

            if(res != null){
               res = await  Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ChangePassScreen(
                      userName: _userName.text.trim(),
                    ),
                  )
              );
               if(res != null && res == true ){
                 WidgetsBinding.instance
                     .addPostFrameCallback((_) {
                     Flushbar(
                       backgroundColor: Colors.green,
                       icon: Icon(Icons.check_circle, color: Colors.white,),
                       message:
                       "Mật khẩu cập nhật thành công.",
                       duration: Duration(seconds: 2),)
                       ..show(context).then((value){
                         Navigator.pushReplacement(context, MaterialPageRoute(builder:
                             (BuildContext context) => _myApp));
                       });
                 });
               }
            }


 */
            /* không dùng modal bottom sheet
 */

            showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                isDismissible: false,
                builder: (BuildContext bc) {
                  return
                    Form(
                      key: _formValidateCodeKey,
                      autovalidateMode: _autoValidateCode == true? AutovalidateMode.always : AutovalidateMode.disabled,
                      child:
                      Stack(
                        children: <Widget>[
                      Padding(
                        padding: MediaQuery.of(context).viewInsets,
                        child: Container(
                          padding: EdgeInsets.only(
                              left: 40, right: 40, top: 30, bottom: 30),
                          color: Colors.white,
                          child:
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  "Xác nhận số điện thoại của bạn",
                                  style: TextStyle(
                                      color: theme.primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Mã 6 ký tự sẽ gửi đến điện thoại của bạn.",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 16),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                TextFormField(
                                  controller: _codeController,
                                  validator: _validatorCode,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    filled: true,
                                    hintText: 'Mã số của bạn',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      borderSide:
                                          BorderSide(color: theme.primaryColor),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      borderSide:
                                          BorderSide(color: Colors.grey),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    if (_formValidateCodeKey.currentState!
                                        .validate()) {
                                      _formValidateCodeKey.currentState!.save();
                                      final code = _codeController.text.trim();
                                      try {
                                        AuthCredential credential =
                                            PhoneAuthProvider.credential(
                                                verificationId: verificationId ?? "",
                                                smsCode: code);
                                        if (credential != null) {
                                          changePassword(context);
                                        } else {
                                          flush = Flushbar<bool>(
                                              isDismissible: true,
                                              backgroundColor: Colors.red,
                                              message:
                                                  "Mã xác nhận sai. Vui lòng nhập lại. ${credential.toString()}",
                                              duration: Duration(seconds: 20),
                                              mainButton: TextButton(
                                                onPressed: () {
                                                  flush.dismiss(
                                                      true); // result = true
                                                },
                                                child: Text(
                                                  "Đóng",
                                                ),
                                              ))
                                            ..show(context);
                                        }
                                      } catch (e) {
                                        flush = Flushbar<bool>(
                                            isDismissible: true,
                                            backgroundColor: Colors.red,
                                            message:
                                                "Xảy ra lỗi trong quá trình xác nhận số điện thoại. Vui lòng nhập lại. ${e.toString()}",
                                            duration: Duration(seconds: 20),
                                            mainButton: TextButton(
                                              onPressed: () {
                                                flush.dismiss(
                                                    true); // result = true
                                              },
                                              child: Text(
                                                "Đóng",
                                              ),
                                            ))
                                          ..show(context);
                                      }
                                    } else {
                                      setState(() {
                                        _autoValidateCode = true;
                                      });
                                    }
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.symmetric(vertical: 15),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                              color: Colors.grey.shade200,
                                              offset: Offset(2, 4),
                                              blurRadius: 5,
                                              spreadRadius: 2)
                                        ],
                                        gradient: LinearGradient(
                                            begin: Alignment.centerLeft,
                                            end: Alignment.centerRight,
                                            colors: [
                                              theme.primaryColor,
                                              theme.primaryColor
                                                  .withOpacity(.75)
                                            ])),
                                    child: Text(
                                      'Xác nhận',
                                      style: TextStyle(
                                          fontSize: 18, color: Colors.white),
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                      ),
                          Positioned(
                            top: 10,
                            right: 10,
                            child:
                            GestureDetector(
                              onTap: () async {
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.clear,color: theme.primaryColor,),
                            ),
                          )
                        ],
                      ));
                }
            );
          },
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    }

    Widget _entryField(String title, TextEditingController controller,
        {bool isPassword = false}) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
                controller: controller,
                obscureText: isPassword,
                validator: _validatorPhone,
                keyboardType: isPassword
                    ? TextInputType.visiblePassword
                    : TextInputType.phone,
                decoration: InputDecoration(
                    hintText: title,
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true))
          ],
        ),
      );
    }


    Widget _phoneField(String title, TextEditingController controller) {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
                width: 130,
                child:
                ColoredBox(
                  color: Color(0xfff3f3f4),
                  child:
                  Padding(
                    padding: EdgeInsets.only(left: 10,right: 0),
                    child:
                    CountryCodePicker(
                      showDropDownButton: true,
                      onChanged: (CountryCode countryCode) {
                        _phoneCode = countryCode.toString();
                      },// Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: '+84',
                      favorite: ['+84','+1','+1','+44','+61'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      showOnlyCountryWhenClosed: false,
                      // optional. aligns the flag and the Text left
                      alignLeft: false,
                    ),
                  ),
                )
            ),
            SizedBox(width: 10),
            SizedBox(
              width: MediaQuery.of(context).size.width - 180,
              child:
              TextFormField(
                controller: controller,
                validator: _validatorTitle,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: title,
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
              ),
            )
          ],
        ),
      );
    }

    Widget _submitButton() {
      return GestureDetector(
          onTap: () {
            setState(() {
              labelText = "Đang xử lý";
            });
            if (_formValidatePhoneKey.currentState!.validate()) {
              _formValidatePhoneKey.currentState!.save();
              var phoneNumber = Functions.getStandardPhone(phone: _phoneCode, maQuocGia: _userName.text);
              loginUser(phoneNumber, context);
            } else {
              setState(() {
                _autoValidatePhone = true;
              });
              return;
            }
          },
          child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Colors.grey.shade200,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        theme.primaryColor,
                        theme.primaryColor.withOpacity(.75)
                      ])),
              child:
                  labelText == "Đang xử lý" ?
                  SizedBox(
                    width: 21,
                    height: 21,
                    child: CircularProgressIndicator(
                        valueColor:
                        new AlwaysStoppedAnimation<
                            Color>(Colors.white)
                    ),
                  )
                      :
                  Text(
                    'Gửi',
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  )));
    }

    /*
    Widget _confirmCode(String verificationId) {
      FirebaseAuth _auth = FirebaseAuth.instance;
      return Container(
        color: Colors.white,
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                "Xác nhận số điện thoại của bạn",
                style: TextStyle(
                    color: theme.primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Mã 6 ký tự sẽ gửi đến điện thoại của bạn.",
                style: TextStyle(
                    color: Colors.grey, fontSize: 16),
              ),
              SizedBox(
                height: 30,
              ),
              TextFormField(
                controller: _codeController,
                validator: _validatorCode,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding:
                  EdgeInsets.fromLTRB(20, 10, 20, 10),
                  filled: true,
                  hintText: 'Mã số của bạn',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                        Radius.circular(10.0)),
                    borderSide:
                    BorderSide(color: theme.primaryColor),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                        Radius.circular(10.0)),
                    borderSide:
                    BorderSide(color: Colors.grey),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: () async {
                  /*
                  if (_formValidateCodeKey.currentState
                      .validate()) {
                    _formValidateCodeKey.currentState.save();
                   */

                    final code = _codeController.text.trim();
                    if(code == "") {
                      flush = Flushbar<bool>(
                          isDismissible: true,
                          backgroundColor: Colors.red,
                          message:
                          "Vui lòng nhập mã xác nhận của bạn!",
                          duration: Duration(seconds: 20),
                          mainButton: TextButton(
                            onPressed: () {
                              flush.dismiss(
                                  true); // result = true
                            },
                            child: Text(
                              "Đóng",
                            ),
                          ))
                        ..show(context);
                      return;
                    }
                    try {
                      AuthCredential credential =
                      PhoneAuthProvider.getCredential(
                          verificationId: verificationId,
                          smsCode: code);

                      AuthResult result = await _auth
                          .signInWithCredential(credential);

                      FirebaseUser user = result.user;

                      if (user != null) {
                        setState(() {
                          isChangePass = true;
                        });
                        //Navigator.pop(context,user);
                        changePassword(context);
                      } else {
                        flush = Flushbar<bool>(
                            isDismissible: true,
                            backgroundColor: Colors.red,
                            message:
                            "Mã xác nhận sai. Vui lòng nhập lại.",
                            duration: Duration(seconds: 20),
                            mainButton: TextButton(
                              onPressed: () {
                                flush.dismiss(
                                    true); // result = true
                              },
                              child: Text(
                                "Đóng",
                              ),
                            ))
                          ..show(context);
                      }
                    } catch (e) {
                      flush = Flushbar<bool>(
                          isDismissible: true,
                          backgroundColor: Colors.red,
                          message:
                          "Mã xác nhận sai. Vui lòng nhập lại.",
                          duration: Duration(seconds: 20),
                          mainButton: TextButton(
                            onPressed: () {
                              flush.dismiss(
                                  true); // result = true
                            },
                            child: Text(
                              "Đóng",
                            ),
                          ))
                        ..show(context);
                    }
                  /*
                  } else {
                    setState(() {
                      _autoValidateCode = true;
                    });
                  }
                  */
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 15),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey.shade200,
                            offset: Offset(2, 4),
                            blurRadius: 5,
                            spreadRadius: 2)
                      ],
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            theme.primaryColor,
                            theme.primaryColor
                                .withOpacity(.75)
                          ])),
                  child: Text(
                    'Xác nhận',
                    style: TextStyle(
                        fontSize: 18, color: Colors.white),
                  ),
                ),
              )
            ]),
      );
    }
    */

    Widget _loginAccountLabel() {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Bạn muốn đăng nhập?',
              style: TextStyle(fontSize: 15),
            ),
            SizedBox(
              width: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SignInScreen()));
              },
              child: Text(
                'Đăng nhập',
                style: TextStyle(
                  color: theme.primaryColor,
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      );
    }

    Widget _title() {
      return new Container(
        width: 80.0,
        height: 80.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage(_logo), fit: BoxFit.fitWidth)),
      );
    }

    Widget _titleScreen() {
      return Padding(
          padding: EdgeInsets.only(bottom: 8),
          child: Text(
            'Lấy lại mật khẩu',
            style: TextStyle(color: Colors.black87, fontSize: 20),
          ));
    }

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Form(
            key: _formValidatePhoneKey,
            autovalidateMode: _autoValidatePhone == true?
            AutovalidateMode.always : AutovalidateMode.disabled,
            child: SingleChildScrollView(
                child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height / 2.5,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.grey.shade200,
                                    offset: Offset(2, 4),
                                    blurRadius: 5,
                                    spreadRadius: 2)
                              ],
                              gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    theme.primaryColor,
                                    theme.primaryColor.withOpacity(.75)
                                  ])),
                          child: Center(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              _title(),
                              Text(
                                "Ứng dụng chăm sóc sức khoẻ tại nhà",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          )),
                        ),
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: <Widget>[
                              _titleScreen(),
                              _phoneField("Số điện thoại", _userName),
                              SizedBox(
                                height: 10,
                              ),
                              _submitButton(),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: _loginAccountLabel(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ))));
  }
}
