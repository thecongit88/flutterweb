import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/route_generator.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/app_state.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:month_year_picker/month_year_picker.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MyAppManager extends StatefulWidget {
  final Widget? defaultHome;
  const MyAppManager({Key? key, this.defaultHome}) : super(key: key);

  @override
  _MyAppManagerState createState() => _MyAppManagerState();
}

class _MyAppManagerState extends State<MyAppManager> {

  AuthState authState = AuthState();

  @override
  void initState() {
    super.initState();
    authState.getUser();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      //statusBarBrightness: Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return
      ChangeNotifierProvider(
          create: (_) => authState,
      child:
      MaterialApp(
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          MonthYearPickerLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('vi'),
        ],
      title: 'HMH - Chăm sóc sức khoẻ',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
        primaryColor: ManagerAppTheme.nearlyBlue,
        primarySwatch: Colors.blue,
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
      home:
      Consumer<AuthState>(builder: (context, state, child) {
      return buildScreen(state,context);
      }),
      initialRoute: '/',
      builder:(context,child){
        return MediaQuery(data: MediaQuery.of(context).copyWith(textScaleFactor: 1), child: child!);
      },
      onGenerateRoute: RouteGenerator.generateRoute,
    ));

  }

  Widget buildScreen(AuthState state,BuildContext ctx) {
    switch (state.authResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            const Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        if(state.authResponse.data != null && state.authResponse.data!.isNotEmpty){
          return widget.defaultHome!;
        } else {
          return SignInScreen();
        }
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          Flushbar(
            message: state.authResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }
  }

}


