import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/app_manager.dart';
import 'package:hmhapp/app_member.dart';
import 'package:hmhapp/app_webadmin.dart';
import 'package:hmhapp/dp_managers/screens/manager_main_screen.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/dp_webadmins/home_admin.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/service_locator.dart';

import 'application.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget defaultHome = Constants.APP_MODULE == Constants.HBCC ?  const ManagerMainScreen():MemberMainScreen();
  runApp(
      ScreenUtilInit(
          designSize: Size(360, 690),
          builder: (context, _) {
            return NotificationManager(
              child: Constants.APP_MODULE == Constants.HBCC ?
              MyAppManager(defaultHome: defaultHome) : MyAppMember(
                defaultHome: defaultHome,),
            );
          }
      )
  );
}