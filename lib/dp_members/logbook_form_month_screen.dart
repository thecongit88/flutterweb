import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/form_item_logbook_state.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'design_course_app_theme.dart';

class LogBookFormMonthScreen extends StatefulWidget {
  final int? memberId;

  const LogBookFormMonthScreen({Key? key, this.memberId}) : super(key: key);

  @override
  _LogBookFormMonthScreenState createState() => _LogBookFormMonthScreenState();
}

class _LogBookFormMonthScreenState extends State<LogBookFormMonthScreen>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  bool clicked = false;

  FormTypeState formTypeState = new FormTypeState();
  MemberState _memberState = new MemberState();
  Map<String, dynamic> map = new Map<String, dynamic>();

  dynamic response;
  String formSurveyData = "";
  var formatStr = "dd-MM-yyyy";
  String _dateLabel = "Chọn ngày";
  FormItemLogBook? _itemLogBook;
  DateTime? _formDate;
  int? _formUser;
  FormSurveyState _formSurveyState = new FormSurveyState();
  ReportTanSuatNCSState _reportTanSuatNCSState = new ReportTanSuatNCSState();
  FormItemLogbookState _formItemLogbookState = new FormItemLogbookState();

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    //setData();
    formTypeState.getFormTypeNCS(isFormDay: false);
    _memberState.getMember();
    super.initState();
    _formDate = DateTime.now();
    _dateLabel = new DateFormat(formatStr).format(_formDate!).toString();
    _formItemLogbookState.isValidForCreateSurvey(
        widget.memberId, _formDate, "month");
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    String _date = "Chọn ngày";
    String _picked = "Không cải thiện - giữ nguyên như cũ";
    String _pickedVetloet = "Giữ nguyên như cũ";

    return Container(
      color: MemberAppTheme.nearlyWhite,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: MemberAppTheme.nearlyBlue,
            leading: InkWell(
              borderRadius:
                  BorderRadius.circular(AppBar().preferredSize.height),
              child: Icon(
                Icons.close,
                color: MemberAppTheme.nearlyWhite,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            title: Text("Đánh giá theo dõi cuối tháng"),
          ),
          backgroundColor: Colors.transparent,
          body: MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (_) => _memberState),
                ChangeNotifierProvider(create: (_) => formTypeState),
                ChangeNotifierProvider(create: (_) => _formItemLogbookState),
                ChangeNotifierProvider(create: (_) => _formSurveyState)
              ],
              child: SingleChildScrollView(
                child: Container(
                  color: HexColor("#F3F4F9"),
                  child: Stack(
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                            color: MemberAppTheme.nearlyWhite,
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(8.0),
                                topRight: Radius.circular(8.0)),
                          ),
                          margin: const EdgeInsets.only(top: 10),
                          child:
                          Consumer<MemberState>(
                              builder: (context, state, child) {
                                return wdgMemberSuccessCheckRender(
                                    state.memberResponse, context);
                              })),
                      Consumer<FormSurveyState>(
                          builder: (context, state, child) {
                        switch (state.formResponse.status) {
                          case Status.LOADING:
                            return new Positioned(
                                bottom: 300,
                                left: 0,
                                right: 0,
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      color: Colors.white30,
                                      child: Center(
                                        child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                        Color>(
                                                    MemberAppTheme.nearlyBlue)),
                                      ),
                                    )));
                            break;
                          case Status.COMPLETED:
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Flushbar(
                                message: 'Bạn đã thực hiện thành công đánh giá!',
                                backgroundColor: Colors.green,
                                duration: Duration(seconds: 2),
                                flushbarPosition: FlushbarPosition.TOP,
                                icon: Icon(
                                  Icons.check_circle,
                                  color: Colors.white,
                                ),
                              ).show(context).then((shouldUpdate) {
                                Navigator.pop(context);
                               // Navigator.of(context, rootNavigator: true).pop(true);
                              });
                            });

                            return SizedBox();
                            break;
                          case Status.ERROR:
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return planChange();
                                  });
                            });
                            return SizedBox();
                            break;
                          case Status.INIT:
                            return SizedBox();
                            break;
                          default:
                            return SizedBox();
                            break;
                        }
                      }),
                    ],
                  ),
                ),
              ))),
    );
  }

  planSuccess() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Flushbar(
        message: 'Bạn đã thực hiện thành công đánh giá!',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 50),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    });
    /*
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:

      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyBlue,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.card_giftcard,color: Colors.lightBlue, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Chúc mừng bạn, tháng này đã thực hiện đúng kế hoạch đề ra!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 10),
                  child:
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    icon: Icon(Icons.send,color: MemberAppTheme.nearlyWhite),
                    label: Text("Đóng", style: TextStyle(color: MemberAppTheme.nearlyWhite),),
                    color: MemberAppTheme.nearlyBlue,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );*/
  }

  Widget planChange() {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyYellow,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.info,
                    color: MemberAppTheme.nearlyYellow,
                    size: 50,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(
                    'Tháng này bạn thực hiện khác kế hoạch ban đầu đề ra!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, top: 10.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: new TextField(
                        maxLines: 3,
                        decoration: const InputDecoration(
                          hintText: "Lý do thay đổi của bạn là gì?",
                        )),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        child: ElevatedButton.icon(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(Icons.send,
                              color: MemberAppTheme.nearlyWhite),
                          label: Text(
                            "Gửi",
                            style: TextStyle(color: MemberAppTheme.nearlyWhite),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


  Widget wdgMemberSuccessCheckRender(ApiResponse<Member> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgMemberSuccessCheckContent(data.data!,context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgMemberSuccessCheckContent(Member member, BuildContext context) {
    return member.approved==null || member.approved == false
        ? Padding(
        padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
        child: Center(
          child: Text(
            'Tài khoản của bạn chưa được duyệt. Nên không có quyền thực hiên chức năng này!',
            style: TextStyle(color: Colors.red),
          ),
        ))
        :
    Consumer<FormItemLogbookState>(
        builder: (context, state, child) {
          return wdgFormItemCheckRender(
              state.formItemCheckResponse, context);
        });
  }

  Widget wdgFormItemCheckRender(ApiResponse<bool> data, context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormItemCheckContent(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgFormItemCheckContent(bool isValid, BuildContext context) {
    return !isValid
        ? Padding(
            padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
            child: Center(
              child: Text(
                'Báo lỗi, Bạn đã tạo đánh giá của tháng này!',
                style: TextStyle(color: Colors.red),
              ),
            ))
        : Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Text(
                            'Thời gian thực hiện:',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  )),
              Consumer<FormTypeState>(builder: (context, state, child) {
                return wdgFormTypeNCSRender(state.formTypeNCSResponse, context);
              }),
              AnimatedOpacity(
                duration: const Duration(milliseconds: 500),
                opacity: opacity3,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, bottom: 25, top: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 48,
                          decoration: BoxDecoration(
                            color: MemberAppTheme.nearlyBlue,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(16.0),
                            ),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: MemberAppTheme.nearlyBlue
                                      .withOpacity(0.5),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 10.0),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              'Hoàn thành',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                letterSpacing: 0.0,
                                color: MemberAppTheme.nearlyWhite,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          );
  }

  Widget wdgFormTypeNCSRender(ApiResponse<FormType> data, context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormTypeNCSContent(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgFormTypeNCSContent(FormType ft, BuildContext context) {
    Map data = ft.formFormat!;
    /*
    var fields = data["fields"];
    String value;
    (fields as List<dynamic>).forEach((item) {
      if(item["type"].toString() == "RadioButton"){
        if(value == "true" || value == "false") value = value == "true" ? "1" : "0";
        else value = value.toString();
      }
      item["value"] = value;
    });
    data["fields"] = fields;
    */

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                print('confirm $date');
                _formDate = date;
                setState(() {
                  _dateLabel =
                      new DateFormat(formatStr).format(_formDate!).toString();
                });
              }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            Text(
                              " $_dateLabel",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ),
        ),
        JsonSchema(
          formMap: data,
          onChanged: (dynamic response) {
            this.response = response;
            //print(response);
          },
          actionSave: (data) async {
            //kiểm tra mỗi tháng chỉ dc tạo đánh giá 1 lần
            //bool isValid = await _formItemLogbookState.isValidForCreateSurvey(
            //    _memberState.memberId, _formDate, "month");
            // if (isValid == false) {
            //   final snackBar = SnackBar(
            //       content: Text('Báo lỗi, Bạn đã tạo đánh giá của tháng này!'),
            //       backgroundColor: Colors.red);
            //   Scaffold.of(context).showSnackBar(snackBar);
            //   return;
            // }
            //-- hết kiểm tra mỗi tháng chỉ dc tạactiveo đánh giá 1 lần
            if (!clicked) {
              clicked = true;
              var fields = data["fields"];
              String objText = '{';
              fields.forEach((field) {
                objText += '"${field["key"]}":${field["value"].toString()},';
                map["${field["key"]}"] = field["value"].toString();
              });
              objText += '}';

              _formUser = _memberState.memberId;
              map["form_type"] = new FormType()..id = ft.id;
              map["member"] = _formUser;
              map["personal_assistant"] = _memberState.personalAssistantId;
              await _memberState
                  .getPersonalAssistant(map["personal_assistant"]);
              map["relationship_type"] = _memberState.relationshipTypeId;
              map["form_date"] = _formDate.toString();

              _itemLogBook = new FormItemLogBook();
              _itemLogBook!.name = ft.formName;
              _itemLogBook!.formDate = _formDate.toString();
              _itemLogBook!.memberId = _formUser;
              _itemLogBook!.formTypeId = ft.id;
              _itemLogBook!.formType = ft;
              _itemLogBook!.member = new Member(id: _formUser);
              _itemLogBook!.hbcc = new HBCC(id: 0);
              _itemLogBook!.is_ncs = true;
              try {
                _formSurveyState.create(map, _itemLogBook!, ft.formObject!);

                /* .. cập nhật thông tin cho bảng Form_ncs_logbook_days */
                await UpdateReportTanSuatNCS();

              } catch (ex) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  Flushbar(
                    message: 'Xảy ra lỗi trong quá trình xử lý.',
                    backgroundColor: Colors.red,
                    icon: Icon(
                      Icons.error,
                      color: Colors.white,
                    ),
                    duration: Duration(seconds: 2),
                    flushbarPosition: FlushbarPosition.TOP,
                  ).show(context).then((shouldUpdate) {
                    Navigator.pop(context);
                    //Navigator.of(context, rootNavigator: true).pop(true);
                  });
                });
              }
            } else {
              clicked = true;
            }
          },
          buttonSave: Padding(
              padding:
                  const EdgeInsets.only(left: 0, bottom: 25, top: 16, right: 0),
              child: InkWell(
                child: Center(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: ManagerAppTheme.nearLightBlue,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ManagerAppTheme.nearLightBlue
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color: ManagerAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )),
              )),
        )
      ],
    );
    return SizedBox();
  }

  Future<void> UpdateReportTanSuatNCS() async {
    await _reportTanSuatNCSState.getListReportTanSuatNCS(
        month: _formDate!.month,
        year: _formDate!.year,
        mId: _memberState!.memberId!);
    List<ReportTanSuatNCS> repTanSuatNCS = _reportTanSuatNCSState.repTanSuatNCS;
    var itemReportTanSuatNCS = new ReportTanSuatNCS();
    //cập nhật
    if (repTanSuatNCS.length > 0) {
      itemReportTanSuatNCS.id = repTanSuatNCS[0].id;
      itemReportTanSuatNCS.month = repTanSuatNCS[0].month;
      itemReportTanSuatNCS.year = repTanSuatNCS[0].year;
      itemReportTanSuatNCS.tansuat_thuchien =
          repTanSuatNCS[0].tansuat_thuchien! + 1;
      itemReportTanSuatNCS.tansuat_kehoach = repTanSuatNCS[0].tansuat_kehoach;
      itemReportTanSuatNCS.memberId = repTanSuatNCS[0].memberId;
      await _reportTanSuatNCSState.update(itemReportTanSuatNCS);
    } else {
      //thêm mới
      itemReportTanSuatNCS.month = _formDate!.month;
      itemReportTanSuatNCS.year = _formDate!.year;
      itemReportTanSuatNCS.tansuat_thuchien = 1;
      itemReportTanSuatNCS.tansuat_kehoach = 0;
      itemReportTanSuatNCS.memberId = _memberState.memberId;
      await _reportTanSuatNCSState.add(itemReportTanSuatNCS);
    }
  }
}
