import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/post.dart';
import 'package:hmhapp/main.dart';
import 'package:hmhapp/models/youtube.dart';

class PopularVideoListView extends StatefulWidget {
  final Function? callBack;
  final List<YoutubeModel>? listVideosData;
  PopularVideoListView({Key? key, this.listVideosData, this.callBack}) : super(key: key);

  @override
  _PopularVideoListViewState createState() => _PopularVideoListViewState();
}

class _PopularVideoListViewState extends State<PopularVideoListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  int maxVideoItems = 0;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    maxVideoItems = widget.listVideosData!.length;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return GridView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: List<Widget>.generate(
                maxVideoItems,
                (int index) {
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / maxVideoItems) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return VideoView(
                    callback: () {
                      widget.callBack!();
                    },
                    post: widget.listVideosData![index],
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 14.0,
                crossAxisSpacing: 15.0,
                childAspectRatio: 0.7,
              ),
            );
          }
        },
      ),
    );
  }
}

class VideoView extends StatelessWidget {
  const VideoView(
      {Key? key,
      this.post,
      this.animationController,
      this.animation,
      this.callback})
      : super(key: key);

  final VoidCallback? callback;
  final YoutubeModel? post;
  final AnimationController? animationController;
  final Animation<double>? animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController!,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation!,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation!.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                //callback();
                Navigator.of(context)
                    .pushNamed("/video_detail_search",
                    arguments: this.post
                );
              },
              child:  Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: ClipRRect(
                        borderRadius:
                        const BorderRadius.all(Radius.circular(16.0)),
                        child: AspectRatio(
                            aspectRatio: 1.28,
                            //child: Image.asset(post.imagePath)),
                            child: post!.thumbnail!.isEmpty ?
                            Image.asset("assets/images/youtube_default.jpg", fit: BoxFit.cover) :
                            Image.network(post!.thumbnail!, fit: BoxFit.cover)),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 16),
                              child: Text(
                                post!.title ?? "",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  letterSpacing: 0.27,
                                  color: MemberAppTheme
                                      .darkerText,
                                ),
                              ),
                            ),

                            /*
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8,),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment
                                    .spaceBetween,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '${post.createdDate}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w200,
                                      fontSize: 12,
                                      letterSpacing: 0.27,
                                      color: MemberAppTheme
                                          .grey,
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          '${post.rating}',
                                          textAlign:
                                          TextAlign.left,
                                          style: TextStyle(
                                            fontWeight:
                                            FontWeight.w200,
                                            fontSize: 14,
                                            letterSpacing: 0.27,
                                            color:
                                            MemberAppTheme
                                                .grey,
                                          ),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color:
                                          MemberAppTheme
                                              .nearlyBlue,
                                          size: 14,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            */

                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
