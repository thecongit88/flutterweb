import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/course_info_screen.dart';
import 'package:hmhapp/dp_members/lastest_video_list_view.dart';
import 'package:hmhapp/widgets/posts/post_list_view.dart';
import 'package:hmhapp/widgets/footer.dart';
import 'design_course_app_theme.dart';

class HomeNewsScreen extends StatefulWidget {
  @override
  _HomeNewsScreenState createState() => _HomeNewsScreenState();
}

class _HomeNewsScreenState extends State<HomeNewsScreen> {
  late AnimationController animationController;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MemberAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
        backgroundColor: MemberAppTheme.nearlyBlue,
        leading: InkWell(
          borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
          child: Icon(
            Icons.arrow_back_ios,
            color: MemberAppTheme.nearlyWhite,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Video hướng dẫn"),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20),
              child:
              Icon(Icons.search),
            )
          ],
      ),
        backgroundColor: Colors.transparent,
        body:
        Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      getLastestVideos(),
                      Footer()
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getCategoryUI() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 18.0, left: 18, right: 16),
          child:
          Row(
            children: <Widget>[
              const SizedBox(
                height: 16,
              ),
              Expanded(
                child: Text(
                  'Tin tức',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    color: MemberAppTheme.darkerText,
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(4.0)),
                onTap: () {},
                child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Tất cả",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                          letterSpacing: 0.5,
                          color: MemberAppTheme.nearlyBlack,
                        ),
                      ),
                      SizedBox(
                        height: 38,
                        width: 26,
                        child: Icon(
                          Icons.chevron_right,
                          color: MemberAppTheme.darkText,
                          size: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        PostListView(
          callBack: () {
            moveTo();
          },
        ),
      ],
    );
  }

  Widget getLastestVideos() {
    return LastestVideoListView(
      callBack: () {
        moveTo();
      },
    );
  }

  void moveTo() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => CourseInfoScreen(),
      ),
    );
  }

}
