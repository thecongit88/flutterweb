class PlansResponse {
  List<Plan> items = <Plan>[];

  PlansResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Plan.fromJson(item);
        items.add(it);
      });
    }
  }
}

class PlanResponse {
  Plan? plan;

  PlanResponse.fromJson(Map<String, dynamic> json) {
    plan = Plan.fromJson(json);
  }
}

class Plan {
  int? id;
  int? skillId; //skiil dropdown
  int? userId; //member dropdown
  int? relationship_types; //relationship_types dropdown
  int? tan_suat; //tan_suat dropdown
  String? relationship_other;
  String? thoigian_dukien;
  String? hoatdong_chamsoc;
  String? daura_mongmuon;
  String? note_other;
  String? tansuat_khac;
  String? title;
  bool? isPublic;
  bool? isPlaned;
  Map? formFormat;
  String? formObject;
  int? skillGroup; //skill category Id
  int? skillType; //skill type

  Plan({
    this.id = 0,
    this.title = '',
    this.skillId = 0,
    this.userId = 0,
    this.relationship_types = 0,
    this.tan_suat = 0,
    this.relationship_other = "",
    this.thoigian_dukien = "",
    this.hoatdong_chamsoc = '',
    this.daura_mongmuon = '',
    this.note_other = '',
    this.tansuat_khac = "0",
    this.isPublic = true,
    this.isPlaned = false,
    this.formFormat,
    this.formObject,
    this.skillGroup = 0,
    this.skillType = 0
  });

  Plan.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.title = parsedJson["skiil"] != null ? parsedJson["skiil"]["name"] : "No name";
    this.skillId = parsedJson["skiil"] != null ? parsedJson["skiil"]["id"] : 0;
    this.skillGroup = parsedJson["skiil"] != null ? parsedJson["skiil"]["skill_category"] : 0;
    this.skillType = parsedJson["skiil"] != null ? parsedJson["skiil"]["skill_type"] : 0;
    this.formFormat = parsedJson["skiil"] != null ? parsedJson["skiil"]["form_format"] : "";
    this.formObject = parsedJson["skiil"] != null ? refectFormObjectName(parsedJson["skiil"]["form_object"]) : "";
    this.userId = parsedJson["member"] != null ? parsedJson["member"]["id"] : 0;
    this.relationship_types = parsedJson["relationship_types"] != null ? parsedJson["relationship_types"]["id"] : 0;
    this.tan_suat = parsedJson["tan_suat"] != null ? parsedJson["tan_suat"]["id"] : 0;
    this.relationship_other = parsedJson["relationship_other"];
    this.thoigian_dukien = parsedJson["thoigian_dukien"];
    this.hoatdong_chamsoc = parsedJson["hoatdong_chamsoc"];
    this.daura_mongmuon = parsedJson["daura_mongmuon"];
    this.note_other = parsedJson["note_other"];
    this.tansuat_khac = parsedJson["tansuat_khac"].toString();
    this.isPublic = parsedJson["isPublic"];
    this.isPlaned = parsedJson["is_planed"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['skiil'] = this.skillId;
    data['member'] = this.userId;
    data['relationship_types'] = this.relationship_types;
    data['tan_suat'] = this.tan_suat;
    data['relationship_other'] = this.relationship_other;
    data['thoigian_dukien'] = this.thoigian_dukien;
    data['hoatdong_chamsoc'] = this.hoatdong_chamsoc;
    data['daura_mongmuon'] = this.daura_mongmuon;
    data['note_other'] = this.note_other;
    data['tansuat_khac'] = this.tansuat_khac;
    data['isPublic'] = this.isPublic;
    data['is_planed'] = this.isPlaned;
    return data;
  }


  static List<Plan> planList = <Plan>[
    Plan(
      title: 'Hướng dẫn TKT tự đi vệ sinh bằng bô',
      skillId: 1,
      userId: 1,
      isPublic: true,
    ),
    Plan(
      title: 'Hướng dẫn TKT tự đi vệ sinh bằng bô',
      skillId: 1,
      userId: 1,
      isPublic: true,
    ),
    Plan(
      title: 'Hướng dẫn TKT tự đi vệ sinh bằng bô',
      skillId: 1,
      userId: 1,
      isPublic: true,
    ),
    Plan(
      title: 'Hướng dẫn TKT tự đi vệ sinh bằng bô',
      skillId: 1,
      userId: 1,
      isPublic: true,
    ),

  ];
}


refectFormObjectName(String string) {
  if(string == null || string.isEmpty) return "";
  String _lastCharacter = string.length > 0 ? string.substring(string.length - 1) : "";
  if(_lastCharacter == "s") return string;
  return string+"s";
}


class PlanCheck {
  int? id;
  String? title;
  bool? isCheck;
  Map? formFormat;
  String? formObject;
}

class SkillProcesed {
  PlanCheck? planCheck;
  dynamic response;
}