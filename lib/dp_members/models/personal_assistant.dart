import 'dart:convert';

import 'package:hmhapp/models/edu_level.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/job_type.dart';
import 'package:hmhapp/models/marital_status.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/relationship_type.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/images_util.dart';

class PersonalAssistantResponse {
  List<PersonalAssistant> items = <PersonalAssistant>[];

  PersonalAssistantResponse.fromJson(List<dynamic> json) {
    for (var item in json) {
      var it = PersonalAssistant.fromJson(item);
      //if(it.idNguoiChamSoc == hbccId) items.add(it);
      items.add(it);
    }
  }
}

class PerResponse {
  PersonalAssistant? mem;
  PerResponse.fromJson(Map<String, dynamic> json) {
    mem = PersonalAssistant.fromJson(json);
  }
}

class PersonalAssistant {
  int? id;
  String? name;
  String? firstName;
  String? lastName;
  String? birth_date;
  bool? is_have_phone;
  bool? is_smartphone;
  bool? is_have_internet;
  int? BirthYear;
  String? gender;
  String? gender_other;
  String? nation;
  int? genderId;
  int? nationId;
  String? nation_other;
  String? edu_level;
  int? edu_level_id;
  String? marital_status;
  int? marital_status_id;
  String? marital_status_other;
  String? job_type;
  int? job_type_id;
  String? job_type_other;
  RelationshipType? relationship_type;
  int? relationship_type_id;
  String? relationship_type_other;
  List<FamilyRole>? family_roles;
  bool? vaitro_ncsc_chichamsoc_nkt;
  bool? vaitro_ncsc_laodong_taothunhap;
  bool? vaitro_ncsc_nguoics_cacthanhvien;

  PersonalAssistant({
    this.id = 0,
    this.name = '',
    this.firstName = '',
    this.lastName = '',
    this.birth_date = '',
    this.is_have_phone = false,
    this.is_smartphone = false,
    this.is_have_internet = false,
    this.BirthYear = 0,
    this.genderId = 0,
    this.nationId = 0,
    this.nation_other="",
    this.relationship_type_id = 0,
    this.relationship_type_other ="",
    this.marital_status_id = 0,
    this.marital_status_other = "",
    this.edu_level_id = 0,
    this.job_type_id = 0,
    this.job_type_other = "",
    this.gender = '',
    this.nation = '',
    this.edu_level = '',
    this.marital_status = '',
    this.job_type = '',
    this.relationship_type,
    this.vaitro_ncsc_chichamsoc_nkt =false,
    this.vaitro_ncsc_laodong_taothunhap = false,
    this.vaitro_ncsc_nguoics_cacthanhvien = false,
    this.gender_other = ''
  });

  PersonalAssistant.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = "${parsedJson["fist_name"].toString().trim()} ${parsedJson["last_name"].toString().trim()}";
    firstName = parsedJson["fist_name"].toString().trim();
    lastName = parsedJson["last_name"].toString().trim();
    birth_date = parsedJson["birth_date"];
    is_have_phone = parsedJson["is_have_phone"];
    is_smartphone = parsedJson["is_smartphone"];
    is_have_internet = parsedJson["is_have_internet"];
    BirthYear = parsedJson["BirthYear"] ?? 0;
    gender = parsedJson["gender"] != null ? Gender.fromJson(parsedJson["gender"]).getName() : "";
    nation = parsedJson["nation"] != null ? Nation.fromJson(parsedJson["nation"]).geTitle() : "";
    nation_other = parsedJson["nation_other"];
    genderId = parsedJson["gender"] != null ? Gender.fromJson(parsedJson["gender"]).getId() : 0;
    nationId = parsedJson["nation"] != null ? Nation.fromJson(parsedJson["nation"]).getId() : 0;
    edu_level = parsedJson["edu_level"] != null ? EduLevel.fromJson(parsedJson["edu_level"]).geTitle() : "";
    edu_level_id = parsedJson["edu_level"] != null ? EduLevel.fromJson(parsedJson["edu_level"]).getId() : 0;
    marital_status = parsedJson["marital_status"] != null ? MaritalStatus.fromJson(parsedJson["marital_status"]).geTitle() : "";
    marital_status_id = parsedJson["marital_status"] != null ? MaritalStatus.fromJson(parsedJson["marital_status"]).getId() : 0;
    marital_status_other = parsedJson["marital_status_other"];
    job_type = parsedJson["job_type"] != null ? JobType.fromJson(parsedJson["job_type"]).geTitle() : "";
    job_type_other = parsedJson["job_type_other"];
    job_type_id = parsedJson["job_type"] != null ? JobType.fromJson(parsedJson["job_type"]).getId() : 0;
    relationship_type = parsedJson["relationship_type"] != null ? RelationshipType.fromJson(parsedJson["relationship_type"]) : RelationshipType();
    relationship_type_other = parsedJson["relationship_type_other"];
    family_roles = parsedJson["family_roles"] != null ?
    FamilyRole.fromListJson(parsedJson["family_roles"]).items : <FamilyRole>[];
    vaitro_ncsc_chichamsoc_nkt = parsedJson["vaitro_ncsc_chichamsoc_nkt"];
    vaitro_ncsc_laodong_taothunhap = parsedJson["vaitro_ncsc_laodong_taothunhap"];
    vaitro_ncsc_nguoics_cacthanhvien = parsedJson["vaitro_ncsc_nguoics_cacthanhvien"];
    gender_other = parsedJson["gender_other"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['fist_name'] = firstName;
    data['last_name'] = lastName;
    data['birth_date'] = birth_date;
    data['is_have_phone'] = is_have_phone;
    data['is_smartphone'] = is_smartphone;
    data['is_have_internet'] = is_have_internet;
    data['BirthYear'] = BirthYear;
    data['gender'] = genderId;
    data['nation'] = nationId;
    data['nation_other'] = nation_other;
    data['edu_level'] = edu_level_id;
    data['job_type'] = job_type_id;
    data['job_type_other'] = job_type_other;
    data['marital_status'] = marital_status_id;
    data['marital_status_other'] = marital_status_other;
    data['relationship_type'] = relationship_type_id;
    data['relationship_type_other'] = relationship_type_other;
    data['family_roles'] = family_roles;
    data['vaitro_ncsc_chichamsoc_nkt'] = vaitro_ncsc_chichamsoc_nkt;
    data['vaitro_ncsc_laodong_taothunhap'] = vaitro_ncsc_laodong_taothunhap;
    data['vaitro_ncsc_nguoics_cacthanhvien'] = vaitro_ncsc_nguoics_cacthanhvien;
    data['gender_other'] = gender_other;

    return data;
  }
}

class FamilyRole {
  int? id;
  String? title;
  FamilyRole({
    this.id, this.title
  });

  List<FamilyRole> items = <FamilyRole>[];

  FamilyRole.fromListJson(List<dynamic> json) {
    for (var item in json) {
      var it = FamilyRole.fromJson(item);
      items.add(it);
    }
  }

  FamilyRole.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["title"] : "";
  }
}