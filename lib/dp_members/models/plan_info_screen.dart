import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/tan_suat.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/relationship_type_services.dart';
import 'package:hmhapp/services/tan_suat_services.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';

class PlanInfoScreen extends StatefulWidget {
  final Plan? plan;
  final int? memberId;

  const PlanInfoScreen({Key? key, this.callBack, this.plan, this.memberId}) : super(key: key);
  final Function? callBack;
  @override
  _PlanInfoScreenState createState() => _PlanInfoScreenState();
}

class _PlanInfoScreenState extends State<PlanInfoScreen>
    with TickerProviderStateMixin {
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  PlansState plansState = new PlansState();
  Plan planUpdate = new Plan();
  Plan? plan;

  final hoatDongChamSoc = TextEditingController();
  final thoiGianDuKien = TextEditingController();
  final dauRaMongMuon = TextEditingController();
  final luuYKhac = TextEditingController();
  var mqhKhac = TextEditingController();
  var tansuatKhac = TextEditingController();

  var arrTGDK = ["Sáng","Trưa","Tối"];
  List<DropdownMenuItem<int>> _listTanSuat = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn tần suất"),
  )];

  List<DropdownMenuItem<int>> _listMoiQuanHe = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn mối quan hệ"),
  )];

  List<DropdownMenuItem<String>> _listThoiGianDuKien = [
    new DropdownMenuItem<String>(
    value: "",
    child: new Text("Chọn thời gian dự kiến"),),
    new DropdownMenuItem<String>(
      value: "Sáng",
      child: new Text("Sáng"),),
    new DropdownMenuItem<String>(
      value: "Trưa",
      child: new Text("Trưa"),),
    new DropdownMenuItem<String>(
      value: "Tối",
      child: new Text("Tối"),)
  ];

  TanSuatServices tanSuatServices = new TanSuatServices();
  RelationshipTypeServices relationshipTypeServices = new RelationshipTypeServices();
  int? tanSuatSelected, mqhSelected;
  String? tgdkSelected;

  final _formKey = GlobalKey<FormState>();
  ReportTanSuatNCSState _reportTanSuatNCSState = new ReportTanSuatNCSState();


  Widget listTanSuatDropdown = SizedBox(height: 0,);
  Widget listMqhDropdown = SizedBox(height: 0,);

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    plan = widget.plan;
    tanSuatSelected = 0; mqhSelected = 0;
    tanSuatServices.listTanSuats().then((snapData) {
      snapData.forEach((_ts) {
        _listTanSuat.add(new DropdownMenuItem<int>(
          value: _ts.id,
          child: new Text(_ts.title ?? ""),
        ));
      });
      tanSuatSelected = plan!.tan_suat ?? 0;

      listTanSuatDropdown = DropdownButtonFormField<int>(
        isExpanded: true,
        isDense: true,
        items: _listTanSuat,
        value: tanSuatSelected,
        validator: (int? value) {
          if (value == null || value<= 0) {
            return 'Vui lòng chọn tần suất !';
          }else{
            return null;
          }
        },
        hint: Text('Chọn tần suất'),
        onChanged: (value) {
          setState(() {
            tanSuatSelected = value;
          });
        },
      );
    });

    tgdkSelected = plan!.thoigian_dukien ?? "";
    if(!arrTGDK.contains(tgdkSelected)){
      tgdkSelected = "";
    }
    relationshipTypeServices.listRelationshipType().then((snapData) {
      snapData.forEach((_rt) {
        _listMoiQuanHe.add(new DropdownMenuItem<int>(
          value: _rt.id,
          child: new Text(_rt.title ?? ""),
        ));
      });
      mqhSelected = plan!.relationship_types ?? 0;

      listMqhDropdown = DropdownButtonFormField<int>(
        isExpanded: true,
        isDense: true,
        items: _listMoiQuanHe,
        value: mqhSelected,
        validator: (int? value) {
          if (value == null || value<= 0) {
            return 'Vui lòng chọn mối quan hệ !';
          }else{
            return null;
          }
        },
        hint: Text('Chọn mối quan hệ'),
        onChanged: (value) {
          setState(() {
            mqhSelected = value;
          });
        },
      );
    });
    //set exists values
    hoatDongChamSoc.text = plan!.hoatdong_chamsoc.toString();
    //thoiGianDuKien.text = widget.plan.thoigian_dukien;
    dauRaMongMuon.text = plan!.daura_mongmuon ?? "";
    luuYKhac.text = plan!.note_other ?? "";
    mqhKhac.text = plan!.relationship_other?? "";
    tansuatKhac.text = plan!.tansuat_khac.toString();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _status = false;
    String _value = "1";
    String _ncscValue ="1";
    final FocusNode myFocusNode = FocusNode();
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;
    return Container(
      color: MemberAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius:
            BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: MemberAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Kế hoạch chăm sóc"),
        ),
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Container(
            color: HexColor("#F3F4F9"),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      top: 18.0, left: 18, right: 16),
                  child: Text(plan!.title ?? "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      letterSpacing: 0.27,
                      height: 1.25,
                      color: MemberAppTheme.darkerText,
                    ),
                  ),
                ),
                Container(
                    decoration: BoxDecoration(
                      color: MemberAppTheme.nearlyWhite,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(8.0),
                          topRight: Radius.circular(8.0)),

                    ),
                    margin: const EdgeInsets.only(top: 10),
                    padding: EdgeInsets.only(left: 25,right: 25),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 20,),
                          Text(
                            'Hoạt động chăm sóc:',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                            controller: hoatDongChamSoc,
                            decoration: const InputDecoration(
                              hintText: "Nhập mô tả",
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Vui lòng nhập hoạt động chăm sóc.';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 25,),
                          Text(
                            'Tần suất dự kiến thực hiện:',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                          /*
                          DropdownButtonFormField<int>(
                            isExpanded: true,
                            isDense: true,
                            items: _listTanSuat,
                            value: _listTanSuat.length > 0 ? plan.tan_suat : 0,
                            validator: (int value) {
                              if (value == null || value<= 0) {
                                return 'Vui lòng chọn tần suất !';
                              }else{
                                return null;
                              }
                            },
                            hint: Text('Chọn tần suất'),
                            onChanged: (value) {
                              setState(() {
                                tanSuatSelected = value;
                                print(tanSuatSelected);
                              });
                            },
                          ), */
                          listTanSuatDropdown,
                          SizedBox(height: 25,),
                          tanSuatSelected == 5 ? Text("Tần suất khác",
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),):SizedBox(),
                          tanSuatSelected == 5 ? TextFormField(
                            controller: tansuatKhac,
                            decoration: const InputDecoration(
                              hintText: "Tần suất khác",
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Vui lòng nhập tần suất khác.';
                              }
                              return null;
                            },
                          ):SizedBox(),
                          tanSuatSelected == 5 ? SizedBox(height: 25,):SizedBox(),
                          Text(
                            'Người CSC phụ trách chăm sóc NKT:',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),

                          /*
                          DropdownButtonFormField<int>(
                            isExpanded: true,
                            isDense: true,
                            items: _listMoiQuanHe,
                            value: mqhSelected,
                            validator: (int value) {
                              if (value == null || value<= 0) {
                                return 'Vui lòng chọn mối quan hệ !';
                              }else{
                                return null;
                              }
                            },
                            hint: Text('Chọn mối quan hệ'),
                            onChanged: (value) {
                              setState(() {
                                mqhSelected = value;
                              });
                            },
                          ),
                          */
                          listMqhDropdown,
                          SizedBox(height: 25,),
                          mqhSelected == 6 ? Text("Mối quan hệ khác",
                              style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),):SizedBox(),
                          mqhSelected == 6 ? TextFormField(
                            controller: mqhKhac,
                            decoration: const InputDecoration(
                              hintText: "Quan hệ khác",
                            ),
                            validator: (text) {
                              if (text == null || text.isEmpty) {
                                return 'Vui lòng nhập quan hệ khác.';
                              }
                              return null;
                            },
                          ):SizedBox(),
                          mqhSelected == 6 ? SizedBox(height: 25,):SizedBox(),
                          Text(
                            'Thời gian dự kiến thực hiện:',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                          /*
                          AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: opacity1,
                            child: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Wrap(
                                children: <Widget>[
                                  getTimeBoxUI('T2'),
                                  getTimeBoxUI('T3'),
                                  getTimeBoxUI('T4'),
                                  getTimeBoxUI('T5'),
                                  getTimeBoxUI('T6'),
                                  getTimeBoxUI('T7'),
                                  getTimeBoxUI('CN'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextFormField(
                                      controller: thoiGianDuKien,
                                      decoration: const InputDecoration(
                                        hintText: "Nhập mô tả",
                                      ),
                                      validator: (text) {
                                        if (text == null || text.isEmpty) {
                                          return 'Vui lòng nhập thời gian dự kiến !';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ],
                              )),
                          */
                          DropdownButtonFormField<String>(
                            isExpanded: true,
                            isDense: true,
                            items: _listThoiGianDuKien,
                            value: tgdkSelected,
                            validator: (String? value) {
                              if (value == null || value == "") {
                                return 'Vui lòng chọn thời gian dự kiến!';
                              }else{
                                return null;
                              }
                            },
                            hint: Text('Chọn thời gian dự kiến'),
                            onChanged: (value) {
                              setState(() {
                                tgdkSelected = value;
                              });
                            },
                          ),
                         /* Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Đầu ra mong muốn:',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextFormField(
                                        controller: dauRaMongMuon,
                                        maxLines: 3,
                                        decoration: const InputDecoration(
                                          hintText: "Nhập đầu ra mong muốn",
                                        )
                                    ),
                                  ),
                                ],
                              )),*/
                          SizedBox(height: 25,),
                          Text(
                            'Lưu ý khác (nếu có):',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold),
                          ),
                          TextFormField(
                              controller: luuYKhac,
                              maxLines: 3,
                              decoration: const InputDecoration(
                                hintText: "Nhập Lưu ý khác (nếu có)",
                              )
                          ),
                          AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: opacity3,
                            child: Padding(
                              padding: const EdgeInsets.only( bottom: 25, top:16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: GestureDetector(
                                        child: Container(
                                          height: 48,
                                          decoration: BoxDecoration(
                                            color: MemberAppTheme.nearlyBlue,
                                            borderRadius: const BorderRadius.all(
                                              Radius.circular(16.0),
                                            ),
                                            boxShadow: <BoxShadow>[
                                              BoxShadow(
                                                  color: MemberAppTheme
                                                      .nearlyBlue
                                                      .withOpacity(0.5),
                                                  offset: const Offset(1.1, 1.1),
                                                  blurRadius: 10.0),
                                            ],
                                          ),
                                          child: Center(
                                            child: Text(
                                              'Lưu kế hoạch',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18,
                                                letterSpacing: 0.0,
                                                color: MemberAppTheme
                                                    .nearlyWhite,
                                              ),
                                            ),
                                          ),
                                        ),

                                      onTap: () async {
                                        if (!_formKey.currentState!.validate()) {
                                          return;
                                        }

                                        planUpdate = new Plan();
                                        planUpdate.skillId = widget.plan!.skillId;
                                        planUpdate.userId = widget.plan!.userId;
                                        planUpdate.hoatdong_chamsoc = hoatDongChamSoc.text.trim();
                                        planUpdate.tan_suat = tanSuatSelected;
                                        planUpdate.relationship_types = mqhSelected;
                                        planUpdate.relationship_other = mqhKhac.text;
                                        planUpdate.thoigian_dukien = tgdkSelected;
                                        planUpdate.daura_mongmuon = dauRaMongMuon.text.trim();
                                        planUpdate.note_other = luuYKhac.text.trim();
                                        planUpdate.tansuat_khac = tansuatKhac.text.trim();
                                        planUpdate.isPlaned = true;
                                        plansState.updatePlan(planUpdate, widget.plan!.id!);

                                        final dataReturn = {"planId" : widget.plan!.id, "tansuatId": tanSuatSelected};
                                        Navigator.pop(context, dataReturn);

                                        return;
                                      },
                                    )
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getTimeBoxUI(String text1) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: MemberAppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: MemberAppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: MemberAppTheme.nearlyBlue,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
