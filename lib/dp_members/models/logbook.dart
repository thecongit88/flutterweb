class LogBook {
  LogBook({
    this.title = '',
    this.imagePath = '',
    this.createdDate = '',
    this.rating = 0.0,
    this.logBookType = 0,
  });

  String title;
  String createdDate;
  double rating;
  String imagePath;
  int logBookType;

  static List<LogBook> logBookLastestList = <LogBook>[
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát tháng 02/2020',
      createdDate: '28/02/2020',
      logBookType: 1,
      rating: 4.3,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 27/02/2020',
      createdDate: '27/02/2020',
      logBookType: 0,
      rating: 4.6,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 26/02/2020',
      createdDate: '26/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát ngày 25/02/2020',
      createdDate: '25/02/2020',
      rating: 4.3,
      logBookType: 0,
    ),

  ];

  static List<LogBook> logbookList = <LogBook>[
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát tháng 02/2020',
      createdDate: '28/02/2020',
      rating: 4.3,
      logBookType: 1,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 27/02/2020',
      createdDate: '27/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Khảo sát ngày 26/02/2020',
      createdDate: '26/02/2020',
      rating: 4.6,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Khảo sát ngày 25/02/2020',
      createdDate: '25/02/2020',
      rating: 4.3,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
    LogBook(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Khảo sát ngày 01/02/2020',
      createdDate: '01/02/2020',
      rating: 4.9,
      logBookType: 0,
    ),
  ];
}
