
class Post1 {
  Post1({
    this.title = '',
    this.imagePath = '',
    this.createdDate = '',
    this.rating = 0.0,
  });

  String title;
  String createdDate;
  double rating;
  String imagePath;

  static List<Post1> postList = <Post1>[
    Post1(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Hội thảo bàn về chăm sóc sức khoẻ cộng đồng',
      createdDate: '01/02/2020',
      rating: 4.3,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Các vấn đề thường gặp trong chăm sóc sức khoẻ Người khuyết tật',
      createdDate: '01/02/2020',
      rating: 4.6,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Một số lưu ý khi sử dụng phần mềm Chăm sóc sức khoẻ',
      createdDate: '01/02/2020',
      rating: 4.3,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace2.png',
      title: 'Những ghi nhận tại các gia đình NKT được chăm sóc tại dự án',
      createdDate: '01/02/2020',
      rating: 4.6,
    ),
  ];

  static List<Post1> popularVideoList = <Post1>[
    Post1(
      imagePath: 'assets/design_course/interFace3.png',
      title: 'Hoạt động của nhóm chuyên gia và các khoá đào tạo từ xa.',
      createdDate: '01/02/2020',
      rating: 4.8,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Một số phản hồi từ các thành viên tham gia dự án.',
      createdDate: '01/02/2020',
      rating: 4.9,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace3.png',
      title: 'Ứng dụng chăm sóc sức khoẻ tại nhà là gì?',
      createdDate: '01/02/2020',
      rating: 4.8,
    ),
    Post1(
      imagePath: 'assets/design_course/interFace4.png',
      title: 'Sự vào cuộc của cộng đồng và thông tin chăm sóc sức khoẻ.',
      createdDate: '01/02/2020',
      rating: 4.9,
    ),
  ];
}
