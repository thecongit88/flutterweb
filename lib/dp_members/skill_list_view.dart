import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/dp_members/models/skill.dart';
import 'package:hmhapp/dp_members/models/skill_groups.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/skill_services.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:hmhapp/states/skill_groups_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';

class SkillListView extends StatefulWidget {
  final List<Plan>? listPlans;

  const SkillListView({Key? key, this.callBack, this.listPlans}) : super(key: key);

  final Function? callBack;

  @override
  _SkillListViewState createState() => _SkillListViewState();
}

class _SkillListViewState extends State<SkillListView>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  SkillGroupsState _skillGroupsState = new SkillGroupsState();
  List<int> skillIdsChecked = <int>[];
  bool handleCheck = false;
  int i = 0;
  List<Plan> listPlansTmp = <Plan>[];
  List<Plan> listPlansOrg = <Plan>[];
  List<Skill> _lstSkillChecked = <Skill>[];
  List<int> _listCatSelected = <int>[];
  Skill? _skill;
  var _skillTitleStyle = TextStyle(fontSize: 16);

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
    _skillGroupsState.getListSkillGroups();
    listPlansTmp = <Plan>[];
    listPlansTmp = widget.listPlans!;
    for(i=0; i<listPlansTmp.length; i++) {
      if(skillIdsChecked.contains(listPlansTmp[i].skillId) == false) {
        skillIdsChecked.add(listPlansTmp[i].skillId!);
      }
    }

    /* params for enable/disable checkbox */
    _lstSkillChecked = <Skill>[];
    _listCatSelected = <int>[];;
    listPlansTmp.forEach((_plan) {
      _skill = new Skill();
      _skill!.id = _plan.skillId;
      _skill!.skillGroup = _plan.skillGroup;
      _skill!.skillType = _plan.skillType;
      _lstSkillChecked.add(_skill!);
      _listCatSelected.add(_plan.skillGroup!);
    });
    /* end params for enable/disable checkbox */
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          title: Text("Danh sách kế hoạch"),
          leading: InkWell(
              borderRadius:
              BorderRadius.circular(AppBar().preferredSize.height),
              child: Icon(
                Icons.close,
                color: MemberAppTheme.nearlyWhite,
              ),
              onTap: () {
                Navigator.pop(context);
              },
          ),

        ),
        body: ChangeNotifierProvider(
          create: (_) => _skillGroupsState,
          child: Consumer<SkillGroupsState>(
            builder: (context, state, child) {
              return wdgSkillGroupsRender(state.skillGroupResponseList);
            },
          ),
        )
        );
  }

  Widget wdgSkillGroupsRender(ApiResponse<List<SkillGroup>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgSkillGroupsContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgSkillGroupsContent(List<SkillGroup> listSkillGroups) {
    final ThemeData theme = Theme.of(context);
    final int count = listSkillGroups.length;
    if (count == 0)
      return Center(
        child: Text("Chưa có dữ liệu kế hoạch!"),
      );

    return
      SingleChildScrollView(// add a scrollview
        child:
        Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10,right: 10,top:20),
            child:
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                //var skillsList = listSkillGroups[index].skills;
                //skillsList.sort((a, b) => a.id.compareTo(b.id));
                return ExpansionTile(
                  title: Text(listSkillGroups[index].title ?? "",
                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                  key: PageStorageKey(listSkillGroups[index]),
                  children: listSkillGroups[index].skills!.map((skillObj){
                    Skill skill = Skill.fromJson(skillObj);
                    if(_lstSkillChecked.length == 0) {
                      return CheckboxListTile(
                        title: Text(skill.title ?? "", style: _skillTitleStyle),
                        value: skillIdsChecked.contains(skill.id),
                        onChanged: (bool? value) async {
                          if(value == true) {
                            setState(() {
                              if(skillIdsChecked.contains(skill.id.toString()) == false) {
                                skillIdsChecked.add(skill.id!);
                                listPlansTmp.add(new Plan(skillId: skill.id, title: skill.title, tan_suat: 0, isPlaned: false, skillGroup: listSkillGroups[index].id, skillType: skill.skillType));
                                _lstSkillChecked.add(skill);
                                _listCatSelected.add(skill.skillGroup!);
                              }
                            });
                          } else {
                            setState(() {
                              skillIdsChecked.remove(skill.id);
                              listPlansTmp.removeWhere((item) => item.skillId == skill.id);
                              if(_lstSkillChecked.length > 0) _lstSkillChecked.removeWhere((item) =>
                              item.id == skill.id
                              );
                              _listCatSelected.remove(listSkillGroups[index].id);
                            });
                          }
                        },
                      );
                    } else {
                      if(_listCatSelected.contains(listSkillGroups[index].id) == false) {
                        return
                          CheckboxListTile(
                            title: Text(skill.title ?? "", style: _skillTitleStyle),
                            value: skillIdsChecked.contains(skill.id),
                            onChanged: (bool? value) async {
                              if(value == true) {
                                setState(() {
                                  if(skillIdsChecked.contains(skill.id.toString()) == false) {
                                    skillIdsChecked.add(skill.id!);
                                    listPlansTmp.add(new Plan(skillId: skill.id, title: skill.title, tan_suat: 0, isPlaned: false, skillGroup: listSkillGroups[index].id, skillType: skill.skillType));
                                    _lstSkillChecked.add(skill);
                                    _listCatSelected.add(skill.skillGroup!);
                                  }
                                });
                              } else {
                                setState(() {
                                  skillIdsChecked.remove(skill.id);
                                  listPlansTmp.removeWhere((item) => item.skillId == skill.id);
                                  if(_lstSkillChecked.length > 0) _lstSkillChecked.removeWhere((item) =>
                                  item.id == skill.id
                                  );
                                  _listCatSelected.remove(listSkillGroups[index].id);
                                });
                              }
                            },
                          );
                      } else {
                        if(findSkill(_lstSkillChecked, skill) == true) {
                          return
                            CheckboxListTile(
                              title: Text(skill.title ?? "", style: _skillTitleStyle),
                              value: skillIdsChecked.contains(skill.id),
                              onChanged: (bool? value) async {
                                if (value == true) {
                                  setState(() {
                                    if (skillIdsChecked.contains(
                                        skill.id.toString()) == false) {
                                      skillIdsChecked.add(skill.id!);
                                      listPlansTmp.add(new Plan(skillId: skill.id,
                                          title: skill.title,
                                          tan_suat: 0,
                                          isPlaned: false,
                                          skillGroup: listSkillGroups[index].id,
                                          skillType: skill.skillType));
                                      _lstSkillChecked.add(skill);
                                      _listCatSelected.add(skill.skillGroup!);
                                    }
                                  });
                                } else {
                                  setState(() {
                                    skillIdsChecked.remove(skill.id);
                                    listPlansTmp.removeWhere((item) =>
                                    item.skillId == skill.id);
                                    if (_lstSkillChecked.length >
                                        0) _lstSkillChecked.removeWhere((item) =>
                                    item.id == skill.id
                                    );
                                    _listCatSelected.remove(
                                        listSkillGroups[index].id);
                                  });
                                }
                              },
                            );
                        } else {
                          return CheckboxListTile(
                            title: Text(skill.title ?? "",
                                style: TextStyle(fontSize: 16, color: Colors.grey)),
                            value: false,
                            activeColor: Colors.grey,
                            onChanged: (bool? value) => null,
                          );
                        }
                      }
                    }
                  }).toList(),
                );

              },
              itemCount: count,
            ),
          ),
          Center(
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 15, bottom: 25, top: 16, right: 15),
                  child:
                  InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                              color: theme.primaryColor,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: theme.primaryColor
                                        .withOpacity(0.5),
                                    offset: const Offset(1.1, 1.1),
                                    blurRadius: 10.0),
                              ],
                            ),
                            child: Center(
                              child: Text(
                                'Lưu kế hoạch',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  letterSpacing: 0.0,
                                  color:
                                  MemberAppTheme.nearlyWhite,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    onTap: (){
                      final dataReturn = {"skillIdsChecked" : skillIdsChecked, "listPlansChecked": listPlansTmp};
                      Navigator.pop(context, dataReturn);
                    },
                  )
              ),
            ),
        ],
    )
      );
  }

  bool findSkill(List<Skill> _lstSkillCheck, Skill skill) {
    bool found = false;
    int n = _lstSkillCheck.length;
    if(n > 0) {
      for(int i = 0; i < n; i++) {
        if (skill.skillGroup == _lstSkillCheck[i].skillGroup && skill.skillType == _lstSkillCheck[i].skillType) {
          found = true;
          break;
        }
      }
    }
    return found;
  }
}

class SkillChecked {
  int skillCategoryId;
  int skillId;
  int skillType;
  SkillChecked(this.skillCategoryId, this.skillId, this.skillType);
}
