import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/report_tinhtrang_nkt.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/states/report_tinhtrang_nkt_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:provider/provider.dart';

class ReportSkillScreen extends StatefulWidget {

  final int? memberId;
  const ReportSkillScreen({
    Key? key,
    this.memberId
  }) : super(key: key);

  @override
  _ReportSkillScreenState createState() => _ReportSkillScreenState();
}

class _ReportSkillScreenState extends State<ReportSkillScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  String? _year;
  String? _nam;
  Map<String, num>? _soLuong;
  int? _currentSelection;

  Map<int, Widget> _children = {
    0: Text('Báo cáo Tần suất'),
    1: Text('Báo cáo Tình trạng')
  };

  List<DropdownMenuItem<String>> selectYearsArr = <DropdownMenuItem<String>>[];
  int startYear = 2019;
  ReportTanSuatNCSState reportTanSuatNCSState = new ReportTanSuatNCSState();
  ReportTinhTrangNKTState reportTinhTrangNKTState = new ReportTinhTrangNKTState();

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    _year = DateTime.now().year.toString();
    _currentSelection = 0;
    super.initState();

    selectYearsArr = <DropdownMenuItem<String>>[];
    int vYear = int.parse(_year!);
    if(startYear == vYear)
      selectYearsArr!.add(DropdownMenuItem<String>(
        child: Text(startYear.toString()),
        value: startYear.toString(),
      ));
    else
      for(int i = startYear; i <= vYear; i++)
        selectYearsArr.add(DropdownMenuItem<String>(
          child: Text(i.toString()),
          value: i.toString(),
        ));

    reportTanSuatNCSState.getListReportTanSuatNCS(year: int.parse(_year!), mId: widget.memberId!);
    reportTinhTrangNKTState.getListReportTinhTrangNKT(year: int.parse(_year!), mId: widget.memberId!);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceOrientation = MediaQuery.of(context).orientation;

    List<Widget> _childWidgets = <Widget>[];
    _childWidgets.add(wdgBCTanSuat());
    _childWidgets.add(wdgBCTinhTrang());

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => reportTanSuatNCSState),
        ChangeNotifierProvider(create: (_) => reportTinhTrangNKTState),
      ],
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          title: Text("Báo cáo"),
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: MemberAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: double.infinity,
                      child: MaterialSegmentedControl(
                        children: _children,
                        verticalOffset: 10,
                        borderColor: Colors.lightBlue,
                        selectedColor: Colors.lightBlue,
                        unselectedColor: Colors.white,
                        selectionIndex: _currentSelection,
                        borderRadius: 20.0,
                        onSegmentChosen: (int? index) {
                          setState(() {
                            _currentSelection = index;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 20,),
                    new Container(child: _childWidgets![_currentSelection!]),
                  ],
                ))),
      ),
    );
  }

  Widget wdgBCTanSuat(){
    final children =
    SizedBox(
      child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text("Chọn năm báo cáo"),
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
              child: DropdownButton<String>(
                value: _year,
                items: selectYearsArr,
                isExpanded: false,
                onChanged: (String? value) {
                  setState(() {
                    _year = value;
                  });
                  reportTanSuatNCSState.getListReportTanSuatNCS(year: int.parse(_year!), mId: widget.memberId!);
                },
              ),
            ),
          ],
        ),
        Consumer<ReportTanSuatNCSState>(
            builder: (context, state, child) {
              return chartBCTanSuatRender(state.reporttansuatResponseList);
            }
        ),
        SizedBox(height: 10,),
        Text(
          "Báo cáo tần suất thực hiện năm $_year",
          textAlign: TextAlign.center,
        ),
      ],
    ),);
    return children;
  }
  Widget wdgBCTinhTrang(){
    final children =
        SizedBox(
        child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text("Chọn năm báo cáo"),
              Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 0),
                child: DropdownButton<String>(
                  value: _year,
                  items: selectYearsArr,
                  isExpanded: false,
                  onChanged: (String? value) {
                    setState(() {
                      _year = value;
                    });
                    reportTinhTrangNKTState.getListReportTinhTrangNKT(year: int.parse(_year!), mId: widget.memberId!);
                  },
                ),
              ),
            ],
          ),

          Consumer<ReportTinhTrangNKTState>(
              builder: (context, state, child) {
                return chartBCTinhTrangNKTRender(state.reportTinhTrangNKTResponseList);
              }
          ),

          SizedBox(height: 10,),
          Text(
            "Báo cáo tình trạng bệnh nhân năm $_year",
            textAlign: TextAlign.center,
          ),
        ]),);
    return children;
  }

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;

    String nam = "";
    final soluong = <String, num>{};

    // We get the model that updated with a list of [SeriesDatum] which is
    // simply a pair of series & datum.
    //
    // Walk the selection updating the measures map, storing off the sales and
    // series name for each selection point.
    if (selectedDatum.isNotEmpty) {
       nam = selectedDatum.first.datum.year;
      selectedDatum.forEach((charts.SeriesDatum datumPair) {
        soluong[datumPair.series.displayName!] = datumPair.datum.soLuong;
      });
    }

    // Request a build.
    setState(() {
      _nam = nam;
      _soLuong = soluong;
    });
  }

  static List<charts.Series<TanSuat, String>> _sampleData2020(List<ReportTanSuatNCS> data) {
    var vTH = <ReportTanSuatNCS>[];
    final List<TanSuat> dataDaThucHien = [];
    final List<TanSuat> dataKeHoach = [];
    for(int i=1; i<=12; i++){
      vTH = data.where((item) => item.month == i).toList();
      if(vTH.length > 0) {
        dataDaThucHien.add(new TanSuat('T$i', vTH[0].tansuat_thuchien!));
        dataKeHoach.add(new TanSuat('T$i', vTH[0].tansuat_kehoach!));
      } else {
        dataDaThucHien.add(new TanSuat('T$i', 0));
        dataKeHoach.add(new TanSuat('T$i', 0));
      }
    }

    return [
      new charts.Series<TanSuat, String>(
        id: 'Đã thực hiện',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
      new charts.Series<TanSuat, String>(
        id: 'Kế hoạch',
        domainFn: (TanSuat sales, _) => sales.year,
        measureFn: (TanSuat sales, _) => sales.soLuong,
        data: dataKeHoach,
      )
    ];
  }

  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2020(List<ReportTinhTrangNKT> data) {
    var vTH = <ReportTinhTrangNKT>[];
    final List<TinhTrang> dataDaThucHien = [];
    for(int i=1; i<=12; i++){
      vTH = data.where((item) => item.month == i).toList();
      if(vTH.length > 0) {
        dataDaThucHien.add(new TinhTrang(i, vTH[0].scores!));
      } else {
        dataDaThucHien.add(new TinhTrang(i, 0));
      }
    }

    return [
      new charts.Series<TinhTrang, int>(
        id: 'Đã thực hiện',
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
    ];
  }

  static List<charts.Series<TinhTrang, int>> _sampleDataTinhTrang2019() {
    final dataDaThucHien = [
      new TinhTrang(1, 3),
      new TinhTrang(2, 4),
      new TinhTrang(3, 5),
      new TinhTrang(4, 4),
      new TinhTrang(5, 3),
      new TinhTrang(6, 5),
      new TinhTrang(7, 6),
      new TinhTrang(8, 8),
      new TinhTrang(9, 6),
      new TinhTrang(10, 5),
      new TinhTrang(11, 6),
      new TinhTrang(12, 10),
    ];
    return [
      new charts.Series<TinhTrang, int>(
        id: 'Đã thực hiện',
        domainFn: (TinhTrang sales, _) => sales.year,
        measureFn: (TinhTrang sales, _) => sales.soLuong,
        data: dataDaThucHien,
      ),
    ];
  }

  Widget chartBCTanSuatRender(ApiResponse<List<ReportTanSuatNCS>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return chartBCTanSuatContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget chartBCTanSuatContent(List<ReportTanSuatNCS> data) {
    return SizedBox(
      height: 400.0,
      child: charts.BarChart(
        _sampleData2020(data),
        animate: false,
        barGroupingType: charts.BarGroupingType.grouped,
        behaviors: [new charts.SeriesLegend(
          showMeasures: true,
          measureFormatter: (num? value) {
            return value == null ? '-' : '${value}';
          },
        )],
      ),
    );
  }

  Widget chartBCTinhTrangNKTRender(ApiResponse<List<ReportTinhTrangNKT>> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return chartBCTinhTrangNKTContent(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget chartBCTinhTrangNKTContent(List<ReportTinhTrangNKT> data) {
    return SizedBox(
      height: 400.0,
      child: charts.LineChart(
          _sampleDataTinhTrang2020(data),
          animate: false,
          defaultRenderer: new charts.LineRendererConfig(
            includePoints: true,
          )
      ),
    );
  }

}

class TanSuat {
  final String year;
  final int soLuong;

  TanSuat(this.year, this.soLuong);
}

class TinhTrang {
  final int year;
  final int soLuong;

  TinhTrang(this.year, this.soLuong);
}