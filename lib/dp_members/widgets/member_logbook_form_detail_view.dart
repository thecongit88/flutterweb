import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_survey_services.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MemberLogBookFormDetailView extends StatefulWidget {
  final Member? member;
  final FormType? formType;
  final int? formSurveyId; // Id of record bang FormIte
  final int? formId; //Id of form chi tiet
  final int? memberId;

  const MemberLogBookFormDetailView({Key? key,this.formType, this.member, this.formSurveyId,this.formId, this.memberId}) : super(key: key);
  @override
  _MemberLogBookFormViewScreenState createState() => _MemberLogBookFormViewScreenState();
}

class _MemberLogBookFormViewScreenState extends State<MemberLogBookFormDetailView>
    with TickerProviderStateMixin {
  Map<String, dynamic> map = new Map<String, dynamic>();

  late FormSurveyState _formSurveyState;
  dynamic response;
  late Map forma ;
  bool clicked= false;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  Member? _member;
  List<Plan>? _planList;
  late DateTime _formDate;
  var formatStr =  "dd-MM-yyyy";
  int _formUser = 0;
  String _dateLabel = "Chọn ngày";

  String formSurveyData = "", memberName = "Chưa gán NKT";
  FormSurveyServices _servicesSurvey = new FormSurveyServices();
  PlansState plansState = new PlansState();

  DateTime? formSurveyDate;
  FormType? _formTypeObj;

  List<int> planIdsChecked = <int>[];
  var _arrPlanIds = [];

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _formSurveyState = new FormSurveyState();
    _formDate = DateTime.now();
    _formUser = widget.member!=null?widget.member!.id ?? 0: 0;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();

    formSurveyData = "";
    formSurveyDate = DateTime.now();
    super.initState();

    _formTypeObj = widget.formType;
    _servicesSurvey.getFormSurveyData(_formTypeObj!.formObject, widget.formId).then((value) {
      setState(() {
        formSurveyData = value; // lấy nội dung đã đánh giá từ 1 bảng dựa vào object trong bảng TormType (loại đánh giá)
        var jsonObject = json.decode(value);
        _member = Member.fromMap(jsonObject['member']);
        _formUser = _member!.id ?? 0;

        if(_formTypeObj!.formNCS == true && _formTypeObj!.formDay == true) {
          for(int i = 0; i < jsonObject["plans"].length; i++){
            _arrPlanIds.add(jsonObject["plans"][i]["id"]);
          }
        }

        if(jsonObject['form_date'] !=null && jsonObject['form_date'].toString().isNotEmpty) {
          _formDate = DateTime.parse(jsonObject['form_date'].toString());
          _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
        }
      });
    });
    plansState.getListPlans(mId: _formUser);
  }

  @override
  Widget build(BuildContext ctx) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => _formSurveyState),
          ChangeNotifierProvider(create: (_) => plansState)
        ],
        child:
    Stack(
        children: <Widget>[

          Padding(
              padding: const EdgeInsets.only(
                  left: 1, right: 1, top:15),
              child:
              _formTypeObj!.formFormat != null && _formTypeObj!.formFormat!.isNotEmpty ?
              wdgFormLogbook(_formTypeObj!.formFormat!) :
              Center(child: CircularProgressIndicator())
          ),
          Consumer<FormSurveyState>(
              builder: (context, state, child) {
                switch (state.formUpdateResponse.status) {
                  case Status.LOADING:
                    return
                      new Positioned(
                          bottom: 300,
                          left: 0,
                          right: 0,
                          child: Align(
                              alignment: Alignment.center,
                          child:Container(
                            color: Colors.white30,
                            child:
                            Center(
                              child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(MemberAppTheme.nearlyBlue)),
                            ),
                          )));
                    break;
                  case Status.COMPLETED:
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      Flushbar(
                        message: 'Bạn đã cập nhật thành công đánh giá!',
                        backgroundColor: Colors.green,
                        duration: Duration(seconds: 2),
                        flushbarPosition: FlushbarPosition.TOP,
                        icon: Icon(Icons.check_circle,color: Colors.white,),
                      ).show(context).then((shouldUpdate) {
                        Navigator.of(context).pop();
                      });
                      /*
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return planSuccess();
                                  }).then((shouldUpdate) {
                                Navigator.of(context).pop();
                              });*/
                    });
                    return SizedBox();
                    break;
                  case Status.ERROR:
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return planChange();
                          });
                    });
                    return SizedBox();
                    break;
                  case Status.INIT:
                    return SizedBox();
                    break;
                  default:
                    return SizedBox();
                    break;
                }
              }),
        ])
    );
  }

  Widget wdgFormLogbook(Map data){
    if(formSurveyData == "") {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if(data == null) {
      return Center(
        child: Text('Mẫu đánh giá chưa được cấu hình.',style: TextStyle(color: Colors.red),),
      );
    }

    var _fromSurveyedJson = json.decode(formSurveyData);
    var fields = data["fields"];
    String value;

    (fields as List<dynamic>).forEach((item) {
      //print(item["value"].toString());
      value = _fromSurveyedJson[item["key"]].toString();
      if(item["type"].toString() == "RadioButton"){
        if(value == "true" || value == "false") value = value == "true" ? "1" : "0";
        else value = value.toString();
      }
      item["value"] = value;
    });
    data["fields"] = fields;

    return new JsonSchema(
      formMap: data,
      onChanged: (dynamic response) {
        this.response = response;
        //print(response);
      },
      actionSave: (data) {
        //print(data);
        ///--> fixcode: chỉ được cập nhật các đánh giá hàng ngày trong tháng
        var now = DateTime.now();
        if(_formTypeObj!.formObject == "form-ncs-logbook-days" &&
            (_formDate.year != now.year || _formDate.month != now.month || _formDate.day > now.day)
        ) {
          Flushbar(
            message: 'Bạn chỉ được cập nhật đánh giá của tháng hiện tại!',
            backgroundColor: Colors.red,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
            icon: Icon(
              Icons.check_circle,
              color: Colors.white,
            ),
          ).show(context).then((shouldUpdate) {
            Navigator.pop(context);
          });
          return;
        }
        ///chỉ được cập nhật các đánh giá hàng ngày trong tháng <--


        if(!clicked) {
          clicked = true;
          var fields = data["fields"];
          String objText = '{';
          fields.forEach((field) {
            objText += '"${field["key"]}":${field["value"].toString()},';
            map["${field["key"]}"] = field["value"].toString();
          });
          objText += '}';

          //map["form_type"] = _formTypeObj.id;
          //map["member"] = _member.id;
          map["form_date"] = _formDate.toString();
          map["plans"] = _arrPlanIds;

          _formSurveyState.update(
              map, widget.formId!, _formTypeObj!.formObject!);
        }else{
          clicked = true;
        }
      },
      selectUsers: wdgNKT(),
      chooseDate: wdgThoiGian(),
      buttonSave: Container(
        child: Center(
          child: Padding(
              padding: const EdgeInsets.only(
                  left: 0, bottom: 25, top: 16, right: 0),
              child:
              InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: MemberAppTheme.nearlyBlue,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: MemberAppTheme
                                    .nearlyBlue
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color:
                              MemberAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
          ),
        ),
      ),

      //end container btnSave
    );
  }

  Widget wdgNKT() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 0.0, bottom: 15.0),
        child:
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Đánh giá tại nhà: ' + _member!.name!,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),);
  }

  Widget wdgThoiGian() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Thời gian thực hiện:',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                    print('confirm $date');
                    _formDate = date;
                    setState(() {
                      _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
                      map["form_date"] = _formDate.toString();
                    });
                  }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              '$_dateLabel',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _formTypeObj!.formNCS == true && _formTypeObj!.formDay == true ?
          Consumer<PlansState>(
              builder: (context, state, child) {
                return wdgPlanRender(state.plansResponseList, context);
              }
          ):
          SizedBox()
        ],
      ),
    );
  }

  Widget planSuccess(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:

      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyBlue,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.card_giftcard,color: Colors.lightBlue, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Bạn đã cập nhật thành công đánh giá!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 10),
                  child:
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.send,color: ManagerAppTheme.nearlyWhite),
                    label: Text("Đóng", style: TextStyle(color: ManagerAppTheme.nearlyWhite),)
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget planChange(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyYellow,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.info,color: MemberAppTheme.nearlyYellow, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Cập nhật đánh giá lỗi! Vui lòng thực hiện lại.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget wdgPlanRender(ApiResponse<List<Plan>> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgPlanForm(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgPlanForm(List<Plan> listPlans, context) {
    //List<String> list = new List<String>();
    //list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    //list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    //List<UserSkill> list = UserSkill.userSkillList;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 25.0, bottom: 15.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    'Kế hoạch chăm sóc',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              new Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: Divider(
                    color: Colors.lightBlue,
                    height: 1,
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 15.0),
          child: ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Divider(
                color: MemberAppTheme.nearlyWhite,
              ),
              itemCount: listPlans.length,
              itemBuilder: (context, index) {
                //xóa những cái bỏ tick
                Plan plan = listPlans[index];
                return CheckboxListTile(
                    title: Text(plan.title ?? "", style: TextStyle(height: 1.5)),
                    value: _arrPlanIds.contains(plan.id),
                    onChanged: (bool? value) {
                      setState(() {
                        if(value == true) {
                          setState(() {
                            if(_arrPlanIds.contains(plan.id.toString()) == false) {
                              _arrPlanIds.add(plan.id);
                              //listPlansTmp.add(new Plan(id: plan.id, skillId: plan.id, title: plan.title, isPlaned: false));
                            }
                          });
                        } else {
                          setState(() {
                            _arrPlanIds.remove(plan.id);
                            //listPlansTmp.removeWhere((item) => item.id == plan.id);
                          });
                        }
                      });
                    }
                );
              }
          ),
        ),
      ],
    );
  }
}

