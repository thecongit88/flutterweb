import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/dp_members/models/skill.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/states/form_item_logbook_state.dart';
import 'package:hmhapp/states/form_type_state.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'design_course_app_theme.dart';

class LogBookFormScreen extends StatefulWidget {
  final int? memberId;
  final String? selectedDate;
  final bool? dacodanhgia;
  const LogBookFormScreen({
    Key? key,
    this.memberId,
    this.selectedDate,
    this.dacodanhgia
  }) : super(key: key);

  @override
  _LogBookFormScreenState createState() => _LogBookFormScreenState();
}

class _LogBookFormScreenState extends State<LogBookFormScreen>
    with TickerProviderStateMixin {
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  bool clicked = false;

  FormTypeState _formTypeState = new FormTypeState();
  MemberState _memberState = new MemberState();
  PlansState _plansState = new PlansState();
  FormSurveyState _formSurveyState = new FormSurveyState();
  ReportTanSuatNCSState _reportTanSuatNCSState = new ReportTanSuatNCSState();
  FormItemLogbookState _formItemLogbookState = new FormItemLogbookState();

  Map<String, dynamic> map = new Map<String, dynamic>();
  dynamic response;
  String formSurveyData = "";
  var formatStr =  "dd-MM-yyyy";
  String _dateLabel = "Chọn ngày";
  FormItemLogBook? _itemLogBook;
  late DateTime _formDate;
  int? _formUser;
  List<Plan>? listPlans;
  List<int> planIdsChecked = <int>[];
  late FormItemLogbookServices _formItemLogbookServices;
  bool? dacodanhgia;
  String? selectedDate;
  late DateTime now;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    //setData();
    super.initState();

    now = DateTime.now();
    dacodanhgia = widget.dacodanhgia;
    selectedDate = widget.selectedDate;

    _formTypeState.getFormTypeNCS(isFormDay: true);
    _memberState.getMember();
    _plansState.getListPlans(mId: widget.memberId!);

    _formDate = now;
    _dateLabel = new DateFormat(formatStr).format(_formDate).toString();

    if(selectedDate != null && selectedDate != "") {
      _formDate = DateTime.parse(selectedDate!);
      _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
    }

    _formItemLogbookState.isValidForCreateSurvey(
        widget.memberId, _formDate, "day");

    _formItemLogbookServices = new FormItemLogbookServices();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _status = false;
    String _value = "1";
    String _ncscValue = "1";

    //print("Da co danh gia: ${dacodanhgia}");

    final FocusNode myFocusNode = FocusNode();
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;

    return Container(
      color: MemberAppTheme.nearlyWhite,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: MemberAppTheme.nearlyBlue,
          leading: InkWell(
            borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
            child: Icon(
              Icons.close,
              color: MemberAppTheme.nearlyWhite,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Đánh giá theo dõi hàng ngày"),
        ),
        backgroundColor: Colors.transparent,
        body: MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => _memberState),
            ChangeNotifierProvider(create: (_) => _formTypeState),
            ChangeNotifierProvider(create: (_) => _plansState),
            ChangeNotifierProvider(create: (_) => _reportTanSuatNCSState),
            ChangeNotifierProvider(create: (_) => _formItemLogbookState),
            ChangeNotifierProvider(create: (_) => _formSurveyState)
          ],
          child: SingleChildScrollView(
            child: Container(
              color: HexColor("#F3F4F9"),
              child: Stack(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        color: MemberAppTheme.nearlyWhite,
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            topRight: Radius.circular(8.0)),
                      ),
                      margin: const EdgeInsets.only(top: 10),
                      child:
                      Consumer<MemberState>(
                          builder: (context, state, child) {
                            return wdgMemberSuccessCheckRender(
                                state.memberResponse, context);
                          })),
                  Consumer<FormSurveyState>(
                      builder: (context, state, child) {
                        switch (state.formResponse.status) {
                          case Status.LOADING:
                            return new Positioned(
                                bottom: 300,
                                left: 0,
                                right: 0,
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      color: Colors.white30,
                                      child: Center(
                                        child: CircularProgressIndicator(
                                            valueColor:
                                            new AlwaysStoppedAnimation<
                                                Color>(
                                                MemberAppTheme.nearlyBlue)),
                                      ),
                                    )));
                            break;
                          case Status.COMPLETED:
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              /***
                              sau khi vừa tạo đánh giá theo dõi hàng ngày xong thì gán là đã tạo bằng true
                              để khi bấm tạo lần nữa sẽ hiện thị thông báo bạn vừa tạo đánh giá hàng ngày của ngày hôm nay
                              ***/
                              _formItemLogbookServices.setDaCoDanhGia(true);
                              Flushbar(
                                message: 'Bạn đã thực hiện thành công đánh giá!',
                                backgroundColor: Colors.green,
                                duration: Duration(seconds: 2),
                                flushbarPosition: FlushbarPosition.TOP,
                                icon: Icon(
                                  Icons.check_circle,
                                  color: Colors.white,
                                ),
                              ).show(context).then((shouldUpdate) {
                                Navigator.pop(context);
                                //Navigator.of(context, rootNavigator: true).pop(true);
                              });
                            });
                            return SizedBox();
                            break;
                          case Status.ERROR:
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Flushbar(
                                message: 'Xảy ra lỗi trong quá trình thực hiện',
                                backgroundColor: Colors.red,
                                duration: Duration(seconds: 2),
                                flushbarPosition: FlushbarPosition.TOP,
                                icon: Icon(
                                  Icons.check_circle,
                                  color: Colors.white,
                                ),
                              ).show(context).then((shouldUpdate) {
                                Navigator.pop(context);
                               // Navigator.of(context, rootNavigator: true).pop(true);
                              });
                            }); return SizedBox();
                            break;
                          case Status.INIT:
                            return SizedBox();
                            break;
                          default:
                            return SizedBox();
                            break;
                        }
                      }),
                ],
              ),
            ),
          )
        )
      ),
    );
  }


  Widget wdgMemberSuccessCheckRender(ApiResponse<Member> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgMemberSuccessCheckContent(data.data!,context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgMemberSuccessCheckContent(Member member, BuildContext context) {
    return member.approved == null || member.approved == false
        ? Padding(
        padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
        child: Center(
          child: Text(
            'Tài khoản của bạn chưa được duyệt. Nên không có quyền thực hiên chức năng này!',
            style: TextStyle(color: Colors.red),
          ),
        ))
        :
    Consumer<FormItemLogbookState>(
        builder: (context, state, child) {
          return wdgFormItemCheckRender(
              state.formItemCheckResponse, context);
        });
  }

  Widget wdgFormItemCheckRender(ApiResponse<bool> data, context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormItemCheckContent(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgFormItemCheckContent(bool isValid, BuildContext context) {
    //kiểm tra nếu ncs chọn sau ngày hiện tại để đánh giá
    if(_formDate.year == now.year && _formDate.month == now.month)
      if(_formDate.day > now.day)
        return Padding(
            padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
            child: Center(
              child: Text(
                'Báo lỗi, Bạn chỉ được đánh giá từ ngày ${DateFormat(formatStr).format(now).toString()} trở về trước của tháng ${now.month}.',
                style: TextStyle(color: Colors.red),
              ),
            ));

    //nếu kiểm tra ngày hiện tại chưa có đánh giá thì set dacodanhgia = null
    if(isValid == false) _formItemLogbookServices.setDaCoDanhGia(null);

    //nếu dacodanhgia = false thì cho phép tạo đánh giá ngày đã bấm ở lịch
    if(dacodanhgia != null && dacodanhgia != "" && dacodanhgia == false) isValid = true;

    return !isValid
        ? Padding(
        padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
        child: Center(
          child: Text(
            'Báo lỗi, Bạn đã thực hiện đánh giá ngày hôm nay!',
            style: TextStyle(color: Colors.red),
          ),
        ))
        :
    Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(
                left: 25.0, right: 25.0, top: 25.0),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Thời gian thực hiện:',
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            )
        ),
        Padding(
          padding: EdgeInsets.only(
              left: 25.0, right: 25.0, top: 25.0),
          child: ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(2000, 1, 1),
                  maxTime: DateTime.now(),
                  onConfirm: (date) {
                    print('confirm $date');
                    _formDate = date;
                    setState(() {
                      _dateLabel = new DateFormat(formatStr).format(_formDate).toString();
                    });
                  },
                  currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            Text(
                              " $_dateLabel",
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 15.0),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ),
        ),
        Consumer<PlansState>(
            builder: (context, state, child) {
              return wdgPlanRender(state.plansResponseList, context);
            }
        ),
        Consumer<FormTypeState>(
            builder: (context, state, child) {
              return wdgFormTypeNCSRender(state.formTypeNCSResponse, context);
            }
        ),
      ],
    );
  }


  Widget wdgUserSkills() {
    List<UserSkill> list = UserSkill.userSkillList;
    return ListView.builder(
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (context, index) {
          UserSkill skill = list[index];
          return CheckboxListTile(
            title: Text(skill.title, style: TextStyle(color: MemberAppTheme.nearlyBlack)),
            value: skill.isHavePlan,
            onChanged: (bool? value) {
              setState(() {
                skill.isHavePlan = (value == true);
              });
            },
          );
        }
    );
  }

  Widget getTanSuat() {
    List<TanSuat> _companies = TanSuat.getTanSuats();
    List<DropdownMenuItem<TanSuat>>? _dropdownMenuItems;
    TanSuat? _selectedTanSuat;

    onChangeDropdownItem(TanSuat? selectedTanSuat) {
      setState(() {
        _selectedTanSuat = selectedTanSuat;
      });
    }

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: DropdownButton(
          value: _selectedTanSuat,
          items: _dropdownMenuItems,
          onChanged: onChangeDropdownItem,
        ));
  }

  Widget getTimeBoxUI(String text1) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: MemberAppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: MemberAppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: MemberAppTheme.nearlyBlue,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget wdgFormTypeNCSRender(ApiResponse<FormType> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgFormTypeNCSContent(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgFormTypeNCSContent(FormType ft, BuildContext context) {
    Map data = ft.formFormat!;

    return JsonSchema(
      formMap: data,
      onChanged: (dynamic response) {
        this.response = response;
        //print(response);
      },
      actionSave: (data) async {
        //kiểm tra mỗi ngày chỉ dc tạo đánh giá 1 lần
        bool? isValid = await _formItemLogbookState.isValidForCreateSurvey(_memberState.memberId, _formDate, "day");
        if(isValid == false) {
          /*
          final snackBar = SnackBar(content: Text('Xin lỗi, bạn đã tạo đánh giá của ngày hôm nay!'),
              backgroundColor: Colors.red);
          Scaffold.of(context).showSnackBar(snackBar);
          */
          print('Xin lỗi, bạn đã tạo đánh giá của ngày bạn đã chọn!');
          return;
        }
        //-- hết kiểm tra mỗi ngày chỉ dc tạo đánh giá 1 lần
        if(!clicked) {
          clicked = true;
          var fields = data["fields"];
          String objText = '{';
          fields.forEach((field) {
            objText += '"${field["key"]}":${field["value"].toString()},';
            map["${field["key"]}"] = field["value"].toString();
          });
          objText += '}';

          _formUser = _memberState.memberId;
          map["form_type"] = new FormType()
            ..id = ft.id;
          map["member"] = _formUser;
          map["personal_assistant"] = _memberState.personalAssistantId;
          await _memberState.getPersonalAssistant(map["personal_assistant"]);
          map["relationship_type"] = _memberState.relationshipTypeId;
          map["form_date"] = _formDate.toString();
          //var map2 = {};
          //listPlansTmp.forEach((plan) => map2[plan.id] = plan.id);
          var planIdArray = [];
          planIdsChecked.forEach((planId) {
            planIdArray.add(planId);
          });
          map["plans"] = planIdArray;

          _itemLogBook = new FormItemLogBook();
          _itemLogBook!.name = ft.formName;
          _itemLogBook!.formDate = _formDate.toString();
          _itemLogBook!.memberId = _formUser;
          _itemLogBook!.formTypeId = ft.id;
          _itemLogBook!.formType = ft;
          _itemLogBook!.member = new Member(id: _formUser);
          _itemLogBook!.hbcc = new HBCC(id: 0);
          _itemLogBook!.is_ncs = true;
          _formSurveyState.create(
              map, _itemLogBook!, ft.formObject!);

          /* .. cập nhật thông tin cho bảng Form_ncs_logbook_days */
          await UpdateReportTanSuatNCS();

          //Navigator.pop(context);
         /* WidgetsBinding.instance.addPostFrameCallback((_) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return planSuccess();
                }).then((shouldUpdate) {
              Navigator.of(context, rootNavigator: true).pop(true);
            });
          });*/
        }else{
          clicked = true;
        }
      },
      buttonSave: Container(
        child: Center(
          child: Padding(
              padding: const EdgeInsets.only(
                  left: 0, bottom: 25, top: 16, right: 0),
              child:
              InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: ManagerAppTheme.nearLightBlue,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: ManagerAppTheme.nearLightBlue
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color:
                              ManagerAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
          ),
        ),
      ),
    );
    return SizedBox();
  }

  Widget wdgPlanRender(ApiResponse<List<Plan>> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgPlanForm(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgPlanForm(List<Plan> listPlans, context) {
    //List<String> list = new List<String>();
    //list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    //list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    //List<UserSkill> list = UserSkill.userSkillList;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    'Kế hoạch chăm sóc',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              new Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: Divider(
                    color: Colors.lightBlue,
                    height: 1,
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10, right: 10.0, top: 25.0),
          child: ListView.separated(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => Divider(
                color: MemberAppTheme.nearlyWhite,
              ),
              itemCount: listPlans.length,
              itemBuilder: (context, index) {
                //xóa những cái bỏ tick
                Plan plan = listPlans[index];
                return CheckboxListTile(
                  title: Text(plan.title ?? "", style: TextStyle(height: 1.5)),
                  value: planIdsChecked.contains(plan.id),
                  onChanged: (bool? value) {
                    setState(() {
                      if(value == true) {
                        setState(() {
                          if(planIdsChecked.contains(plan.id.toString()) == false) {
                            planIdsChecked.add(plan.id!);
                            //listPlansTmp.add(new Plan(id: plan.id, skillId: plan.id, title: plan.title, isPlaned: false));
                          }
                        });
                      } else {
                        setState(() {
                          planIdsChecked.remove(plan.id);
                          //listPlansTmp.removeWhere((item) => item.id == plan.id);
                        });
                      }
                    });
                  }
                );
              }
          ),
        ),
      ],
    );
  }

  Future<void> UpdateReportTanSuatNCS() async {
    await _reportTanSuatNCSState.getListReportTanSuatNCS(month: _formDate.month, year: _formDate.year,
        mId: _memberState.memberId!);
    List<ReportTanSuatNCS> repTanSuatNCS = _reportTanSuatNCSState.repTanSuatNCS;
    var itemReportTanSuatNCS = new ReportTanSuatNCS();
    //cập nhật
    if(repTanSuatNCS.length > 0) {
      itemReportTanSuatNCS.id = repTanSuatNCS[0].id;
      itemReportTanSuatNCS.month = repTanSuatNCS[0].month;
      itemReportTanSuatNCS.year = repTanSuatNCS[0].year;
      itemReportTanSuatNCS.tansuat_thuchien = repTanSuatNCS[0].tansuat_thuchien! + 1;
      itemReportTanSuatNCS.tansuat_kehoach = repTanSuatNCS[0].tansuat_kehoach;
      itemReportTanSuatNCS.memberId = repTanSuatNCS[0].memberId;
      await _reportTanSuatNCSState.update(itemReportTanSuatNCS);
    } else {
      //thêm mới
      itemReportTanSuatNCS.month = _formDate.month;
      itemReportTanSuatNCS.year = _formDate.year;
      itemReportTanSuatNCS.tansuat_thuchien = 1;
      itemReportTanSuatNCS.tansuat_kehoach = 0;
      itemReportTanSuatNCS.memberId = _memberState.memberId;
      await _reportTanSuatNCSState.add(itemReportTanSuatNCS);
    }
  }

  Widget planSuccess(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:

      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyBlue,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.card_giftcard,color: Colors.lightBlue, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Chúc mừng bạn đã thực hiện đúng kế hoạch đề ra hôm nay!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 10),
                  child:
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    icon: Icon(Icons.send,color: MemberAppTheme.nearlyWhite),
                    label: Text("Đóng", style: TextStyle(color: MemberAppTheme.nearlyWhite),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TanSuat {
  int id;
  String name;

  TanSuat(this.id, this.name);

  static List<TanSuat> getTanSuats() {
    return <TanSuat>[
      TanSuat(1, 'Apple'),
      TanSuat(2, 'Google'),
      TanSuat(3, 'Samsung'),
      TanSuat(4, 'Sony'),
      TanSuat(5, 'LG'),
    ];
  }
}
