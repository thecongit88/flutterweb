import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/logbook_form_month_screen.dart';
import 'package:hmhapp/dp_members/logbook_form_screen.dart';
import 'package:hmhapp/dp_members/models/plan_info_screen.dart';
import 'package:hmhapp/models/tabIcon.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:shared_preferences/shared_preferences.dart';


class BottomBarView extends StatefulWidget {
  const BottomBarView(
      {Key? key,required this.tabIconsList,required this.changeIndex,required this.addClick,this.selectIndex})
      : super(key: key);

  final int? selectIndex;
  final Function(int index) changeIndex;
  final Function addClick;
  final List<TabIcon> tabIconsList;

  @override
  _BottomBarViewState createState() => _BottomBarViewState();
}

class _BottomBarViewState extends State<BottomBarView>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  late AnimationController animationController;
  late int _memId;
  late String selectedDate;
  late bool dacodanhgia;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1000),
    );
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: <Widget>[
        AnimatedBuilder(
          animation: animationController,
          builder: (BuildContext context, Widget? child) {
            return Transform(
              transform: Matrix4.translationValues(0.0, 0.0, 0.0),
              child: PhysicalShape(
                color: Colors.white,
                elevation: 16.0,
                clipper: TabClipper(
                    radius: Tween<double>(begin: 0.0, end: 1.0)
                            .animate(CurvedAnimation(
                                parent: animationController,
                                curve: Curves.fastOutSlowIn))
                            .value *
                        38.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 62,
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 8, right: 8, top: 4),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: TabIcons(
                                  isActive: widget.selectIndex==0,
                                  tabIcon: widget.tabIconsList[0],
                                  removeAllSelect: () {
                                    setRemoveAllSelection(
                                        widget.tabIconsList[0]);
                                    widget.changeIndex(0);
                                  }),
                            ),
                            Expanded(
                              child: TabIcons(
                                  isActive: widget.selectIndex==1,
                                  tabIcon: widget.tabIconsList[1],
                                  removeAllSelect: () {
                                    setRemoveAllSelection(
                                        widget.tabIconsList[1]);
                                    widget.changeIndex(1);
                                  }),
                            ),
                            SizedBox(
                              width: Tween<double>(begin: 0.0, end: 1.0)
                                      .animate(CurvedAnimation(
                                          parent: animationController,
                                          curve: Curves.fastOutSlowIn))
                                      .value *
                                  64.0,
                            ),
                            Expanded(
                              child: TabIcons(
                                  isActive: widget.selectIndex==2,
                                  tabIcon: widget.tabIconsList[2],
                                  removeAllSelect: () {
                                    setRemoveAllSelection(
                                        widget.tabIconsList[2]);
                                    widget.changeIndex(2);
                                  }),
                            ),
                            Expanded(
                              child: TabIcons(
                                  isActive: widget.selectIndex==3,
                                  tabIcon: widget.tabIconsList[3],
                                  removeAllSelect: () {
                                    setRemoveAllSelection(
                                        widget.tabIconsList[3]);
                                    widget.changeIndex(3);
                                  }),
                            ),
                            Expanded(
                              child: TabIcons(
                                  isActive: widget.selectIndex==4,
                                  tabIcon: widget.tabIconsList[4],
                                  removeAllSelect: () {
                                    setRemoveAllSelection(
                                        widget.tabIconsList[4]);
                                    widget.changeIndex(4);
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).padding.bottom,
                    )
                  ],
                ),
              ),
            );
          },
        ),
        Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: SizedBox(
            width: 38 * 2.0,
            height: 38 + 62.0,
            child: Container(
              alignment: Alignment.topCenter,
              color: Colors.transparent,
              child: SizedBox(
                width: 38 * 2.0,
                height: 38 * 2.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ScaleTransition(
                    alignment: Alignment.center,
                    scale: Tween<double>(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                            parent: animationController,
                            curve: Curves.fastOutSlowIn)),
                    child: Container(
                      // alignment: Alignment.center,s
                      decoration: BoxDecoration(
                        color: MemberAppTheme.nearlyBlue,
                        gradient: LinearGradient(
                            colors: [
                              MemberAppTheme.nearlyBlue,
                              HexColor('#6A88E5'),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight),
                        shape: BoxShape.circle,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: MemberAppTheme.nearlyBlue
                                  .withOpacity(0.4),
                              offset: const Offset(3.0, 6.0),
                              blurRadius: 16.0),
                        ],
                      ),
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Colors.white.withOpacity(0.1),
                          highlightColor: Colors.transparent,
                          focusColor: Colors.transparent,
                          onTap: () async {
                            _settingModalBottomSheet(context);
                            /*
                            var _authServices = new AuthServices();
                            this._memId = await _authServices.getSignedMemberId();
                            DateTime now = DateTime.now();
                            int lastday = DateTime(now.year, now.month + 1, 0).day;
                            //LogBookFormScreen or LogBookFormMonthScreen
                            Navigator.push<dynamic>(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (BuildContext context) => now.day == lastday ? LogBookFormMonthScreen() : LogBookFormScreen(memberId: this._memId),
                                ));

                             */
                            //widget.addClick();
                            //_showNewTransactionDialog(context);
                          },
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _settingModalBottomSheet(context) async {
    final ThemeData theme = Theme.of(context);

    var _authServices = new AuthServices();
    this._memId = await _authServices.getSignedMemberId();
    DateTime now = DateTime.now();
    int lastday = DateTime(now.year, now.month + 1, 0).day;
    bool isMonthLogbook = now.day >= 26 && now.day <= lastday;
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          //return ManagerChooseLogbookWidget();
          return Container(
            padding: EdgeInsets.only(top: 20, bottom: 10),
            child: new Wrap(
              children: <Widget>[
                Padding(
                  child: Center(
                    child: Text(
                      "Chọn đánh giá",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: theme.primaryColor),
                    ),
                  ),
                  padding: EdgeInsets.only(bottom: 10),
                ),
                Divider(
                  height: 1,
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  child: Padding(
                    child: Row(
                      children: <Widget>[
                        new Icon(Icons.schedule,color: theme.primaryColor,),
                        SizedBox(width: 10,),
                        new Text("Đánh giá theo dõi hàng ngày", style: TextStyle(
                          color: isMonthLogbook? Colors.grey: Colors.black
                        ),),
                      ],
                    ),
                    padding: EdgeInsets.only(
                        left: 20, right: 10, top: 6, bottom: 6),
                  ),
                  onTap: () async {
                    if (now.day < 25) {
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      setState(() {
                        selectedDate = prefs.getString('selectedDate')!;
                        ///BIẾN: dacodanhgia
                        /// true: khi chạm vào lịch tuần mà có dữ liệu tức là chưa có đánh giá ngày hôm nay,
                        /// false: khi chạm vào lịch tuần mà ko có dữ liệu tức là chưa có đánh giá ngày hôm nay
                        /// null: ban đầu tiên sẽ set là dacodanhgia là null (chưa bấm vào lịch)
                        dacodanhgia = prefs.getBool('dacodanhgia')!;
                      });

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                LogBookFormScreen(memberId: this._memId, selectedDate: selectedDate),
                          ));
                    }
                  },
                ),
                SizedBox(height: 40,),
                GestureDetector(
                  child: Padding(
                    child: Row(
                      children: <Widget>[
                        new Icon(Icons.event_available,color: theme.primaryColor),
                        SizedBox(width: 10,),
                        new Text("Đánh giá theo dõi hàng tháng", style: TextStyle(
                            color: isMonthLogbook? Colors.black: Colors.grey
                        ),),
                      ],
                    ),
                    padding: EdgeInsets.only(
                        left: 20, right: 10, top: 6, bottom: 6),
                  ),
                  onTap: () async {
                    if(now.day <= lastday && now.day>=26) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  LogBookFormMonthScreen(memberId: this._memId)
                          ));
                    }
                  },
                ),
                SizedBox(height: 40,),
              ],
            ),
          );
        }
    );}

  void _showNewTransactionDialog(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15.0),
          ),
        ),
        isScrollControlled: true,
        builder: (_) {
          return LogBookFormScreen();
        });
  }

  Widget getPlans() {
    List<String> list = <String>[];
    list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    return Wrap(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(20),
            child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Center(
              child: new Text(
                'Thực hiện tự đánh giá',
                style:
                TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15.0),
              child: ListView.separated(
                shrinkWrap: true,
                separatorBuilder: (context, index) => Divider(
                  color: MemberAppTheme.nearlyWhite,
                ),
                itemCount: list.length,
                itemBuilder: (context, index) => Container(
                  decoration: BoxDecoration(
                    color: HexColor('#F8FAFB'),
                    borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                    // border: new Border.all(
                    //     color: MemberAppTheme.notWhite),
                  ),
                  child: ListTile(
                    title: Text(list[index], style: TextStyle(height: 1.5)),
                    trailing: Container(
                      decoration: BoxDecoration(
                        color: MemberAppTheme
                            .nearlyBlue,
                        borderRadius:
                        const BorderRadius.all(
                            Radius.circular(
                                8.0)),
                      ),
                      child: Padding(
                        padding:
                        const EdgeInsets.all(
                            4.0),
                        child: Icon(
                          Icons.add,
                          color:
                          MemberAppTheme
                              .nearlyWhite,
                        ),
                      ),
                    ),
                    onLongPress: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => PlanInfoScreen(),
                          ));
                    },
                  ),
                ),
              ),
            ),
          ],
        ))
      ],
    );
  }

  void setRemoveAllSelection(TabIcon tabIconData) {
    if (!mounted) return;
    setState(() {
      widget.tabIconsList.forEach((TabIcon tab) {
        tab.isSelected = false;
        if (tabIconData.index == tab.index) {
          tab.isSelected = true;
        }
      });
    });
  }
}

class TabIcons extends StatefulWidget {
  const TabIcons({Key? key,required this.tabIcon, this.removeAllSelect,this.isActive})
      : super(key: key);

  final TabIcon tabIcon;
  final Function? removeAllSelect;
  final bool? isActive;

  @override
  _TabIconsState createState() => _TabIconsState();
}

class _TabIconsState extends State<TabIcons> with TickerProviderStateMixin {
  @override
  void initState() {
    widget.tabIcon.animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    )..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          if (!mounted) return;
          widget.removeAllSelect!();
          widget.tabIcon.animationController!.reverse();
        }
      });
    super.initState();
  }

  void setAnimation() {
    widget.tabIcon.animationController!.forward();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return AspectRatio(
      aspectRatio: 1,
      child: Center(
        child: InkWell(
          splashColor: Colors.transparent,
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          onTap: () {
            /* fix loi icon bottom home ko click dc
            if (!widget.tabIcon.isSelected) {
              setAnimation();
            }*/
            setAnimation();
          },
          child: IgnorePointer(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                ScaleTransition(
                  alignment: Alignment.center,
                  scale: Tween<double>(begin: 0.88, end: 1.0).animate(
                      CurvedAnimation(
                          parent: widget.tabIcon.animationController!,
                          curve:
                              Interval(0.1, 1.0, curve: Curves.fastOutSlowIn))),
                  child: Column(
                    children: <Widget>[
                      Icon(widget.tabIcon.icon,color: widget.isActive!?theme.primaryColor:Colors.black87,),
                      Text(
                        widget.tabIcon.title ?? "",
                        style:
                            TextStyle(fontSize: 14.0, color: widget.isActive!?theme.primaryColor: MemberAppTheme.nearlyBlack),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TabClipper extends CustomClipper<Path> {
  TabClipper({this.radius = 0});

  final double radius;

  @override
  Path getClip(Size size) {
    final Path path = Path();

    final double v = radius * 2;
    path.lineTo(0, 0);
    path.arcTo(Rect.fromLTWH(0, 0, radius, radius), degreeToRadians(180),
        degreeToRadians(90), false);
    path.arcTo(
        Rect.fromLTWH(
            ((size.width / 2) - v / 2) - radius + v * 0.04, 0, radius, radius),
        degreeToRadians(270),
        degreeToRadians(70),
        false);

    path.arcTo(Rect.fromLTWH((size.width / 2) - v / 2, -v / 2, v, v),
        degreeToRadians(160), degreeToRadians(-140), false);

    path.arcTo(
        Rect.fromLTWH((size.width - ((size.width / 2) - v / 2)) - v * 0.04, 0,
            radius, radius),
        degreeToRadians(200),
        degreeToRadians(70),
        false);
    path.arcTo(Rect.fromLTWH(size.width - radius, 0, radius, radius),
        degreeToRadians(270), degreeToRadians(90), false);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(TabClipper oldClipper) => true;

  double degreeToRadians(double degree) {
    final double redian = (math.pi / 180) * degree;
    return redian;
  }
}


class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
