import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/dp_members/models/plan_info_screen.dart';
import 'dart:io';
import 'package:hmhapp/dp_members/report_view.dart';
import 'package:hmhapp/dp_members/screens/member_info_edit_screen.dart';
import 'package:hmhapp/dp_members/screens/member_ncs_edit_screen.dart';
import 'package:hmhapp/dp_members/skill_list_view.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/tan_suat.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:hmhapp/services/tan_suat_services.dart';
import 'package:hmhapp/services/ward_services.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/plans_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/states/skill_groups_state.dart';
import 'package:hmhapp/utils/camera_screen.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:material_segmented_control/material_segmented_control.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'design_course_app_theme.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as pPath;

class MemberInfoScreen extends StatefulWidget {
  @override
  _MemberInfoScreenState createState() => _MemberInfoScreenState();
}

class _MemberInfoScreenState extends State<MemberInfoScreen>
    with TickerProviderStateMixin {

  final _provinceService = ProvinceServices();
  final _districtServices = DistrictServices();
  final _wardServices = WardServices();

  var provinceSelect = 0;
  var districtSelect = 0;
  var wardSelect = 0;

  var provinceCode = "";
  var orderProvince = 0;
  Province provinceObj = Province();
  List<Province> listProvinces = <Province>[];
  List<District> listDistricts = <District>[];
  List<Ward> listWards =  <Ward>[];

  int _currentSelection = 0;
  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  File? _imageFile;
  String? _avatar, imageAvatarUrl = "";

  MemberState memberState = MemberState();
  MemberHomeState memberHomeState = MemberHomeState();
  final ReportTanSuatNCSState _reportTanSuatNCSState = ReportTanSuatNCSState();

  PlansState plansState = PlansState();
  SkillGroupsState skillGroupState = SkillGroupsState();
  List<Plan> listPlans =  <Plan>[];
  int? idPlanChangedTanSuat = 0, idTanSuatChanged = 0, currentYear, currentMonth;

  final double _fontSize = 16.0;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: const Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    _avatar = "assets/design_course/userImage.png";
    setData();
    super.initState();
    memberState.getMember();
    memberState.getAvatar();
    listPlans = <Plan>[];
    currentYear = DateTime.now().year;
    currentMonth = DateTime.now().month;
    listWards  = <Ward>[];
    listDistricts = <District>[];
    getAllTinh();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  Future<File> _getLocalFile(String filename) async {
    String dir = (await pPath.getApplicationDocumentsDirectory()).path;
    File f = File('$dir/$filename');
    return f;
  }

  final Map<int, Widget> _children = {
    0: const Text('Người KT'),
    1: const Text('Người CSC'),
    2: const Text('Kỹ năng')
  };

  @override
  Widget build(BuildContext context) {
    Map<int, Widget> map = Map();
    List<Widget> childWidgets = <Widget>[];
    int selectedIndex = 0;
    childWidgets.add(wdgNKT());
    childWidgets.add(wdgNCSC());
    childWidgets.add(wdgPlan(context));

    return
    MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => memberState),
      ChangeNotifierProvider(create: (_) => memberHomeState),
      ChangeNotifierProvider(create: (_) => plansState),
    ],
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: MemberAppTheme.nearlyBlue,
            elevation: 0,
            title: const Text("Cá nhân"),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 15,top:10),
                child:
                InkWell(
                  child: const Icon(Icons.pie_chart),
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => ReportSkillScreen(memberId: memberState.memberId!),
                        ));

                  },
                ),
              )
            ],
          ),
          backgroundColor: Colors.transparent,
          body: Container(
            color: Colors.white,
            child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      height: 120.0,
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                              child: Consumer<MemberState>(
                                  builder: (context, state, child) {
                                    return buildAvatar(state,context);
                                  }
                              )

                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: MaterialSegmentedControl(
                        children: _children,
                        verticalOffset: 10,
                        borderColor: Colors.lightBlue,
                        selectedColor: Colors.lightBlue,
                        unselectedColor: Colors.white,
                        selectionIndex: _currentSelection,
                        borderRadius: 20.0,
                        onSegmentChosen: (int? index) {
                          setState(() {
                            _currentSelection = index!;
                            if(index == 1) {
                              memberState.getPersonalAssistant(memberState.personalAssistantId);
                            }
                            if(index == 2) {
                              plansState.getListPlans(mId:  memberState.memberId!);
                            }
                          });
                        },
                      ),
                    ),
                    Container(child: childWidgets[_currentSelection]),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  Widget buildAvatar(MemberState state, context){
    imageAvatarUrl = (state.avatarUrl == null?"": state.avatarUrl);

    switch (state.avatarResponse.status) {
      case Status.LOADING:
        return avatar(state,context);
      case Status.COMPLETED:
        return avatar(state,context);
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.avatarResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return avatar(state,context);
      default:
        return avatar(state,context);
    }
  }

  Widget avatar(MemberState state, context){
    return Stack(fit: StackFit.loose, children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
              child:
              Container(
                width: 80.0,
                height: 80.0,
                child:
                state.getIsCapture() == false || imageAvatarUrl!.isEmpty ?
                CircleAvatar(
                    radius: 20,
                    backgroundImage:
                    NetworkImage(imageAvatarUrl!)
                ):
                CircleAvatar(
                    radius: 20,
                    backgroundImage:
                    AssetImage(_avatar!)
                ),
              )
          )
        ],
      ),
      Padding(
          padding:
          const EdgeInsets.only(top: 40.0, right: 50.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                  backgroundColor: Colors.red,
                  radius: 20.0,
                  child: IconButton(
                    icon: const Icon(Icons.camera_alt, size: 20.0, color: AppTheme.white),
                    tooltip: 'Chụp ảnh',
                    onPressed: () async {
                      var imagePathCapture = await Navigator.push(context, MaterialPageRoute(
                          builder: (context) => CameraScreen()),
                      );
                      if(imagePathCapture == null)
                        print("Bạn chưa chụp hình!");
                      else {
                        if(imagePathCapture != "") {
                          var path = join((await getTemporaryDirectory()).path,imagePathCapture);
                          memberState.setAvatarUrl(path,imagePathCapture);
                          memberState.setIsCapture(true);
                        }
                      }
                    },
                  )
              )
            ],
          )),
    ]);
  }

  Widget wdgNKT() {
    return Consumer<MemberState>(
        builder: (context, state, child) {
          return wdgNKTRender(state.memberResponse,context);
        }
    );
  }

  Widget wdgNCSC() {
    return Consumer<MemberState>(
        builder: (context, state, child) {
          return wdgNCSCRender(state.personalAssistantResponse,context);
        }
    );
  }

  Widget wdgNCSCForm(PersonalAssistant ncs, context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const Text(
                    'Thông tin người chăm sóc',
                    style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    child: const Icon(Icons.library_add, color: MemberAppTheme.nearlyBlue,),
                    onTap: () async {
                        var retSavedNCS = await
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => MemberNCSEditScreen(personalAssistantId:
                            memberState.personalAssistantId,),
                          ));

                        if(retSavedNCS!=null &&  retSavedNCS["saved_ncs"] != null && retSavedNCS["saved_ncs"] == true) {
                            memberState.getPersonalAssistant(memberState.personalAssistantId);
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Flushbar(
                                message: 'Bạn thay đổi thành công thông tin người chăm sóc!',
                                backgroundColor: Colors.green,
                                duration: const Duration(seconds: 5),
                                flushbarPosition: FlushbarPosition.BOTTOM,
                              ).show(context);
                            });
                        }
                    },
                  )
                ],
              ),
                Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: const Divider(
                      color: Colors.lightBlue,
                      height: 1,
                    )),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    'Họ và tên NCSC:',
                    style: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                      child: Text(ncs!.name != null ? ncs!.name?? "" : "",
                      style: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Năm sinh NCSC:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                      child: Text(ncs.BirthYear!=null? ncs.BirthYear.toString():""))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Giới tính NCSC:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                      child: Text(ncs.gender!= null? ncs.gender.toString():""))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Dân tộc NCSC:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                      child: Text(ncs!.nation_other!= null && ncs!.nation_other!.isNotEmpty?
                      ncs!.nation_other! : (ncs.nation != null? ncs.nation! :"")))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Tình trạng hôn nhân NCSC:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                      child: Text(ncs!.marital_status_other != null && ncs!.marital_status_other!.isNotEmpty
                          ? ncs!.marital_status_other! : (ncs!.marital_status! != null ? ncs!.marital_status!:"")))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Trình độ học vấn:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0, top: 2.0),
                      child: ncs.edu_level != null ? Text(ncs.edu_level.toString()) : const Text("---")
                  )
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Nghề nghiệp chính:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 0.0, right: 25.0, top: 2.0),
                      child: Text(ncs.job_type_other != null && ncs.job_type_other!.isNotEmpty
                          ?ncs.job_type_other! : (ncs.job_type!=null?ncs.job_type!:"")))
                ],
              )
          ),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Mối quan hệ của NCSC với NKT:',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                      child: Text(ncs.relationship_type_other != null && ncs.relationship_type_other!.isNotEmpty?
                      ncs.relationship_type_other! : (ncs.relationship_type!=null && ncs!.relationship_type!.title!=null?
                      ncs.relationship_type!.title!:"")))
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Vai trò của NCSC trong gia đình?',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(height: 5,),
                  ncs.vaitro_ncsc_chichamsoc_nkt != null && ncs.vaitro_ncsc_chichamsoc_nkt == true ?
                  const Text('- Chỉ chăm sóc NKT'): const SizedBox(height: 0,),
                  ncs.vaitro_ncsc_laodong_taothunhap != null  && ncs.vaitro_ncsc_laodong_taothunhap == true?
                  const Text('- Là người lao động tạo thu nhập cho gia đình'): const SizedBox(height: 0,),
                  ncs.vaitro_ncsc_nguoics_cacthanhvien != null && ncs.vaitro_ncsc_nguoics_cacthanhvien == true?
                  const Text(
                        "- Là người chăm sóc các thành viên trong gia đình, trong đó có NKT"): const SizedBox(height: 0,),
                  /*ListView.builder (
                      shrinkWrap: true,
                      itemCount: ncs.family_roles.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                            padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                            child: Text(ncs.family_roles[index].title.toString()));
                      }
                  )*/
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const Text(
                        'Nhà bạn có internet không?',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ],
              )),
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(ncs.is_have_internet!= null && ncs.is_have_internet == true ? "- Có" : "- Không")
                ],
              )),
          Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Text(
                  'Bạn sử dụng điện thoại di động?',
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                    child: Text(ncs.is_have_phone!= null && ncs.is_have_phone == true ? "- Có" : "- Không")),
              ],
            ),
          ),
          ncs.is_have_phone!= null &&  ncs.is_have_phone == true?
          Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0,bottom: 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text(
                    'Nếu Có, Điện thoại của bạn thuộc loại nào?',
                    style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                      child: Text(ncs.is_smartphone == true ? "- Điện thoại có thể tiếp cận internet" : "- Điện thoại không thể tiếp cận internet"))
                ],
              )):const SizedBox(height: 0,),
          const SizedBox(height: 100,)
        ],
      )
    );
  }

  Widget wdgPlan(context) {
     if(memberState.approved != null && memberState.approved == true) {
       return Consumer<PlansState>(
           builder: (context, state, child) {
             return wdgPlanRender(state.plansResponseList, context);
           });
     }else {
       return const Padding(
           padding: EdgeInsets.only(left: 20, top: 20, right: 20),
           child: Center(
             child: Text(
               'Tài khoản của bạn chưa được duyệt. Nên không có quyền thực hiên chức năng này!',
               style: TextStyle(color: Colors.red),
             ),
           ));
     }
  }

  Widget getTimeBoxUI(String text1, String txt2) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: MemberAppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: MemberAppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: MemberAppTheme.nearlyBlue,
                ),
              ),
              Text(
                txt2,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontWeight: FontWeight.w200,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: MemberAppTheme.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget wdgNKTRender(ApiResponse<Member> data,context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgNKTForm(data.data! ,context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget wdgNKTForm(Member member,context) {
    if(listDistricts.isEmpty && member.provinceId != null) {
      getAllHuyen(member.provinceId!);
    }
    if(listWards.isEmpty && member.districtId != null) {
      getAllXa(member.districtId!);
    }
    return Container(
      color: const Color(0xffFFFFFF),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 50.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      const Text(
                        'Thông tin người khuyết tật',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                      InkWell(
                        child: const Icon(Icons.library_add, color: MemberAppTheme.nearlyBlue,),
                        onTap: () async {
                          var retSavedNKT = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => MemberInfoEditScreen(
                                  provinces: listProvinces,
                                  disticts:  listDistricts,
                                  wards: listWards,
                                  member: member,
                                ),
                              ));

                          if(retSavedNKT!= null && retSavedNKT["saved_nkt"] != null && retSavedNKT["saved_nkt"] == true) {
                            memberState.getMember();
                            listDistricts = <District>[];
                            listWards = <Ward>[];
                            //setState(() {
                            //  listDistricts = retSavedNKT["listHuyen"];
                            //  listWards = retSavedNKT["listXa"];
                            //});
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Flushbar(
                                message: 'Bạn thay đổi thành công thông tin người khuyết tật!',
                                backgroundColor: Colors.green,
                                duration: const Duration(seconds: 5),
                                flushbarPosition: FlushbarPosition.BOTTOM,
                              ).show(context);
                            });
                          }
                        },
                      )
                    ],
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: const Divider(
                        color: Colors.lightBlue,
                        height: 1,
                      )),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Mã:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(member.code ?? ""))
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Thời gian bắt đầu dự án:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("${member.projectJoinDate!=null?
                        DateFormat("dd-MM-yyyy").format(DateTime.parse(member.projectJoinDate.toString())):""}")
                    )
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Họ và tên:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(member.name ?? "")
                    )
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Giới tính:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(member.gender!=null?member.gender!:""))
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Dân tộc:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(member.nation_other!= null && member.nation_other!.isNotEmpty?
                        member.nation_other!: (member.nation!=null?member.nation!:"")))
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child:
                member.birth_date != null && member.birth_date!.isNotEmpty?
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Ngày sinh:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("${member.birth_date!= null?
                        DateFormat("dd-MM-yyyy").format(DateTime.parse(member.birth_date!)):""}"))
                  ],
                ):
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Năm sinh:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("${member.birth_year.toString()}"))
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Số tuổi:',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text("${(
                            currentYear! - (member.birth_date != null && member.birth_date!.isNotEmpty?
                            DateTime.parse(member.birth_date!).year:member.birth_year!)
                            )}"))
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      'Dạng khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(height: 10,),
                    Text("${member.disabilityType}")
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Thời điểm khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 10.0, right: 25.0, top: 2.0),
                        child: Text(member.disabilityStart!=null?member.disabilityStart!:"")
                    ),
          ],
                )),
            member.disabilityStartId == 2?
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Text(
                      'Năm khuyết tật :',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: member.disability_year != null ?
                        Text("${member.disability_year}") : const Text("---")
                    )
                  ],
                )):const SizedBox(height: 0,),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text(
                          'NKT có giấy xác nhận chất độc da cam :',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                )),
            Padding(
                padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0,bottom: 50),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(member.is_agent_orange!=null && member.is_agent_orange == true ? "Có" : "Không")
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget wdgNCSCRender(ApiResponse<PersonalAssistant> data,context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgNCSCForm(data.data!,context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget wdgPlanRender(ApiResponse<List<Plan>> data, context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgPlanForm(data.data!, context);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

  Widget wdgPlanForm(List<Plan> listPlansTmp, context) {
    listPlans = listPlansTmp;
    int countPlans = listPlans.length;

    //sort plans
    listPlans.sort((a, b) => a.title!.compareTo(b.title ?? ""));

    //List<String> list = new List<String>();
    //list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    //list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");
    //List<UserSkill> list = UserSkill.userSkillList;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const Text(
                    'Kế hoạch chăm sóc',
                    style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    child: const Icon(Icons.add_circle, color: MemberAppTheme.nearlyBlue,),
                    onTap: () async {
                      var listPlansReturn = <Plan>[];
                      var skillIdsChecked = <int>[];
                      var currentPlansChecked = <Plan>[];
                      int i; Plan plan;

                      var dataSkillIdsCheked = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => SkillListView(
                            listPlans: listPlans
                          ),
                      ));

                      if(dataSkillIdsCheked != null) {
                        skillIdsChecked = dataSkillIdsCheked["skillIdsChecked"];
                        listPlansReturn = dataSkillIdsCheked["listPlansChecked"];
                        plansState.setNewListPlans(listPlansReturn);

                        currentPlansChecked = await skillGroupState.getCurrentPlanSkillList(memberState.memberId!);
                        //xóa những bản ghi cũ mà có skillId không nằm trong những skillId vừa check
                        int idTanSuat = 0, valueTanSuatKeHoach = 0;
                        int ncurrentPlansChecked = currentPlansChecked.length;
                        ReportTanSuatNCS itemReportTanSuatNCS;

                        await _reportTanSuatNCSState.getListReportTanSuatNCS(month: currentMonth!,
                            year: currentYear!, mId: memberState.memberId!);
                        List<ReportTanSuatNCS> repTanSuatNCS = _reportTanSuatNCSState.repTanSuatNCS;
                        if(repTanSuatNCS.length == 0) {
                          //thêm mới
                          itemReportTanSuatNCS = ReportTanSuatNCS();
                          itemReportTanSuatNCS.month = currentMonth;
                          itemReportTanSuatNCS.year = currentYear;
                          itemReportTanSuatNCS.tansuat_thuchien = 0;
                          itemReportTanSuatNCS.tansuat_kehoach = 0;
                          itemReportTanSuatNCS.memberId = memberState.memberId;
                          await _reportTanSuatNCSState.add(itemReportTanSuatNCS);
                        }

                        for(i=0; i<ncurrentPlansChecked; i++) {
                          if(skillIdsChecked.contains(currentPlansChecked[i].skillId) == false) {
                            await plansState.deletePlan(currentPlansChecked[i].id); //delete by plan id

                            idTanSuat = currentPlansChecked[i]!.tan_suat ?? 0;
                            valueTanSuatKeHoach = await getVaueTanSuatKeHoach(idTanSuat);
                            //print(valueTanSuatKeHoach);
                            /* .. cập nhật thông tin cho bảng Form_ncs_logbook_days */
                            //cập nhật
                            if(repTanSuatNCS.length > 0) {
                              itemReportTanSuatNCS = ReportTanSuatNCS();
                              itemReportTanSuatNCS.id = repTanSuatNCS[0].id;
                              itemReportTanSuatNCS.month = repTanSuatNCS[0].month;
                              itemReportTanSuatNCS.year = repTanSuatNCS[0].year;
                              itemReportTanSuatNCS.tansuat_thuchien = repTanSuatNCS[0].tansuat_thuchien;
                              itemReportTanSuatNCS.tansuat_kehoach = repTanSuatNCS[0]!.tansuat_kehoach! - valueTanSuatKeHoach;
                              itemReportTanSuatNCS.memberId = repTanSuatNCS[0].memberId;
                              await _reportTanSuatNCSState.update(itemReportTanSuatNCS);
                            }
                            /* .. end cập nhật thông tin cho bảng Form_ncs_logbook_days */

                          } else {
                            skillIdsChecked.remove(currentPlansChecked[i].skillId);
                          }
                        }
                        //thêm plan
                        for(i=0; i<skillIdsChecked.length; i++) {
                          plan = Plan();
                          plan.skillId = skillIdsChecked[i];
                          await plansState.addPlan(plan, mId: memberState.memberId!);
                        }
                      }
                    },
                  )
                ],
              ),
              Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  child: const Divider(
                    color: Colors.lightBlue,
                    height: 1,
                  )),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25.0, top: 25.0,bottom: 100),
          child: ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              separatorBuilder: (context, index) => const Divider(
                color: MemberAppTheme.nearlyWhite,
              ),
              itemCount: countPlans,
              itemBuilder: (context, index) {
                //xóa những cái bỏ tick
                Plan plan = listPlans[index];
                return Container(
                  decoration: BoxDecoration(
                    color: HexColor('#F8FAFB'),
                    borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                    // border: new Border.all(
                    //     color: MemberAppTheme.notWhite),
                  ),
                  child: GestureDetector(
                    child: ListTile(
                      title: Text(plan.title ?? "", style: const TextStyle(height: 1.5)),
                      trailing: plan.isPlaned == true ?
                      const Icon(
                        Icons.mode_edit,
                        color: MemberAppTheme.nearlyBlue,
                      ):
                      const Icon(
                        Icons.add,
                        color: MemberAppTheme.nearlyBlue,
                      )
                    ),
                    onTap: () async {
                      var _plan = Plan();
                      //lấy plan objec theo skillId vì trong list danh sách plans có thể plan id = 0 do lúc chọn ko nạp từ csdl
                      //truong hop khi them moi plan, do object tra ve khong bao gom planId, nen ko dung plan object de bind info
                      await plansState.getPlanBySkill(plan.skillId!, memberState.memberId!);
                      _plan = plansState.plan;
                      var ret = await  Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => PlanInfoScreen(
                              plan: _plan,
                              memberId: memberState.memberId,
                            ),
                          ));
                      if(ret !=null) {
                        plan.isPlaned = true;
                        int idTanSuat = 0,
                            valueTanSuatKeHoach = 0;
                        await _reportTanSuatNCSState.getListReportTanSuatNCS(
                            month: currentMonth!,
                            year: currentYear!,
                            mId: memberState.memberId!);
                        List<
                            ReportTanSuatNCS> repTanSuatNCS = _reportTanSuatNCSState
                            .repTanSuatNCS;

                        for (int i = 0; i < countPlans; i++) {
                          if (ret["planId"] != null &&
                              ret["planId"] == listPlans[i].id)
                            idTanSuat = ret["tansuatId"];
                          else
                            idTanSuat = listPlans[i]!.tan_suat ?? 0;
                          valueTanSuatKeHoach +=
                          await getVaueTanSuatKeHoach(idTanSuat);
                          /* .. cập nhật thông tin cho bảng Form_ncs_logbook_days */
                          var itemReportTanSuatNCS = ReportTanSuatNCS();

                          //cập nhật
                          if (repTanSuatNCS.length > 0) {
                            itemReportTanSuatNCS.id = repTanSuatNCS[0].id;
                            itemReportTanSuatNCS.month = repTanSuatNCS[0].month;
                            itemReportTanSuatNCS.year = repTanSuatNCS[0].year;
                            itemReportTanSuatNCS.tansuat_thuchien =
                                repTanSuatNCS[0].tansuat_thuchien;
                            itemReportTanSuatNCS.tansuat_kehoach =
                                valueTanSuatKeHoach;
                            itemReportTanSuatNCS.memberId =
                                repTanSuatNCS[0].memberId;
                            await _reportTanSuatNCSState.update(
                                itemReportTanSuatNCS);
                          } else {
                            //thêm mới
                            itemReportTanSuatNCS.month = currentMonth;
                            itemReportTanSuatNCS.year = currentYear;
                            itemReportTanSuatNCS.tansuat_thuchien = 0;
                            itemReportTanSuatNCS.tansuat_kehoach =
                                valueTanSuatKeHoach;
                            itemReportTanSuatNCS.memberId =
                                memberState.memberId;
                            await _reportTanSuatNCSState.add(
                                itemReportTanSuatNCS);
                          }
                          /* .. end cập nhật thông tin cho bảng Form_ncs_logbook_days */
                        }
                      }
                    },
                  ),
                );
              }
          ),
        ),
      ],
    );
  }


  getAllTinh() async{
    _provinceService.listProvinces()
        .then((data) {
          listProvinces = data;
    });
  }

  getAllHuyen(int provinceId){
    _districtServices
        .listDistrictsByProvince(provinceId)
        .then((data) {
          listDistricts = data;
    });
  }

  getAllXa(int districtId) async {
    _wardServices
        .listWardsByDistrict(districtId)
        .then((data) {
          listWards = data;
    });
  }

}

Future<int> getVaueTanSuatKeHoach(int idTanSuat) async {
  int tanSuatKeHoach = 0;
  var tansuatOptions = <TanSuatCLass>[];
  var vTanSuat = <TanSuatCLass>[];
  var tanSuatServices = TanSuatServices();
  tansuatOptions = await tanSuatServices.listTanSuats();
  vTanSuat = tansuatOptions.where((item) => item.id == idTanSuat).toList();
  if(vTanSuat.length > 0) tanSuatKeHoach = vTanSuat[0]!.value!;
  return tanSuatKeHoach;
}
