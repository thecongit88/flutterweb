
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/dp_managers/models/support/support_message.dart';
import 'package:hmhapp/states/support/support_message_state.dart';

import '../../../../common/app_constant.dart';
import '../../../../common/app_global.dart';
import '../../../../common/text_theme_app.dart';
import '../../../../common_screens/camera/camera_state.dart';
import '../../../../models/files/file_fields_upload.dart';
import '../../../../states/support/support_conversation_state.dart';
import '../../../../widgets/button_widget.dart';
import '../../../../widgets/camera/camera_capture_widget.dart';

class MemberSupportConversationWidget extends StatefulWidget {
  MemberSupportConversationWidget();

  @override
  State<StatefulWidget> createState() {
    return MemberSupportConversationWidgetState();
  }
}

class MemberSupportConversationWidgetState extends State<MemberSupportConversationWidget>
    with AutomaticKeepAliveClientMixin<MemberSupportConversationWidget> {
  final contentController = TextEditingController();
  final titleController = TextEditingController();

  var supportConversationState = SupportConversationState();
  final cameraState = CameraState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    super.build(context);
    return Column(
      children: [
        TextThemeApp.wdgTitle("Tiêu đề", isRequired: true),
        TextFormField(
          controller: titleController,
          decoration:  InputDecoration(
            hintText: "Nhập tiêu đề yêu cầu",
            hintStyle: TextStyle(fontSize: 12.sp),
          ),
        ),
        AppConstant.spaceVerticalMediumExtra,
        TextThemeApp.wdgTitle("Nội dung", isRequired: true),
        TextFormField(
          controller: contentController,
          maxLines: 5,
          decoration:  InputDecoration(
              hintText: "Nhập chi tiết nội dung yêu cầu",
              hintStyle: TextStyle(fontSize: 12.sp),
          ),
        ),
        AppConstant.spaceVerticalMediumExtra,
        CameraCaptureWidget(controllerCamera: cameraState),
        AppConstant.spaceVerticalMediumExtra,
        Row(
          children: [
            Expanded(
              child: ButtonWidget(
                bgColor: theme.primaryColor,
                ic: Icons.check_circle,
                title: "Gửi phản hồi",
                onPress: () {
                  save(context); //status 1 - xuat ban
                },
              ),
            )
          ],
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  void save(context) async {
    if(titleController.text.isEmpty) {
      showErrorToast(context, "Bạn chưa nhập tiêu đề.");
      return;
    }
    if(contentController.text.isEmpty) {
      showErrorToast(context, "Bạn chưa nhập nội dung.");
      return;
    }

    final item = SupportConversation();
    item.summary = contentController.text.trim();
    item.title = titleController.text.trim();
    item.status = AppConstant.SUPPORT_CHOXEM;
    item.images = [];
    var res = await supportConversationState.addConversation(item);
    if (res != null) {
      var fields = FileFieldUpload()
        ..ref = "support-conversations"
        ..refId = res.id
        ..field = "images";

      var fileResult = await cameraState.uploadImage(fields);
      if (fileResult == null) {
        supportConversationState.deleteConversation(res.id);
        Flushbar(
          message: 'Xảy ra lỗi trong quá trình upload hình ảnh.',
          backgroundColor: Colors.red,
          icon: const Icon(Icons.check_circle,color: Colors.white,),
          duration: const Duration(seconds: 5),
          flushbarPosition: FlushbarPosition.TOP,
        ).show(context);
      }else{
        Navigator.pop(context, true);
      }
    } else {
      Flushbar(
        message: 'Xảy ra lỗi trong quá trình thêm yêu cầu.',
        backgroundColor: Colors.red,
        icon: const Icon(Icons.check_circle,color: Colors.white,),
        duration: const Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.TOP,
      ).show(context);
    }
  }
}
