import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/common/app_global.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/manager_member_detail_screen.dart';
import 'package:hmhapp/dp_managers/widgets/manage_members_search_delegate.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:month_year_picker/month_year_picker.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../common/text_theme_app.dart';
import '../../../states/hbcc_ke_hoach_state.dart';
import '../../../states/hbcc_member_ke_hoach_state.dart';
import '../../../states/hbcc_state.dart';
import '../../../states/ncs_ke_hoach_state.dart';
import '../../../utils/utils.dart';
import '../../../widgets/bottom_sheet_widget.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/empty_data_widget.dart';
import '../../../widgets/month_year_dialog.dart';
import '../../design_course_app_theme.dart';

class FilterNcsListKeHoachScreen extends StatefulWidget {
  final int? monthFiltered;
  final int? yearFiltered;
  final int? memberIdFiltered;
  const FilterNcsListKeHoachScreen({Key? key, this.monthFiltered, this.yearFiltered, this.memberIdFiltered}) : super(key: key);

  @override
  _FilterNcsListKeHoachScreenState createState() => _FilterNcsListKeHoachScreenState();
}

class _FilterNcsListKeHoachScreenState extends State<FilterNcsListKeHoachScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  var hbccState = HbccState();
  var keHoachState = NcsKeHoachState();
  int dayOfMonth = 0;

  TextStyle headerStyle = const TextStyle(fontWeight: FontWeight.w600);
  TextStyle itemStyle = const TextStyle(fontWeight: FontWeight.normal, fontSize: 13);

  final MemberState memberState = MemberState();
  DateTime? monthYearFilter;
  int thangSelected = DateTime.now().month, namSelected = DateTime.now().year, memberIdSelected = 0;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
    memberState.getListMembers();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
            backgroundColor: MemberAppTheme.nearlyBlue,
            elevation: 0,
            title: const Text("Lọc dữ liệu"),
          ),
          //backgroundColor: Colors.transparent,
          body: MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => keHoachState),
              ChangeNotifierProvider(create: (_) => memberState),
            ],
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Consumer<NcsKeHoachState>(
                        builder: (context, state, child) {
                          return Row(
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    TextThemeApp.wdgTitle("Chọn tháng", isRequired: false),
                                    SizedBox(height: 8,),
                                    DropdownButtonFormField(
                                      isExpanded: true,
                                      isDense: true,
                                      value: widget.monthFiltered == null || widget.monthFiltered == 0 ? thangSelected : widget.monthFiltered,
                                      items: getListThang(),
                                      onChanged: (value) {
                                        thangSelected = int.parse(value.toString());
                                      },
                                      hint: const Text('Chọn tháng'),
                                    ),
                                  ],
                                )
                              ),
                              Expanded(
                                  child: Column(
                                    children: [
                                      TextThemeApp.wdgTitle("Chọn năm", isRequired: false),
                                      SizedBox(height: 8,),
                                      DropdownButtonFormField(
                                        isExpanded: true,
                                        isDense: true,
                                        value: widget.yearFiltered == null || widget.yearFiltered == 0 ? namSelected : widget.yearFiltered,
                                        items: getListYear(),
                                        onChanged: (value) {
                                          namSelected = int.parse(value.toString());
                                        },
                                        hint: const Text('Chọn năm'),
                                      ),
                                    ],
                                  )
                              )
                            ],
                          );
                        }
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 50, bottom: 15),
                      width: 130,
                      child: ButtonWidget(
                        height: 39,
                        bgColor: AppColors.orange,
                        ic: Icons.filter_list,
                        title: "Lọc",
                        onPress: () {
                          var ret = {
                            "thang":  thangSelected,
                            "nam":  namSelected,
                            "memberId":  memberIdSelected,
                          };
                          Navigator.pop(context, ret);
                        },
                      ),
                    ),
                  ],
                ),
              )
            )
        ),
      );
  }

  Widget? wdgMembersList(ApiResponse<List<Member>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message!,
        );
        break;
      case Status.COMPLETED:
        List<Member> list = data.data!;
        List<Member> lstMemberSelected = [];
        if(list.isNotEmpty && list.length > 0 && (widget.memberIdFiltered ?? 0) > 0) {
          lstMemberSelected = list.where((Member mb) => mb.id == widget.memberIdFiltered).toList();
        }
        return SizedBox(
          height: 48,
          child: DropdownSearch<Member>(
            mode: Mode.MENU,
            maxHeight: 400,
            isFilteredOnline: true,
            //showClearButton: true,
            //showSelectedItems: true,
            selectedItem: lstMemberSelected.isNotEmpty && (widget.memberIdFiltered ?? 0) > 0 ? lstMemberSelected[0] : Member(),
            showSearchBox: true,
            items: list,
            onFind: (String? filter) => getData(filter, list),
            dropdownSearchDecoration: const InputDecoration(
              //labelText: "Chọn ...",
              contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
              border: OutlineInputBorder(),
            ),
            onChanged: (Member? data) {
              memberIdSelected = data?.id ?? 0;
            },
            //showSearchBox: true,
            dropdownBuilder: _customDropDownExample,
            popupItemBuilder: _customPopupItemBuilderExample2,
          ),
        );
      case Status.ERROR:
        showErrorToast(context, "Có lỗi xảy ra khi nạp dữ liệu nkt");
        return SizedBox(height: 0,);
        break;
      case Status.INIT:
        return SizedBox(height: 0,);
        break;
    }
  }

  Future<List<Member>> getData(String? filter, List<Member> list) async {
    print("filter");
    print(filter);
    List<Member> tmp = list.where((element) => element.fullName!.toUpperCase().contains(filter!.toUpperCase()) ||
        element.phone!.toUpperCase().contains(filter.toUpperCase())).toList();
    return tmp;
  }

  Widget _customDropDownExample(BuildContext context, Member? item) {
    if (item == null) {
      return Container();
    }
    String name = item.fullName ?? "";
    if(item.phone?.trim() != "") {
      name = "$name - ${item.phone}";
    }
    return Container(
      child: (item.id == 0)
          ? ListTile(
        contentPadding: EdgeInsets.all(0),
        title: Text("Chọn người khuyết tật", style: itemStyle,),
      )
          : ListTile(
        contentPadding: EdgeInsets.all(0),
        title: Text("${item.fullName}", style: itemStyle),
      ),
    );
  }

  Widget _customPopupItemBuilderExample2(
      BuildContext context, Member? item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.only(left: 5, right: 5, bottom: 0, top: 0),
        selected: isSelected,
        title: Row(
          children: [
            Expanded(
              flex: 3,
              child: Text("${item?.fullName}", style: itemStyle, textAlign: TextAlign.left),
            ),
            Spacer(),
            Expanded(
              flex: 2,
              child: Text("${item?.phone}", style: itemStyle, textAlign: TextAlign.right,),
            )
          ],
        )
      ),
    );
  }

}