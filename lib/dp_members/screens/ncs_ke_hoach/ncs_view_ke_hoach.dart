import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../../../common/app_colors.dart';
import '../../../common/app_constant.dart';
import '../../../common/text_theme_app.dart';
import '../../../dp_managers/models/hbcc_ke_hoach/add_care_plan_item.dart';
import '../../../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../../../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../../../states/hbcc_state.dart';
import '../../../states/hbcc_member_ke_hoach_state.dart';
import '../../../widgets/alert_confirm.dart';
import '../../../widgets/bottom_sheet_widget.dart';
import '../../../widgets/button_widget.dart';
import '../../../widgets/text_form_field_widget.dart';
import '../../design_course_app_theme.dart';

class NcsViewKeHoach extends StatefulWidget {
  final HbccItemKeHoach? itemKeHoach;

  NcsViewKeHoach({Key? key, this.itemKeHoach}) : super(key: key);
  @override
  _NcsViewKeHoachState createState() => _NcsViewKeHoachState();
}

class _NcsViewKeHoachState extends State<NcsViewKeHoach>
    with TickerProviderStateMixin {

  final _formKey = GlobalKey<FormState>();

  //thong tin cac field
  final maKeHoachController = TextEditingController();
  final nguoiLapController = TextEditingController();
  final thoiGianLapController = TextEditingController(text: DateFormat("dd/MM/yyyy HH:mm").format(DateTime.now()));
  final nktController = TextEditingController();
  final tinhController = TextEditingController();
  final huyenController = TextEditingController();
  final xaController = TextEditingController();
  final namController = TextEditingController();
  final thangController = TextEditingController();
  final tieuDeController = TextEditingController();
  final trangThaiController = TextEditingController();
  double constMargin = 3;
  int thangSelected = DateTime.now().month;
  bool changedThang = false;
  var hbccState = HbccState();
  var keHoachState = HbccMemberKeHoachState();
  int idKeHoach = 0;

  TextStyle headerStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 13.sp);
  TextStyle itemStyle = TextStyle(fontWeight: FontWeight.w600, fontSize: 13.sp);

  @override
  void initState() {
    super.initState();
    hbccState.getHbcc();
    idKeHoach = widget.itemKeHoach?.id ?? 0;
    maKeHoachController.text = widget.itemKeHoach?.code ?? "-"; //'KH{:06d}'.format(idKeHoach);
    nktController.text = widget.itemKeHoach?.member?.fullName?.trim() ?? "";
    tinhController.text = widget.itemKeHoach?.province?.name?.trim() ?? "";
    huyenController.text = widget.itemKeHoach?.district?.name?.trim() ?? "";
    xaController.text = widget.itemKeHoach?.ward?.name?.trim() ?? "";
    namController.text = widget.itemKeHoach?.year.toString() ?? DateTime.now().year.toString();
    thangSelected = widget.itemKeHoach?.month ?? DateTime.now().month;
    tieuDeController.text = widget.itemKeHoach?.title?.trim() ?? "";
    thoiGianLapController.text = DateFormat("dd/MM/yyyy HH:mm").format(DateTime.parse(widget.itemKeHoach?.createdAt ?? DateTime.now().toString()));
    //themKeHoachState.setTieuDeKeHoach();
    trangThaiController.text = (widget.itemKeHoach?.status ?? 0) == 0 ? "Lưu Nháp" : "Xuất Bản";
    keHoachState.getListCarePlanItem(idKeHoach);
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        color: ManagerAppTheme.nearlyWhite,
        child:
        Scaffold(
          appBar: AppBar(
            backgroundColor: MemberAppTheme.nearlyBlue,
            elevation: 0,
            leading: InkWell(
              borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
              child: const Icon(
                Icons.close,
                color: Colors.white,
              ),
              onTap: () {
                Navigator.pop(context, 0);
              },
            ),
            title: const Text("Chi tiết kế hoạch"),
          ),
          backgroundColor: Colors.white,
          body: MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (_) => hbccState),
                ChangeNotifierProvider(create: (_) => keHoachState)
              ],
              child: SingleChildScrollView(
                  child: Container(
                    color: AppColors.grey60,
                    child: Column(
                      children: [
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: const EdgeInsets.only(bottom: 8),
                          padding: const EdgeInsets.all(15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              wdgItem(title: "Mã kế hoạch", value: "${maKeHoachController.text}"),
                              wdgItem(title: "Thời gian lập", value: "${thoiGianLapController.text}"),
                              Consumer<HbccState>(
                                  builder: (context, state, child) {
                                    WidgetsBinding.instance.addPostFrameCallback((_){
                                      HBCC? hbccOb = state.getHbccData();
                                      nguoiLapController.text = hbccOb?.fullName?.trim() ?? "";
                                      return wdgItem(title: "Người lập", value: "${nguoiLapController.text}");
                                    });
                                    return SizedBox(height: 0,);
                                  }
                              ),
                              wdgItem(title: "Người khuyết tật", value: "${nktController.text}"),
                              wdgItem(title: "Địa chỉ", value: chuanHoaTinhHuyenXa(tinh: tinhController.text.trim(), huyen: huyenController.text.trim(), xa: xaController.text.trim())),
                              wdgItem(title: "Trạng thái", value: widget.itemKeHoach?.status == 0 ? "Lưu nháp" : "Đã xuất bản")
                            ],
                          ),
                        ),
                        Container(
                            color: Colors.white,
                            padding: EdgeInsets.all(15),
                            child: Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    /*Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Mã kế hoạch",
                                        textEditingController: maKeHoachController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Thời gian lập",
                                        textEditingController: thoiGianLapController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              AppConstant.spaceVerticalSmallMedium,
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                        margin: EdgeInsets.only(right: constMargin),
                                        child: Consumer<HbccState>(
                                            builder: (context, state, child) {
                                              WidgetsBinding.instance.addPostFrameCallback((_){
                                                HBCC? hbccOb = state.getHbccData();
                                                nguoiLapController.text = hbccOb?.fullName?.trim() ?? "";
                                              });

                                              return TextFormFieldWidget(
                                                label: "Người lập",
                                                textEditingController: nguoiLapController,
                                                isValidator: false,
                                                onChange: (keyword) {
                                                },
                                              );
                                            }
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Người khuyết tật",
                                        textEditingController: nktController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              AppConstant.spaceVerticalSmallMedium,
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Tỉnh/Tp",
                                        textEditingController: tinhController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Quận/Huyện",
                                        textEditingController: huyenController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              AppConstant.spaceVerticalSmallMedium,
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Xã/Phường",
                                        textEditingController: xaController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(left: constMargin),
                                      child: TextFormFieldWidget(
                                        label: "Trạng thái",
                                        textEditingController: trangThaiController,
                                        isValidator: false,
                                        onChange: (keyword) {
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              AppConstant.spaceVerticalSmallMedium,*/
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.only(right: constMargin),
                                            child: IgnorePointer(
                                              ignoring: true,
                                              child: TextFormFieldWidget(
                                                label: "Năm",
                                                textEditingController: namController,
                                                isValidator: false,
                                                onChange: (keyword) {
                                                },
                                              ),
                                            )
                                          ),
                                        ),
                                        Expanded(
                                            child:
                                            Column(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                TextThemeApp.wdgTitle("Tháng", isRequired: false),
                                            const SizedBox(height: 8),
                                            Consumer<HbccMemberKeHoachState>(
                                                builder: (context, state, child) {
                                                  return IgnorePointer(
                                                    ignoring: true,
                                                    child: DropdownButtonFormField(
                                                      isExpanded: true,
                                                      isDense: true,
                                                      value: state.getThang() == 0 ? thangSelected : state.getThang(),
                                                      items: getListThang(),
                                                      onChanged: (value) {
                                                        thangSelected = int.parse(value.toString() ?? "1");
                                                        state.setTieuDeKeHoach(
                                                            nam: int.parse(namController.text.trim()),
                                                            thang: thangSelected
                                                        );
                                                      },
                                                      hint: const Text('Chọn tháng'),
                                                    ),
                                                  );
                                                }
                                            )
                                            ]
                                            )
                                        )
                                      ],
                                    ),
                                    AppConstant.spaceVerticalMediumExtra,
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          flex: 8,
                                          child: Container(
                                              margin: EdgeInsets.only(right: constMargin),
                                              child: Consumer<HbccMemberKeHoachState>(
                                                  builder: (context, state, child) {
                                                    WidgetsBinding.instance.addPostFrameCallback((_){
                                                      if((state.getTieuDeKeHoach().toString().trim() ?? "") != "") {
                                                        tieuDeController.text = state.getTieuDeKeHoach() ?? "";
                                                      }
                                                    });

                                                    return IgnorePointer(
                                                      ignoring: true,
                                                      child: TextFormFieldWidget(
                                                        label: "Tiêu đề",
                                                        textEditingController: tieuDeController,
                                                        isValidator: false,
                                                        onChange: (keyword) {
                                                        },
                                                      ),
                                                    );
                                                  }
                                              )
                                          ),
                                        ),
                                        /*Expanded(
                                          flex: 1,
                                          child: InkWell(
                                            child: Container(
                                              alignment: Alignment.center,
                                              padding: EdgeInsets.only(top: 20),
                                              child: Icon(Icons.open_in_new, size: 23.sp,),
                                            ),
                                            onTap: () {
                                              Navigator.of(context).push(MaterialPageRoute(
                                                builder: (context) => HbccListKeHoachThang(
                                                  thang: thangSelected,
                                                  nam: int.parse(namController.text.trim()),
                                                ),
                                              ));
                                            },
                                          ),
                                        ),*/
                                      ],
                                    ),
                                    AppConstant.spaceVerticalMediumExtra,
                                    //ke hoach chi tiet
                                    Row(
                                      children: [
                                        const Text(
                                          "Kế hoạch chi tiết",
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          textAlign: TextAlign.start,
                                        ),
                                      ],
                                    ),
                                    AppConstant.spaceVerticalSmallMedium,
                                    Consumer<HbccMemberKeHoachState>(
                                        builder: (context, state, child) {
                                          return Column(
                                            children: [
                                              Container(
                                                padding: const EdgeInsets.only(top: 12, bottom: 12),
                                                color: AppColors.grey60,
                                                child: Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 40.sp,
                                                      child: Text("#", textAlign: TextAlign.center, style: headerStyle,),
                                                    ),
                                                    Expanded(
                                                      child: Padding(
                                                        padding: const EdgeInsets.all(5),
                                                        child: Text("Hoạt động", style: headerStyle),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              ListView.separated(
                                                  shrinkWrap: true,
                                                  physics: const NeverScrollableScrollPhysics(),
                                                  separatorBuilder: (BuildContext context, int index){
                                                    return Divider(height: 2.sp,);
                                                  },
                                                  itemCount: state.listPlanItemModifed.length,
                                                  itemBuilder: (BuildContext context, int index) {
                                                    AddCarePlanItem item = state.listPlanItemModifed[index];
                                                    String timeValue = getHourMinite(item.itemTime?.toString() ?? "");
                                                    return
                                                      Slidable(
                                                          key: const ValueKey(
                                                              0),
                                                          endActionPane: ActionPane(
                                                            motion: const ScrollMotion(),
                                                            //dismissible: DismissiblePane(onDismissed: () {}),
                                                            children: [
                                                              SlidableAction(
                                                                // An action can be bigger than the others.
                                                                flex: 2,
                                                                onPressed: (ctx){
                                                                  showDialog(
                                                                      context: ctx,
                                                                      builder: (BuildContext context) {
                                                                        return AlertConfirmWidget(
                                                                          prefixName:  "Bạn có chắc muốn xoá ",
                                                                          middleName:  item.title ?? "",
                                                                          suffixName:  " không?",
                                                                          onPressOk: ()  {
                                                                            keHoachState.removeItemInListCarePlanItem(index);
                                                                            state.refreshUI();
                                                                            Navigator.pop(context);
                                                                          },
                                                                        );
                                                                      }
                                                                  );
                                                                },
                                                                backgroundColor: Colors.red,
                                                                foregroundColor: Colors.white,
                                                                icon: Icons.delete,
                                                                label: 'Xóa',
                                                              )
                                                            ],
                                                          ),
                                                          child:
                                                      InkWell(
                                                      child:
                                                      Padding(
                                                          padding: EdgeInsets.only(bottom: 8.sp,top: 12.sp),
                                                          child:
                                                          Row(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Container(
                                                                width: 24.sp,
                                                                height: 24.sp,
                                                                margin: EdgeInsets.only(right: 16.sp),
                                                                alignment: Alignment.center,
                                                                decoration: BoxDecoration(
                                                                    borderRadius: BorderRadius.circular(100),
                                                                    color: Colors.grey
                                                                ),
                                                                child:
                                                                Text("${index+1}", textAlign: TextAlign.center,
                                                                    style: itemStyle.copyWith(color: Colors.white)),
                                                              ),
                                                              Expanded(
                                                                child:
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    Text("${item.title}", style: itemStyle),
                                                                    AppConstant.spaceVerticalSmallExtra,
                                                                    Text("${DateFormat("dd/MM/yyyy")
                                                                        .format(DateTime.parse(item.itemDate.toString()))} "
                                                                        "$timeValue",
                                                                        style: itemStyle.copyWith(
                                                                            fontWeight: FontWeight.normal,color: Colors.grey)),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          )),
                                                      onTap: () async {
                                                        /*var result = await openBottomSheet(
                                                            context,
                                                            title: "Chi tiết kế hoạch",
                                                            child: HbccMemberAddCarePlanItemScreen(carePlanItem: item,)
                                                        );
                                                        if(result != null) {
                                                          AddCarePlanItem item = AddCarePlanItem.fromJson(json.decode(result));
                                                          keHoachState.updateListCarePlanItem(index, item);
                                                        }*/
                                                      },
                                                    ));
                                                  })
                                            ],
                                          );
                                        }
                                    ),
                                  ],
                                )
                            )
                        )
                      ],
                    ),
                  )
              )
          ),
        ),
      );
  }

  wdgItem({String? title, String? value}) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 2,
            child: Text(title ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.normal, color: Colors.black, fontSize: 12.sp)),
          ),

          Expanded(
            flex: 3,
            child: Text(value ?? "",
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: AppColors.grey, height: 1.3, fontSize: 12.sp)
            ),
          ),
        ],
      ),
    );
  }
}

