import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/district_services.dart';
import 'package:hmhapp/services/nation_services.dart';
import 'package:hmhapp/services/province_services.dart';
import 'package:hmhapp/services/ward_services.dart';
import 'package:hmhapp/states/da_cam_state.dart';
import 'package:hmhapp/states/disability_start_state.dart';
import 'package:hmhapp/states/disability_type_state.dart';
import 'package:hmhapp/states/district_state.dart';
import 'package:hmhapp/states/gender_state.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/nation_state.dart';
import 'package:hmhapp/states/personal_assistant_state.dart';
import 'package:hmhapp/states/province_state.dart';
import 'package:hmhapp/states/ward_state.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MemberInfoEditScreen extends StatefulWidget {
  final List<Province>? provinces;
  final List<District>? disticts;
  final List<Ward>? wards;
  final Member? member;
  const MemberInfoEditScreen({Key? key,this.provinces, this.disticts, this.wards, this.member}) : super(key: key);

  @override
  _MemberInfoEditScreenState createState() => _MemberInfoEditScreenState();
}

class _MemberInfoEditScreenState extends State<MemberInfoEditScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  List<Province>? provinces;
  List<District>? districts;
  List<Ward>? wards;


  var _provinceService = new ProvinceServices();
  var _districtServices = new DistrictServices();
  var _wardServices = new WardServices();
  var _nationServices = new NationServices();

  var provinceSelect = 0;
  var districtSelect = 0;
  var wardSelect = 0;
  bool isFirst = true;
  var provinceCode = "";
  var orderProvince = 0;
  Province provinceObj = new Province();



  MemberState memberState = new MemberState();
  MemberHomeState memberHomeState = new MemberHomeState();
  GenderState genderState = new GenderState();
  NationState nationState = new NationState();
  DisabilityStartState disabilityStartState = new DisabilityStartState();
  DisabilityTypeState disabilityTypeState = new DisabilityTypeState();
  ProvinceState provinceState = new ProvinceState();
  DistrictState districtState = new DistrictState();
  WardState wardState = new WardState();
  DaCamState daCamState = new DaCamState();
  PersonalAssistantState personalAssistantState = new PersonalAssistantState();
  int? currentYear, currentMonth;
  late TextEditingController hoCtrl, phoneCtrl, namSinhCtrl, diaChiCtrl, tenCtrl,namNKTBiKTCtrl,
      dantocKhacCtrl, gioiTinhKhacCtrl, khuyetTatKhacCtrl;
  DateTime? joinProjectDate;
  String joinProjectDateLabel = "";
  int? birthYear = 0;
  int? disableYear = 0;
  int? genderId = 0, disabilityTypeId = 0, nationId = 0, provinceId = 0, districtId, wardId, ncsId = 0,disabilityStartId=0;
  //String provinceCode= "", districtCode = "", wardCode = "";
  String daCamCode = "";
  int genderIdOther = 3, disabilityOtherId = 7;
  bool changeDropDown = false;

  DateTime? _projectJoinDate, _birthDate, _disabilityDate;
  var formatStr =  "dd-MM-yyyy";
  String _dateLabel = "Chọn ngày";

  List<DropdownMenuItem<int>> _nations = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn dân tộc"),
  )];

  List<DropdownMenuItem<int>> _disableTypes = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn dạng khuyết tật"),
  )];

  List<DropdownMenuItem<int>> _genders = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn giới tính"),
  )];

  List<DropdownMenuItem<int>> _tinh = [];
  var _tinhDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn tỉnh"),
  );

  var _huyenDefault = new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn huyện"),
  );
  List<DropdownMenuItem<int>> _huyen = [];
  var _xaDefault= new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn xã"),
  );
  List<DropdownMenuItem<int>> _xa = [];

  List<DropdownMenuItem<String>> _dacam = [new DropdownMenuItem<String>(
    value: "",
    child: new Text("Chọn"),
  )];

  List<DropdownMenuItem<int>> _disabilityStarts = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn "),
  )];

  List<DropdownMenuItem<int>> _ncs = [new DropdownMenuItem<int>(
    value: 0,
    child: new Text("Chọn người chăm sóc"),
  )];

  late Member member;
  List<District> listHuyenByTinh = <District>[];
  List<Ward> listXaByHuyen = <Ward>[];
  bool _changeTinh = false;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    super.initState();
    provinces = widget.provinces;
    districts = widget.disticts;
    wards = widget.wards;

    memberState.getMember();
    genderState.getListGenders();
    nationState.listNations();
    disabilityTypeState.listLoaiKT();
    daCamState.listDaCam();
    personalAssistantState.listPersonalAssistants();
    disabilityStartState.listDisabilityStarts();

    currentYear = DateTime.now().year;
    currentMonth = DateTime.now().month;

    hoCtrl = new TextEditingController();
    tenCtrl = new TextEditingController();
    phoneCtrl = new TextEditingController();
    namSinhCtrl = new TextEditingController();
    diaChiCtrl = new TextEditingController();
    namNKTBiKTCtrl = new TextEditingController();
    dantocKhacCtrl = new TextEditingController();
    gioiTinhKhacCtrl = new TextEditingController();
    khuyetTatKhacCtrl = new TextEditingController();

    setState(() {
      districtId = 0;
      wardId = 0;
    });
    districtState = new DistrictState();
    wardState = new WardState();

    districtSelect = 0;
    wardSelect = 0;
    _tinh.add(_tinhDefault);
    _huyen.add(_huyenDefault);
    _xa.add(_xaDefault);
    bindAllTinh();
    bindAllHuyen();
    bindAllXa();
    //bindAllDanToc();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }


  @override
  Widget build(BuildContext context) {
    Map<int, Widget> map = new Map();
    List<Widget> childWidgets = <Widget>[];
    childWidgets.add(wdgNKT());
    childWidgets.add(wdgNCSC());

    return
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => disabilityStartState),
            ChangeNotifierProvider(create: (_) => memberState),
            ChangeNotifierProvider(create: (_) => memberHomeState),
            ChangeNotifierProvider(create: (_) => genderState),
            ChangeNotifierProvider(create: (_) => nationState),
            ChangeNotifierProvider(create: (_) => disabilityTypeState),
            ChangeNotifierProvider(create: (_) => provinceState),
            ChangeNotifierProvider(create: (_) => districtState),
            ChangeNotifierProvider(create: (_) => wardState),
            ChangeNotifierProvider(create: (_) => daCamState),
            ChangeNotifierProvider(create: (_) => personalAssistantState)
          ],
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: MemberAppTheme.nearlyBlue,
              title: Text("Thông tin người khuyết tật"),
            ),
            backgroundColor: Colors.white,
            body:
            SingleChildScrollView(
              child: wdgNKT(),
            )
            ,
          )
      );
  }

  Widget wdgNKT() {
    return wdgNKTForm(widget.member,context);

    return Consumer<MemberState>(
        builder: (context, state, child) {
          return wdgNKTRender(state.memberResponse,context);
        }
    );
  }

  Widget wdgNKTRender(ApiResponse<Member> data,context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        return wdgNKTForm(data.data,context);
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgNKTForm(Member? mem,context) {
    member = mem ?? new Member();
    if(member!.projectJoinDate != null && member!.projectJoinDate != "") {
      _projectJoinDate = DateTime.parse(member!.projectJoinDate!);
      memberState.setProjectJoinDate(_projectJoinDate!);
    }

    if(member!.birth_date != null && member!.birth_date != "") {
      _birthDate = DateTime.parse(member!.birth_date!);
      memberState.setBirthDate(_birthDate!);
    }

    if(member!.disability_date != null && member!.disability_date != "") {
      _disabilityDate = DateTime.parse(member!.disability_date!);
      memberState.setDisabilityDate(_disabilityDate);
    }

    if(member!.firstName != null && member!.firstName != "") hoCtrl!.text = member!.firstName.toString();
    if(member!.phone != null && member!.phone != "") phoneCtrl.text = member.phone.toString();
    if(member.birth_year != null && member.birth_year != "") namSinhCtrl.text = member.birth_year.toString();
    if(member.address != null && member.address != "") diaChiCtrl.text = member.address.toString();
    if(member.lastName != null && member.lastName != "") tenCtrl.text = member.lastName.toString();

    if(member.genderId != null && member.genderId != "") genderId = member.genderId;
    if(member.genderId != null && member.genderId != "") genderId = member.genderId;
    if(member.nationId != null && member.nationId != "") nationId = member.nationId;
    if (member.nation_other != null && member.nation_other != "")
      dantocKhacCtrl.text = member.nation_other.toString();

    if(member.disabilityTypeId != null && member.disabilityTypeId != "") disabilityTypeId = member.disabilityTypeId;
    if(member.disabilityStartId != null && member.disabilityStartId != "") disabilityStartId = member.disabilityStartId;
    if(member.idNguoiChamSoc != null && member.idNguoiChamSoc != "") ncsId = member.idNguoiChamSoc;
    if(member.is_agent_orange != null) daCamCode = member.is_agent_orange.toString();
    if(member.disability_year != null && member.disability_year != "") namNKTBiKTCtrl.text = member.disability_year.toString();

    genderState.setGenderId(genderId!);
    nationState.setNationId(nationId!);
    disabilityTypeState.setloaiKtId(disabilityTypeId!);
    daCamState.setDaCamCode(daCamCode);
    personalAssistantState.setPersonalAssistantId(ncsId!);
    disabilityStartState.setDisabilityStartId(disabilityStartId!);
    provinceState.setProvinceId(member!.provinceId!);
    districtState.setDistrictId(member!.districtId!);
    wardState.setWardId(member.wardId!);

    return new Container(
      color: Color(0xffFFFFFF),
      child: Padding(
        padding: EdgeInsets.only(bottom: 25.0,left: 25, right: 25,top: 25),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Mã:',
                  style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: Text(member.code ?? ""))
              ],
            ),
            SizedBox(height: 25,),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Họ',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                      TextFormField(
                        controller: hoCtrl,
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Tên',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                      TextFormField(
                        controller: tenCtrl,
                      )
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 25,),
            new Text(
              'Thời gian bắt đầu dự án',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(2000, 1, 1),
                    maxTime: DateTime.now(), onConfirm: (date) {
                      print('confirm $date');
                      _projectJoinDate = date;
                      //setState(() {
                      //  _projectJoinDateLabel = new DateFormat(formatStr).format(_projectJoinDate).toString();
                      //});
                      memberState.setProjectJoinDate(_projectJoinDate!);
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi
                );
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Consumer<MemberState>(
                                  builder: (context, state, child) {
                                    if(memberState.getProjectJoinDate() != null)
                                      _dateLabel = DateFormat(formatStr).
                                      format(memberState.getProjectJoinDate()!).toString();
                                    else _dateLabel = "Chọn ngày";
                                    return Text(
                                      '${_dateLabel}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 15.0),
                                    );
                                  }
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
            SizedBox(height: 25,),
            const Text(
              'Số điện thoại',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            TextFormField(
              controller: phoneCtrl,
            ),
            SizedBox(height: 25,),
            new Text(
              'Giới tính',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            Consumer<GenderState>(
                builder: (context, state, child) {
                  return wdgGioiTinhRender(state.genderResponseList, context);
                }
            ),
            SizedBox(height: 25,),
            new Text(
              'Dân tộc',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            Consumer<NationState>(
                builder: (context, state, child) {
                  return wdgDanTocRender(state.nationResponseList, context)!;
                }
            ),
            SizedBox(height: 25,),
            new Text(
              'Ngày tháng năm sinh',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 5,),
            ElevatedButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    theme: DatePickerTheme(
                      containerHeight: 210.0,
                    ),
                    showTitleActions: true,
                    minTime: DateTime(1900, 1, 1),
                    maxTime: DateTime.now(),
                    onConfirm: (date) {
                      print('confirm1 $date');
                      _birthDate = date;
                      //setState(() {
                      //  _birthDateLabel = new DateFormat(formatStr).format(_birthDate).toString();
                      //});
                      memberState.setBirthDate(_birthDate!);
                    },
                    currentTime: DateTime.now(), locale: LocaleType.vi
                );
              },
              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Consumer<MemberState>(
                                  builder: (context, state, child) {
                                    if(memberState.getBirthDate() != null)
                                      _dateLabel = DateFormat(formatStr).format(memberState.getBirthDate()!).toString();
                                    else _dateLabel = "Chọn ngày";
                                    return Text(
                                      '${_dateLabel}',
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 15.0),
                                    );
                                  }
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ),
            SizedBox(height: 25,),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Năm sinh (nếu không nhớ ngày sinh)',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500
                        ),
                      ),
                      TextFormField(
                        controller: namSinhCtrl,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 25,),
            new Text(
              'Dạng khuyết tật',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            Consumer<DisabilityTypeState>(
                builder: (context, state, child) {
                  return wdgLoaiKtRender(state.disabilityTypeResponseList,context);
                }
            ),
            SizedBox(height: 25,),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Thời điểm khuyết tật',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 5,),
                      Consumer<DisabilityStartState>(
                          builder: (context, state, child) {
                            return wdgThoiDiemKTRender(state.disabilityStartResponseList,context);
                          }
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 25,),
            new Text(
              'NKT có giấy xác nhận chất độc da cam',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            Consumer<DaCamState>(
                builder: (context, state, child) {
                  return wdgDaCamRender(state.daCamResponseList,context);
                }
            ),
            SizedBox(height: 25,),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Tỉnh',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500
                        ),
                      ),
                      Consumer<ProvinceState>(
                          builder: (context, state, child) {
                            return wdgTinhRender(state.provinceResponseList,context);
                          }
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Huyện',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500
                        ),
                      ),
                      Consumer<DistrictState>(
                          builder: (context, state, child) {
                            return wdgHuyenRender(state.districtResponseList,context);
                          }
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Xã',
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500
                        ),
                      ),
                      Consumer<WardState>(
                          builder: (context, state, child) {
                            return wdgXaRender(state.wardResponseList,context);
                          }
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 25,),
            new Text(
              'Địa chỉ cụ thể',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            TextFormField(
              controller: diaChiCtrl,
            ),
            /* SizedBox(height: 25,),
            new Text(
              'Người chăm sóc',
              style: TextStyle(
                  fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
            Consumer<PersonalAssistantState>(
                builder: (context, state, child) {
                  return wdgNCSRender(state.personalAssistantResponseList,context);
                }
            ),*/
            SizedBox(height: 25,),
            InkWell(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 48,
                      decoration: BoxDecoration(
                        color: MemberAppTheme.nearlyBlue,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(16.0),
                        ),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: MemberAppTheme
                                  .nearlyBlue
                                  .withOpacity(0.5),
                              offset: const Offset(1.1, 1.1),
                              blurRadius: 10.0),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Lưu',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            letterSpacing: 0.0,
                            color:
                            ManagerAppTheme.nearlyWhite,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              onTap: () async {
                if(memberState.getProjectJoinDate() == null) {
                  showDialogValidation(context, "Thông báo", "Bạn chưa chọn thời điểm bắt đầu dự án");
                  return;
                }

                if(disabilityStartState.getDisabilityStartId() == null) {
                  showDialogValidation(context, "Thông báo", "Bạn chưa chọn thời điểm khuyết tật");
                  return;
                }

                if(memberState.getBirthDate() == null) {
                  showDialogValidation(context, "Thông báo", "Bạn chưa chọn ngày sinh");
                  return;
                } else {
                  if(memberState.getBirthDate()!.year != int.parse(namSinhCtrl.text)) {
                    showDialogValidation(context, "Thông báo", "Bạn đã nhập năm sinh không đúng!");
                    return;
                  }
                }

                var mem = member;
                mem.code = member.code;
                mem.firstName = hoCtrl.text.trim();
                mem.lastName = tenCtrl.text.trim();
                if(memberState.getProjectJoinDate() != null)
                  mem.projectJoinDate = DateFormat("yyyy-MM-dd").format(memberState.getProjectJoinDate()!).toString();
                mem.phone = phoneCtrl.text.trim();
                mem.genderId = genderState.genderId;
                mem.nationId = nationState.nationId;
                mem.nation_other = dantocKhacCtrl.text;
                mem.provinceId = provinceState.provinceId;
                mem.districtId = districtState.districtId;
                mem.wardId = wardState.wardId;
                mem.address = diaChiCtrl.text.trim();
                mem.disabilityTypeId = disabilityTypeState.getloaiKtId();
                if(memberState.getDisabilityDate() != null)
                  mem.disability_date = DateFormat("yyyy-MM-dd").format(memberState.getDisabilityDate()!).toString();
                if(memberState.getBirthDate() != null)
                  mem.birth_date = DateFormat("yyyy-MM-dd").format(memberState.getBirthDate()!).toString();

                if(memberState.getBirthDate() != null  && namSinhCtrl.text.isNotEmpty)
                  mem.birth_year = int.parse(namSinhCtrl.text.trim());
                mem.disabilityStartId = disabilityStartState.getDisabilityStartId();
                if(namNKTBiKTCtrl.text!=null && namNKTBiKTCtrl.text.isNotEmpty)
                  mem.disability_year = int.parse(namNKTBiKTCtrl.text);

                mem.is_agent_orange = daCamState.getDaCamCode().toString() == "true" ? true : false;
                mem.avatar = "";
                mem.gender_other = gioiTinhKhacCtrl.text.trim();
                //mem.idNguoiChamSoc = personalAssistantState.getPersonalAssistantId();
                await memberState.updateMember(mem, member!.id!);
                final dtRet = {"saved_nkt" : true};
                Navigator.pop(context, dtRet);
              },
            )
          ],
        ),
      ),
    );
  }

  Widget wdgNCSC() {
    return Consumer<MemberState>(
        builder: (context, state, child) {
          return wdgNCSCRender(state.personalAssistantResponse);
        }
    );
  }

  Widget wdgNCSCForm(PersonalAssistant ncs) {
    return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      'Thông tin người chăm sóc chính',
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                    ),
                    InkWell(
                      child: Icon(Icons.library_add, color: MemberAppTheme.nearlyBlue,),
                      onTap: () async {
                      },
                    )
                  ],
                ),
                  new Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: Divider(
                        color: Colors.lightBlue,
                        height: 1,
                      )),
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Họ và tên NCSC',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(ncs.name ?? ""))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Năm sinh NCSC:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(ncs.BirthYear.toString()))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Giới tính NCSC:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(ncs.gender.toString()))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Dân tộc NCSC:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: Text(ncs!.nation_other!.isNotEmpty ? ncs!.nation_other! : ncs!.nation!))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Tình trạng hôn nhân NCSC:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                        child: Text(ncs.marital_status.toString()))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Trình độ học vấn:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                        child: ncs.edu_level != null ? Text(ncs.edu_level.toString()) : Text("---")
                    )
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Nghề nghiệp chính:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 0.0, right: 25.0, top: 2.0),
                        child: Text(ncs.job_type.toString()))
                  ],
                )
            ),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Mối quan hệ của NCSC với NKT:',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                        child: Text(ncs.relationship_type!.title.toString()))
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Vai trò của NCSC trong gia đình?',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    ListView.builder (
                        shrinkWrap: true,
                        itemCount: ncs.family_roles!.length ?? 0,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                              padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                              child: Text(ncs.family_roles![index].title.toString()));
                        }
                    )
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Text(
                          'Nhà bạn có internet không?',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                )),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                child: new Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Text(ncs.is_have_internet == true ? "- Có" : "- Không")
                  ],
                )),
            Padding(
              padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text(
                    'Bạn sử dụng điện thoại di động?',
                    style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                      child: Text(ncs.is_have_phone == true ? "- Có" : "- Không")),
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0,bottom: 100),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Text(
                      'Nếu Có, Điện thoại của bạn thuộc loại nào?',
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 0, right: 25.0, top: 2.0),
                        child: Text(ncs.is_smartphone == true ? "- Điện thoại có thể tiếp cận internet" : "- Điện thoại không thể tiếp cận internet"))
                  ],
                )),
          ],
        )
    );
  }

  Widget wdgNCSCRender(ApiResponse<PersonalAssistant> data) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgNCSCForm(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return SizedBox();
    }
  }

  Widget wdgGioiTinhRender(ApiResponse<List<Gender>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:

        genderId = member!= null? genderState.getGenderId() :0;

        _genders.clear();
        _genders = [new DropdownMenuItem<int>(
          value: 0,
          child: new Text("Chọn giới tính"),
        )];
        data.data!.forEach((Gender gd) {
          _genders.add(new DropdownMenuItem<int>(
            value: gd.id,
            child: Text(gd.name ?? ""),
          ));
        });

        return DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _genders,
          value: _genders.length > 1 ? genderId : 0,
          onChanged: (int? value) {
            genderState.setGenderId(value!);
          },
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget? wdgDanTocRender(ApiResponse<List<Nation>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        _nations.clear();
        _nations = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn dân tộc"),
          )
        ];
        data.data!.forEach((Nation dt) {
          _nations.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title ?? ""),
          ));
        });

        var otherCheck = Nation.isOther(data.data!, nationState.getNationId());
        if(!otherCheck){
          dantocKhacCtrl.clear();
        }

        return Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _nations,
              value: _nations.length > 1 ? nationId : 0,
              onChanged: (int? value) {
                nationState.setNationId(value ?? 0);
              },
            ),
            otherCheck
                ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 25),
                  const Text(
                    'Dân tộc khác',
                    style: TextStyle(
                        fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  TextFormField(
                    controller: dantocKhacCtrl,
                  ),
                ],
              ),
            )
                : SizedBox(
              height: 0,
            ),
          ],
        );
        break;
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
        break;
      case Status.INIT:
        break;
    }
  }

  Widget wdgThoiDiemKTRender(ApiResponse<List<DisabilityStart>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        _disabilityStarts.clear();
        _disabilityStarts = [new DropdownMenuItem<int>(
          value: 0,
          child: new Text("Chọn thời điểm khuyết tật"),
        )];
        data.data!.forEach((DisabilityStart dt) {
          _disabilityStarts.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title),
          ));
        });


        var otherCheck = DisabilityStart.isOther(data.data!, disabilityStartState.getDisabilityStartId());
        if(!otherCheck){
          namNKTBiKTCtrl.clear();
        }

        return Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _disabilityStarts,
              value: _disabilityStarts.length > 1 ? disabilityStartId : 0,
              onChanged: (int? value) {
                disabilityStartState.setDisabilityStartId(value ?? 0);
              },
            ),
            otherCheck
                ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 25),
                  const Text(
                    'Cụ thể năm nào NKT bị khuyết tật?',
                    style: TextStyle(
                        fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    controller: namNKTBiKTCtrl,
                  ),
                ],
              ),
            )
                : SizedBox(
              height: 0,
            ),
          ],
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgLoaiKtRender(ApiResponse<List<DisabilityType>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        _disableTypes.clear();
        _disableTypes = [new DropdownMenuItem<int>(
          value: 0,
          child: new Text("Chọn dạng khuyết tật"),
        )];
        data.data!.forEach((DisabilityType dt) {
          _disableTypes.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title),
          ));
        });
        return Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _disableTypes,
              value: _disableTypes.length > 1 ? disabilityTypeId : 0,
              onChanged: (int? value) {
                disabilityTypeState.setloaiKtId(value ?? 0);
              },
            ),
            disabilityTypeState.getloaiKtId() == disabilityOtherId
                ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 25),
                  const Text(
                    'Giới tính khác',
                    style: TextStyle(
                        fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  TextFormField(controller: khuyetTatKhacCtrl),
                ],
              ),
            )
                : SizedBox(
              height: 0,
            )
          ],
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgTinhRender(ApiResponse<List<Province>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        _tinh.clear();
        _tinh = [new DropdownMenuItem<int>(
          value: 0,
          child: new Text("Chọn tỉnh"),
        )];
        data.data!.forEach((Province dt) {
          _tinh.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.name!),
          ));
        });

        return DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _tinh,
          value: provinceState.provinceId,
          onChanged: (int? value) async {
            provinceState.setProvinceId(value ?? 0);
            districtState.listDistrictsByProvince1(value ?? 0);
            districtState.setChangeProvice(true);
            wardState.listWardsByDistrict(0);
            wardState.setChangeDistrict(true);
          },
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgHuyenRender(ApiResponse<List<District>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
          size: Constants.LOADING_NOMESSAGE,
        );
        break;
      case Status.COMPLETED:
        districtId = districtState.getChangeProvice() == false ? districtState.districtId : 0;
        _huyen.clear();
        _huyen.add(_huyenDefault);
        data.data!.forEach((District dt) {
          _huyen.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.name),
          ));
        });

        var ddlHuyen = new DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            items: _huyen,
            value: districtId,
            onChanged: (int? value) async {
              districtState.setDistrictId(value ?? 0);
              await wardState.listWardsByDistrict(value ?? 0);
              wardState.setChangeDistrict(true);
            }
        );
        //wardState.listWardsByDistrict(districtId);
        return ddlHuyen;
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgXaRender(ApiResponse<List<Ward>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
          size: Constants.LOADING_NOMESSAGE,
        );
        break;
      case Status.COMPLETED:
        wardId = wardState.getChangeDistrict() == false ? wardState.wardId : 0;
        _xa.clear();
        _xa.add(_xaDefault);
        data.data!.forEach((Ward dt) {
          _xa.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.name ?? ""),
          ));
        });
        return new DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _xa,
          value: wardId,
          onChanged: (int? value) {
            wardState.setWardId(value ?? 0);
          },
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgDaCamRender(ApiResponse<List<DaCam>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        daCamCode = member!= null? daCamState.getDaCamCode() :"";
        _dacam.clear();
        _dacam = [new DropdownMenuItem<String>(
          value: "",
          child: new Text("Chọn"),
        )];
        data.data!.forEach((DaCam dt) {
          _dacam.add(new DropdownMenuItem<String>(
            value: dt.code,
            child: Text(dt.name),
          ));
        });
        return DropdownButtonFormField<String>(
          isExpanded: true,
          isDense: true,
          items: _dacam,
          value: daCamCode,
          onChanged: (String? value) {
            daCamState.setDaCamCode(value ?? "");
          },
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  Widget wdgNCSRender(ApiResponse<List<PersonalAssistant>> data, BuildContext context) {
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
        break;
      case Status.COMPLETED:
        _ncs.clear();
        _ncs = [new DropdownMenuItem<int>(
          value: 0,
          child: new Text("Chọn người chăm sóc"),
        )];
        data.data!.forEach((PersonalAssistant dt) {
          _ncs.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.name ?? ""),
          ));
        });
        return DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _ncs,
          value: _ncs.length > 1 ? ncsId : 0,
          onChanged: (int? value) {
            personalAssistantState.setPersonalAssistantId(value??0);
          },
        );
        break;
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
        break;
      case Status.INIT:
        return SizedBox();
        break;
    }
  }

  showDialogValidation(context, String title, String msg) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
          ),
          elevation: 0,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: IntrinsicWidth(
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      title,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      msg,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("OK"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },);
  }

  bindAllTinh() async{
    provinceState.bindProvinces(provinces!);
  }

  bindAllDanToc() async{

    _nationServices
        .listNations()
        .then((data) {
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.title!),
        );
        _nations.add(it);
      });
    });

  }

  bindAllHuyen() async{
    districtState.bindDistricts(districts!);
  }

  bindAllXa() async{
    wardState.bindWards(wards!);
  }

  getAllHuyen(int provinceId){
    districtSelect = 0;
    _huyen.clear();
    _huyen.add(_huyenDefault);
    wardSelect = 0;
    _xa.clear();
    _xa.add(_xaDefault);

    _districtServices
        .listDistrictsByProvince(provinceId)
        .then((data) {
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.name),
        );
        _huyen.add(it);
      });
    });

  }

  getAllXa(int districtId) async {
    wardSelect = 0;
    _xa.clear();
    _xa.add(_xaDefault);
    _wardServices
        .listWardsByDistrict(districtId)
        .then((data) {
      data.forEach((element) {
        var it = new DropdownMenuItem<int>(
          value: element.id,
          child: new Text(element.name!),
        );
        _xa.add(it);
      });
    });
  }

}
