import 'package:flutter/material.dart';
import 'package:hmhapp/common_screens/videos/home_videos_screen.dart';
import 'package:hmhapp/dp_members/bottom_navigation_view/bottom_bar_view.dart';
import 'package:hmhapp/dp_members/member_home_screen.dart';
import 'package:hmhapp/dp_members/logbook_list_view.dart';
import 'package:hmhapp/dp_members/member_info_screen.dart';
import 'package:hmhapp/models/tabIcon.dart';
import 'package:hmhapp/states/bottom_tab_state.dart';
import 'package:provider/provider.dart';

import 'ncs_ke_hoach/ncs_list_ke_hoach_screen.dart';

class MemberHomeScreen extends StatefulWidget {
  final int screenIndex;

  const MemberHomeScreen({Key? key, this.screenIndex=0}) : super(key: key);
  @override
  _MemberHomeScreenState createState() => _MemberHomeScreenState();
}

class _MemberHomeScreenState extends State<MemberHomeScreen> with TickerProviderStateMixin  {
  late AnimationController animationController;

  List<TabIcon> tabIconsList = TabIcon.tabIconsMemberList;
  late List<Widget> _screens;
  Widget tabBody = Container(
    color: Colors.grey,
  );

  @override
  void initState() {
    for (var tab in tabIconsList) {
      tab.isSelected = false;
    }
    tabIconsList[widget.screenIndex].isSelected = true;

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    super.initState();

    _screens = <Widget>[];
    _screens.add(HomeMemberScreen());
    _screens.add(const LogBookListView());
    _screens.add(HomeVideosScreen());
    _screens.add(NcsListKeHoachScreen());
    _screens.add(MemberInfoScreen());
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      ChangeNotifierProvider(
          create: (_) => BottomTabState(),
          child:
          Container(
              color: Colors.white,
              child: Scaffold(
                backgroundColor: Colors.transparent,
                body:
                Consumer<BottomTabState>(
                    builder: (context, state, child) {
                      return Stack(
                        children: <Widget>[
                          _screens[state.screenIndex],
                          bottomBar(state),
                        ],
                      );
                    }),
              )
          ));
  }

  Widget bottomBar(BottomTabState state) {
    return Column(
      children: <Widget>[
        const Expanded(
          child: SizedBox(),
        ),
        BottomBarView(
          selectIndex: state.screenIndex,
          tabIconsList: tabIconsList,
          addClick: () {},
          changeIndex: (int index) {
            if (!mounted) {
              return;
            }
            state.setScreenIndex(index);
          },
        ),
      ],
    );
  }
}

