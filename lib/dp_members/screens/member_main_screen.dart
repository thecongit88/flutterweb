import 'package:hmhapp/common_screens/about_us_screen.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/custom_drawer/drawer_user_controller.dart';
import 'package:hmhapp/custom_drawer/home_drawer.dart';
import 'package:hmhapp/dp_managers/screens/manager_tab_screen.dart';
import 'package:hmhapp/common_screens/feedback_screen.dart';
import 'package:hmhapp/common_screens/help_screen.dart';
import 'package:hmhapp/common_screens/invite_friend_screen.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/widgets/manager_home_view.dart';
import 'package:hmhapp/dp_members/screens/member_tab_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberMainScreen extends StatefulWidget {
  @override
  _MemberMainScreenState createState() => _MemberMainScreenState();

}

class _MemberMainScreenState extends State<MemberMainScreen> {
  late Widget screenView;
  late DrawerIndex drawerIndex;
  late AnimationController sliderAnimationController;

  Future<String?> getLoginUser() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('accessToken');
  }

  @override
  void initState() {
    drawerIndex = DrawerIndex.HOME;
    screenView = MemberHomeScreen();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          backgroundColor: Colors.white,
          body:
          DrawerUserController(
            screenIndex: drawerIndex ?? DrawerIndex.HOME,
            drawerWidth: MediaQuery.of(context).size.width * 0.55,
            onDrawerCall: (DrawerIndex drawerIndexdata) {
              changeIndex(drawerIndexdata);
            },
            screenView: screenView,
          ),
        ),
      ),
    );
  }

  void changeIndex(DrawerIndex drawerIndexdata) {
    if (drawerIndex != drawerIndexdata) {
      drawerIndex = drawerIndexdata;
      if (drawerIndex == DrawerIndex.HOME) {
        setState(() {
          screenView = MemberHomeScreen();
        });
      } else if (drawerIndex == DrawerIndex.Help) {
        setState(() {
          screenView = HelpScreen();
        });
      } else if (drawerIndex == DrawerIndex.FeedBack) {
        setState(() {
          screenView = FeedbackScreen();
        });
      } else if (drawerIndex == DrawerIndex.Invite) {
        setState(() {
          screenView = InviteFriend();
        });
      } else if(drawerIndex == DrawerIndex.About){
        setState(() {
          screenView = AboutUsScreen();
        });
      }
    }
  }
}
