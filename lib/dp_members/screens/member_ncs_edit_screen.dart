import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/models/edu_level.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/job_type.dart';
import 'package:hmhapp/models/marital_status.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/models/relationship_type.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/edu_level_state.dart';
import 'package:hmhapp/states/gender_state.dart';
import 'package:hmhapp/states/have_internet_state.dart';
import 'package:hmhapp/states/have_phone_state.dart';
import 'package:hmhapp/states/is_smartphone_state.dart';
import 'package:hmhapp/states/job_type_state.dart';
import 'package:hmhapp/states/marital_status_state.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/states/nation_state.dart';
import 'package:hmhapp/states/personal_assistant_state.dart';
import 'package:hmhapp/states/relationship_type_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MemberNCSEditScreen extends StatefulWidget {
  final personalAssistantId;

  MemberNCSEditScreen({this.personalAssistantId});

  @override
  _MemberNCSEditScreenState createState() => _MemberNCSEditScreenState();
}

class _MemberNCSEditScreenState extends State<MemberNCSEditScreen>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  PersonalAssistant personalAssistant = new PersonalAssistant();

  MemberState memberState = new MemberState();
  MemberHomeState memberHomeState = new MemberHomeState();
  PersonalAssistantState personalAssistantState = new PersonalAssistantState();
  GenderState genderState = new GenderState();
  NationState nationState = new NationState();
  EduLevelState eduLevelState = new EduLevelState();
  JobTypeState jobTypeState = new JobTypeState();
  MaritalStatusState maritalStatusState = new MaritalStatusState();
  RelationshipTypeState relationshipTypeState = new RelationshipTypeState();
  HaveInternetState haveInternetState = new HaveInternetState();
  IsSmartphoneState isSmartphoneState = new IsSmartphoneState();
  HavePhoneState havePhoneState = new HavePhoneState();

  int maritalStatusLast = 7, genderIdOther = 3;
  bool vaitro01 = false;
  late int currentYear, currentMonth;
  late TextEditingController hotenCtrl,
      soTuoiCtl,
      dantocKhacCtrl,
      tinhTrangHonNhanKhacCtrl,
      ngheNghiepKhacCtrl,
      trinhDoHocVanKhacCtrl,
      moiQuanHeKhacCuaNCSCVoiNKTCtrl,
      gioiTinhKhacCtrl
  ;
  var formatStr = "dd-MM-yyyy";
  String _dateLabel = "Chọn ngày";

  List<DropdownMenuItem<int>> _nations = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn dân tộc"),
    )
  ];

  List<DropdownMenuItem<int>> _genders = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn giới tính"),
    )
  ];

  List<DropdownMenuItem<int>> _jobTypes = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn loại công việc"),
    )
  ];

  List<DropdownMenuItem<int>> _eduLevels = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn trình độ học vấn"),
    )
  ];

  List<DropdownMenuItem<int>> _maritalStatus = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn tình trạng hôn nhân"),
    )
  ];

  List<DropdownMenuItem<int>> _relationshipTypes = [
    new DropdownMenuItem<int>(
      value: 0,
      child: new Text("Chọn mối quan hệ"),
    )
  ];

  late TextEditingController hoCtrl, namSinhCtrl, tenCtrl;
  DateTime? _birthDate;

  late int genderId = 0,
      nationId = 0,
      eduLevelId = 0,
      jobTypeId = 0,
      maritalStatusId = 0,
      relationshipTypeId;
  bool? isSmartphoneCode, isHaveInternetCode, isHavePhoneCode;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();

    hoCtrl = new TextEditingController();
    tenCtrl = new TextEditingController();
    namSinhCtrl = new TextEditingController();
    ngheNghiepKhacCtrl = new TextEditingController();
    dantocKhacCtrl = new TextEditingController();
    moiQuanHeKhacCuaNCSCVoiNKTCtrl = new TextEditingController();
    tinhTrangHonNhanKhacCtrl = new TextEditingController();
    gioiTinhKhacCtrl = new TextEditingController();

    super.initState();

    memberState.getPersonalAssistant(widget.personalAssistantId);
    currentYear = DateTime.now().year;
    currentMonth = DateTime.now().month;

    genderState.getListGenders();
    nationState.listNations();
    eduLevelState.listEduLevels();
    jobTypeState.listJobTypes();
    maritalStatusState.listMaritalStatus();
    relationshipTypeState.listRelationshipType();
    havePhoneState.listHavePhones();
    haveInternetState.listHaveInternet();
    isSmartphoneState.listIsSmartphones();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => memberState),
          ChangeNotifierProvider(create: (_) => memberHomeState),
          ChangeNotifierProvider(create: (_) => personalAssistantState),
          ChangeNotifierProvider(create: (_) => genderState),
          ChangeNotifierProvider(create: (_) => nationState),
          ChangeNotifierProvider(create: (_) => eduLevelState),
          ChangeNotifierProvider(create: (_) => jobTypeState),
          ChangeNotifierProvider(create: (_) => maritalStatusState),
          ChangeNotifierProvider(create: (_) => relationshipTypeState),
          ChangeNotifierProvider(create: (_) => haveInternetState),
          ChangeNotifierProvider(create: (_) => havePhoneState),
          ChangeNotifierProvider(create: (_) => isSmartphoneState)
        ],
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MemberAppTheme.nearlyBlue,
            title: Text("Thông tin người chăm sóc"),
          ),
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: wdgNCSC(),
          ),
        ));
  }

  Widget wdgNCSC() {
    return Consumer<MemberState>(builder: (context, state, child) {
      return wdgNCSCRender(state.personalAssistantResponse, context);
    });
  }

  Widget wdgNCSCForm(PersonalAssistant ncs, context) {
    personalAssistant = ncs;
    if (ncs.firstName != null && ncs.firstName != "")
      hoCtrl.text = ncs.firstName.toString();
    if (ncs.lastName != null && ncs.lastName != "")
      tenCtrl.text = ncs.lastName.toString();
    if (ncs.nation_other != null && ncs.nation_other != "")
      dantocKhacCtrl.text = ncs.nation_other.toString();
    if (ncs.marital_status_other != null && ncs.marital_status_other != "")
      tinhTrangHonNhanKhacCtrl.text = ncs.marital_status_other.toString();
    if (ncs.job_type_other != null && ncs.job_type_other != "")
      ngheNghiepKhacCtrl.text = ncs.job_type_other.toString();
    if (ncs.relationship_type_other != null &&
        ncs.relationship_type_other != "")
      moiQuanHeKhacCuaNCSCVoiNKTCtrl.text =
          ncs.relationship_type_other.toString();
    if (ncs.BirthYear != null) namSinhCtrl.text = ncs.BirthYear.toString();
    if (ncs.genderId != null && ncs.genderId != "") genderId = ncs.genderId!;
    if (ncs.nationId != null && ncs.nationId != "") nationId = ncs.nationId!;
    if (ncs.edu_level_id != null && ncs.edu_level_id != "")
      eduLevelId = ncs.edu_level_id!;
    if (ncs.job_type_id != null && ncs.job_type_id != "")
      jobTypeId = ncs.job_type_id!;
    if (ncs.marital_status_id != null && ncs.marital_status_id != "")
      maritalStatusId = ncs.marital_status_id!;
    if (ncs.relationship_type != null && ncs.relationship_type != "")
      relationshipTypeId = ncs.relationship_type!.id!;
    isSmartphoneCode = ncs.is_smartphone != null && ncs.is_smartphone != ""
        ? ncs.is_smartphone
        : false;
    isHavePhoneCode = ncs.is_have_phone != null && ncs.is_have_phone != ""
        ? ncs.is_have_phone
        : false;
    isHaveInternetCode =
        ncs.is_have_internet != null && ncs.is_have_internet != ""
            ? ncs.is_have_internet
            : false;
    ncs.vaitro_ncsc_chichamsoc_nkt = ncs.vaitro_ncsc_chichamsoc_nkt != null
        ? ncs.vaitro_ncsc_chichamsoc_nkt
        : false;
    ncs.vaitro_ncsc_laodong_taothunhap =
        ncs.vaitro_ncsc_laodong_taothunhap != null
            ? ncs.vaitro_ncsc_laodong_taothunhap
            : false;
    ncs.vaitro_ncsc_nguoics_cacthanhvien =
        ncs.vaitro_ncsc_nguoics_cacthanhvien != null
            ? ncs.vaitro_ncsc_nguoics_cacthanhvien
            : false;

    if (ncs.birth_date != null && ncs.birth_date != "") {
      _birthDate = DateTime.parse(ncs.birth_date!);
      personalAssistantState.setBirthDate(_birthDate!);
    }
    personalAssistantState
        .setvaitro_ncsc_chichamsoc_nkt(ncs.vaitro_ncsc_chichamsoc_nkt);
    personalAssistantState.setvaitro_ncsc_nguoics_cacthanhvien(
        ncs.vaitro_ncsc_nguoics_cacthanhvien);
    personalAssistantState
        .setvaitro_ncsc_laodong_taothunhap(ncs.vaitro_ncsc_laodong_taothunhap);
    genderState.setGenderId(genderId);
    nationState.setNationId(nationId);
    eduLevelState.setEduLevelId(eduLevelId);
    jobTypeState.setJobTypeId(jobTypeId);
    maritalStatusState.setMaritalStatusId(maritalStatusId);
    relationshipTypeState.setRelationshipTypeId(relationshipTypeId);
    haveInternetState.setHaveInternetCode(isHaveInternetCode!);
    havePhoneState.setHavePhoneCode(isHavePhoneCode!);
    havePhoneState.setIsSmartphoneCode(isSmartphoneCode!);

    return SingleChildScrollView(
        child: Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Họ',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(
                      controller: hoCtrl,
                    )
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Tên',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(
                      controller: tenCtrl,
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 25),
          Text(
            'Ngày sinh',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 5,
          ),
          ElevatedButton(
            onPressed: () {
              DatePicker.showDatePicker(context,
                  theme: DatePickerTheme(
                    containerHeight: 210.0,
                  ),
                  showTitleActions: true,
                  minTime: DateTime(1930, 1, 1),
                  maxTime: DateTime.now(), onConfirm: (date) {
                print('confirm $date');
                _birthDate = date;
                personalAssistantState.setBirthDate(_birthDate!);
              }, currentTime: DateTime.now(), locale: LocaleType.vi);
            },
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.date_range,
                              size: 18.0,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Consumer<PersonalAssistantState>(
                                builder: (context, state, child) {
                              if (personalAssistantState.getBirthDate() != null)
                                _dateLabel = DateFormat(formatStr)
                                    .format(
                                        personalAssistantState.getBirthDate()!)
                                    .toString();
                              return Text(
                                '${_dateLabel}',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              );
                            })
                          ],
                        ),
                      )
                    ],
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ),
          SizedBox(height: 25),
          new Text(
            'Năm sinh',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          TextFormField(
            controller: namSinhCtrl,
          ),
          SizedBox(height: 25),
          Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Giới tính',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    Consumer<GenderState>(builder: (context, state, child) {
                      return wdgGioiTinhRender(
                          state.genderResponseList, context);
                    }),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 25),
          const Text(
            'Dân tộc',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<NationState>(builder: (context, state, child) {
            return wdgDanTocRender(state.nationResponseList, context);
          }),
          SizedBox(height: 25),
          const Text(
            'Tình trạng hôn nhân',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<MaritalStatusState>(builder: (context, state, child) {
            return wdgHonNhanRender(state.maritalResponseList, context);
          }),
          SizedBox(height: 25),
          const Text(
            'Trình độ học vấn cao nhất mà NCSC đã hoàn thành',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<EduLevelState>(builder: (context, state, child) {
            return wdgEduLevelRender(state.eduLevelsResponseList, context);
          }),
          SizedBox(height: 25),
          const Text(
            'Nghề nghiệp chính của NCSC hiện tại',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<JobTypeState>(builder: (context, state, child) {
            return wdgJobTypeRender(state.jobTypeResponseList, context);
          }),
          SizedBox(height: 25),
          const Text(
            'Mối quan hệ của NCSC với NKT?',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<RelationshipTypeState>(builder: (context, state, child) {
            return wdgMoiQuanHeRender(
                state.relationshipTypeResponseList, context);
          }),
          SizedBox(
            height: 25,
          ),
          const Text(
            'Vai trò của NCSC trong gia đình? (Câu hỏi nhiều lựa chọn)',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Consumer<PersonalAssistantState>(
                  builder: (context, state, child) {
                return Row(children: <Widget>[
                  Checkbox(
                    value: state.getvaitro_ncsc_chichamsoc_nkt(),
                    onChanged: (val) {
                      state.setvaitro_ncsc_chichamsoc_nkt(val);
                    },
                  ),
                  Text("Chỉ chăm sóc NKT")
                ]);
              }),
              SizedBox(
                width: 20,
              ),
              Consumer<PersonalAssistantState>(
                  builder: (context, state, child) {
                return Row(children: <Widget>[
                  Checkbox(
                    value: state.getvaitro_ncsc_laodong_taothunhap(),
                    onChanged: (val) {
                      state.setvaitro_ncsc_laodong_taothunhap(val);
                    },
                  ),
                  const Text("Là người lao động tạo thu nhập cho gia đình")
                ]);
              }),
              Consumer<PersonalAssistantState>(
                  builder: (context, state, child) {
                return Row(children: <Widget>[
                  Checkbox(
                    value: state.getvaitro_ncsc_nguoics_cacthanhvien(),
                    onChanged: (val) {
                      state.setvaitro_ncsc_nguoics_cacthanhvien(val);
                    },
                  ),
                  Expanded(
                    child: const Text(
                        "Là người chăm sóc các thành viên trong gia đình, trong đó có NKT"),
                  ),
                ]);
              })
            ],
          ),
          SizedBox(height: 25),
          const Text(
            'Nhà anh/chị có đường truyền internet không?',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<HaveInternetState>(builder: (context, state, child) {
            isHaveInternetCode = state.getHaveInternetCode();
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(children: <Widget>[
                  Radio(
                    groupValue: isHaveInternetCode,
                    value: true,
                    onChanged: (bool? val) {
                      isHaveInternetCode = val;
                      state.setHaveInternetCode(val ?? false);
                    },
                  ),
                  const Text("Có")
                ]),
                SizedBox(
                  width: 20,
                ),
                Row(children: <Widget>[
                  Radio(
                    groupValue: isHaveInternetCode,
                    value: false,
                    onChanged: (bool? val) {
                      isHaveInternetCode = val;
                      state.setHaveInternetCode(val ?? false);
                    },
                  ),
                  const Text("Không")
                ]),
              ],
            );
          }),
          SizedBox(
            height: 25,
          ),
          const Text(
            'Anh/chị có sử dụng điện thoại di động không?',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
          ),
          Consumer<HavePhoneState>(builder: (context, state, child) {
            isHavePhoneCode = state.getHavePhoneCode();
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(children: <Widget>[
                  Radio(
                    groupValue: isHavePhoneCode,
                    value: true,
                    onChanged: (bool? val) {
                      isHavePhoneCode = val;
                      state.setHavePhoneCode(val ?? false);
                    },
                  ),
                  const Text("Có")
                ]),
                SizedBox(
                  width: 20,
                ),
                Row(children: <Widget>[
                  Radio(
                    groupValue: isHavePhoneCode,
                    value: false,
                    onChanged: (bool? val) {
                      isHavePhoneCode = val;
                      state.setHavePhoneCode(val ?? false);
                    },
                  ),
                  const Text("Không")
                ]),
              ],
            );
          }),
          SizedBox(
            height: 25,
          ),
          Consumer<HavePhoneState>(builder: (context, state, child) {
            isHavePhoneCode = state.getHavePhoneCode();
            isSmartphoneCode = state.getIsSmartphoneCode();
            return isHavePhoneCode == true
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Text(
                        'Nếu có, điện thoại của anh/chị thuộc loại nào?',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                      Row(children: <Widget>[
                        Radio(
                          groupValue: isSmartphoneCode,
                          value: true,
                          onChanged: (bool? val) {
                            isSmartphoneCode = val;
                            state.setIsSmartphoneCode(val ?? false);
                          },
                        ),
                        const Text("Điện thoại có thể tiếp cận internet")
                      ]),
                      SizedBox(
                        width: 20,
                      ),
                      Row(children: <Widget>[
                        Radio(
                          groupValue: isSmartphoneCode,
                          value: false,
                          onChanged: (bool? val) {
                            isSmartphoneCode = val;
                            state.setIsSmartphoneCode(val ?? false);
                          },
                        ),
                        const Text("Điện thoại không tiếp cận được internet")
                      ]),
                    ],
                  )
                : SizedBox(
                    height: 0,
                  );
          }),
          SizedBox(
            height: 25,
          ),
          InkWell(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 48,
                    decoration: BoxDecoration(
                      color: ManagerAppTheme.nearLightBlue,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16.0),
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color:
                                ManagerAppTheme.nearLightBlue.withOpacity(0.5),
                            offset: const Offset(1.1, 1.1),
                            blurRadius: 10.0),
                      ],
                    ),
                    child: Center(
                      child: const Text(
                        'Lưu',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          letterSpacing: 0.0,
                          color: ManagerAppTheme.nearlyWhite,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            onTap: () async {
              if (personalAssistantState.getBirthDate() == null) {
                showDialogValidation(
                    context, "Thông báo", "Bạn chưa chọn ngày sinh");
                return;
              } else {
                if(personalAssistantState.getBirthDate()!.year != int.parse(namSinhCtrl.text)) {
                  showDialogValidation(context, "Thông báo", "Bạn đã nhập năm sinh không đúng!");
                  return;
                }
              }

              var persistant = personalAssistant;
              persistant.firstName = hoCtrl.text.trim();
              persistant.lastName = tenCtrl.text.trim();
              if (personalAssistantState.getBirthDate() != null)
                persistant.birth_date = DateFormat("yyyy-MM-dd")
                    .format(personalAssistantState.getBirthDate()!)
                    .toString();
              persistant.BirthYear = int.parse(namSinhCtrl.text.trim());
              persistant.genderId = genderState.getGenderId();
              persistant.nationId = nationState.getNationId();
              persistant.nation_other = dantocKhacCtrl.text.trim();
              persistant.edu_level_id = eduLevelState.getEduLevelId();
              persistant.job_type_id = jobTypeState.getJobTypeId();
              persistant.job_type_other = ngheNghiepKhacCtrl.text.trim();
              persistant.marital_status_id =
                  maritalStatusState.getMaritalStatusId();
              persistant.marital_status_other =
                  tinhTrangHonNhanKhacCtrl.text.trim();
              persistant.relationship_type_id =
                  relationshipTypeState.getRelationshipTypeId();
              persistant.relationship_type_other =
                  moiQuanHeKhacCuaNCSCVoiNKTCtrl.text.trim();
              persistant.gender_other = gioiTinhKhacCtrl.text.trim();
              persistant.is_have_internet =
                  haveInternetState.getHaveInternetCode().toString() == "true"
                      ? true
                      : false;
              persistant.is_have_phone =
                  havePhoneState.getHavePhoneCode().toString() == "true"
                      ? true
                      : false;
              persistant.is_smartphone =
              havePhoneState.getIsSmartphoneCode().toString() == "true"
                      ? true
                      : false;
              persistant.vaitro_ncsc_nguoics_cacthanhvien =
                  personalAssistantState.getvaitro_ncsc_nguoics_cacthanhvien();
              persistant.vaitro_ncsc_chichamsoc_nkt =
                  personalAssistantState.getvaitro_ncsc_chichamsoc_nkt();
              persistant.vaitro_ncsc_laodong_taothunhap =
                  personalAssistantState.getvaitro_ncsc_laodong_taothunhap();
              await personalAssistantState.updatePersonalAssistant(
                  persistant, widget.personalAssistantId);
              final dtRet = {"saved_ncs": true};
              Navigator.pop(context, dtRet);
            },
          )
        ],
      ),
    ));
  }

  Widget wdgNCSCRender(ApiResponse<PersonalAssistant> data, context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return wdgNCSCForm(data.data!, context);
       case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgGioiTinhRender(
      ApiResponse<List<Gender>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _genders.clear();
        _genders = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn giới tính"),
          )
        ];
        data.data!.forEach((Gender gd) {
          _genders.add(new DropdownMenuItem<int>(
            value: gd.id,
            child: Text(gd.name ?? ""),
          ));
        });

        return DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _genders,
          value: _genders.length > 1 ? genderId : 0,
          onChanged: (int? value) {
            genderState.setGenderId(value ?? 0);
          },
        );
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgDanTocRender(ApiResponse<List<Nation>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _nations.clear();
        _nations = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn dân tộc"),
          )
        ];
        data.data!.forEach((Nation dt) {
          _nations.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title ?? ""),
          ));
        });

        return Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _nations,
              value: _nations.length > 1 ? nationId : 0,
              onChanged: (value) {
                nationState.setNationId(value ?? 0);
              },
            ),
            Nation.isOther(data.data!, nationState.getNationId())
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 25),
                        const Text(
                          'Dân tộc khác',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500),
                        ),
                        TextFormField(
                          controller: dantocKhacCtrl,
                        ),
                      ],
                    ),
                  )
                : SizedBox(
                    height: 0,
                  ),
          ],
        );
        break;
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgEduLevelRender(
      ApiResponse<List<EduLevel>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _eduLevels.clear();
        _eduLevels = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn trình độ học vấn"),
          )
        ];
        data.data!.forEach((EduLevel dt) {
          _eduLevels.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title),
          ));
        });
        return DropdownButtonFormField<int>(
          isExpanded: true,
          isDense: true,
          items: _eduLevels,
          value: _eduLevels.length > 1 ? eduLevelId : 0,
          onChanged: (int? value) {
            eduLevelState.setEduLevelId(value ?? 0);
          },
        );
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgJobTypeRender(
      ApiResponse<List<JobType>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _jobTypes.clear();
        _jobTypes = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn công việc"),
          )
        ];
        data.data!.forEach((JobType dt) {
          _jobTypes.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title ?? ""),
          ));
        });
        return Column(children: <Widget>[
          DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            items: _jobTypes,
            value: _jobTypes.length > 1 ? jobTypeId : 0,
            onChanged: (int? value) {
              jobTypeState.setJobTypeId(value ?? 0);
            },
          ),
          JobType.isOther(data.data!, jobTypeState.getJobTypeId())
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 25),
                    const Text(
                      'Nghề nghiệp khác của NCSC là gì?',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(
                      controller: ngheNghiepKhacCtrl,
                    ),
                  ],
                )
              : SizedBox(
                  height: 0,
                )
        ]);
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgHonNhanRender(
      ApiResponse<List<MaritalStatus>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _maritalStatus.clear();
        _maritalStatus = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn tình trạng hôn nhân"),
          )
        ];
        data.data!.forEach((MaritalStatus dt) {
          _maritalStatus.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title ?? ""),
          ));
        });
        return Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              isExpanded: true,
              isDense: true,
              items: _maritalStatus,
              value: _maritalStatus.length > 1 ? maritalStatusId : 0,
              onChanged: (int? value) {
                maritalStatusState.setMaritalStatusId(value ?? 0);
              },
            ),
            maritalStatusState.getMaritalStatusId() == maritalStatusLast
                ? Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 25),
                        const Text(
                          'Tình trạng hôn nhân khác',
                          style: TextStyle(
                              fontSize: 16.0, fontWeight: FontWeight.w500),
                        ),
                        TextFormField(controller: tinhTrangHonNhanKhacCtrl),
                      ],
                    ),
                  )
                : SizedBox(
                    height: 0,
                  ),
          ],
        );
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  Widget wdgMoiQuanHeRender(
      ApiResponse<List<RelationshipType>> data, BuildContext context) {
    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        _relationshipTypes.clear();
        _relationshipTypes = [
          new DropdownMenuItem<int>(
            value: 0,
            child: const Text("Chọn mối quan hệ"),
          )
        ];
        data.data!.forEach((RelationshipType dt) {
          _relationshipTypes.add(new DropdownMenuItem<int>(
            value: dt.id,
            child: Text(dt.title ?? ""),
          ));
        });

        return Column(children: <Widget>[
          DropdownButtonFormField<int>(
            isExpanded: true,
            isDense: true,
            items: _relationshipTypes,
            value: _relationshipTypes.length > 1 ? relationshipTypeId : 0,
            onChanged: (int? value) {
              relationshipTypeState.setRelationshipTypeId(value ?? 0);
            },
          ),
          RelationshipType.isOther(
                  data.data!, relationshipTypeState.getRelationshipTypeId())
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 25),
                    const Text(
                      'Mối quan hệ khác của NCSC với NKT là?',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    TextFormField(controller: moiQuanHeKhacCuaNCSCVoiNKTCtrl),
                  ],
                )
              : SizedBox(
                  height: 0,
                )
        ]);
      case Status.ERROR:
        return ErrorMessage(errorMessage: data.message);
      case Status.INIT:
        return SizedBox.shrink();
    }
  }

  showDialogValidation(context, String title, String msg) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
          ),
          elevation: 0,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            child: IntrinsicWidth(
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      title,
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      msg,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("OK"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  bool isLast(List<JobType> items, int id) {
    if ((items.singleWhere((it) => it.id == id && it.code == 99)) !=
        null) {
      return true;
    } else {
      return false;
    }
  }
}
