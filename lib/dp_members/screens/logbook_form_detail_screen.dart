import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/widgets/member_logbook_form_detail_view.dart';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/states/form_item_logbook_state.dart';
import 'package:hmhapp/states/forms/form_survey_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/utils/json_schema.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class MemberLogBookFormDetailScreen extends StatefulWidget {
  final FormItemLogBook? formItemLogBook;

  const MemberLogBookFormDetailScreen({
    Key? key,
    this.formItemLogBook
  }) : super(key: key);
  @override
  _MemberLogBookFormDetailScreenState createState() => _MemberLogBookFormDetailScreenState();
}

class _MemberLogBookFormDetailScreenState extends State<MemberLogBookFormDetailScreen>
    with TickerProviderStateMixin {

  dynamic response;
  Map? forma ;

  final double infoHeight = 364.0;
  late AnimationController animationController;
  late Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;

  DateTime? _formDate;
  var formatStr =  "yyyy-MM-dd";
  int? _formUser = 0;
  String _dateLabel = "Chọn ngày";
  int _tuAnUongScore = 1;
  int _tuDanhRangScore = 1;
  int _tuTamScore = 1;
  int _tuMacQuanAoScore = 1;
  int _tuDaiTieuTienScore = 1;
  int _tuLenGiuongScore = 1;
  int _kiemSoatTieuTienScore = 1;
  int _tongDiem = 1;
  int _level = 1;
  String _ketLuan = "";
  List<Color> colors = [Colors.green,Colors.blue,Colors.deepPurple, Colors.amber, Colors.red];
  List<IconData> icons = [Icons.check_circle,Icons.info,Icons.adjust, Icons.remove_circle, Icons.cancel];

  String formSurveyData = "", memberName = "Chưa gán NKT";

  MemberServices _memberService = new MemberServices();
  DateTime? formSurveyDate;
  FormSurveyState _formSurveyState = new FormSurveyState();
  FormItemLogbookState _formItemLogbookState = new FormItemLogbookState();
  ReportTanSuatNCSState _reportTanSuatNCSState = new ReportTanSuatNCSState();

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));

    _formDate = DateTime.now();
    _formUser = widget.formItemLogBook!.memberId != null ? widget.formItemLogBook!.memberId : 0;
    calScore();
    _dateLabel = new DateFormat(formatStr).format(_formDate!).toString();

    formSurveyDate = DateTime.now();

    _memberService.getMemberById(widget.formItemLogBook!.memberId).then((Member _member) {
      setState(() {
        memberName = _member!.name!;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return
      ChangeNotifierProvider(
          create: (_) => _formSurveyState,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: theme.primaryColor,
            leading: InkWell(
              borderRadius: BorderRadius.circular(AppBar().preferredSize.height),
              child: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            title: Text(widget.formItemLogBook!.formType!.formName!),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: InkWell(
                  child: Icon(Icons.delete),
                  onTap: () async {
                    //lấy 1 bản ghi từ bảng FormItems
                    await _formItemLogbookState.getFormItemsLogBook(form_date: widget.formItemLogBook!.formDate,
                        formId: widget.formItemLogBook!.formId,
                        memId: widget.formItemLogBook!.memberId, is_ncs: true);
                    List<FormItemLogBook> formItems = _formItemLogbookState.formItems;
                    if(formItems.length > 0) {
                      //xóa bản ghi ở bảng FormItems
                      await _formItemLogbookState.delete(formItems[0].id!); //formItemId
                      //xóa bản ghi ở bảng Form_ncs_logbook_months/Form_ncs_logbook_days
                      await _formSurveyState.delete(widget.formItemLogBook!.formId!,
                          widget.formItemLogBook!.formType!.formObject!);
                      //trừ đi 1 đơn vị cho Tansuat_thuchien trong bảng Report_tansuat_ncs
                      DateTime _date = DateFormat("yyyy-MM-dd").parse(widget.formItemLogBook!.formDate!);
                      await _reportTanSuatNCSState.getListReportTanSuatNCS(month: _date.month, year: _date.year,
                          mId: widget.formItemLogBook!.memberId!);
                      List<ReportTanSuatNCS> repTanSuatNCS = _reportTanSuatNCSState.repTanSuatNCS;
                      var itemReportTanSuatNCS = new ReportTanSuatNCS();
                      if(repTanSuatNCS.length > 0) {
                        itemReportTanSuatNCS.id = repTanSuatNCS[0].id;
                        itemReportTanSuatNCS.month = repTanSuatNCS[0].month;
                        itemReportTanSuatNCS.year = repTanSuatNCS[0].year;
                        itemReportTanSuatNCS.tansuat_thuchien = repTanSuatNCS[0].tansuat_thuchien! > 0 ?
                        repTanSuatNCS[0].tansuat_thuchien! - 1 : 0;
                        itemReportTanSuatNCS.tansuat_kehoach = repTanSuatNCS[0].tansuat_kehoach;
                        itemReportTanSuatNCS.memberId = repTanSuatNCS[0].memberId;
                        await _reportTanSuatNCSState.update(itemReportTanSuatNCS);
                      }

                      final dataReturn = {"delete" : true};
                      Navigator.pop(context,dataReturn);
                    }
                  },
                ),
              )

            ],
          ),
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: MemberLogBookFormDetailView(
              formType: widget.formItemLogBook!.formType!,
              formSurveyId: widget.formItemLogBook!.id!,
              formId: widget.formItemLogBook!.formId!,
              memberId: widget.formItemLogBook!.memberId!,
              member: widget.formItemLogBook!.member!,
            ),
          ),
        ),
      );
  }

  Widget wdgFormLogbook(Map data){
    if(formSurveyData == "") return Container();
    if(data == null) {
      return Center(
        child: Text('Mẫu đánh giá chưa được cấu hình.',style: TextStyle(color: Colors.red),),
      );
    }

    var _fromSurveyedJson = json.decode(formSurveyData);
    var fields = data["fields"];
    String value;

    (fields as List<dynamic>).forEach((item) {
      //print(item["value"].toString());
      value = _fromSurveyedJson[item["key"]].toString();
      if(item["type"].toString() == "RadioButton"){
        if(value == "true" || value == "false") value = value == "true" ? "1" : "0";
        else value = value.toString();
      }
      item["value"] = value;
    });
    data["fields"] = fields;

    final ThemeData theme = Theme.of(context);

    return new JsonSchema(
      formMap: data,
      onChanged: (dynamic response) {
        this.response = response;
        print(response);
      },
      actionSave: (data) {
        print(data);
        var fields = data["fields"];
        Map<String, dynamic> map = new Map<String, dynamic>();
        String objText = '{';
        fields.forEach((field) {
          objText += '"${field["key"]}":${field["value"].toString()},';
          map["${field["key"]}"] = field["value"].toString();
        });
        objText += '}';
        map["people_with_disability"] = _formUser;
        map["form_date"] =
            new DateFormat(formatStr).format(_formDate!).toString();
        //var obj = json.encode(objText);
        //FormSurveyServices().create(map);
        finishForm();
      },
      selectUsers: wdgNKT(),
      chooseDate: wdgThoiGian(),
      buttonSave:
      Container(
        child: Center(
          child: Padding(
              padding: const EdgeInsets.only(
                  left: 0, bottom: 25, top: 16, right: 0),
              child:
              InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: theme.primaryColor,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: theme.primaryColor
                                    .withOpacity(0.5),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 10.0),
                          ],
                        ),
                        child: Center(
                          child: Text(
                            'Hoàn thành',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color:
                              MemberAppTheme.nearlyWhite,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                onTap: (){
                  finishForm();
                },
              )
          ),
        ),
      ),

      //end container btnSave
    );
  }

  Widget wdgNKT() {
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 0.0),
        child:
        new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Đánh giá tại nhà: ' + memberName.toString(),
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),);
  }

  Widget wdgThoiGian(){
    var _dateLabel = '${formSurveyDate!.day}/${formSurveyDate!.month}/${formSurveyDate!.year}';
    return
      Padding(
        padding: EdgeInsets.only(
            left: 15.0, right: 15.0, top: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Thời gian thực hiện:',
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            ElevatedButton(
              onPressed: () { },

              child: Container(
                alignment: Alignment.center,
                height: 50.0,
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.date_range,
                                size: 18.0,
                              ),
                              SizedBox(width: 10,),
                              Text(
                                '$_dateLabel',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            )
          ],
        ),);
  }

  Widget wdgTinhDiem(){
    return
      Padding(
        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0,bottom: 0),
        child:
        Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                  top: BorderSide( //                    <--- top side
                    color: colors[_level - 1],
                    width: 2.0,
                  )
              ),
            ),
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Icon(icons[_level-1],color: colors[_level - 1],size: 30,),
                SizedBox(height: 5,),
                Text(
                  _ketLuan,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      letterSpacing: 0.0,
                      color: colors[_level - 1]
                  ),
                ),
              ],
            )

        ),
      );
  }

  void calScore(){
    _tongDiem = _tuAnUongScore +_tuDanhRangScore + _tuTamScore + _tuMacQuanAoScore + _tuDaiTieuTienScore + _tuLenGiuongScore + _kiemSoatTieuTienScore;
    setState((){
      _tongDiem = _tongDiem;
    });
    var ketLuan = "";
    if(_tongDiem ==7){
      ketLuan = "Tự thực hiện";
      _level  = 1;
    }else if(_tuAnUongScore== 5 || _tuDanhRangScore== 5 ||  _tuTamScore ==5 || _tuMacQuanAoScore == 5 ||
        _tuDaiTieuTienScore == 5 || _tuLenGiuongScore == 5 || _kiemSoatTieuTienScore == 5
    ){
      ketLuan = "Phụ thuộc hoàn toàn";
      _level  = 5;
    }else if(_tuAnUongScore== 4 || _tuDanhRangScore== 4 ||  _tuTamScore == 4 || _tuMacQuanAoScore == 4 ||
        _tuDaiTieuTienScore == 4 || _tuLenGiuongScore == 4 || _kiemSoatTieuTienScore == 4
    ){
      ketLuan = "Cần người khác trợ giúp để thực hiện";
      _level  = 4;
    }else if(
    _tuAnUongScore==3 || _tuDanhRangScore==3 ||  _tuTamScore ==3 || _tuMacQuanAoScore ==3 ||
        _tuDaiTieuTienScore ==3 || _tuLenGiuongScore ==3 || _kiemSoatTieuTienScore==3
    ){
      ketLuan = "Cần người khác hướng dẫn/giám sát";
      _level  = 3;
    }else if(_tongDiem == 8){
      ketLuan = "Cần hỗ trợ của dụng cụ trợ giúp";
      _level  = 2;
    }
    setState((){
      _ketLuan = ketLuan;
    });
  }

  void finishForm(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          var rng = new Random();
          int randomNumber = rng.nextInt(2);
          if(randomNumber==0){
            return planSuccess();
          }else{
            return planChange();
          }
        });
  }

  Widget planSuccess(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:

      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyBlue,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.card_giftcard,color: Colors.lightBlue, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Bạn đã thực hiện thành công đánh giá!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.only(
                left: 20, right: 20, top: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 10),
                  child:
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.send,color: ManagerAppTheme.nearlyWhite),
                    label: Text("Đóng", style: TextStyle(color: ManagerAppTheme.nearlyWhite),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget planChange(){
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content:
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            color: MemberAppTheme.nearlyYellow,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: 60.0,
                  height: 60.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.info,color: MemberAppTheme.nearlyYellow, size: 50,),
                ),
                Padding(
                  padding: EdgeInsets.only(top:10),
                  child:
                  Text(
                    'Đánh giá lỗi! Vui lòng thực hiện lại.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: MemberAppTheme.nearlyWhite,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

