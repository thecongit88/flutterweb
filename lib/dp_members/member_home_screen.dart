import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/posts/post_screen.dart';
import 'package:hmhapp/dp_members/logbook_form_month_screen.dart';
import 'package:hmhapp/dp_members/logbook_form_screen.dart';
import 'package:hmhapp/dp_members/models/plan_info_screen.dart';
import 'package:hmhapp/dp_members/popular_course_list_view.dart';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/services/report_tan_suat_services.dart';
import 'package:hmhapp/states/bottom_tab_state.dart';
import 'package:hmhapp/states/report_tan_suat_ncs_state.dart';
import 'package:hmhapp/widgets/error_message.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:hmhapp/widgets/posts/post_carousel_view.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/member_home_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/widgets/footer.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'design_course_app_theme.dart';
import 'screens/member_support/member_support_list_screen.dart';
import 'screens/ncs_ke_hoach/ncs_list_ke_hoach_screen.dart';

class HomeMemberScreen extends StatefulWidget {
  @override
  _HomeMemberScreenState createState() => _HomeMemberScreenState();
}

class _HomeMemberScreenState extends State<HomeMemberScreen> {
  final GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();

  int categoryId = 0, memberId = 0;
  late AnimationController animationController;
  MemberHomeState memberHomeState = MemberHomeState();
  var currentDateTime = DateTime.now();
  bool showNotifyDay = false;
  late AuthServices _authServices;

  late FormItemLogbookServices _formItemLogbookServices;

  @override
  void initState() {
    _authServices = AuthServices();
    super.initState();
    memberHomeState.getListPostCategories();
    memberHomeState.getListPost();
    memberHomeState.getListVideos();
    memberHomeState.getAvatar();
    categoryId = 0;

    //ban đầu chưa có đánh giá nào được chọn
    _formItemLogbookServices = FormItemLogbookServices();
    _formItemLogbookServices.setDaCoDanhGia(null);
    _formItemLogbookServices.setSelectedDate(null);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ChangeNotifierProvider(
        create: (_) => memberHomeState,
    child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: theme.primaryColor,
          elevation: 0,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const <Widget>[
            Text(
              'Chăm sóc',
              textAlign: TextAlign.left,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                letterSpacing: 0.2,
                color: Colors.white,
              ),
            ),
            Text(
              'NKT tại nhà',
              textAlign: TextAlign.left,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
                letterSpacing: 0.27,
                color: Colors.white,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child:
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/profile");
                },
                child:
                Container(
                  padding: const EdgeInsets.only(top:8,bottom: 8),
                  width: 40,
                  child:
                  Consumer<MemberHomeState>(
                      builder: (context, state, child) {
                        return state.avataUrl == null || state.avataUrl.isEmpty ? const CircleAvatar(
                            backgroundImage:
                            AssetImage('assets/images/userImage.png')
                        ): CircleAvatar(
                        backgroundImage: NetworkImage( state.avataUrl)
                        );
                      }),

                )
            ),
          )
        ],
      ),
        key: scaffoldkey,
        backgroundColor: Colors.transparent,
        body:
        Column(
          children: <Widget>[
           Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    getNotification(),
                    //getSearchBarUI(),
                    /*Consumer<MemberHomeState>(
                      builder: (context, state, child) {
                        return getCategory(state);
                    }),*/
                    wdgControlsHome(),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16,bottom: 0),
                      child:
                      Row(
                        children: <Widget>[
                          const Expanded(
                            child: Text(
                              'Tin tức',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                color: Colors.black87,
                              ),
                            ),
                          ),
                          InkWell(
                            highlightColor: Colors.transparent,
                            borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) => PostScreen()));
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Row(
                                children: const <Widget>[
                                  Text(
                                    "Tất cả",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 16,
                                      letterSpacing: 0.5,
                                      color: Colors.black87,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 38,
                                    width: 26,
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.black87,
                                      size: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Consumer<MemberHomeState>(
                        builder: (context, state, child) {
                          return getLastestPost(state);
                        }),
                    //getPopularCourseUI(),
                    Consumer<MemberHomeState>(
                      builder: (context, state, child) {
                        return getPopularCourse(state);
                      },
                    ),
                    Footer()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  wdgControlsHome() {
    var list = [
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: MemberAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.access_time, size: 30.sp,color: MemberAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Khảo sát",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: MemberAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(2);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                padding: const EdgeInsets.all(8),
                alignment: Alignment.center,
                decoration: MemberAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.notifications_active_outlined, size: 30.sp,color: MemberAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Tin tức",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: MemberAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PostScreen()));
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: MemberAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.video_collection_outlined, size: 30.sp,color: MemberAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Video",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: MemberAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Provider.of<BottomTabState>(context, listen: false).setScreenIndex(4);
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                alignment: Alignment.center,
                decoration: MemberAppTheme.myHomeBoxDecoration(),
                child: const Icon(Icons.post_add, size: 35,
                    color: MemberAppTheme.nearlyBlue)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                child:
                Text("Kế hoạch chăm sóc",
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: MemberAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const NcsListKeHoachScreen()));
        },
      ),
      InkWell(
        child: Column(
          children: <Widget>[
            Container(
                width: 60.sp,
                height: 60.sp,
                padding: const EdgeInsets.all(8),
                alignment: Alignment.center,
                decoration: MemberAppTheme.myHomeBoxDecoration(),
                child: Icon(Icons.support, size: 30.sp,color: MemberAppTheme.nearlyBlue,)
            ),
            SizedBox(
              height: 10.sp,
            ),
            SizedBox(
                width: 90.sp,
                child:
                Text("Hỗ trợ",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: MemberAppTheme.homeTitle.copyWith(fontSize: 14.sp)))
          ],
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const MemberSupportListScreen()));
        },
      ),
    ];

    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 20, left: 16, right: 16),
      child: GridView.builder(
        padding: EdgeInsets.zero,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          crossAxisSpacing: 4.sp,
          mainAxisSpacing: 4.sp,
          mainAxisExtent: 110.sp,
        ),
        shrinkWrap: true,
        primary: false,
        itemCount: list?.length ?? 0,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return list![index];
        },
      ),
    );
  }

  
  Widget getPlans() {
    List<String> list = <String>[];
    list.add("Hướng dẫn TRẺ khuyết tật TỰ ĐI VỆ SINH BẰNG BÔ");
    list.add("Hướng dẫn NKT NAM TỰ ĐI TIỂU khi NGỒI XE LĂN hoặc NGỒI GHẾ");

    final ThemeData theme = Theme.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Chăm sóc NKT',
                style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),),
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25.0, top: 5.0),
          child:
          ListView.separated(
            shrinkWrap: true,
            separatorBuilder: (context, index) => const Divider(
              color: Colors.white,
            ),
            itemCount: list.length,
            itemBuilder: (context, index) =>
                Container(
                  decoration: BoxDecoration(
                    color: HexColor('#F8FAFB'),
                    borderRadius: const BorderRadius.all(
                        Radius.circular(16.0)),
                    // border: new Border.all(
                    //     color: MemberAppTheme.notWhite),
                  ),
                  child: ListTile(
                    title: Text(list[index],style: const TextStyle( height: 1.5)),
                    trailing: Icon(Icons.chevron_right,color: theme.primaryColor,),
                    onLongPress: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => const PlanInfoScreen(),
                          ));
                    },
                  ),
                ),
          ),
        ),
      ],
    );
  }

  Widget getCategory(MemberHomeState state) {
    switch (state.postCategoryResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(MemberAppTheme.nearlyBlue)),
            ),
          );
        break;
      case Status.COMPLETED:
        var categories = state.postCategoryResponse.data;
        List<Widget> widgets = <Widget>[];
        if(categories!.length>0) {
          widgets = categories.map((obj) {
            var index = categories.indexOf(obj);
            var isLast = index == categories.length - 1;
            return getButtonUI(obj, isLast, state);
          }).toList();
        }
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            categories.length>0?
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16,top:16),
              child: Row(
                children: widgets,
              ),
            ) : const SizedBox(),
          ],
        );
        break;
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.postCategoryResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
        break;
      case Status.INIT:
        return const SizedBox();
        break;
      default:
        return const SizedBox();
        break;
    }
  }

  Widget getLastestPost(MemberHomeState state) {
    switch (state.postResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(MemberAppTheme.nearlyBlue)),
            ),
          );
        break;
      case Status.COMPLETED:
        return PostCarouselView(
            posts: state.postResponse.data!,
            callBack: () {
              moveTo();
            },
          );
        break;
      case Status.ERROR:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Flushbar(
            message: state.postResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
        break;
      case Status.INIT:
        return const SizedBox();
        break;
      default:
        return const SizedBox();
        break;
    }
  }

  Widget getPopularCourseUI(List<YoutubeModel> listVideosData) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              const Expanded(
                child: Text(
                  'Video hướng dẫn',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    color: MemberAppTheme.darkerText,
                  ),
                ),
              ),
              InkWell(
                highlightColor: Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(4.0)),
                onTap: () {
                  Provider.of<BottomTabState>(context, listen: false).setScreenIndex(2);
                  //Navigator.push(context,
                    //  MaterialPageRoute(
                      //    builder: (BuildContext context) => HomeVideosScreen()));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Row(
                    children: const <Widget>[
                      Text(
                        "Tất cả",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16,
                          letterSpacing: 0.5,
                          color: MemberAppTheme.nearlyBlack,
                        ),
                      ),
                      SizedBox(
                        height: 38,
                        width: 26,
                        child: Icon(
                          Icons.chevron_right,
                          color: MemberAppTheme.darkText,
                          size: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          PopularVideoListView(
            listVideosData: listVideosData,
            callBack: () {
              moveTo();
            },
          ),
        ],
      ),
    );
  }

  void moveTo() {
    /*
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => VideoDetail(
          detail: YoutubeModel.youtubeData[0],
        ),
      ),

    );
     */
  }

  Widget getButtonUI(PostCategory postCategory, bool isLast, MemberHomeState state) {

    final ThemeData theme = Theme.of(context);

    return Expanded(
      child:
      Container(
        margin: EdgeInsets.only(right: isLast?0: 15),
        decoration: BoxDecoration(
            color: postCategory.id == state.categoryId
                ? theme.primaryColor
                : Colors.white,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: theme.primaryColor)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
            state.setCategoryId(postCategory.id ?? 0);
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 12, bottom: 12, left: 18, right: 18),
              child: Center(
                child: Text(
                  postCategory.name ?? "",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: postCategory.id == state.categoryId
                        ? Colors.white
                        : theme.primaryColor,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getSearchBarUI() {
    final ThemeData theme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.75,
            height: 64,
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Container(
                decoration: BoxDecoration(
                  color: HexColor('#F8FAFB'),
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(13.0),
                    bottomLeft: Radius.circular(13.0),
                    topLeft: Radius.circular(13.0),
                    topRight: Radius.circular(13.0),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        child: TextFormField(
                          style: TextStyle(
                            fontFamily: 'WorkSans',
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: theme.primaryColor,
                          ),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            labelText: 'Tìm kiếm video, tin tức',
                            border: InputBorder.none,
                            helperStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: HexColor('#B9BABC'),
                            ),
                            labelStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              letterSpacing: 0.2,
                              color: HexColor('#B9BABC'),
                            ),
                          ),
                          onEditingComplete: () {},
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 60,
                      height: 60,
                      child: Icon(Icons.search, color: HexColor('#B9BABC')),
                    )
                  ],
                ),
              ),
            ),
          ),
          const Expanded(
            child: SizedBox(),
          )
        ],
      ),
    );
  }

  Widget getAppBreturnarUI() {
    final ThemeData theme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 18),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Text(
                  'Chăm sóc',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    letterSpacing: 0.2,
                    color: Colors.grey,
                  ),
                ),
                Text(
                  'Bệnh nhân',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                    letterSpacing: 0.27,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              scaffoldkey.currentState!.openDrawer();
            },
            child:
            Container(
              width: 45,
              height: 45,
              child: Image.asset('assets/design_course/userImage.png'),
            )
          )
        ],
      ),
    );
  }

  DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);

  Widget getNotification() {
    var currentDate = DateTime(currentDateTime.year, currentDateTime.month, currentDateTime.day);
    var nextCurrentMonth = DateTime(currentDateTime.year, currentDateTime.month + 1, 1);
    var endOfCurrentMonth = DateTime(nextCurrentMonth.year, nextCurrentMonth.month, nextCurrentMonth.day - 1);
    //return notifyLogBookMonth(endOfCurrentMonth.month);
    if(currentDate == endOfCurrentMonth) return notifyLogBookMonth(endOfCurrentMonth.month);
    else {
      //thứ 5 là số 4; chủ nhật: 7
      if(currentDateTime.weekday == 4 || currentDateTime.weekday == 7) {
        _authServices.getSignedMemberId().then((mId){
          memberId = mId;

          if(memberId == 0)
            return const Center(
              child: CircularProgressIndicator(),
            );

          ReportTanSuatNCSServices _reportTanSuatNCSServices = ReportTanSuatNCSServices();
          int tansuat_kehoach, tansuat_thuchien_thu_5 = 0, tansuat_thuchien_cn = 0;
          _reportTanSuatNCSServices.listReportTanSuatNCS(currentDateTime.month, currentDateTime.year, memberId)
              .then((List<ReportTanSuatNCS> _reportTanSuatNCS) {
            tansuat_kehoach = _reportTanSuatNCS[0].tansuat_kehoach ?? 0;
            DateTime start_of_week = getDate(currentDate.subtract(Duration(days: currentDate.weekday - 1)));
            DateTime thursday_of_week = getDate(currentDate.subtract(Duration(days: currentDate.weekday - 4)));
            DateTime end_of_week = getDate(currentDate.add(Duration(days: DateTime.daysPerWeek - currentDate.weekday)));
            FormItemLogbookServices _formItemLogbookServices = FormItemLogbookServices();

            if(currentDateTime.weekday == 4) {
              _formItemLogbookServices.countSurveyExcuted(memberId, start_of_week, thursday_of_week).then((value) {
                tansuat_thuchien_thu_5 = value;
                double percentExcecute = tansuat_thuchien_thu_5/tansuat_kehoach * 100;
                if(percentExcecute < 50)
                  if (this.mounted) { /* avoid the error setState() called after dispose() */
                    setState(() {
                      showNotifyDay = true;
                    });
                  }
              });
            }

            if(currentDateTime.weekday == 7) {
              _formItemLogbookServices.countSurveyExcuted(memberId, thursday_of_week, end_of_week).then((value) {
                tansuat_thuchien_cn = value;
                double percentExcecute = tansuat_thuchien_cn/tansuat_kehoach * 100;
                if(percentExcecute < 100)
                  if (this.mounted) { /* avoid the error setState() called after dispose() */
                    setState(() {
                      showNotifyDay = true;
                    });
                  }
              });
            }
          });
          return const SizedBox();
        });

        //print('Start of week: ${getDate(currentDate.subtract(Duration(days: currentDate.weekday - 1)))}');
        //print('Thu 5 of week: ${getDate(currentDate.subtract(Duration(days: currentDate.weekday - 4)))}');
        //print('End of week: ${getDate(currentDate.add(Duration(days: DateTime.daysPerWeek - currentDate.weekday)))}');
      }
      if(showNotifyDay == true) return notifyLogBookDay();
      return const SizedBox(height: 1);
    }

    /*
    var rng = new Random();
    int randomNumber = rng.nextInt(3);
    if(randomNumber==0){
      return notifyLogBookDay();
    }else if(randomNumber==1){
      return notifyLogBookMonth();
    }else {
      return SizedBox(height: 1,);
    }
    */
  }

  Widget notifyLogBookMonth(int month) {
    String _month = month < 10 ? "0$month" : month.toString();
    return
      GestureDetector(
        child: Container(
            decoration: const BoxDecoration(
              color: MemberAppTheme.nearlyYellow,
              borderRadius: BorderRadius.all(
                  Radius.circular(26.0)),
            ),
            margin: const EdgeInsets.fromLTRB(20,20,20,10),
            padding: const EdgeInsets.fromLTRB(20,10,20,10),
            child:
            Row(
              mainAxisAlignment:MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Icon(Icons.new_releases,color: Colors.white, size: 14.0,),
                Text("Thực hiện đánh giá cuối tháng $_month",
                    style:const TextStyle(color: Colors.white, fontSize: 14.0)),
                const Icon(Icons.chevron_right,color: Colors.white,),
              ],
            )
        ),
        onTap: (){
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => const LogBookFormMonthScreen(),
              ));
        },
      );
  }

  Widget notifyLogBookDay() {
   return GestureDetector(
      child: Container(
          decoration: const BoxDecoration(
            color: Colors.orange,
            borderRadius: BorderRadius.all(
                Radius.circular(26.0)),
          ),
          margin: const EdgeInsets.fromLTRB(20,20,20,10),
          padding: const EdgeInsets.fromLTRB(20,10,20,10),
          child:
          Row(
            mainAxisAlignment:MainAxisAlignment.spaceBetween,
            children: const <Widget>[
              Icon(Icons.date_range,color: Colors.white, size: 14.0,),
              Text("Vui lòng thực hiện đánh giá theo ngày",
                  style:TextStyle(color: Colors.white, fontSize: 14.0)),
              Icon(Icons.chevron_right,color: Colors.white,),
            ],
          )
      ),
      onTap: (){
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => LogBookFormScreen(memberId: memberId),
            ));
      },
    );
  }

  Widget getPopularCourse(MemberHomeState state) {
    ApiResponse<List<YoutubeModel>> data = state.youtubeVideoResponseList;
    switch(data.status){
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message,
        );
      case Status.COMPLETED:
        return getPopularCourseUI(data.data!);
      case Status.ERROR:
        return ErrorMessage(
            errorMessage: data.message
        );
      case Status.INIT:
        return const SizedBox.shrink();
    }
  }

}
