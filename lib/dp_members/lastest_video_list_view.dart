import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/course_info_screen.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/video.dart';

class LastestVideoListView extends StatefulWidget {
  const LastestVideoListView({Key? key, this.callBack}) : super(key: key);

  final Function? callBack;
  @override
  _LastestVideoListViewState createState() => _LastestVideoListViewState();
}

class _LastestVideoListViewState extends State<LastestVideoListView>
    with TickerProviderStateMixin {
   late AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0),
      child: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return ListView.separated(
              shrinkWrap: true,
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(),
              itemCount: Video.popularVideoList.length,
              itemBuilder: (BuildContext context, int index) {
                final int count = Video.popularVideoList.length;
                final Animation<double> animation =
                Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                        parent: animationController,
                        curve: Interval((1 / count) * index, 1.0,
                            curve: Curves.fastOutSlowIn)));
                animationController.forward();
                if(index==0){
                  return VideoView(
                    callback: () {
                      widget.callBack!();
                    },
                    video:   Video.popularVideoList[index],
                    animation: animation,
                    animationController: animationController,
                  );

                }else{
                  Video video =    Video.popularVideoList[index];
                  return ListTile(
                    leading: Image.asset(video.imagePath),
                    title: Padding(
                        padding: EdgeInsets.only(top: 7),
                        child: Text(
                        video.title,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        )),
                    subtitle:
                    Padding(padding:EdgeInsets.only(top:5),
                    child: Text(video.createdDate,style: TextStyle(
                      fontWeight: FontWeight.w200,
                      fontSize: 12,
                      letterSpacing: 0.27,
                      color: MemberAppTheme.grey,
                    ),)),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => CourseInfoScreen(),
                        ),
                      );
                    },
                  );
                }
              },
            );
          }
        },
      ),
    );
  }
}

class VideoView extends StatelessWidget {

  final VoidCallback? callback;
  final Video? video;
  final AnimationController? animationController;
  final Animation<double>? animation;

  const VideoView(
      {Key? key,
      this.video,
      this.animationController,
      this.animation,
      this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController!,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation!,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation!.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                callback!();
              },
              child:  Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 200,
                      child: Image.asset(video!.imagePath,fit: BoxFit.fitWidth),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 16,left: 18, right: 18),
                      child: Text(
                        video!.title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          letterSpacing: 0.27,
                          color: MemberAppTheme
                              .darkerText,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
