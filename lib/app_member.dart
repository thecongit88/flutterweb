import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/route_generator.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/services/form_item_logbook_services.dart';
import 'package:hmhapp/states/auth_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MyAppMember extends StatefulWidget {
  final Widget defaultHome;
  const MyAppMember({Key? key, required this.defaultHome}) : super(key: key);

  @override
  _MyAppMemberState createState() => _MyAppMemberState();
}

class _MyAppMemberState extends State<MyAppMember> {

  AuthState authState = AuthState();
  FormItemLogbookServices _formItemLogbookServices = new FormItemLogbookServices();

  @override
  void initState() {
    super.initState();
    authState = AuthState();
    authState.getUser();
    //ban đầu chưa có đánh giá nào được chọn
    _formItemLogbookServices = new FormItemLogbookServices();
    _formItemLogbookServices.setDaCoDanhGia(null);
    _formItemLogbookServices.setSelectedDate(null);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return
      ChangeNotifierProvider(
        create: (_) => authState,
    child:
    MaterialApp(
      title: 'HMH - Chăm sóc sức khoẻ',
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('vi'),
      ],
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(
            color: Colors.white,
            ),
        ),
        primaryColor: MemberAppTheme.nearlyBlue,
        textTheme: AppTheme.textTheme,
        platform: TargetPlatform.iOS,
      ),
      home:Consumer<AuthState>(builder: (context, state, child) {
        return buildScreen(state,context);
      }),
      initialRoute: '/',
      builder:(context,child){
        return MediaQuery(data: MediaQuery.of(context).copyWith(textScaleFactor: 1), child: child!);
      },
      onGenerateRoute: RouteGenerator.generateRoute,
    ));
  }

  Widget buildScreen(AuthState state,BuildContext ctx) {
    switch (state.authResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        if(state.authResponse.data != null && state.authResponse.data!.isNotEmpty){
          return widget.defaultHome;
        } else {
          return SignInScreen();
        }
        break;
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          Flushbar(
            message: state.authResponse.message,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return SizedBox();
        break;
      case Status.INIT:
        return SizedBox();
        break;
      default:
        return SizedBox();
        break;
    }
  }
}
