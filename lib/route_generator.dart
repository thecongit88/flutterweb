import 'package:flutter/material.dart';
import 'package:hmhapp/common_screens/posts/post_detail_screen.dart';
import 'package:hmhapp/common_screens/videos/categories_videos_screen.dart';
import 'package:hmhapp/common_screens/videos/video_detail_screen.dart';
import 'package:hmhapp/common_screens/videos/video_detail_search_screen.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/dp_managers/screens/hbcc_info_screen.dart';
import 'package:hmhapp/dp_managers/screens/manager_member_detail_screen.dart';
import 'package:hmhapp/dp_members/member_info_screen.dart';
import 'package:hmhapp/dp_members/screens/member_main_screen.dart';
import 'package:hmhapp/common_screens/signin_screen.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/youtube.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MemberMainScreen());
      case '/signin':
      return MaterialPageRoute(builder: (_) => SignInScreen());
      case '/profile':
        return MaterialPageRoute(builder: (_) => MemberInfoScreen());
      case '/hbcc_profile':
        return MaterialPageRoute(builder: (_) => HBCCInfoScreen());
      case '/videos_category' :
      return MaterialPageRoute(builder: (_) => CategoryVideosScreen());
      case '/video_detail':
        if (args is YoutubeModel) {
          return MaterialPageRoute(
            builder: (_) => VideoDetail(
              detail: args,
            ),
          );
        }
        return _errorRoute();
      case '/post':
        if (args is Post) {
          return MaterialPageRoute(
            builder: (_) => PostDetailScreen(
              entry: args,
            ),
          );
        }
        return _errorRoute();
      case '/video_detail_search':
        if (args is YoutubeModel) {
          return MaterialPageRoute(
            builder: (_) => VideoDetailSearch(
              detail: args,
            ),
          );
        }
        return _errorRoute();
      case '/member_detail':
        if (args is Member) {
          return MaterialPageRoute(
            builder: (_) => ManagerMemberDetailScreen(
              member: args,
            ),
          );
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}