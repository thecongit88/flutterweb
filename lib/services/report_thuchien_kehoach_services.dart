import 'dart:convert';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/report_thuchien_kehoach.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReportThuchienKehoachServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId;

  ReportThuchienKehoachServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  Future<List<ReportThuchienKehoach>> listReportThuchienKehoach(int month, int year) async {
    await getSignedHbccId();
    String _endpoint = month == 0 ?
        EnpointUrl.REPORT_THUCHIEN_KEHOACH + "?year=$year&hbcc.id=$hbccId"
        :
    EnpointUrl.REPORT_THUCHIEN_KEHOACH + "?month=$month&year=$year&hbcc.id=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListThuchienKehoachResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<List<ReportThuchienKehoach>> listReportThuchienKehoachTheoKhoangThang(int fromMonth, int toMonth, int year) async {
    await getSignedHbccId();
    String _endpoint = EnpointUrl.REPORT_THUCHIEN_KEHOACH + "?month_gte=$fromMonth&month_lte=$toMonth&year=$year&hbcc.id=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListThuchienKehoachResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportThuchienKehoach?> add(ReportThuchienKehoach reportItem) async {
    String _endpoint = EnpointUrl.REPORT_THUCHIEN_KEHOACH;
    try{
      Response response = await baseApi.postAsync(_endpoint, reportItem.toJson());
      var result = new ReportThuchienKehoachResponse.fromJson(json.decode(response.body));
      return result.reportThuchienKehoach;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportThuchienKehoach?> update(ReportThuchienKehoach reportItem) async {
    String _endpoint = EnpointUrl.REPORT_THUCHIEN_KEHOACH + "/${reportItem.id}";
    try{
      Response response = await baseApi.putAsync(_endpoint, reportItem.toJson());
      var result = new ReportThuchienKehoachResponse.fromJson(json.decode(response.body));
      return result.reportThuchienKehoach;
    } catch(e) {
      throw(e);
    }
  }
}

