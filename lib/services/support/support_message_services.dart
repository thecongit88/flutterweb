import 'dart:convert';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/dp_managers/models/support/support_message.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:http/http.dart';
import '../../dp_managers/models/checkin/hbcc_checkin.dart';

class SupportMessageServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  final String _endpoint = EnpointUrl.HBCC_SUPPORT_MESSAGE;

  SupportMessageServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }


  Future<List<SupportMessage>> listByConversation({int? conversationId}) async {
    try{
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&conversation.id_eq=$conversationId");
      var result = SupportMessageResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      rethrow;
    }
  }

  Future<SupportMessage> create(SupportMessage item) async {
    try {
      final response =
      await baseApi.postAsync(_endpoint, item.toJson());
      return SupportMessage.fromJson(json.decode(response.body));
    }catch(ex){
      rethrow;
    }
  }

  Future<SupportMessage> update(SupportMessage item, int id) async {
    try {
      final response =
      await baseApi.putAsync("$_endpoint/$id", item.toJson());
      var result = json.decode(response.body);
      return SupportMessage.fromJson(result);
    }catch(ex){
      rethrow;
    }
  }

  Future<Map> delete(int id) async {
    try {
      final response =
      await baseApi.deleteAsync("$_endpoint/$id");
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      rethrow;
    }
  }

}
