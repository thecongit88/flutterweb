import 'dart:convert';
import 'package:hmhapp/dp_managers/models/support/support_conservation.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:http/http.dart';
import '../../dp_managers/models/checkin/hbcc_checkin.dart';

class SupportConversationServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  final String _endpoint = EnpointUrl.HBCC_SUPPORT_CONSERVATION;

  SupportConversationServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }


  Future<List<SupportConversation>> listByHbcc({int? hbccId}) async {
    try{
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&hbcc.id_eq=$hbccId");
      var result = SupportConversationResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      rethrow;
    }
  }


  Future<List<SupportConversation>> listByMember({int? memberId}) async {
    try{
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&member.id_eq=$memberId");
      var result = SupportConversationResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      rethrow;
    }
  }

  Future<SupportConversation> create(SupportConversation item) async {
    try {
      final response =
      await baseApi.postAsync(_endpoint, item.toJson());
      return SupportConversation.fromJson(json.decode(response.body));
    }catch(ex){
      rethrow;
    }
  }

  //list ke hoach cua hbcc va loc theo thang/nam + member

  Future<dynamic> update(SupportConversation item, int id) async {
    try {
      final response =
      await baseApi.putAsync("$_endpoint/$id", item.toJson());
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      rethrow;
    }
  }

  Future<Map> delete(int id) async {
    try {
      final response =
      await baseApi.deleteAsync("$_endpoint/$id");
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      rethrow;
    }
  }

}
