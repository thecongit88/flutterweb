import 'dart:convert';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReportTanSuatNCSServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId;

  ReportTanSuatNCSServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
  }

  Future<List<ReportTanSuatNCS>> listReportTanSuatNCS(int month, int year, int memberId) async {
    String _endpoint = month == 0 ?
        EnpointUrl.REPORT_TANSUAT_NCS + "?year=$year&member.id=$memberId"
        :
    EnpointUrl.REPORT_TANSUAT_NCS + "?month=$month&year=$year&member.id=$memberId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTanSuatNCSResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  /* Dành cho BC so sánh % thực hiện kế hoạch HBCC */
  Future<List<ReportTanSuatNCS>> listReportTanSuatThucHienKeHoachHBCC(int month, int year) async {
    await getSignedHbccId();
    String _endpoint = month == 0 ?
    EnpointUrl.REPORT_TANSUAT_NCS + "?year=$year&member.hbcc=$hbccId"
        :
    EnpointUrl.REPORT_TANSUAT_NCS + "?month=$month&year=$year&member.hbcc=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTanSuatNCSResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<List<ReportTanSuatNCS>> listReportTanSuatThucHienKeHoachHBCCTheoKhoangThang(int fromMonth, int toMonth, int year) async {
    await getSignedHbccId();
    String _endpoint = EnpointUrl.REPORT_TANSUAT_NCS + "?month_gte=$fromMonth&month_lte=$toMonth&year=$year&member.hbcc=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTanSuatNCSResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTanSuatNCS?> add(ReportTanSuatNCS reportItem) async {
    String _endpoint = EnpointUrl.REPORT_TANSUAT_NCS;
    try{
      Response response = await baseApi.postAsync(_endpoint, reportItem.toJson());
      var result = new ReportTanSuatNCSResponse.fromJson(json.decode(response.body));
      return result.reportTanSuatNCS;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTanSuatNCS?> update(ReportTanSuatNCS reportItem) async {
    String _endpoint = EnpointUrl.REPORT_TANSUAT_NCS + "/${reportItem.id}";
    try{
      Response response = await baseApi.putAsync(_endpoint, reportItem.toJson());
      var result = new ReportTanSuatNCSResponse.fromJson(json.decode(response.body));
      return result.reportTanSuatNCS;
    } catch(e) {
      throw(e);
    }
  }
}

