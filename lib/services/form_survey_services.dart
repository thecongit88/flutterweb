import 'dart:convert';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:logging/logging.dart';

class FormSurveyServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  FormSurveyServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<Map> create(Map form,String formType) async {
    try {
      final response =
      await baseApi.postAsync(formType,form);
      var result = json.decode(response.body);

      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future<Map> update(Map form,int formId,String formType) async {
    try {
      var _endpoint = "${formType}/${formId}";

      final response =
      await baseApi.putAsync(_endpoint,form);
      var result = json.decode(response.body);

      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future<List<FormSurvey>> getListFormsSurvey(formName, formDate) async {
    String _endpoint;
    if(formDate != "") _endpoint = formName+"?_sort=id:ASC&form_date_eq=$formDate";
    else _endpoint = formName+"?_sort=id:ASC";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormSurveyResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  /* Đếm số lượng đánh giá đã thực hiện của 1 member từ đầu tháng đến cuối tháng */
  Future<int> countSurveyExcuted(formName, memberId, startDate, endDate) async {
    String _endpoint = formName+"/count?member.id=$memberId&form_date_gte=$startDate&form_date_lt=$endDate";
    //print(_endpoint);
    Response response = await baseApi.getAsync(_endpoint);
    var result = json.decode(response.body);
    return result;
  }


  Future<String> getFormSurveyData(formObject, id) async {
    String _endpoint = formObject+"/$id";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      return response.body;
      //var result = new FormSurveyResponse.fromJson(json.decode(response.body));
      //return result.items[0];
    }
    catch(e){
      throw(e);
    }
  }

  Future<FormSurvey> getFormSurveyObject(formObject, id) async {
    String _endpoint = formObject+"/$id";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormSurvey.fromJson(json.decode(response.body));
      return result;
    }
    catch(e){
      throw(e);
    }
  }


  Future<Map> delete(int formId,String formType) async {
    try {
      String _endpoint = "${formType}/${formId}";
      final response =
      await baseApi.deleteAsync(_endpoint);
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

}
