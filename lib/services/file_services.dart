import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/files/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/files/file_fields_upload.dart';
import '../models/files/file_result_upload.dart';

class FileServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  FileServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<dynamic> uploadImages(List<FileUploadSimple> files,FileFieldUpload fields) async {
    const endpoint = EnpointUrl.FILE_UPLOAD;
    final response =
    await baseApi.uploadFileAsync(endpoint,files,fields.toJson());
    return json.decode(response.toString());
  }


}

