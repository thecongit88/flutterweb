import 'dart:convert';
import 'package:hmhapp/models/video_categories.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class VideoCategoryServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String tokenAuth = "";

  VideoCategoryServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<VideoCategory>> listCategories(
      {bool hideEmpty: true, List<int>? excludeIDs}) async {

    String _endpoint = EnpointUrl.VIDEO_CATEGORY_URL_LIST;

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new VideoCategoryResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

}