import 'dart:async';
import 'dart:convert';
import "dart:core";
import 'dart:io';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:hmhapp/models/files/file_upload.dart';
import 'package:hmhapp/services/app_exceptions.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart';

import '../common/utils.dart';
import '../models/file_upload.dart';

class QueryString {
  static Map parse(String query) {
    var search = new RegExp('([^&=]+)=?([^&]*)');
    var result = new Map();

    // Get rid off the beginning ? in query strings.
    if (query.startsWith('?')) query = query.substring(1);
    // A custom decoder.
    decode(String s) => Uri.decodeComponent(s.replaceAll('+', ' '));

    // Go through all the matches and build the result map.
    for (Match match in search.allMatches(query)) {
      result[decode(match.group(1)!)] = decode(match.group(2)!);
    }
    return result;
  }
}


Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

class BaseAPI{
  String? url;
  String? consumerKey;
  String? consumerSecret;
  bool? isHttps;
  String? tokenAuth = "";

  BaseAPI({this.url, this.consumerKey, this.consumerSecret}) {
    if (this.url!.startsWith("https")) {
      this.isHttps = true;
    } else {
      this.isHttps = false;
    }
    this.url = EnpointUrl.URL_CLOUD;
  }

  getToken() async {
    SharedPreferences preferences = await _prefs;
    tokenAuth = preferences.getString('accessToken') ?? '';
  }

  Future<dynamic> getAsync(String endPoint, {Map? data}) async {
    var responseJson;
    var url = Uri.parse(EnpointUrl.BASE_URL + endPoint);
    await getToken();
    Map<String, String> headers;
    if(tokenAuth!.isNotEmpty) {
      headers = {
        'Content-type': 'application/json',
        'Authorization': 'Bearer $tokenAuth'
      };
    }else{
      headers = {
        'Content-type': 'application/json'
      };
    }

    try {
      final response = await http.get(url, headers: headers);
      print("Get Data");
      print(url);
      prettyPrintJson(response.body);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  Future<dynamic> postAsync(String endPoint, Map data) async {
    var responseJson;
    var url = EnpointUrl.BASE_URL + endPoint;

    await getToken();

    var client = new http.Client();
    Map<String,String> headers = {
      'Content-type' : 'application/json'
    };
    if(tokenAuth!.isNotEmpty){
      headers['Authorization'] = 'Bearer $tokenAuth';
    }

    headers.update(
        HttpHeaders.contentTypeHeader, (_) => 'application/json; charset=utf-8',
        ifAbsent: () => 'application/json; charset=utf-8');
   // headers.update(HttpHeaders.cacheControlHeader, (_) => 'no-cache',
    //    ifAbsent: () => 'no-cache');
    var body = json.encode(data);

    print(endPoint);
    print("Pretty Json Data");
    print(url);
    prettyPrintJson(body);

    try {
      var response = await client.post(Uri.parse(url), headers: headers, body: body);
      print("Pretty Json Data");
      prettyPrintJson(response.body);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  Future<dynamic> putAsync(String endPoint, Map data) async {
    var responseJson;
    var url = EnpointUrl.BASE_URL + endPoint;
    await getToken();

    var client = new http.Client();
    Map<String,String> headers = {
      'Content-type' : 'application/json'
    };
    if(tokenAuth!.isNotEmpty){
      headers['Authorization'] = 'Bearer $tokenAuth';
    }

    headers.update(
        HttpHeaders.contentTypeHeader, (_) => 'application/json; charset=utf-8',
        ifAbsent: () => 'application/json; charset=utf-8');
    headers.update(HttpHeaders.cacheControlHeader, (_) => 'no-cache',
        ifAbsent: () => 'no-cache');
    var body = json.encode(data);
    print(url);
    //print(body);
    try {
      var response = await client.put(Uri.parse(url),
          headers: headers, body: body);
      print("Pretty Json Data");
      prettyPrintJson(response.body);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  Future<dynamic> deleteAsync(String endPoint, {Map? data}) async {
    var responseJson;
    var url = Uri.parse(EnpointUrl.BASE_URL + endPoint);
    await getToken();
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization' : 'Bearer $tokenAuth'
    };

    try {
      final response = await http.delete(url, headers: headers);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  Future<dynamic> postFileAsync(String endPoint, String filePath, String fileName,Map<String,String> fields) async {
    var responseJson;
    var url = EnpointUrl.BASE_URL + endPoint;

    await getToken();

    var client = new http.Client();
    Map<String,String> headers = {
      'Content-type' : 'imultipart/form-data;'
    };
    if(tokenAuth!.isNotEmpty){
      headers['Authorization'] = 'Bearer $tokenAuth';
    }
    headers['datatype'] = 'image/jpeg';
    headers.update(
        HttpHeaders.contentTypeHeader, (_) => 'multipart/form-data',
        ifAbsent: () => 'multipart/form-data');
    headers.update(HttpHeaders.cacheControlHeader, (_) => 'no-cache',
        ifAbsent: () => 'no-cache');


    var multipartRequest = http.MultipartRequest("POST", Uri.parse(url));
    multipartRequest.headers.addAll(headers);
    multipartRequest.fields.addAll(fields);

    /* fix imamge rotate when capture */
    File rotatedImage =
    await FlutterExifRotation.rotateAndSaveImage(path: filePath);

    var pic = await http.MultipartFile.fromPath("files",
        rotatedImage.path,
        filename: fileName,
        contentType: new MediaType('image', 'png'));

    /* old code */
    /*
    var pic = await http.MultipartFile.fromPath("files",
        filePath,
        filename: fileName,
        contentType: new MediaType('image', 'png'));
     */

    multipartRequest.files.add(pic);


    try {
      var response = await multipartRequest.send();
     // var responseData = await response.stream.toBytes();
      //var responseString = String.fromCharCodes(responseData);
      responseJson = _returnStreamResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  Future<dynamic> uploadFileAsync(String endPoint, List<FileUploadSimple> files,Map<String,String>? fields) async {
    var responseJson;
    var url = EnpointUrl.BASE_URL + endPoint;
    var client = new http.Client();
    Map<String,String> headers = {
      'Content-type' : 'imultipart/form-data;'
    };

    await getToken();

    if(tokenAuth!.isNotEmpty){
      headers['Authorization'] = 'Bearer $tokenAuth';
    }

    headers['datatype'] = 'image/jpeg';
    headers.update(
        HttpHeaders.contentTypeHeader, (_) => 'multipart/form-data',
        ifAbsent: () => 'multipart/form-data');
    headers.update(HttpHeaders.cacheControlHeader, (_) => 'no-cache',
        ifAbsent: () => 'no-cache');

    var multipartRequest = http.MultipartRequest("POST", Uri.parse(url));
    multipartRequest.headers.addAll(headers);
    if(fields != null) {
      multipartRequest.fields.addAll(fields);
    }
    /* fix imamge rotate when capture */
    if(files.isNotEmpty) {
      for(var f in files){
        File rotatedImage =
        await FlutterExifRotation.rotateAndSaveImage(path: f.path!);

        var pic = await http.MultipartFile.fromPath("files",
            rotatedImage.path,
            filename: f.name,
            contentType: new MediaType('image', 'png'));

        multipartRequest.files.add(pic);
      }
    }

    try {
      var response = await multipartRequest.send();
      responseJson = _returnStreamResponse(response);
    } on SocketException {
      throw FetchDataException('Xảy ra lỗi trong khi kết nối máy chủ.');
    }
    return responseJson;
  }

  void dispose() {}
}

dynamic _returnResponse(http.Response response) {
  switch (response.statusCode) {
    case 200:
      return response;
    case 400:
      throw BadRequestException(response.statusCode);
    case 401:
    case 403:
      //throw UnauthorisedException(response.body.toString());
      throw UnauthorisedException("Bạn không có quyền truy cập tài nguyên này.");
    case 500:
    default:
      throw FetchDataException(
          'Xảy ra lỗi trong khi kết nối máy chủ, mã lỗi : ${response.statusCode}');
  }
}


dynamic _returnStreamResponse(http.StreamedResponse response) async {
  switch (response.statusCode) {
    case 200:
      var responseData = await response.stream.toBytes();
      return String.fromCharCodes(responseData);
    case 400:
      var responseData = await response.stream.toBytes();
      throw BadRequestException(String.fromCharCodes(responseData));
    case 401:
    case 403:
    //throw UnauthorisedException(response.body.toString());
      throw UnauthorisedException("Bạn không có quyền truy cập tài nguyên này.");
    case 500:
    default:
      throw FetchDataException(
          'Xảy ra lỗi trong khi kết nối máy chủ, mã lỗi : ${response.statusCode}');
  }
}