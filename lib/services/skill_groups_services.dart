import 'dart:convert';
import 'package:hmhapp/dp_members/models/skill_groups.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SkillGroupsServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId, userId;

  SkillGroupsServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  getSignedUserId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = await preferences.getInt('userId');
  }

  Future<List<SkillGroup>> listSkillGroups() async {
    String _endpoint = EnpointUrl.LIST_SKILL_GROUPS_URL + "?_sort=id:ASC&is_active=true&skills.is_active=true";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = SkillGroupResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }
}

