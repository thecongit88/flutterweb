import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProvinceServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  ProvinceServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<Province>> listProvinces() async {
    String _endpoint = EnpointUrl.LIST_TINH;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = ProvinceResponse.fromJson(json.decode(response.body));
      print(response.body);
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }


  Future<int> update(Province province) async {
    String _endpoint = EnpointUrl.LIST_TINH + "/${province.id}";
    try{
      Response response = await baseApi.putAsync(_endpoint, province.toJson());
      return 1;
    } catch(e) {
      throw(e);
    }
  }
}

