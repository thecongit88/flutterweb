import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/ward.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WardServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  WardServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<Ward>> listWards() async {
    String _endpoint = EnpointUrl.LIST_XA;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = WardResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<List<Ward>> listWardsByDistrict(int districtId) async {
    //String _endpoint = EnpointUrl.LIST_XA + "?district.id=$districtId";
    String _endpoint = districtId != null && districtId.toString().toLowerCase() != "null" ? EnpointUrl.LIST_XA + "?district.id=$districtId" : EnpointUrl.LIST_XA;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = WardResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

