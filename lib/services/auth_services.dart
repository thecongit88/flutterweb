import 'dart:io';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class AuthServices{

  late BaseAPI baseApi;
  int? _hbccId;
  int? _memberId;
  String? tokenAuth = "";

  String? fullName = "", avataUrl = "";

  AuthServices(){
      getToken();
      baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
  }

  getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    tokenAuth = await preferences.getString('accessToken');
  }

  setToken(String token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('accessToken',token);
  }

  setSignedHbccId(int hbccId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('hbccId', hbccId);
  }

  setSignedMemberId(int memberId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('memberId', memberId);
  }

  getSignedMemberId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('memberId');
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt('hbccId');
  }

  getUserName() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('accessUserName');
  }

  setUserName(String userName) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('accessUserName', userName);
  }

  Future<User> signIn(UserLogin user) async {
    String endpoint = EnpointUrl.AUTH_URL_LOGIN;
    try{
      Response response = await baseApi.postAsync(endpoint,user.toJson());
      var result = UserResponse.fromJson(json.decode(response.body));
      await setToken(result.token ?? "");

      if(Constants.APP_MODULE == Constants.HBCC) {
        await getHbccInfo(result.user!.id); //name and avata also use in drawer menu
        if(_hbccId == 0) {
          result.user!.inValid = false;
          await setToken("");
        }
        else {
          await setSignedHbccId(_hbccId ?? 0);
        }
      } else {
        await getMemberInfo(result.user!.id);
        if(_memberId == null || _memberId == 0) {
          result.user!.inValid = false;
          await setToken("");
        } else {
          await getMemberInfoAfterRegister(result.user!.id);
          if(_memberId == null || _memberId == 0) {
            result.user!.approved = false;
            await setToken("");
          } else {
            await setSignedMemberId(_memberId ?? 0);
          }
          await setSignedHbccId(_hbccId ?? 0);
        }
      }

      if(result.user != null && result.user!.inValid == null) {
        await setSignedUserId(result.user!.id!); //User có thể là nkt hoặc hbcc
        await setFullName(fullName ?? "");
        await setAvataUrl(avataUrl ?? "");
        await setUserName(result.user!.username ?? "");
      }

      return result.user!;
    }
    catch(e){
      rethrow;
    }
  }

  setSignedUserId(int userId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userId', userId);
  }

  getSignedUserId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.getInt('userId');
  }

  setFullName(String fullName) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('fullName', fullName);
  }

  setAvataUrl(String avataUrl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('avataUrl', avataUrl);
  }

  Future<void> getHbccInfo(user_id) async {
    String endpoint = "${EnpointUrl.LIST_HBCC_URL}?user.id_eq=$user_id";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = HBCCResponse.fromJson(json.decode(response.body));
      _hbccId = result.items.isNotEmpty  ? result.items[0].id : 0;
      fullName = result.items.isNotEmpty  ? result.items[0].fullName : "";
      avataUrl = result.items.isNotEmpty  ? result.items[0].avatar : "";
    }
    catch(e){
      rethrow;
    }
  }

  Future<void> getMemberInfo(user_id) async {
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}?user.id_eq=$user_id";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberResponse.fromJson(json.decode(response.body));
      _memberId = result.items.isNotEmpty  ? result.items[0].id : 0;
      _hbccId = result.items.isNotEmpty  ? result.items[0].hbccId : 0;
      fullName = result.items.isNotEmpty ? result.items[0].name : "";
      avataUrl = result.items.isNotEmpty ? result.items[0].avatar : "";
    }
    catch(e){
      rethrow;
    }
  }

  Future<void> getMemberInfoAfterRegister(user_id) async {
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}?user.id_eq=$user_id&approved=true";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberResponse.fromJson(json.decode(response.body));
      _memberId = result.items.isNotEmpty  ? result.items[0].id : 0;
      fullName = result.items.isNotEmpty ? result.items[0].name : "";
      avataUrl = result.items.isNotEmpty ? result.items[0].avatar : "";
    }
    catch(e){
      rethrow;
    }
  }

  Future<String> getCurentUser() async {
    try{
      await getToken();
      return tokenAuth ?? "";
    }
    catch(e){
      rethrow;
    }

  }

  Future<User> signUp(UserRegister user) async {
    String endpoint = EnpointUrl.AUTH_URL_REGISTER;
    try{
      Response response = await baseApi.postAsync(endpoint,user.toJson());
      var result = UserResponse.fromJson(json.decode(response.body));
      //await setToken(result.token);
      //await setSignedUserId(result.user.id);
      if(Constants.APP_MODULE == Constants.HBCC) {
       // await getHbccInfo(result.user.id);
        //await setSignedHbccId(this._hbccId);
      }
      return result.user!;
    }
    catch(e){
      rethrow;
    }

  }

  Future<User> changePassword(UserChangePassword user) async {
    String endpoint = EnpointUrl.AUTH_URL_CHANGEPASS;

    try{
      Response response = await baseApi.postAsync(endpoint,user.toJson());
      var result = UserResponse.fromJson(json.decode(response.body));
      //await setToken(result.token);
      //await setSignedHbccId(result.user.id);
      return result.user!;
    }
    catch(e){
      rethrow;
    }
  }

}