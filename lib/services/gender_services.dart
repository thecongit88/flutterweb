import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenderServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  GenderServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<Gender>> listGenders() async {
    String _endpoint = EnpointUrl.LIST_GENDER;
    try{
     // Response response = await baseApi.getAsync(_endpoint);
      //var result = GenderResponse.fromJson(json.decode(response.body));

      var result = GenderResponse.fromJson(json.decode(Constants.resGenders));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

