import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/have_internet.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HaveInternetServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  HaveInternetServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<HaveInternet>> listInternet() async {
    try{
      var result = HaveInternetesponse.fromJson(json.decode(Constants.resJsonYesNo));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }
}

