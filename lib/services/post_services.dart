import 'dart:convert';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/utils/service_functions.dart';
import 'package:http/http.dart';

class PostServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String tokenAuth = "";

  PostServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<Post>> listPosts(
      {bool hideEmpty: true, List<int>? excludeIDs}) async {

    String _endpoint = EnpointUrl.POST_URL;

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new PostResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }


  Future<PostPageResponse> listAll(int pg,int categoryId) async {
    var fr = (pg-1)* Constants.PAGE_SIZE;
    var lm = Constants.PAGE_SIZE;
    var param = categoryId>0? 'post_category.id_eq=$categoryId':'';
    param += '&_start=$fr&_limit=$lm';
    String _endpoint = EnpointUrl.POST_URL+'?$param';

    try{
      var pages = await getPages(categoryId);
      Response response = await baseApi.getAsync(_endpoint);
      var result = new PostPageResponse.fromJson(json.decode(response.body));
      result.pages = pages;
      return result;
    }
    catch(e){
      throw(e);
    }
  }

  Future<Post> getPost(int id) async {

    String _endpoint = EnpointUrl.POST_URL+'/${id}';

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new Post.fromJson(json.decode(response.body));
      return result;
    }
    catch(e){
      throw(e);
    }
  }

  Future<int> getPages(int categoryId) async {

    var param = categoryId>0? 'post_category.id_eq=$categoryId':'';
    String _endpoint = EnpointUrl.POST_URL+'/count?$param';

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = json.decode(response.body);
      return pages(result,Constants.PAGE_SIZE);
    }
    catch(e){
      throw(e);
    }
  }

  Future<List<Post>> relatedPosts(int categoryId,int currentId) async {

    String _endpoint = EnpointUrl.POST_URL+"?post_category.id_eq=$categoryId&id_nin=$currentId";

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new PostResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }


  Future<PostPageResponse> search(int pg,String keyword) async {
    var fr = (pg-1)* Constants.PAGE_SIZE_ALL;
    var lm = Constants.PAGE_SIZE_ALL;
    var param = keyword.isNotEmpty? 'title_contains=$keyword':'';
    param += '&_start=$fr&_limit=$lm';
    String _endpoint = EnpointUrl.POST_URL+'?$param';

    try{
      var pages = 1;//await getPages(categoryId);
      Response response = await baseApi.getAsync(_endpoint);
      var result = new PostPageResponse.fromJson(json.decode(response.body));
      result.pages = pages;
      return result;
    }
    catch(e){
      throw(e);
    }
  }

}