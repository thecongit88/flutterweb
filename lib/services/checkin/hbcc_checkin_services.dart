import 'dart:convert';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:http/http.dart';
import '../../dp_managers/models/checkin/hbcc_checkin.dart';
import '../../utils/constants.dart';
import '../../utils/functions.dart';

class HbccCheckinServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  final String _endpoint = EnpointUrl.HBCC_CHECKIN;

  HbccCheckinServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }


  Future<List<HbccCheckin>> listByHbcc({int? hbccId,int? memberId,int? year,int? month,int? pg}) async {
    try{
      var fr = (pg??1 - 1) * Constants.PAGE_SIZE;
      var lm = Constants.PAGE_SIZE;
      var paramDate = Functions.getFilteDateParam(fieldName: "checkin_time",year: year,month: month);
      var param = '_sort=id:DESC';
      param += '&_start=$fr&_limit=$lm';
      param += '&hbcc.id_eq=$hbccId';
      param += memberId != null && memberId > 0 ? '&member.id_eq=$memberId' : '';
      param += paramDate.isNotEmpty ? paramDate : '';
      String endpoint = '$_endpoint?$param';

      Response response = await baseApi.getAsync(endpoint);
      var result = HbccCheckinResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      rethrow;
    }
  }

  Future<HbccCheckin> create(HbccCheckin hbccThemCheckin) async {
    try {
      final response =
      await baseApi.postAsync(_endpoint, hbccThemCheckin.toJson());
      return HbccCheckin.fromJson(json.decode(response.body));
    }catch(ex){
      rethrow;
    }
  }

  Future<HbccCheckin> update(HbccCheckin hbccThemCheckin, int id) async {
    try {
      final response =
      await baseApi.putAsync("$_endpoint/$id", hbccThemCheckin.toJson());
      return HbccCheckin.fromJson(json.decode(response.body));
    }catch(ex){
      throw ex;
    }
  }

  Future<Map> delete(int id) async {
    try {
      final response =
      await baseApi.deleteAsync("$_endpoint/$id");
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      rethrow;
    }
  }

}
