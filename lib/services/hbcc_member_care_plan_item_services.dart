import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:hmhapp/dp_managers/models/hbcc_ke_hoach/care_plan_item.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import '../dp_managers/models/hbcc_ke_hoach/add_care_plan_item.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';

class HbccMemberCarePlanItemServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String _endpoint = EnpointUrl.HBCC_KE_HOACH_ITEM;

  HbccMemberCarePlanItemServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<dynamic> create(AddCarePlanItem item) async {
    try {
      print("item saved");
      print(json.encode(item));
      if(item.itemTime != null && item.itemTime!.trim() == "") {
        item.itemTime = null;
      }
      final response =
      await baseApi.postAsync(_endpoint, item.toJson());
      return json.decode(response.body);
    }catch(ex){
      throw ex;
    }
  }

  Future<List<CarePlanItem>> listCarePlanItems(int planId) async {
    try{
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:ASC&plan.id_eq=$planId");
      var result = CarePlanItemResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      throw(e);
    }
  }

  Future<dynamic> update(AddCarePlanItem item, int id) async {
    try {
      final response =
      await baseApi.putAsync("$_endpoint/$id", item.toJson());
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future<Map> delete(int id) async {
    try {
      final response =
      await baseApi.deleteAsync("$_endpoint/$id");
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

}
