import 'dart:convert';
import 'package:hmhapp/models/report_tinhtrang_nkt.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class ReportTinhTrangNKTServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  ReportTinhTrangNKTServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
  }

  Future<List<ReportTinhTrangNKT>> listReportTinhTrangNKT(int month, int year, int memberId) async {
    String _endpoint = month == 0 ?
        EnpointUrl.REPORT_TINHTRANG_NKT + "?year=$year&member.id=$memberId"
        :
    EnpointUrl.REPORT_TINHTRANG_NKT + "?month=$month&year=$year&member.id=$memberId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTinhTrangNKTResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTinhTrangNKT?> add(ReportTinhTrangNKT reportItem) async {
    String _endpoint = EnpointUrl.REPORT_TINHTRANG_NKT;
    try{
      Response response = await baseApi.postAsync(_endpoint, reportItem.toJson());
      var result = new ReportTinhTrangNKTResponse.fromJson(json.decode(response.body));
      return result.reportTanSuatNCS;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTinhTrangNKT?> update(ReportTinhTrangNKT reportItem) async {
    String _endpoint = EnpointUrl.REPORT_TINHTRANG_NKT + "/${reportItem.id}";
    try{
      Response response = await baseApi.putAsync(_endpoint, reportItem.toJson());
      var result = new ReportTinhTrangNKTResponse.fromJson(json.decode(response.body));
      return result.reportTanSuatNCS;
    } catch(e) {
      throw(e);
    }
  }
}

