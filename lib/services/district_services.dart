import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/district.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DistrictServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  DistrictServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<District>> listDistricts() async {
    String _endpoint = EnpointUrl.LIST_HUYEN;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = DistrictResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<List<District>> listDistrictsByProvince(int provinceId) async {
    //String _endpoint = EnpointUrl.LIST_HUYEN+"?province.id_eq=$provinceId";
    String _endpoint = provinceId != null && provinceId.toString().toLowerCase() != "null" ? EnpointUrl.LIST_HUYEN+"?province.id_eq=$provinceId" : EnpointUrl.LIST_HUYEN;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = DistrictResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

