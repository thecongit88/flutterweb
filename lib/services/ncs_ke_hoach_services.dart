import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';

class NcsKeHoachServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String _endpoint = EnpointUrl.HBCC_KE_HOACH;

  NcsKeHoachServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedNcsId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? ncsId = await preferences.getInt('memberId');
    return ncsId ?? 0;
  }

  //list ke hoach cua hbcc va loc theo thang/nam + member
  Future<List<HbccItemKeHoach>> listKeHoachNcs({int? month, int? year, int pg = 1}) async {
    try{
      var fr = (pg-1) * Constants.PAGE_SIZE;
      var lm = Constants.PAGE_SIZE;

      String param = "";
      List<String> params = [];
      if(month != null && month > 0) {
        params.add("month=$month");
      }
      if(year != null && year > 0) {
        params.add("year=$year");
      }

      /*if(memberId != null && memberId != 0) {
        params.add("member.id_eq=$memberId");
      }*/
      if(params.length > 0) param = params.join("&");
      if(param.trim() != "") param = "${param}&";

      int ncsId = await getSignedNcsId();
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&_start=$fr&_limit=$lm&${param}ncs.id_eq=$ncsId");
      var result = HbccItemKeHoachResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      throw(e);
    }
  }
}
