
class EnpointUrl {

/*
  static const String ROOT_URL = "http://192.168.1.28:1337";
  static const String BASE_URL = "http://192.168.1.28:1337/";
  static final URL_CLOUD = 'http://192.168.1.28:1337';
*/

  static const String ROOT_URL = "http://118.70.133.2:1337/";
  static const String BASE_URL = "http://118.70.133.2:1337/";
  static const String URL_CLOUD = 'http://118.70.133.2:1337';


/*
  static const String ROOT_URL = "http://141.164.44.67:1337/";
  static const String BASE_URL = "http://141.164.44.67:1337/";
  static const String URL_CLOUD = 'http://141.164.44.67:1337';
*/
  static const String AUTHENTICATE_URL = ROOT_URL+"TokenAuth/Authenticate";
  static const String GET_BANNER_INFO = BASE_URL + "/get_banner_info";

  static const String TRANSACTION_URL_CREATE = BASE_URL+"Transaction/Create";
  static const String TRANSACTION_URL_UPDATE = BASE_URL+"Transaction/Update";
  static const String TRANSACTION_URL_DELETE = BASE_URL+"Transaction/Delete";
  static const String TRANSACTION_URL_GET = BASE_URL+"Transaction/Get";
  static const String TRANSACTION_URL_LIST = BASE_URL+"Transaction/GetList";
  static const String TRANSACTION_URL_LASTEST = BASE_URL+"Transaction/GetLastest";

  static const String WALLET_URL_GET = BASE_URL+"Wallet/GetWallet";

  static const String USER_URL_BASE = "users";
  static const String AUTH_URL_REGISTER = "auth/local/register";
  static const String AUTH_URL_LOGIN = "auth/local";
  static const String USER_URL_ME = USER_URL_BASE + "me";
  static const String AUTH_URL_CHANGEPASS = "auth/change-password";

  static const String CONTACT_URL_GET = BASE_URL+"Contact/Get";
  static const String CONTACT_URL_LIST = BASE_URL+"Contact/GetList";


  static const String MEMBER_URL_CREATE = BASE_URL+"Member/Create";
  static const String MEMBER_URL_UPDATE = BASE_URL+"Member/Update";
  static const String MEMBER_URL_DELETE = BASE_URL+"Member/Delete";
  static const String MEMBER_URL_GET = BASE_URL+"Member/Get";
  //static const String MEMBER_URL_LIST = BASE_URL+"Member/GetList";
  static const String MEMBER_URL_LIST = "members";

  static const String VIDEO_CATEGORY_URL_LIST = "video-categories";
  static const String POST_CATEGORY_URL_LIST = "post-categories";
  static const String POST_URL = "posts";

  static const String FORM_TYPE_URL = "form-types";

  static const String LIST_VIDEO_BY_CATEGORY_URL = "videos";
  static const String LIST_PERSONAL_ASSISTANTS_URL = "personal-assistants";
  static const String LIST_HBCC_URL = "home-based-care-collaborators";

  static const String MEMBER_URL_COUNT = "members/count";
  static const String LIST_FORMITEMS_URL = "form-items";
  static const String LIST_FORMITEMS_COUNT_URL = "form-items/count";
  static const String UPLOAD_URL = "upload";


  static const String LIST_SKILL_GROUPS_URL = "skill-categories";
  static const String LIST_SKILL_URL = "skiils";
  static const String LIST_GENDER = "genders";
  static const String LIST_NATION = "nations";
  static const String LIST_TINH = "provinces";
  static const String LIST_HUYEN = "districts";
  static const String LIST_XA = "wards";
  static const String LIST_LOAI_KT = "disability-types";

  static const String PLAN_URL = "plans";
  static const String TANSUAT_URL = "tan-suats";
  static const String RELATIONSHIPTYPE_URL = "relationship-types";
  static const String EDU_LEVEL_URL = "edu-levels";
  static const String JOB_TYPE_URL = "job-types";
  static const String MARTERIAL_STATUS_URL = "marital-statuses";
  static const String DISABILITY_START_URL = "disability-starts";

  static const String FORM_KNCS_ITEMS_URL = "form-kncs-items";
  static const String REPORT_TANSUAT_NCS = "report-tansuat-ncs";
  static const String REPORT_TINHTRANG_NKT = "report-tinhtrang-nkts";
  static const String REPORT_THUCHIEN_KEHOACH = "report-thuchien-kehoaches";
  static const String REPORT_TINHTRANG_NKT_HBCC = "report-tinhtrangnkt-hbccs";
  static const String HBCC_KE_HOACH = "care-plans";
  static const String HBCC_KE_HOACH_ITEM = "care-plan-items";
  static const String HBCC_CHECKIN = "checkins";
  static const String HBCC_SUPPORT_CONSERVATION = "support-conversations";
  static const String HBCC_SUPPORT_MESSAGE = "support-messages";
  static const String FILE_UPLOAD = "upload";
}