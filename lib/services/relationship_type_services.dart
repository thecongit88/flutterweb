import 'dart:convert';
import 'package:hmhapp/models/relationship_type.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class RelationshipTypeServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  RelationshipTypeServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<RelationshipType>> listRelationshipType() async {
    String _endpoint = EnpointUrl.RELATIONSHIPTYPE_URL + "?_sort=code:ASC";
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = RelationshipTypeResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

