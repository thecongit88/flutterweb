import 'dart:convert';
import 'package:hmhapp/dp_members/models/plan.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class PlansServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  dynamic isDeleted;

  PlansServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<Plan>> listPlans(int memberId) async {
    String _endpoint = EnpointUrl.PLAN_URL + "?_sort=id:ASC&member.id=$memberId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = PlansResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<Plan> getPlan(int planId) async {
    String _endpoint = EnpointUrl.PLAN_URL + "/$planId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = PlansResponse.fromJson(json.decode(response.body));
      return result.items.length > 0 ? result.items[0] : Plan();
    } catch(e) {
      throw(e);
    }
  }

  Future<Plan> getPlanBySkill(int skillId, int memberId) async {
    String _endpoint = EnpointUrl.PLAN_URL + "?skiil.id=$skillId&member.id=$memberId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = PlansResponse.fromJson(json.decode(response.body));
      return result.items.length > 0 ? result.items[0] : Plan();
    } catch(e) {
      throw(e);
    }
  }

  Future<Plan?> addPlan(Plan plan, int mId) async {
    //thêm mới plan
    String _endpoint = EnpointUrl.PLAN_URL;
    try{
      plan.userId = mId;
      Response response = await baseApi.postAsync(_endpoint, plan.toJson());
      var result = new PlanResponse.fromJson(json.decode(response.body));
      return result.plan;
    } catch(e) {
      throw(e);
    }
  }

  Future<Plan?> updatePlan(Plan plan, int planId) async {
    //thêm mới plan
    String _endpoint = EnpointUrl.PLAN_URL + "/$planId";
    try{
      Response response = await baseApi.putAsync(_endpoint, plan.toJson());
      var result = new PlanResponse.fromJson(json.decode(response.body));
      return result.plan;
    } catch(e) {
      throw(e);
    }
  }

  Future<Plan> deletePlan(int planId) async {
    String _endpoint = EnpointUrl.PLAN_URL + "/$planId";
    try{
      isDeleted = await baseApi.deleteAsync(_endpoint);
      return isDeleted;
    } catch(e) {
      throw(e);
    }
  }

}

