import 'dart:convert';
import 'package:hmhapp/dp_members/models/personal_assistant.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class PersonalAssistantServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  PersonalAssistantServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<PersonalAssistant> getPersonalAst(id) async {
    String endpoint = "${EnpointUrl.LIST_PERSONAL_ASSISTANTS_URL}?id_eq=$id";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = PersonalAssistantResponse.fromJson(json.decode(response.body));
      return result.items[0];
    }
    catch(e){
      rethrow;
    }
  }

  Future<List<PersonalAssistant>> listPersonalAssistants() async {
    String endpoint = EnpointUrl.LIST_PERSONAL_ASSISTANTS_URL;
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = PersonalAssistantResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      rethrow;
    }
  }

  Future<PersonalAssistant?> updatePersonalAssistant(PersonalAssistant mem, int memId) async {
    String endpoint = "${EnpointUrl.LIST_PERSONAL_ASSISTANTS_URL}/$memId";
    //try{
    Response response = await baseApi.putAsync(endpoint, mem.toJson());
    var result = PerResponse.fromJson(json.decode(response.body));
    return result.mem;
    //} catch(e) {
    //  throw(e);
    //}
  }

  Future<PersonalAssistant?> createNCS(PersonalAssistant per) async {
    String endpoint = EnpointUrl.LIST_PERSONAL_ASSISTANTS_URL;
    try{
      Response response = await baseApi.postAsync(endpoint, per.toJson());
      var result = PerResponse.fromJson(json.decode(response.body));
      return result.mem;
    } catch(e) {
      rethrow;
    }
  }
}

