import 'dart:convert';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class VideoListViewServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  VideoListViewServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<YoutubeModel>> listVideosByCatgoryId(category_video_id) async {

    String _endpoint = EnpointUrl.LIST_VIDEO_BY_CATEGORY_URL+"?video_category.id_eq=$category_video_id";

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new YoutubeModelResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<List<YoutubeModel>> listVideos() async {
    var lm = Constants.MAX_LASTEST_VIDEOS;
    var param = '&_start=0&_limit=$lm';
    String _endpoint = EnpointUrl.LIST_VIDEO_BY_CATEGORY_URL + '?$param';

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new YoutubeModelResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<List<YoutubeModel>> listRelatedVideosByCatgoryId(category_video_id, current_video_id) async {

    String _endpoint = EnpointUrl.LIST_VIDEO_BY_CATEGORY_URL+"?video_category.id_eq=$category_video_id&id_neq=$current_video_id";

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new YoutubeModelResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<VideosPageResponse> search(int pg,String? keyword) async {
    var fr = (pg-1) * Constants.PAGE_SIZE;
    var lm = Constants.PAGE_SIZE;
    var param = keyword!.isNotEmpty? 'title_contains=$keyword':'';
    param += '&_start=$fr&_limit=$lm';
    String _endpoint = EnpointUrl.LIST_VIDEO_BY_CATEGORY_URL+'?$param';

    try{
      var pages = 1; //await getPages(categoryId);
      Response response = await baseApi.getAsync(_endpoint);
      var result = new VideosPageResponse.fromJson(json.decode(response.body));
      result.pages = pages;
      return result;
    }
    catch(e){
      throw(e);
    }
  }


}