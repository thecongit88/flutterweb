import 'dart:convert';
import 'package:hmhapp/models/post_categories.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class PostCategoryServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String tokenAuth = "";

  PostCategoryServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<PostCategory>> listCategories(
      {bool hideEmpty: true, List<int>? excludeIDs}) async {

    String _endpoint = EnpointUrl.POST_CATEGORY_URL_LIST;

    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = PostCategoryResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

}