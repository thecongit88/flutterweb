import 'dart:convert';
import 'package:hmhapp/models/form_kncs_item.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:logging/logging.dart';

class FormKNCSServices{
  final Logger _logger = new Logger('API');

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  FormKNCSServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<Map> create(FormKNCSItem form,Map formObj,String formType) async {
    String _endpoint = EnpointUrl.FORM_KNCS_ITEMS_URL;
  try {
    var body = form.toJson();
    //Do form danh gia ky nang cham soc la form dong, nen object
    body["form_evaluate_skill"] = formObj;
    //body['${formType}'] = '${formId}';

      final response =
      await baseApi.postAsync(_endpoint,body);
      var result = json.decode(response.body);

      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future createMulti(List<FormKNCSItem> list,Map formObj,String formType) async {
    try {
      list.forEach((element) {
        this.create(element,formObj,formType);
      });
    }
    catch(e){
      throw(e);
    }
  }


  Future<Map> delete(int itemId) async {
    try {
      String _endpoint = "${EnpointUrl.FORM_KNCS_ITEMS_URL}/${itemId}";
      final response =
      await baseApi.deleteAsync(_endpoint);
      var result = json.decode(response.body);

      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future deleteMulti(List<FormKNCSItem> list) async {
    try {
      list.forEach((element) {
        this.delete(element.id!);
      });
    }
    catch(e){
      throw(e);
    }
  }

}
