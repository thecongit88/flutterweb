import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/disability_type.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DisabilityTypeServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  DisabilityTypeServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<DisabilityType>> listLoaiKT() async {
    String _endpoint = EnpointUrl.LIST_LOAI_KT;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = DisabilityTypeResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

