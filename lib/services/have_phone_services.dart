import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/have_phone.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HavePhoneServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  HavePhoneServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<HavePhone>> listPhones() async {
    try{
      var result = HavePhoneResponse.fromJson(json.decode(Constants.resJsonYesNo));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }
}

