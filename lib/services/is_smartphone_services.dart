import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/da_cam.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/is_smartphone.dart';
import 'package:hmhapp/models/province.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IsSmartphoneServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  IsSmartphoneServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<IsSmartphone>> listSmartphones() async {
    try{
      var result = IsSmartphoneResponse.fromJson(json.decode(Constants.resJsonYesNo));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }
}

