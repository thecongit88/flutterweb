import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/disability_start.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/job_type.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DisabilityStartServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  DisabilityStartServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<DisabilityStart>> listDisabilityStarts() async {
    String _endpoint = EnpointUrl.DISABILITY_START_URL;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = DisabilityStartResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

