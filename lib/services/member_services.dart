import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId,memberId, userId;

  MemberServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  getSignedMemberId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    memberId = await preferences.getInt('memberId');
  }


  getSignedUserId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = await preferences.getInt('userId');
  }

  Future<List<Member>> listMembers() async {
    await getSignedHbccId();
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}?_sort=last_name:ASC&hbcc.id_eq=$hbccId";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      rethrow;
    }
  }

  Future<Member> getMember() async {
    await getSignedUserId();
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}?user.id_eq=$userId";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberResponse.fromJson(json.decode(response.body));
      return result.items[0];
    }
    catch(e){
      rethrow;
    }
  }


  setAvataUrl(String avataUrl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('avataUrl', avataUrl);
  }

  Future<Member> getMemberById(id) async {
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}?id_eq=$id";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberResponse.fromJson(json.decode(response.body));
      return result.items[0];
    }
    catch(e){
      rethrow;
    }
  }

  Future<MemberPageResponse> search(int? pg,String? keyword) async {
    var fr = (pg??1 - 1) * Constants.PAGE_SIZE;
    var lm = Constants.PAGE_SIZE;
    var param = keyword!.isNotEmpty ? 'full_name_contains=$keyword':'';
    await getSignedHbccId();
    param += '&hbcc.id_eq=$hbccId';
    param += '&_start=$fr&_limit=$lm';
    String endpoint = '${EnpointUrl.MEMBER_URL_LIST}?$param';
    print("Enpoint: ${endpoint}");
    try{
      var pages = 1;
      Response response = await baseApi.getAsync(endpoint);
      var result = MemberPageResponse.fromJson(json.decode(response.body));
      result.pages = pages;
      return result;
    }
    catch(e){
      rethrow;
    }
  }

  Future<int> countMembers(String param, int val) async {
    await getSignedHbccId();
    String endpoint = "${EnpointUrl.MEMBER_URL_COUNT}?hbcc.id_eq=$hbccId";

    if(param == "GioiTinh") endpoint += "&gender.id=$val";
    else if(param == "LoaiKhuyetTat") endpoint += "&disability_type.id=$val";

    try{
      Response response = await baseApi.getAsync(endpoint);
      var count = json.decode(response.body);
      return count;
    }
    catch(e){
      rethrow;
    }
  }

  Future<FileUpload> uploadAvatar(String filePath, String fileName) async {
    await getSignedMemberId();
    String endpoint = EnpointUrl.UPLOAD_URL;
    try{

      Map<String,String> fields = {
        'ref' : 'member',
        'refId' : '${memberId}',
        'field' : 'avatar',
      };
      var response = await baseApi.postFileAsync(endpoint,filePath,fileName,fields);
      var result = FileUploadResponse.fromJson(response.toString());
      if(result.items.length>0){
        setAvataUrl(result.items[0].formats!.thumbnail!.url!);
      }
      return result.items[0];
    }
    catch(e){
      rethrow;
    }
  }

  Future<Member?> updateMember(Member mem, int memId) async {
    String endpoint = "${EnpointUrl.MEMBER_URL_LIST}/$memId";
    try{
      Response response = await baseApi.putAsync(endpoint, mem.toJson());
      var result = MemResponse.fromJson(json.decode(response.body));
      return result.mem;
    } catch(e) {
      rethrow;
    }
  }

  Future<Member?> createMember(Member mem) async {
    String endpoint = EnpointUrl.MEMBER_URL_LIST;
    try{
      Response response = await baseApi.postAsync(endpoint, mem.toJson());
      var result = MemResponse.fromJson(json.decode(response.body));
      return result.mem;
    } catch(e) {
      rethrow;
    }
  }

  Future<int> getUserItem({userName: ""}) async {
    String endpoint = "${EnpointUrl.USER_URL_BASE}?username_eq=${Uri.encodeQueryComponent(userName)}";
    try{
      Response response = await baseApi.getAsync(endpoint);
      var result = json.decode(response.body);
      return result.length;
    }
    catch(e){
      rethrow;
    }
  }
}

