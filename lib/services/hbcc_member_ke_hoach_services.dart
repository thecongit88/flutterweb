import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';

class HbccMemberKeHoachServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String _endpoint = EnpointUrl.HBCC_KE_HOACH;

  HbccMemberKeHoachServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<dynamic> create(HbccThemKeHoachModel hbccThemKeHoach) async {
    try {
      final response =
      await baseApi.postAsync(_endpoint, hbccThemKeHoach.toJson());
      return json.decode(response.body);
    }catch(ex){
      throw ex;
    }
  }

  Future<List<HbccItemKeHoach>> listKeHoachMember({int pg = 1, int? memberId}) async {
    try{
      var fr = (pg-1) * Constants.PAGE_SIZE;
      var lm = Constants.PAGE_SIZE;
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&_start=$fr&_limit=$lm&member.id_eq=$memberId");
      var result = HbccItemKeHoachResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      throw(e);
    }
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? hbccId = await preferences.getInt('hbccId');
    return hbccId ?? 0;
  }

  Future<dynamic> update(HbccThemKeHoachModel hbccThemKeHoach, int id) async {
    try {
      final response =
      await baseApi.putAsync("$_endpoint/$id", hbccThemKeHoach.toJson());
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

  Future<Map> delete(int id) async {
    try {
      final response =
      await baseApi.deleteAsync("$_endpoint/$id");
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

}
