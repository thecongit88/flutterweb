import 'dart:convert';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/models/gender.dart';
import 'package:hmhapp/models/marital_status.dart';
import 'package:hmhapp/models/nation.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MaritalStatusServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  MaritalStatusServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<MaritalStatus>> listMaritalStatus() async {
    String _endpoint = EnpointUrl.MARTERIAL_STATUS_URL;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = MaritalStatusResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

