import 'dart:convert';
import 'package:hmhapp/dp_managers/models/formitemlogbook.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/services/member_services.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormItemLogbookServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId, memberId;
  String? selectedDate;


  FormItemLogbookServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  getSignedMemberId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    memberId = await preferences.getInt('memberId');
  }

  setDaCoDanhGia(bool? dacodanhgia) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool('dacodanhgia', dacodanhgia ?? false);
  }

  setSelectedDate(String? selectedDate) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('selectedDate', selectedDate ?? "");
  }

  Future<List<FormItemLogBook>> getListFormItemsLogBook() async {
    await getSignedHbccId();
    var lm = Constants.PAGE_SIZE;
    String _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&hbcc.id=$hbccId&_limit=$lm&is_ncs=false";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormItemLogBookResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  //lấy danh sách các đánh giá của các member mà nó thuộc về hbcc đang đăng nhập
  Future<List<int>> getMembersInLoggedHbcc() async {
    var memberServices = new MemberServices();
    var listMemberId = <int>[];
    var listMember = <Member>[];
    listMember = await memberServices.listMembers();
    listMember.forEach((element) {
      if(element.id! > 0){
        listMemberId.add(element.id!);
      }
    });
    return listMemberId;
  }

  Future<List<FormItemLogBook>> getAllListFormItemsLogBookHBCC(formDate) async {
    String _endpoint;
    await getSignedHbccId();
    var lm = Constants.PAGE_SIZE;

    //lấy danh sách các đánh giá của các member mà nó thuộc về hbcc đang đăng nhập
    List<int> listMemberId = await getMembersInLoggedHbcc();
    String surveyInMembersParam = listMemberId.join("&member_in=");
    surveyInMembersParam = listMemberId !=null && listMemberId.length > 0 ? "member_in=${surveyInMembersParam}" : "member_in=0";

    if(formDate != "") {
      //_endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&hbcc.id=$hbccId&form_date_eq=$formDate&is_ncs=false";
      _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&${surveyInMembersParam}&form_date_eq=$formDate&is_ncs=false";
    }
    else {
      //_endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&hbcc.id=$hbccId&is_ncs=false";
      _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&${surveyInMembersParam}&is_ncs=false";
    }

    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormItemLogBookResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e){
      throw(e);
    }
  }

  /* Hàm này dùng trong tab Khảo Sát (Member - Người Chăm sóc), khi bấm vào phần tháng để lọc danh sách */
  Future<List<FormItemLogBook>> getAllListFormItemsLogBookNCS(formDate) async {
    String _endpoint;
    await getSignedMemberId();
    if(formDate != "")
      _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&member.id=$memberId&form_date_eq=$formDate&is_ncs=true";
    else
      _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?_sort=id:DESC&member.id=$memberId&is_ncs=true";

    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormItemLogBookResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  /*
  mỗi tháng chỉ dc tạo 1 báo cáo, 1 ngày chỉ dc 1 đánh giá, hàm này kiểm tra việc đó
  type: day or month
  * */
  Future<bool> isValidForCreateSurvey(memberId, formDate, type) async {
    bool isValid = true;
    if(type == "day") {
      String _endpoint = EnpointUrl.LIST_FORMITEMS_COUNT_URL+"?member.id=$memberId&form_date_eq=$formDate&is_ncs=true&form_type.form_day=true";
      Response response = await baseApi.getAsync(_endpoint);
      var result = json.decode(response.body);
      if(result > 0) isValid = false;
    }
    if(type == "month") {
      var beginCurrentMonth = DateTime(formDate.year, formDate.month, 1);
      var nextCurrentMonth = DateTime(formDate.year, formDate.month + 1, 1);
      String _endpoint = EnpointUrl.LIST_FORMITEMS_COUNT_URL+"?member.id=$memberId&form_date_gte=$beginCurrentMonth&form_date_lt=$nextCurrentMonth&is_ncs=true&form_type.form_day=false";
      //print(_endpoint);
      Response response = await baseApi.getAsync(_endpoint);
      var result = json.decode(response.body);
      if(result > 0) isValid = false;
    }

    return isValid;
  }

  /* Đếm số lượng đánh giá đã thực hiện của 1 member trong khoảng ngày */
  Future<int> countSurveyExcuted(memberId, fromDate, toDate) async {
    String _endpoint = EnpointUrl.LIST_FORMITEMS_COUNT_URL+"?member.id=$memberId&form_date_gte=$fromDate&form_date_lte=$toDate&is_ncs=true";
    Response response = await baseApi.getAsync(_endpoint);
    var result = json.decode(response.body);
    return result;
  }

  /* Hàm get 1 Form-Item trong bảng FormItems dựa vào Form Id và Is_ncs */
  Future<List<FormItemLogBook>> getFormItemsLogBook(form_date, formId, memId, is_ncs) async {
    String _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"?form_date=$form_date&form_id=$formId&member.id=$memId&is_ncs=$is_ncs";

    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormItemLogBookResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }

  Future<Map> create(FormItemLogBook formItem) async {
    String endpoint = EnpointUrl.LIST_FORMITEMS_URL;
    try {
      final response =
      await baseApi.postAsync(endpoint,formItem.toJson());
      return response;
        //else if(response.statusCode == 403) {
        //throw "Bạn không có quyền truy cập dữ liệu.";
        //} else if(response.statusCode == 404) {
        //throw "Không tìm thấy tài nguyên.";
        //} else if(response.statusCode == 403) {
        //throw Exception();
        //}
    }catch(ex){
      throw ex;
    }
  }

  Future<dynamic> delete(int id) async {
    try {
      String _endpoint = EnpointUrl.LIST_FORMITEMS_URL+"/$id";
      final response = await baseApi.deleteAsync(_endpoint);
      var result = json.decode(response.body);
      return result;
    }catch(ex){
      throw ex;
    }
  }

}

