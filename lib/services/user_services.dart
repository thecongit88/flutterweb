import 'dart:io';
import 'package:hmhapp/models/user.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class UserServices{
  String tokenAuth = "";
  UserServices(){
    getToken();
  }
  getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    tokenAuth = (await preferences.getString('accessToken'))!;
  }
  setToken(String token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('accessToken',token);
  }


  Future<User?> currentUser() async {
    var url = Uri.parse(EnpointUrl.USER_URL_ME);
    await getToken();
    Map<String,String> headers = {
      'Content-type' : 'application/json',
      'Authorization' : 'Bearer $tokenAuth'
    };
    try {
      final response =
      await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        return User.fromJson(body);
      }
      //else if(response.statusCode == 403) {
        //throw "Bạn không có quyền truy cập dữ liệu.";
      //} else if(response.statusCode == 404) {
        //throw "Không tìm thấy tài nguyên.";
      //} else if(response.statusCode == 403) {
        //throw Exception();
      //}
    }catch(ex){
      throw ex;
    }
  }

}