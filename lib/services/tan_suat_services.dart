import 'dart:convert';
import 'package:hmhapp/models/tan_suat.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';

class TanSuatServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;

  TanSuatServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  Future<List<TanSuatCLass>> listTanSuats() async {
    String _endpoint = EnpointUrl.TANSUAT_URL;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = TanSuatClassResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }
}

