import 'dart:convert';
import 'dart:io';
import 'package:hmhapp/dp_managers/models/hbcc.dart';
import 'package:hmhapp/models/file_upload.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HbccServices{
  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId;

  HbccServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = (await preferences.getInt('hbccId'))!;
  }


  Future<HBCC?> getHBCC() async {
    await getSignedHbccId();
    String _endpoint = EnpointUrl.LIST_HBCC_URL+"?id_eq=$hbccId";
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new HBCCResponse.fromJson(json.decode(response.body));
      return result.items.length>0 ? result.items[0] : null;
    }
    catch(e){
      throw(e);
    }
  }


  setAvataUrl(String _avataUrl) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('avataUrl', _avataUrl);
  }


  Future<FileUpload> uploadAvatar(String filePath, String fileName) async {
    await getSignedHbccId();
    String _endpoint = EnpointUrl.UPLOAD_URL;
    try{

      Map<String,String> fields = {
        'ref' : 'home-based-care-collaborator',
        'refId' : '${hbccId}',
        'field' : 'avatar',
      };
      var response = await baseApi.postFileAsync(_endpoint,filePath,fileName,fields);
      var result = new FileUploadResponse.fromJson(response.toString());
      if(result.items.length>0){
        setAvataUrl(result.items![0].formats!.thumbnail!.url!);
      }
      return result.items[0];
    }
    catch(e){
      throw(e);
    }
  }


  Future<HBCC?> createHBCC(HBCC hbcc) async {
    String _endpoint = EnpointUrl.LIST_HBCC_URL;
    try{
      Response response = await baseApi.postAsync(_endpoint, hbcc.toJson());
      var result = new HBCCSingleResponse.fromJson(json.decode(response.body));
      return result.hbcc;
    } catch(e) {
      throw(e);
    }
  }

}

