import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:hmhapp/models/form_survey.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_item_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_them_ke_hoach.dart';
import '../dp_managers/models/hbcc_ke_hoach/hbcc_update_ke_hoach.dart';

class HbccKeHoachServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  String _endpoint = EnpointUrl.HBCC_KE_HOACH;

  HbccKeHoachServices(){
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int? hbccId = await preferences.getInt('hbccId');
    return hbccId ?? 0;
  }

  //list ke hoach cua hbcc va loc theo thang/nam + member
  Future<List<HbccItemKeHoach>> listKeHoachHbcc({int? month, int? year, int? memberId, int pg = 1}) async {
    try{
      var fr = (pg-1) * Constants.PAGE_SIZE;
      var lm = Constants.PAGE_SIZE;

      String param = "";
      List<String> params = [];
      /*month = month ?? DateTime.now().month;
      params.add("month=$month");
      year = year ?? DateTime.now().year;
      params.add("year=$year");*/
      if(month != null && month > 0) {
        params.add("month=$month");
      }
      if(year != null && year > 0) {
        params.add("year=$year");
      }
      if(memberId != null && memberId != 0) {
        params.add("member.id_eq=$memberId");
      }
      if(params.length > 0) param = params.join("&");
      if(param.trim() != "") param = "${param}&";

      int hbccId = await getSignedHbccId();
      Response response = await baseApi.getAsync("$_endpoint?_sort=id:DESC&_start=$fr&_limit=$lm&${param}hbcc.id_eq=$hbccId");
      var result = HbccItemKeHoachResponse.fromJson(json.decode(response.body)).items;
      return result;
    }
    catch(e){
      throw(e);
    }
  }
}
