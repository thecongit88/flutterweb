import 'dart:convert';
import 'package:hmhapp/models/report_tan_suat_ncs.dart';
import 'package:hmhapp/models/report_thuchien_kehoach.dart';
import 'package:hmhapp/models/report_tinhtrangnkt_hbcc.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReportTinhtrangnktHbccServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId;

  ReportTinhtrangnktHbccServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
  }

  getSignedHbccId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    hbccId = await preferences.getInt('hbccId');
  }

  Future<List<ReportTinhTrangNktHbcc>> listReportTinhtrangnktHbcc(int month, int year) async {
    await getSignedHbccId();
    String _endpoint = month == 0 ?
        EnpointUrl.REPORT_TINHTRANG_NKT_HBCC + "?year=$year&hbcc.id=$hbccId"
        :
    EnpointUrl.REPORT_TINHTRANG_NKT_HBCC + "?month=$month&year=$year&hbcc.id=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTinhTrangNktHbccResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<List<ReportTinhTrangNktHbcc>> listReportTinhtrangnktHbccTheoKhoangThang(int fromMonth, int toMonth, int year) async {
    await getSignedHbccId();
    String _endpoint = EnpointUrl.REPORT_TINHTRANG_NKT_HBCC + "?month_gte=$fromMonth&month_lte=$toMonth&year=$year&hbcc.id=$hbccId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = ReportListTinhTrangNktHbccResponse.fromJson(json.decode(response.body));
      return result.items;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTinhTrangNktHbcc?> add(ReportTinhTrangNktHbcc reportItem) async {
    await getSignedHbccId();
    reportItem.hbccId = this.hbccId;
    String _endpoint = EnpointUrl.REPORT_TINHTRANG_NKT_HBCC;
    try{
      Response response = await baseApi.postAsync(_endpoint, reportItem.toJson());
      var result = new ReportTinhTrangNktHbccResponse.fromJson(json.decode(response.body));
      return result.reportTinhTrangNktHbcc;
    } catch(e) {
      throw(e);
    }
  }

  Future<ReportTinhTrangNktHbcc?> update(ReportTinhTrangNktHbcc reportItem) async {
    String _endpoint = EnpointUrl.REPORT_TINHTRANG_NKT_HBCC + "/${reportItem.id}";
    try{
      Response response = await baseApi.putAsync(_endpoint, reportItem.toJson());
      var result = new ReportTinhTrangNktHbccResponse.fromJson(json.decode(response.body));
      return result.reportTinhTrangNktHbcc;
    } catch(e) {
      throw(e);
    }
  }
}

