import 'dart:convert';
import 'package:hmhapp/models/form_types.dart';
import 'package:hmhapp/services/auth_services.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormTypeServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  late AuthServices _authServices;

  String tokenAuth = "";

  FormTypeServices(){
    getToken();
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
    _authServices = new AuthServices();
  }


  getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    tokenAuth = (await preferences.getString('accessToken'))!;
  }

  Future<List<FormType>> listFormTypes() async {
    var userName = await _authServices.getUserName();
    //chỉ hiển thị tất cả các loại đánh giá với số 0936458438
    String _endpoint = userName=="0936458438" ?
        EnpointUrl.FORM_TYPE_URL+"?_sort=id:ASC&form_ncs=false"
        :
        EnpointUrl.FORM_TYPE_URL+"?_sort=id:ASC&form_ncs=false&is_active=true"
    ;
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormTypeResponse.fromJson(json.decode(response.body));
      return result.items;
    }
    catch(e){
      throw(e);
    }
  }


  Future<FormType?> getFormType(int id) async {
    if (id == null) {
      return null;
    }
    String _endpoint = EnpointUrl.FORM_TYPE_URL+'/$id';
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormType.fromJson(json.decode(response.body));
      return result;
    }
    catch(e){
      throw(e);
    }
  }

  Future<FormType> getFormTypeNCS(bool isFormDay) async {
    String _endpoint = EnpointUrl.FORM_TYPE_URL+'?form_ncs=true&form_day=$isFormDay';
    try{
      Response response = await baseApi.getAsync(_endpoint);
      var result = new FormTypeResponse.fromJson(json.decode(response.body));
      return result.items.length > 0 ? result.items[0] : FormType();
    }
    catch(e){
      throw(e);
    }
  }
}
