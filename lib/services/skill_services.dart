import 'dart:convert';
import 'package:hmhapp/dp_members/models/skill.dart';
import 'package:hmhapp/dp_members/models/skill_groups.dart';
import 'package:hmhapp/services/base_api.dart';
import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SkillServices{

  late BaseAPI baseApi;
  String? url;
  bool? isSecure;
  int? hbccId, userId;

  SkillServices() {
    baseApi = BaseAPI(url: EnpointUrl.URL_CLOUD);
    isSecure = true;
  }

  //skillType = 0, 1, 2 (1 là tự thực hiện, 2 phục thuộc hoàn toàn)
  Future<int> countSkills({int? skillId, int? skillType}) async {
    String _endpoint = EnpointUrl.LIST_SKILL_URL + "/count?id=$skillId&skill_type=$skillType";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = json.decode(response.body);
      return result;
    } catch(e) {
      throw(e);
    }
  }

  Future<Map<String, dynamic>> getSkill({int? skillId}) async {
    String _endpoint = EnpointUrl.LIST_SKILL_URL + "/$skillId";
    try {
      Response response = await baseApi.getAsync(_endpoint);
      var result = json.decode(response.body);
      return result;
    } catch(e) {
      throw(e);
    }
  }
}

