library json_to_form;

import 'dart:convert';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class JsonSchema extends StatefulWidget {

  final Map errorMessages;
  final Map validations;
  final Map decorations;
  final String? form;
  final Map formMap;
  final double padding;
  final Widget? buttonSave;
  final Widget? selectUsers;
  final Widget? chooseDate;
  final Function? actionSave;
  final ValueChanged<dynamic> onChanged;

  const JsonSchema({
    this.form,
    required this.onChanged,
    this.padding = 0,
    required this.formMap,
    this.errorMessages = const {},
    this.validations = const {},
    this.decorations = const {},
    this.buttonSave,
    this.selectUsers,
    this.chooseDate,
    this.actionSave,
  });

  @override
  _CoreFormState createState() =>
      new _CoreFormState(formMap ?? json.decode(form ?? ""));
}

class _CoreFormState extends State<JsonSchema> {
  final dynamic formGeneral;

  String radioValue = "false";
  bool switchValue = false;
  bool checkValue = false;
  String selectValue = "";
  String selectDate = "";
  String selectTime = "";
  String selectDateTime = "";
  double scores = 0.0;

  var _tmpChangeCheckLabel = <String>[];
  Map<String,bool> hiddenElements = new Map<String,bool>();
  //Danh sach cac TextField Controller, su dung de tụ dong cap nhat khoang ngay cua Danh gia chung
  Map<String,TextEditingController> durationElements = new Map<String,TextEditingController>();
  //Danh sach cac Selectbox, cua Danh gia kha nang thuc hien
  Map<String,String> selectElements = new Map<String,String>();
  Map<String,String> selectValueElements = new Map<String,String>();
  Map<String,TextEditingController> selectResultElements = new Map<String,TextEditingController>();

// Value tat ca element da chon
  Map<String,String> selectValues = new Map<String,String>();
  DateTime _formDate = DateTime.now();
  String _dateLabel = "Chọn ngày";

  // validators

  String? isRequired(item, value) {
    if (value.isEmpty) {
      return widget.errorMessages[item['key']] ?? 'Vui lòng điên hoặc chọn thông tin.';
    }
    return null;
  }

  String? validateEmail(item, String value) {
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      return null;
    }
    return 'Email is not valid';
  }

  bool labelHidden(item) {
    if (item.containsKey('hiddenLabel')) {
      if (item['hiddenLabel'] is bool) {
        return !item['hiddenLabel'];
      }
    } else {
      return true;
    }
    return false;
  }

  // Return widgets

  bool checkConditionAND(conditional_elements){
    var result = true;
    if(conditional_elements != null && conditional_elements.length>0){
      for(var z = 0; z< conditional_elements.length; z++){
        var cel = conditional_elements[z];
        var  cel_key = cel["key"];
        var  cel_value = cel["value"];
        var  cel_operator = cel["operator"];
        var  cel_type = cel["type"];
        if(cel_type == "calculator"){
          var cel_choose_value = calFormula(cel_key,selectValues);
          cel_value = double.parse(cel_value);
          switch (cel_operator) {
            case "==":
              result = (cel_choose_value == cel_value);
              break;
            case "!=":
              result = (cel_choose_value != cel_value);
              break;
            case ">":
              result = (cel_choose_value > cel_value);
              break;
            case "<":
              result = (cel_choose_value < cel_value);
              break;
            case ">=":
              result = (cel_choose_value >= cel_value);
              break;
            case "<=":
              result = (cel_choose_value <= cel_value);
              break;
          }
        }else {
          var cel_choose_value = selectValues[cel_key];
          switch (cel_operator) {
            case "==":
              result = (cel_value == cel_choose_value);
              break;
            case "!=":
              result = (cel_value != cel_choose_value);
              break;
            case ">":
              result = (cel_value > cel_choose_value);
              break;
            case "<":
              result = (cel_value < cel_choose_value);
              break;
            case ">=":
              result = (cel_value >= cel_choose_value);
              break;
            case "<=":
              result = (cel_value <= cel_choose_value);
              break;
            case "in":
              result = cel_choose_value != null &&
                  cel_choose_value.contains(cel_value);
              break;
          }
        }
        if(result == false){
          break;
        }
      }
    }
    return result;
  }

  bool checkConditionOR(conditional_elements){
    var result = true;
    if(conditional_elements != null && conditional_elements.length>0){
      for(var z = 0; z< conditional_elements.length; z++){
        var cel = conditional_elements[z];
        var  cel_key = cel["key"];
        var  cel_value = cel["value"];
        var  cel_operator = cel["operator"];
        var  cel_type = cel["type"];
        if(cel_type == "calculator"){
          var cel_choose_value = calFormula(cel_key,selectValues);
          cel_value = double.parse(cel_value);
          switch (cel_operator) {
            case "==":
              result = (cel_choose_value == cel_value);
              break;
            case "!=":
              result = (cel_choose_value != cel_value);
              break;
            case ">":
              result = (cel_choose_value > cel_value);
              break;
            case "<":
              result = (cel_choose_value < cel_value);
              break;
            case ">=":
              result = (cel_choose_value >= cel_value);
              break;
            case "<=":
              result = (cel_choose_value <= cel_value);
              break;
          }
        }else {
          var cel_choose_value = selectValues[cel_key];
          switch (cel_operator) {
            case "==":
              result = (cel_value == cel_choose_value);
              break;
            case "!=":
              result = (cel_value != cel_choose_value);
              break;
            case ">":
              result = (cel_value > cel_choose_value);
              break;
            case "<":
              result = (cel_value < cel_choose_value);
              break;
            case ">=":
              result = (cel_value >= cel_choose_value);
              break;
            case "<=":
              result = (cel_value <= cel_choose_value);
              break;
            case "in":
              result = cel_choose_value != null &&
                  cel_choose_value.contains(cel_value);
              break;
          }
        }
        if(result == true){
          break;
        }
      }
    }
    return result;
  }

  double calFormula(String formula,Map<dynamic,dynamic> items){
    double res = 0;
    if(formula != null && formula != "") {
      RegExp exp = RegExp(r'(\+)');
      Iterable<RegExpMatch> matches = exp.allMatches(formula);
      var arr = formula.split(exp);
      for (var m in arr) {
        try{
          res += double.parse(items[m]);
        }catch(ex){

        }
      }
    }
    return res;
  }



  bool checkIsHidden(item){
    //"conditional_type": "show",
    //"conditional_logic": "1"
    var res = false;
    var isHidden = false;
    var conditionalType = item["conditional_type"];
    var conditionalLogic = item["conditional_logic"];
    //Ktra xem elemnt co check logic khong? == 1 la kieu AND
    if(conditionalLogic == "1"){
      //Get mang logic
      var conditionals = item["conditionals"];
      if(conditionals != null && conditionals.length>0){
        //Duyet qua toan bo mang logic, moi item cua mang logic nay theo dieu kien OR
        for (var i = 0; i < conditionals.length; i++) {
          //Get item cua mang logic, item la 1 mang cac logic theo dieu kien dạng AND
          var conditional_elements = conditionals[i];
          var conditionAND = checkConditionAND(conditional_elements);
          if(conditionAND == true){
            res = conditionAND;
            break;
          }
        }
        if(res==true){
          if(conditionalType == "show"){
            isHidden = false;
          }else{
            isHidden = true;
          }
        }else{
          if(conditionalType == "hide"){
            isHidden = false;
          }else{
            isHidden = true;
          }
        }

      }else{
        isHidden = false;
      }
    }else if(conditionalLogic == "2"){ //Check kieu OR
      var conditionals = item["conditionals"];
      if(conditionals != null && conditionals.length>0){
        //Duyet qua toan bo mang logic, moi item cua mang logic nay theo dieu kien OR
        for (var i = 0; i < conditionals.length; i++) {
          //Get item cua mang logic, item la 1 mang cac logic theo dieu kien dạng AND
          var conditional_elements = conditionals[i];
          var conditionAND = checkConditionOR(conditional_elements);
          if(conditionAND == true){
            res = conditionAND;
            break;
          }
        }
        if(res==true){
          if(conditionalType == "show"){
            isHidden = false;
          }else{
            isHidden = true;
          }
        }else{
          if(conditionalType == "hide"){
            isHidden = false;
          }else{
            isHidden = true;
          }
        }

      }else{
        isHidden = false;
      }
    }
    else{
      isHidden = false;
    }
    return isHidden;
  }


  List<Widget> jsonToForm() {
    String result = "";

    List<Widget> listWidget = <Widget>[];
    if (formGeneral['result'] != null) {
      result = formGeneral['result'];
    }

    if (formGeneral['title'] != null) {
      listWidget.add(
          Padding(padding: EdgeInsets.only(left: 15.0,right:15.0 ),
              child: Text( formGeneral['title'], style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),)
          ));
    }
    if (formGeneral['description'] != null) {
      listWidget.add(
          Padding(padding: EdgeInsets.only(left: 15.0,right:15.0,top: 5.0,bottom: 10.0),
              child:
              Text(
                formGeneral['description'],
                style: new TextStyle(fontSize: 14.0,),
              )));
    }

    if (widget.selectUsers != null) {
      listWidget.add(widget.selectUsers!);
    }

    if (widget.chooseDate != null) {
      listWidget.add(widget.chooseDate!);
    }

    for (var count = 0; count < formGeneral['fields'].length; count++) {
      Map item = formGeneral['fields'][count];
      var key =  item['key'];

      if(item['type'] == "Panel"){
        List<Widget> fieldChilds = jsonToChildForm(item);
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
            var panel    = Column  (
              children: fieldChilds
            );
            listWidget.add(panel);
        }
      }

      if (item['type'] == "Label") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          listWidget.add(
            new Container(
                margin: new EdgeInsets.only(top: 25.0, left: 15.0, right: 15.0),
                child: new Text(
                  item['label'],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold ,fontSize: 18.0),
                )
            ),
          );
        }
      }

      if (item['type'] == "RadioButton") {
        radioValue = item['value'];
        selectValues[key] = radioValue.toString();
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          List<Widget> radios = [];

          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          radioValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (radioValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          for (var i = 0; i < item['items'].length; i++) {
            radios.add(
                Row(
                    children: <Widget>[
                      new Radio<String>(
                          value: formGeneral['fields'][count]['items'][i]['value'].toString(),
                          groupValue: radioValue,
                          onChanged: (String? value) {
                            this.setState(() {
                              radioValue = value!;

                              //Gai tri moi
                              selectValues[key] = value;

                              formGeneral['fields'][count]['value'] = value;

                              if(item['scores']!=null) {
                                scores += double.parse(item['scores'][i]['score']);
                                formGeneral['fields'][count]['score'] = item['scores'][i]['score'];
                              }
                              _handleChanged();


                              //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                              if(formGeneral['result']!=null && formGeneral['result'] == 'conclude'){
                                //Xac dinh key cua El hien thi ket qua cua Form
                                var resultTo = formGeneral['result_to'];

                                //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                                if(item["ignore"] == null || item["ignore"]!= "1") {
                                  selectElements[key] = value;
                                }

                                var title = "0";
                                if(selectElements.containsValue("5")){
                                  title = "5";
                                }else if(selectElements.containsValue("4")){
                                  title = "4";
                                }else if(selectElements.containsValue("3")){
                                  title = "3";
                                }else if(selectElements.containsValue("2")){
                                  title = "2";
                                }else{
                                  title = "1";
                                }

                                var resultItems = selectValueElements[resultTo];
                                var resultText = json.decode(resultItems!)[int.parse(title)-1];
                                selectResultElements[resultTo]!.text = resultText['label'];

                                for (var count = 0; count < formGeneral['fields'].length; count++) {
                                  Map item = formGeneral['fields'][count];
                                  if(resultTo == item['key']){
                                    formGeneral['fields'][count]['value'] = resultText['label'];
                                  }
                                }
                              }
                            });
                          }),
                      formGeneral['fields'][count]['items'][i]['size']=="lg"?
                      Expanded(
                          child:
                          new Text(formGeneral['fields'][count]['items'][i]['label'],softWrap:true)
                      ):
                      Wrap(
                        children: <Widget>[
                          new Text(formGeneral['fields'][count]['items'][i]['label'],softWrap:true)
                        ],
                      ),
                    ])
            );
          }

          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, left: 0, right: 15.0),
              child:
              layout == 'column' ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: radios,
              ) :
              Row(
                children: radios,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Switch") {
        var val = formGeneral['fields'][count]['value'] != null &&
            formGeneral['fields'][count]['value'].toLowerCase() == 'true';
        selectValues[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (item['value'] == null) {
            formGeneral['fields'][count]['value'] = false;
          }
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (switchValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 0, right: 15.0),
              child: new Row(children: <Widget>[
                new Checkbox(
                  value: val,
                  onChanged: (bool? value) {
                    this.setState(() {
                      switchValue = value!;
                      formGeneral['fields'][count]['value'] = value.toString();
                      selectValues[key] = value.toString();
                      _handleChanged();
                    });
                  },
                ),
                new Text(item['label']),
              ]),
            ),
          );
        }
      }

      if (item['type'] == "Checkbox") {
        var val = formGeneral['fields'][count]['value'] != null &&
            formGeneral['fields'][count]['value'].toLowerCase() == 'true';
        selectValues[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          var checkValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (checkValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  /*
                    hiddenElements[snapshot] = true; //old code
                  */
                  /* fix code */
                  if(_tmpChangeCheckLabel.contains(snapshot) == false)
                    hiddenElements[snapshot] = true;
                  /* fix code */

                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          for (var i = 0; i < item['items'].length; i++) {
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: bool.fromEnvironment(formGeneral['fields'][count]['items'][i]['value'].toString()),
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = value;
                        selectValues[key] = value.toString();
                        formGeneral['fields'][count]['items'][i]['value'] = value;

                        /* fix code */
                        var _item_i_hidden_for = formGeneral['fields'][count]['items'][i]['hidden_for'].split(',');
                        int n = _item_i_hidden_for.length;
                        if(checkValue == true) {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.add(_item_i_hidden_for[i].toString().trim());
                          }
                        } else  {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.remove(_item_i_hidden_for[i].toString().trim());
                          }
                        }
                        /* fix code */

                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(formGeneral['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }


      if (item['type'] == "CheckboxList") {
        selectValues[key] = item['value'];

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          //Trộn dữ liệu -- Giap
          var val = formGeneral['fields'][count]['value'];
          if(val != null && val.isNotEmpty){
            var arr = val.split(",");
            for (var i = 0; i < item['items'].length; i++) {
              var label = item['items'][i]['label'];
              var ck = arr.contains(label);
              item['items'][i]['value'] = ck.toString();
            }
          }
          //

          for (var i = 0; i < item['items'].length; i++) {
            var check = formGeneral['fields'][count]['items'][i]['value'] != null &&
                formGeneral['fields'][count]['items'][i]['value'].toLowerCase() == 'true';
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: check,
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = value!;
                        formGeneral['fields'][count]['items'][i]['value'] = value.toString();
                        var checkListValues ="";
                        for (var i = 0; i < item['items'].length; i++) {
                          if(formGeneral['fields'][count]['items'][i]['value'] != null &&
                              formGeneral['fields'][count]['items'][i]['value'].toLowerCase() == 'true'){
                            checkListValues += formGeneral['fields'][count]['items'][i]['label']+",";
                          }
                        }
                        formGeneral['fields'][count]['value'] = checkListValues;
                        selectValues[key] = checkListValues;
                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(formGeneral['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Select") {
        selectValue = (selectValue == null && selectValue == "")?
        formGeneral['fields'][count]['value']:"";
        selectValues[key] = selectValue.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Text(item['label'],
                style:
                new TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0));
          }

          var hiddenValue = (item['hidden'] != null && item['hidden'].contains(",") == true) ? item['hidden'].split(',') : item['hidden'];
          //var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');
            this.setState(() {
              if (hiddenValue.contains(selectValue)) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          selectElements[key] = formGeneral['fields'][count]['value'];

          try{

            var expanded = item['expanded'];

            listWidget.add(new Container(
              margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  label,
                  new DropdownButtonFormField<String>(
                    validator: (value) {
                      if (item.containsKey('required')) {
                        if (item['required'] == true ||
                            item['required'] == 'True' ||
                            item['required'] == 'true') {
                          return isRequired(item, value);
                        }
                      }
                      return null;
                    },
                    isExpanded: (expanded == true),
                    hint: new Text("Select a user"),
                    value: formGeneral['fields'][count]['value'],
                    onChanged: (String? newValue) {
                      setState(() {
                        selectValue = newValue!;
                        selectValues[key] = newValue.toString();
                        formGeneral['fields'][count]['value'] = newValue;

                        //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                        if(formGeneral['result']!=null && formGeneral['result'] == 'conclude'){
                          //Xac dinh key cua El hien thi ket qua cua Form
                          var resultTo = formGeneral['result_to'];
                          //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                          if(item["ignore"] == null || item["ignore"]!= "1"){
                            selectElements[key] = newValue;
                          }

                          var title = "0";
                          if(selectElements.containsValue("5")){
                            title = "5";
                          }else if(selectElements.containsValue("4")){
                            title = "4";
                          }else if(selectElements.containsValue("3")){
                            title = "3";
                          }else if(selectElements.containsValue("2")){
                            title = "2";
                          }else{
                            title = "1";
                          }

                          var resultItems = selectValueElements[resultTo];
                          var resultText = json.decode(resultItems!)[int.parse(title)-1];
                          selectResultElements[resultTo]!.text = resultText['label'];

                          for (var count = 0; count < formGeneral['fields'].length; count++) {
                            Map item = formGeneral['fields'][count];
                            if(resultTo == item['key']){
                              formGeneral['fields'][count]['value'] = resultText['label'];
                            }
                          }
                        }
                        _handleChanged();
                      });
                    },
                    items:
                    item['items'].map<DropdownMenuItem<String>>((dynamic data) {
                      return DropdownMenuItem<String>(
                        value: data['value'],
                        child: new Text(
                          data['label'],
                          style: new TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ));

          }catch(ex){
            var a = selectValue;
          }
        }
      }

      if (item['type'] == "Input" ||
          item['type'] == "Password" ||
          item['type'] == "Email" ||
          item['type'] == "TextArea" ||
          item['type'] == "TextInput") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Container(
              child: new Text(
                item['label'],
                style: new TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 16.0),
              ),
            );
          }
          var lines = 1;
          if(item['lines'] != null && item['lines'] != "" && item['lines'] is int ){
            lines =  item['lines'];
          }

          //check enabled for input type
          bool enabled = true;
          if(item['enabled'] != null && item['enabled'] != "" && item['enabled'] == false){
            enabled =  false;
          }
          bool read_only = false;
          if(item['read_only'] != null && item['read_only'] != "" && item['read_only'] == true){
            read_only =  true;
          }

          var defaultValue = formGeneral['fields'][count]['value'] ?? "";

          var controller = new TextEditingController(text: defaultValue);
          if(durationElements.containsKey(key)) {
            controller = durationElements[key]!;
          }else{
            durationElements[key] = controller;
          }

          //Kiem tra form co phai la danh gia kieu result =  conclude
          //Lay key cua elemnt se hien thi ket qua 'result_to' cua Form
          //Kiem tra 'result_to' cua Element, xac dinh elment nay co phai la el hien thi ket qua khong?
          //Neu phai thi gan controller cua El nay vao map selectResultElements
          //Gan gia tri ket qua trong attr "result_items" cua El vao map selectValueElements
          if(formGeneral['result']!=null && formGeneral['result'] == 'conclude') {
            var resultTo = formGeneral['result_to'];
            if (item['result_to'] != null) {
              selectResultElements[resultTo] = controller;
              selectValueElements[resultTo] = json.encode(item["result_items"]);
            }
          }

          listWidget.add(new Container(
            margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                label,
                new TextFormField(
                  readOnly: read_only,
                  enabled: enabled,
                  controller: controller,
                  autofocus: false,
                  decoration: item['decoration'] ??
                      widget.decorations[item['key']] ??
                      new InputDecoration(
                        hintText: item['placeholder'] ?? "",
                        helperText: item['helpText'] ?? "",
                      ),
                  maxLines: item['type'] == "TextArea" ? lines : 1,
                  onChanged: (String value) {
                    formGeneral['fields'][count]['value'] = value;
                    _handleChanged();
                  },
                  obscureText: item['type'] == "Password" ? true : false,
                  validator: (value) {
                    if (widget.validations.containsKey(item['key'])) {
                      return widget.validations[item['key']](item, value);
                    }
                    if (item.containsKey('validator')) {
                      if (item['validator'] != null) {
                        if (item['validator'] is Function) {
                          return item['validator'](item, value);
                        }
                      }
                    }
                    if (item['type'] == "Email") {
                      return validateEmail(item, value!);
                    }

                    if (item.containsKey('required')) {
                      if (item['required'] == true ||
                          item['required'] == 'True' ||
                          item['required'] == 'true') {
                        return isRequired(item, value);
                      }
                    }

                    return null;
                  },
                ),
              ],
            ),
          ));
        }
      }

      if(item['type'] == "Date"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          if(item['value'] == null || item['value'].toString().isEmpty){
            formGeneral['fields'][count]['value'] =  DateTime.now().toString();
          }else{
            _dateLabel = new DateFormat(formatStr)
                .format(DateTime.parse(item['value']))
                .toString();
          }

          listWidget.add(
            Padding(
                padding: EdgeInsets.only(left: 15,right: 15),
                child:
                ElevatedButton(
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime(2000, 1, 1),
                        //maxTime: DateTime.now(),
                        onConfirm: (date) {
                          print('confirm $date');
                          _formDate = date;
                          setState(() {
                            _dateLabel = new DateFormat(formatStr)
                                .format(_formDate)
                                .toString();

                            formGeneral['fields'][count]['value'] =  _formDate.toString();
                            var displayFor = item['display_for'];
                            if(displayFor != null) {
                              var formDateStr = formGeneral["form_date"];
                              if(formDateStr != null){
                                var formDate = DateTime.parse(formDateStr);
                                formDate = new DateTime(formDate.year,formDate.month,formDate.day);
                                if(formDate != null && date != null) {
                                  Duration difference = date.difference(formDate);
                                  var days = difference.inDays;
                                  durationElements[displayFor]!.text = days.toString();
                                  for (var count = 0; count < formGeneral['fields'].length; count++) {
                                    Map item = formGeneral['fields'][count];
                                    if(displayFor == item['key']){
                                      formGeneral['fields'][count]['value'] = days.toString();
                                    }
                                  }
                                }
                              }
                            }
                            _handleChanged();

                          });
                        },
                        currentTime: DateTime.now(),
                        locale: LocaleType.vi);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18.0,
                                  ),
                                  SizedBox(width: 10,),
                                  Text(
                                    '$_dateLabel',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    ),
                  ),
                )
            ),
          );
        }
      }

      if(item['type'] == "Time"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              new DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, value) async {
                  final time = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(value ?? DateTime.now()),
                  );
                  this.setState(
                        () {
                      formGeneral['fields'][count]['value'] =
                          new DateFormat(formatStr).format(DateTimeField.convert(time)!).toString();
                      _handleChanged();
                    },
                  );
                  return DateTimeField.convert(time);
                },
              ),
            ),
          );
        }
      }

      if(item['type'] == "DateTime"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, currentValue) async {
                  final date = await showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                  if (date != null) {
                    final time = await showTimePicker(
                      context: context,
                      initialTime:
                      TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                    );
                    this.setState(
                          () {
                        formGeneral['fields'][count]['value'] =
                            DateTimeField.combine(date, time).toString();
                        _handleChanged();
                      },
                    );
                    return DateTimeField.combine(date, time);
                  } else {
                    this.setState(
                          () {
                        formGeneral['fields'][count]['value'] =  currentValue.toString();
                        _handleChanged();
                      },
                    );
                    return currentValue;
                  }

                },

              ),
            ),
          );
        }
      }

      //image
      if (item['type'] == "Image") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = Padding(padding: EdgeInsets.only(top: 10,left: 15, right: 15.0),
                child: new Text(item['label'],
                    style: new TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16.0))
            );
          }

          listWidget.add(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  label,
                  Container(
                      margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                      child: Image.network(item['url'], fit: BoxFit.cover)
                  ),
                ],
              )
          );
        }
      }
      //end image

    }

    if (widget.buttonSave != null) {
      listWidget.add(new Container(
        margin: EdgeInsets.only(top: 10.0,left: 15.0,right:15.0),
        child: InkWell(
          onTap: () {
            if (_formKey.currentState!.validate()) {
              widget.actionSave!(formGeneral);
            }
          },
          child: widget.buttonSave,
        ),
      ));
    }
    return listWidget;
  }

  List<Widget> jsonToFormOld() {
    String result = "";

    List<Widget> listWidget = <Widget>[];
    if (formGeneral['result'] != null) {
      result = formGeneral['result'];
    }

    if (formGeneral['title'] != null) {
      listWidget.add(
          Padding(padding: EdgeInsets.only(left: 15.0,right:15.0 ),
              child: Text( formGeneral['title'], style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),)
          ));
    }
    if (formGeneral['description'] != null) {
      listWidget.add(
          Padding(padding: EdgeInsets.only(left: 15.0,right:15.0,top: 5.0,bottom: 10.0),
              child:
              Text(
                formGeneral['description'],
                style: new TextStyle(fontSize: 14.0,),
              )));
    }

    if (widget.selectUsers != null) {
      listWidget.add(widget.selectUsers!);
    }

    if (widget.chooseDate != null) {
      listWidget.add(widget.chooseDate!);
    }

    for (var count = 0; count < formGeneral['fields'].length; count++) {
      Map item = formGeneral['fields'][count];
      var key =  item['key'];

      if (item['type'] == "Label") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          listWidget.add(
            new Container(
                margin: new EdgeInsets.only(top: 25.0, left: 15.0, right: 15.0),
                child: new Text(
                  item['label'],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold ,fontSize: 18.0),
                )
            ),
          );
        }
      }

      if (item['type'] == "RadioButton") {
        radioValue = item['value'];
        selectValues[key] = radioValue.toString();
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          List<Widget> radios = [];

          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          radioValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (radioValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          for (var i = 0; i < item['items'].length; i++) {
            radios.add(
                Row(
                    children: <Widget>[
                      new Radio<String>(
                          value: formGeneral['fields'][count]['items'][i]['value'].toString(),
                          groupValue: radioValue,
                          onChanged: (String? value) {
                            this.setState(() {
                              radioValue = value ?? "";

                              //Gai tri moi
                              selectValues[key] = value ?? "";

                              formGeneral['fields'][count]['value'] = value;

                              if(item['scores']!=null) {
                                scores += double.parse(item['scores'][i]['score']);
                                formGeneral['fields'][count]['score'] = item['scores'][i]['score'];
                              }
                              _handleChanged();


                              //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                              if(formGeneral['result']!=null && formGeneral['result'] == 'conclude'){
                                //Xac dinh key cua El hien thi ket qua cua Form
                                var resultTo = formGeneral['result_to'];

                                //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                                if(item["ignore"] == null || item["ignore"]!= "1") {
                                  selectElements[key] = value ?? "";
                                }

                                var title = "0";
                                if(selectElements.containsValue("5")){
                                  title = "5";
                                }else if(selectElements.containsValue("4")){
                                  title = "4";
                                }else if(selectElements.containsValue("3")){
                                  title = "3";
                                }else if(selectElements.containsValue("2")){
                                  title = "2";
                                }else{
                                  title = "1";
                                }

                                var resultItems = selectValueElements[resultTo];
                                var resultText = json.decode(resultItems ?? "")[int.parse(title)-1];
                                selectResultElements[resultTo]!.text = resultText['label'];

                                for (var count = 0; count < formGeneral['fields'].length; count++) {
                                  Map item = formGeneral['fields'][count];
                                  if(resultTo == item['key']){
                                    formGeneral['fields'][count]['value'] = resultText['label'];
                                  }
                                }
                              }
                            });
                          }),
                      formGeneral['fields'][count]['items'][i]['size']=="lg"?
                      Expanded(
                          child:
                          new Text(formGeneral['fields'][count]['items'][i]['label'],softWrap:true)
                      ):
                      Wrap(
                        children: <Widget>[
                          new Text(formGeneral['fields'][count]['items'][i]['label'],softWrap:true)
                        ],
                      ),
                    ])
            );
          }

          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, left: 0, right: 15.0),
              child:
              layout == 'column' ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: radios,
              ) :
              Row(
                children: radios,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Switch") {
        var val = formGeneral['fields'][count]['value'] != null &&
            formGeneral['fields'][count]['value'].toLowerCase() == 'true';
        selectValues[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (item['value'] == null) {
            formGeneral['fields'][count]['value'] = false;
          }
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (switchValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 0, right: 15.0),
              child: new Row(children: <Widget>[
                new Checkbox(
                  value: val,
                  onChanged: (bool? value) {
                    this.setState(() {
                      switchValue = (value == true);
                      formGeneral['fields'][count]['value'] = value.toString();
                      selectValues[key] = value.toString();
                      _handleChanged();
                    });
                  },
                ),
                new Text(item['label']),
              ]),
            ),
          );
        }
      }

      if (item['type'] == "Checkbox") {
        var val = formGeneral['fields'][count]['value'] != null &&
            formGeneral['fields'][count]['value'].toLowerCase() == 'true';
        selectValues[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          var checkValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (checkValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  /*
                    hiddenElements[snapshot] = true; //old code
                  */
                  /* fix code */
                  if(_tmpChangeCheckLabel.contains(snapshot) == false)
                    hiddenElements[snapshot] = true;
                  /* fix code */

                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          for (var i = 0; i < item['items'].length; i++) {
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: bool.fromEnvironment(formGeneral['fields'][count]['items'][i]['value'].toString()),
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = (value == true);
                        selectValues[key] = value.toString();
                        formGeneral['fields'][count]['items'][i]['value'] = value;

                        /* fix code */
                        var _item_i_hidden_for = formGeneral['fields'][count]['items'][i]['hidden_for'].split(',');
                        int n = _item_i_hidden_for.length;
                        if(checkValue == true) {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.add(_item_i_hidden_for[i].toString().trim());
                          }
                        } else  {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.remove(_item_i_hidden_for[i].toString().trim());
                          }
                        }
                        /* fix code */

                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(formGeneral['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }


      if (item['type'] == "CheckboxList") {
        selectValues[key] = item['value'];

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          //Trộn dữ liệu -- Giap
          var val = formGeneral['fields'][count]['value'];
          if(val != null && val.isNotEmpty){
            var arr = val.split(",");
            for (var i = 0; i < item['items'].length; i++) {
              var label = item['items'][i]['label'];
              var ck = arr.contains(label);
              item['items'][i]['value'] = ck.toString();
            }
          }
          //

          for (var i = 0; i < item['items'].length; i++) {
            var check = formGeneral['fields'][count]['items'][i]['value'] != null &&
                formGeneral['fields'][count]['items'][i]['value'].toLowerCase() == 'true';
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: check,
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = (value == true);
                        formGeneral['fields'][count]['items'][i]['value'] = value.toString();
                        var checkListValues ="";
                        for (var i = 0; i < item['items'].length; i++) {
                          if(formGeneral['fields'][count]['items'][i]['value'] != null &&
                              formGeneral['fields'][count]['items'][i]['value'].toLowerCase() == 'true'){
                            checkListValues += formGeneral['fields'][count]['items'][i]['label']+",";
                          }
                        }
                        formGeneral['fields'][count]['value'] = checkListValues;
                        selectValues[key] = checkListValues;
                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(formGeneral['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Select") {
        selectValue = (selectValue == null && selectValue == "")?
        formGeneral['fields'][count]['value']:"";
        selectValues[key] = selectValue.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Text(item['label'],
                style:
                new TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0));
          }

          var hiddenValue = (item['hidden'] != null && item['hidden'].contains(",") == true) ? item['hidden'].split(',') : item['hidden'];
          //var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');
            this.setState(() {
              if (hiddenValue.contains(selectValue)) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          selectElements[key] = formGeneral['fields'][count]['value'];

          try{

            var expanded = item['expanded'];

            listWidget.add(new Container(
              margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  label,
                  new DropdownButtonFormField<String>(
                    validator: (value) {
                      if (item.containsKey('required')) {
                        if (item['required'] == true ||
                            item['required'] == 'True' ||
                            item['required'] == 'true') {
                          return isRequired(item, value);
                        }
                      }
                      return null;
                    },
                    isExpanded: (expanded == true),
                    hint: new Text("Select a user"),
                    value: formGeneral['fields'][count]['value'],
                    onChanged: (String? newValue) {
                      setState(() {
                        selectValue = newValue ?? "";
                        selectValues[key] = newValue.toString();
                        formGeneral['fields'][count]['value'] = newValue;

                        //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                        if(formGeneral['result']!=null && formGeneral['result'] == 'conclude'){
                          //Xac dinh key cua El hien thi ket qua cua Form
                          var resultTo = formGeneral['result_to'];
                          //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                          if(item["ignore"] == null || item["ignore"]!= "1"){
                            selectElements[key] = newValue!;
                          }

                          var title = "0";
                          if(selectElements.containsValue("5")){
                            title = "5";
                          }else if(selectElements.containsValue("4")){
                            title = "4";
                          }else if(selectElements.containsValue("3")){
                            title = "3";
                          }else if(selectElements.containsValue("2")){
                            title = "2";
                          }else{
                            title = "1";
                          }

                          var resultItems = selectValueElements[resultTo];
                          var resultText = json.decode(resultItems ?? "")[int.parse(title)-1];
                          selectResultElements[resultTo]!.text = resultText['label'];

                          for (var count = 0; count < formGeneral['fields'].length; count++) {
                            Map item = formGeneral['fields'][count];
                            if(resultTo == item['key']){
                              formGeneral['fields'][count]['value'] = resultText['label'];
                            }
                          }
                        }
                        _handleChanged();
                      });
                    },
                    items:
                    item['items'].map<DropdownMenuItem<String>>((dynamic data) {
                      return DropdownMenuItem<String>(
                        value: data['value'],
                        child: new Text(
                          data['label'],
                          style: new TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ));

          }catch(ex){
            var a = selectValue;
          }
        }
      }

      if (item['type'] == "Input" ||
          item['type'] == "Password" ||
          item['type'] == "Email" ||
          item['type'] == "TextArea" ||
          item['type'] == "TextInput") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Container(
              child: new Text(
                item['label'],
                style: new TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 16.0),
              ),
            );
          }
          var lines = 1;
          if(item['lines'] != null && item['lines'] != "" && item['lines'] is int ){
            lines =  item['lines'];
          }

          //check enabled for input type
          bool enabled = true;
          if(item['enabled'] != null && item['enabled'] != "" && item['enabled'] == false){
            enabled =  false;
          }
          bool read_only = false;
          if(item['read_only'] != null && item['read_only'] != "" && item['read_only'] == true){
            read_only =  true;
          }

          var defaultValue = formGeneral['fields'][count]['value'] ?? "";

          var controller = new TextEditingController(text: defaultValue);
          if(durationElements.containsKey(key)) {
            controller = durationElements[key]!;
          }else{
            durationElements[key] = controller;
          }

          //Kiem tra form co phai la danh gia kieu result =  conclude
          //Lay key cua elemnt se hien thi ket qua 'result_to' cua Form
          //Kiem tra 'result_to' cua Element, xac dinh elment nay co phai la el hien thi ket qua khong?
          //Neu phai thi gan controller cua El nay vao map selectResultElements
          //Gan gia tri ket qua trong attr "result_items" cua El vao map selectValueElements
          if(formGeneral['result']!=null && formGeneral['result'] == 'conclude') {
            var resultTo = formGeneral['result_to'];
            if (item['result_to'] != null) {
              selectResultElements[resultTo] = controller;
              selectValueElements[resultTo] = json.encode(item["result_items"]);
            }
          }

          listWidget.add(new Container(
            margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                label,
                new TextFormField(
                  readOnly: read_only,
                  enabled: enabled,
                  controller: controller,
                  autofocus: false,
                  decoration: item['decoration'] ??
                      widget.decorations[item['key']] ??
                      new InputDecoration(
                        hintText: item['placeholder'] ?? "",
                        helperText: item['helpText'] ?? "",
                      ),
                  maxLines: item['type'] == "TextArea" ? lines : 1,
                  onChanged: (String value) {
                    formGeneral['fields'][count]['value'] = value;
                    _handleChanged();
                  },
                  obscureText: item['type'] == "Password" ? true : false,
                  validator: (value) {
                    if (widget.validations.containsKey(item['key'])) {
                      return widget.validations[item['key']](item, value);
                    }
                    if (item.containsKey('validator')) {
                      if (item['validator'] != null) {
                        if (item['validator'] is Function) {
                          return item['validator'](item, value);
                        }
                      }
                    }
                    if (item['type'] == "Email") {
                      return validateEmail(item, value ?? "");
                    }

                    if (item.containsKey('required')) {
                      if (item['required'] == true ||
                          item['required'] == 'True' ||
                          item['required'] == 'true') {
                        return isRequired(item, value);
                      }
                    }

                    return null;
                  },
                ),
              ],
            ),
          ));
        }
      }

      if(item['type'] == "Date"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          if(item['value'] == null || item['value'].toString().isEmpty){
            formGeneral['fields'][count]['value'] =  DateTime.now().toString();
          }else{
            _dateLabel = new DateFormat(formatStr)
                .format(DateTime.parse(item['value']))
                .toString();
          }

          listWidget.add(
            Padding(
                padding: EdgeInsets.only(left: 15,right: 15),
                child:
                ElevatedButton(
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime(2000, 1, 1),
                        //maxTime: DateTime.now(),
                        onConfirm: (date) {
                          print('confirm $date');
                          _formDate = date;
                          setState(() {
                            _dateLabel = new DateFormat(formatStr)
                                .format(_formDate)
                                .toString();

                            formGeneral['fields'][count]['value'] =  _formDate.toString();
                            var displayFor = item['display_for'];
                            if(displayFor != null) {
                              var formDateStr = formGeneral["form_date"];
                              if(formDateStr != null){
                                var formDate = DateTime.parse(formDateStr);
                                formDate = new DateTime(formDate.year,formDate.month,formDate.day);
                                if(formDate != null && date != null) {
                                  Duration difference = date.difference(formDate);
                                  var days = difference.inDays;
                                  durationElements[displayFor]!.text = days.toString();
                                  for (var count = 0; count < formGeneral['fields'].length; count++) {
                                    Map item = formGeneral['fields'][count];
                                    if(displayFor == item['key']){
                                      formGeneral['fields'][count]['value'] = days.toString();
                                    }
                                  }
                                }
                              }
                            }
                            _handleChanged();

                          });
                        },
                        currentTime: DateTime.now(),
                        locale: LocaleType.vi);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18.0,
                                  ),
                                  SizedBox(width: 10,),
                                  Text(
                                    '$_dateLabel',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    ),
                  ),
                )
            ),
          );
        }
      }

      if(item['type'] == "Time"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              new DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, value) async {
                  final time = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(value ?? DateTime.now()),
                  );
                  this.setState(
                        () {
                      formGeneral['fields'][count]['value'] =
                          new DateFormat(formatStr).format(DateTimeField.convert(time)!).toString();
                      _handleChanged();
                    },
                  );
                  return DateTimeField.convert(time);
                },
              ),
            ),
          );
        }
      }

      if(item['type'] == "DateTime"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, currentValue) async {
                  final date = await showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                  if (date != null) {
                    final time = await showTimePicker(
                      context: context,
                      initialTime:
                      TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                    );
                    this.setState(
                          () {
                        formGeneral['fields'][count]['value'] =
                            DateTimeField.combine(date, time).toString();
                        _handleChanged();
                      },
                    );
                    return DateTimeField.combine(date, time);
                  } else {
                    this.setState(
                          () {
                        formGeneral['fields'][count]['value'] =  currentValue.toString();
                        _handleChanged();
                      },
                    );
                    return currentValue;
                  }

                },

              ),
            ),
          );
        }
      }

      //image
      if (item['type'] == "Image") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = Padding(padding: EdgeInsets.only(top: 10,left: 15, right: 15.0),
                child: new Text(item['label'],
                    style: new TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16.0))
            );
          }

          listWidget.add(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  label,
                  Container(
                      margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                      child: Image.network(item['url'], fit: BoxFit.cover)
                  ),
                ],
              )
          );
        }
      }
      //end image

    }

    if (widget.buttonSave != null) {
      listWidget.add(new Container(
        margin: EdgeInsets.only(top: 10.0,left: 15.0,right:15.0),
        child: InkWell(
          onTap: () {
            if (_formKey.currentState!.validate()) {
              widget.actionSave!(formGeneral);
            }
          },
          child: widget.buttonSave,
        ),
      ));
    }
    return listWidget;
  }

  List<Widget> jsonToChildForm(Map<dynamic, dynamic> items) {
    String result = "";

    List<Widget> listWidget = <Widget>[];

    for (var count = 0; count < items['fields'].length; count++) {
      Map item = items['fields'][count];
      var key =  item['key'];

      if (item['type'] == "Label") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          listWidget.add(
            new Container(
                margin: new EdgeInsets.only(top: 25.0, left: 15.0, right: 15.0),
                child: new Text(
                  item['label'],
                  style: new TextStyle(
                      fontWeight: FontWeight.bold ,fontSize: 18),
                )
            ),
          );
        }
      }

      if (item['type'] == "RadioButton") {
        radioValue = item['value'];
        selectValues[key] = radioValue.toString();
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false) {
          List<Widget> radios = [];

          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          radioValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (radioValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          for (var i = 0; i < item['items'].length; i++) {
            radios.add(
                Row(
                    children: <Widget>[
                      new Radio<String>(
                          value: items['fields'][count]['items'][i]['value'].toString(),
                          groupValue: radioValue,
                          onChanged: (String? value) {
                            this.setState(() {
                              radioValue = value ?? "";

                              //Gai tri moi
                              selectValues[key] = value ?? "";

                              items['fields'][count]['value'] = value;

                              if(item['scores']!=null) {
                                scores += double.parse(item['scores'][i]['score']);
                                items['fields'][count]['score'] = item['scores'][i]['score'];
                              }
                              _handleChanged();


                              //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                              if(formGeneral['result']!=null && formGeneral['result'] == 'conclude'){
                                //Xac dinh key cua El hien thi ket qua cua Form
                                var resultTo = formGeneral['result_to'];

                                //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                                if(item["ignore"] == null || item["ignore"]!= "1") {
                                  selectElements[key] = value ?? "";
                                }

                                var title = "0";
                                if(selectElements.containsValue("5")){
                                  title = "5";
                                }else if(selectElements.containsValue("4")){
                                  title = "4";
                                }else if(selectElements.containsValue("3")){
                                  title = "3";
                                }else if(selectElements.containsValue("2")){
                                  title = "2";
                                }else{
                                  title = "1";
                                }

                                var resultItems = selectValueElements[resultTo];
                                var resultText = json.decode(resultItems ?? "")[int.parse(title)-1];
                                selectResultElements[resultTo]!.text = resultText['label'];

                                for (var count = 0; count < formGeneral['fields'].length; count++) {
                                  Map item = formGeneral['fields'][count];
                                  if(resultTo == item['key']){
                                    formGeneral['fields'][count]['value'] = resultText['label'];
                                  }
                                }
                              }
                            });
                          }),
                      items['fields'][count]['items'][i]['size']=="lg"?
                      Expanded(
                          child:
                          new Text(items['fields'][count]['items'][i]['label'],softWrap:true)
                      ):
                      Wrap(
                        children: <Widget>[
                          new Text(items['fields'][count]['items'][i]['label'],softWrap:true)
                        ],
                      ),
                    ])
            );
          }

          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, left: 0, right: 15.0),
              child:
              layout == 'column' ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: radios,
              ) :
              Row(
                children: radios,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Switch") {
        var val = items['fields'][count]['value'] != null &&
            items['fields'][count]['value']== 'true';
        items[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (item['value'] == null) {
            items['fields'][count]['value'] = false;
          }
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (switchValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }

          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 0, right: 15.0),
              child: new Row(children: <Widget>[
                new Checkbox(
                  value: val,
                  onChanged: (bool? value) {
                    this.setState(() {
                      switchValue = (value == true);
                      items['fields'][count]['value'] = value.toString();
                      selectValues[key] = value.toString();
                      _handleChanged();
                    });
                  },
                ),
                new Text(item['label']),
              ]),
            ),
          );
        }
      }

      if (item['type'] == "Checkbox") {
        var val = items['fields'][count]['value'] != null &&
            items['fields'][count]['value'].toLowerCase() == 'true';
        selectValues[key] = val.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          var checkValue = item['value'];
          var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');

            this.setState(() {
              if (checkValue == hiddenValue) {
                hiddenForValue.forEach((snapshot) {
                  /*
                    hiddenElements[snapshot] = true; //old code
                  */
                  /* fix code */
                  if(_tmpChangeCheckLabel.contains(snapshot) == false)
                    hiddenElements[snapshot] = true;
                  /* fix code */

                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          for (var i = 0; i < item['items'].length; i++) {
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: bool.fromEnvironment(items['fields'][count]['items'][i]['value'].toString()),
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = (value == true);
                        selectValues[key] = value.toString();
                        items['fields'][count]['items'][i]['value'] = value;

                        /* fix code */
                        var _item_i_hidden_for = items['fields'][count]['items'][i]['hidden_for'].split(',');
                        int n = _item_i_hidden_for.length;
                        if(checkValue == true) {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.add(_item_i_hidden_for[i].toString().trim());
                          }
                        } else  {
                          for(int i=0; i<n; i++){
                            if(_item_i_hidden_for[i] != "")
                              _tmpChangeCheckLabel.remove(_item_i_hidden_for[i].toString().trim());
                          }
                        }
                        /* fix code */

                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(items['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }


      if (item['type'] == "CheckboxList") {
        selectValues[key] = item['value'];

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          List<Widget> checkboxes = [];
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top:15,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style:
                        new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }
          //Trộn dữ liệu -- Giap
          var val = items['fields'][count]['value'];
          if(val != null && val.isNotEmpty){
            var arr = val.split(",");
            for (var i = 0; i < item['items'].length; i++) {
              var label = item['items'][i]['label'];
              var ck = arr.contains(label);
              item['items'][i]['value'] = ck.toString();
            }
          }
          //

          for (var i = 0; i < item['items'].length; i++) {
            var check = items['fields'][count]['items'][i]['value'] != null &&
                items['fields'][count]['items'][i]['value'].toLowerCase() == 'true';
            checkboxes.add(
              new Row(
                children: <Widget>[
                  new Checkbox(
                    value: check,
                    onChanged: (bool? value) {
                      this.setState(() {
                        checkValue = (value == true);
                        items['fields'][count]['items'][i]['value'] = value.toString();
                        var checkListValues ="";
                        for (var i = 0; i < item['items'].length; i++) {
                          if(items['fields'][count]['items'][i]['value'] != null &&
                              items['fields'][count]['items'][i]['value'].toLowerCase() == 'true'){
                            checkListValues += items['fields'][count]['items'][i]['label']+",";
                          }
                        }
                        items['fields'][count]['value'] = checkListValues;
                        selectValues[key] = checkListValues;
                        _handleChanged();
                      },
                      );
                    },
                  ),
                  new Text(items['fields'][count]['items'][i]['label']),
                ],
              ),
            );
          }
          var layout = item['layout'];
          listWidget.add(
            new Container(
              margin: new EdgeInsets.only(top: 5.0, right: 15.0),
              child:
              layout == "column" ?
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: checkboxes,
              ) :
              Row(
                children: checkboxes,
              ),
            ),
          );
        }
      }

      if (item['type'] == "Select") {
        selectValue = (selectValue == null && selectValue == "")?
        items['fields'][count]['value']:"";
        selectValues[key] = selectValue.toString();

        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Text(item['label'],
                style:
                new TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0));
          }

          var hiddenValue = (item['hidden'] != null && item['hidden'].contains(",") == true) ? item['hidden'].split(',') : item['hidden'];
          //var hiddenValue = item['hidden'];
          var hiddenFor = item['hidden_for'];
          if(hiddenFor != null) {
            var hiddenForValue = item['hidden_for'].split(',');
            this.setState(() {
              if (hiddenValue.contains(selectValue)) {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                hiddenForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          var showFor = item['show_for'];
          if(showFor != null) {
            var showForValue = item['show_for'].split(',');

            this.setState(() {
              if (radioValue != hiddenValue) {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = true;
                });
              } else {
                showForValue.forEach((snapshot) {
                  hiddenElements[snapshot] = false;
                });
              }
            });
          }


          selectElements[key] = items['fields'][count]['value'];

          try{

            var expanded = item['expanded'];

            listWidget.add(new Container(
              margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  label,
                  new DropdownButtonFormField<String>(
                    validator: (value) {
                      if (item.containsKey('required')) {
                        if (item['required'] == true ||
                            item['required'] == 'True' ||
                            item['required'] == 'true') {
                          return isRequired(item, value);
                        }
                      }
                      return null;
                    },
                    isExpanded: (expanded == true),
                    hint: new Text("Select a user"),
                    value: items['fields'][count]['value'],
                    onChanged: (String? newValue) {
                      setState(() {
                        selectValue = newValue ?? "";
                        selectValues[key] = newValue.toString();
                        items['fields'][count]['value'] = newValue;

                        //Kiem tra form kieu danh gia co ket qua la "Kết luận" = 'conclude'
                        if(items['result']!=null && items['result'] == 'conclude'){
                          //Xac dinh key cua El hien thi ket qua cua Form
                          var resultTo = items['result_to'];
                          //Neu tieu chi nao co ignore = 1 thi bo qua ket qua tieu chi nay, khong dua vao danh gia
                          if(item["ignore"] == null || item["ignore"]!= "1"){
                            selectElements[key] = newValue ?? "";
                          }

                          var title = "0";
                          if(selectElements.containsValue("5")){
                            title = "5";
                          }else if(selectElements.containsValue("4")){
                            title = "4";
                          }else if(selectElements.containsValue("3")){
                            title = "3";
                          }else if(selectElements.containsValue("2")){
                            title = "2";
                          }else{
                            title = "1";
                          }

                          var resultItems = selectValueElements[resultTo];
                          var resultText = json.decode(resultItems ?? "")[int.parse(title)-1];
                          selectResultElements[resultTo]!.text = resultText['label'];

                          for (var count = 0; count < items['fields'].length; count++) {
                            Map item = items['fields'][count];
                            if(resultTo == item['key']){
                              items['fields'][count]['value'] = resultText['label'];
                            }
                          }
                        }
                        _handleChanged();
                      });
                    },
                    items:
                    item['items'].map<DropdownMenuItem<String>>((dynamic data) {
                      return DropdownMenuItem<String>(
                        value: data['value'],
                        child: new Text(
                          data['label'],
                          style: new TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ));

          }catch(ex){
            var a = selectValue;
          }
        }
      }

      if (item['type'] == "Input" ||
          item['type'] == "Password" ||
          item['type'] == "Email" ||
          item['type'] == "TextArea" ||
          item['type'] == "TextInput") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = new Container(
              child: new Text(
                item['label'],
                style: new TextStyle(
                    fontWeight: FontWeight.w500, fontSize: 16.0),
              ),
            );
          }
          var lines = 1;
          if(item['lines'] != null && item['lines'] != "" && item['lines'] is int ){
            lines =  item['lines'];
          }

          //check enabled for input type
          bool enabled = true;
          if(item['enabled'] != null && item['enabled'] != "" && item['enabled'] == false){
            enabled =  false;
          }
          bool read_only = false;
          if(item['read_only'] != null && item['read_only'] != "" && item['read_only'] == true){
            read_only =  true;
          }

          var defaultValue = items['fields'][count]['value'] ?? "";

          var controller = new TextEditingController(text: defaultValue);
          if(durationElements.containsKey(key)) {
            controller = durationElements[key]!;
          }else{
            durationElements[key] = controller;
          }

          //Kiem tra form co phai la danh gia kieu result =  conclude
          //Lay key cua elemnt se hien thi ket qua 'result_to' cua Form
          //Kiem tra 'result_to' cua Element, xac dinh elment nay co phai la el hien thi ket qua khong?
          //Neu phai thi gan controller cua El nay vao map selectResultElements
          //Gan gia tri ket qua trong attr "result_items" cua El vao map selectValueElements
          if(items['result']!=null && items['result'] == 'conclude') {
            var resultTo = items['result_to'];
            if (item['result_to'] != null) {
              selectResultElements[resultTo] = controller;
              selectValueElements[resultTo] = json.encode(item["result_items"]);
            }
          }

          listWidget.add(new Container(
            margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                label,
                new TextFormField(
                  readOnly: read_only,
                  enabled: enabled,
                  controller: controller,
                  autofocus: false,
                  decoration: item['decoration'] ??
                      widget.decorations[item['key']] ??
                      new InputDecoration(
                        hintText: item['placeholder'] ?? "",
                        helperText: item['helpText'] ?? "",
                      ),
                  maxLines: item['type'] == "TextArea" ? lines : 1,
                  onChanged: (String value) {
                    items['fields'][count]['value'] = value;
                    setState(() {
                      selectValues[key] = value.toString();
                    });
                      _handleChanged();
                  },
                  obscureText: item['type'] == "Password" ? true : false,
                  validator: (value) {
                    if (widget.validations.containsKey(item['key'])) {
                      return widget.validations[item['key']](item, value);
                    }
                    if (item.containsKey('validator')) {
                      if (item['validator'] != null) {
                        if (item['validator'] is Function) {
                          return item['validator'](item, value);
                        }
                      }
                    }
                    if (item['type'] == "Email") {
                      return validateEmail(item, value ?? "");
                    }

                    if (item.containsKey('required')) {
                      if (item['required'] == true ||
                          item['required'] == 'True' ||
                          item['required'] == 'true') {
                        return isRequired(item, value);
                      }
                    }

                    return null;
                  },
                ),
              ],
            ),
          ));
        }
      }

      if(item['type'] == "Date"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          if(item['value'] == null || item['value'].toString().isEmpty){
            items['fields'][count]['value'] =  DateTime.now().toString();
          }else{
            _dateLabel = new DateFormat(formatStr)
                .format(DateTime.parse(item['value']))
                .toString();
          }

          listWidget.add(
            Padding(
                padding: EdgeInsets.only(left: 15,right: 15),
                child:
                ElevatedButton(
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime(2000, 1, 1),
                        //maxTime: DateTime.now(),
                        onConfirm: (date) {
                          print('confirm $date');
                          _formDate = date;
                          setState(() {
                            _dateLabel = new DateFormat(formatStr)
                                .format(_formDate)
                                .toString();

                            items['fields'][count]['value'] =  _formDate.toString();
                            var displayFor = item['display_for'];
                            if(displayFor != null) {
                              var formDateStr = items["form_date"];
                              if(formDateStr != null){
                                var formDate = DateTime.parse(formDateStr);
                                formDate = new DateTime(formDate.year,formDate.month,formDate.day);
                                if(formDate != null && date != null) {
                                  Duration difference = date.difference(formDate);
                                  var days = difference.inDays;
                                  durationElements[displayFor]!.text = days.toString();
                                  for (var count = 0; count < items['fields'].length; count++) {
                                    Map item = items['fields'][count];
                                    if(displayFor == item['key']){
                                      items['fields'][count]['value'] = days.toString();
                                    }
                                  }
                                }
                              }
                            }
                            _handleChanged();

                          });
                        },
                        currentTime: DateTime.now(),
                        locale: LocaleType.vi);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18.0,
                                  ),
                                  SizedBox(width: 10,),
                                  Text(
                                    '$_dateLabel',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Icon(Icons.arrow_drop_down)
                      ],
                    ),
                  ),
                )
            ),
          );
        }
      }

      if(item['type'] == "Time"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              new DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, value) async {
                  final time = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(value ?? DateTime.now()),
                  );
                  this.setState(
                        () {
                          items['fields'][count]['value'] =
                              new DateFormat(formatStr).format(DateTimeField.convert(time)!).toString();
                      _handleChanged();
                    },
                  );
                  return DateTimeField.convert(time);
                },
              ),
            ),
          );
        }
      }

      if(item['type'] == "DateTime"){
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          if (labelHidden(item)) {
            listWidget.add(
                new Padding(padding: EdgeInsets.only(top: 15.0,left: 15, right: 15.0),
                    child: new Text(item['label'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 16.0)))
            );
          }

          var formatStr =  "dd-MM-yyyy HH:mm";
          if(item['format'] != null && item['format'] != ""){
            formatStr = item['format'];
          }

          listWidget.add(
            Padding(
              padding: EdgeInsets.only(left: 15,right: 15),
              child:
              DateTimeField(
                initialValue: DateTime.now(),
                format: DateFormat(formatStr),
                onShowPicker: (context, currentValue) async {
                  final date = await showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                  if (date != null) {
                    final time = await showTimePicker(
                      context: context,
                      initialTime:
                      TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                    );
                    this.setState(
                          () {
                            items['fields'][count]['value'] =
                            DateTimeField.combine(date, time).toString();
                        _handleChanged();
                      },
                    );
                    return DateTimeField.combine(date, time);
                  } else {
                    this.setState(
                          () {
                            items['fields'][count]['value'] =  currentValue.toString();
                        _handleChanged();
                      },
                    );
                    return currentValue;
                  }

                },

              ),
            ),
          );
        }
      }

      //image
      if (item['type'] == "Image") {
        if((hiddenElements[key] == null || hiddenElements[key]==false ) && checkIsHidden(item)==false){
          Widget label = SizedBox.shrink();
          if (labelHidden(item)) {
            label = Padding(padding: EdgeInsets.only(top: 10,left: 15, right: 15.0),
                child: new Text(item['label'],
                    style: new TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 16.0))
            );
          }

          listWidget.add(
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  label,
                  Container(
                      margin: new EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                      child: Image.network(item['url'], fit: BoxFit.cover)
                  ),
                ],
              )
          );
        }
      }
      //end image

    }

    return listWidget;
  }

  _CoreFormState(this.formGeneral);

  void _handleChanged() {
    widget.onChanged(formGeneral);
  }
  void _validateInputs() {
    final form = _formKey.currentState;
    if (form!.validate()) {

      var fields = formGeneral["fields"];
      fields.forEach((field) {
        var val = field["value"];
        var req = field["required"];
        print(val);
        print(req);

        if(req == true && (val == null || val.toString().isEmpty)){
          var req = field["required"];
          formGeneral['validate'] = false;
          return;
        }
      });
      widget.actionSave!(formGeneral);
    }
  }


  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
      autovalidateMode: formGeneral['autoValidated'] ? AutovalidateMode.always : AutovalidateMode.disabled,
      key: _formKey,
      child: new Container(
        padding: new EdgeInsets.all(widget.padding ?? 8.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: jsonToForm(),
        ),
      ),
    );
  }



}
