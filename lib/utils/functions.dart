import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class Functions {
  static String timeRangeAllowCreateSurvey() {
    var formatStr = "dd/MM/yyyy";
    var now = DateTime.now();
    var startDate = DateFormat(formatStr)
        .format(DateTime(now.year, now.month, 26))
        .toString();
    var endDate =  DateFormat(formatStr)
        .format(DateTime(now.year, now.month + 1 , 5))
        .toString();
    return "Bạn chỉ được tạo đánh giá trong khoảng thời gian từ $startDate đến hết $endDate";
  }

  static bool timeRangeAllowEditSurvey(DateTime formDate) {
    var formatStr = "dd/MM/yyyy";
    var now = DateTime.now();
    var month = now.month;
    if(now.day <= 5){
      month--;
    }

    var startDate = DateTime(now.year, month, 26);
    var endDate =  DateTime(now.year, month + 1 , 5);

    if(formDate.difference(startDate).inDays>=0 && formDate.difference(endDate).inDays<=0){
      return true;
    }
    return false;
  }


  static String msgRangeAllowEditSurvey() {
    var formatStr = "dd/MM/yyyy";
    var now = DateTime.now();
    var month = now.month;
    if(now.day < 5){
      month--;
    }
    var startDate = DateFormat(formatStr).format(DateTime(now.year, month, 26));
    var endDate =  DateFormat(formatStr).format(DateTime(now.year, month + 1 , 5));

    return "Đánh giá này đã lưu trữ và không thể chỉnh sửa.";
  }



  static Widget? showSnackBarMsg(BuildContext context, String msg, {color: Colors.redAccent, backPrev: false}) {
    final snackBar = SnackBar(
      content: Text(msg),
      backgroundColor: color,
      duration: Duration(seconds: 15),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  /// Converts fully qualified YouTube Url to video id.
  ///
  /// If videoId is passed as url then no conversion is done.
  static String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    assert(url?.isNotEmpty ?? false, 'Url cannot be empty');
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var exp in [
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(
          r"^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$"),
      RegExp(r"^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$")
    ]) {
      Match match = exp.firstMatch(url)!;
      if (match != null && match.groupCount >= 1) return match.group(1)!;
    }

    return null;
  }

  /*static validateMobile(String value) {
    String patttern = r'((09|03|07|08|05)+([0-9]{8})\b)';
    RegExp regExp = new RegExp(patttern);
    if (value.isEmpty || value.trim().isEmpty) {
      return 'Số điện thoại không được để trống.';
    } else if (!regExp.hasMatch(value.trim())) {
      return 'Định dạng số điện thoại $value chưa đúng';
    }
    return null;
  }

  static String chuanHoaSoDienThoai(value) {
    String origin = value.toString().trim();
    //neu so dien thoai co dau .
    origin = origin.replaceAll(".", "");
    //truong hop 3 ky tu dau la: +84
    String fCharacters = value.substring(0, 3).trim();
    if(fCharacters == "+84") {
      origin = origin.replaceAll("+84", "0");
    }
    //truong hop 5 ky tu dau la: (+84)
    String sCharacters = value.substring(0, 5).trim();
    if(sCharacters == "(+84)") {
      origin = origin.replaceAll("(+84)", "0");
    }
    //kiem tra tinh hop le cua so dien thoai
    String originRemoveFrist0 = "";
    if(validateMobile(origin) == null) {
      originRemoveFrist0 = origin.substring(1);
    } else {
      return "${validateMobile(origin)}";
    }
    return "+84$originRemoveFrist0";
  }*/

  //Cach su dung ham: Functions.getStandardPhone(phone: "+987988838", maQuocGia: "+84");
  static String getStandardPhone({required String phone, required String maQuocGia}) {
    String origin = phone.toString().trim();
    /*
    1. bắt đầu bằng số 0 -> Xóa số 0 đi -> Cộng mã quốc gia
    2. bắt đầu bằng số khác 0 và khác dấu + -> Cộng mã quốc gia
    3. bắt đầu bằng dấu + -> Giữ nguyên
    */
    //kiem tra ky tu dau tien
    String fCharacter = origin.substring(0, 1).trim();
    if(fCharacter == "0") {
      origin = origin.substring(1);
      origin = "$maQuocGia$origin";
    } else if(fCharacter != "0") {
      if(fCharacter != "+") {
        origin = "$maQuocGia$origin";
      }
    }
    print("Phone number: $origin");
    return origin;
  }

  /*
  fieldName : Tên field cần lọc
   */
  static String getFilteDateParam({required String fieldName,int? year,int? month}){
    if(year == null || year == 0){
      return "";
    }else{
      if(month == null || month == 0){
        var fr = Jiffy(DateTime(year,1,1));
        var to = fr.clone().add(years: 1);
        return "&${fieldName}_gte=${fr.format("yyyy-MM-dd")}&${fieldName}_lte=${to.format("yyyy-MM-dd")}";
      }else{
        var fr = Jiffy(DateTime(year,month,1));
        var to = fr.clone().add(months: 1);
        return "&${fieldName}_gte=${fr.format("yyyy-MM-dd")}&${fieldName}_lte=${to.format("yyyy-MM-dd")}";
      }
    }
  }
}