import 'package:get_it/get_it.dart';
import 'package:hmhapp/utils/calls_and_messages_service.dart';

final locator = GetIt.instance;
void setupLocator() {
  locator.registerSingleton(CallsAndMessagesService());
}