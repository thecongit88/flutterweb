import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

wdgCopyText(Widget element, String value) {
  return value.trim() != "-" && value.trim() != "" ?
  Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      element,
      SizedBox(width: 8),
      wdgCopyElm(value)
    ],
  ) :
  Text("-");
}

wdgCopyElm(value) {
  return InkWell(
    onTap: () async {
      FirebaseMessaging f = FirebaseMessaging.instance;
      String? token = await f.getToken();

      Clipboard.setData(ClipboardData(text: value != "" ? value : token));
      Fluttertoast.showToast(
          msg: "Đã sao chép",
          gravity: ToastGravity.CENTER
      );
    },
    child: Icon(Icons.copy_rounded, size: 15, color: Colors.red,),
  );
}

bool isNullOrEmpty(value) {
  return value == null || value.toString().trim() == "null" || value.toString().trim() == "";
}

String chuanHoaTinhHuyenXa({String? tinh, String? huyen, String? xa}) {
  String param = "";
  List<String> params = [];
  if(!isNullOrEmpty(xa)) params.add("$xa");
  if(!isNullOrEmpty(huyen)) params.add("$huyen");
  if(!isNullOrEmpty(tinh)) params.add("$tinh");
  if(params.length > 0) param = params.join(" - ");
  if(param.trim() != "") param = "${param}";
  return param;
}

getHourMinite(value) {
  if(value == "") return "";
  var timeArr = value.trim().split(":");
  String timeValue = "";
  if(timeArr != null && timeArr.length >= 3) {
    timeValue = "${timeArr[0]}:${timeArr[1]}";
  }
  return timeValue;
}

List<DropdownMenuItem<int>> getListThang() {
  List<int?> ddl = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  return ddl
      .map((value) => DropdownMenuItem(
    value: value,
    child: value == 0 ? const Text("Chọn tháng") : Text("Tháng $value"),
  ))
      .toList();
}

List<DropdownMenuItem<int>> getListYear() {
  List<int?> ddl = [0];
  for(var i = 2020; i <= DateTime.now().year + 1; i++){
    ddl.add(i);
  }
  return ddl
      .map((value) => DropdownMenuItem(
    value: value,
    child: value == 0 ? const Text("Chọn năm") :Text("$value"),
  ))
      .toList();
}