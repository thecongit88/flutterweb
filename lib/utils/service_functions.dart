
  String _addParamToQueryString(String queryString, String key, String value) {
    if (queryString == null) {
      queryString = '';
    }

    if (queryString.length == 0) {
      queryString += '?';
    } else {
      queryString += '&';
    }

    queryString += '$key=$value';

    return queryString;
  }

  int pages(int total, int pageSize){
    var page = total~/pageSize;
    var ext = total%pageSize;
    if(ext==0){
      return page;
    }else{
      return page+1;
    }
  }