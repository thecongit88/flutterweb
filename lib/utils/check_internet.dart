import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../app_theme.dart';

class HasNetwork {
  hasInternet() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      return false;
    }
  }

  static void showToastMesage() {
    Fluttertoast.showToast(
      msg: "Không có kết nối. Vui lòng thử lại",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      textColor: Colors.white,
    );
  }

  static void showToastMesageLogin() {
    Fluttertoast.showToast(
      msg: "Không có kết nối. Vui lòng thử lại",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      textColor: Colors.white,
      backgroundColor: Colors.red
    );
  }
}