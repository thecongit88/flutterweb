import 'package:flutter/material.dart';


class Constants {
  static const CONSUMER_KEY_CLOUD =
      'ck_6baf55024da04d741431e797fca035c9d283512b';
  static const CONSUMER_SECRET_CLOUD =
      'cs_e713154ae4c7d1fa394e75e98a1c39226db2bf96';

  static const APP_MODULE = 'HBCC';
  static const HBCC = 'HBCC';
  static const NKT = 'NKT';
  static const ADMIN = 'ADMIN';


  static const Success = "success";
  static const Error = "error";
  static const Info = "info";
  static const Warning = "warning";


  static const LOADING_NORMAL = "NORMAL";
  static const LOADING_LARGE = "LARGE";
  static const LOADING_SMALL = "SMALL";
  static const LOADING_NOMESSAGE = "NOMESSAGE";

  // keys for localstorage
  static final kLocalKey = {
    "userInfo": "userInfo",
    "shippingAddress": "shippingAddress",
    "recentSearches": "recentSearches",
    "wishlist": "wishlist",
    "home": "home",
    'isDarkTheme': 'isDarkTheme',
    'productsInCart': 'productsInCart',
    'cartproducts': 'products',
    'categories':'categories',
    'productVariationsInCart': 'productVariationsInCart'
  };

  static double baseHeight = 640;

  static double screenAwareSize(double size, BuildContext context) {
    return size * MediaQuery.of(context).size.height / baseHeight;
  }

  //Report of Manager App (HBCC)
  static const MANAGER_REPORT_PLAN_PERCENT_PIE = 'PlanPercentPie';
  static const MANAGER_REPORT_PLAN_PERCENT_COLUMN = 'PlanPercentColumn';
  static const MANAGER_REPORT_COMPARE_STATE_LINE = 'CompareStateLine';

  //
  static const VIDEO_CATEGORY_IMAGE_DEFAULT = 'assets/design_course/interFace1.png';

  static const PAGE_SIZE = 2;
  static const PAGE_SIZE_ALL = 50;
  static const MAX_LASTEST_VIDEOS = 6;

  static const GENDER_MALE = 1;
  static const GENDER_FEMALE = 2;
  static const DACAM = 8;

  static const String resJsonYesNo = '[{ "id": 0, "code": "false", "name": "Không"}, { "id": 1, "code": "true", "name": "Có"}]';
  static const String resGenders = '[{ "id": 1, "Name": "Nam"},{ "id": 2, "Name": "Nữ"},{ "id": 3, "Name": "Khác"}]';
  static const String resNations = '[{"id":1,"nation_name":"Kinh"}, {"id":2,"nation_name":"Tày"}, {"id":3,"nation_name":"Thái"}, {"id":4,"nation_name":"Mường"}, {"id":5,"nation_name":"Khmer"}, {"id":6,"nation_name":"H mông"}, {"id":7,"nation_name":"Nùng"}, {"id":8,"nation_name":"Hoa"}, {"id":9,"nation_name":"Dao"}, {"id":10,"nation_name":"Gia Rai"}, {"id":11,"nation_name":"Ê Đê"}, {"id":12,"nation_name":"Ba Na"}, {"id":13,"nation_name":"Xơ Đăng"}, {"id":14,"nation_name":"Sán Chay"}, {"id":15,"nation_name":"Cơ Ho"}, {"id":16,"nation_name":"Chăm"}, {"id":17,"nation_name":"Sán Dìu"}, {"id":18,"nation_name":"Hrê"}, {"id":19,"nation_name":"Ra Glai"}, {"id":20,"nation_name":"M Nông"}, {"id":21,"nation_name":"X’Tiêng"}, {"id":22,"nation_name":"Bru-Vân Kiều"}, {"id":23,"nation_name":"Thổ"}, {"id":24,"nation_name":"Khơ Mú"}, {"id":25,"nation_name":"Cơ Tu"}, {"id":26,"nation_name":"Giáy"}, {"id":27,"nation_name":"Giẻ Triêng"}, {"id":28,"nation_name":"Tà Ôi"}, {"id":29,"nation_name":"Mạ"}, {"id":30,"nation_name":"Co"}, {"id":31,"nation_name":"Chơ Ro"}, {"id":32,"nation_name":"Xinh Mun"}, {"id":33,"nation_name":"Hà Nhì"}, {"id":34,"nation_name":"Chu Ru"}, {"id":35,"nation_name":"Lào"}, {"id":36,"nation_name":"Kháng"}, {"id":37,"nation_name":"La Chí"}, {"id":38,"nation_name":"Phù Lá"}, {"id":39,"nation_name":"La Hủ"}, {"id":40,"nation_name":"La Ha"}, {"id":41,"nation_name":"Pà Thẻn"}, {"id":42,"nation_name":"Chứt"}, {"id":43,"nation_name":"Lự"}, {"id":44,"nation_name":"Lô Lô"}, {"id":45,"nation_name":"Mảng"}, {"id":46,"nation_name":"Cờ Lao"}, {"id":47,"nation_name":"Bố Y"}, {"id":48,"nation_name":"Cống"}, {"id":49,"nation_name":"Ngái"}, {"id":50,"nation_name":"Si La"}, {"id":51,"nation_name":"Pu Péo"}, {"id":52,"nation_name":"Rơ măm"}, {"id":53,"nation_name":"Brâu"}, {"id":54,"nation_name":"Ơ Đu"}]';
// static final String resJsonYesNo = '[{ "id": 0, "code": "false", "name": "Không"}, { "id": 1, "code": "true", "name": "Có"}]';
}

