import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';

class ImageUtil {
  static getFullImagePath(Map<String, dynamic> json){
    String imgPath = "";
    if(json != null){
      //String provider = json["provider"];
      String url = json["url"];
      //if(provider !=null && provider.isNotEmpty && url.isNotEmpty){
        //if(provider == "local"){
          imgPath = "${EnpointUrl.URL_CLOUD}$url";
        //}else{
        //  imgPath = url;
        //}
      //}
    }
    return imgPath;
  }


  static getFullImagePathFromAbsolute(String absolutPath){
    String imgPath = "";
    if(absolutPath != null && absolutPath.isNotEmpty){
      imgPath = "${EnpointUrl.URL_CLOUD}$absolutPath";
    }
    return imgPath;
  }


  static getImageNameFromLocalPath(String path) {
    var arr = path.split("/");
    return arr[arr.length - 1];
  }

}