class HaveInternet {
  int? id;
  String? code;
  String? name;

  HaveInternet({
    this.id = 0, this.code = "false", this.name
  });

  HaveInternet.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  String getName(){
    return this.name ?? "";
  }

  String getCode(){
    return this.code ?? "";
  }
}

class HaveInternetesponse {
  List<HaveInternet> items = <HaveInternet>[];
  HaveInternetesponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new HaveInternet.fromJson(item);
        items.add(it);
      });
    }
  }
}