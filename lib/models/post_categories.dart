
import 'package:hmhapp/utils/images_util.dart';

class PostCategoryResponse {
  List<PostCategory> items = <PostCategory>[];

  PostCategoryResponse.fromJson(List<dynamic> json) {
    for (var item in json) {
      var it = PostCategory.fromJson(item);
      items.add(it);
    }
  }
}


class PostCategory {
  int? id;
  String? name;
  String? description;
  String? avatar;

  PostCategory();

  PostCategory.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    name = map['name'];
    description = map['description'];
    avatar = map['avatar'] != null ? ImageUtil.getFullImagePath(map['avatar']) : "";
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'description': description,
    'name': name,
  };

  toString() =>
      "Post Category => " + toJson().toString();
}