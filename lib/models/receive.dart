import 'package:flutter/cupertino.dart';

class Give {
  int? id;
  String? name;
  String? description;
  String? permalink;
  double? averageRating;
  int? ratingCount;
  List<String>? images;
  String? featuredImage;
  int? categoryId;
  double? lat;
  double? long;
  String? address;
  String? categoryName;
  String? openTime;
  String? phone;
  bool? isCertificate;
  Give({
    this.name ="",
    this.address ="",
    this.categoryName = "",
    this.openTime ="",
    this.phone = "",
    this.description ="",
    this.averageRating = 0,
    this.ratingCount = 0,
    this.isCertificate = false
  });


  Give.empty(int id) {
    this.id = id;
    name = 'Loading...';
    featuredImage = '';
    address = "";
    categoryName = "";
  }

  bool isEmptyProduct() {
    return name == 'Loading...' && featuredImage == '';
  }

  Give.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = parsedJson["name"];
    description = parsedJson["description"];
    permalink = parsedJson["permalink"];

    averageRating = double.parse(parsedJson["average_rating"]);
    ratingCount = int.parse(parsedJson["rating_count"].toString());
    categoryId =
        parsedJson["categories"] != null && parsedJson["categories"].length > 0
            ? parsedJson["categories"][0]["id"]
            : 0;
    List<String> list = [];
    for (var item in parsedJson["images"]) {
      list.add(item["src"]);
    }
    images = list;
    featuredImage = images![0];
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "description": description,
      "permalink": permalink,
      "averageRating": averageRating,
      "ratingCount": ratingCount,
      "images": images,
      "imageFeature": featuredImage,
      "categoryId": categoryId
    };
  }

  Give.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'];
      name = json['name'];
      description = json['description'];
      permalink = json['permalink'];
      averageRating = json['averageRating'];
      ratingCount = json['ratingCount'];
      List<String> imgs = [];
      for (var item in json['images']) {
        imgs.add(item);
      }
      images = imgs;
      featuredImage = json['imageFeature'];
      categoryId = json['categoryId'];
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  String toString() => 'Product { id: $id name: $name }';
}

class Review {
  int? id, productId;
  String? reviewer, reviewerEmail, review;
  int? rating;

  Review(
      {this.id,
      this.productId,
      this.reviewer,
      this.reviewerEmail,
      this.review,
      this.rating});

  Map<String, dynamic> toJson() {
    return {
      'product_id': productId,
      'reviewer': reviewer,
      'reviewer_email': reviewerEmail,
      'review': review,
      'rating': rating
    };
  }

  Review.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    productId = parsedJson['product_id'];
    reviewer = parsedJson['reviewer'];
    reviewerEmail = parsedJson['reviewer_email'];
    review = parsedJson['review'];
    rating = parsedJson['rating'];
  }
}
