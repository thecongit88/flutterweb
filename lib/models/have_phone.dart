class HavePhone {
  int? id;
  String? code;
  String? name;

  HavePhone({
    this.id = 0, this.code = "false", this.name
  });

  HavePhone.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  String getName(){
    return this.name ?? "";
  }

  String getCode(){
    return this.code ?? "";
  }
}

class HavePhoneResponse {
  List<HavePhone> items = <HavePhone>[];
  HavePhoneResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new HavePhone.fromJson(item);
        items.add(it);
      });
    }
  }
}