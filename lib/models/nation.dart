class NationResponse {
  List<Nation> items = <Nation>[];
  NationResponse.fromJson(List<dynamic> json) {
    json.forEach((item) {
      var it = new Nation.fromJson(item);
      items.add(it);
    });
  }
}

class Nation {
  int? id;
  String? title;
  int? code;

  Nation({
    this.id = 0, this.title = "",this.code=0
  });

  Nation.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["nation_name"] : "";
    try {
      code = parsedJson["nation_code"] != null && parsedJson["nation_code"]
          .toString()
          .isNotEmpty ?
      int.parse(parsedJson["nation_code"]) : 0;
    }catch(ex){
      code = 0;
    }

  }

  String geTitle(){
    return this.title ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }

  static bool isOther(List<Nation> items,int id){

    if ((items.singleWhere((it) => it.id == id && it.code == 99)) != null) {
      return true;
    } else {
      return false;
    }
  }
}