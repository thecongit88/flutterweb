import 'package:hmhapp/utils/images_util.dart';

class YoutubeModelResponse {
  List<YoutubeModel> items = <YoutubeModel>[];

  YoutubeModelResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new YoutubeModel.fromJson(item);
        items.add(it);
      });
    }
  }
}

class VideosPageResponse {
  List<YoutubeModel> items = <YoutubeModel>[];
  int pages = 0;

  VideosPageResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new YoutubeModel.fromJson(item);
        items.add(it);
      });
    }
  }
}


class YoutubeModel {
  int? id;
  String? title;
  String? description;
  String? thumbnail;
  String? groupName;
  String? channelTitle;
  String? channelAvatar;
  String? viewCount = "0";
  String? likeCount = "0";
  String? link;
  int? video_category_id;
  //String dislikeCount;

  YoutubeModel({
    this.id = 0,
    this.title = "",
    this.description = "",
    this.thumbnail = "",
    this.groupName = "",
    this.channelTitle = "",
    this.channelAvatar = "",
    this.viewCount = "",
    this.likeCount = "",
    this.link = "",
    this.video_category_id = 0,
    //this.dislikeCount
  });

  YoutubeModel.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    int view_count = int.parse(parsedJson["view_count"].toString() != "null" ? parsedJson["view_count"].toString() : "0");
    int like_count = int.parse(parsedJson["like_count"].toString() != "null" ? parsedJson["like_count"].toString() : "0");
    String view_count_str = "", like_count_str = "";
    if(view_count == 0) view_count_str = "0 view";
    else {
      if(view_count < 1000) view_count_str = (view_count).toString() + " views";
      if(view_count >= 1000) view_count_str = (view_count/1000).toString() + "K views";
      if(view_count >= 1000000) view_count_str = (view_count/1000000).toString() + "M views";
    }
    if(like_count == 0) like_count_str = "0 like";
    else {
      if(like_count >= 1000) like_count_str = (like_count/1000).toString() + "K likes";
      if(like_count < 1000) like_count_str = (like_count).toString() + " likes";
    }

    this.id = parsedJson["id"];
    this.title = parsedJson["title"];
    this.description = parsedJson["description"];
    this.thumbnail = ImageUtil.getFullImagePath(parsedJson["thumbnail"]);
    this.groupName = parsedJson["video_category"] !=null ? parsedJson["video_category"]["name"] : "";
    this.channelTitle = parsedJson["channelTitle"];
    this.channelAvatar = parsedJson["channelAvatar"];
    this.viewCount = view_count_str;
    this.likeCount = like_count_str;
    this.link = parsedJson["link"];
    this.video_category_id = parsedJson["video_category_id"];
    //this.dislikeCount = parsedJson["dislikeCount"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> parsedJson = new Map<String, dynamic>();
    parsedJson["id"] = this.id;
    parsedJson["title"] = this.title;
    parsedJson["description"] = this.description;
    parsedJson["thumbnail"] = this.thumbnail;
    parsedJson["groupName"] = this.groupName;
    parsedJson["channelTitle"] = this.channelTitle;
    parsedJson["channelAvatar"] = this.channelAvatar;
    parsedJson["viewCount"] = this.viewCount;
    parsedJson["likeCount"] = this.likeCount;
    parsedJson["link"] = this.link;
    parsedJson["video_category_id"] = this.video_category_id;
    //parsedJson["dislikeCount"] = this.dislikeCount;
    return parsedJson;
  }

  static List<YoutubeModel> youtubeData = [
    YoutubeModel(
      title: "Hướng dẫn TKT tự đi vệ sinh bằng bô",
      description:
      "Cách chăm sóc vệ sinh cá nhân",
      thumbnail:
      "https://i.ytimg.com/vi/ixkoVwKQaJg/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDrYjizQef0rnqvBc0mZyU3k13yrg",
      groupName: "Cách chăm sóc vệ sinh cá nhân",
      //channelTitle: "DJ Snake",
      channelAvatar:
      "https://yt3.ggpht.com/a-/AN66SAzkcvkwVn1Y5Zdpb1jkn9zyJ7vGxO8qHBxCTg=s288-mo-c-c0xffffffff-rj-k-no",
      viewCount: "50M views",
      likeCount: "34K",
      //dislikeCount: "2K",
    ),
    YoutubeModel(
      title: "Chăm sóc TKT nặng đi vệ sinh!",
      description: "Marques Brownlee gives his opinion on Pixel 3 XL",
      thumbnail:
      "https://i.ytimg.com/vi/Lg9N8XAZ6u4/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC5n3UMS9pjWuzugjML9AcoqbEMOA",
      groupName: "Đánh răng rửa mặt",
      //channelTitle: "Marqueus Brownlee",
      channelAvatar:
      "https://yt3.ggpht.com/a-/AN66SAwxVf-12cuqSiSP2HKPkpDqI0NCAghAiE7IVg=s288-mo-c-c0xffffffff-rj-k-no",
      viewCount: "917K views",
      likeCount: "20k",
      //dislikeCount: "51",
    ),
    YoutubeModel(
      title: "Kỹ năng hướng dẫn TKT tự tắm",
      description: "Listen to Venom (Music From The Motion Picture), out now: http://smarturl.it/EminemVenom",
      thumbnail:
      "https://i.ytimg.com/vi/8CdcCD5V-d8/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLA7A5_7k458KMkDNG0sweixgq856g",
      groupName: "Tắm rửa và thay quần áo",
      //channelTitle: "EminemMusic",
      channelAvatar:
      "https://yt3.ggpht.com/-qtnbIDAbSNQ/AAAAAAAAAAI/AAAAAAAAAJc/Zt6FE6ofr1I/s88-nd-c-c0xffffffff-rj-k-no/photo.jpg",
      viewCount: "53M views",
      likeCount: "20k",
      //dislikeCount: "51",
    ),
  ];

}

List<YoutubeModel> youtubeData = [
  YoutubeModel(
    title: "Hướng dẫn tắm cho TKT ở tư thế ngồi",
    description:
    "DJ Snake - Taki Taki ft. Selena Gomez, Ozuna, Cardi takes you on a ride",
    thumbnail:
    "https://i.ytimg.com/vi/ixkoVwKQaJg/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDrYjizQef0rnqvBc0mZyU3k13yrg",
    groupName: "Tắm rửa và thay quần áo",
    //channelTitle: "DJ Snake",
    channelAvatar:
    "https://yt3.ggpht.com/a-/AN66SAzkcvkwVn1Y5Zdpb1jkn9zyJ7vGxO8qHBxCTg=s288-mo-c-c0xffffffff-rj-k-no",
    viewCount: "50M views",
    likeCount: "34K",
    //dislikeCount: "2K",
  ),
  YoutubeModel(
    title: "Hướng dẫn TKT tự đánh răng",
    description: "Marques Brownlee gives his opinion on Pixel 3 XL",
    thumbnail:
    "https://i.ytimg.com/vi/Lg9N8XAZ6u4/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC5n3UMS9pjWuzugjML9AcoqbEMOA",
    groupName: "Đánh răng rửa mặt",
    //channelTitle: "Marqueus Brownlee",
    channelAvatar:
    "https://yt3.ggpht.com/a-/AN66SAwxVf-12cuqSiSP2HKPkpDqI0NCAghAiE7IVg=s288-mo-c-c0xffffffff-rj-k-no",
    viewCount: "917K views",
    likeCount: "20k",
    //dislikeCount: "51",
  ),
  YoutubeModel(
    title: "Kỹ năng đánh răng cho TKT, NKT",
    description: "Listen to Venom (Music From The Motion Picture), out now: http://smarturl.it/EminemVenom",
    thumbnail:
    "https://i.ytimg.com/vi/8CdcCD5V-d8/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLA7A5_7k458KMkDNG0sweixgq856g",
    groupName: "Đánh răng rửa mặt",
    //channelTitle: "EminemMusic",
    channelAvatar:
    "https://yt3.ggpht.com/-qtnbIDAbSNQ/AAAAAAAAAAI/AAAAAAAAAJc/Zt6FE6ofr1I/s88-nd-c-c0xffffffff-rj-k-no/photo.jpg",
    viewCount: "53M views",
    likeCount: "20k",
    //dislikeCount: "51",
  ),
];
