class DaCam {
  int id = 0;
  String code = "";
  String name = "";

  DaCam({
    this.id = 0, this.code = "false", this.name=""
  });

  DaCam.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  String getName(){
    return this.name;
  }

  String getCode(){
    return this.code;
  }
}

class DaCamResponse {
  List<DaCam> items = <DaCam>[];
  DaCamResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new DaCam.fromJson(item);
        items.add(it);
      });
    }
  }
}