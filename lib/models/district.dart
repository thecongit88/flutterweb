class District {
  int id = 0;
  String code = "";
  String name = "";

  District({
    this.id = 0, this.code = "", this.name = ""
  });

  District.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"] != null ? parsedJson["id"]  : 0;
    code = parsedJson["code"] ?? "";
    name = parsedJson["name"] ?? "";
  }

  String getName(){
    return this.name;
  }

  int getId(){
    return this.id;
  }
}

class DistrictResponse {
  List<District> items = <District>[];
  DistrictResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new District.fromJson(item);
        items.add(it);
      });
    }
  }
}