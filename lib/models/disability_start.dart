class DisabilityStart {
  int id = 0;
  String title = "";
  int code = 0;

  DisabilityStart({
    this.id = 0, this.title = "",this.code = 0
  });

  DisabilityStart.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    title = parsedJson["title"];
    code = parsedJson["code"];
  }

  String geTitle(){
    return this.title;
  }

  int getId(){
    return this.id;
  }
  int getCode(){
    return this.code;
  }


  static bool isOther(List<DisabilityStart> items,int id){

    if ((items.singleWhere((it) => it.id == id && it.code == 2)) != null) {
      return true;
    } else {
      return false;
    }
  }
}

class DisabilityStartResponse {
  List<DisabilityStart> items = <DisabilityStart>[];
  DisabilityStartResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new DisabilityStart.fromJson(item);
        items.add(it);
      });
    }
  }
}