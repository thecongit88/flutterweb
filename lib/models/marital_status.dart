class MaritalStatus {
  int? id;
  String? title;

  MaritalStatus({
    this.id = 0, this.title = ""
  });

  MaritalStatus.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["title"] : "";
  }

  String geTitle(){
    return this.title ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }
}

class MaritalStatusResponse {
  List<MaritalStatus> items = <MaritalStatus>[];
  MaritalStatusResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new MaritalStatus.fromJson(item);
        items.add(it);
      });
    }
  }
}