class ReportListThuchienKehoachResponse {
  List<ReportThuchienKehoach> items = <ReportThuchienKehoach>[];

  ReportListThuchienKehoachResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new ReportThuchienKehoach.fromJson(item);
        items.add(it);
      });
    }
  }
}

class ReportThuchienKehoachResponse {
  ReportThuchienKehoach? reportThuchienKehoach;

  ReportThuchienKehoachResponse.fromJson(Map<String, dynamic> json) {
    reportThuchienKehoach = ReportThuchienKehoach.fromJson(json);
  }
}

class ReportThuchienKehoach {
  int? id;
  int? month;
  int? year;
  int? soluong_theokehoach;
  int? soluong_khongtheokehoach;
  int? hbccId;

  ReportThuchienKehoach({
    this.id = 0,
    this.month = 0,
    this.year = 0,
    this.soluong_theokehoach = 0,
    this.soluong_khongtheokehoach = 0,
    this.hbccId = 0,
  });

  ReportThuchienKehoach.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.month = parsedJson["month"];
    this.year = parsedJson["year"];
    this.soluong_theokehoach = parsedJson["soluong_theokehoach"];
    this.soluong_khongtheokehoach = parsedJson["soluong_khongtheokehoach"];
    this.hbccId = parsedJson["hbcc"] != null ? parsedJson["hbcc"]["id"] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['month'] = this.month;
    data['year'] = this.year;
    data['soluong_theokehoach'] = this.soluong_theokehoach;
    data['soluong_khongtheokehoach'] = this.soluong_khongtheokehoach;
    data['hbccId'] = this.hbccId;
    return data;
  }
}