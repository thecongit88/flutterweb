class FormTypeResponse {
  List<FormType> items = <FormType>[];
  FormTypeResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new FormType.fromJson(item);
        items.add(it);
      });
    }
  }
}


class FormType {
  int? id;
  String? formName;
  String? icon;
  Map? formFormat;
  String? formObject;
  bool? formKynang;
  bool? formNCS;
  bool? formDay;

  FormType();

  FormType.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    formFormat = map['form_format'];
    formName = map['form_name'];
    icon = map['icon'];
    formKynang = map['form_kynang'] != null?  map['form_kynang'] : false;
    formObject = refectFormObjectName(map['form_object']);
    formNCS = map['form_ncs'] != null ?  map['form_ncs'] : false;
    formDay = map['form_day'] != null ?  map['form_day'] : false;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'form_name': formName,
    'form_format': formFormat,
    'form_object': formObject,
    'form_kynang': formKynang,
    'form_ncs': formNCS,
    'form_day': formDay,
    'icon': icon
  };

  refectFormObjectName(String string) {
    if(string == null || string.isEmpty) return "";
    String _lastCharacter = string.length > 0 ? string.substring(string.length - 1) : "";
    if(_lastCharacter == "s") return string;
    return string+"s";
  }

  toString() =>
      "FormType => " + toJson().toString();
}
