class GenderResponse {
  List<Gender> items = <Gender>[];
  GenderResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Gender.fromJson(item);
        items.add(it);
      });
    }
  }
}

class Gender {
  int? id;
  String? name;

  Gender({
    this.id = 0, this.name = ""
  });

  Gender.fromJson(Map<String, dynamic> parsedJson) {
    id =  parsedJson != null ? parsedJson["id"] : 0;
    name =  parsedJson != null ? parsedJson["Name"] : "";
  }

  String getName(){
    return this.name ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }
}