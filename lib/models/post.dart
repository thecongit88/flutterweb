
import 'package:hmhapp/utils/images_util.dart';

class PostResponse {
  List<Post> items = <Post>[];
  PostResponse.fromJson(List<dynamic> json) {
    json.forEach((item) {
      var it = new Post.fromJson(item);
      items.add(it);
    });
  }
}


class PostPageResponse {
  List<Post> items = <Post>[];
  int pages = 0;

  PostPageResponse.fromJson(List<dynamic> json) {
    json.forEach((item) {
      var it = new Post.fromJson(item);
      items.add(it);
    });
  }
}

class Post {
  Post({
    this.title = '',
    this.content = '',
    this.description = '',
    this.thumbnail = '',
  });

  int? id;
  String? title;
  String? description;
  String? content;
  String? thumbnail;
  String? created_at;
  int? categoryId;
  String? categoryName;
  String? Link;


  Post.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    title = map['title'];
    description = map['description'];
    content = map['content'];
    created_at = map['created_at'];
    thumbnail = map['thumbnail'] != null ? ImageUtil.getFullImagePath(map['thumbnail']) : "";
    categoryId = map['post_category']!= null? map['post_category']['id'] :0;
    categoryName = map['post_category']!= null? map['post_category']['name']:"";
    Link = map['Link'] ?? "";
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'description': description,
    'content': content,
    'created_at': created_at,
    'Link': Link,
  };

}
