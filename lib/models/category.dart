class Category {
  int id = 0;
  int count = 0;
  String description = "";
  String link = "";
  String name = "";
  String slug = "";
  String taxonomy = "";
  int parent = 0;
  dynamic meta; // List?

  Category();

  Category.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }

    id = map['id'];
    count = map['count'];
    description = map['description'];
    link = map['link'];
    name = map['name'];
    slug = map['slug'];
    taxonomy = map['taxonomy'];
    parent = map['parent'];
    meta = map['meta'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'count': count,
    'description': description,
    'link': link,
    'name': name,
    'slug': slug,
    'taxonomy': taxonomy,
    'parent': parent,
    'meta': meta
  };

  toString() =>
      "Category => " + toJson().toString();
}
