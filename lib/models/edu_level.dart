class EduLevel {
  int id = 0;
  String title = "";

  EduLevel({
    this.id = 0, this.title = ""
  });

  EduLevel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["title"] : "";
  }

  String geTitle(){
    return this.title;
  }

  int getId(){
    return this.id;
  }
}

class EduLevelResponse {
  List<EduLevel> items = <EduLevel>[];
  EduLevelResponse.fromJson(List<dynamic> json) {
    json.forEach((item) {
      var it = new EduLevel.fromJson(item);
      items.add(it);
    });
  }
}