class IsSmartphone {
  int? id;
  String? code;
  String? name;

  IsSmartphone({
    this.id = 0, this.code = "false", this.name
  });

  IsSmartphone.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  String getName(){
    return this.name ?? "";
  }

  String getCode(){
    return this.code ?? "";
  }
}

class IsSmartphoneResponse {
  List<IsSmartphone> items = <IsSmartphone>[];
  IsSmartphoneResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new IsSmartphone.fromJson(item);
        items.add(it);
      });
    }
  }
}