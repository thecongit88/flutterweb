class UserResponse {
  String? token;
  User? user;

  UserResponse.fromJson(Map<String, dynamic> json) {
    token = json['jwt'];
    user = User.fromJson(json['user']);
    }
}


class User {
  int? id;
  String? username;
  String? email;
  String? provider;
  bool? confirmed;
  bool? blocked;
  String? created_at;
  String? updated_at;
  bool? inValid;
  bool? approved;

  User({
      this.id,
      this.username,
      this.email,
      this.provider,
      this.confirmed,
      this.blocked,
      this.created_at,
      this.updated_at,
      this.inValid = true,
      this.approved = true
      });


  User.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    username = parsedJson["username"];
    email = parsedJson["email"];
    provider = parsedJson["provider"];
    confirmed = parsedJson["confirmed"];
    blocked = parsedJson["blocked"];
    created_at = parsedJson["created_at"];
    updated_at = parsedJson["updated_at"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['email'] = this.email;
    data['provider'] = this.provider;
    data['confirmed'] = this.confirmed;
    data['blocked'] = this.blocked;
    data['created_at'] = this.created_at;
    data['updated_at'] = this.updated_at;
    return data;
  }

  User.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json["id"];
      username = json["username"];
      email = json["email"];
      provider = json["provider"];
      confirmed = json["confirmed"];
      blocked = json["blocked"];
      created_at = json["created_at"];
      updated_at = json["updated_at"];
    } catch (e) {
      print(e.toString());
    }
  }

  bool isValid() {
    return username!.isNotEmpty &&
        email!.isNotEmpty;
  }

  @override
  String toString() {
    return '\nUser Name:$username\nemail:$email\n';
  }
}

class UserLogin {
  String? identifier;
  String? password;

  UserLogin({
    this.identifier,
    this.password,
  });


  UserLogin.fromJson(Map<String, dynamic> parsedJson) {
    identifier = parsedJson["identifier"];
    password = parsedJson["password"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identifier'] = this.identifier;
    data['password'] = this.password;
    return data;
  }

  bool isValid() {
    return identifier!.isNotEmpty &&
        password!.isNotEmpty;
  }

  @override
  String toString() {
    return '\nIdentifier:$identifier\nPassword:$password\n';
  }
}

class UserRegister{
  String? username;
  String? email;
  String? provider;
  String? password;
  String? resetPasswordToken;
  bool? confirmed;
  bool? blocked;
  String? role;


  UserRegister({
    this.username,
    this.email,
    this.provider,
    this.password,
    this.resetPasswordToken,
    this.confirmed,
    this.blocked,
    this.role
  });


  UserRegister.fromJson(Map<String, dynamic> parsedJson) {
    username = parsedJson["username"];
    password = parsedJson["password"];
    email = parsedJson["email"];
    provider = parsedJson["provider"];
    resetPasswordToken = parsedJson["resetPasswordToken"];
    confirmed = parsedJson["confirmed"];
    blocked = parsedJson["blocked"];
    role = parsedJson["role"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    data['email'] = this.email;
    data['provider'] = this.provider;
    data['resetPasswordToken'] = this.resetPasswordToken;
    data['confirmed'] = this.confirmed;
    data['blocked'] = this.blocked;
    data['role'] = this.role;
    return data;
  }

}

class UserChangePassword {
  String? username;
  String? password;
  String? passwordConfirmation;

  UserChangePassword({
    this.username,
    this.password,
    this.passwordConfirmation
  });


  UserChangePassword.fromJson(Map<String, dynamic> parsedJson) {
    username = parsedJson["username"];
    password = parsedJson["password"];
    passwordConfirmation = parsedJson["passwordConfirmation"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    data['passwordConfirmation'] = this.passwordConfirmation;
    return data;
  }

  bool isValid() {
    return username!.isNotEmpty &&
        password!.isNotEmpty && passwordConfirmation!.isNotEmpty && password == passwordConfirmation;
  }

  @override
  String toString() {
    return '\nusername:$username\nPassword:$password\n';
  }
}
