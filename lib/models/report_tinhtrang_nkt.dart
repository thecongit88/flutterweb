class ReportListTinhTrangNKTResponse {
  List<ReportTinhTrangNKT> items = <ReportTinhTrangNKT>[];

  ReportListTinhTrangNKTResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new ReportTinhTrangNKT.fromJson(item);
        items.add(it);
      });
    }
  }
}

class ReportTinhTrangNKTResponse {
  ReportTinhTrangNKT? reportTanSuatNCS;

  ReportTinhTrangNKTResponse.fromJson(Map<String, dynamic> json) {
    reportTanSuatNCS = ReportTinhTrangNKT.fromJson(json);
  }
}

class ReportTinhTrangNKT {
  int? id;
  int? month;
  int? year;
  int? scores;
  String? info;
  int? memberId;

  ReportTinhTrangNKT({
    this.id = 0,
    this.month = 0,
    this.year = 0,
    this.scores = 0,
    this.info = "",
    this.memberId = 0,
  });

  ReportTinhTrangNKT.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.month = parsedJson["month"];
    this.year = parsedJson["year"];
    this.scores = parsedJson["scores"];
    this.info = parsedJson["info"];
    this.memberId = parsedJson["member"] != null ? parsedJson["member"]["id"] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['month'] = this.month;
    data['year'] = this.year;
    data['scores'] = this.scores;
    data['info'] = this.info;
    data['member'] = this.memberId;
    return data;
  }
}