
class FileUploadSimple {
  FileUploadSimple({
    this.path,
    this.name
  });

  String? path;
  String? name;

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['path'] = "${this.path}";
    data['name'] = "${this.name}";
    return data;
  }
}
