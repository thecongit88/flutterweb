import 'dart:convert';

import 'package:hmhapp/services/enpoint_url.dart';
import 'package:hmhapp/utils/constants.dart';

class FileResultUploadResponse {
  factory FileResultUploadResponse.fromJson(String str) => FileResultUploadResponse.fromMap(json.decode(str));

  List<FileResultUpload> items = <FileResultUpload>[];
  FileResultUploadResponse.fromMap(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new FileResultUpload.fromMap(item);
        items.add(it);
      });
    }
  }
}


class FileResultUpload {
  FileResultUpload({
    this.id,
    this.name,
    this.alternativeText,
    this.caption,
    this.width,
    this.height,
    this.formats,
    this.hash,
    this.ext,
    this.mime,
    this.size,
    this.url,
    this.previewUrl,
    this.provider,
    this.providerMetadata,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? alternativeText;
  String? caption;
  int? width;
  int? height;
  Formats? formats;
  String? hash;
  String? ext;
  String? mime;
  double? size;
  String? url;
  String? previewUrl;
  String? provider;
  String? providerMetadata;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory FileResultUpload.fromJson(String str) => FileResultUpload.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory FileResultUpload.fromMap(Map<String, dynamic> json) => FileResultUpload(
    id: json["id"],
    name: json["name"],
    alternativeText: json["alternativeText"],
    caption: json["caption"],
    width: json["width"],
    height: json["height"],
    formats: Formats.fromMap(json["formats"]),
    hash: json["hash"],
    ext: json["ext"],
    mime: json["mime"],
    size: json["size"].toDouble(),
    url: "${EnpointUrl.URL_CLOUD}${json["url"]}",
    previewUrl: "${EnpointUrl.URL_CLOUD}${json["previewUrl"]}",
    provider: json["provider"],
    providerMetadata: json["provider_metadata"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "alternativeText": alternativeText,
    "caption": caption,
    "width": width,
    "height": height,
    "formats": formats?.toMap(),
    "hash": hash,
    "ext": ext,
    "mime": mime,
    "size": size,
    "url": url,
    "previewUrl": previewUrl,
    "provider": provider,
    "provider_metadata": providerMetadata,
    "created_at": createdAt?.toIso8601String(),
    "updated_at": updatedAt?.toIso8601String(),
  };
}

class Formats {
  Formats({
    this.large,
    this.small,
    this.medium,
    this.thumbnail,
  });

  Large? large;
  Large? small;
  Large? medium;
  Large? thumbnail;

  factory Formats.fromJson(String str) => Formats.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Formats.fromMap(Map<String, dynamic> json) => Formats(
    large: json["large"] != null && json["large"].toString().toLowerCase() != "null" ? Large.fromMap(json["large"]) : null,
    small: json["small"] != null && json["small"].toString().toLowerCase() != "null" ? Large.fromMap(json["small"]) : null,
    medium: json["medium"] != null && json["medium"].toString().toLowerCase() != "null" ? Large.fromMap(json["medium"]) : null,
    thumbnail: json["thumbnail"] != null && json["thumbnail"].toString().toLowerCase() != "null" ? Large.fromMap(json["thumbnail"]) : null,
  );

  Map<String, dynamic> toMap() => {
    "large": large?.toMap(),
    "small": small?.toMap(),
    "medium": medium?.toMap(),
    "thumbnail": thumbnail?.toMap(),
  };
}

class Large {
  Large({
    this.ext,
    this.url,
    this.hash,
    this.mime,
    this.path,
    this.size,
    this.width,
    this.height,
  });

  String? ext;
  String? url;
  String? hash;
  String? mime;
  String? path;
  double? size;
  int? width;
  int? height;

  factory Large.fromJson(String str) => Large.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Large.fromMap(Map<String, dynamic> json) => Large(
    ext: json["ext"],
    url: "${EnpointUrl.URL_CLOUD}${json["url"]}",
    hash: json["hash"],
    mime: json["mime"],
    path: json["path"],
    size: json["size"].toDouble(),
    width: json["width"],
    height: json["height"],
  );

  Map<String, dynamic> toMap() => {
    "ext": ext,
    "url": url,
    "hash": hash,
    "mime": mime,
    "path": path,
    "size": size,
    "width": width,
    "height": height,
  };
}