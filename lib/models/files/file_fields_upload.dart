

class FileFieldUpload {
  FileFieldUpload({
    this.refId,
    this.ref,
    this.field,
  });

  int? refId;
  String? ref;
  String? field;

  Map<String, String> toJson() => {
    "refId": "${refId}",
    "ref": "${ref}",
    "field": "${field}"
  };
}
