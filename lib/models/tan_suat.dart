class TanSuatClassResponse {
  List<TanSuatCLass> items = <TanSuatCLass>[];

  TanSuatClassResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new TanSuatCLass.fromJson(item);
        items.add(it);
      });
    }
  }
}

class TanSuatCLass {
  int? id;
  String? title;
  int? value;

  TanSuatCLass({
    this.id = 0,
    this.title = '',
    this.value
  });

  TanSuatCLass.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.title = parsedJson["name"];
    this.value = parsedJson["value"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.title;
    data['value'] = this.value;
    return data;
  }
}