class RelationshipTypeResponse {
  List<RelationshipType> items = <RelationshipType>[];

  RelationshipTypeResponse.fromJson(List<dynamic> json) {
    json.forEach((item) {
      var it = new RelationshipType.fromJson(item);
      items.add(it);
    });
  }
}

class RelationshipType {
  int? id;
  String? title;
  int? code;

  RelationshipType({
    this.id = 0,
    this.title = "",
    this.code = 0
  });

  RelationshipType.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["title"] : "";
    code = parsedJson != null ? parsedJson["code"] : "";
  }

  String geTitle(){
    return this.title ?? "";
  }


  static bool isOther(List<RelationshipType> items,int id){

    if ((items.singleWhere((it) => it.id == id && it.code == 99)) != null) {
      return true;
    } else {
      return false;
    }
  }
}