class JobType {
  int? id;
  String? title;
  int? code;

  JobType({
    this.id = 0, this.title = "",this.code
  });

  JobType.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson != null ? parsedJson["id"] : 0;
    title = parsedJson != null ? parsedJson["title"] : "";
    code = parsedJson != null ? parsedJson["code"] : "";
  }

  String geTitle(){
    return this.title ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }
  int getCode(){
    return this.code ?? 0;
  }


  static bool isOther(List<JobType> items,int id){

    if ((items.singleWhere((it) => it.id == id && it.code == 99)) != null) {
      return true;
    } else {
      return false;
    }
  }
}

class JobTypeResponse {
  List<JobType> items = <JobType>[];
  JobTypeResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new JobType.fromJson(item);
        items.add(it);
      });
    }
  }
}