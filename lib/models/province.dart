class Province {
  int? id;
  String? code;
  String? name;
  int? ordering;

  Province({
    this.id = 0, this.code = "", this.name,this.ordering
  });

  Province.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
    ordering = parsedJson["ordering"];
  }


  Map<String, dynamic> toJson() => {
    'id': id,
    'code': code,
    'name': name,
    'ordering': ordering
  };

  String getName(){
    return this.name ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }
}

class ProvinceResponse {
  List<Province> items = <Province>[];
  ProvinceResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Province.fromJson(item);
        items.add(it);
      });
    }
  }
}