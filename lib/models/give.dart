import 'package:flutter/cupertino.dart';

class Give {
  int? id;
  String? title;
  String? excerpt;
  String? permalink;
  double? averageRating;
  int? ratingCount;
  List<String>? images;
  String? featuredMediaUrl;
  String? featuredMediaFullUrl;
  bool? featuredMediaThumb;
  int? categoryId;
  double? lat;
  double? long;
  String? address;
  String? categoryName;
  String? openTime;
  String? phone;
  String? contact;
  String? facebook;
  String? gmail;
  String? zalo;
  String? content;
  DateTime? date;
  DateTime? startDate;
  DateTime? endDate;
  bool? isCertificate;
  Give({
    this.title ="",
    this.address ="",
    this.categoryName = "",
    this.openTime ="",
    this.phone = "",
    this.excerpt ="",
    this.contact = "",
    this.facebook = "",
    this.gmail = "",
    this.zalo = "",
    this.featuredMediaThumb = false,
    this.featuredMediaUrl = "",
    this.featuredMediaFullUrl = "",
    this.averageRating = 0,
    this.ratingCount = 0,
    this.isCertificate = false
  });


  Give.empty(int id) {
    this.id = id;
    title = 'Loading...';
    featuredMediaUrl = '';
    address = "";
    categoryName = "";
  }

  bool isEmptyProduct() {
    return title  == 'Loading...' && featuredMediaUrl == '';
  }

  Give.fromJson(Map<String, dynamic> map) {
    this.id = map['id'];
    this.title = map['title']["rendered"];
    this.excerpt = map['excerpt']["rendered"];
    this.content = map['content']['rendered'];
    this.categoryName = "";
    this.date = map["date"] != null
        ? DateTime.parse(map["date"])
        : null;
    this.featuredMediaUrl = map['featured_media_url'] == null
        ? 'assets/images/placeholder.png'
        :map['featured_media_url'];
    this.featuredMediaThumb = map['featured_media_url'] == null? false:true;
    this.featuredMediaFullUrl = map['featured_media_url'] == null
        ? 'assets/images/placeholder.png'
        :map['featured_media_url'];
    this.averageRating = 0;
    this.address = (map["location-meta"] != null && map["location-meta"]["location_address"] != null)?
    map["location-meta"]["location_address"][0]:"";
    var latlong = (map["location-meta"] != null && map["location-meta"]["location_location"] != null)?
    map["location-meta"]["location_location"][0]:"";
    if(latlong!=""){
      var latlonarr = latlong.split(",");
      if(latlonarr.length==2) {
        this.lat = latlonarr[0]!=""?double.parse(latlonarr[0]):0;
        this.long = latlonarr[1]!=""?double.parse(latlonarr[1]):0;
      }
    }
    this.isCertificate = (map["location-meta"] != null && map["location-meta"]["location_certificate"] != null)?
    (map["location-meta"]["location_certificate"][0]=="on"):false;
    this.phone = (map["location-meta"] != null && map["location-meta"]["location_phone"] != null)?
    map["location-meta"]["location_phone"][0]:"";
    this.facebook = (map["location-meta"] != null && map["location-meta"]["location_facebook"] != null)?
    map["location-meta"]["location_facebook"][0]:"";
    this.gmail = (map["location-meta"] != null && map["location-meta"]["location_gmail"] != null)?
    map["location-meta"]["location_gmail"][0]:"";
    this.zalo = (map["location-meta"] != null && map["location-meta"]["location_zalo"] != null)?
    map["location-meta"]["location_zalo"][0]:"";
    this.contact = (map["location-meta"] != null && map["location-meta"]["location_contact"] != null)?
    map["location-meta"]["location_contact"][0]:"";

  }


  Give.fromMapObject(Map<String, dynamic> map) {
    this.id = map['id'];
    this.title = map['title']["rendered"];
    this.excerpt = map['excerpt']["rendered"];
    this.date = map["date"] != null
        ? DateTime.parse(map["date"])
        : null;
    this.featuredMediaUrl = map['featured_media_url'] == null
        ? 'assets/images/placeholder.png'
        :map['featured_media_url'];
    this.featuredMediaThumb = map['featured_media_url'] == null? false:true;
    this.featuredMediaFullUrl = map['featured_media_url'] == null
        ? 'assets/images/placeholder.png'
        :map['featured_media_url'];
    this.averageRating = 0;
    this.address = (map["location-meta"] != null && map["location-meta"]["location_address"] != null)?
    map["location-meta"]["location_address"][0]:"";
    var latlong = (map["location-meta"] != null && map["location-meta"]["location_location"] != null)?
    map["location-meta"]["location_location"][0]:"";
    if(latlong!=""){
      var latlonarr = latlong.split(",");
      if(latlonarr.length==2) {
       this.lat = latlonarr[0]!=""?double.parse(latlonarr[0]):0;
       this.long = latlonarr[1]!=""?double.parse(latlonarr[1]):0;
      }
    }
    this.isCertificate = (map["location-meta"] != null && map["location-meta"]["location_certificate"] != null)?
    (map["location-meta"]["location_certificate"][0]=="on"):false;
    this.phone = (map["location-meta"] != null && map["location-meta"]["location_phone"] != null)?
    map["location-meta"]["location_phone"][0]:"";
    this.facebook = (map["location-meta"] != null && map["location-meta"]["location_facebook"] != null)?
    map["location-meta"]["location_facebook"][0]:"";
    this.gmail = (map["location-meta"] != null && map["location-meta"]["location_gmail"] != null)?
    map["location-meta"]["location_gmail"][0]:"";
    this.zalo = (map["location-meta"] != null && map["location-meta"]["location_zalo"] != null)?
    map["location-meta"]["location_zalo"][0]:"";
    this.contact = (map["location-meta"] != null && map["location-meta"]["location_contact"] != null)?
    map["location-meta"]["location_contact"][0]:"";
  }


  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "title": title,
      "description": excerpt ,
      "permalink": permalink,
      "averageRating": averageRating,
      "ratingCount": ratingCount,
      "images": images,
      "featuredMediaUrl": featuredMediaUrl,
      "categoryId": categoryId
    };
  }

  Give.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'];
      title = json['title'];
      excerpt = json['excerpt'];
      permalink = json['permalink'];
      averageRating = json['averageRating'];
      ratingCount = json['ratingCount'];
      List<String> imgs = [];
      for (var item in json['images']) {
        imgs.add(item);
      }
      images = imgs;
      featuredMediaUrl = json['featuredMediaUrl'];
      categoryId = json['categoryId'];
    } catch (e) {
      print(e.toString());
    }
  }



  @override
  String toString() => 'Product { id: $id title: $title }';
}

class Review {
  int? id, productId;
  String? reviewer, reviewerEmail, review;
  int? rating;

  Review(
      {this.id,
      this.productId,
      this.reviewer,
      this.reviewerEmail,
      this.review,
      this.rating});

  Map<String, dynamic> toJson() {
    return {
      'product_id': productId,
      'reviewer': reviewer,
      'reviewer_email': reviewerEmail,
      'review': review,
      'rating': rating
    };
  }

  Review.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    productId = parsedJson['product_id'];
    reviewer = parsedJson['reviewer'];
    reviewerEmail = parsedJson['reviewer_email'];
    review = parsedJson['review'];
    rating = parsedJson['rating'];
  }
}
