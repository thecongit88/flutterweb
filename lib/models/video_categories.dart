
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/utils/images_util.dart';

class VideoCategoryResponse {
  List<VideoCategory> items = <VideoCategory>[];

  VideoCategoryResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new VideoCategory.fromJson(item);
        items.add(it);
      });
    }
  }
}

class VideoCategory {
  int? id;
  String? name;
  String? description;
  String? avatar;


  VideoCategory.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    name = map['name'];
    description = map['description'];
    avatar = ImageUtil.getFullImagePath(map['avatar']);
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'description': description,
    'name': name,
  };

  toString() =>
      "Video Category => " + toJson().toString();
}