class DisabilityType {
  int id = 0;
  String title = "";

  DisabilityType({
    this.id = 0, this.title = ""
  });

  DisabilityType.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    title = parsedJson["type_name"];
  }

  String geTitle(){
    return this.title;
  }

  int getId(){
    return this.id;
  }
}

class DisabilityTypeResponse {
  List<DisabilityType> items = <DisabilityType>[];
  DisabilityTypeResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new DisabilityType.fromJson(item);
        items.add(it);
      });
    }
  }
}