class ReportListTanSuatNCSResponse {
  List<ReportTanSuatNCS> items = <ReportTanSuatNCS>[];

  ReportListTanSuatNCSResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new ReportTanSuatNCS.fromJson(item);
        items.add(it);
      });
    }
  }
}

class ReportTanSuatNCSResponse {
  ReportTanSuatNCS? reportTanSuatNCS;

  ReportTanSuatNCSResponse.fromJson(Map<String, dynamic> json) {
    reportTanSuatNCS = ReportTanSuatNCS.fromJson(json);
  }
}

class ReportTanSuatNCS {
  int? id;
  int? month;
  int? year;
  int? tansuat_kehoach;
  int? tansuat_thuchien;
  int? memberId;

  ReportTanSuatNCS({
    this.id = 0,
    this.month = 0,
    this.year = 0,
    this.tansuat_kehoach = 0,
    this.tansuat_thuchien = 0,
    this.memberId = 0,
  });

  ReportTanSuatNCS.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.month = parsedJson["month"];
    this.year = parsedJson["year"];
    this.tansuat_kehoach = parsedJson["tansuat_kehoach"];
    this.tansuat_thuchien = parsedJson["tansuat_thuchien"];
    this.memberId = parsedJson["member"] != null ? parsedJson["member"]["id"] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['month'] = this.month;
    data['year'] = this.year;
    data['tansuat_kehoach'] = this.tansuat_kehoach;
    data['tansuat_thuchien'] = this.tansuat_thuchien;
    data['member'] = this.memberId;
    return data;
  }
}