import 'package:flutter/material.dart';

class TabIcon {
  TabIcon({
    this.icon,
    this.title = "",
    this.index = 0,
    this.isSelected = false,
    this.animationController,
  });

  IconData? icon;
  bool? isSelected;
  String? title = "";
  int? index;

  AnimationController? animationController;

  static List<TabIcon> tabIconsMemberList = <TabIcon>[
    TabIcon(
      icon: Icons.home,
      title: "Trang chủ",
      index: 0,
      isSelected: true,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.access_time,
      title: "Khảo sát",
      index: 1,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.video_label,
      title: "Video",
      index: 2,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.post_add,
      title: "Kế hoạch",
      index: 3,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.perm_identity,
      title: "Cá nhân",
      index: 3,
      isSelected: false,
      animationController: null,
    ),
  ];

  static List<TabIcon> tabIconsManagerList = <TabIcon>[
    TabIcon(
      icon: Icons.home,
      title: "Trang chủ",
      index: 0,
      isSelected: true,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.group,
      title: "NKT",
      index: 1,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.access_time,
      title: "Đánh giá",
      index: 2,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.post_add,
      title: "Kế hoạch",
      index: 3,
      isSelected: false,
      animationController: null,
    ),
    TabIcon(
      icon: Icons.pie_chart,
      title: "Báo cáo",
      index: 4,
      isSelected: false,
      animationController: null,
    ),
  ];
}
