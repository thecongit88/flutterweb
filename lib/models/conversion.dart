import 'package:flutter/material.dart';

class Conversion {
  int id = 0;
  String name = "";
  String image = "";
  int parent = 0;
  int totalProduct = 0;

  Conversion(
      {this.id = 0,
        this.name = "",
        this.image = "",
        this.parent = 0,
        this.totalProduct = 0});

  Conversion.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson["slug"] == 'uncategorized') {
      return;
    }

    id = parsedJson["id"];
    name = parsedJson["name"];
    parent = parsedJson["parent"];
    totalProduct = parsedJson["count"];

    final image = parsedJson["image"];
    if (image != null) {
      this.image = image["src"];
    } else {
      this.image = 'category_image_default.jpg';
    }
  }

  Conversion.fromLocalJson(Map<String, dynamic> parsedJson) {
    try {
      if (parsedJson["slug"] == 'uncategorized') {
        return;
      }

      id = parsedJson["id"];
      name = parsedJson["name"];
      parent = parsedJson["parent"];
      totalProduct = parsedJson["count"];

      final image = parsedJson["image"];
      if (image != null) {
        this.image = image["src"];
      } else {
        this.image = 'category_image_default.jpg';
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  String toString() => 'Category { id: $id  name: $name}';
}
