class ReportListTinhTrangNktHbccResponse {
  List<ReportTinhTrangNktHbcc> items = <ReportTinhTrangNktHbcc>[];

  ReportListTinhTrangNktHbccResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new ReportTinhTrangNktHbcc.fromJson(item);
        items.add(it);
      });
    }
  }
}

class ReportTinhTrangNktHbccResponse {
  ReportTinhTrangNktHbcc? reportTinhTrangNktHbcc;

  ReportTinhTrangNktHbccResponse.fromJson(Map<String, dynamic> json) {
    reportTinhTrangNktHbcc = ReportTinhTrangNktHbcc.fromJson(json);
  }
}

class ReportTinhTrangNktHbcc {
  int? id;
  int? month;
  int? year;
  int? count_xauhon;
  int? count_giunguyen;
  int? count_caithienit;
  int? count_caithienvua;
  int? count_caithiennhieu;
  int? hbccId;

  ReportTinhTrangNktHbcc({
    this.id = 0,
    this.month = 0,
    this.year = 0,
    this.count_xauhon = 0,
    this.count_giunguyen = 0,
    this.count_caithienit = 0,
    this.count_caithienvua = 0,
    this.count_caithiennhieu = 0,
    this.hbccId = 0,
  });

  ReportTinhTrangNktHbcc.fromJson(Map<String, dynamic> parsedJson) {
    this.id = parsedJson["id"];
    this.month = parsedJson["month"];
    this.year = parsedJson["year"];
    this.count_xauhon = parsedJson["count_xauhon"];
    this.count_giunguyen = parsedJson["count_giunguyen"];
    this.count_caithienit = parsedJson["count_caithienit"];
    this.count_caithienvua = parsedJson["count_caithienvua"];
    this.count_caithiennhieu = parsedJson["count_caithiennhieu"];
    this.hbccId = parsedJson["hbcc"] != null ? parsedJson["hbcc"]["id"] : 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['month'] = this.month;
    data['year'] = this.year;
    data['count_xauhon'] = this.count_xauhon;
    data['count_giunguyen'] = this.count_giunguyen;
    data['count_caithienit'] = this.count_caithienit;
    data['count_caithienvua'] = this.count_caithienvua;
    data['count_caithiennhieu'] = this.count_caithiennhieu;
    data['hbcc'] = this.hbccId;
    return data;
  }
}