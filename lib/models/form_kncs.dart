import 'package:hmhapp/dp_members/models/plan.dart';


class FormKNCS{
  int? id;
  int? scores;
  String? result;
  Map? formFormat;
  Plan? plan;

  FormKNCS();

  FormKNCS.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    formFormat = map['form_format'];
    scores = map['scores'];
    result = map['result'];
    plan = map['plan'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'scores': scores,
    'form_format': formFormat,
    'result': result,
    'plan': plan,
  };

  refectFormObjectName(String string) {
    if(string == null || string.isEmpty) return "";
    String _lastCharacter = string.length > 0 ? string.substring(string.length - 1) : "";
    if(_lastCharacter == "s") return string;
    return string+"s";
  }

  toString() =>
      "FormKNCSItem => " + toJson().toString();
}
