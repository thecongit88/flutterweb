class Ward {
  int? id;
  String? code;
  String? name;

  Ward({
    this.id = 0, this.code = "", this.name
  });

  Ward.fromJson(Map<String, dynamic> parsedJson) {
    if(parsedJson == null) return;
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  String getName(){
    return this.name ?? "";
  }

  int getId(){
    return this.id ?? 0;
  }
}

class WardResponse {
  List<Ward> items = <Ward>[];
  WardResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new Ward.fromJson(item);
        items.add(it);
      });
    }
  }
}