class FormSurveyResponse {
  List<FormSurvey> items = <FormSurvey>[];
  FormSurveyResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new FormSurvey.fromJson(item);
        items.add(it);
      });
    }
  }
}


class FormSurvey {
  int? id;
  String? formDate;
  int? memberId;

  FormSurvey();

  FormSurvey.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    formDate = map['form_date'] != null ? map['form_date'] : "";
    memberId = map['member'] != null ? map['member']["id"] : 0;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'form_date': formDate,
    'memberId': memberId
  };

  toString() =>
      "FormSurvey => " + toJson().toString();
}
