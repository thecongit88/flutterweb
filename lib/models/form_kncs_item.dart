import 'dart:convert';

import 'package:hmhapp/dp_members/models/plan.dart';

class FormKNCSItemResponse {
  List<FormKNCSItem> items = <FormKNCSItem>[];
  FormKNCSItemResponse.fromJson(List<dynamic> json) {
    if (json != null) {
      json.forEach((item) {
        var it = new FormKNCSItem.fromMap(item);
        items.add(it);
      });
    }
  }
}


class FormKNCSItem {
  int? id;
  double? scores;
  String? result;
  Map? formFormat;
  Plan? plan;


  FormKNCSItem();

  FormKNCSItem.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    formFormat = map['form_format'];
    scores = map['scores'];
    result = map['result'];
    plan = Plan.fromJson(map['plan']);
  }

//Convert Plan khi duoc get tu level2. Danh sach Item duoc get tu Form
  FormKNCSItem.fromMap(Map<String, dynamic> map) {
    if (map == null) {
      return;
    }
    id = map['id'];
    formFormat = map['form_format'];
    scores = map['scores']!= null? double.parse(map['scores'].toString()):0;
    result = map['result'];
    plan = new Plan()..id = map['plan'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'scores': scores,
    'form_format': json.encode(formFormat).toString(),
    'result': result,
    'plan': plan,
  };

  refectFormObjectName(String string) {
    if(string == null || string.isEmpty) return "";
    String _lastCharacter = string.length > 0 ? string.substring(string.length - 1) : "";
    if(_lastCharacter == "s") return string;
    return string+"s";
  }

  toString() =>
      "FormKNCSItem => " + toJson().toString();
}
