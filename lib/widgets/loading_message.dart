
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/utils/constants.dart';

class LoadingMessage extends StatelessWidget {
  final String loadingMessage;
  final String? size;

  const LoadingMessage({Key? key,required this.loadingMessage,this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    var showLoading = (size == Constants.LOADING_SMALL || size == Constants.LOADING_NOMESSAGE ? false:true);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          showLoading?
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: theme.primaryColor,
              fontSize: 18,
            ),
          ):const SizedBox(),
          showLoading?
          const SizedBox(height: 18):const SizedBox(),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(theme.primaryColor),
          ),
        ],
      ),
    );
  }
}