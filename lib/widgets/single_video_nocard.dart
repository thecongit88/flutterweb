import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/models/youtube.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';

class SingleVideoNoCard extends StatelessWidget {
  final YoutubeModel entry;
  final int maxLines;
  final bool showCategoryName;
  final bool showAuthor;
  final Function onTap;

  SingleVideoNoCard(
      {Key? key,
      required this.entry,
      required this.showCategoryName,
      this.maxLines = 3,
      this.showAuthor = false,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {

    final subTitle = "${StringUtils.capitalize(entry.viewCount ?? "0")}";

    return ListTile(
      leading:
      entry.thumbnail!.isNotEmpty?
      CoverImageDecoration(
        url: entry.thumbnail,
        width: 100,
        height: 80,
        borderRadius: 2.0,
      ):
          Image(
            width: 100,
            height: 80,
            image:
            Image.asset("assets/images/youtube_default.jpg", fit: BoxFit.fitWidth).image
          ),

      title: Text(
        entry.title ?? "",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        maxLines: maxLines,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Padding(
          padding: EdgeInsets.only(top: 5),
          child: Row(children: [
            Text(
              subTitle,
              maxLines: 1,
              style: TextStyle(fontSize: 12),
            )
          ])),
      onTap:  onTap(),
    );
  }
}
