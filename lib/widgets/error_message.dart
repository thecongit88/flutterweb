import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/common/app_constant.dart';

class ErrorMessage extends StatelessWidget {
  final String errorMessage;
  final Function? onRetryPressed;

  const ErrorMessage({Key? key,required this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          AppConstant.spaceVerticalMediumExtra,
          const Icon(Icons.error_outline,size: 50,color: Colors.red,),
          AppConstant.spaceVerticalSmallMedium,
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.red,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
