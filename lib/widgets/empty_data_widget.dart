import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../common/app_colors.dart';

class EmptyDataWidget extends StatelessWidget {
  final Function()? onReloadData;
  final String? msg;

  const EmptyDataWidget({Key? key, this.onReloadData, this.msg = 'Chưa có dữ liệu'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onReloadData,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        alignment: Alignment.center,
        child: Text(
          "$msg",
          style: TextStyle(color: AppColors.grey300, fontSize: 12.sp, height: 1.4),
          textAlign: TextAlign.center,
        ),
      )
    );
  }
}
