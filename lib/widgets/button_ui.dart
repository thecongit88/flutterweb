import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post_categories.dart';

class ButtonUI extends StatelessWidget {
  final bool isLast;
  final PostCategory? postCategory;
  final int? categoryId;
  final VoidCallback? onTap;

  ButtonUI(
      {
        this.isLast = false,
        this.postCategory,
      this.categoryId,
      this.onTap});

  @override
  Widget build(BuildContext ctx) {
    return Expanded(
      child:
      Container(
        margin: EdgeInsets.only(right: isLast?0: 15),
        decoration: BoxDecoration(
            color: postCategory!= null && postCategory!.id == categoryId
                ? MemberAppTheme.nearlyBlue
                : MemberAppTheme.nearlyWhite,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            border: Border.all(color: MemberAppTheme.nearlyBlue)),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.white24,
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            onTap: () {
              //state.setCategoryId(postCategory.id);
            },
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 12, bottom: 12, left: 18, right: 18),
              child: Center(
                child: Text(
                  postCategory != null? postCategory!.name! :"",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 12,
                    letterSpacing: 0.27,
                    color: postCategory != null && postCategory!.id == categoryId
                        ? MemberAppTheme.nearlyWhite
                        : MemberAppTheme.nearlyBlue,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
