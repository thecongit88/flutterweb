/*import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../common/app_colors.dart';

class ButtonWidget extends StatelessWidget {
  final Function()? onPress;
  final String? title;
  final Color? color;

  final TextStyle? titleStyle;
  final double? height;
  final double? radius;

  const ButtonWidget({Key? key, this.onPress, this.title,this.color,this.titleStyle,this.height,this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height != null ? height: 44.sp,
      child: TextButton(
          onPressed: onPress,
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(6.sp)
                )
            ),
            backgroundColor: MaterialStateProperty.all<Color>(color ?? AppColors.blue),
          ),
          child: Text(
            '$title',
            style: TextStyle(
              color: Colors.white, fontSize: 12.sp
            )
          )),
    );
  }
}*/

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../common/app_colors.dart';

class ButtonWidget extends StatelessWidget {
  final Function()? onPress;
  final String? title;
  final Color? bgColor;
  final IconData? ic;
  final double? height;

  const ButtonWidget({Key? key, this.onPress, this.title, this.bgColor, this.ic, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height ?? 48.sp,
      child: TextButton(
          onPressed: onPress,
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.sp)
                )
            ),
            backgroundColor: MaterialStateProperty.all<Color>(bgColor ?? AppColors.blue),
          ),
          child:
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ic != null ? Container(
                height: height ?? 48.sp,
                margin: const EdgeInsets.only(right: 8),
                child: Icon(ic, size: 20.sp, color: Colors.white,),
              ) : const SizedBox(height: 0,),
              Text('$title', style: const TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.center,)
            ],
          )
      ),
    );
  }
}

