import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/dp_managers/manager_app_theme.dart';

import '../common/app_colors.dart';

//Tham số child là nội dung bên trong BottomSheet
Future openBottomSheet(BuildContext context, {Widget? child, String? title,Color? color}) {
  return showModalBottomSheet(
    context: context,
      builder: (BuildContext context) {
        return Stack(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Container(
                constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.9),
                padding: EdgeInsets.only(
                    top: title != null ? 70 : 15.0,
                    bottom: 10.0),
                margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.04),
                child: SingleChildScrollView(
                  child: Wrap(
                    children: [
                      child ?? const SizedBox(height: 0,)
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            title != null ?
            _bottomHeader(context, title,color) : const SizedBox(height: 0,),
          ],
        );
      },
      backgroundColor: Colors.white,
      isScrollControlled: true,
      isDismissible: false,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      enableDrag: true,
  );
}

Widget _bottomHeader(BuildContext context, String title,Color? color) {
  return Container(
      decoration: BoxDecoration(
        color: color ?? ManagerAppTheme.nearlyBlue,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            padding: const EdgeInsets.all(15),
            height: 50.0,
            child: Text(title,
              style: TextStyle(fontSize: 16.sp, color: Colors.white, fontWeight: FontWeight.w600),
              maxLines: 2,
            ),
          ),
          Positioned(
              right: MediaQuery.of(context).size.width * 0.01,
              top: 0,
              child: IconButton(
                icon: const Icon(
                  Icons.close,
                  size: 20.0,
                  color: Colors.white,
                ),
                onPressed: () => Navigator.of(context).pop()
              )),
        ],
      ),
    );
}