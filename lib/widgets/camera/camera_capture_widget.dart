import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/models/files/file_upload.dart';
import 'package:provider/provider.dart';

import '../../common/app_colors.dart';
import '../../common/app_constant.dart';
import '../../common_screens/camera/camera_screen.dart';
import '../../services/api_response.dart';
import '../../common_screens/camera/camera_state.dart';
import '../../utils/images_util.dart';

class CameraCaptureWidget extends StatelessWidget{
  final CameraState controllerCamera;
  const CameraCaptureWidget({
    Key? key,
    required this.controllerCamera
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return  MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => controllerCamera),
    ],
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Chụp ảnh thực tế (có thể chụp tối đa 3 ảnh)",
          style: AppColors.catBoxLabel.copyWith(fontSize: 13.sp,
              fontWeight: FontWeight.w400,
              color: AppColors.grey),),
        AppConstant.spaceVerticalSmallExtra,
        Row(
          children: [
            Consumer<CameraState>(
              builder: (context, state, child) {
                if(state.listResponse.status == Status.COMPLETED){
                  var files = state.listResponse.data ?? [];
                  return files.isNotEmpty ?
                  SizedBox(
                    height: 80.sp,
                    child:
                    ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: files.length,
                      itemBuilder: (context, index) {
                        var item = files[index];
                        return Container(
                            width: 80.sp,
                            height: 80.sp,
                            margin: EdgeInsets.only(right: 10.sp),
                            decoration: BoxDecoration(
                              color: const Color(0xFFF9FAFB),
                              border: Border.all(
                                  width: 1.0,
                                  color: const Color(0xFFF9FAFB),
                                  style: BorderStyle.solid
                              ),
                              borderRadius: const BorderRadius.all(
                                  Radius.circular(12)
                              ),
                            ),
                            child:
                            Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child:Image.file(File(item.path!),fit: BoxFit.cover,width: 80,)),
                                Positioned(top: 5.sp,right: 2.sp,child:
                                InkWell(
                                  onTap: (){
                                    controllerCamera.removeFile(index);
                                  },
                                  child:Container(
                                      decoration: const BoxDecoration(
                                        color: Color(0xFFF9FAFB),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16)
                                        ),
                                      ),
                                      padding: const EdgeInsets.all(2),
                                      child:Icon(Icons.close,color: Colors.red,size: 20.sp,)
                                  ),),),
                              ],
                            ));
                      },
                    ),
                  ) :
                  const SizedBox.shrink();
                }else{
                  return const SizedBox.shrink();
                }
              },
            ),
            Consumer<CameraState>(
            builder: (context, state, child) {
              if(state.listResponse.status == Status.COMPLETED){
                var files = state.listResponse.data ?? [];
                return files.length >= 3?
                const SizedBox.shrink():
                initAddCapture(context,theme,controllerCamera);
              }else if(state.listResponse.status == Status.INIT){
               return initAddCapture(context,theme,controllerCamera);
              }else{
                return const SizedBox.shrink();
              }
            }
            )

    ],
        )
      ],
    ));
  }

  initAddCapture(context,theme,controllerCamera){
    return
    InkWell(
      child: SizedBox(
        width: 80.sp,
        height: 80.sp,
        child:
        Container(
            decoration: BoxDecoration(
              color: const Color(0xFFF9FAFB),
              border: Border.all(
                  width: 1.0,
                  color: theme.primaryColor,
                  style: BorderStyle.solid
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(12.r)
              ),
            ),
            child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.camera_alt_rounded, color: theme.primaryColor,
                  size: 32.sp,),
                AppConstant.spaceVerticalSmall,
                Text("Chụp ảnh", style: AppColors.catBoxLabel,)
              ],
            )
        ),
      ),
      onTap: () async {
        var result = await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const CameraScreen(),
        ));

        if (result != null) {
          String imagePathCapture = result["filePath"];
          if (imagePathCapture.isNotEmpty) {
            if (imagePathCapture != "") {
              var file = FileUploadSimple(
                  name: ImageUtil.getImageNameFromLocalPath(imagePathCapture),
                  path: imagePathCapture);
              controllerCamera.addFile(file);
            }
          }
        }
      },
    );
  }

}
