import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/models/files/file_upload.dart';
import 'package:hmhapp/models/files/images.dart';
import 'package:provider/provider.dart';

import '../../common/app_colors.dart';
import '../../common/app_constant.dart';
import '../../common_screens/camera/camera_screen.dart';
import '../../services/api_response.dart';
import '../../common_screens/camera/camera_state.dart';
import '../../utils/images_util.dart';

class CameraCaptureSingleWidget extends StatelessWidget{
  final CameraState controllerCamera;
  const CameraCaptureSingleWidget({
    Key? key,
    required this.controllerCamera
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return  MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => controllerCamera),
    ],
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Chụp ảnh thực tế",
          style: AppColors.catBoxLabel.copyWith(fontSize: 13.sp,
              fontWeight: FontWeight.w400,
              color: AppColors.grey),),
        AppConstant.spaceVerticalSmallExtra,
        InkWell(
          child: Stack(
            children: <Widget>[
              Consumer<CameraState>(
                  builder: (context, state, child) {
                    var data = state.listResponse;
                    switch (data.status) {
                      case Status.COMPLETED:
                        var img = data.data![0];
                        return buidImageCapture(img,theme);
                      case Status.ERROR:
                        return initCapture(context,theme);
                      case Status.INIT:
                        return initCapture(context,theme);
                      default:
                        return initCapture(context,theme);
                    }
                  }),
            ],
          ),
          onTap: () async {
            var result = await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const CameraScreen(),
            ));

            if (result != null) {
              String imagePathCapture = result["filePath"];
              if (imagePathCapture.isNotEmpty) {
                if (imagePathCapture != "") {
                  var file = FileUploadSimple(
                      name: ImageUtil.getImageNameFromLocalPath(imagePathCapture),
                      path: imagePathCapture);
                  controllerCamera.addFile(file);
                }
              }
            }

          },
        ),
      ],
    ));
  }

  initCapture(context,theme){
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      width: double.infinity,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: theme.primaryColor,width: 1),
            color: Colors.grey.shade100
        ),
        height: 230.0,
        width: double.infinity,
        child:
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.camera_alt_rounded, color: theme.primaryColor,
              size: 50.sp,),
            AppConstant.spaceVerticalSmallExtra,
            const Text("Nhấn để chụp ảnh")
          ],
        ),
      ),
    );
  }

  buidImageCapture(FileUploadSimple img,theme){
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: theme.primaryColor,width: 1)
      ),
      height: 230.0,
      width: double.infinity,
      child: img.path != null && img.path != "" ? Center(child: Image.file(File(img.path!))) :
      const SizedBox(),
    );
  }

}
