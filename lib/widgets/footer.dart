import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';

class Footer extends StatefulWidget {
  @override
  _FooterState createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
            color: HexColor('#F8FAFB'),
            borderRadius: const BorderRadius.all(
                Radius.circular(16.0)),
          ),
            width: double.infinity,
            child: Container(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child:
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 60,
                            width: 170,
                            child: Image.asset('assets/icons/logoPHAD.png'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child:
                  Padding(
                    padding: const EdgeInsets.only(top: 18, left: 4),
                    child: Text(
                      'Ứng dụng được phát triển bởi PHAD',
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: MemberAppTheme.nearlyBlack,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  ),
                  Center(
                    child:
                    Padding(
                      padding: const EdgeInsets.only(top: 8, left: 4),
                      child: Text(
                        'Địa chỉ: Số 18, ngõ 132, Hoa Bằng, Cầu Giấy, Hà Nội',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: MemberAppTheme.nearlyBlack,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                Center(
                  child:
                  Padding(
                    padding: const EdgeInsets.only(top: 8, left: 4),
                    child: Text(
                      'Điện thoại: (+84) 2473 000 988',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: MemberAppTheme.nearlyBlack,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
                  Center(
                    child:
                    Padding(
                      padding: const EdgeInsets.only(top: 18, left: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 32,
                          width: 32,
                          child: Image.asset('assets/icons/facebook.png'),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          height: 32,
                          width: 32,
                          child: Image.asset('assets/icons/twitter.png'),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          height: 32,
                          width: 32,
                          child: Image.asset('assets/icons/youtube.png'),
                        ),
                      ],
                    ),
                    ),
                  ),
                SizedBox(height:90,)
                ],
              ),
            ),
          ),
        ],
      );
  }
}