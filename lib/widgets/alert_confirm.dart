import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../common/app_colors.dart';
import '../common/app_constant.dart';

/*
void confirmDelete(BuildContext context, LinhVuc? item) {
  final AdminManageLinhVucController controller = Get.find();
  controller.maXacNhan.text = "";
  Get.dialog(
      AlertConfirmWidget(
        middleName: item?.ten ?? "",
        ctlController: controller.maXacNhan,
        phaiXacNhan: true,
        onPressOk: () async {
          await controller.deleteLinhVuc(item?.iD ?? 0);
        },
      )
  );
}
* */
class AlertConfirmWidget extends StatelessWidget {
  final Function()? onPressOk;
  final Function()? onPressCanel;
  final String? prefixName;
  final String middleName;
  final String? suffixName;
  final String? titleCancel;
  final String? titleOk;

  const AlertConfirmWidget({
    Key? key,
    this.onPressOk,
    this.onPressCanel,
    this.prefixName,
    required this.middleName,
    this.suffixName,
    this.titleCancel,
    this.titleOk,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      contentPadding: EdgeInsets.zero,
      title: Icon(Icons.alarm, color: AppColors.green, size: 50.sp,),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.sp))),
      content: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(15.sp),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                          style: const TextStyle(color: Colors.black87, height: 1.5),
                          children: [
                            TextSpan(
                              text: prefixName ?? "Bạn có muốn xóa: ",
                            ),
                            TextSpan(
                              text: middleName,
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                              text: suffixName ?? " không?",
                            )
                          ])
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(color: AppColors.border, width: 0.5),
                        right: BorderSide(color: AppColors.border, width: 0.5),
                      ),
                    ),
                    child: TextButton(
                      child: Text("Bỏ qua", style: TextStyle(fontWeight: FontWeight.w600, color: AppColors.grey300, fontSize: 13.sp),),
                      onPressed: () => onPressCanel?.call() ?? Navigator.of(context).pop()
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(color: AppColors.border, width: 0.5),
                        //left: BorderSide(color: AppColors.grey, width: 0.3),
                      ),
                    ),
                    child: TextButton(
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {
                          formKey.currentState!.save();
                          onPressOk!.call();
                        }
                      },
                      child: Text('Xác nhận', style: TextStyle(fontWeight: FontWeight.w600, color: AppColors.red, fontSize: 13.sp)),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    return alert;
  }
}
