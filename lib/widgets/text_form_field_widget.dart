import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../common/app_colors.dart';
import '../common/app_constant.dart';
import '../common/text_theme_app.dart';
import '../common/utils.dart';

class TextFormFieldWidget extends StatelessWidget {
  const TextFormFieldWidget({
    Key? key,
    required this.onChange,
    this.textEditingController,
    this.label,
    this.suffixIcon,
    this.isValidator = true,
    this.type,
  }) : super(key: key);

  final Function(String)? onChange;
  final TextEditingController? textEditingController;
  final String? label;
  final String? type;
  final Widget? suffixIcon;
  final bool isValidator;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        label != null ? Align(
          alignment: Alignment.centerLeft,
          child: TextThemeApp.wdgTitle("$label", isRequired: isValidator)
        ) : const SizedBox(height: 0),
        const SizedBox(height: 8),
        TextFormField(
          style: Theme.of(context).textTheme.caption?.copyWith(color: Colors.black, fontSize: 14.sp),
          controller: textEditingController ?? TextEditingController(),
          validator: (value) {
            if (isValidator == true && (value == null || value.isEmpty)) {
              return "Thông tin bắt buộc.";
            }
            if(type == AppConstant.emailType) {
              return isValidator == true ? validateEmail(value ?? "") : null;
            }
            else if(type == AppConstant.phoneType) return validateMobile(value ?? "");
            return null;
          },
          onChanged: onChange,
          decoration: InputDecoration(
              fillColor: AppColors.blue100,
              filled: false,
              /*enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.r)),
                  borderSide: const BorderSide(
                    color: Colors.grey,
                  )
              ),
              enabledBorder: const UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
              ),*/
              contentPadding: const EdgeInsets.symmetric(
                //horizontal: 15,
                vertical: 15
              ),
              hintText: "Chạm để nhập",
              hintStyle: TextStyle(fontSize: 12.sp),
              //prefixIcon: prefixIcon,
              suffixIcon: suffixIcon
          ),
        )
      ],
    );
  }
}
