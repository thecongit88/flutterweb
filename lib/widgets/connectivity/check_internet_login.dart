import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class InternetErrorLogin extends StatelessWidget {
  final Widget endableWidget;
  final Widget disableWidget;

  const InternetErrorLogin({Key? key,required this.endableWidget,required this.disableWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConnectivityWidget(
      showOfflineBanner: false,
      builder: (context, isOnline) => Container(
          child: isOnline ? endableWidget : disableWidget
    )
    );
  }
}
