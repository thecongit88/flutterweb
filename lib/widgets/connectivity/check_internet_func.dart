import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class InternetErrorFunc extends StatelessWidget {
  final Widget myWidget;

  const InternetErrorFunc({Key? key,required this.myWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConnectivityWidget(
      showOfflineBanner: false,
      builder: (context, isOnline) => Container(
          child: isOnline ? myWidget : const SizedBox(height: 0,)
      ),
    );
  }
}
