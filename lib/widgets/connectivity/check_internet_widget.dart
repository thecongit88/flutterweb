import 'package:connectivity_widget/connectivity_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../app_theme.dart';

class InternetErrorWidget extends StatelessWidget {
  final Widget myWidget;

  const InternetErrorWidget({Key? key,required this.myWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return ConnectivityWidget(
      showOfflineBanner: false,
      builder: (context, isOnline) => Container(
          child: isOnline ? myWidget :
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.wifi, color: isOnline ? Colors.green : Colors.red, size: 80,),
                const SizedBox(height: 7,),
                Text(
                  isOnline ? 'Đã kết nối' : 'Thiết bị không kết nối mạng. Vui lòng thử lại!',
                  style: TextStyle(color: isOnline ? Colors.green : Colors.red),
                )
              ],
            ),
          )
      ),
    );
  }
}
