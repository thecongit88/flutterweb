import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hmhapp/services/enpoint_url.dart';

import '../common/app_colors.dart';
import '../common/app_constant.dart';
import '../common_screens/image_player_screen.dart';
import '../models/files/images.dart';

class ImageDisplayListWidget extends StatelessWidget{
  const ImageDisplayListWidget({
    Key? key,
    required this.files,
    this.isHideTitle
  }) : super(key: key);

  final List<Images> files;
  final bool? isHideTitle;

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return
      files.isNotEmpty?
      Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        isHideTitle == true? const SizedBox.shrink():
        Text("Hình ảnh",
          style: AppColors.catBoxLabel.copyWith(fontSize: 13.sp,
              fontWeight: FontWeight.w400,
              color: theme.primaryColor),),
        AppConstant.spaceVerticalSmallExtra,
        Row(
          children: [
            SizedBox(
              height: 60.sp,
              child:
              ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: files.length,
                itemBuilder: (context, index) {
                  var item = files[index];
                  return Container(
                      width: 60.sp,
                      height: 60.sp,
                      margin: EdgeInsets.only(right: 10.sp),
                      decoration: BoxDecoration(
                        color: const Color(0xFFF9FAFB),
                        border: Border.all(
                            width: 1.0,
                            color: const Color(0xFFF9FAFB),
                            style: BorderStyle.solid
                        ),
                        borderRadius: const BorderRadius.all(
                            Radius.circular(12)
                        ),
                      ),
                      child:
                          InkWell(
                            child:
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child:
                            Image.network("${EnpointUrl.URL_CLOUD}${item.formats != null ? item!.formats!.thumbnail!.url! : ""}",
                                fit: BoxFit.cover),
                          ),
                            onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ImagePlayerScreen(
                                    imgLink: "${EnpointUrl.URL_CLOUD}${item.formats != null ? item!.formats!.large!.url! : ""}"
                                  ),
                                ));

                            },
                          )
                  );
                },
              ),
            )
          ],
        )
      ],
    ):const SizedBox.shrink();
  }

}
