import 'package:flutter/material.dart';
import 'package:basic_utils/basic_utils.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';
import 'package:intl/intl.dart';

class SinglePostCard extends StatelessWidget {
  final Post entry;
  final bool showCategoryName;
  final bool showAuthor;
  final Function onTap;


  const SinglePostCard(
      {Key? key,required this.entry, this.showCategoryName = false, this.showAuthor = false,required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    var dFormat = DateFormat('dd/MM/yyyy');
    final subTitle = dFormat.format(DateTime.parse(entry.created_at ?? "")).toString();

    return Column(
      children: [
        _header(ctx, "categoryName"),
        ListTile(
          title: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                entry.title ?? "",
                style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 18),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              )),
          subtitle: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 0),
              child: Row(children: [
                Text(subTitle, maxLines: 1,style: const TextStyle(fontSize: 15),)
              ])),
          onTap: (){
            onTap();
          }
        ),
      ],
    );
  }

  Widget _header(BuildContext ctx, String categoryName) {
    return Stack(
      children: [
        CoverImageDecoration(
            url: entry.thumbnail,
            height: 220.0,
            onTap: (){
              onTap();
            }),
      ],
    );
  }

}
