import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/posts/post_detail_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/widgets/posts/single_post_nocard.dart';
import 'package:provider/provider.dart';

class PostRelated extends StatefulWidget {
  final int categoryId;
  final int currentId;
  const PostRelated(this.categoryId,this.currentId, {Key? key}) : super(key: key);

  @override
  PostRelatedState createState() => PostRelatedState();
}

class PostRelatedState extends State<PostRelated> {
  int _categoryId = 0;
  int _currentId = 0;
  final PostDetailState _postDetailState = PostDetailState();

  @override
  void initState() {
    super.initState();
    _categoryId = widget.categoryId;
    _currentId = widget.currentId;
    _postDetailState.getRelatedPosts(_categoryId,_currentId);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => _postDetailState,
      child: Consumer<PostDetailState>(builder: (context, state, child) {
        return buildRelated(state);
      })
    );
  }

  Widget buildRelated(PostDetailState state){
    switch (state.relatedPostResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            const Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        return
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Padding(
                padding: EdgeInsets.only(top: 10, left: 15),
                child: Text("Tin bài liên quan",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start)),
            Padding(
              padding: const EdgeInsets.all(0),
              child: ListView.separated(
                  separatorBuilder: (context, index) => const Divider(
                  ),
                  padding:
                  const EdgeInsets.only(top: 20, right: 10, bottom: 20, left: 0),
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: state.relatedPosts.length,
                  itemBuilder: (BuildContext context, int index) {
                    Post entry = state.relatedPosts[index];
                    return SinglePostNoCard(
                        key: ValueKey(entry.id),
                        entry: entry,
                        maxLines: 2,
                        showCategoryName: false,
                        onTap: () {
                          return Navigator.of(context).popAndPushNamed("/post", arguments: entry);
                        });
                  }),
            )
          ]);
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          Flushbar(
            message: state.relatedPostResponse.message,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return const SizedBox();
      case Status.INIT:
        return const SizedBox();
      default:
        return const SizedBox();
    }

  }

}
