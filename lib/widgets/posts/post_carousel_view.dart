import 'package:flutter/material.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/common_screens/posts/post_detail_screen.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/dp_members/models/post.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:intl/intl.dart';

class PostCarouselView extends StatefulWidget {
  final List<Post> posts;
  const PostCarouselView({Key? key,required this.posts,required this.callBack}) : super(key: key);

  final Function callBack;
  @override
  _PostCarouselViewState createState() => _PostCarouselViewState();
}

class _PostCarouselViewState extends State<PostCarouselView>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16, bottom: 16),
      child: Container(
        width: double.infinity,
        height: 143,
        child:
        widget.posts.length==0?
            SizedBox():
        ListView.builder(
          shrinkWrap: true,
          padding: const EdgeInsets.only(right: 16, left: 16),
          itemCount: widget.posts.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            final int count = widget.posts.length;
            final Animation<double> animation =
            Tween<double>(begin: 0.0, end: 1.0).animate(
                CurvedAnimation(
                    parent: animationController,
                    curve: Interval((1 / count) * index, 1.0,
                        curve: Curves.fastOutSlowIn)));
            animationController.forward();

            return PostView(
              post: widget.posts[index],
              animation: animation,
              animationController: animationController,
              callback: () {
                widget.callBack();
              },
            );
          },
        ),
      ),
    );
  }
}

class PostView extends StatelessWidget {

  const PostView(
      {Key? key,
      required this.post,
      required this.animationController,
      required this.animation,
      required this.callback})
      : super(key: key);

  final VoidCallback callback;
  final Post post;
  final AnimationController animationController;
  final Animation<double> animation;


  @override
  Widget build(BuildContext context) {
    var dFormat = DateFormat('dd/MM/yyyy');

    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                100 * (1.0 - animation.value), 0.0, 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                 Navigator.of(context).pushNamed("/post", arguments: post);
              },
              child: SizedBox(
                width: 280,
                child: Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 20),
                      child: Row(
                        children: <Widget>[
                          const SizedBox(
                            width: 48,
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: HexColor('#F8FAFB'),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(16.0)),
                              ),
                              child: Row(
                                children: <Widget>[
                                  const SizedBox(
                                    width: 48 + 20.0,
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                            const EdgeInsets.only(top: 16, right: 10),
                                            child: Text(
                                              post.title ?? "",
                                              maxLines: 4,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16,
                                                letterSpacing: 0.27,
                                                color: Colors.black87,
                                              ),
                                            ),
                                          ),
                                          const Expanded(
                                            child: SizedBox(),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 16, bottom: 20),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceBetween,
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  dFormat.format(DateTime.parse(post.created_at ?? "")).toString(),
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w200,
                                                    fontSize: 12,
                                                    letterSpacing: 0.27,
                                                    color: MemberAppTheme
                                                        .grey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20, bottom: 20, left: 0),
                        child: Row(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(16.0)),
                              child: AspectRatio(
                                  aspectRatio: 1.0,
                                  child: post.thumbnail!.isEmpty?
                                  Image.asset( Constants.VIDEO_CATEGORY_IMAGE_DEFAULT,fit: BoxFit.cover):
                                  Image.network(post!.thumbnail!,fit: BoxFit.cover)),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
