import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hmhapp/app_theme.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/posts/post_category_state.dart';
import 'package:hmhapp/states/posts/post_detail_state.dart';
import 'package:hmhapp/states/posts/post_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/widgets/button_ui.dart';
import 'package:hmhapp/widgets/posts/single_post_nocard.dart';
import 'package:provider/provider.dart';

class PostCategoryList extends StatefulWidget {
  final PostState state;
  PostCategoryList(this.state);

  @override
  _PostCategoryListState createState() => _PostCategoryListState();
}

class _PostCategoryListState extends State<PostCategoryList> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PostState>(builder: (context, state, child) {
      return buildCategory(state);
    });
  }

  Widget buildCategory(PostState state) {

    final ThemeData theme = Theme.of(context);

    switch (state.postCategoryResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            child:
            Center(
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        var categories = state.postCategoryResponse.data;
        return Container(
          padding: EdgeInsets.only(left: 0,right: 0, top: 10, bottom: 10),
            height: 60,
            child:
            ListView.builder(
              padding: const EdgeInsets.only(
                  top: 0, bottom: 0, right: 16, left: 0),
              itemCount: categories!.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                var cate = categories[index];
                var isSelected = cate.id == state.categoryId;
                return Container(
                  margin: EdgeInsets.only(right: 5,left: 5),
                  decoration: BoxDecoration(
                      color: isSelected
                          ? theme.primaryColor
                          : Colors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                      border: Border.all(color: theme.primaryColor)),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      splashColor: Colors.white24,
                      borderRadius:
                      const BorderRadius.all(Radius.circular(24.0)),
                      onTap: () {
                        state.setCategoryId(cate!.id!);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 8, bottom: 8, left: 15, right: 15),
                        child: Center(
                          child: Text(
                            cate.name ?? "",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: isSelected
                                  ? Colors.white
                                  : theme.primaryColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            )
        );
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          Flushbar(
            message: state.postCategoryResponse.message,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return SizedBox();
        break;
      case Status.INIT:
        return SizedBox();
        break;
      default:
        return SizedBox();
        break;
    }
  }

}
