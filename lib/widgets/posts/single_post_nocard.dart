import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/utils/constants.dart';
import 'package:hmhapp/widgets/cover_image_decoration.dart';
import 'package:intl/intl.dart';

class SinglePostNoCard extends StatelessWidget {
  final Post entry;
  final int maxLines;
  final bool showCategoryName;
  final bool showAuthor;
  final Function onTap;

  const SinglePostNoCard(
      {Key? key,
      required this.entry,
      this.showCategoryName = false,
      this.maxLines = 3,
      this.showAuthor = false,
       required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {

    var dFormat = DateFormat('dd/MM/yyyy');
    final subTitle = dFormat.format(DateTime.parse(entry.created_at ?? "")).toString();

    return ListTile(
      leading:
      entry.thumbnail!.isNotEmpty?
      CoverImageDecoration(
        url: entry.thumbnail,
        width: 100,
        height: 80,
        borderRadius: 2.0,
      ):
          Image(
            width: 100,
            height: 80,
            image:
            AssetImage(Constants.VIDEO_CATEGORY_IMAGE_DEFAULT),
          ),

      title: Text(
        entry.title ?? "",
        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        maxLines: maxLines,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Row(children: [
            Text(
              subTitle,
              maxLines: 1,
              style: const TextStyle(fontSize: 12),
            )
          ])),
      onTap:  (){
        onTap();
      },
    );
  }
}
