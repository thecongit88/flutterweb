import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/posts/post_state.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:hmhapp/widgets/posts/single_post_nocard.dart';
import 'package:provider/provider.dart';

class PostSearchDelegate extends SearchDelegate {

  PostSearchDelegate();

  @override
  String get searchFieldLabel => "Nhập tìm kiếm";

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme.copyWith(
        inputDecorationTheme: InputDecorationTheme(
            hintStyle: TextStyle(fontSize: 18,color: Colors.white)),
        primaryColor: theme.primaryColor,
        primaryIconTheme: theme.primaryIconTheme.copyWith(color: Colors.white),
        primaryColorBrightness: theme.primaryColorBrightness,
        primaryTextTheme: theme.primaryTextTheme);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );

  }

  @override
  Widget buildResults(BuildContext context) {

    if (query.length < 3) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              "Từ khóa cần nhập 3 kí tự trở lên.",
            ),
          )
        ],
      );
    }

    final PostState state = PostState();
    state.search(query,1);
    return ChangeNotifierProvider(
        create: (_) => state,
        child:
        Consumer<PostState>(
            builder: (context, state, child) {
             return bindList(state,context);
            })
    );
  }


  Widget bindList(PostState state,BuildContext context){

    switch (state.postSearchResponse.status) {
      case Status.LOADING:
        return
          Container(
            color: Colors.white30,
            height: 500,
            child:
            Center(
              child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Colors.green)),
            ),
          );
        break;
      case Status.COMPLETED:
        return getList(state.postSearch,context);
        break;
      case Status.ERROR:
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          Flushbar(
            message: state.postSearchResponse.message,
            backgroundColor: Colors.green,
            duration: Duration(seconds: 2),
            flushbarPosition: FlushbarPosition.TOP,
          ).show(context);
        });
        return SizedBox();
        break;
      case Status.INIT:
        return SizedBox();
        break;
      default:
        return SizedBox();
        break;
    }
  }

  Widget getList(List<Post> posts,BuildContext context) {
    return
      posts.length==0?
      Container(
          alignment: Alignment.center,
          height: MediaQuery.of(context).size.height * 0.5,
          child:
          Text("Không tìm thấy bài viết nào.")
      ) :

      ListView.separated(
          separatorBuilder: (context, index) => Divider(
            color: Colors.grey,
          ),
          shrinkWrap: true,
          physics:  NeverScrollableScrollPhysics(),
          itemCount: posts.length,
          itemBuilder: (context, pos) {
            Post entry = posts[pos];
            return Padding(
                padding:
                new EdgeInsets.symmetric(vertical: 0),
                child: SinglePostNoCard(
                  key: ValueKey(entry.id),
                  entry: entry,
                  showCategoryName: false,
                  showAuthor: true,
                  onTap: () {
                    return Navigator.of(context)
                        .pushNamed("/post",
                        arguments: entry);
                  },
                ));
          });;
  }


  @override
  Widget buildSuggestions(BuildContext context) {

    // This method is called everytime the search term changes.
    // If you want to add search suggestions as the user enters their search term, this is the place to do that.
    return Column();
  }
}