import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../app_theme.dart';

class MonthYearDiaglogPage extends StatefulWidget {
  DateTime? mySelectedDate;
  MonthYearDiaglogPage({
    this.mySelectedDate
  });

  @override
  _MonthYearDiaglogPage createState() => _MonthYearDiaglogPage();
}
class _MonthYearDiaglogPage extends State<MonthYearDiaglogPage> {
  PageController pageController = PageController(initialPage: DateTime.now().year);
  DateTime? selectedDate;
  int? displayedYear;

  @override
  void initState() {
    super.initState();
    if(widget.mySelectedDate != null) {
      selectedDate = DateTime(widget.mySelectedDate!.year, widget.mySelectedDate!.month);
    } else {
      selectedDate = DateTime(DateTime.now().year, DateTime.now().month);
    }
    displayedYear = selectedDate?.year;
  }

  @override
  Widget build(BuildContext context) {
    //get current year if you selected before
    pageController = PageController(initialPage: displayedYear!);

    return Scaffold(
      appBar: AppBar(
        title: Text('Lọc dữ liệu'),
        backgroundColor: AppTheme.nearlyBlack,
      ),
      body: yearMonthPicker(),
    );
  }

  yearMonthPicker() => Column(
    mainAxisSize: MainAxisSize.min,
    children: <Widget>[
      Builder(builder: (context) {
        if (MediaQuery.of(context).orientation == Orientation.portrait) {
          return IntrinsicWidth(
            child: Column(children: [
              buildHeader(),
              Material(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [buildPager()],
                ),
              )
            ]),
          );
        }
        return IntrinsicHeight(
          child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                buildHeader(),
                Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [buildPager()],
                  ),
                )
              ]),
        );
      }),
    ],
  );
  buildHeader() {
    return Material(
      color: Color(0xff0D5CAB),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Text('Bạn đã chọn: ${DateFormat.yM().format(selectedDate)}', style: TextStyle(color: Colors.white, fontSize: 16.0)),
            //SizedBox(height: 20.0,),
            //Divider(color: Colors.white,),
            //SizedBox(height: 5.0,),
            SizedBox(
              height: 20.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Năm ${DateFormat.y().format(DateTime(displayedYear!))}',
                      style: TextStyle(color: Colors.white, fontSize: 20.0)),
                  Row(
                    children: <Widget>[
                      IconButton(
                        padding: EdgeInsets.only(top: 0),
                        icon: Icon(Icons.arrow_back, color: Colors.white),
                        onPressed: () => pageController.animateToPage(
                            (displayedYear ?? 0) - 1,
                            duration: Duration(milliseconds: 400),
                            curve: Curves.easeInOut),
                      ),
                      IconButton(
                        padding: EdgeInsets.only(top: 0),
                        icon:
                        Icon(Icons.arrow_forward, color: Colors.white),
                        onPressed: () => pageController.animateToPage(
                            (displayedYear ?? 0) + 1,
                            duration: Duration(milliseconds: 400),
                            curve: Curves.easeInOut),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  buildPager() => Container(
    color: Colors.white,
    height: 250.0,
    width: 500.0,
    child: Theme(
        data: Theme.of(context).copyWith(
            buttonTheme: ButtonThemeData(
                padding: EdgeInsets.all(0.0),
                shape: CircleBorder(),
                minWidth: 1.0)),
        child: PageView.builder(
          controller: pageController,
          scrollDirection: Axis.vertical,
          onPageChanged: (index) {
            setState(() {
              displayedYear = index;
            });
          },
          itemBuilder: (context, year) {
            /* pageController.animateToPage(
                displayedYear,
                duration: Duration(milliseconds: 400),
                curve: Curves.easeInOut);
             */

            return GridView.count(
              padding: EdgeInsets.all(12.0),
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 6,
              children: List<int>.generate(12, (i) => i + 1)
                  .map((month) => DateTime(year, month))
                  .map(
                    (date) => Padding(
                  padding: EdgeInsets.all(4.0),
                  child: ElevatedButton(
                    onPressed: () => setState(() {
                      selectedDate = DateTime(date.year, date.month);
                      //trả về màn hình trước đó cùng giá trị ngày đã chọn được
                      Navigator.of(context).pop(selectedDate);
                    }),
                    /*color: date.month == selectedDate?.month &&
                        date.year == selectedDate?.year
                        ? Colors.orange
                        : null,
                    textColor: date.month == selectedDate?.month &&
                        date.year == selectedDate?.year
                        ? Colors.white
                        : date.month == selectedDate?.month &&
                        date.year == selectedDate?.year
                        ? Colors.orange
                        : null,*/
                    child: Text(
                      DateFormat.M().format(date),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
              )
                  .toList(),
            );
          },
        )),
  );
}