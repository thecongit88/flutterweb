import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post_categories.dart';

/// Các item thông tin search đã chọn
class FilterItemUI extends StatelessWidget {
  final String label;
  final String? value;

  const FilterItemUI({Key? key, required this.label, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return
      value != null && value!.isNotEmpty && value != "0" ?
      Container(
          padding: const EdgeInsets.all(8),
          margin: const EdgeInsets.only(right: 4,bottom: 4,top: 4),
          decoration: BoxDecoration(
              color: theme.primaryColor,
              borderRadius: BorderRadius.circular(8)
          ),
          child:Text("$label: $value",style: const TextStyle(color: Colors.white),)):
    const SizedBox.shrink();
  }
}
