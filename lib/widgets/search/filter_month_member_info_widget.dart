import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/dp_members/design_course_app_theme.dart';
import 'package:hmhapp/models/post_categories.dart';

import 'filter_item_ui_widget.dart';
import 'filter_month_member_widget.dart';

class FilterMonthMemberInfoWidget extends StatelessWidget {
  final FilterMonthMemberWidgetModel? filter;
  final Function onTap;

  const FilterMonthMemberInfoWidget({Key? key, this.filter,required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return filter != null && filter?.isNotEmpty() ?
        Container(
        margin: const EdgeInsets.all(8),
        padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
        decoration: BoxDecoration(
            color: Colors.grey.withOpacity(.1),
            borderRadius: BorderRadius.circular(8)
        ),
        child:
        Row(
          children: [
            Expanded(child:
            Wrap(
              children: [
                FilterItemUI(label: "Năm",value: "${filter?.year}"),
                FilterItemUI(label: "Tháng",value: "${filter?.month}"),
                FilterItemUI(label: "NKT",value: filter?.memberName),
              ],
            )),
            SizedBox(
              width: 20,
              child: InkWell(
                child: const Icon(Icons.remove_circle_outline,color: Colors.red,),
                onTap: () async {
                  await onTap();
                },
              ),
            )
          ],
        )
    )
        : const SizedBox.shrink();
  }
}
