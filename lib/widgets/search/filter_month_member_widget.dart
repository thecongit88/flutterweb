
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:hmhapp/common/app_constant.dart';
import 'package:hmhapp/common/app_global.dart';
import 'package:hmhapp/dp_managers/models/member.dart';
import 'package:hmhapp/services/api_response.dart';
import 'package:hmhapp/states/member_state.dart';
import 'package:hmhapp/widgets/loading_message.dart';
import 'package:provider/provider.dart';

import '../../../common/app_colors.dart';
import '../../../common/text_theme_app.dart';
import '../../../utils/utils.dart';
import '../../../widgets/button_widget.dart';


/// Modal search
class FilterMonthMemberWidget extends StatefulWidget {
  final FilterMonthMemberWidgetModel? modelFiltered;
  const FilterMonthMemberWidget({Key? key, this.modelFiltered}) : super(key: key);

  @override
  FilterMonthMemberWidgetState createState() => FilterMonthMemberWidgetState();
}

class FilterMonthMemberWidgetState extends State<FilterMonthMemberWidget>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  final MemberState memberState = MemberState();
  DateTime? monthYearFilter;
  int? monthSelected, yearSelected, memberIdSelected;
  String? memberNameSelected;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
    monthSelected = widget.modelFiltered != null ? widget.modelFiltered?.month ?? 0 : 0;
    yearSelected = widget.modelFiltered != null  ? widget.modelFiltered?.year ?? 0 : 0;
    memberIdSelected = widget.modelFiltered != null  ? widget.modelFiltered?.memberId ?? 0 : 0;
    memberState.getListMembers();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final ThemeData theme = Theme.of(context);

    return
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => memberState),
          ],
          child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: [
                    TextThemeApp.wdgTitle("Chọn người khuyết tật", isRequired: false),
                    Consumer<MemberState>(
                        builder: (context, state, child) {
                          return wdgMembersList(state.memberResponseList, context)!;
                        }
                    ),
                    AppConstant.spaceVerticalMediumExtra,
                    Row(
                      children: [
                        Expanded(
                            child: Column(
                              children: [
                                TextThemeApp.wdgTitle("Chọn tháng", isRequired: false),
                                DropdownButtonFormField(
                                  isExpanded: true,
                                  isDense: true,
                                  value: monthSelected,
                                  items: getListThang(),
                                  onChanged: (value) {
                                    monthSelected = int.parse(value.toString());
                                  },
                                  hint: const Text('Chọn tháng'),
                                ),
                              ],
                            )
                        ),
                        Expanded(
                            child: Column(
                              children: [
                                TextThemeApp.wdgTitle("Chọn năm", isRequired: false),
                                DropdownButtonFormField(
                                  isExpanded: true,
                                  isDense: true,
                                  value: yearSelected,
                                  items: getListYear(),
                                  onChanged: (value) {
                                    yearSelected = int.parse(value.toString());
                                  },
                                  hint: const Text('Chọn năm'),
                                ),
                              ],
                            )
                        )
                      ],
                    ),
                    AppConstant.spaceVerticalMediumExtra,
                    ButtonWidget(
                      bgColor: theme.primaryColor,
                      ic: Icons.check,
                      title: "Áp dụng",
                      onPress: () {
                        var res = FilterMonthMemberWidgetModel(
                          memberId: memberIdSelected,
                          memberName:  memberNameSelected,
                          month:  monthSelected,
                          year: yearSelected
                        );
                        Navigator.pop(context, res);
                      },
                    ),
                  ],
                ),
              )
          )
      );
  }

  Widget? wdgMembersList(ApiResponse<List<Member>> data, BuildContext context) {
    final ThemeData theme = Theme.of(context);

    switch (data.status) {
      case Status.LOADING:
        return LoadingMessage(
          loadingMessage: data.message!,
        );
      case Status.COMPLETED:
        List<Member> list = data.data!;
        List<Member> lstMemberSelected = [];
        if(list.isNotEmpty && list.isNotEmpty && (memberIdSelected ?? 0) > 0) {
          lstMemberSelected = list.where((Member mb) => mb.id == memberIdSelected).toList();
        }
        return
          DropdownSearch<Member>(
            mode: Mode.BOTTOM_SHEET,
            popupTitle: Padding(
              padding: const EdgeInsets.all(16),
              child:
              Text(
                "Tìm kiếm người khuyết tật",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: theme.primaryColor
                ),
              ),
            ),
            maxHeight: 600,
            popupShape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(16),topRight: Radius.circular(16))),
            isFilteredOnline: true,
            selectedItem: lstMemberSelected.isNotEmpty && (memberIdSelected ?? 0) > 0 ? lstMemberSelected[0] : Member(),
            showSearchBox: true,
            items: list,
            onChanged: (Member? data) {
              memberIdSelected = data?.id ?? 0;
            },
            dropdownBuilder: _customDropDownInput,
            popupItemBuilder: _customPopupItemBuilder,
            dropdownSearchDecoration: const InputDecoration(
              contentPadding: EdgeInsets.zero
            ),
            searchFieldProps: TextFieldProps(
              padding: const EdgeInsets.only(left: 18,right: 18,bottom: 18),
              decoration: const InputDecoration(
                  hintText: "Nhập tìm kiếm",
                  hintStyle: TextStyle(color: Colors.grey),
                  prefixIcon: Icon(Icons.search,color: Colors.grey)
              )
            ),
          );
      case Status.ERROR:
        showErrorToast(context, "Có lỗi xảy ra khi nạp dữ liệu nkt");
        return const SizedBox(height: 0,);
      case Status.INIT:
        return const SizedBox(height: 0,);
    }
  }

  Widget _customDropDownInput(BuildContext context, Member? item) {
    if (item == null) {
      return Container();
    }
    String name = item.fullName ?? "";
    if(item.phone?.trim() != "") {
      name = "$name - ${item.phone}";
    }
    if ((item.id == 0)) {
      return const Text("Vui lòng chọn");
    } else {
      memberNameSelected = item.fullName;
      return Text("${item.fullName}");
    }
  }

  Widget _customPopupItemBuilder(
      BuildContext context, Member? item, bool isSelected) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 16),
      decoration: !isSelected
          ? null
          : BoxDecoration(
        border: Border.all(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child: Text("${item?.fullName ?? ""} (${item?.phone ?? ""})",textAlign: TextAlign.left),
    );
  }

}

/// Class model search
class FilterMonthMemberWidgetModel {
  int? year;
  int? month;
  int? memberId;
  String? memberName;

  FilterMonthMemberWidgetModel({
    this.year = 0,
    this.month = 0,
    this.memberId = 0,
    this.memberName = ""
  });

  isNotEmpty(){
    return (memberId != null && memberId! > 0)
        || (year != null && year! > 0)
        || (month != null && month! > 0);
  }

}

